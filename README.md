
Notes
=====

Sluice now requires v8 version 3.21.  For Mavericks, you can install via
Homebrew. (`brew install v8` does the trick.)  For Ubuntu 12.04, you'll
need to run the script `mct/build-v8-for-linux.sh`.

Ubuntu 12.04 build requirements
===============================

You'll need to install the following packages via apt:

* openjdk-6-jdk
* ant
* autoconf
* clang-3.3
* curl

To do the build on U12.04 you'll need to be firm about compiler choice and
g-speak/YOBUILD location

    env CC=clang CXX=clang++ YOBUILD=/opt/oblong/deps \
      G_SPEAK_HOME=/opt/oblong/g-speak3.12 ./bld/conflags.pl optimized \
      exec ./autogen.sh

MCT Features
============

### Forced zoom levels

Sluice now respects a new descrips list sent to edge-to-sluice:

    descrips:
    - sluice
    - "prot-spec v1.0"
    - store
    - force-levels
    ingests:
      forced-level: "zoom level here" # or leave this out if you don't want to set it to null

A present *and* non-null `forced-level` ingest will cause Sluice -- every new Fluoroscope to 
be created, at least -- to use the named zoom level and only the named zoom level.  If you 
supply either a null `forced-level` value or no `forced-level` value Sluice will go back to
choosing a zoom-level based on visible latitude.

This zoom level will be saved to and loaded from mummy proteins.
