#include "GridLayout.h"

using namespace oblong::loam;
using namespace oblong::staging;

// TODO add border width/height since gaps are only internal
// TODO add option to use wrangler instead of SetLoc

GridLayout::GridLayout ()
    : scrollMode (SCROLL_BOTH), fixedw (-1), fixedh (-1), kidw (100.0),
      kidh (100.0), vgap (10.0), hgap (10.0), nRows (0), nCols (0),
      parent (NULL) {}

GridLayout::GridLayout (FlatThing *parentThing)
    : scrollMode (SCROLL_BOTH), fixedw (-1), fixedh (-1), kidw (100.0),
      kidh (100.0), vgap (10.0), hgap (10.0), nRows (0), nCols (0),
      parent (parentThing) {}

void GridLayout::SetParent (FlatThing *parentThing) { parent = parentThing; }

void GridLayout::UpdateNumRowsCols (int nKids) {
    double n = nKids;
    if (scrollMode == SCROLL_BOTH) {
        nRows = (int)floor (sqrt (n));
        nCols = (int)ceil (n / nRows);
    } else if (scrollMode == SCROLL_NONE) {
        double ratio = fixedw / fixedh;
        nRows = (int)ceil (sqrt (n / ratio));
        nCols = (int)ceil (n / nRows);

        const double gapPercent = 0.06;
        kidw = fixedw / (nCols + (1 + nCols) * gapPercent);
        kidh = fixedh / (nRows + (1 + nRows) * gapPercent);
        hgap = kidw * gapPercent;
        vgap = kidh * gapPercent;

        // printf("cube(no scroll): n=%d  [%d x %d]  %.1f x %.1f  (%.1f,
        // %.1f)\n",
        //       (int)n, nRows, nCols, kidw, kidh, hgap, vgap);
    } else if (scrollMode == SCROLL_HORZ) {
        nCols = (int)ceil (n / nRows);
    } else {
        assert (scrollMode == SCROLL_VERT);
        nRows = (int)ceil (n / nCols);
    }
}

void GridLayout::SetGapSize (double width, double height) {
    hgap = width;
    vgap = height;
}

void GridLayout::SetupNoScroll (double width, double height) {
    scrollMode = SCROLL_NONE;
    fixedw = width;
    fixedh = height;
    UpdateNumRowsCols (NumKids ());
}

void GridLayout::SetupVertScroll (int nc, double width, double kidHeight) {
    scrollMode = SCROLL_VERT;
    nCols = nc;
    fixedw = width;
    kidw = (fixedw - hgap * (nc - 1)) / nc;
    kidh = (kidHeight < 0 ? kidw : kidHeight);
    UpdateNumRowsCols (NumKids ());
}

void GridLayout::SetupHorzScroll (int nr, double height, double kidWidth) {
    scrollMode = SCROLL_HORZ;
    nRows = nr;
    fixedh = height;
    kidh = (fixedh - vgap * (nr - 1)) / nr;
    kidw = (kidWidth < 0 ? kidh : kidWidth);
    UpdateNumRowsCols (NumKids ());
}

void GridLayout::SetupBothScroll (double kidWidth, double kidHeight) {
    scrollMode = SCROLL_BOTH;
    kidw = kidWidth;
    kidh = kidHeight;
    UpdateNumRowsCols (NumKids ());
}

bool GridLayout::DoLayout () {
    if (parent == NULL)
        return false;

    const int nKids = NumKids ();
    UpdateNumRowsCols (nKids);

    Vect vtl = parent->LocOf (FlatThing::V::Top, FlatThing::H::Left);
    Vect over = parent->Over ();
    Vect up = parent->Up ();

    if (nCols % 2 == 0)
        vtl -= over * ((hgap + kidw) * ((nCols - 1) / 2.0));
    else
        vtl -= over * ((hgap + kidw) * (nCols / 2));

    if (nRows % 2 == 0)
        vtl += up * ((vgap + kidh) * ((nRows - 1) / 2.0));
    else
        vtl += up * ((vgap + kidh) * (nRows / 2));

    // printf("#kids=%d  grid=[%d x %d]  kids=[%.1f x %.1f]\n",
    //         nKids, nRows, nCols, kidw, kidh);

    int iKid = 0;
    for (int iRow = 0; iRow < nRows; iRow++) {
        for (int iCol = 0; iCol < nCols; iCol++) {
            if (iKid >= nKids)
                break;
            FlatThing *kid =
                dynamic_cast<FlatThing *>(parent->NthChild (iKid++));
            if (kid == NULL) {
                fprintf (stderr,
                         "Warning: GridLayout found null kid (%d,%d=%d)\n",
                         iCol,
                         iRow,
                         iKid);
                continue;
            }
            float64 ar = kid->Width () / kid->Height ();
            if (nCols >= nRows)
                kid->SetSize (kidw, kidw / ar);
            else
                kid->SetSize (kidh * ar, kidh);
            kid->OrientLikeOther (parent);
            Vect v = vtl + over * (iCol * (kidw + hgap)) -
                     up * (iRow * (kidh + vgap));
            kid->SetAlignment (FlatThing::V::Center, FlatThing::H::Center);
            kid->SetLoc (v);
            // printf("%d: (%d, %d) -> (%.1f, %.1f, %.1f)\n", iKid, iRow, iCol,
            // v.x, v.y, v.z);
        }
    }

    return true;
}
