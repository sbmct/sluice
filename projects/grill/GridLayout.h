#ifndef __GRID_LAYOUT_H__
#define __GRID_LAYOUT_H__

#include "libLoam/c++/AnkleObject.h"
#include "libNoodoo/FlatThing.h"

namespace oblong {
namespace staging {

using namespace oblong::noodoo;

/**
 * The GridLayout class helps arrange FlatThings so that they are in a
 * grid centered around and oriented like their parent object.  The
 * grid can be configured for a vertical arrangement, a horizontal
 * arrangement, or for expansion in both dimensions.
 *
 * GridLayout is designed to work with the children of a parent
 * object.  Only children that descend from FlatThing are manipulated.
 * If other children are present, they will be ignored, thus leaving a
 * gap in the grid.
 */
class GridLayout : public AnkleObject {
    PATELLA_SUBCLASS (GridLayout, AnkleObject);

public:
    GridLayout ();
    GridLayout (FlatThing *parentThing);

    void SetParent (FlatThing *parentThing);
    int NumKids () const {
        return (parent == NULL ? 0 : parent->ChildCount ());
    }

    /**
     * Configure this GridLayout for vertical scrolling.  The number of
     * columns is specified by \a nc, while the total width of the
     * layout is specified by \a width.  All of the objects (plus any
     * horizontal gaps) will be forced to fit within this width.  If \a
     * kidHeight is negative (the default), then the height of each
     * object will set to be equal to the calculated width.
     */
    void SetupVertScroll (int nc, double width, double kidHeight = -1);

    /**
     * Configure this GridLayout for horizontal scrolling.  The number
     * of rows is specified by \a nr, while the total height of the
     * layout is specified by \a height.  All of the objects (plus any
     * vertical gaps) will be forced to fit within this height.  If \a
     * kidWidth is negative (the default), then the width of each object
     * will set to be equal to the calculated height.
     */
    void SetupHorzScroll (int nr, double height, double kidWidth = -1);

    /**
     * Configure this GridLayout for expansion/scrolling in both
     * dimensions.  The parameters (\a kidWidth and \a kidHeight)
     * specify the size of each child object.  The number of rows and
     * columns will be automatically calculated to generate a roughly
     * square grid (with more columns than rows if necessary).
     */
    void SetupBothScroll (double kidWidth, double kidHeight);

    /**
     * Configure this GridLayout to have a fixed total size.  The \a
     * width and \a height parameters specify the bounds on the layout.
     * All children will be sized to fit within these dimensions.
     */
    void SetupNoScroll (double width, double height);

    /**
     * Specify the horizontal (\a width) and vertical (\a height) gap
     * size, which corresponds to the empty space between children.  The
     * default size is 10mm for both dimensions.
     */
    void SetGapSize (double width, double height);

    /**
     * Instruct this GridLayout to modify the size and location of child
     * objects to form a a grid.  GridLayout uses soft functions to
     * directly move the children (e.g., SetLoc vs. SetLocHard or a
     * translation wrangler).  Also, only children that are descendants
     * of FlatThing will be affected since that is the most abstract
     * class that exposes size and location information.
     */
    bool DoLayout ();

private:
    enum ScrollMode { SCROLL_VERT, SCROLL_HORZ, SCROLL_BOTH, SCROLL_NONE };

    void UpdateNumRowsCols (int nKids);

    ScrollMode scrollMode;
    double fixedw, fixedh, kidw, kidh, vgap, hgap;
    int nRows, nCols;
    FlatThing *parent;
};
}
} // end namespace

#endif
