
/* (c)  oblong industries */

#ifndef HOBOKEN_IS_NEAR_MANHATTAN_BUT_STILL_IN_JERSEY
#define HOBOKEN_IS_NEAR_MANHATTAN_BUT_STILL_IN_JERSEY

#include "Acetate.h"

#include "VidfluxBrowser.h"
#include "DeckBrowser.h"

#include "MzHandi.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * Hoboken is the area below the main slide viewer (Trenton)
 * and contains the SystemArea (left), VidfluxBrowser (middle),
 * and DeckBrowser (right).
 */
class Hoboken : public Acetate, public MzHandi::Evts {
    PATELLA_SUBCLASS (Hoboken, Acetate);

protected:
    ObRef<VidfluxBrowser *> vdf_brws;
    ObRef<DeckBrowser *> dck_brws;

public:
    VidfluxBrowser *OurVidfluxBrowser () const { return ~vdf_brws; }
    DeckBrowser *OurDeckBrowser () const { return ~dck_brws; }

    VideoThing *NewVideoFromSlot (const int64 &slot);
    VideoThing *NewVideoFromViddleName (const Str &name);

    virtual void InitiallyUnfurl (Atmosphere *atm);

    virtual void ArrangeDenizens ();

    virtual void InitializePoolRegistration (Hasselhoff *hoff);

    virtual ObRetort MzHandiCondense (MzHandiCondenseEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
};
}
} // bye bye namespaces sluice, oblong

#endif
