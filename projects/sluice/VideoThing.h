
/* (c)  oblong industries */

#ifndef VideoThing_H
#define VideoThing_H

#include "Acetate.h"
#include "FPS.h"
#include "MzHandi.h"

#include <libImpetus/OEBlurt.h>
#include <libImpetus/OEPointing.h>

#include <libNoodoo/TexQuad.h>
#include <libNoodoo/VidQuad.h>
#include <libNoodoo/GlyphoString.h>

#include <projects/event-slurper/TaggedVidQuad.h>
#include <projects/quartermaster/ViddleSet.h>

#include <libBasement/ob-logging-macros.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::loam;
using namespace oblong::quartermaster;
using namespace oblong::remote;
using namespace oblong::impetus;

/**
 * VideoOrigin: Represents a single computer source from which
 *              one or more viddles originated from
 */
class VideoOrigin : public AnkleObject {
    PATELLA_SUBCLASS (VideoOrigin, AnkleObject);

public:
    Str name;
    ObTrove<Str> viddle_names;
};

/**
 * VideoPassthroughManager: Keeps track of lists of VideoOrigins and handles
 *                          provenance checking
 */
class VideoPassthroughManager : public AnkleObject {
    PATELLA_SUBCLASS (VideoPassthroughManager, AnkleObject);

    ObMap<Str, Str> provenance_to_source_map;
    ObMap<Str, VideoOrigin *> video_origins;

public:
    VideoOrigin *VideoOriginWithViddleName (Str viddle_name);
    Str ExtractBaseProvenance (Str provenance);

    bool HasControl (Str provenance, Str viddle_name);
    bool TryAcquireControl (Str provenance, Str viddle_name);
    void ReleaseControl (Str provenance);
};

/**
 * VideoThing
 */
class VideoThing : public Acetate,
                   public OEBlurt::Evts,
                   public OEPointing::Evts,
                   public MzHandi::Evts {
    PATELLA_SUBCLASS (VideoThing, Acetate);

protected:
    ObRef<TaggedVidQuad *> video_quad;
    ObRef<TexQuad *> placeholder_quad;
    ObRef<Viddle *> viddle;
    ObRef<GlyphoString *> name_text;
    ObRef<VideoPassthroughManager *> passthrough_manager;

    ObUniqueTrove<Str> hoverers;

    int64
        tag; // Used to identify which vidfluximp this VT was instantiated from
    Str viddle_name; // The viddle name associated with this VideoThing.
                     // Needed since a particular viddle may not have been
                     // live at the time  of creating this VideoThing, such as
                     // during the loading of a dossier

#ifdef PERFORMANCE
    ObRef<VidFPS *> fps;
#endif

public:
    SOFT_MACHINERY (SoftColor, BackingColor);

    VideoThing (VideoPassthroughManager *manager = NULL);
    virtual ~VideoThing ();

    virtual void ArrangeDenizens ();

    void SetPassthroughManager (VideoPassthroughManager *manager) {
        passthrough_manager = manager;
    }

    TaggedVidQuad *VideoQuad () const { return (~video_quad); };
    TexQuad *PlaceHolderQuad () const { return (~placeholder_quad); };
    GlyphoString *NameText () const { return ~name_text; }

    Viddle *GetViddle () const { return (~viddle); }
    ObRetort SetViddle (Viddle *aViddle, VideoSource *aVideoSource = NULL);

    Str EventPoolName () const {
        return ((~video_quad) ? (~video_quad)->PoolName () : NULL);
    }
    void SetEventPoolName (const Str &poolName) {
        if (TaggedVidQuad *tvq = ~video_quad)
            tvq->SetPoolName (poolName);
    }

    VideoSource *GetVideoSource () const {
        if (TaggedVidQuad *tvq = ~video_quad)
            return tvq->VidSource ();
        return NULL;
    }

    int64 Tag () const { return tag; }
    void SetTag (int64 new_tag) { tag = new_tag; }

    void SetNameText (const Str &text);
    void CenterNameText ();

    Str ViddleName () const { return viddle_name; }
    void SetViddleName (Str newViddleName) { viddle_name = newViddleName; }

    void SetBackingColor (const ObColor &c) {
        if (SoftColor *sc = BackingColorSoft ())
            sc->Set (c);
    }

    // Overriding several FlatThing functions to pass them onto
    // its children
    void InstallVerticalAlignment (SoftFloat *sf);
    void InstallHorizontalAlignment (SoftFloat *sf);

    ObRetort VidQuadSizeChanged (VidQuad *vq, Atmosphere *atm);

    bool EventIsInside (OEPointingEvent *e);
    bool TryConsumeEvent (MzHandiEvent *e);
    bool TryAcquireControl (MzHandiEvent *e, Atmosphere *atm);
    void ReleaseControl (MzHandiEvent *e, Atmosphere *atm);

    // casts a return stroke to the conductivity source for the
    // given electrical event, letting it know that passthrough
    // is or is not allowed and how to update any relevant
    // HandiPoint styles.
    void CastPassthroughAllowed (ElectricalEvent *e,
                                 const bool &allow_passthrough,
                                 Atmosphere *atm);

    // returns true iff there is an underlying TaggedVidQuad
    // and that TaggedVidQuad has a non-NULL VideoSource
    bool HasVideoSource () const;

    ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *atm);
    ObRetort OEBlurtVanish (OEBlurtVanishEvent *e, Atmosphere *atm);
    ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e, Atmosphere *atm);

    ObRetort MzHandiEnter (MzHandiEnterEvent *e, Atmosphere *atm);
    ObRetort MzHandiExit (MzHandiExitEvent *e, Atmosphere *atm);
    ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                   Atmosphere *atm);

    virtual ObRetort Travail (Atmosphere *atm);

    void DrawSelf (VisiFeld *vf, Atmosphere *atm);
};
}
} // bye bye namespaces sluice, oblong

#endif
