
/* (c)  oblong industries */

#ifndef UID_SPIGOT_IS_YOUR_GUARANTY_OF_QUALITY
#define UID_SPIGOT_IS_YOUR_GUARANTY_OF_QUALITY

#include <libBasement/KneeObject.h>

namespace oblong {
namespace sluice {
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class UIDSpigot : public KneeObject {
    PATELLA_SUBCLASS (UIDSpigot, KneeObject);

public:
    int64 most_recently_dribbled_uid;
    int64 next_uid;
    Str mrd_uid_str;
    Str n_uid_str;
    Str prefix, postfix;
    unt16 nm_digits;

public:
    UIDSpigot ();
    virtual ~UIDSpigot ();

    int64 MostRecentlyDribbledUID () const {
        return most_recently_dribbled_uid;
    }
    int64 NextUIDPeek () const { return next_uid; }
    int64 NextUID ();

    const Str &MostRecentlyDribbledUIDAsStr () const { return mrd_uid_str; }
    const Str &NextUIDPeekAsStr () const { return n_uid_str; }
    const Str &NextUIDAsStr () {
        NextUID ();
        return mrd_uid_str;
    }

    const Str &Prefix () const { return prefix; }
    void SetPrefix (const Str &pf) { prefix = pf; }

    const Str &Postfix () const { return postfix; }
    void SetPostfix (const Str &pf) { postfix = pf; }

    unt16 NumDigits () const { return nm_digits; }
    void SetNumDigits (unt16 nd) { nm_digits = nd; }

    void SetMostRecentlyDribbledUID (int64 uid);

    ObRetort ParseMostRecentlyDribbledUID (const Str &uid_str);
    bool ConditionallyUpdateMostRecentlyDribbledUID (const Str &uid_str);

    void FreshenStrings ();
};
}
} // the ol' ticker finally stops for namespaces sluice and oblong.

#endif
