
#ifndef SLUICE_PLASMA_API_AND_OTHER_CONSTANTS
#define SLUICE_PLASMA_API_AND_OTHER_CONSTANTS

#include <libPlasma/c++/Plasma.h>
#include <libBasement/ob-logging-macros.h>
#include <libLoam/c++/ObTrove.h>

namespace oblong {
namespace sluice {

using namespace oblong::plasma;
using namespace oblong::basement;
using namespace oblong::loam;

template <typename... Args> Slaw ProtV1 (Args... args) {
    return Slaw::List ("sluice", "prot-spec v1.0", args...);
}

template <typename... Args> Slaw ProtV2 (Args... args) {
    return Slaw::List ("conduce", "prot-2.0", args...);
}

/**
* SluiceConsts:
*   This class is used for keeping track and managing all constants used from
*   within sluice.  This includes all pool names as well as Descrips for all
*   known proteins within Sluice's API.  All classes within sluice should use
*   the accessors provided in this class for retrieving pool names and matching
*   against descrips.
*/

class SluiceConsts {
public:
    /** logical pool names
     * -----------------------------------------------------*/
    static Str TardisLogicalHoseName () { return "sluice-tardis"; }

    static Str HeartbeatHoseName () { return "sluice-to-heart"; }

    static Str EdgeInLogicalHoseName () { return "edge-to-sluice"; }
    static Str EdgeOutLogicalHoseName () { return "sluice-to-edge"; }

    static Str TopoInLogicalHoseName () { return "topo-to-sluice"; }
    static Str TopoOutLogicalHoseName () { return "sluice-to-topo"; }

    static Str ObsInLogicalHoseName () { return "obs-to-sluice"; }

    static Str FluoroResponseLogicalHoseName () { return "fluoro-to-sluice"; }
    static Str FluoroRequestLogicalHoseName () { return "sluice-to-fluoro"; }

    static Str StoreResponseLogicalHoseName () { return "store-to-sluice"; }
    static Str StoreRequestLogicalHoseName () { return "sluice-to-store"; }

    static Str DebugInLogicalHoseName () { return "sluice-debug-in"; }

    static Str DebugOutLogicalHoseName () { return "sluice-debug-out"; }

    static Str AlertsLogicalHoseName () { return "alerts-to-sluice"; }

    static Str VidMessagingLogicalHoseName () { return "vid-messaging"; }

    static Str SynchroSystemInHoseName () { return "synchro-to-sluice"; }
    static Str SynchroSystemOutHoseName () { return "sluice-to-synchro"; }

    static Str MrProcessorHoseName () { return "mz-processor"; }

    static Str StoreInLogicalHoseName () { return "store-to-sluice"; }
    static Str StoreOutLogicalHoseName () { return "sluice-to-store"; }

    static Str VBOOutLogicalHoseName () { return "sluice-to-gl"; }

    /** descrips
     * ---------------------------------------------------------------*/
    /** edge device pool proteins */
    static Slaw AtlasViewRequestDescrips ();
    static Slaw AtlasViewPSADescrips ();
    static Slaw ZoomRequestDescrips ();
    static Slaw ZoomParameterDescrips ();

    static Slaw FaultsRequestDescrips ();
    static Slaw FaultsResponseDescrips ();

    static Slaw KindsRequestDescrips ();
    static Slaw KindsResponseDescrips ();

    static Slaw NewFluoroTemplateRequestDescrips ();
    static Slaw NewFluoroTemplateResponseDescrips (bool advisory);
    static Slaw FluoroscopesRequestDescrips ();
    static Slaw FluoroscopesResponseDescrips ();

    static Slaw AddInculcatorsDescrips ();
    static Slaw RemoveInculcatorsDescrips ();
    static Slaw ChangeInculcatorsDescrips ();
    static Slaw ListInculcatorsDescrips ();
    static Slaw ListInculcatorsResDescrips ();

    static Slaw NewFluoroInstanceRequestDescrips ();
    static Slaw NewFluoroInstanceResponseDescrips ();
    static Slaw NewFluoroInstancePSADescrips ();
    static Slaw RemoveFluoroInstanceRequestDescrips ();
    static Slaw RemoveAllFluorosDescrips ();

    static Slaw FluoroPositionChangedDescrips ();
    static Slaw FluoroPositionChangeRequest (const Slaw &fluoro_qid_slaw);

    static Slaw ConfigureFluoroRequestDescrips ();
    static Slaw UpdateFluoroResponseDescrips (const Slaw &fluoro_qid_slaw);

    static Slaw RequestStaticLayersConfigDescrips ();
    static Slaw AddStaticLayerDescrips ();
    static Slaw RemoveStaticLayerDescrips ();
    static Slaw RemoveAllStaticLayersDescrips ();

    static Slaw ReorderRequestDescrips ();
    static Slaw WebAppFluoroHoverDescrips ();
    static Slaw WebAppNoHoverDescrips ();

    static Slaw FluoroHoverPSA ();
    static Slaw NoFluoroHoverPSA ();

    static Slaw MummifyRequestDescrips ();
    static Slaw VivifyRequestDescrips ();

    static Slaw NodeHoverPSA ();
    static Slaw PokedItDescrips ();

    static Slaw ClearFaultsRequestDescrips ();

    static Slaw SkewerDescrips ();

    static Slaw NewWebRequestDescrips ();
    static Slaw RemoveWebRequestDescrips ();

    static Slaw SnapshotPostDescrips ();

    static Slaw CurateRequestDescrips ();
    static Slaw ExhibitRequestDescrips ();

    /** texture pool proteins */
    static Slaw TextureRequestDescrips (const Slaw &fluoro_qid_slaw,
                                        const Str &fluoro_type);
    static Slaw TextureResponseDescrips ();

    // video pool proteins
    static Slaw VideoRequestDescrips (const Slaw &fluoro_qid_slaw,
                                      const Str &fluoro_type);
    static Slaw VideoResponseDescrips ();

    /** topology proteins */

    static Slaw TopologyRequestDescrips ();
    static Slaw TopologyAddDescrips ();
    static Slaw TopologyChangeDescrips ();
    static Slaw TopologyRemoveDescrips ();
    static Slaw TopologyClearDescrips ();

    /** time proteins */
    static Slaw TimeAnnounceRequestDescrips ();
    static Slaw TimeAnnouncePSADescrips ();
    static Slaw TimeSetDescrips ();
    static Slaw TimeDisplaySetCfg ();
    static Slaw TimeDisplayRequestCfg ();
    static Slaw TimeDisplayResponseCfg ();

    /** heartbeat proteins */
    static Slaw HeartbeatDescrips ();

    /** observations proteins */
    static Slaw ObservationsDescrips ();

    /** vid messaging proteins */
    static Slaw EventPoolDescrips ();
    static Slaw ViddleDisplayNameDescrips ();
    static Slaw VidHurlingDescrips ();
    static Slaw VidYankingDescrips ();

    /** synchro system proteins */
    static Slaw MayJoinDescrips (Str prov, int64 tid);
    static Slaw SluiceStateDescrips (Str prov, int64 tid);
    static Slaw DeckStateDescrips ();
    static Slaw RatchetDescrips (Str prov, int64 tid);
    static Slaw ClearWindshieldDescrips ();
    static Slaw ClearFluoroscopesDescrips ();
    static Slaw FeldStreamStateResponseDescrips ();

    /** bookmarks proteins */
    static Slaw BookmarksAnnounceRequestDescrips ();
    static Slaw BookmarksAnnounceResponseDescrips ();
    static Slaw BookmarksNewRequestDescrips ();
    static Slaw BookmarksNewResponseDescrips ();
    static Slaw BookmarksDeleteRequestDescrips ();

    /** mr processor proteins */
    static Slaw StopStreamingProcessesDescrips ();
    static Slaw StartStreamingProcessesDescrips ();

    /** Debug utility proteins */
    static Slaw TopoDebugRequestDescrips ();
    static Slaw TopoDebugResponseDescrips ();

    /** security proteins */
    static Slaw AuthLoginRequestDescrips ();
    static Slaw AuthLogoutRequestDescrips ();
    static Slaw AuthRequestCurrentCredsDescrips ();
    static Slaw AuthCurrentCredsReponseDescrips ();

    // Fluoroscope API
    static Slaw ListFluoroscopeTemplatesReqDescrips ();
    static Slaw ListFluoroscopeTemplatesResDescrips ();
    static Slaw RemoveFluoroscopeTemplateDescrips ();

    // Snapshot API
    static Slaw SnapshotRequestDescrips ();
    static Slaw SnapshotResponseDescrips ();

    // Subscription API
    static Slaw SubscriptionCreate ();
    static Slaw SubscriptionRemove ();
    static Slaw SubscriptionReceiveData (Slaw ident);
    static Slaw SubscriptionReceiveDelta (Slaw ident);
    static Slaw SubscriptionHeartbeat ();

    // Web Rendering API
    static Slaw VBOInfoUpdated (Slaw ident);
    static Slaw VBOInfoPayload (Slaw ident);

private:
    static const int GetEnvMaxNodeHistory ();
};
}
} // namespaces oblong, sluice

#endif // SLUICE_PLASMA_API_AND_OTHER_CONSTANTS
