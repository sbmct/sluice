
#ifndef I_DESPERATELY_WANT_TO_REMOVE_COMPOSITE_NODE_FROM_SLUICE
#define I_DESPERATELY_WANT_TO_REMOVE_COMPOSITE_NODE_FROM_SLUICE

#include "Node.h"
#include "Timeseries.h"
#include <set>

namespace oblong {
namespace sluice {

template <typename PairT> struct first_greater {
    bool operator()(const PairT &a, const PairT &b) {
        if (a.first == b.first)
            return a.second < b.second;
        else
            return a.first > b.first;
    }
};

class CompositeNode : public Node {
    PATELLA_SUBCLASS (CompositeNode, Node);

private:
    typedef std::multimap<Node *, float64> nodetimes_t;
    typedef std::pair<float64, Node *> timenode_t;
    typedef first_greater<timenode_t> comp_t;
    typedef std::set<timenode_t, comp_t> timenodes_t;

    nodetimes_t additions, expirations;
    timenodes_t nodes_by_time;

    void RemoveNodeWithTime (Node *n, const float64 t);
    const bool
    HasExpirationBetween (Node *n, const float64 min_t, const float64 max_t);

public:
    CompositeNode (const Str ident);
    ~CompositeNode ();

    void AddNode (Node *n, const float64 t);
    const bool RemoveNode (Node *n, const float64 t);
    void ExpireNode (Node *n, const float64 t);

    virtual bool HasPath (const float64 t) const { return false; }
    virtual void SetPath (const ObTrove<LatLon> &lls, const float64 t) {}

    virtual const bool HasGeoloc (const float64 t);

    size_t TotalDataSize () const {
        return additions.size () + expirations.size () + nodes_by_time.size ();
    }
};
}
}

#endif //! I_DESPERATELY_WANT_TO_REMOVE_COMPOSITE_NODE_FROM_SLUICE
