/* Copyright (C) 2011 Oblong Industries */

#ifndef OBLONG_SLUICE_NODESTOREIMPL_H
#define OBLONG_SLUICE_NODESTOREIMPL_H

#include <map>
#include <set>
#include <unordered_set>

#include <libBasement/ob-hook-troves.h>
#include <libBasement/KneeObject.h>
#include <libLoam/c++/FatherTime.h>

#include <libPlasma/c++/Hose.h>

#include <algorithm>

#include "NodeStore.h"

#include "Node.h"
#include "geo_utils.h"
#include "Hasselhoff.h"
#include "DateParser.h"
#include "GeoTree.h"
#include "GeoSubset.h"
#include "Inculcator.h"
#include "Grouper.h"
#include "Javascript.h"

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;

/**
 * Actual implementation of the NodeStore class
 *
 */
class NodeStoreImpl : public NodeStore {
    PATELLA_SUBCLASS (NodeStoreImpl, NodeStore);

protected:
    ObTrove<Node *> delete_me_not;
    typedef std::pair<Node *, float64> node_update_t;
    std::set<node_update_t> to_be_updated;
    std::set<node_update_t> to_be_removed;

    typedef std::pair<GeoIndex *, std::pair<Node *, float64>> indexable_t;
    typedef std::set<indexable_t> to_index_t;
    to_index_t to_index;

    typedef std::multimap<float64, Node *> node_update_history_t;
    node_update_history_t node_update_history;

    DateParser date_parser;
    void InitDateParser ();

    ObMap<float64, GeoIndex *> geo_indices;
    ObMap<GeoIndex *, Grouper *> groupers;
    Str forced_zoom_level;
    GeoIndex *GeoIndexForVisibleDegrees (const float64 visible_deg) const;

    std::map<Str, Node *> ident_map;
    std::multimap<Str, Node *> kind_map;
    std::unordered_set<std::string> kind_set;

    typedef ObMap<Inculcator *, size_t> inculmap_t;
    typedef inculmap_t::MapCons inculcons_t;
    inculmap_t inculcators;
    void AddInculcator (Inculcator *i);
    void RemoveInculcator (Inculcator *i);

    void IndexNode (Node *n, const float64 t);
    void RemoveFromComposites (Node *n, const float64 t);
    void ExpireFromComposites (Node *n, const float64 t);
    ObRetort CompositeUpdated (Node *n, const float64 t);
    void DeindexNode (Node *n);

    ObRetort StoreNodeHistory (Node *n, const float64 t);
    ObRetort RemoveNodeHistory (Node *n);

    void ExpireIndex (const float64 expireTime);

    FatherTime heartbeat_timer;
    float64 heartbeat_threshold;

    TimeState time_state;

    FatherTime sluice_time;
    float64 last_time;
    node_update_history_t::iterator earliest_history;

    static const int MAX_HISTORY_SIZE;

    void UpdateNode (Node *n, const float64 t);

    void SetupInculcators (const Slaw inculs);
    void SetupZoomLevels (const Slaw zooms);
    void SetInculcators (const Slaw inculs);

    void ReindexInculcators ();

    const bool NodeLastUpdateIsRelevant (Node *n, const float64 t);
    const bool IsAlmostZero (const float64 t);
    void AdvanceEarliestHistory ();

public:
    virtual ~NodeStoreImpl () {}
    NodeStoreImpl ();
    NodeStoreImpl (const NodeStoreImpl &) = delete;
    NodeStoreImpl &operator=(const NodeStoreImpl &) = delete;

    virtual ObTrove<Inculcator *> Inculcators () override;

    void SetConfigurationSlaw (const Slaw s);

    void AddInculcators (const Slaw inculs) override;
    void RemoveInculcators (const Slaw inculs) override;

    // Add node should replace existing nodes, updating will come separately
    void AddNode (Node *n, const float64 t);
    void RemoveNode (Node *n, const float64 t);

    void ForceIndexComposites ();

    /// How many nodes are currently in the store?
    // For now, just used for testing
    const int64 NodeCount () const { return delete_me_not.Count (); }

    const int64 KindsCount () const override { return kind_set.size (); }
    void KindsPSA () const;

    Node *FindByIdent (const Str x) const;

    const void FindByLatLonRange (const LatLon bl,
                                  const LatLon tr,
                                  const float64 visible_degrees,
                                  std::set<Node *> &out);

    const void SubsetByLatLonRange (const LatLon bl,
                                    const LatLon tr,
                                    const float64 visible_degrees,
                                    GeoSubset &subset);

    ObRetort MetabolizeForceZoomLevels (const Protein &, Atmosphere *);

    ObRetort MetabolizeClearTopo (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeRemoveTopo (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeChangeTopo (const Protein &prt, Atmosphere *atmo);

    ObRetort MetabolizeObservation (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeRemoveObservations (const Protein &prt,
                                           Atmosphere *atmo);

    ObRetort MetabolizeAddInculcators (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeRemoveInculcators (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeChangeInculcators (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeListInculcators (const Protein &prt, Atmosphere *atmo);

    ObRetort MetabolizeCurateRequest (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeExhibitRequest (const Protein &prt, Atmosphere *atmo);

    ObRetort MetabolizeKindsRequest (const Protein &prt, Atmosphere *atmo);

    virtual void InitializePoolRegistration (Hasselhoff *hoff);

    const bool TimeIsInDataTimeRange (const float64 t);
    ObRetort SetSluiceTime (const float64 x);
    ObRetort SetSluiceTime (const Str time_str);
    const float64 SluiceTime ();
    const bool QueryOnLiveTime () const { return LiveTime == time_state; }

    const float64 MaxTime ();
    const float64 MinTime ();

    ObRetort SetSluiceSecondsPerSecond (const float64 x);
    const float64 SluiceSecondsPerSecond () const;

    bool IsTimePaused () const;
    ObRetort PauseTime ();
    ObRetort UnPauseTime ();
    ObRetort UseLiveTime ();

    Slaw TimeChangeNotificationSlaw ();
    void SendTimeChangeNotificationProtein ();

    ObRetort ParseDate (const Str &s, float64 &when);

    virtual ObRetort Inhale (Atmosphere *atmo);

    /// Determine whether we have crossed in to a new "zoom level"
    const bool CrossesZoomLevelBoundary (const float64 visdeg_a,
                                         const float64 visdeg_b) const;

    const float64 HeartbeatThreshold () const { return heartbeat_threshold; }
    void SetHeartbeatThreshold (const float64 x) { heartbeat_threshold = x; }

    const bool IsActiveForVisibleDegrees (Node *n, const float64 zoom);
    virtual bool GranularityForVisibleDegrees (const float64 visdeg,
                                               float64 &output) const override;

    const unt32 PigmentFromName (const Str s);

    virtual void ForceZoomLevel (const Str &);
    virtual Str ForcedZoomLevel () const;
    virtual ObRetort AdjustPausedTime (const float64 prev_max_time);
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_NODESTOREIMPL_H
