
/* (c)  Oblong Industries */

#ifndef GAZETTEER_H
#define GAZETTEER_H

#include <libBasement/KneeObject.h>
#include <libLoam/c++/Vect.h>

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;

/**
 * Gazetteer:
 *
 * Gazetteer manages the bookmarks collection.
 *
 **/
class Fluoroscope;
class Hasselhoff;
class Gazetteer : public KneeObject {
    PATELLA_SUBCLASS (Gazetteer, KneeObject);

public:
    Gazetteer ();
    virtual ~Gazetteer () {}

    virtual void InitializePoolRegistration (Hasselhoff *hoff);
    void SetFluoroscope (Fluoroscope *f);
    void SetPersistentStore (const Str &ps);
    const Str &PersistentStore () const { return persistent_store; }
    void LoadFromPersistentStore ();
    void SaveToPersistentStore ();

    void FromSlaw (const Slaw &s);
    Slaw ToSlaw ();

    bool BookmarkExists (const Str &name);
    ObRetort AddBookmark (const Str &name);
    ObRetort
    AddBookmark (const Str &name, float64 lat, float64 lon, float64 level);
    ObRetort DeleteBookmark (const Str &name);

    ObRetort MetabolizeRequestAnnounceBookmarks (const Protein &prt,
                                                 Atmosphere *atmo);
    ObRetort MetabolizeRequestNewBookmark (const Protein &prt,
                                           Atmosphere *atmo);
    ObRetort MetabolizeRequestDeleteBookmark (const Protein &prt,
                                              Atmosphere *atmo);

private:
    Gazetteer (const Gazetteer &);
    Gazetteer &operator=(const Gazetteer &);

    struct Gazette : AnkleObject {
        Str name;
        Vect loc;
        Gazette (const Str &n, const Vect &l)
            : AnkleObject (), name (n), loc (l) {}
    };

    ObRetort AnnounceBookmarks ();
    void Deposit (const Protein &prot);

    ObTrove<Gazette *> gazettes;
    Fluoroscope *fluoroscope;
    Str persistent_store;
};
}
}

#endif // GAZETTEER_H
