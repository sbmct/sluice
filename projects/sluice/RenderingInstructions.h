
// (c) Conduce Inc.

#ifndef RENDER_THIS_WAY
#define RENDER_THIS_WAY

#include "Shader.h"
#include "SlawTraits.h"
#include <vector>

namespace oblong {
namespace sluice {

// Rendering instructions for a VBO with the following layout
// [V T A...]
// V: vertex, 0-n floats
// T: texture, 0-n floats
// A...: attributes, each with some number of floats
class RenderingInstructions {
public:
    struct Attr {
        Attr (GLuint i, size_t s, Str n, GLint z)
            : index{i}, start{s}, name{n}, size{z} {}
        Attr () = default;
        Attr (const Attr &) = default;
        Attr (Attr &&) = default;
        Attr &operator=(const Attr &) = default;
        Attr &operator=(Attr &&) = default;
        GLuint index;
        size_t start;
        Str name;
        GLint size;
    };

private:
    bool setup_complete{false};
    size_t vertex_start{0};
    size_t texture_start{0};
    size_t n_verts{0};
    size_t n_texcoords{0};

    std::vector<Attr> attrs;

    // Some GL state that we'll want to restore. If we were
    // programming in Java, we'd mark these bits `transient`.
    GLboolean old_depth_mask{};

    void SetupGLState (GLint vbo_id);
    void TeardownGLState ();

public:
    void SetNumVerts (size_t);
    void SetNumTexCoords (size_t);
    void AddAttr (Str name, size_t size);

    void SetVertexStart (size_t);
    void SetTextureStart (size_t);
    void SetAttrStart (Str name, size_t size);

    // returns true if setup work was necessary
    bool Setup (Shader *);

    // Assuming that the VBO and texture are already bound,
    // draw!
    void draw (GLint vbo_id, GLsizei n_indices);

    size_t getNVerts () const { return n_verts; }
    size_t getVertexStart () const { return vertex_start; }
    size_t getNTexCoords () const { return n_texcoords; }
    size_t getTextureStart () const { return texture_start; }
    const std::vector<Attr> &getAttrs () const { return attrs; }
};
}
}

DECLARE_SLAW_TRAITS_SPECIALIZATION (
    oblong::sluice::RenderingInstructions::Attr);
DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::RenderingInstructions);

#endif //!RENDER_THIS_WAY
