
/* (c)  oblong industries */

#ifndef PELLIGATOR_TOR_TOR_TOR_TOR_TOR_TOR_TOR_TOR_TOR
#define PELLIGATOR_TOR_TOR_TOR_TOR_TOR_TOR_TOR_TOR_TOR

#include <libBasement/InterpFloat.h>
#include <libBasement/RWrangler.h>

#include <libNoodoo/GlyphoString.h>
#include <libNoodoo/ShowyThing.h>
#include <libNoodoo/SoftVertexForm.h>
#include <libNoodoo/TexQuad.h>
#include <libNoodoo/VertexForm.h>
#include <libNoodoo/VertEllipse.h>

#include "Tweezers.h"
#include "MzHandi.h"
#include "Acetate.h"
#include "LockGlyph.h"
#include "VertDisk.h"
#include "UnduLine.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

#define GATOR_SCALE_FACTOR 3.5
#define GATOR_FF_UNLOCK_DETENT 60.0

/**
 * I think the following is true...
 * Pelligator handles dragging and zooming the main map (AtlasQuad).
 * It also handles dragging and zooming for all Fluoroscopes...
 */
class Pelligator : public Tweezers, public MzHandi::Evts {
    PATELLA_SUBCLASS (Pelligator, Tweezers);

    struct ScalePapoose { // hand / mouse loc when scaling begins
        Vect scale_origin;
        // original height and width of target when scale started
        float64 scale_width, scale_height;
        float64 feld_inscribed_width, feld_inscribed_height;

        float64 min_object_size, max_object_size;

        float64 along, orig_detent;

        float64 ffd_start, ffd_pass;

        float64 ffd_detent_offset;

        Vect relo_origin;
        bool is_full_felding;

        ScalePapoose () {}
        ScalePapoose (const Vect &so,
                      const float64 &sw,
                      const float64 &sh,
                      const Vect &rfc,
                      const float64 &fiw,
                      const float64 &fih) {
            scale_origin = so;
            scale_width = sw;
            scale_height = sh;
            // relevant_feld_center = rfc;
            feld_inscribed_width = fiw;
            feld_inscribed_height = fih;
            min_object_size = 75.0;
            max_object_size = 4530.0;
            along = 0.0;
            orig_detent = 30.0;

            float64 w_to_grow = fiw - sw;
            ffd_detent_offset = w_to_grow / GATOR_SCALE_FACTOR;

            float64 lock_detent = GATOR_FF_UNLOCK_DETENT;
            if (sw == fiw)
                lock_detent = 0.0;

            // and some initial starting values, given all the above.
            if (ffd_detent_offset > 0.0) {
                ffd_start = ffd_detent_offset + orig_detent;
                ffd_pass = ffd_start + lock_detent;
            } else //(ffd_detent_offset  <  0.0)
            {
                ffd_start = ffd_detent_offset - orig_detent;
                ffd_pass = ffd_start - lock_detent;
            }
            is_full_felding = false;
        }
    };

    struct MovePapoose {
        Vect move_offset;
        Vect move_origin;
        Vect mover_loc;
        MovePapoose () {}
        MovePapoose (const Vect &moff, const Vect &mori, const Vect &mlo) {
            move_offset = moff;
            move_origin = mori;
            mover_loc = mlo;
        }
    };

private:
    Str motive_prov;
    /* contains all child showies except for the GlyphoStrings, such
     * that these child objects may be styled differently (via AdjColor)
     * from the strings
     */
    ObRef<ShowyThing *> glyph_bucket;
    ObRef<LockGlyph *> ff_marker;
    ObRef<TexQuad *> scale_backing_glyph;
    ObRef<TexQuad *> scale_progress_glyph;
    ObRef<RWrangler *> scale_progress_rotter;
    ObRef<TexQuad *> origin_marker;
    ObRef<TexQuad *> loc_marker;
    ObRef<TexQuad *> prv_tex;

    ObRef<SoftVect *> shared_scale_glyph_loc;

    ObRef<UnduLine *> umbilical;

    ObRef<GlyphoString *> del_string;
    ObRef<GlyphoString *> debugo_string;

    Vect grab_origin;
    Vect targ_orig_loc;
    v2float64 targ_orig_size;
    float64 last_sugg_scale;

    MovePapoose move_poose;
    ScalePapoose scale_poose;

    VisiFeld *feld_origin;

    bool am_conducting;

    v2float64 grab_rel_loc;

    Vect avg_input_aim;
    float64 aim_alpha;
    Vect avg_input_velocity;
    float64 velocity_alpha;

    ObWeakRef<FlatThing *> target;
    ObWeakRef<FlatThing *> target_parent;

    // list of all living Pelligators
    static ObUniqueTrove<Pelligator *, WeakRef> all_pelligator_trove;

    bool target_is_endangered;

public:
    Pelligator (FlatThing *ft);
    virtual ~Pelligator ();

    void SetTargetsParent (FlatThing *ft) { target_parent = ft; }

    virtual ObRetort Dismiss (bool break_connection = true);

    void InitiateScaling (MzHandiEvent *e, Atmosphere *atm);
    void AbortScaling (MzHandiEvent *e, Atmosphere *atm);
    void WrapUpScaling (MzHandiEvent *e, Atmosphere *atm);

    void InitiateFullFelding (const Vect &origin, const Vect &through);
    void AbortFullFelding (const Vect &scale_from);

    void SetMigrantLoc (MzHandiEvent *e, Atmosphere *atm); // const Vect &v);
    // void SetMigrantLocHard (const Vect &v);
    // void SetMigrantLocGoal (const Vect &v);

    void SetGlyphLoc (const Vect &loc);
    void SetGlyphLocHard (const Vect &loc);
    void SetGlyphLocGoal (const Vect &loc);

    void SetScaleGlyphSize (const float64 &frac);

    float64 RespectfullySetMigrantSizeFromWidth (const float64 &w,
                                                 MzHandiEvent *e,
                                                 Atmosphere *atm);

    void UpdateScaleDetentGlyphs (const float64 &m);
    void UpdateFeldCenteringGlyphs (const float64 &m,
                                    const Vect &scale_from,
                                    const Vect &origin,
                                    const Vect &through,
                                    const bool &fwd,
                                    MzHandiEvent *e,
                                    Atmosphere *atm);
    void UpdateScaleGlyphs (const float64 &m,
                            const float64 &w,
                            const Vect &scale_from,
                            const bool &reset,
                            const bool &fwd,
                            MzHandiEvent *e,
                            Atmosphere *atm);
    void UpdateFullFeldOrigin (const Vect &origin, const Vect &through);

    float64 CalculateScaleProgress (const Vect &aim,
                                    const Vect &origin,
                                    const Vect &prev_origin);

    void
    InitiateMoving (const Vect &grab_loc, MzHandiEvent *e, Atmosphere *atm);
    void ContinueMoving (MzHandiEvent *e, Atmosphere *atm); // const Vect &v);
    void AbortMoving (MzHandiEvent *e, Atmosphere *atm);
    void WrapUpMoving (MzHandiEvent *e, Atmosphere *atm);

    bool IsTargetEndangered () const;
    void EndangerTarget ();
    void UnEndangerTarget ();

    void ResetScaleThresholds (const bool &fwd);

    void SetDebugString (const Str &s);

    const Str &MotiveProvenance () const { return motive_prov; }
    void SetMotiveProvenance (const Str &mp);

    ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e, Atmosphere *atm);

    ObRetort
    ReturnStroke (const Slaw &news, ElectricalEvent *ee, Atmosphere *atm);

    void ReportChangesToWindshield (Atmosphere *atm);

    Vect GrabOffset () const;

    virtual ObRetort Travail (Atmosphere *atm);
};
}
} // bye bye namespaces mezzanine, oblong

#endif
