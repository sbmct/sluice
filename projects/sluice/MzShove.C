
/* (c)  oblong industries */

#include "MzShove.h"

using namespace oblong::sluice;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

Vect MzShoveEvent::ShoveOffset () const { return shove_loc - prev_shove_loc; }

const Vect &MzShoveEvent::PrevShoveLoc () const { return prev_shove_loc; }

const Vect &MzShoveEvent::ShoveLoc () const { return shove_loc; }

const Vect &MzShoveEvent::ShoveOrigin () const { return shove_origin; }

float64 MzShoveEvent::ShoveDistance () const {
    return (shove_loc - shove_origin).Dot (shove_direction);
}

const Vect &MzShoveEvent::ShoveDirection () const { return shove_direction; }

float64 MzShoveEvent::ShimmyDelta () const { return shimmy_delta; }

const Vect &MzShoveEvent::ShimmyAxis () const { return shimmy_axis; }

OEDisplacementEvent *MzShoveEvent::RawOEDisplacementEvent () const {
    return dynamic_cast<OEDisplacementEvent *> (RawEvent ());
}

void MzShoveEvent::SetRawOEDisplacementEvent (OEDisplacementEvent *e) {
    SetRawEvent (e);
}

void MzShoveEvent::SetInitialLocationAndDirection (const Vect &loc,
                                                   const Vect &axis) {
    shove_origin = loc;
    shove_direction = axis;
}

void MzShoveEvent::SetPreviousShove (const Vect &shv) { prev_shove_loc = shv; }

void MzShoveEvent::SetCurrentShove (const Vect &shv) { shove_loc = shv; }

void MzShoveEvent::SetShimmyDeltaAndAxis (float64 sd, const Vect &axis) {
    shimmy_delta = sd;
    shimmy_axis = axis;
}

static Str sho ("shove-origin");
static Str shd ("shove-direction");
static Str shl ("shove-location");
static Str pvl ("prev-shove-location");

void MzShoveEvent::SynthesizeInternalProtein () {
    super::SynthesizeInternalProtein ();
    Slaw more = Slaw::Map (sho, shove_origin, shd, shove_direction, shl,
                           shove_loc, pvl, prev_shove_loc);

    evpro = Protein (evpro.Descrips (), evpro.Ingests ().MapMerge (more));
}

ObRetort MzShoveEvent::AnalyzeInternalProtein () {
    ObRetort ret = super::AnalyzeInternalProtein ();
    if (!ret.IsSplend ())
        return ret;

    Slaw ing = evpro.Ingests ();
    bool all = true;
    all &= ing.MapFind (sho).Into (shove_origin);
    all &= ing.MapFind (shd).Into (shove_direction);
    all &= ing.MapFind (shl).Into (shove_loc);

    all &= ing.MapFind (pvl).Into (prev_shove_loc);

    if (!all)
        return OB_INCOMPLETE_EVENT_PROTEIN;
    return OB_OK;
}
