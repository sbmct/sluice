
/* (c)  oblong industries */

#ifndef DOSSIER_IS_IN_NO_WAY_HARD_TO_PRONOUNCE
#define DOSSIER_IS_IN_NO_WAY_HARD_TO_PRONOUNCE

#include "Acetate.h"

#include "Trenton.h"
#include "Paramus.h"
#include "Hoboken.h"
#include "Windshield.h"

#include "MzShove.h"
#include "MzHandi.h"
#include "MzTender.h"
#include "MzScroll.h"

#include "UIDSpigot.h"

#include <libBasement/TWrangler.h>

#include <libLoam/c++/LoamxxRetorts.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * Dossier encapsulates and organizes the elements of a mezz session,
 * including Assets in Paramus, the SystemArea, DVIBrowser, and DeckBrowser
 * in Hoboken, and the Deck(s) in Trenton.
 */
class Dossier : public Acetate,
                public MzShove::Evts,
                public MzHandi::Evts,
                public MzTender::Evts,
                public MzScroll::Evts,
                public OEBlurt::Evts {
    PATELLA_SUBCLASS (Dossier, Acetate);

public:
    ObRef<TWrangler *> pushy_wrangler;

    ObRef<Trenton *> ton;
    ObRef<Paramus *> mus;
    ObRef<Hoboken *> ken;
    ObRef<Windshield *> eld;

    UIDSpigot asset_uid_source;

    Str shove_prov;

    float64 pushback_lock_dist;

    Str uid;

public:
    Dossier ();

    Trenton *OurTrenton () const { return ~ton; }
    Paramus *OurParamus () const { return ~mus; }
    Hoboken *OurHoboken () const { return ~ken; }

    Windshield *OurWindshield () const { return ~eld; }

    const Str &MostRecentlyDribbledAssetUID () const {
        return asset_uid_source.MostRecentlyDribbledUIDAsStr ();
    }

    const Str &NextAssetUIDPeek () const {
        return asset_uid_source.NextUIDPeekAsStr ();
    }

    const Str &NextAssetUID () { return asset_uid_source.NextUIDAsStr (); }

    void SetMostRecentlyDribbledAssetUID (int64 duid) {
        asset_uid_source.SetMostRecentlyDribbledUID (duid);
    }

    float64 PushbackLockDist () const { return pushback_lock_dist; }
    void SetPushbackLockDist (float64 pld) { pushback_lock_dist = pld; }

    const Str &UID () { return uid; }
    void SetUID (const Str &u) { uid = u; }

    virtual void InitiallyUnfurl (Atmosphere *atm);

    virtual void ArrangeDenizens ();

    virtual void InitializePoolRegistration (Hasselhoff *hoff);

    virtual ObRetort AppendParamusElementDuringLoad (const Str &dos_uid,
                                                     const Str &ast_uid,
                                                     const Str &path_prefix);

    virtual ObRetort AppendWindshieldElementDuringLoad (const Str &dos_uid,
                                                        const Str &ast_uid,
                                                        const Str &el_uid,
                                                        const Vect &loc,
                                                        float64 wid,
                                                        float64 hei,
                                                        const Str &path_prefix);

    virtual ObRetort AppendScreenDuringLoad (const Str &dos_uid,
                                             const Str &ast_uid,
                                             const Str &ws_uid,
                                             const Str &path_prefix);

    Slaw PushbackParametersMap () const;

    Vect PushbackDisplacement () const;
    Vect PushbackDisplacementGoal () const;
    void SetPushbackDisplacement (const Vect &pd);
    void SetPushbackDisplacementHard (const Vect &pd);

    float64 PushbackFraction () const;
    float64 PushbackFractionGoal () const;
    void SetPushbackFraction (float64 t);
    void SetPushbackFractionHard (float64 t);

    void SettleToPushbackLock ();
    void ReleaseFromPushback ();

    void SetWindshieldAssetOpacity (float64 opaq);
    void SetWindshieldAssetOpacityHard (float64 opaq);

    // In the future, the following should be pushed down to lower levels
    // such as Acetate. At which point it can become a generic handipoint
    // hover management mechanism
    ObMap<Str, FlatThing *, NoMemMgmt, WeakRef> handi_hover_map;
    FlatThing *TopmostFlatThing (const Vect &from, const Vect &to);
    ObRetort MzHandiHoverDetection (MzHandiMoveEvent *e, Atmosphere *atm);

    ObRetort ClearScreen (Atmosphere *atm);

    ObRetort ClearParamus (Atmosphere *atm);

    VideoThing *NewVideoFromHobokenSlot (const int64 &slot);
    VideoThing *NewVideoFromViddleName (const Str &name);

    virtual bool PassUnclaimedEventsUpAcceptorAncestry () { return true; }

    virtual ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *atm);

    virtual ObRetort MzShoveBegin (MzShoveBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzShoveContinue (MzShoveContinueEvent *e, Atmosphere *atm);
    virtual ObRetort MzShoveEnd (MzShoveEndEvent *e, Atmosphere *atm);

    //  virtual ObRetort MzHandi (MzHandiEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                           Atmosphere *atm);

    virtual ObRetort MzTenderProbe (MzTenderProbeEvent *e, Atmosphere *atm);
    virtual ObRetort MzTenderSolidify (MzTenderSolidifyEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzTenderRescind (MzTenderRescindEvent *e, Atmosphere *atm);

    virtual ObRetort MzScrollProbe (MzScrollProbeEvent *e, Atmosphere *atm);
    virtual ObRetort MzScrollBegin (MzScrollBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzScrollContinue (MzScrollContinueEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzScrollEnd (MzScrollEndEvent *e, Atmosphere *atm);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm) {
        super::DrawSelf (vf, atm);
    }
};
}
} // bye bye namespaces sluice, oblong

#endif
