
/* (c)  oblong industries */

#ifndef MZ_TENDER_EVENTS_TO_LAST_A_LIFETIME
#define MZ_TENDER_EVENTS_TO_LAST_A_LIFETIME

#include <libImpetus/OEPointing.h>

#include <libGanglia/ElectricalEvent.h>

#include "Acetate.h"

namespace oblong {
namespace sluice {
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class MzTenderEvent : public ElectricalEvent {
    PATELLA_SUBCLASS (MzTenderEvent, ElectricalEvent);
    OE_MISCY_MAKER (MzTender, Electrical, "tender");

public:
    Vect curr_loc, prev_loc, estab_loc;
    Str intent;
    ObWeakRef<Acetate *> source_entity;
    ObWeakRef<KneeObject *> item;

public:
    MzTenderEvent (KneeObject *orig_ko = NULL) : ElectricalEvent (orig_ko) {}

    const Vect &EstabLocation () const { return estab_loc; }
    const Vect &CurrLocation () const { return curr_loc; }
    const Vect &PrevLocation () const { return prev_loc; }

    void SetEstabLocation (const Vect &el) { estab_loc = el; }
    void SetCurrLocation (const Vect &cl) { curr_loc = cl; }
    void SetPrevLocation (const Vect &pl) { prev_loc = pl; }

    const Str &Intent () const;
    void SetIntent (const Str &ent);

    Acetate *SourceEntity () const { return ~source_entity; }
    void SetSourceEntity (Acetate *ate) {
        if (ate)
            source_entity = ate;
        else
            source_entity.Nullify ();
    }

    KneeObject *Item () const { return ~item; }
    void SetItem (KneeObject *ko) {
        if (ko)
            item = ko;
        else
            item.Nullify ();
    }

    OEPointingEvent *RawOEPointingEvent () const;
    void SetRawOEPointingEvent (OEPointingEvent *pe);

    virtual void SynthesizeInternalProtein ();
    virtual ObRetort AnalyzeInternalProtein ();
};

class MzTenderProbeEvent : public MzTenderEvent {
    PATELLA_SUBCLASS (MzTenderProbeEvent, MzTenderEvent);
    OE_MISCY_MAKER (MzTenderProbe, MzTender, "probe");
    MzTenderProbeEvent (KneeObject *orig_ko = NULL) : MzTenderEvent (orig_ko) {}
};

class MzTenderSolidifyEvent : public MzTenderEvent {
    PATELLA_SUBCLASS (MzTenderSolidifyEvent, MzTenderEvent);
    OE_MISCY_MAKER (MzTenderSolidify, MzTender, "solidify");
    MzTenderSolidifyEvent (KneeObject *orig_ko = NULL)
        : MzTenderEvent (orig_ko) {}
};

class MzTenderConfirmEvent : public MzTenderEvent {
    PATELLA_SUBCLASS (MzTenderConfirmEvent, MzTenderEvent);
    OE_MISCY_MAKER (MzTenderConfirm, MzTender, "confirm");
    MzTenderConfirmEvent (KneeObject *orig_ko = NULL)
        : MzTenderEvent (orig_ko) {}
};

class MzTenderRescindEvent : public MzTenderEvent {
    PATELLA_SUBCLASS (MzTenderRescindEvent, MzTenderEvent);
    OE_MISCY_MAKER (MzTenderRescind, MzTender, "rescind");
    MzTenderRescindEvent (KneeObject *orig_ko = NULL)
        : MzTenderEvent (orig_ko) {}
};

class MzTenderEventAcceptorGroup
    : public MzTenderProbeEvent::MzTenderProbeAcceptor,
      public MzTenderSolidifyEvent::MzTenderSolidifyAcceptor,
      public MzTenderConfirmEvent::MzTenderConfirmAcceptor,
      public MzTenderRescindEvent::MzTenderRescindAcceptor {};

class MzTender {
public:
    typedef MzTenderEventAcceptorGroup Evts;
};
}
} // a rousing finale for namespaces sluice and oblong

DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED (oblong::sluice::MzTender::Evts);

#endif
