#ifndef ITS_GOING_TO_BE_A_BLANK_YEAR
#define ITS_GOING_TO_BE_A_BLANK_YEAR

#include <libNoodoo/FlatThing.h>
#include <libBasement/SineColor.h>
#include <libBasement/AsympFloat.h>
#include <libBasement/AsympColor.h>
#include <libNoodoo/GlyphoString.h>
#include <libLoam/c++/ObColor.h>
#include <libBasement/TWrangler.h>

#include "geo_utils.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;

class MessageBanner : public FlatThing {
    PATELLA_SUBCLASS (MessageBanner, FlatThing);

public:
    MessageBanner (Str msg, FlatThing *f);
    // TODO: Additional constructor where you can pass in a pointer to a sine
    // vect
    // this will allow all the loading fuoroscopes to synchronize their pulsing
    // backgrounds

    void Show ();
    void Hide ();
    void SetMessage (Str s);
    void SetShouldRepeatText (bool b);
    bool ShouldRepeatText () { return repeat_text; }
    void SetBackgroundLiveliness (bool b);
    bool BackgroundLiveliness () { return background_liveliness; }
    void MoveTo (Vect c, float w, float h);
    void MimicFluoroDimensions ();

    virtual ObRetort Travail (Atmosphere *atm);
    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

private:
    // short message that is repeated across the banner
    Str coreText, renderText;
    bool repeat_text, background_liveliness, too_small_for_banner,
        manual_dimens_getting;
    int64 textSize, bannerHeight;
    ObRef<TWrangler *> our_center;
    ObRef<SoftFloat *> our_width, our_height;
    ObRef<ObColor *> backColor, foreColor, textColor, borderColor;
    ObRef<SineColor *> pulse_color;
    ObRef<AsympColor *> transition_fader_color, pulse_color_amp;
    ObRef<GlyphoString *> textBanner;
    ObWeakRef<FlatThing *> myFluoro;
    static SineColor global_pulser;
    // GeoRect fBounds;

    void RepeatText ();

    void DrawPlane (
        float32 x1, float32 y1, float32 z1, float32 x2, float32 y2, float32 z2);
    void DrawPlane (const Vect &tl, const Vect &br);
};
}
} // namespace oblong fades into the static

#endif // ITS_GOING_TO_BE_A_BLANK_YEAR
