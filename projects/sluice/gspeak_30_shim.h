

#ifndef GSPEAK_30_SHIM
#define GSPEAK_30_SHIM

#define TEXGL_INCLUDE <libStaging/libNoodoo/TexGL.h>
#define FTT_INCLUDE <libStaging/libLoam/c++/FatherTimeTracker.h>

#include <libLoam/c++/ObResolvedPath.h>

#define SPLEND_VAL(x) ((x).IsSplend ())
#define FOUND_VAL(x) (OB_OK == (x))
#define FIND_FILE(path, file) ObResolvedPath::ForFile ((path), (file))
#define FIND_FILE_QUIETLY(path, file)                                          \
    ObResolvedPath::ForFile ((path), (file), "FR",                             \
                             ObResolvedPath::Verbosity_Taciturn)
#define FIND_FOLDER(path, file) ObResolvedPath::ForFile ((path), (file), "DW")
#define MIP_LOAD_POLICY MipOnLoad
#define NONE_POLICY NoMipmap
#define POT_POLICY POT

#endif
