
#pragma once

#include <libLoam/c++/ObRef.h>
#include <libLoam/c++/FatherTime.h>
#include <libPlasma/c++/Slaw.h>
#include <libBasement/ob-hook-troves.h>

#include "Acetate.h"
#include "MzHandi.h"
#include "NodeStore.h"

#include <set>
#include <map>

namespace oblong {
namespace sluice {

class NodeyScope;
class SimpleTexRect;

class Hovercraft : public Acetate, public MzHandi::Evts {
    PATELLA_SUBCLASS (Hovercraft, Acetate);

protected:
    NodeyScope *parent_scope;
    oblong::loam::ObRef<NodeStore *> &node_store;
    std::set<Str> &active_kinds;

    // indexed by kind
    std::map<Str, Str> display_name;
    // indexed by kind
    std::map<Str, std::map<Str, Str>> label_rename;
    // indexed by kind
    std::map<Str, std::set<Str>> label_skip;

    float64 minimum_zoom;
    int64 too_crowded;

    float64 info_wait;
    FatherTime info_timer;

    ObTrove<Str> prov_map;
    Vect handi_loc;

    oblong::plasma::Slaw config_slaw;
    SimpleTexRect *artsy;

    GeoRect search_bounds;

    Node *current_closest;
    Node *ClosestNode (const Vect loc);
    LatLon last_cursor;
    bool WhacksAppropriately (MzHandiEvent *e);

    Str MakeLabelFor (Node *n);

    void Dismiss (oblong::ganglia::ElectricalEvent *ee, Atmosphere *atmo);

    FLAG_MACHINERY (ClosestChanged);
    FLAG_MACHINERY (Hardened);
    FLAG_MACHINERY (DrawBoundingBox);
    FLAG_MACHINERY (CurrentlyTooCrowded);
    FLAG_MACHINERY (CurrentlyTooHigh);
    FLAG_MACHINERY (ShouldGoAway);

    // provenance -> text
    DOUBLE_ARG_HOOK_MACHINERY (ShowInfo, const Str, const Str);
    // provenance
    SINGLE_ARG_HOOK_MACHINERY (HideInfo, const Str);

public:
    Hovercraft (NodeyScope *scope,
                oblong::loam::ObRef<NodeStore *> &store,
                std::set<oblong::loam::Str> &kinds);
    virtual ~Hovercraft ();

    void SetConfigSlaw (const oblong::plasma::Slaw &s);
    void DismissAllStrings ();
    void NodeUpdated (Node *n);

    // raw handipoint evt
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                           Atmosphere *atm);

    virtual ObRetort Inhale (Atmosphere *atmo);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atmo);
};
}
}
