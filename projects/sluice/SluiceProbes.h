
/* (c)  Oblong Industries */

#ifndef PROBING_THE_SLUICE_WAY
#define PROBING_THE_SLUICE_WAY

#include <libBasement/PaceThing.h>
#include <libBasement/UrDrome.h>

#include <libOuija/Probe.h>

namespace oblong {
namespace sluice {
using namespace oblong::ouija;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
  AOTeachingProbe:
    This probe injects new values into the anatomy of the poi.  It uses a Tap
    on the poi to set the objects member variables based on an anatomy slaw.

    FillProbeFromSlaw requires the anatomy map to include the key/value pair of
      Slaw::Map ("anatomy", Slaw::Map (poi's new anatomy))
        NOTE: see Taps for anatomy format

    Invoke (KO): Creates a Tap for the KneeObject and calls FillFromAnatomy to
      set the values as set forth in the ao_anatomy slaw. Upon completion,
      Invoke uses the Tap to slurp to return the new values from the KneeObject
*/
class SluiceTeachingProbe : public AOTeachingProbe {
    PATELLA_SUBCLASS (SluiceTeachingProbe, AOTeachingProbe);

public:
    SluiceTeachingProbe () : AOTeachingProbe () {}
    SluiceTeachingProbe (KneeObject *ko) : AOTeachingProbe (ko) {}
    PROBE_CONSTRUCTOR (SluiceTeachingProbe);

    virtual Slaw InvokeAt (KneeObject *ko);
};

/**
  AOLearningProbe:
    This probe extracts the anatomy of the Probe's poi.  It can either extract
    the object's entire anatomy or it can extract only specified anatomy keys.

    AOLearningProbes contain a trove of key Strings which identify the key
      values a tap should retrieve information for.  It is not necessary to
      fill this trove but the probe will then not return any information on the
      poi unless the FullProbe FLAG_MACHINERY is set to true, in which case the
      probe will extract information on ALL available keys for the poi class
      type.

    FillProbeFromSlaw: should contain at least one entry for setting FullProbe
      and optionally an entry with a list of keys to probe over:
        Slaw::Map ("full-probe", [true/false],
                   "keys", Slaw::List (Str1, ...))

    Invoke (KO): Generates a Tap for the given KneeObject and calls either
      SlurpFullAnatomy or SlurpAnatomyFor based on the FullProbe Flag
*/
class SluiceLearningProbe : public AOLearningProbe {
    PATELLA_SUBCLASS (SluiceLearningProbe, AOLearningProbe);

protected:
    virtual Slaw DescendWindshield (KneeObject *ko);
    virtual Slaw DescendDossier (KneeObject *ko);
    virtual Slaw DescendScreen (KneeObject *ko);
    virtual Slaw DescendScopes (KneeObject *ko);

public:
    SluiceLearningProbe () : AOLearningProbe () {}
    SluiceLearningProbe (KneeObject *ko) : AOLearningProbe (ko) {}
    PROBE_CONSTRUCTOR (SluiceLearningProbe);

    virtual Slaw InvokeAt (KneeObject *ko);
};
}
}

#endif
