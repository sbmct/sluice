#pragma once

#include <map>
#include <set>

#include "SluiceSettings.h"

namespace oblong {
namespace sluice {

/**
 * Store a time series of a single type. Allow for expiration.
 */
template <typename T, typename TimeMarker> class TimeSeries {
public:
    typedef std::greater<TimeMarker> cmp_t;
    typedef std::pair<bool, T> stored_t;
    typedef std::map<TimeMarker, stored_t, cmp_t> map_t;

protected:
    map_t store;

    const bool IsExpired (const stored_t &s) const { return !s.first; }
    const T &Value (const stored_t &s) const { return s.second; }
    T &Value (stored_t &s) { return s.second; }

public:
    /** Does the series have a value at or after this time? */
    const bool Has (const TimeMarker t) const {
        typename map_t::const_iterator found = store.lower_bound (t);
        return store.end () != found && !IsExpired (found->second);
    }
    const TimeMarker Last () const { return store.begin ()->first; }
    const TimeMarker StampFor (const TimeMarker t) const {
        typename map_t::const_iterator found = store.lower_bound (t);
        return store.end () == found ? TimeMarker (0) : found->first;
    }
    /** Retrieve the value set most recently before or equal to time t */
    const bool Get (const TimeMarker t, T &out) const {
        typename map_t::const_iterator found = store.lower_bound (t);
        if (store.end () != found && !IsExpired (found->second)) {
            out = Value (found->second);
            return true;
        }
        return false;
    }
    void GetTimes (std::set<TimeMarker> &times) const {
        for (auto &&pair : store) {
            times.insert (pair.first);
        }
    }
    /** A more dangerous way to get.
     *  Make sure that you've checked Has first
     */
    const T &Get (const TimeMarker t) const {
        typename map_t::const_iterator found = store.lower_bound (t);
        return Value (found->second);
    }
    /** Set the value at time t */
    void Set (const TimeMarker t, const T &val) {
        if ((SluiceSettings::SharedSettings ()->MaxNodeHistory () > 0) &&
            (store.size () >
             SluiceSettings::SharedSettings ()->MaxNodeHistory ())) {
            store.erase (--store.end ());
        }
        store[t] = std::make_pair (true, val);
    }
    /** Signify that the series ends (at least for now) at time t
     *
     * Note that there can be more observations set *after* time t,
     * but between T_n and T_(n+1) there is no observation.
     */
    void Expire (const TimeMarker t) {
        store[t] = std::make_pair (false, T ());
    }

    /** Does exactly this time exist? */
    const bool HasExactly (const TimeMarker t) {
        return store.end () != store.find (t);
    }
    /** Get exactly this value. (Dangerous) */
    const T &Exactly (const TimeMarker t) const { return Value (store[t]); }
    /** Get exactly this value. (Dangerous) */
    T &Exactly (const TimeMarker t) { return Value (store[t]); }
};
}
}
