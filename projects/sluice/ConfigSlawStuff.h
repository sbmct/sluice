/* Copyright (C) 2011 Oblong Industries */

#ifndef OBLONG_SLUICE_CONFIGSLAWSTUFF_H
#define OBLONG_SLUICE_CONFIGSLAWSTUFF_H

#include <libLoam/c++/ObTrove.h>
#include <libBasement/ob-logging-macros.h>
#include <libPlasma/c++/Slaw.h>

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::plasma;

inline std::vector<Str> ListAttributes (const Slaw config) {
    std::vector<Str> out;
    Slaw attrs = config.MapFind ("attributes");
    for (int64 i = 0; i < attrs.Count (); ++i) {
        Slaw s = attrs.Nth (i);
        Str name;
        if (s.MapFind ("name").Into (name)) {
            out.push_back (name);
        }
    }
    return out;
}

inline Slaw FindAttribute (const Slaw s, const Str name) {
    Slaw attrs, attr;
    Str n;
    attrs = s.MapFind ("attributes");
    for (int64 ai = 0; ai < attrs.Count (); ++ai) {
        attr = attrs.Nth (ai);
        if (attr.MapFind ("name").Into (n) && name == n)
            return attr;
    }
    return Slaw ();
}

template <typename T>
void ReadAttributeValues (const Slaw attr, ObTrove<T> &target) {
    Str typ;
    bool selected;
    T tmp;
    if (attr.MapFind ("selection-type").Into (typ)) {
        bool exclusive = "exclusive" == typ;
        Slaw contents = attr.MapFind ("contents");
        for (int64 i = 0; i < contents.Count (); ++i) {
            Slaw elt = contents.Nth (i);
            if (elt.MapFind ("name").Into (tmp) &&
                elt.MapFind ("selected").Into (selected)) {
                if (selected) {
                    target.Append (tmp);
                    if (exclusive) {
                        break;
                    }
                }
            }
        }
    }
}

template <typename T>
inline bool ReadAttribute (const Slaw s, const Str name, ObTrove<T> &target) {
    Slaw attrs, attr;
    Str n, typ;
    attrs = s.MapFind ("attributes");
    for (int64 ai = 0; ai < attrs.Count (); ++ai) {
        attr = attrs.Nth (ai);
        if (attr.MapFind ("selection-type").Into (typ) &&
            attr.MapFind ("name").Into (n) && name == n) {
            ReadAttributeValues (attr, target);
            return true;
        }
    }
    return false;
}

template <typename T>
inline bool ReadAttribute (const Slaw s, const Str name, T &target) {
    ObTrove<T> tmp;
    if (ReadAttribute (s, name, tmp)) {
        target = tmp.Nth (0);
        return true;
    }
    return false;
}

inline Str Join (ObTrove<Str> &trove, const Str sep) {
    Str ret;
    ObCrawl<Str> cr = trove.Crawl ();
    bool first = true;
    while (!cr.isempty ()) {
        Str x = cr.popfore ();
        if (first)
            first = false;
        else
            ret.Append (sep);
        ret.Append (x);
    }
    return ret;
}
}
}
#endif // OBLONG_SLUICE_CONFIGSLAWSTUFF_H
