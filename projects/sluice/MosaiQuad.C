
/* (c)  Oblong Industries */

#include "MosaiQuad.h"
#include <libBasement/QuadraticFloat.h>

#include <libMedia/JpegImageClot.h>
#include <libMedia/PngImageClot.h>

using namespace oblong::sluice;

#include <list>

#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>

#include "MosaiQuad.h"
#include "profile.h"

//#define DEBUG_COLOURS yes

#ifndef DEBUG_COLOURS
#define HigherColor(c) c
#define LowerColor(c) c
#define HighresColor(c) c
#define UnitileColor(c) c
#define BigtileColor(c) c
#else
inline ObColor HigherColor (ObColor c) {
    ObColor out (c);
    // out . r = 0.0;
    return out;
}
inline ObColor LowerColor (ObColor c) {
    ObColor out (c);
    // out . b = 0.0;
    return out;
}
inline ObColor HighresColor (ObColor c) {
    ObColor out (c);
    // out . g = 0.0;
    return out;
}
inline ObColor UnitileColor (ObColor c) {
    ObColor out (c);
    out.b *= 0.75;
    out.g *= 0.75;
    return out;
}
inline ObColor BigtileColor (ObColor c) {
    ObColor out (c);
    out.b *= 0.75;
    out.r *= 0.75;
    return out;
}
#endif

const float64 MosaiQuad::REQUEST_TIMEOUT = 2.5;

static Str MakeRand () {
    Str ret;
    int32 r = rand ();
    int32 r2 = rand ();
    ret.Sprintf ("%04x%04x", r, r2);
    return ret;
}

static bool InitializedPoolNameTicker = false;
Str MosaiQuad::MakeRandomPoolStr () {
    if (!InitializedPoolNameTicker) {
        srand (0);
        InitializedPoolNameTicker = true;
    }
    return MakeRand ();
}

// static const int DEFAULT_OPACITY = 0;
// static const int MAXIMUM_OPACITY = 255;

void MosaiQuad::ResetOpacityConditionally (const TileID tid) {
    std::map<TileID, float64>::iterator iter = tile_opacity.find (tid);
    if (iter == tile_opacity.end ())
        tile_opacity[tid] = FatherTime::AbsoluteTime ();
}

void MosaiQuad::SetTileOpacity (const TileID tid, const float64 x) {
    tile_opacity[tid] = x;
}

const float64 MosaiQuad::TileOpacity (const TileID tid) {
    std::map<TileID, float64>::iterator iter = tile_opacity.find (tid);
    if (iter == tile_opacity.end ()) {
        iter = tile_opacity.insert (tile_opacity.end (),
                                    std::map<TileID, float64>::value_type (
                                        tid, FatherTime::AbsoluteTime ()));
    }
    return iter->second;
}

const bool MosaiQuad::IsOpaque (const TileID &tid) const {
    std::map<TileID, float64>::const_iterator iter = tile_opacity.find (tid);
    if (iter == tile_opacity.end ())
        return false;
    else {
        const float64 d = FatherTime::AbsoluteTime () - iter->second;
        return d * alpha_per_second >= 1.0;
    }
}

void MosaiQuad::SetOpacityForLevelToDefault (int level) {
    SetOpacityForLevel (level, FatherTime::AbsoluteTime ());
}

void MosaiQuad::SetOpacityForLevelToMaximum (int level) {
    SetOpacityForLevel (level, 0.0);
}

void MosaiQuad::SetOpacityForLevel (int level, const float64 new_opacity) {
    typedef std::map<TileID, float64>::value_type pair_t;
    BOOST_FOREACH (pair_t &pair, tile_opacity)
        if (pair.first.Z () == level)
            pair.second = new_opacity;
}

MosaiQuad::MosaiQuad (int tileSize,
                      int factor_,
                      bool x_wrap,
                      Str remoteTileReqPool_,
                      Str remoteTileRespPool_,
                      double startingX,
                      double startingY,
                      double startingScale,
                      Str startingMapID,
                      double cacheLayers_)
    : TexQuad (), tile_render_index (0), alpha_per_second (2.0) {
    SetDrawBoxes (false);
    SetShouldInhale (true);
    current_pod.ready = false;
    cacheLayers = cacheLayers_;
    hack_cur_layer = -1;
    t = -1;
    use_alpha = false;
    show_boxen = false;
    roi_x = 0.5;
    roi_y = 0.5;

    mapID = startingMapID;
    unitile = NULL;
    unitileMap = "";

    remoteTileReqPool = remoteTileReqPool_;
    remoteTileRespPool = remoteTileRespPool_ + "-" + MakeRandomPoolStr ();

    ObRetort ret;
    remoteReq = Pool::Participate (remoteTileReqPool.utf8 (), &ret);
    if (!ret.IsSplend ()) {
        OB_LOG_DEBUG ("couldn't connect to pool '%s'\n",
                      remoteTileReqPool.utf8 ());
        exit (1);
    }
    // printf ("OB_POOLS_DIR = %s\n", getenv("OB_POOLS_DIR"));
    Pool::Configuration config;
    config.cpolicy = Pool::Create_Auto_Disposable;
    config.size = Pool::SIZE_SMALL;
    remoteResp2 = Pool::Participate (remoteTileRespPool.utf8 (), config, &ret);
    if (!ret.IsSplend ()) {
        OB_LOG_DEBUG ("couldn't connect to pool '%s'\n",
                      remoteTileRespPool.utf8 ());
        exit (1);
    }

    AppendChild (cache = new TileCache (tileSize, remoteResp2, remoteReq));
    cache->SetCenter (TileID (mapID, 0, 0, 0));
    cache->Start ();

    cache->AppendTileArrivedHook (&MosaiQuad::TileArrived, this, "tile-arr");
    cache->AppendTileExpiredHook (&MosaiQuad::TileExpired, this, "tile-exp");

    resolution = 1.0;
    maxtiles = 0;
    tile_size = tileSize;
    xwrap = x_wrap;
    factor = factor_;

    InstallSquetch (new AsympFloat (1.0));
    InstallCenterX (new LinearFloat (startingX));
    InstallCenterY (new LinearFloat (startingY));
    InstallScaledenom (new LinearFloat (startingScale));
    SetTilesUpdated (true);
}

void MosaiQuad::Shutdown () {
    if (cache) {
        cache->Shutdown ();
        cache = NULL;
    }
}

ObRetort MosaiQuad::TileArrived (const TileID &tid) {
    SetTilesUpdated (true);
    return OB_OK;
}
ObRetort MosaiQuad::TileExpired (const TileID &tid) {
    SetTilesUpdated (true);
    tile_opacity.erase (tid);
    return OB_OK;
}

void MosaiQuad::SetResolution (double newResolution) {
    resolution = newResolution;
}
void MosaiQuad::SetShowDebugBoxen (bool showBoxen) { show_boxen = showBoxen; }
void MosaiQuad::SetScale (double newScale) {
    ScaledenomSoft ()->Set (std::max (newScale, 1.0));
}
void MosaiQuad::MoveTo (double x, double y) {
    // printf ("x y --> %f %f\n", x, y);
    CenterXSoft ()->Set (x); //*FULL_SIZE);
    CenterYSoft ()->Set (y); //*FULL_SIZE);
}

void MosaiQuad::SetScaleHard (double newScale) {
    ScaledenomSoft ()->SetHard (std::max (newScale, 1.0));
}

void MosaiQuad::MoveToHard (double x,
                            double y) { // printf ("x y --> %f %f\n", x, y);
    CenterXSoft ()->SetHard (x);        //*FULL_SIZE);
    CenterYSoft ()->SetHard (y);        //*FULL_SIZE);
}

void MosaiQuad::SetMap (Str newMapID) {
    if (newMapID != mapID) {
        OB_LOG_INFO ("MosaiQuad: map-id changed to %s", newMapID.utf8 ());
        tileFreshened.clear ();
        // Need a "set center" that just replaces the map.canlmwsfu
        cache->SetMap (newMapID);
        unitile = NULL;
        unitileMap = mapID = newMapID;
    }
}

void MosaiQuad::RecalculateMaxTiles () {
    double wTileAspect = 1.0;
    double hTileAspect = 1.0;
    // float w = this -> Width ();
    // float h = this -> Height ();
    double tileAspect = Squetch (); // * h / w;
    if (tileAspect < 1.0)
        hTileAspect = 1.0 / tileAspect;
    else
        wTileAspect = tileAspect;

    // double otherW = 1.0 * this -> Width () / tile_size;
    // double otherH = 1.0 * this -> Height () / tile_size;

    // 2 + ... in case numtiles = one or two, in one dimension or the other
    int maxcoverx = (int)(3.0f + resolution / wTileAspect);
    int maxcovery = (int)(3.0f + resolution / hTileAspect);
    int new_maxtiles = maxcoverx * maxcovery * cacheLayers;
    if (new_maxtiles > this->maxtiles) {
        this->maxtiles = new_maxtiles;
        // printf ("quad size = %f x %f\n", w, h);
        OB_LOG_DEBUG ("cover with %d x %d tiles x %f layers = %d maxtiles\n",
                      maxcoverx, maxcovery, cacheLayers, maxtiles);
        cache->SetMaximumTexGLCacheSize (this->maxtiles);
    }
}

void MosaiQuad::SetCacheLayers (double cacheLayers_) {
    cacheLayers = cacheLayers_;
}

TileID MosaiQuad::FindFirstBigtile (
    const int tx0,
    const int ty0,
    const int z,
    const int coverx,
    const int covery,
    const int morex0,
    const int levelTilesI) { // take into account x_wrap
    int ntx0 = tx0;
    int ntx1 = tx0 + coverx - 1;
    if (xwrap) {
        // printf ("before: ntx0, ntx1 = %d %d\n", ntx0, ntx1);
        ntx0 = ((-morex0 + ntx0) + (levelTilesI * 1000)) % levelTilesI;
        ntx1 = ((tx0 + coverx - 1) + (levelTilesI * 1000)) % levelTilesI;
        // printf ("after:  ntx0, ntx1 = %d %d\n", ntx0, ntx1);
    }

    TileID bl (mapID, z, ty0, ntx0);
    TileID tr (mapID, z, ty0 + covery - 1, ntx1);

    while (0 < bl.Z () && (bl != tr)) {
        bl = bl.Up ();
        tr = tr.Up ();
    }

    return bl;
}

struct GLState {
    GLenum cap;
    bool previous, reset;

    GLState (GLenum cap_, bool state) : cap{cap_} {
        previous = glIsEnabled (cap);
        if (previous == state) {
            reset = false;
        } else {
            reset = true;
            if (state) {
                glEnable (cap);
            } else {
                glDisable (cap);
            }
        }
    }

    ~GLState () {
        if (reset) {
            if (previous) {
                glEnable (cap);
            } else {
                glDisable (cap);
            }
        }
    }
};

void MosaiQuad::DrawSelf (VisiFeld *v, Atmosphere *a) {
    glLineWidth (2.0);
    const float64 now = FatherTime::AbsoluteTime ();

    bool blending = glIsEnabled (GL_BLEND);
    if (!blending) {
        glEnable (GL_BLEND);
    }
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    TexGL *texgl = NULL;

    GLState poly_smooth (GL_POLYGON_SMOOTH, false);

    for (int j = 0; j < tile_render_index; ++j) {
        const RenderTile &tile = tiles_to_render[j];
        ObColor c (tile.color);
        const float64 alpha =
            std::min (1.0, (now - tile.alpha_start) * alpha_per_second);
        OB_glColor (c.SetAlpha (alpha) * AdjColor ());
        glEnable (GL_TEXTURE_2D);
        if (tile.tex != texgl) {
            texgl = tile.tex;
            texgl->BindTexture ();
        }
        glBegin (GL_QUADS);
        for (int i = 0; i < 4; ++i) {
            glTexCoord2f (tile.texcoors[i].x, tile.texcoors[i].y);
            OB_glVertex (tile.verts[i]);
        }
        glEnd ();
        glDisable (GL_TEXTURE_2D);
    }

    if (!blending) {
        glDisable (GL_BLEND);
    }
}

template <typename T> inline bool SetIfDifferent (T &target, const T &source) {
    if (target == source)
        return true;
    target = source;
    return false;
}

void MosaiQuad::SetupDrawPod () {
    RecalculateMaxTiles ();
    if (maxtiles == 0)
        return;

    // SPECIAL CASE, we need the low res tile! ASAP!
    TexGL *uuu = cache->TileFor (TileID (mapID, 0, 0, 0));
    if (NULL == uuu) {
        cache->AppendTileRequest (TileID (mapID, 0, 0, 0));
        return;
    } else {
        unitileMap = mapID;
        unitile = uuu;
    }

    const bool crossfading = !(AdjColor ().a < 1.0 || use_alpha);

    const float w = this->Width ();
    const float h = this->Height ();

    const double tileAspect = Squetch (); // * h / w;
    const double wTileAspect = (tileAspect < 1.0) ? 1.0 : tileAspect;
    const double hTileAspect = (tileAspect < 1.0) ? (1.0 / tileAspect) : 1.0;

    const double otherW = 2.0 * w / tile_size;
    const double otherH = 2.0 * h / tile_size;

    const float scaledenom = Scaledenom () * 2.0;

    const int level =
        (int)floor (std::max (0.0, log (scaledenom) / log (factor)));

    const int z = level;
    // reset opacity on tiles when layer changes, to force crossfade
    // this will make things much prettier
    Zooming zooming = NOT_ZOOMING;
    if (crossfading && hack_cur_layer != level) {
        zooming = hack_cur_layer > level ? ZOOMING_OUT : ZOOMING_IN;
        SetOpacityForLevelToMaximum (hack_cur_layer);
        if (ZOOMING_IN == zooming)
            SetOpacityForLevelToDefault (level);
    }
    // store that new level
    hack_cur_layer = level;

    const double scaledenomF = scaledenom;
    const double levelTiles = pow (factor, level);
    const int levelTilesI = std::max ((int)levelTiles, 1);
    const float cursize =
        tile_size * scaledenomF / (levelTiles * factor * resolution *
                                   2.0); //(float)(fullScaled / levelTiles);
    const float cursizeW = cursize * wTileAspect * otherW;
    const float cursizeH = cursize * hTileAspect * otherH;

    const double centerx = CenterX ();
    const double centery = 1.0 - CenterY ();

    const double fullSizeW = cursizeW * levelTiles;
    const double fullSizeH = cursizeH * levelTiles;

    const double x0 = 0.5 * w - centerx * fullSizeW;
    const double y0 = 0.5 * h - centery * fullSizeH;

    const double phasex = (x0 >= 0.0) ? x0 : -fmod (-x0, cursizeW);
    const double phasey = (y0 >= 0.0) ? y0 : -fmod (-y0, cursizeH);

    const int coverx = (w - phasex) / cursizeW;
    const int covery = (h - phasey) / cursizeH;

    // while (coverx*cursizeW+phasex < w) coverx++;
    // while (covery*cursizeH+phasey < h) covery++;
    // printf ("cover with %d x %d tiles\n", coverx, covery);

    const int tx0 = (x0 < 0) ? int(floor (-x0 / cursizeW)) : 0;
    const int ty0 = (y0 < 0) ? int(floor (-y0 / cursizeH)) : 0;

    const int morex0 = (!xwrap) ? 0 : (coverx + phasex / cursizeW);

    TileID original_big_tile =
        FindFirstBigtile (tx0, ty0, z, coverx, covery, morex0, levelTilesI);

    // SetBigTileStack will tell us which big tile we actually *have*
    // in the cache. This is why we can't just use the originally
    // computed big tile value.
    TileID bigtile = cache->SetBigTileStack (original_big_tile);

    const int64 center_x = tx0 + (coverx / 2);
    const int64 center_y = ty0 + (covery / 2);

    cache->SetCenter (center_x, center_y, z);

    bool pod_same = true;
    pod_same &= SetIfDifferent (current_pod.phasex, phasex);
    pod_same &= SetIfDifferent (current_pod.phasey, phasey);
    pod_same &= SetIfDifferent (current_pod.levelTilesI, levelTilesI);
    pod_same &= SetIfDifferent (current_pod.cursizeW, cursizeW);
    pod_same &= SetIfDifferent (current_pod.cursizeH, cursizeH);
    pod_same &= SetIfDifferent (current_pod.color, ObColor (1.0, 1.0));
    pod_same &= SetIfDifferent (current_pod.crossfading, crossfading);
    pod_same &= SetIfDifferent (current_pod.best_bigtile_z, bigtile.Z ());
    pod_same &=
        SetIfDifferent (current_pod.best_bigtile_tgl, cache->TileFor (bigtile));
    pod_same &= SetIfDifferent (current_pod.tx0, tx0);
    pod_same &= SetIfDifferent (current_pod.ty0, ty0);
    pod_same &= SetIfDifferent (current_pod.coverx, coverx);
    pod_same &= SetIfDifferent (current_pod.covery, covery);
    pod_same &= SetIfDifferent (current_pod.morex0, morex0);
    pod_same &= SetIfDifferent (current_pod.z, z);
    pod_same &= SetIfDifferent (current_pod.zooming, zooming);
    current_pod.ready = true;

    bool draw = QueryTilesUpdated () || !pod_same;

    if (draw) {
        tile_render_index = 0;
        const int FRINGE = 1;

        for (int y = 0 - FRINGE; y < current_pod.covery + FRINGE; y++)
            for (int x = -(current_pod.morex0) - FRINGE;
                 x < (current_pod.coverx + FRINGE); x++)
                DrawInner (x, y);
        SetTilesUpdated (false);
    }
}

ObRetort MosaiQuad::Inhale (Atmosphere *atm) {
    current_pod.ready = false;
    SetupDrawPod ();
    return super::Inhale (atm);
}

bool MosaiQuad::HasHigherRes (const TileID &tid) const {
    TileID a, b, c, d;
    tid.Down (a, b, c, d);
    return cache->Has (a) && cache->Has (b) && cache->Has (c) && cache->Has (d);
}

bool MosaiQuad::HasLowerRes (const TileID &tid) const {
    return cache->Has (tid.Up ());
}

void MosaiQuad::DrawInner (int x, int y) {
    const DrawPod &pod = current_pod;
    const int z = pod.z;

    int tx = x + pod.tx0;
    int old_tx = tx;
    if (xwrap)
        tx = ((x + pod.tx0) + (pod.levelTilesI * 1000)) % pod.levelTilesI;
    bool drawn_more_than_once = tx != old_tx;
    int ty = y + pod.ty0;

    if (tx < 0 || ty < 0 || tx >= pod.levelTilesI || ty >= pod.levelTilesI)
        return;

    bool useBG = true;
    bool useLower = pod.crossfading;
    bool use1x1 = pod.crossfading;
    bool useHigher = pod.crossfading;
    TileID me (mapID, z, ty, tx);
    if (!pod.crossfading) {
        use1x1 = cache->Has (me);
        if (use1x1 && IsOpaque (me))
            useLower = false;
        else {
            useLower = HasLowerRes (me);
            cache->AppendTileRequest (me);
        }

        if (useHigher &&
            useLower) // prefer one over the other for crossfading purposes
        {             // prefer blown up low res tiles for prettier cross fading
            if (pod.zooming == ZOOMING_IN) {
                useHigher = false;
            }
            // prefer shrunken high res tiles for prettier cross fading
            if (pod.zooming == ZOOMING_OUT) {
                useLower = false;
            }
        }

        if (use1x1) {
            useBG = false;
            useLower = false;
            useHigher = false;
        } else if (useHigher) {
            useBG = false;
            useLower = false;
            use1x1 = false;
        } else if (useLower) {
            useBG = false;
            useHigher = false;
            use1x1 = false;
        }
    } else {
        if (useHigher &&
            useLower) // prefer one over the other for crossfading purposes
        {             // prefer blown up low res tiles for prettier cross fading
            if (pod.zooming == ZOOMING_IN) {
                useHigher = false;
            }
            // prefer shrunken high res tiles for prettier cross fading
            if (pod.zooming == ZOOMING_OUT) {
                useLower = false;
            }
        }
    }

    DrawPass (x, y, z, tx, ty, drawn_more_than_once, useBG, use1x1, useLower,
              useHigher);
}

void MosaiQuad::DrawPass (const int x,
                          const int y,
                          const int z,
                          const int tx,
                          const int ty,
                          const bool drawn_more_than_once,
                          bool &useBG,
                          bool &use1x1,
                          bool &useLower,
                          bool &useHigher) { // Str kfull = Str::Format
                                             // ("%s_%d_%d_%d", mapID.utf8(), z,
                                             // ty, tx);
    TileID kfull (mapID, z, ty, tx);
    int totalSubTileAttempts = 2; // LOD; // go up to LOD levels up (lower z
                                  // values) in search of lower res tiles

    if (!cache->Has (kfull)) {
        cache->AppendTileRequest (kfull);
    } else {
        if (IsOpaque (kfull))
            totalSubTileAttempts = 0;
    }

    // draw 0 0 0 uniTile
    if (useBG)
        DrawInnerUnitile (x, y, z, tx, ty, useBG);

    // if ((tx+ty) % 2 == 0) continue; // TODO this is a hack!

    // bool smallerFound = false;
    // ----------------------
    // search for a lower res tile (if full requested tile is not (a) loaded
    // onto GPU and (b) fully opaque)
    // and draw it at full opacity if found
    if (useLower)
        DrawUseLower (x, y, z, tx, ty, totalSubTileAttempts, useLower, useBG);
    // --------------------- draw shrunken 'higher level' (z) tiles if
    // available, starting far away and zooming out
    // if (! smallerFound) // prefer upsampling lower-res ones to shrinking
    // high-res ones...
    // note: this is weird -- not sure if this makes it worse or better visually
    // ^ -- jupdike
    else if (useHigher)
        DrawUseHigher (x, y, z, tx, ty, useHigher, useBG);

    DrawHighres (x, y, kfull, drawn_more_than_once, use1x1);
}

void MosaiQuad::DrawInnerUnitile (const int x,
                                  const int y,
                                  const int z,
                                  const int tx,
                                  const int ty,
                                  bool &useBG) {
    const DrawPod &pod = current_pod;
    double subx0 = 0, suby0 = 0, subx1 = 1, suby1 = 1;
    int subx = tx, suby = ty, subz = z;
    TileID k = TileID (mapID, subz, suby, subx);
    // k.Sprintf("%s_%d_%d_%d", mapID.utf8(), subz, suby, subx);

    // just walk up the pyramid to calculate the subsection of the uniTile we
    // need to draw
    while (true) {
        int xm2 = subx % 2;
        int ym2 = suby % 2;
        double nx0, ny0, nx1, ny1;
        if (xm2) {
            nx0 = 0.5;
            nx1 = 1.0;
        } else {
            nx0 = 0.0;
            nx1 = 0.5;
        }

        if (ym2) {
            ny0 = 0.0;
            ny1 = 0.5;
        } else {
            ny0 = 0.5;
            ny1 = 1.0;
        } // fixes issue with swapped rows
        double ox0 = subx0, oy0 = suby0;
        double ox1 = subx1, oy1 = suby1;
        double dy = ny1 - ny0;
        double dx = nx1 - nx0;
        subx0 = nx0 + ox0 * dx;
        subx1 = nx0 + ox1 * dx;
        suby0 = ny0 + oy0 * dy;
        suby1 = ny0 + oy1 * dy;
        subz--;
        if (subz < 0)
            break; // probably not hit
        suby /= 2;
        subx /= 2;
        if (subz > 0) { // check for best BigTile
            if (subz != pod.best_bigtile_z || !pod.best_bigtile_tgl) {
                k = k.Up ();
                continue;
            }
            // ok, this is it!

            ResetOpacityConditionally (k);
            DrawAbsoluteRect (
                k, subx0, suby0, subx1, suby1, pod.phasex + (x)*pod.cursizeW,
                pod.phasey + y * pod.cursizeH,
                pod.phasex + (x + 1) * pod.cursizeW,
                pod.phasey + (y + 1) * pod.cursizeH,
                BigtileColor (pod.color), // debug red
                pod.best_bigtile_tgl, pod.cursizeW, pod.cursizeH, 0.0);
        }
        // normal 0 0 0 case!
        if (subz == 0) {
            ResetOpacityConditionally (k);
            DrawAbsoluteRect (k, subx0, suby0, subx1, suby1,
                              pod.phasex + (x)*pod.cursizeW,
                              pod.phasey + y * pod.cursizeH,
                              pod.phasex + (x + 1) * pod.cursizeW,
                              pod.phasey + (y + 1) * pod.cursizeH,
                              UnitileColor (pod.color), // debug red
                              ~unitile, pod.cursizeW, pod.cursizeH, 0.0);
        }
        break;
    }
}

void MosaiQuad::DrawUseLower (const int x,
                              const int y,
                              const int z,
                              const int tx,
                              const int ty,
                              const int totalSubTileAttempts,
                              bool &useLower,
                              bool &useBG) {
    const DrawPod &pod = current_pod;
    double subx0 = 0, suby0 = 0, subx1 = 1, suby1 = 1;
    int subx = tx, suby = ty, subz = z;
    TileID k = TileID (mapID, subz, suby, subx);
    // k.Sprintf("%s_%d_%d_%d", mapID.utf8(), subz, suby, subx);

    for (int attempts = 0; attempts < totalSubTileAttempts; attempts++) {
        int xm2 = subx % 2;
        int ym2 = suby % 2;
        double nx0 = (0 == xm2) ? 0.0 : 0.5;
        double nx1 = (0 == xm2) ? 0.5 : 1.0;
        double ny0 = (0 == ym2) ? 0.5 : 0.0;
        double ny1 = (0 == ym2) ? 1.0 : 0.5;
        double ox0 = subx0, oy0 = suby0;
        double ox1 = subx1, oy1 = suby1;
        double dy = ny1 - ny0;
        double dx = nx1 - nx0;
        subx0 = nx0 + ox0 * dx;
        subx1 = nx0 + ox1 * dx;
        suby0 = ny0 + oy0 * dy;
        suby1 = ny0 + oy1 * dy;
        subz--;
        if (subz < 0)
            break;
        suby /= 2;
        subx /= 2;
        k = TileID (mapID, subz, suby, subx);
        // k.Sprintf("%s_%d_%d_%d", mapID.utf8(), subz, suby, subx);

        if (attempts == 1) // grab a 25% tile while we're at it?
        {
            cache->AppendTileRequest (TileID (mapID, z, ty, tx));
        }

        if (cache->Has (k)) { // smallerFound = true;
            if (useLower) {
                TexGL *tgl = cache->TileFor (k);
                if (NULL != tgl)
                    DrawAbsoluteRect (k, subx0, suby0, subx1, suby1,
                                      pod.phasex + (x)*pod.cursizeW,
                                      pod.phasey + y * pod.cursizeH,
                                      pod.phasex + (x + 1) * pod.cursizeW,
                                      pod.phasey + (y + 1) * pod.cursizeH,
                                      LowerColor (pod.color), tgl, pod.cursizeW,
                                      pod.cursizeH, 0.0);
            }
            break;
        }
    }
}

void MosaiQuad::DrawUseHigher (const int x,
                               const int y,
                               const int z,
                               const int tx,
                               const int ty,
                               bool &useHigher,
                               bool &useBG) { // ... the reason we prefer lower
                                              // res tiles is that there are 4x
                                              // or 16x fewer of them
    // since drawing creates demand, and demand creates cache invalidation,
    // which in general
    // we don't want
    // ... ?
    const DrawPod &pod = current_pod;

    double cornerx0 =
        pod.phasex + (x)*pod.cursizeW; //, cornerx1 = phasesx + (x+1)*cursize;
    double cornery0 =
        pod.phasey + (y)*pod.cursizeH; //, cornery1 = phasesy + (y+1)*cursize;
    int attempts = 1;
    int subz = z + attempts; // if (subz > LOD) continue;
    int p2 = 2;
    double d2 = pow (0.5, attempts);
    for (int yy = 0; yy < p2; yy++)
        for (int xx = 0; xx < p2; xx++) {
            int subx = (tx << attempts) + xx, suby = (ty << attempts) + yy;
            TileID k = TileID (mapID, subz, suby, subx);
            // k.Sprintf("%s_%d_%d_%d", mapID.utf8(), subz, suby, subx);

            if (cache->Has (k)) {
                if (useHigher || pod.crossfading) {
                    TexGL *tgl = cache->TileFor (k);
                    if (NULL != tgl)
                        DrawAbsoluteRect (
                            k, 0, 0, 1, 1, cornerx0 + xx * d2 * pod.cursizeW,
                            cornery0 + yy * d2 * pod.cursizeH,
                            cornerx0 + (xx + 1) * d2 * pod.cursizeW,
                            cornery0 + (yy + 1) * d2 * pod.cursizeH,
                            HigherColor (pod.color), tgl, d2 * pod.cursizeW,
                            d2 * pod.cursizeH, 0.0);
                }
            }
        }
}

void MosaiQuad::DrawHighres (const int x,
                             const int y,
                             const TileID &kfull,
                             const bool drawn_more_than_once,
                             bool &use1x1) { // ----------------------
    // now draw higher res (1 to 1) tile if it is available
    const DrawPod &pod = current_pod;

    if (cache->Has (kfull)) {
        // don't fade in too fast when xwrap is doing its thing
        // (otherwise incr would be call more than once on this tile!)
        if (!pod.crossfading)
            SetTileOpacity (kfull, 0.0);

        if (use1x1 || pod.crossfading) {
            ResetOpacityConditionally (kfull);
            TexGL *tgl = cache->TileFor (kfull);
            if (NULL != tgl)
                DrawAbsoluteRect (kfull, 0, 0, 1, 1,
                                  pod.phasex + (x)*pod.cursizeW,
                                  pod.phasey + y * pod.cursizeH,
                                  pod.phasex + (x + 1) * pod.cursizeW,
                                  pod.phasey + (y + 1) * pod.cursizeH,
                                  HighresColor (pod.color), tgl, pod.cursizeW,
                                  pod.cursizeH, TileOpacity (kfull));
        }
    }
}

static const v2float64 V2F64 (const float64 x, const float64 y) {
    v2float64 out;
    out.x = x;
    out.y = y;
    return out;
}

void MosaiQuad::DrawRelativeRect (const TileID &tid,
                                  float x0,
                                  float y0,
                                  float x1,
                                  float y1,
                                  ObColor color,
                                  TexGL *tgl,
                                  float lef,
                                  float top,
                                  float rig,
                                  float bot,
                                  const float64 opacity_start) {
    float o = 0.5f;

    while (tiles_to_render.size () <= size_t (tile_render_index))
        tiles_to_render.push_back (RenderTile ());

    RenderTile &tile = tiles_to_render[tile_render_index++];
    tile.tid = tid;
    tile.color = color;
    tile.alpha_start = opacity_start;
    tile.tex = tgl;
    tile.texcoors[0] = (V2F64 (lef, bot));
    tile.verts[0] = (LocOf (x0 - o, y0 - o));
    tile.texcoors[1] = (V2F64 (rig, bot));
    tile.verts[1] = (LocOf (x0 - o, y1 - o));
    tile.texcoors[2] = (V2F64 (rig, top));
    tile.verts[2] = (LocOf (x1 - o, y1 - o));
    tile.texcoors[3] = (V2F64 (lef, top));
    tile.verts[3] = (LocOf (x1 - o, y0 - o));
}

void MosaiQuad::DrawAbsoluteRect (const TileID &tid,
                                  float tx0,
                                  float ty0,
                                  float tx1,
                                  float ty1,
                                  float x0,
                                  float y0,
                                  float x1,
                                  float y1,
                                  ObColor color,
                                  TexGL *tgl,
                                  float cursizeW,
                                  float cursizeH,
                                  const float64 opacity_start) {
    if (NULL == tgl)
        return;

    float w = Width ();
    float h = Height ();

    if ((x0 < 0 && x1 < 0) || (x0 > w && x1 > w) || (y0 < 0 && y1 < 0) ||
        (y0 > h && y1 > h))
        return; // draw nothing if entire rect is outside of (0,0)-(w,h)

    // clip
    float lef = 0, top = 1;
    float rig = 1, bot = 0;
    float nx0 = x0, ny0 = y0;
    float nx1 = x1, ny1 = y1;
    if (x0 < 0) {
        nx0 = 0;
        lef = -x0 / cursizeW;
    }
    if (x1 > w) {
        nx1 = w;
        rig = 1.0 - (x1 - w) / cursizeW;
    }
    if (y0 < 0) {
        ny0 = 0;
        top = 1.0 - (-y0 / cursizeH);
    }
    if (y1 > h) {
        ny1 = h;
        bot = (y1 - h) / cursizeH;
    }

    double dtx = tx1 - tx0;
    double dty = ty1 - ty0;

    // almost right, rows of subtiles are flipped vertically!
    lef = tx0 + dtx * lef;
    rig = tx0 + dtx * rig;
    top = ty0 + dty * top;
    bot = ty0 + dty * bot;

    // draw a tile (or a clipped subtile)
    DrawRelativeRect (tid, 1.0 - ny0 / h, nx0 / w, 1.0 - ny1 / h, nx1 / w,
                      color, tgl, lef, bot, rig, top, opacity_start);
}
