
/* (c)  oblong industries */

#ifndef BUFFED_ACETATE_UNDERLAY
#define BUFFED_ACETATE_UNDERLAY

#include <libNoodoo/VertexForm.h>

#include "Acetate.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::ganglia;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class BuffedAcetate : public Acetate {
    PATELLA_SUBCLASS (BuffedAcetate, Acetate);

protected:
    ObRef<VertexForm *> underlay;
    float64 corner_radius;
    SOFT_MACHINERY (SoftColor, BackingColor);
    SOFT_MACHINERY (SoftColor, BorderColor);

public:
    BuffedAcetate ();
    virtual ~BuffedAcetate ();

    virtual ObRetort AcknowledgeLocationChange ();
    virtual ObRetort AcknowledgeSizeChange ();

    void SetBackingColor (const ObColor &c);
    void SetBorderColor (const ObColor &c);

    void SetCornerRadius (const float64 &rad);
    float64 CornerRadius () const;

    virtual void _RecomputeUnderlayVertices ();
    void _ComputeBezCorner (VertexForm *vform,
                            const Vect &corner,
                            const Vect &from,
                            const Vect &to,
                            const float &radius,
                            const int32 &start_pt,
                            const int32 &num_pts);

    virtual ObRetort Travail (Atmosphere *atm);
};
}
} // bye bye namespaces sluice, oblong

#endif
