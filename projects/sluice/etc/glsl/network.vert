#version 120

uniform vec2  add_to_carto;
uniform vec2  mul_by_carto;
uniform float eltsize;
uniform vec4  eltcolor;
uniform vec3  up;
uniform vec3  over;
#ifndef USE_WEBGL
uniform vec3  normal;
#endif
uniform vec3  center;

#ifdef USE_WEBGL
uniform vec4 glcolor;
attribute vec3 color;
varying vec4 glFrontColor;
varying vec2 glTexCoord[2];
varying vec4 glClipVertex;
#endif

vec3 CartoToWorld(vec2 carto)
{ float x_out = add_to_carto.x + (mul_by_carto.x * carto.x);
  float y_out = add_to_carto.y + (mul_by_carto.y * carto.y);
  return center + y_out * up + x_out * over;
}


void main()
{
#ifdef USE_WEBGL
  vec2 tx = uv.xy;
  vec3 loc = CartoToWorld (position.xy);
#else
  vec2 tx = gl_MultiTexCoord0.xy;
  vec3 loc = CartoToWorld (gl_Vertex.xy);
#endif

  vec4 where = vec4(loc + eltsize * (over * (tx.x - 0.5) + up * (tx.y - 0.5)), 1.0);

#ifdef USE_WEBGL
  glFrontColor = vec4(color, 1.0);
  glTexCoord[0] = uv;
  gl_Position = projectionMatrix * modelViewMatrix * where;
  glClipVertex = modelViewMatrix * where;
#else
  gl_FrontColor = gl_Color;
  gl_TexCoord[0] = gl_MultiTexCoord0;
  gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * where;
  gl_ClipVertex = gl_ModelViewMatrix * where;
#endif
}
