#version 120

uniform sampler2D tex;
uniform vec4  eltcolor;
uniform float offset;
// uniform float whitespace;

#ifdef USE_WEBGL
uniform vec4 glcolor;
varying vec2 glTexCoord[2];
#endif

float easeInOut( float t )
{
  return t * t * (3.0 - 2.0 * t); // mschuresko : I think glsl
  // has a built-in function for this as well.
}

/*
animates tex coords to cause a stretching effect
along the texture's vertical axis
*/

void main()
{
#ifdef USE_WEBGL
  vec2 texCoord = glTexCoord[0].st;
#else
  vec2 texCoord = gl_TexCoord[0].st;
#endif
  float scalar = fract(offset);
  if( scalar < 0.5 )
  { scalar = easeInOut(scalar * 2.0); }
  else
  { scalar = easeInOut(2.0 - scalar * 2.0); }
  // scalar *= whitespace;
  scalar *= 0.4; // 2.0 / 5.0

  float delta = easeInOut (texCoord.s) - 0.5;
  texCoord.s = 0.5 + (0.6 * (texCoord.s - 0.5)) + delta * scalar;
#ifdef USE_WEBGL
  gl_FragColor = glcolor * eltcolor *texture2D(tex, texCoord.st);
#else
  gl_FragColor = gl_Color * eltcolor *texture2D(tex, texCoord.st);
#endif
  // gl_FragColor = vec4(scalar, 0, 0, 1);
}
