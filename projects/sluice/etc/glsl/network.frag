#version 120

uniform vec4      eltcolor;
#ifdef USE_WEBGL
uniform sampler2D texture;
uniform vec4 glcolor;
varying vec2 glTexCoord[2];
varying vec4 glFrontColor;
#else
uniform sampler2D tex;
#endif

void main ()
{
#ifdef USE_WEBGL
  gl_FragColor = glcolor * eltcolor * texture2D(texture, glTexCoord[0].st);
#else
  gl_FragColor = gl_Color * eltcolor * texture2D(tex, gl_TexCoord[0].st);
#endif
}
