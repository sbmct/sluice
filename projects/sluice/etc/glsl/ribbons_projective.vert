#version 120

uniform vec2  add_to_carto;
uniform vec2  mul_by_carto;
uniform float eltsize;
uniform float ribbon_texture_length_scale;
uniform vec4  eltcolor;
uniform vec3  up;
uniform vec3  over;
#ifndef USE_WEBGL
uniform vec3  normal;
#endif
uniform vec3  center;

#ifdef USE_WEBGL
uniform vec4 glcolor;
attribute vec3 color;
varying vec4 glFrontColor;
varying vec4 glTexCoord[2]; 
varying vec4 glClipVertex;
#endif

// So, sideways. We're going to calculate this variable in
// carto space and pass it in for each vertex.  It's a
// normalized vector, so carto space should totally work
// out the same as world space, right? What could possibly
// go wrong?  IMPORTANT NOTE: EXPECT ALL OF THIS TO GO
// 100% WRONG IF WE EVER CHANGE THE ORIENTATION OF THE MAP.
attribute vec2 sideways;

vec3 CartoToWorld(vec2 carto)
{ float x_out = add_to_carto.x + (mul_by_carto.x * carto.x);
  float y_out = add_to_carto.y + (mul_by_carto.y * carto.y);
  return center + y_out * up + x_out * over;
}



void main ()
{
#ifdef USE_WEBGL
  vec3 loc = CartoToWorld (position.xy);
  vec4 where = vec4(loc + vec3(sideways.xy, 0.0) * eltsize, 1.0);

  glFrontColor = glcolor;
  glTexCoord[0] = vec4(uv.s,
			ribbon_texture_length_scale *
			uv.t,
			1.0, 1.0);
  gl_Position = projectionMatrix * modelViewMatrix * where;
  glClipVertex = modelViewMatrix * where;
#else
  vec3 loc = CartoToWorld (gl_Vertex.xy);
  vec4 where = vec4(loc + vec3(sideways.xy, 0.0) * eltsize, 1.0);

  gl_FrontColor = gl_Color;
  gl_TexCoord[0] = vec4(gl_MultiTexCoord0.s,
			ribbon_texture_length_scale *
			gl_MultiTexCoord0.t,
			1.0, 1.0);
  gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * where;
  gl_ClipVertex = gl_ModelViewMatrix * where;
#endif
}
