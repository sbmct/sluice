
How to convert from carto space to screen projection.

First converting from cartographic space to "flat thing" space.

Assumes the minimum and maximum boundaries of the carto world
in flat space.

> CartoToUV (x, y) = (u, v) where
>     u = carto_min_x + (x + 0.5) * (carto_max_x - carto_min_x)
>     v = carto_min_y + (y + 0.5) * (carto_max_y - carto_min_y)

And the from FT space to world space. Assumes center, over, and
up vectors as well as width, height, vert_align and horiz_align
floats.

> LocOf (u, v) = center + dva * up * height + dha * over * width where
>     dva = v - vert_align
>     dha = u - horz_align

Finally, wrapping the whole thing up.

> CartoToWrldPos (x, y) = LocOf $ CartoToUV (x, y)
