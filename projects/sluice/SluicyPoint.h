/* Copyright (C) 2011 Oblong Industries */

#ifndef OBLONG_SLUICE_SLUICYPOINT_H
#define OBLONG_SLUICE_SLUICYPOINT_H

#include "HandiPoint.h"

namespace oblong {
namespace sluice {
class Windshield;

/**
 * A HandiPoint that knows how to deal with the mouse wheel
 *
 */
class SluicyPoint : public HandiPoint {
    PATELLA_SUBCLASS (SluicyPoint, HandiPoint);

protected:
    ObMap<ObEvent *, float64> synth_pointing;
    FatherTime touchpad_timer;

    void AppendHarden (OEPointingHardenEvent *evt,
                       const Vect offset,
                       const float64 x);
    void
    AppendMove (OEPointingHardenEvent *evt, const Vect offset, const float64 x);
    void AppendSoften (OEPointingHardenEvent *evt,
                       const Vect offset,
                       const float64 x);

    void AppendSynthShove (OEPointingHardenEvent *evt);
    void AppendSynthPull (OEPointingHardenEvent *evt);
    void AppendSynthZoom (Windshield *vf,
                          OEPointingHardenEvent *evt,
                          const Vect norm,
                          const float64 zoom);
    void AppendSynthEvent (ObEvent *evt, const float64 wait);

    void ConsumeSynthPointing (Atmosphere *atmo);

public:
    SluicyPoint ();

    virtual ObRetort OEPointingHarden (OEPointingHardenEvent *evt,
                                       Atmosphere *atm);
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_SLUICYPOINT_H
