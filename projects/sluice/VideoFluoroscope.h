/* Copyright (C) 2012 Oblong Industries */

#ifndef OBLONG_SLUICE_VIDEOFLUOROSCOPE_H
#define OBLONG_SLUICE_VIDEOFLUOROSCOPE_H

#include "Fluoroscope.h"

#include <libLoam/c++/FatherTime.h>
#include <libBasement/ImageClot.h>
#include <libBasement/SoftColor.h>

namespace oblong {
namespace sluice {

using namespace oblong::basement;

/**
 * Show a geolocated video
 *
 * Logic about refreshing the video and what-have-you goes here?
 */
class VideoFluoroscope : public Fluoroscope {
    PATELLA_SUBCLASS (VideoFluoroscope, Fluoroscope);

private:
    float64 video_info_wait_threshold;
    FatherTime update_timer, refresh_timer;
    float64 refresh_frequency;
    GeoRect visible_bounds;
    bool stay_put;
    Slaw config_video_control;

    FLAG_MACHINERY (KnowsVisibleBounds);
    FLAG_MACHINERY (ShouldRefreshPeriodically);
    FLAG_MACHINERY (DisableNotifications);

    FLAG_MACHINERY (RebaseRequested);
    FLAG_MACHINERY (SourceLocationStatic);
    FLAG_MACHINERY (WaitingForStaticSource);
    FatherTime rebase_timer;
    float64 rebase_wait_minimum;

    LatLon full_bl, full_tr;

    /**
     * Use this to compare new slaw to decide whether to open a new video
     * when a new update comes from the daemon.
     */
    Slaw last_opened_video_slaw;

    SOFT_MACHINERY (SoftColor, NotificationColor);
    FLAG_MACHINERY (WaitingForVideoInfo);

public:
    virtual ~VideoFluoroscope ();
    VideoFluoroscope ();

    virtual ObRetort SetConfigurationSlaw (const Slaw config);

    virtual ObRetort AcknowledgeGeoQuadSizeChanged (GeoQuad *);
    virtual ObRetort AcknowledgeGeoQuadLocationChanged (GeoQuad *);

    virtual void InitializePoolRegistration (Hasselhoff *hoff);
    ObRetort MetabolizeVideoUpdate (const Protein &prt, Atmosphere *atm);

    const float64 VideoInfoWaitThreshold () const;
    void SetVideoInfoWaitThreshold (const float64 x);

    virtual void Rebase ();

    virtual ObRetort Inhale (Atmosphere *atmo);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atmo);

protected:
    virtual ObRetort RequestProtein (Protein &prot);
    virtual void
    SetVideoSlaw (const Slaw &resource, const LatLon &bl, const LatLon &tr);
    virtual void SetBounds (const LatLon &bl, const LatLon &tr);
    virtual void PropogateStayPut ();
    virtual void RequestNewVideoInfo ();

    bool MatchesCachedSlaw (const Slaw &compare_to) const;
    void SetCachedSlaw (const Slaw &cache);
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_VIDEOFLUOROSCOPE_H
