/* Copyright (C) 2012 Oblong Industries */

#ifndef OBLONG_SLUICE_FLUOROFACTORY_H
#define OBLONG_SLUICE_FLUOROFACTORY_H

#include <libPlasma/c++/Slaw.h>

namespace oblong {
namespace sluice {

using oblong::plasma::Slaw;

class Fluoroscope;
/**
 * Build Fluoroscopes from slaw
 *
 */
class FluoroFactory {
public:
    static Fluoroscope *Fabricate (const Slaw config);
    static bool IsValidConfigurationSlaw (const Slaw config);
    // TODO: Provide a way to dynamically add fluoroscope builders
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_FLUOROFACTORY_H
