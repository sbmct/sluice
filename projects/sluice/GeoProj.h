
#ifndef GEO_PROJ_HAS_THE_WHOLE_WORLD_IN_ITS_HANDS
#define GEO_PROJ_HAS_THE_WHOLE_WORLD_IN_ITS_HANDS

#include <libLoam/c++/AnkleObject.h>

#include "geo_utils.h"

namespace oblong {
namespace sluice {
using namespace oblong::loam;
using namespace oblong::sluice;

/**
 * GeoProj -->
 *
 * A GeoProj encapsulates a cylindrical map projection which ensures parallel,
 * equally spaces longitudinal lines.
 *
 * Coordinates & Conventions:
 *
 * Geographic coordinates are parameterized in terms of LatLon's (degrees
 * Latitude/Longitude). LatLon's are projected into a normalized 2D unit square.
 * We refer to this as "Cartographic" space due to its close relationship with
 * a cartographic map. From there, this square may be transformed (scaled &
 * translated) within the 2D plane. We refer to this fully transformed projected
 * space in terms of "UV" coordinates.
 *
 *  - Geo    : spherical coordinates in degrees latitude/longitude
 *
 *  - Carto  : cartographic space (NON-transformed, normalized projection area)
 *                [0,0] --> MinValidLatLon
 *                [1,1] --> MaxValidLatLon
 *
 *  - UV     : transformed projected area
 *                uv_proj_min --> MinValidLatLon
 *                uv_proj_max --> MaxValicLatLon
 *
 *
 * Extrapolation Policy -->
 *
 * While any valid LatLon will always map to a real UV value, there are many
 * scenarios for interpreting the latitude and longitude of a point outside of
 * the projected unit rectangle (i.e. XY values <0 or >1).
 *
 * For example, as we cross +180° lon, we could
 *   a. Clamp
 *   b. Wrap to -180°
 *   c. Extrapolate into >180°
 *
 * Latitudes, on the other hand are necessarily bounded to a finite range of
 * logical values. Therefore, any UV coordinates mapping outside the valid
 * latitude range are always clamped.
 *
 **/
class GeoProj : public AnkleObject {
    PATELLA_SUBCLASS (GeoProj, AnkleObject);

public:
    enum Longitude_Extrapolation_Policy { Clamp = 0, Wrap, Extrapolate };

protected:
    Longitude_Extrapolation_Policy lon_extrap_plcy;

public:
    GeoProj ();

    virtual LatLon MinValidLatLon () const = 0;
    virtual LatLon MaxValidLatLon () const = 0;

    int LEP () const;
    void SetLEP (Longitude_Extrapolation_Policy lep);

    ///
    /// Coordinate Conversions
    ///

    /// Carto <--> LatLon
    ///
    /// The actual projection between spherical LatLon space and normalized
    /// rectangular space. These must be overwritten by subclasses.
    ///
    virtual void CartoToLatLon (const float64 &cx,
                                const float64 &cy,
                                float64 &lat,
                                float64 &lon) const = 0;
    void CartoToLatLon (const v2float64 &cxcy, LatLon &ll) const;

    LatLon CartoToLatLon (const float64 &cx, const float64 &cy) const;
    LatLon CartoToLatLon (const v2float64 &cxcy) const;

    virtual void LatLonToCarto (const float64 &lat,
                                const float64 &lon,
                                float64 &cx,
                                float64 &cy) const = 0;
    void LatLonToCarto (const LatLon &ll, v2float64 &cxcy) const;

    v2float64 LatLonToCarto (const float64 &lat, const float64 &lon) const;
    v2float64 LatLonToCarto (const LatLon &ll) const;
};

/**
 * EquirectProj -->
 *
 * The Equirectangular projection. This is the simplest projection and maps
 * latitude and longitude directly to the x&y axes of a regular rectangle.
 * While simple to implement, it exhibits much stretching and distortion as you
 * move away from the equator.
 *
 **/
class EquirectProj : public GeoProj {
    PATELLA_SUBCLASS (EquirectProj, GeoProj);

public:
    EquirectProj ();

    virtual LatLon MinValidLatLon () const override;
    virtual LatLon MaxValidLatLon () const override;

    virtual void CartoToLatLon (const float64 &cx,
                                const float64 &cy,
                                float64 &lat,
                                float64 &lon) const override;

    virtual void LatLonToCarto (const float64 &lat,
                                const float64 &lon,
                                float64 &cx,
                                float64 &cy) const override;
};

/**
 * MercProj -->
 *
 * The Mercator projection. This projection counteracts much of the
 * "squishy-ness" of the equirectangular projection near the poles, but is also
 * characterized by its inability to map the full range of latitude.
 *
 * Additionally, users of this projection should take care when implementing
 * actions such as panning & zooming a projected area. Since latitude is
 *projected
 * non-linearly here, moving 1 deg latitude will vary UV space.
 *
 **/
class MercProj : public GeoProj {
    PATELLA_SUBCLASS (MercProj, GeoProj);

private:
    /// Constant for all instances, yet expensive to compute (see inlines below)
    /// Let's just find this once, eh?
    static const float64 max_valid_lat;

    /// given normalized [0,1] value
    /// returns reverse-projected value in valid mercator latitude range
    static inline float64 _merc_norm_to_lat (const float64 &y) {
        return atan (sinh (((2.0 * y) - 1.0) * M_PI));
    }

    /// given latitude in radians
    ///  - note: must be in valid range --> +/-reverse_project(PI)
    /// returns normalized projected value [0,1] over whole valid range
    static inline float64 _merc_lat_to_norm (const float64 &rad_lat) {
        return (1.0 + log ((sin (rad_lat) + 1.0) / cos (rad_lat)) / M_PI) / 2.0;
    }

public:
    MercProj ();

    virtual LatLon MinValidLatLon () const override;
    virtual LatLon MaxValidLatLon () const override;

    virtual void CartoToLatLon (const float64 &cx,
                                const float64 &cy,
                                float64 &lat,
                                float64 &lon) const override;

    virtual void LatLonToCarto (const float64 &lat,
                                const float64 &lon,
                                float64 &cx,
                                float64 &cy) const override;
};
}
} /// end namespaces oblong, staging

#endif // GEO_PROJ_HAS_THE_WHOLE_WORLD_IN_ITS_HANDS
