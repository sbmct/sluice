static const char *tweet_inculs =
    "%YAML 1.1\n"
    "%TAG ! tag:oblong.com,2009:slaw/\n"
    "--- !protein\n"
    "descrips:\n"
    "- sluice\n"
    "- \"prot-spec v1.0\"\n"
    "- inculcators\n"
    "- change\n"
    "ingests:\n"
    "  inculcators:\n"
    "  - name: Composite\n"
    "    kinds:\n"
    "    - composite\n"
    "    pigment: Composite\n"
    "  - name: point_Breaker_default\n"
    "    kinds:\n"
    "    - Breaker\n"
    "    pigment: point_Breaker_default\n"
    "    properties:\n"
    "      status: ~\n"
    "      state: ~\n"
    "  - name: point_Breaker_status_open\n"
    "    kinds:\n"
    "    - Breaker\n"
    "    pigment: point_Breaker_status_open\n"
    "    properties:\n"
    "      status: open\n"
    "  - name: point_Breaker_status_closed\n"
    "    kinds:\n"
    "    - Breaker\n"
    "    pigment: point_Breaker_status_closed\n"
    "    properties:\n"
    "      status: closed\n"
    "  - name: point_Breaker_state_outage root\n"
    "    kinds:\n"
    "    - Breaker\n"
    "    pigment: point_Breaker_state_outage root\n"
    "    properties:\n"
    "      state: outage root\n"
    "  - name: point_Breaker_state_outage\n"
    "    kinds:\n"
    "    - Breaker\n"
    "    pigment: point_Breaker_state_outage\n"
    "    properties:\n"
    "      state: outage\n"
    "  - name: point_TransformerBank_default\n"
    "    kinds:\n"
    "    - TransformerBank\n"
    "    pigment: point_TransformerBank_default\n"
    "    properties:\n"
    "      mount-type: ~\n"
    "  - name: point_TransformerBank_mount-type_pad\n"
    "    kinds:\n"
    "    - TransformerBank\n"
    "    pigment: point_TransformerBank_mount-type_pad\n"
    "    properties:\n"
    "      mount-type: pad\n"
    "  - name: point_TransformerBank_mount-type_pole-top\n"
    "    kinds:\n"
    "    - TransformerBank\n"
    "    pigment: point_TransformerBank_mount-type_pole-top\n"
    "    properties:\n"
    "      mount-type: pole-top\n"
    "  - name: point_FuseBank_default\n"
    "    kinds:\n"
    "    - FuseBank\n"
    "    pigment: point_FuseBank_default\n"
    "  - name: point_FuseBank_outage_root\n"
    "    kinds:\n"
    "    - FuseBank\n"
    "    pigment: point_FuseBank_outage_root\n"
    "    properties:\n"
    "      state: outage root\n"
    "  - name: point_FuseBank_open\n"
    "    kinds:\n"
    "    - FuseBank\n"
    "    pigment: point_FuseBank_open\n"
    "    properties:\n"
    "      status: open\n"
    "  - name: point_FuseBank_closed\n"
    "    kinds:\n"
    "    - FuseBank\n"
    "    pigment: point_FuseBank_closed\n"
    "    properties:\n"
    "      status: closed\n"
    "  - name: point_WireChangeAsset_default\n"
    "    kinds:\n"
    "    - WireChangeAsset\n"
    "    pigment: point_WireChangeAsset_default\n"
    "  - name: point_FaultIndicator_default\n"
    "    kinds:\n"
    "    - FaultIndicator\n"
    "    pigment: point_FaultIndicator_default\n"
    "  - name: point_SwitchBank_default\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_default\n"
    "    properties:\n"
    "      status: ~\n"
    "      state: ~\n"
    "      Switch.normalOpen: ~\n"
    "  - name: point_SwitchBank_default_closed\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_default_closed\n"
    "    properties:\n"
    "      status: ~\n"
    "      Switch.normalOpen: 'false'\n"
    "  - name: point_SwitchBank_default_open\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_default_open\n"
    "    properties:\n"
    "      status: ~\n"
    "      Switch.normalOpen: 'true'\n"
    "  - name: point_SwitchBank_status_de_energized_due_to_outage\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_status_open\n"
    "    properties:\n"
    "      status: de-energized due to outage\n"
    "  - name: point_SwitchBank_status_de_energized\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_status_open\n"
    "    properties:\n"
    "      status: de-energized\n"
    "  - name: point_SwitchBank_status_energized\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_status_closed\n"
    "    properties:\n"
    "      status: energized\n"
    "  - name: point_SwitchBank_status_open\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_status_open\n"
    "    properties:\n"
    "      status: open\n"
    "  - name: point_SwitchBank_status_closed\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_status_closed\n"
    "    properties:\n"
    "      status: closed\n"
    "  - name: point_SwitchBank_state_outage root\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_state_outage root\n"
    "    properties:\n"
    "      state: outage root\n"
    "  - name: point_SwitchBank_state_outage\n"
    "    kinds:\n"
    "    - SwitchBank\n"
    "    pigment: point_SwitchBank_state_outage\n"
    "    properties:\n"
    "      state: outage\n"
    "  - name: point_DistributionTransformerWinding_default\n"
    "    kinds:\n"
    "    - DistributionTransformerWinding\n"
    "    pigment: point_DistributionTransformerWinding_default\n"
    "    properties:\n"
    "      SinglePhaseActivePower: ~\n"
    "  - name: point_Meter_default\n"
    "    kinds:\n"
    "    - Meter\n"
    "    pigment: point_Meter_default\n"
    "  - name: point_Meter_RevenueProtection_ON\n"
    "    kinds:\n"
    "    - Meter\n"
    "    pigment: point_Meter_RevenueProtection_ON\n"
    "    properties:\n"
    "      RevenueProtection: ON\n"
    "  - name: point_Meter_RevenueProtection_OFF\n"
    "    kinds:\n"
    "    - Meter\n"
    "    pigment: point_Meter_RevenueProtection_OFF\n"
    "    properties:\n"
    "      RevenueProtection: OFF\n"
    "  - name: point_Meter_Social_ON\n"
    "    kinds:\n"
    "    - Meter\n"
    "    pigment: point_Meter_Social_ON\n"
    "    properties:\n"
    "      Social: ON\n"
    "  - name: point_Meter_Social_OFF\n"
    "    kinds:\n"
    "    - Meter\n"
    "    pigment: point_Meter_Social_OFF\n"
    "    properties:\n"
    "      Social: OFF\n"
    "  - name: point_Meter_PowerQuality_ON\n"
    "    kinds:\n"
    "    - Meter\n"
    "    pigment: point_Meter_PowerQuality_ON\n"
    "    properties:\n"
    "      PowerQuality: ON\n"
    "  - name: point_Meter_PowerQuality_OFF\n"
    "    kinds:\n"
    "    - Meter\n"
    "    pigment: point_Meter_PowerQuality_OFF\n"
    "    properties:\n"
    "      PowerQuality: OFF\n"
    "  - name: point_SocialMedia_default\n"
    "    kinds:\n"
    "    - SocialMedia\n"
    "    pigment: point_SocialMedia_default\n"
    "    properties:\n"
    "      socialMediaType: ~\n"
    "  - name: point_SocialMedia_socialMediaType_TWITTER\n"
    "    kinds:\n"
    "    - SocialMedia\n"
    "    pigment: point_SocialMedia_socialMediaType_TWITTER\n"
    "    properties:\n"
    "      socialMediaType: TWITTER\n"
    "  - name: point_SocialMedia_socialMediaType_FACEBOOK\n"
    "    kinds:\n"
    "    - SocialMedia\n"
    "    pigment: point_SocialMedia_socialMediaType_FACEBOOK\n"
    "    properties:\n"
    "      socialMediaType: FACEBOOK\n"
    "  - name: path_MeterTransformerLineSegment_default\n"
    "    kinds:\n"
    "    - MeterTransformerLineSegment\n"
    "    pigment: path_MeterTransformerLineSegment_default\n"
    "    properties:\n"
    "      socialMediaType: ~\n"
    "  - name: path_DistributionLineSegment_default\n"
    "    kinds:\n"
    "    - DistributionLineSegment\n"
    "    pigment: path_DistributionLineSegment_default\n"
    "    properties:\n"
    "      status: ~\n"
    "      state: ~\n"
    "  - name: path_DistributionLineSegment_status_energized\n"
    "    kinds:\n"
    "    - DistributionLineSegment\n"
    "    pigment: path_DistributionLineSegment_status_energized\n"
    "    properties:\n"
    "      status: energized\n"
    "  - name: path_DistributionLineSegment_status_de-energized\n"
    "    kinds:\n"
    "    - DistributionLineSegment\n"
    "    pigment: path_DistributionLineSegment_status_de-energized\n"
    "    properties:\n"
    "      status: de-energized\n"
    "  - name: path_DistributionLineSegment_status_de-energized due to outage\n"
    "    kinds:\n"
    "    - DistributionLineSegment\n"
    "    pigment: path_DistributionLineSegment_status_de-energized due to "
    "outage\n"
    "    properties:\n"
    "      status: de-energized due to outage\n"
    "  - name: path_DistributionLineSegment_state_faulty\n"
    "    kinds:\n"
    "    - DistributionLineSegment\n"
    "    pigment: path_DistributionLineSegment_state_faulty\n"
    "    properties:\n"
    "      state: faulty\n"
    "  - name: path_OverheadDistributionLineSegment_default\n"
    "    kinds:\n"
    "    - OverheadDistributionLineSegment\n"
    "    pigment: path_OverheadDistributionLineSegment_default\n"
    "    properties:\n"
    "      status: ~\n"
    "      state: ~\n"
    "  - name: path_OverheadDistributionLineSegment_status_energized\n"
    "    kinds:\n"
    "    - OverheadDistributionLineSegment\n"
    "    pigment: path_OverheadDistributionLineSegment_status_energized\n"
    "    properties:\n"
    "      status: energized\n"
    "  - name: path_OverheadDistributionLineSegment_status_de-energized\n"
    "    kinds:\n"
    "    - OverheadDistributionLineSegment\n"
    "    pigment: path_OverheadDistributionLineSegment_status_de-energized\n"
    "    properties:\n"
    "      status: de-energized\n"
    "  - name: path_OverheadDistributionLineSegment_status_de-energized due "
    "to\n"
    "      outage\n"
    "    kinds:\n"
    "    - OverheadDistributionLineSegment\n"
    "    pigment: path_OverheadDistributionLineSegment_status_de-energized "
    "due\n"
    "      to outage\n"
    "    properties:\n"
    "      status: de-energized due to outage\n"
    "  - name: path_OverheadDistributionLineSegment_state_faulty\n"
    "    kinds:\n"
    "    - OverheadDistributionLineSegment\n"
    "    pigment: path_OverheadDistributionLineSegment_state_faulty\n"
    "    properties:\n"
    "      state: faulty\n"
    "  - name: path_UndergroundDistributionLineSegment_default\n"
    "    kinds:\n"
    "    - UndergroundDistributionLineSegment\n"
    "    pigment: path_UndergroundDistributionLineSegment_default\n"
    "    properties:\n"
    "      status: ~\n"
    "      state: ~\n"
    "  - name: path_UndergroundDistributionLineSegment_status_energized\n"
    "    kinds:\n"
    "    - UndergroundDistributionLineSegment\n"
    "    pigment: path_UndergroundDistributionLineSegment_status_energized\n"
    "    properties:\n"
    "      status: energized\n"
    "  - name: path_UndergroundDistributionLineSegment_status_de-energized\n"
    "    kinds:\n"
    "    - UndergroundDistributionLineSegment\n"
    "    pigment: path_UndergroundDistributionLineSegment_status_de-energized\n"
    "    properties:\n"
    "      status: de-energized\n"
    "  - name: path_UndergroundDistributionLineSegment_status_de-energized "
    "due\n"
    "      to outage\n"
    "    kinds:\n"
    "    - UndergroundDistributionLineSegment\n"
    "    pigment: path_UndergroundDistributionLineSegment_status_de-energized\n"
    "      due to outage\n"
    "    properties:\n"
    "      status: de-energized due to outage\n"
    "  - name: path_UndergroundDistributionLineSegment_state_faulty\n"
    "    kinds:\n"
    "    - UndergroundDistributionLineSegment\n"
    "    pigment: path_UndergroundDistributionLineSegment_state_faulty\n"
    "    properties:\n"
    "      state: faulty\n"
    "...";
