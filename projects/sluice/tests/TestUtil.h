
/* (c) Conduce Inc. */

#ifndef TEST_UTIL_H__
#define TEST_UTIL_H__

#include <initializer_list>

#include <gtest/gtest.h>

#include <libBasement/UrDrome.h>
#include <libPlasma/c++/Hose.h>

using namespace oblong::plasma;
using namespace oblong::sluice;

#include "../NodeStoreImpl.h"
#include "../SluiceSettings.h"
#include "../SluiceConsts.h"
#include "../AtlasQuad.h"
#include "../YaroSneeze.h"
#include "../WideScreen.h"
#include "../Hasselhoff.h"

class HoseChecker {
private:
    ObRef<Hose *> hose;

public:
    HoseChecker (const Str name) : hose{new Hose (name)} { hose->ToLast (); }
    HoseChecker (Hose *h) : hose{h} {}

    Protein Current () { return hose->Current (); }

    bool HasFutureProteins () {
        auto cur = hose->CurrentIndex (), newest = hose->NewestIndex ();
        return cur < newest;
    }

    bool ToLast () { return hose->ToLast ().IsSplend (); }
    bool CurrentHasDescrips (const Slaw d) {
        auto pro = Current ();
        if (-1 == slaw_list_gapsearch (pro.Descrips ().SlawValue (),
                                       d.SlawValue ())) {
            return false;
        } else {
            return true;
        }
    }
    bool CurrentHasIngestKeys (std::initializer_list<Slaw> keys) {
        auto pro = Current ();
        auto ing = pro.Ingests ();
        for (auto s : keys) {
            if (ing.MapFind (s).IsNull ()) {
                return false;
            }
        }
        return true;
    }

    bool CurrentHasKeyVal (Slaw key, Slaw val) {
        auto pro = Current ();
        auto ing = pro.Ingests ();
        Slaw tmp;
        if (ing.MapFind (key).Into (tmp)) {
            return tmp == val;
        }
        return false;
    }

    template <typename T> bool GetCurrentIngestVal (Slaw key, T &val) {
        auto ing = Current ().Ingests ();
        ing.SlawValue (); // annoying plasma bug
        return ing.MapFind (key).Into (val);
    }

    template <typename T> bool CurrentInto (Slaw key, T &val) {
        return Current ().Ingests ().MapFind (key).Into (val);
    }

    void Deposit (Protein p) { hose->Deposit (p); }
};

class BasicSluiceTest : public ::testing::Test {
protected:
    ObRef<YaroSneeze *> sneeze;
    ObRef<NodeStoreImpl *> store;
    ObRef<UrDrome *> drome;
    ObRef<Hasselhoff *> hoff;

public:
    virtual void SetUp () {
        drome = new UrDrome;
        hoff = new Hasselhoff;
        sneeze = new YaroSneeze;
        store = new NodeStoreImpl;
        drome->AppendChild (~sneeze);
        drome->AppendChild (~store);

        Dossier *doss = new Dossier ();
        sneeze->InstallDossier (doss);
        doss->InitiallyUnfurl (nullptr);

        sneeze->InstallHasselhoff (~hoff);

        auto map = MainMap ();
        map->SetNodeStore (~store);
    }

    virtual void TearDown () {
        store = nullptr;
        drome = nullptr;
    }

    AtlasQuad *MainMap () {
        WideScreen *wide = drome->Find<WideScreen>("*");
        EXPECT_TRUE (wide != nullptr) << "Widescreen was null";
        AtlasQuad *map = dynamic_cast<AtlasQuad *>(wide->MainMap ());
        EXPECT_TRUE (nullptr != map) << "Main Map is null";
        return map;
    }

    void RespireOnce () { drome->RespireSingly (); }
};

// Some convienence functions for making sluice-style configuration proteins
inline Slaw Selection (Str name, Str display, bool selected = false) {
    return Slaw::Map (
        "name", name, "display-text", display, "selected", selected);
}

inline Slaw Attr (Str name, bool inclusive, std::vector<Slaw> selections) {
    auto sels = Slaw::List ();
    for (auto sel : selections) {
        sels = sels.ListAppend (sel);
    }
    return Slaw::Map ("name",
                      name,
                      "selection-type",
                      inclusive ? "inclusive" : "exclusive",
                      "contents",
                      sels);
}

#endif
