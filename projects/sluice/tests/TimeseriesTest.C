#include <gtest/gtest.h>
#include "../Timeseries.h"

using namespace oblong::sluice;

typedef TimeSeries<int, double> series_t;

TEST (TimeSeries, Empty) {
    series_t series;
    EXPECT_FALSE (series.Has (1.0));
}

TEST (TimeSeries, NoExpiration) {
    series_t series;
    for (int i = 1; i < 10; ++i)
        series.Set (10.0 * i, i);
    EXPECT_FALSE (series.Has (0.0));
    EXPECT_FALSE (series.Has (9.0));
    EXPECT_TRUE (series.Has (10.0));
    EXPECT_TRUE (series.Has (100000.0));
    for (int i = 1; i < 10; ++i) {
        int tmp;
        EXPECT_TRUE (series.Get (0.5 + 10.0 * i, tmp));
        EXPECT_EQ (i, tmp);
    }
}

TEST (TimeSeries, LastAndStampFor) {
    series_t series;
    series.Set (2.0, 2);
    series.Set (1.0, 1);

    EXPECT_EQ (2.0, series.Last ());

    series.Set (3.0, 3);
    series.Expire (2.5);

    EXPECT_EQ (3.0, series.Last ());

    EXPECT_EQ (3.0, series.StampFor (3.5));
    EXPECT_EQ (1.0, series.StampFor (1.5));
}

TEST (TimeSeries, WithExpiration) {
    series_t series;
    series.Set (10.0, 1);
    series.Expire (15.0);
    series.Set (20.0, 2);

    EXPECT_FALSE (series.Has (0.0));
    EXPECT_TRUE (series.Has (10.0));
    EXPECT_TRUE (series.Has (14.99));
    EXPECT_FALSE (series.Has (15.0));
    EXPECT_FALSE (series.Has (19.99));
    EXPECT_TRUE (series.Has (20.0));
    EXPECT_TRUE (series.Has (25000.0));

    int tmp;
    EXPECT_TRUE (series.Get (10.0, tmp));
    EXPECT_EQ (1, tmp);
    EXPECT_TRUE (series.Get (14.99, tmp));
    EXPECT_EQ (1, tmp);
    EXPECT_FALSE (series.Get (15.0, tmp));
    EXPECT_FALSE (series.Get (19.99, tmp));
    EXPECT_TRUE (series.Get (20.0, tmp));
    EXPECT_EQ (2, tmp);
    EXPECT_TRUE (series.Get (250000.0, tmp));
    EXPECT_EQ (2, tmp);
}
