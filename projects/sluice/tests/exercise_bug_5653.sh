#!/bin/sh

cat <<EOF
Sluice should be running. The Mapquest OSM map should be selected.

EOF

poke edge-to-sluice <<EOF
%YAML 1.1
%TAG ! tag:oblong.com,2009:slaw/
--- !protein
descrips:
- sluice
- prot-spec v1.0
- request
- zoom
ingests:
  lon: -199.0
  level: 70.0
  lat: 0.0
...
EOF

cat <<EOF
If you don't see a screen full of Ocean -- that is, if Sulice
is showing a half-blue, half-grey screen with a cut off "{-1:-1}"
in the middle, Sluice is still broken.  If it's all Ocean, you're
good.
EOF


