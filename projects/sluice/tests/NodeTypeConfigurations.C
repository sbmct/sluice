#include <gtest/gtest.h>
#include "../SimpleNodeArt.h"
#include "../Fluoroscope.h"
#include "../TopoWard.h"

using namespace oblong::sluice;

Slaw A = Slaw::Map ("name",
                    "A",
                    "observations-labels",
                    Slaw::Map ("a.1", "1", "a.2", "2"),
                    "observations-blacklist",
                    Slaw::List ("a.3"));
Slaw B = Slaw::Map ("name",
                    "B",
                    "observations-labels",
                    Slaw::Map ("b.1", "1", "b.2", "2"),
                    "observations-blacklist",
                    Slaw::List ("b.3"));

Slaw nodes = Slaw::Map ("name",
                        "entity-kinds",
                        "selection-type",
                        "inclusive",
                        "contents",
                        Slaw::List (A));

Slaw net = Slaw::Map ("name",
                      "network",
                      "icon",
                      "network.png",
                      "type",
                      "network",
                      "attributes",
                      Slaw::List (nodes, paths));

Slaw NodeA = Slaw::Map ("id",
                        "a-1",
                        "kind",
                        "A",
                        "loc",
                        Vect (0.0, 0.0, 0.0),
                        "attrs",
                        Slaw::Map ("a.1", "a1", "a.2", "a2", "a.3", "a3"));
Slaw NodeB = Slaw::Map ("id",
                        "b-1",
                        "kind",
                        "B",
                        "path",
                        Slaw::List (Vect (0.0, 0.0, 0.0), Vect (1.0, 0.0, 0.0)),
                        "attrs",
                        Slaw::Map ("b.1", "b1", "b.2", "b2", "b.3", "b3"));

TEST (NodeTypeConfigurations, HoverLocLabel) {
    SimpleNodeArt *sna = new SimpleNodeArt (new Fluoroscope);
    sna->RefreshNodeSeriesDisplays (net);

    NodeStore *ns = new NodeStore ();
    TopoWard *tw = new TopoWard (ns);

    Node *n = NULL;
    tw->ReadNode (NodeA, n);
    if (n) {
        float64 t = ns->SluiceTime ();
        VizNodeTemplate *vnt = sna->VizTemplateForNode (n);
        Str txt = n->HoverTextAtTime (t, vnt);
        EXPECT_TRUE (txt.Match ("\t1: a1"));
        EXPECT_FALSE (txt.Match ("\ta.1: a1"));
        EXPECT_TRUE (txt.Match ("\t2: a2"));
        EXPECT_FALSE (txt.Match ("\ta.2: a2"));
    }
}

TEST (NodeTypeConfigurations, HoverPathLabel) {
    SimpleNodeArt *sna = new SimpleNodeArt (new Fluoroscope);
    sna->RefreshPathSeriesDisplays (net);

    NodeStore *ns = new NodeStore ();
    TopoWard *tw = new TopoWard (ns);

    Node *n = NULL;
    tw->ReadNode (NodeB, n);
    if (n) {
        float64 t = ns->SluiceTime ();
        VizNodeTemplate *vnt = sna->VizTemplateForNode (n);
        Str txt = n->HoverTextAtTime (t, vnt);
        EXPECT_TRUE (txt.Match ("\t1: b1"));
        EXPECT_FALSE (txt.Match ("\tb.1: b1"));
        EXPECT_TRUE (txt.Match ("\t2: b2"));
        EXPECT_FALSE (txt.Match ("\tb.2: b2"));
    }
}

TEST (NodeTypeConfigurations, HoverLocBlacklist) {
    SimpleNodeArt *sna = new SimpleNodeArt (new Fluoroscope);
    sna->RefreshNodeSeriesDisplays (net);

    NodeStore *ns = new NodeStore ();
    TopoWard *tw = new TopoWard (ns);

    Node *n = NULL;
    tw->ReadNode (NodeA, n);
    if (n) {
        float64 t = ns->SluiceTime ();
        VizNodeTemplate *vnt = sna->VizTemplateForNode (n);
        Str txt = n->HoverTextAtTime (t, vnt);
        EXPECT_FALSE (txt.Match ("a.3"));
        EXPECT_FALSE (txt.Match ("a3"));
        EXPECT_FALSE (txt.Match ("3"));
    }
}

TEST (NodeTypeConfigurations, HoverPathBlacklist) {
    SimpleNodeArt *sna = new SimpleNodeArt (new Fluoroscope);
    sna->RefreshPathSeriesDisplays (net);

    NodeStore *ns = new NodeStore ();
    TopoWard *tw = new TopoWard (ns);

    Node *n = NULL;
    tw->ReadNode (NodeB, n);
    if (n) {
        float64 t = ns->SluiceTime ();
        VizNodeTemplate *vnt = sna->VizTemplateForNode (n);
        Str txt = n->HoverTextAtTime (t, vnt);
        EXPECT_FALSE (txt.Match ("b.3"));
        EXPECT_FALSE (txt.Match ("b3"));
        EXPECT_FALSE (txt.Match ("3"));
    }
}
