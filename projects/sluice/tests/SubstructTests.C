#include <gtest/gtest.h>
#include <libPlasma/c++/Protein.h>

#include "../Subscription.h"
#include "../Javascript.h"
#include "../Inculcator.h"

using namespace oblong::sluice;
using namespace oblong::plasma;
using namespace conduce::plasma;

TEST (Substruct, StrVect) {
    std::vector<Str> strs = {"foo", "bar", "baz"};
    Slaw s{strs};
    std::vector<Str> results;
    EXPECT_TRUE (s.Into (results));
    ASSERT_EQ (results.size (), strs.size ());
    for (size_t i = 0; i < strs.size (); ++i) {
        EXPECT_EQ (strs[i], results[i]);
    }
}

TEST (Substruct, EmptyStrVect) {
    std::vector<Str> strs;
    Slaw s{strs};
    std::vector<Str> results;
    EXPECT_TRUE (s.Into (results));
    ASSERT_EQ (results.size (), strs.size ());
    for (size_t i = 0; i < strs.size (); ++i) {
        EXPECT_EQ (strs[i], results[i]);
    }
}

TEST (Substruct, SubCache) {
    SubscriptionCache orig;
    orig.bounds = {{1.1, 2.2}, {3.3, 4.4}};
    orig.has_bounds = true;
    orig.modified_time = 1.2;
    orig.start_time = 3.4;
    orig.end_time = 5.6;

    Slaw s{orig};
    SubscriptionCache result;
    EXPECT_TRUE (s.Into (result));
    EXPECT_EQ (orig.bounds, result.bounds);
    EXPECT_EQ (orig.has_bounds, result.has_bounds);
    EXPECT_EQ (orig.modified_time, result.modified_time);
    EXPECT_EQ (orig.start_time, result.start_time);
    EXPECT_EQ (orig.end_time, result.end_time);
}

TEST (Substruct, SubCacheNoBounds) {
    SubscriptionCache orig;
    orig.has_bounds = false;
    orig.modified_time = 1.2;
    orig.start_time = 3.4;
    orig.end_time = 5.6;

    Slaw s{orig};
    SubscriptionCache result;
    EXPECT_TRUE (s.Into (result));
    EXPECT_EQ (orig.has_bounds, result.has_bounds);
    EXPECT_EQ (orig.modified_time, result.modified_time);
    EXPECT_EQ (orig.start_time, result.start_time);
    EXPECT_EQ (orig.end_time, result.end_time);
}

TEST (Substruct, SubscriptionNoCache) {
    Subscription orig, result;
    orig.ident = Slaw::Map ("hello", "world", 4, Slaw::List (1.1, 2.2, 3.3));
    orig.bounds = {{1.1, 2.2}, {3.3, 4.4}};
    orig.current_time = 1415408345.000000;
    orig.time_rate = 0.99;
    orig.kinds = {"foo", "bar", "baz"};
    orig.attributes = {"one", "two", "3"};
    orig.binning.should_bin = true;
    orig.binning.granularity = 2.0;

    Slaw s{orig};

    EXPECT_TRUE (s.Into (result));

    EXPECT_TRUE (orig.ident == result.ident);
    EXPECT_EQ (orig.bounds, result.bounds);
    EXPECT_EQ (orig.current_time, result.current_time);
    EXPECT_EQ (orig.time_rate, result.time_rate);

    EXPECT_EQ (orig.kinds.size (), result.kinds.size ());
    for (size_t i = 0; i < orig.kinds.size (); ++i) {
        EXPECT_EQ (orig.kinds[i], result.kinds[i]);
    }

    EXPECT_EQ (orig.attributes.size (), result.attributes.size ());
    for (size_t i = 0; i < orig.attributes.size (); ++i) {
        EXPECT_EQ (orig.attributes[i], result.attributes[i]);
    }
    EXPECT_EQ (true, result.binning.should_bin);
    EXPECT_EQ (2.0, result.binning.granularity);
}

SubscriptionCache SC (GeoRect b, bool hb, float64 m, float64 s, float64 e) {
    SubscriptionCache out;
    out.bounds = b;
    out.has_bounds = hb;
    out.modified_time = m;
    out.start_time = s;
    out.end_time = e;
    return out;
}

TEST (Substruct, SubscriptionWithCache) {
    Subscription orig, result;
    orig.ident = Slaw::Map ("hello", "world", 4, Slaw::List (1.1, 2.2, 3.3));
    orig.bounds = {{1.1, 2.2}, {3.3, 4.4}};
    orig.visible_bounds = {{ 0.1, 2.0}, {3.8, 5.1}};
    orig.current_time = 1.5;
    orig.time_rate = 0.99;
    orig.kinds = {"foo", "bar", "baz"};
    orig.attributes = {"one", "two", "3"};

    orig.cache = std::vector<SubscriptionCache>{
        SC ({{1.1, 2.2}, {3.3, 4.4}}, true, 1.1, 2.2, 3.3),
        SC ({{2.2, 3.3}, {3.3, 4.4}}, true, 6.6, 7.7, 8.8),
        SC ({{1.1, 2.2}, {2.2, 1.1}}, false, 7.7, 8.8, 9.9)};

    Slaw s{orig};

    EXPECT_TRUE (s.Into (result));

    EXPECT_TRUE (orig.ident == result.ident);
    EXPECT_EQ (orig.bounds, result.bounds);
    EXPECT_EQ (orig.visible_bounds, result.visible_bounds);
    EXPECT_EQ (orig.current_time, result.current_time);
    EXPECT_EQ (orig.time_rate, result.time_rate);

    EXPECT_EQ (orig.kinds.size (), result.kinds.size ());
    for (size_t i = 0; i < orig.kinds.size (); ++i) {
        EXPECT_EQ (orig.kinds[i], result.kinds[i]);
    }

    EXPECT_EQ (orig.attributes.size (), result.attributes.size ());
    for (size_t i = 0; i < orig.attributes.size (); ++i) {
        EXPECT_EQ (orig.attributes[i], result.attributes[i]);
    }

    ASSERT_EQ (3, result.cache.size ());
    EXPECT_EQ (true, result.cache[0].has_bounds);
    EXPECT_EQ (true, result.cache[1].has_bounds);
    EXPECT_EQ (false, result.cache[2].has_bounds);
}

TEST (Substruct, SubscriptionNode) {
    SubscriptionNode orig, result;
    orig.ident = "foo";
    orig.kind = "bar";
    orig.attrs = {{"a", "b"}, {"c", "d"}};
    orig.path = {{1.1, 1.2, 1.3}, {2.1, 2.2, 2.3}, {3.1, 3.2, 3.3}};
    orig.timestamp = 1.2;
    orig.endtime = 3.4;

    Slaw s{orig};
    EXPECT_TRUE (s.Into (result));

    EXPECT_EQ (orig.ident, result.ident);
    EXPECT_EQ (orig.kind, result.kind);
    EXPECT_EQ (orig.attrs.size (), result.attrs.size ());

    for (size_t i = 0; i < orig.attrs.size (); ++i) {
        EXPECT_EQ (orig.attrs[i], result.attrs[i]);
    }

    EXPECT_EQ (orig.path.size (), result.path.size ());
    for (size_t i = 0; i < orig.path.size (); ++i) {
        auto a = orig.path[i];
        auto b = result.path[i];
        EXPECT_EQ (a.x, b.x);
        EXPECT_EQ (a.y, b.y);
        EXPECT_EQ (a.z, b.z);
    }
    EXPECT_EQ (orig.timestamp, result.timestamp);
    EXPECT_EQ (orig.endtime, result.endtime);
}

TEST (Substruct, SubscriptionDataWithKluge) {
    SubscriptionNode a;
    a.ident = "foo";
    a.kind = "bar";
    a.attrs = {{"a", "b"}, {"c", "d"}};
    a.path = {{1.1, 1.2, 1.3}, {2.1, 2.2, 2.3}, {3.1, 3.2, 3.3}};
    a.timestamp = 1.2;
    a.endtime = 3.4;

    SubscriptionData data, result;
    data.current_time = 5.5;
    data.modified_time = 6.6;
    std::vector<SubscriptionNode> add{a};
    data.add_ = SlawMake (a);
    data.remove_ = slaw_nil ();

    Protein pro (Slaw::List ("hello", "world"), Slaw{data});
    Slaw ing = pro.Ingests ();
    ing.SlawValue ();
    ASSERT_TRUE (ing.Into (result));
    EXPECT_EQ (data.current_time, result.current_time);
    EXPECT_EQ (data.modified_time, result.modified_time);
    EXPECT_EQ (data.addCount (), result.addCount ());
}

TEST (Substruct, SubscriptionDataWithoutKluge) {
    SubscriptionNode a;
    a.ident = "foo";
    a.kind = "bar";
    a.attrs = {{"a", "b"}, {"c", "d"}};
    a.path = {{1.1, 1.2, 1.3}, {2.1, 2.2, 2.3}, {3.1, 3.2, 3.3}};
    a.timestamp = 1.2;
    a.endtime = 3.4;

    SubscriptionData data, result;
    data.current_time = 5.5;
    data.modified_time = 6.6;
    std::vector<SubscriptionNode> add{a};
    data.add_ = SlawMake (a);
    data.remove_ = slaw_nil ();

    Protein pro (Slaw::List ("hello", "world"), Slaw{data});
    ASSERT_TRUE (SlawCoerce (pro.Ingests (), result));
}

TEST (Substruct, SubnodeJS) {
    auto &js = Javascript::Instance ();
    SubscriptionNode a;
    a.ident = "foo";
    a.kind = "bar";
    a.attrs = {{"a", "b"}, {"c", "d"}};
    a.path = {{1.1, 1.2, 1.3}, {2.1, 2.2, 2.3}, {3.1, 3.2, 3.3}};
    a.timestamp = 1.2;
    a.endtime = 3.4;

    bool result{false};
    ASSERT_TRUE (
        js.RunScriptWithNode ("'bar' == kind(n)", a, result).IsSplend ());
    EXPECT_TRUE (result);
    ASSERT_TRUE (
        js.RunScriptWithNode ("'qux' == kind(n)", a, result).IsSplend ());
    EXPECT_FALSE (result);

    ASSERT_TRUE (js.RunScriptWithNode ("'b' == observation(n, 'a', t)",
                                       a,
                                       result).IsSplend ());
    EXPECT_TRUE (result);

    ASSERT_TRUE (js.RunScriptWithNode ("'BEE' == observation(n, 'a', t)",
                                       a,
                                       result).IsSplend ());
    EXPECT_FALSE (result);

    ASSERT_TRUE (js.RunScriptWithNode ("'d' == observation(n, 'see', t)",
                                       a,
                                       result).IsSplend ());
    EXPECT_FALSE (result);
}

TEST (Substruct, TestInculcator) {
    ObTrove<Str> ks;
    ks.Append ("refinery");
    auto incul = new Inculcator{"refinery_inc", "refinery_icon", ks};

    auto &js = Javascript::Instance ();
    SubscriptionNode a;
    a.ident = "refinery-Al_Diwaniyah Refinery";
    a.kind = "refinery";
    a.attrs = {{"name", "Al_Diwaniyah Refinery"}};
    a.path = {{31.89, 44.88, 0.0}};
    a.timestamp = 0.0;

    EXPECT_TRUE (incul->Matches (a, js));
    a.kind = "something else";
    EXPECT_FALSE (incul->Matches (a, js));
}
