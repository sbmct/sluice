#include <gtest/gtest.h>
#define GTEST_KLUGE
#include "../NodeStoreImpl.h"
#include "../TopoWard.h"

#include <algorithm>

using namespace oblong::sluice;

::std::ostream &operator<<(::std::ostream &os, const ObRetort &tort) {
    return os << tort.Description ().utf8 ();
}

static void ADD_NODE (NodeStore *store, const float64 lat, const float64 lon) {
    static int64 counter = 0;
    Str ident = Str ().Sprintf ("Node: %" OB_FMT_64 "d", counter++);
    Node *n = new Node (ident, "testing");
    n->SetGeoloc (LatLon (lat, lon), 0.0);
    store->AddNode (n, 0.0);
}

class Hoser {
protected:
    ObRef<Hose *> h;

public:
    Hoser (const Str pool) {
        ObRetort tort;
        Hose *hh = Pool::Participate (pool, Pool::MMAP_MEDIUM, &tort, true);
        if (OB_OK == tort)
            h = hh;
    }

    ~Hoser () {
        Hose *hh = ~h;
        hh->Withdraw ();
        Pool::DisposeOfAutoDisposables ();
    }

    const bool good () const { return NULL != ~h; }

    Hose *hose () const { return ~h; }
};

typedef std::set<Node *> nodeset_t;

TEST (SpatialSearch, JustNodes) {
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());

    ADD_NODE (store, 0.0, 0.0);
    ADD_NODE (store, -1.0, -1.0);
    ADD_NODE (store, 1.0, 1.0);

    nodeset_t out;
    store->FindByLatLonRange (LatLon (-.5, -.5), LatLon (.5, .5), 180.0, out);
    EXPECT_EQ (size_t (1), out.size ());

    Node *n = *(out.begin ());
    ASSERT_NE ((Node *)NULL, n);
    EXPECT_TRUE (n->HasGeoloc (0.0));
    EXPECT_EQ (LatLon (0.0, 0.0), n->Geoloc (0.0));

    store->Delete ();
}

TEST (SpatialSearch, JustNodesAllIn) {
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());

    ADD_NODE (store, 0.0, 0.0);
    ADD_NODE (store, -1.0, -1.0);
    ADD_NODE (store, 1.0, 1.0);

    nodeset_t out;
    store->FindByLatLonRange (
        LatLon (-1.5, -1.5), LatLon (1.5, 1.5), 180.0, out);
    EXPECT_EQ (size_t (3), out.size ());

    store->Delete ();
}

TEST (SpatialSearch, PathEntirelyWithin) {
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());
    Node *n = new Node ("path", "path");

    ObTrove<LatLon> path;
    path.Append (LatLon (-0.25, -0.25));
    path.Append (LatLon (0.0, 0.0));
    path.Append (LatLon (0.25, 0.25));
    n->SetPath (path, 0.0);

    store->AddNode (n, 0.0);

    nodeset_t out;
    store->FindByLatLonRange (
        LatLon (-1.0, -1.0), LatLon (1.0, 1.0), 180.0, out);
    EXPECT_EQ (n, *(out.begin ()));

    store->Delete ();
}

TEST (SpatialSearch, PathPartiallyWithin) {
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());
    Node *n = new Node ("path", "path");

    ObTrove<LatLon> path;
    path.Append (LatLon (-10.25, -10.25));
    path.Append (LatLon (0.0, 0.0));
    path.Append (LatLon (0.25, 0.25));
    n->SetPath (path, 0.0);

    store->AddNode (n, 0.0);

    nodeset_t out;
    store->FindByLatLonRange (
        LatLon (-1.0, -1.0), LatLon (1.0, 1.0), 180.0, out);

    EXPECT_EQ (n, *(out.begin ()));
    store->Delete ();
}

TEST (SpatialSearch, PathPassingThrough) {
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());
    Node *n = new Node ("path", "path");

    ObTrove<LatLon> path;
    path.Append (LatLon (-0.25, -0.25));
    path.Append (LatLon (0.0, 0.0));
    path.Append (LatLon (0.25, 0.25));
    n->SetPath (path, 0.0);

    store->AddNode (n, 0.0);

    nodeset_t out;

    store->FindByLatLonRange (
        LatLon (0.09, 0.1), LatLon (0.2, 0.21), 180.0, out);
    EXPECT_EQ (n, *(out.begin ()));

    store->Delete ();
}

TEST (TimeMgmt, SetCurrentTime) {
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());

    EXPECT_TRUE (store->QueryOnLiveTime ());
    float64 now = NodeStore::NowGMT ();
    EXPECT_GE (now, store->SluiceTime ());

    store->SetSluiceTime (12345.0);

    EXPECT_FALSE (store->QueryOnLiveTime ());
    EXPECT_FLOAT_EQ (12345.0, store->SluiceTime ());

    store->UseLiveTime ();

    EXPECT_TRUE (store->QueryOnLiveTime ());
    now = NodeStore::NowGMT ();
    EXPECT_GE (now, store->SluiceTime ());

    store->Delete ();
}

TEST (TimeMgmt, NoopIfTimeIsTheSame) {
    Hoser hoser ("sluice-test-hose");
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());

    EXPECT_TRUE (store->QueryOnLiveTime ());
    EXPECT_EQ (OB_NOTHING_TO_DO, store->UseLiveTime ());

    EXPECT_NE (12345.0, store->SluiceTime ());
    EXPECT_EQ (OB_OK, store->SetSluiceTime (12345.0));
    EXPECT_FLOAT_EQ (12345.0, store->SluiceTime ());
    EXPECT_EQ (OB_NOTHING_TO_DO, store->SetSluiceTime (12345.0));

    EXPECT_EQ (OB_OK, store->UseLiveTime ());

    store->Delete ();
}

class HookHaver : public KneeObject {
    PATELLA_SUBCLASS (HookHaver, KneeObject);

public:
    int64 count;
    HookHaver () : count (0) {}

    ObRetort HookUpdated (NodeStore *ns, Node *n) {
        ++count;
        return OB_OK;
    }
};

TEST (TimeMgmt, NotifyIfTimeIsLive) {
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());
    HookHaver *have = new HookHaver;

    store->AppendNodeUpdatedHook (&HookHaver::HookUpdated, have, "for-testing");

    EXPECT_EQ (0, have->count);
    EXPECT_TRUE (store->QueryOnLiveTime ());

    Node *n = new Node ("foo", "bar");

    store->AddNode (n, 0.0);

    EXPECT_EQ (1, have->count);

    store->RemoveNodeUpdatedHook ("for-testing");
    have->Delete ();
    store->Delete ();
}

TEST (TimeMgmt, DoNotNotifyIfTimeIsNOTLive) {
    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());
    HookHaver *have = new HookHaver;

    store->AppendNodeUpdatedHook (&HookHaver::HookUpdated, have, "for-testing");

    store->SetSluiceTime (12345.0);

    EXPECT_EQ (0, have->count);
    EXPECT_FALSE (store->QueryOnLiveTime ());

    Node *n = new Node ("foo", "bar");

    store->AddNode (n, NodeStore::NowGMT ());

    EXPECT_EQ (0, have->count);

    store->RemoveNodeUpdatedHook ("for-testing");
    have->Delete ();
    store->Delete ();
}

#define EXPECT_NODE(n, lt, ln, ts)                                             \
    do {                                                                       \
        LatLon ll = n->Geoloc (ts);                                            \
        EXPECT_FLOAT_EQ (lt, ll.lat);                                          \
        EXPECT_FLOAT_EQ (ln, ll.lon);                                          \
    } while (false);

class UnsafeNodeStore : public NodeStoreImpl {
    PATELLA_SUBCLASS (UnsafeNodeStore, NodeStoreImpl);

public:
    void UpdateNode (Node *n, const float64 t) { super::UpdateNode (n, t); }
};

TEST (TimeTravel, SimpleAddAndRewindTest) {
    UnsafeNodeStore *store = new UnsafeNodeStore;
    store->SetConfigurationSlaw (Slaw ());

    float64 lat = 0.1, lon = 0.2, stamp = 12345.0;

    Node *n = new Node ("foo", "bar");

    for (int i = 0; i < 100; ++i) {
        n->SetGeoloc (LatLon (lat, lon), stamp);
        store->AddNode (n, stamp);
        lat += 1.0;
        lon += 1.0;
        stamp += 1.0;
    }

    EXPECT_NODE (n, 99.1, 99.2, 12345.0 + 99.0);

    EXPECT_EQ (OB_OK, store->SetSluiceTime (12345.5));
    EXPECT_NODE (n, 0.1, 0.2, 12345.0);

    EXPECT_EQ (OB_OK, store->SetSluiceTime (12355.5));
    EXPECT_NODE (n, 10.1, 10.2, 12355.0);

    store->Delete ();
}

TEST (TimeTravel, SimpleAddAndRewindTestWithSeveralNodes) {
    Hoser hoser ("sluice-test-hose");
    ASSERT_EQ (true, hoser.good ()) << "Create our hose";

    NodeStoreImpl *store = new NodeStoreImpl;
    store->SetConfigurationSlaw (Slaw ());

    float64 lat = 0.1, lon = 0.2, stamp = 12345.0;

    Node *n0 = new Node ("foo", "bar");
    Node *n1 = new Node ("baz", "bar");
    Node *n2 = new Node ("qux", "bar");

    for (int i = 0; i < 100; ++i) {
        n0->SetGeoloc (LatLon (lat, lon), stamp);
        store->AddNode (n0, stamp);

        if (0 == i % 10) {
            n1->SetGeoloc (LatLon (lat, lon), stamp);
            store->AddNode (n1, 0.0);
        }

        if (0 == i % 32) {
            n2->SetGeoloc (LatLon (lat, lon), stamp);
            store->AddNode (n2, 0.0);
        }

        lat += 1.0;
        lon += 1.0;
        stamp += 1.0;
    }

    EXPECT_NODE (n0, 99.1, 99.2, 12345.0 + 99.0);
    EXPECT_NODE (n1, 90.1, 90.2, 12345.0 + 90.0);
    EXPECT_NODE (n2, 96.1, 96.2, 12345.0 + 96.0);

    EXPECT_EQ (OB_OK, store->SetSluiceTime (12345.5));
    EXPECT_NODE (n0, 0.1, 0.2, 12345.0);
    EXPECT_NODE (n1, 0.1, 0.2, 12345.0);
    EXPECT_NODE (n2, 0.1, 0.2, 12345.0);

    EXPECT_EQ (OB_OK, store->SetSluiceTime (12355.5));
    EXPECT_NODE (n0, 10.1, 10.2, 12355.0);
    EXPECT_NODE (n1, 10.1, 10.2, 12355.0);
    EXPECT_NODE (n2, 0.1, 0.2, 12345.0);

    EXPECT_EQ (OB_OK, store->SetSluiceTime (12395.5));
    EXPECT_NODE (n0, 50.1, 50.2, 12395.0);
    EXPECT_NODE (n1, 50.1, 50.2, 12395.0);
    EXPECT_NODE (n2, 32.1, 32.2, 12377.0);

    EXPECT_EQ (OB_OK, store->SetSluiceTime (12397.5));
    EXPECT_NODE (n0, 52.1, 52.2, 12397.0);
    EXPECT_NODE (n1, 50.1, 50.2, 12395.0);
    EXPECT_NODE (n2, 32.1, 32.2, 12377.0);

    store->Delete ();
}
#if USE_OLD_BAD_TEST_WITH_FILESYSTEM

static const char *demo_files[] = {
    "/kipple/sluice/data/ge_demo_timed/Network_Model/Line_0007NMProtein.bin",
    "/kipple/sluice/data/ge_demo_timed/Network_Model/Line_0005NMProtein.bin",
    "/kipple/sluice/data/ge_demo_timed/Network_Model/Line_0006NMProtein.bin",
    "/kipple/sluice/data/ge_demo_timed/Network_Model/Line_0009NMProtein.bin",
    "/kipple/sluice/data/ge_demo_timed/Network_Model/Line_0002NMProtein.bin",
    "/kipple/sluice/data/ge_demo_timed/Network_Model/Line_0001NMProtein.bin",
    0};

TEST (NodeFindTests, TestFromDemoSlawWithZoomLevels) {
    NodeStoreImpl *ns = new NodeStoreImpl;
    ns->SetConfigurationSlaw (Slaw::Map (
        "zoom-levels",
        Slaw::List (Slaw::Map ("name",
                               "toplevel",
                               "visible",
                               180.0,
                               "group",
                               true,
                               "granularity",
                               1.0),
                    Slaw::Map ("name",
                               "regional",
                               "visible",
                               5.0,
                               "group",
                               true,
                               "granularity",
                               0.1),
                    Slaw::Map ("name", "everything", "visible", 0.25))));

    TopoWard *topo = new TopoWard (ns);
    ObRef<NodeStore *> mort (ns);
    ObRef<TopoWard *> tmort (topo);

    ObRetort err;
    for (int c = 0; demo_files[c]; ++c) {
        const char *demo_file = demo_files[c];
        Slaw prot_slaw = Slaw::FromFile (Str (demo_file), &err);
        ASSERT_EQ (true, err.IsSplend ());
        ASSERT_EQ (true, prot_slaw.IsProtein ());
        Protein p (prot_slaw);
        topo->AppendToPile (p.Ingests ());
    }

    ASSERT_NE (0, topo->the_pile.Count ());
    topo->FloodTheDeck ();

    ASSERT_NE (0, ns->NodeCount ());

    std::set<Node *> all;
    ns->FindByLatLonRange (
        LatLon (24.2132, -118.081), LatLon (57.14, -58.0813), 46.3688, all);

    ASSERT_EQ (3, int64 (all.size ()));

    int64 geo_count = 0;
    GeoRect world (-90.0, -180.0, 90.0, 180.0);
    std::set<Node *>::const_iterator iter = all.begin ();
    while (iter != all.end ()) {
        Node *n = (*iter++);
        if (n->HasGeoloc (0.0)) {
            ++geo_count;
            LatLon ll = n->Geoloc (0.0);
            ASSERT_EQ (true, world.Contains (ll));
        }
    }
    ASSERT_NE (0, geo_count);

    LatLon bl (-90.0, -180.0), tr (90.0, 180.0);
    // LatLon bl (40.0, -90.0), tr (41.0, -80.0);

    nodeset_t nodes;
    ns->FindByLatLonRange (bl, tr, 180.0, nodes);

    ASSERT_NE (size_t (0), nodes.size ());
    ASSERT_EQ (size_t (3), nodes.size ());
}

TEST (NodeFindTests, TestFromDemoSlawDefault) {
    NodeStoreImpl *ns = new NodeStoreImpl;
    ns->SetConfigurationSlaw (Slaw ());
    TopoWard *topo = new TopoWard (ns);
    ObRef<NodeStore *> mort (ns);
    ObRef<TopoWard *> tmort (topo);

    ObRetort err;
    for (int c = 0; demo_files[c]; ++c) {
        const char *demo_file = demo_files[c];
        Slaw prot_slaw = Slaw::FromFile (Str (demo_file), &err);
        ASSERT_EQ (true, err.IsSplend ());
        ASSERT_EQ (true, prot_slaw.IsProtein ());
        Protein p (prot_slaw);
        topo->AppendToPile (p.Ingests ());
    }

    ASSERT_NE (0, topo->the_pile.Count ());
    topo->FloodTheDeck ();

    ASSERT_NE (0, ns->NodeCount ());

    std::set<Node *> all;
    ns->FindByLatLonRange (
        LatLon (-90.0, -180.0), LatLon (90.0, 180.0), 180.0, all);

    ASSERT_EQ (ns->NodeCount (), int64 (all.size ()));

    int64 geo_count = 0;
    GeoRect world (-90.0, -180.0, 90.0, 180.0);
    std::set<Node *>::const_iterator iter = all.begin ();
    while (iter != all.end ()) {
        Node *n = (*iter++);
        if (n->HasGeoloc (0.0)) {
            ++geo_count;
            LatLon ll = n->Geoloc (0.0);
            ASSERT_EQ (true, world.Contains (ll));
        }
    }
    ASSERT_NE (0, geo_count);

    LatLon bl (-90.0, -180.0), tr (90.0, 180.0);
    // LatLon bl (40.0, -90.0), tr (41.0, -80.0);

    nodeset_t nodes;
    ns->FindByLatLonRange (bl, tr, 180.0, nodes);

    ASSERT_NE (size_t (0), nodes.size ());

    ASSERT_EQ (size_t (ns->NodeCount ()), nodes.size ());
}

#endif // USE_OLD_BAD_TEST_WITH_FILESYSTEM
