#include <gtest/gtest.h>
#include "../Fluoroscope.h"
#include <libLoam/c++/ObTrove.h>

using namespace oblong::loam;
using namespace oblong::sluice;

v2float64 Stuff (Fluoroscope::Pin p) { return p.UV (); }

TEST (PinCorruption, TwoPins) {
    ObTrove<Fluoroscope::Pin> pins;

    pins.Append (Fluoroscope::Pin (0.5, 0.5, 1.0, 1.0));
    pins.Append (Fluoroscope::Pin (0.4, 0.4, 1.1, 1.1));

    for (int i = 0; i < pins.Count (); ++i) {
        Fluoroscope::Pin pin = pins.Nth (i);
        v2float64 uv = pin.UV ();
        printf ("%f\t%f\n", uv.x, uv.y);
    }

    int i = 0;
    ObCrawl<Fluoroscope::Pin> cr = pins.Crawl ();
    while (!cr.isempty ()) {
        v2float64 uv = Stuff (cr.popfore ());
        if (0 == i++) {
            EXPECT_EQ (0.5, uv.x);
            EXPECT_EQ (0.5, uv.y);
        } else {
            EXPECT_EQ (0.4, uv.x);
            EXPECT_EQ (0.4, uv.y);
        }
    }
}
