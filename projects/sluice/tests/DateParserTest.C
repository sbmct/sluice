
#include <gtest/gtest.h>

#include <libLoam/c++/LoamStreams.h>
#include "../DateParser.h"

using namespace oblong::sluice;

TEST (DateParser, TestDefaultFormat) {
    DateParser parser;
    float64 when;
    EXPECT_EQ (OB_OK, parser.Parse ("12345.0", when));
    EXPECT_FLOAT_EQ (12345.0, when);
}

TEST (DateParser, TestYMDStuff) {
    DateParser parser;
    parser.AppendDateFormat ("%Y-%m-%d %H:%M:%S");
    float64 when;
    EXPECT_EQ (OB_OK, parser.Parse ("2011-11-14 19:12:21", when));
    const float64 expected = 1321297941.0;
    EXPECT_FLOAT_EQ (expected, when);
    EXPECT_FLOAT_EQ (0, when - expected);
}

TEST (DateParser, TestJavaStringStuff) {
    DateParser parser;
    parser.AppendDateFormat ("%Y-%m-%dT%H:%M:%SZ");
    float64 when;
    EXPECT_EQ (OB_OK, parser.Parse ("2011-09-18T09:49:47Z", when));
    const float64 expected = 1316339387.0;
    EXPECT_FLOAT_EQ (expected, when);
    EXPECT_FLOAT_EQ (0, when - expected);
}

TEST (DateParser, TestTimeZones) {
    DateParser parser;
    float64 utc{-1.0}, pst{-1.0}, sri_lanka{-1.0};

    ASSERT_EQ (OB_OK, parser.Parse ("1970-01-01 00:00:00 GMT+0", utc));
    EXPECT_EQ (0.0, utc);
    ASSERT_EQ (OB_OK, parser.Parse ("1970-01-01 00:00:00 UTC-0", utc));
    EXPECT_EQ (0.0, utc);
    ASSERT_EQ (OB_OK, parser.Parse ("1970-01-01 00:00:00 UTC+05:30", sri_lanka));
    EXPECT_EQ (-19800.0, sri_lanka);
    ASSERT_EQ (OB_OK, parser.Parse ("1970-01-01 00:00:00 PST-8", pst));
    EXPECT_EQ (28800.0, pst);
}
