
/* (c)  oblong industries */

#ifndef MEZZANINE_FLATE_EVENT
#define MEZZANINE_FLATE_EVENT

#include <libImpetus/OEPointing.h>

#include <libGanglia/ElectricalEvent.h>

namespace oblong {
namespace sluice {
using namespace oblong::impetus;
using namespace oblong::ganglia;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class MzFlateEvent : public ElectricalEvent {
    PATELLA_SUBCLASS (MzFlateEvent, ElectricalEvent);
    OE_MISCY_MAKER (MzFlate, Electrical, "flate");

    // TODO:
    // consolidate navigate to waddle/flate
    // add intent to waddle/flate
    // add hooks to intent change in handipoints
private:
    Str intent;
    v2float64 original_size;
    float64 suggested_scale;
    KneeObject *target;

public:
    MzFlateEvent (KneeObject *orig_ko = NULL)
        : ElectricalEvent (orig_ko), target (NULL) {}

    const Str &Intent () const;
    void SetIntent (const Str &ent);

    const v2float64 &OriginalSize () const;
    const float64 &SuggestedScale () const;

    KneeObject *Target () const;
    void SetTarget (KneeObject *ft);

    OEPointingEvent *RawOEPointingEvent () const;
    void SetRawOEPointingEvent (OEPointingEvent *pe);

    void SetOriginalSize (const float64 &wid, const float64 &hei);
    void SetOriginalSize (const v2float64 size);
    void SetSuggestedScale (const float64 &scale);

    virtual void SynthesizeInternalProtein ();
    virtual ObRetort AnalyzeInternalProtein ();
};

class MzFlateBeginEvent : public MzFlateEvent {
    PATELLA_SUBCLASS (MzFlateBeginEvent, MzFlateEvent);
    OE_MISCY_MAKER (MzFlateBegin, MzFlate, "begin");
    MzFlateBeginEvent (KneeObject *orig_ko = NULL) : MzFlateEvent (orig_ko) {}
};

class MzFlateContinueEvent : public MzFlateEvent {
    PATELLA_SUBCLASS (MzFlateContinueEvent, MzFlateEvent);
    OE_MISCY_MAKER (MzFlateContinue, MzFlate, "continue");
    MzFlateContinueEvent (KneeObject *orig_ko = NULL)
        : MzFlateEvent (orig_ko) {}
};

class MzFlateEndEvent : public MzFlateEvent {
    PATELLA_SUBCLASS (MzFlateEndEvent, MzFlateEvent);
    OE_MISCY_MAKER (MzFlateEnd, MzFlate, "end");
    MzFlateEndEvent (KneeObject *orig_ko = NULL) : MzFlateEvent (orig_ko) {}
};

class MzFlateAbortEvent : public MzFlateEvent {
    PATELLA_SUBCLASS (MzFlateAbortEvent, MzFlateEvent);
    OE_MISCY_MAKER (MzFlateAbort, MzFlate, "continue");
    MzFlateAbortEvent (KneeObject *orig_ko = NULL) : MzFlateEvent (orig_ko) {}
};

class MzFlateEventAcceptorGroup
    : public MzFlateBeginEvent::MzFlateBeginAcceptor,
      public MzFlateContinueEvent::MzFlateContinueAcceptor,
      public MzFlateEndEvent::MzFlateEndAcceptor,
      public MzFlateAbortEvent::MzFlateAbortAcceptor {};

class MzFlate {
public:
    typedef MzFlateEventAcceptorGroup Evts;
};
}
} // a bittersweet end for namespaces sluice and oblong

DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED (oblong::sluice::MzFlate::Evts);

#endif
