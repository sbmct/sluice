
#pragma once

#include <libLoam/c++/Str.h>

namespace oblong {
namespace sluice {

using oblong::loam::Str;
using oblong::loam::ObRetort;

/**
 * Read the character contents of a file
 *
 */

#define SLUICE_INSTALL_PATH "/opt/oblong/sluice-64-2/"

class SluiceFile {
public:
    /** Static methods for retrieving configuration and resource files via
     *  Sluice's standard method, which is to first look in the user defined
     *  ob_etc_path or ob_share_path and then look locally for an etc or share
     *  folder if no file was found in the user defined folders
     */

    /**
     * Find a configuration file
     *
     * Configurations should be saved directly in the etc folder. However, if
     * this is not the case you can use the prefix form of FindEtcFile to
     *override
     * this assumption
     *
     * Search order:
     * - $OB_ETC_PATH if it is defined
     * - $GSPEAK_DIR/etc if it is defined
     * - /etc/oblong
     * - ./etc
     * - /opt/oblong/sluice-64-2/etc
     */
    static Str FindEtcFile (Str filename) {
        return FindEtcFile (filename, Str ());
    }
    static Str FindEtcFile (Str filename, Str prefix);

    /**
     * Find a resource file
     *
     * Resources should be saved in a sluice folder within the shared resources
     * However, if this is not the case you can use the prefix form of
     * FindResourceFile to override this assumption.
     *
     * In the full version of the function you can suppress messages
     * if you expect to not find the file sometimes.
     *
     * Search order:
     * - $OB_SHARE_PATH if it is defined
     * - $GSPEAK_DIR/share if it is defined
     * - /usr/share/oblong
     */
    static Str FindResourceFile (Str filename) {
        return FindResourceFile (filename, "");
    }
    static Str FindResourceFile (Str filename, Str prefix, bool quiet = true);

    /**
     * Find a variable file
     *
     * Configurations should be saved directly in the var folder. However, if
     * this is not the case you can use the prefix form of FindVarFile to
     *override
     * this assumption
     *
     * Search order:
     * - $OB_VAR_PATH if it is defined
     * - $GSPEAK_DIR/var if it is defined
     * - /var/ob
     */
    static Str FindVarFile (Str filename) {
        return FindVarFile (filename, Str ());
    }
    static Str FindVarFile (Str filename, Str prefix);

    /**
     * Find a resource folder
     *
     * Same as FindResourceFile but we're looking for a folder
     *
     * Search order:
     * - $OB_SHARE_PATH if it is defined
     * - $GSPEAK_DIR/share if it is defined
     * - /usr/share/oblong
     */
    static Str FindResourceFolder (Str foldername) {
        return FindResourceFolder (foldername, "");
    }
    static Str FindResourceFolder (Str foldername, Str prefix);

    /**
     * Find a var folder
     *
     * Same as FindVarFile but we're looking for a folder
     *
     * Search order:
     * - $OB_VAR_PATH if it is defined
     * - $GSPEAK_DIR/var if it is defined
     * - /var/ob
     */
    static Str FindVarFolder (Str foldername) {
        return FindVarFolder (foldername, "");
    }
    static Str FindVarFolder (Str foldername, Str prefix);

    static ObRetort ReadFileContents (const Str &full_path, Str &output);
};
}
}
