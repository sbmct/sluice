
#pragma once

#include "Shader.h"
#include "GLQuad.h"
#include "NodeStore.h"
#include "TopoPigment.h"
#include "PigmentSack.h"
#include "NodeBuffer.h"

#include <map>
#include <set>
#include <libNoodoo/GLTex.h>
#include <libPlasma/c/slaw.h>

namespace oblong {
namespace sluice {

class Node;
class PigmentSack;

/** A GLQuad that renders nodes, OpenGL-style */
class IntStoreQuad : public GLQuad {
    PATELLA_SUBCLASS (IntStoreQuad, GLQuad);

protected:
    typedef std::map<unt32, PigmentSack *> pigmap_t;
    ObRef<ShaderStash *> stash;
    pigmap_t pigments;

    PigmentSack *FindPig (const unt32 id) const;

    ObRef<NodeStore *> node_store;

    std::map<Node *, std::vector<unt32>> existing_mappings;

    size_t AddVerts (Node &node,
                     const topo::Pigment &pig,
                     conduce::sluice::PigmentBuffer &buf,
                     const float64 t);
    int NumPigs ();
    size_t PigmentNodeBufferToVerts (const int pigs_i,
                                     PigmentSack &pig,
                                     const float64 t);
    void SetData (const float64 t);

    void depositRenderingInfo ();

public:
    IntStoreQuad ();

    bool NodeIsShowing (Node *) const;

    void SetNodeStore (NodeStore *);

    void HardReset ();
    void RemoveNode (Node *n);

    void SetPigmentsForNode (Node *n, std::vector<unt32> &ps);
    void PopulatePigmentsFromSlaw (const Str pigment);

    void AddPigment (const Str name, const Slaw s);
    void RemovePigment (const Str name);

    const float64 CurrentZoom () const { return current_zoom; }
    virtual void SetCurrentZoom (float64 x) override;

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

    Slaw DescribePigment (const Str &pigname) const;
};
}
}
