
/* (c)  oblong industries */

#ifdef PERFORMANCE

#ifndef FPS_H_
#define FPS_H_

#include <libNoodoo/LocusThing.h>
#include <libNoodoo/GlyphoString.h>
#include <libNoodoo/ObStyle.h>
#include <libNoodoo/VidQuad.h>
#include <libLoam/c++/FatherTime.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::loam;

class PerfLogger : public AnkleObject {
    PATELLA_SUBCLASS (PerfLogger, AnkleObject);

public:
    static ObRef<PerfLogger *> instance;
    static FILE *file;
    static Str file_path;

    PerfLogger ();
    ~PerfLogger ();
    static PerfLogger *Instance ();

    bool OpenFile ();
    static void Log (const Str &name, const Str &contents);
    static void SetFile (FILE *f);
};

/*
 * FPSThing can be appended to a respiring tree to compute the frames-per-second
 * for that tree. Optionally, it can also be asked to display this count as a
 * glypho
 */
class FPSThing : public LocusThing {
    PATELLA_SUBCLASS (FPSThing, LocusThing);

public:
    FatherTime timer;
    int64 counter;
    ObRef<GlyphoString *> fps_string;
    float64 fps;

    static int64 fps_id;
    static float64 interval;
    static bool show_fps;

    FPSThing (const Str &name);

    ObRetort Travail (Atmosphere *atm);

    ObRetort AcknowledgeLocationChange ();
    ObRetort AcknowledgeOrientationChange ();
};

/*
 * VidFPS is very similar to FPSThing except it can request a vid_quad for its
 * fps values and report them appropriately
 */
class VidFPS : public FPSThing {
    PATELLA_SUBCLASS (VidFPS, FPSThing);

public:
    ObRef<VidQuad *> vid_quad;

    VidFPS (const Str &name, const ObRef<VidQuad *> video);
    ObRetort Travail (Atmosphere *atm);
};
}
}

#endif

#endif
