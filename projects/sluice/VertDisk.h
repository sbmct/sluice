
/* (c)  oblong industries */

#ifndef VERT_DISK_THUMBS_ITS_NOSE_AT_EFFICIENCY
#define VERT_DISK_THUMBS_ITS_NOSE_AT_EFFICIENCY

#include <libBasement/TWrangler.h>
#include <libBasement/SoftFloat.h>
#include <libBasement/SoftColor.h>

#include <libNoodoo/LocusThing.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::loam;

class VertDisk : public LocusThing {
    PATELLA_SUBCLASS (VertDisk, LocusThing);

private:
    GLUquadric *quadric_disk;
    int32 num_slices, num_rings;

    SOFT_MACHINERY (SoftColor, StrokeColor);
    SOFT_MACHINERY (SoftColor, FillColor);
    SOFT_MACHINERY (SoftFloat, OuterRadius);
    SOFT_MACHINERY (SoftFloat, InnerRadius);

    TWrangler *loc_wrang;

public:
    VertDisk ();

    void SetOuterRadius (const float64 &mm);
    void SetInnerRadius (const float64 &mm);
    virtual void InstallLoc (SoftVect *sv);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);
};
}
}

#endif // VERT_DISK_THUMBS_ITS_NOSE_AT_EFFICIENCY
