
/* (c)  oblong industries */

#include <libLoam/c/ob-log.h>

#include "Windshield.h"
#include "SluiceRetorts.h"

#include "Regulator.h"
#include "SluiceSettings.h"
#include "VideoThing.h"
#include "YaroSneeze.h"

#include <libBasement/AsympColor.h>

#include <projects/event-slurper/TaggedVidQuad.h>

using namespace oblong::sluice;
using namespace oblong::remote;
using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

Windshield::Windshield () : Acetate (), booger (NULL), asset_bucket (NULL) {
    element_uid_source.SetPrefix ("el-");
    element_uid_source.SetNumDigits (6);
    element_uid_source.SetMostRecentlyDribbledUID (0);
}

Windshield::~Windshield () { ACETOUT (); }

static Str el_uid_str = "element-uid";
static Str cnt_src_str = "content-source";

Slaw Windshield::WindshieldDigest () const {
    ShowyThing *ket = OurAssetBucket ();
    if (!ket) {
        OB_LOG_WARNING ("Windshield::WindshieldDigest -- no assetbucket...");
        return Slaw ();
    }
    Str el_uid;
    Str ast_uid;
    Slaw el_map = Slaw::Map ();
    ObCrawl<KneeObject *> cr = ket->ChildCrawl ();
    while (!cr.isempty ())
        if (FlatThing *ft = dynamic_cast<FlatThing *> (cr.popfore ())) {
            if (!ft->FindWhatnot (el_uid_str).Into (el_uid) ||
                !ft->FindWhatnot (cnt_src_str).Into (ast_uid))
                continue;
            el_map = el_map.MapPut (
                el_uid, Slaw::List (ast_uid, ft->LocGoalVal (),
                                    ft->WidthGoalVal (), ft->HeightGoalVal ()));
        }
    return el_map;
}

void Windshield::InitiallyUnfurl (Atmosphere *atm) {
    ShowyThing *st = new ShowyThing;

    asset_bucket = st;

    AsympColor *ac = new AsympColor;
    ac->SetInterpTime (.5);
    ac->SetHard (ObColor (1.0, 1.0));
    st->InstallAdjColor (ac);
    InsertChildAt (st, 0);
}

void Windshield::InstallShoveReporter (BoogReporter *br) {
    if (~booger)
        RemoveChild (~booger);
    br->SetParent (this);
    booger = br;
    AppendChild (~booger);
}

void Windshield::InstallSystemArea (SystemArea *sa) {
    if (SystemArea *old_sys = ~sys_area)
        RemoveChild (old_sys);
    if (sa) {
        sa->SetParent (this);
        sys_area = sa;
        AppendChild (~sys_area);
    }
}

void Windshield::ArrangeDenizens () {
    ObRef<FeldGeomPod *> gp_ref = RetrieveGeometry ();
    Vect o = Over ();
    Vect u = Up ();

    if (FeldGeomPod *gp = ~gp_ref) {
        if (SystemArea *sa = ~sys_area) {
            sa->SetLage (gp->cent + (gp->width + gp->mullion_width) * o -
                         (gp->height * 0.405) * u);
            sa->SetSize (gp->width, 0.20 * gp->height);
            sa->ArrangeDenizens ();
        }
    }
}

FlatThing *Windshield::TopmostIntersectedOccupant (const Vect &from_v,
                                                   const Vect &to_v) {
    if (ShowyThing *st = ~asset_bucket) {
        ObCrawl<KneeObject *> cr = st->ChildCrawl ();
        while (!cr.isempty ())
            if (FlatThing *ft = dynamic_cast<FlatThing *> (cr.popaft ()))
                if (ft->WhackCheck (from_v, to_v, NULL, NULL, NULL))
                    if (!ft->IsInclusiveDescendentOf ("Tweezers"))
                        return ft;
    }
    return NULL;
}

int64 Windshield::RemoveAllOccupants () {
    ShowyThing *bk = OurAssetBucket ();
    if (!bk)
        return -1;
    int64 cnt = bk->ChildCount ();
    for (int64 q = cnt - 1; q >= 0; q--)
        if (FlatThing *ft = dynamic_cast<FlatThing *> (bk->NthChild (q))) {
            if (hoveree_by_provenance.ValIsPresent (ft))
                hoveree_by_provenance.RemoveByVal (ft);
            if (manipulee_by_provenance.ValIsPresent (ft))
                manipulee_by_provenance.RemoveByVal (ft);
            if (regulee_by_provenance.ValIsPresent (ft))
                regulee_by_provenance.RemoveByVal (ft);
            bk->RemoveChildAt (q);
        }
    //  bk -> RemoveAllChildren ();
    return cnt;
}

ObRetort Windshield::AppendElementFromDir (const Str &ds_dir,
                                           const Str &ast_uid,
                                           const Str &el_uid,
                                           const Vect &loc,
                                           float64 wid,
                                           float64 hei) {
    ShowyThing *bk = OurAssetBucket ();
    if (!bk) {
        OB_LOG_WARNING (
            "Windshield::AppendElementFromDir -- whoa! no asset bucket!");
        return OB_UNKNOWN_ERR;
    }
    if (ds_dir.IsNull () || ast_uid.IsNull ()) {
        OB_LOG_WARNING (
            "Windshield::AppendElementFromDir -- here's the thing: neither "
            "the dossier directory nor the asset uid may be null. and yet...");
        return OB_ARGUMENT_WAS_NULL;
    }
    Str ast_uid_cpy (ast_uid); // dammit!
    if (ast_uid_cpy.Match ("^as-")) {
        Str path = ds_dir + "/assets/" + ast_uid + "/img-conf.png";
        ImageClot *ic = ImageClot::FindOrLoadBySourceName (path);
        if (!ic) {
            OB_LOG_WARNING (
                "Windshield::AppendElementFromDir -- could not load "
                "image from %s",
                path.utf8 ());
            return OB_SLUICE_DISK_ACCESS_BADTH;
        }
        TexQuad *tq = new TexQuad (ic);
        tq->AppendWhatnot (el_uid_str, Slaw (el_uid));
        tq->AppendWhatnot (cnt_src_str, Slaw (ast_uid));
        tq->SetLocHard (loc);
        tq->SetSizeHard (wid, hei);
        bk->AppendChild (tq);
    } else if (ast_uid_cpy.Match ("^vi-")) {
        if (YaroSneeze *ys = ClosestParent<YaroSneeze> ())
            if (Dossier *doss = ys->OurDossier ()) {
                VideoThing *vt = NULL;

                // First try to see if the video tag is a slot index
                int64 slot = -1;
                if (sscanf (ast_uid.utf8 (), "vi-%" OB_FMT_64 "d", &slot))
                    vt = doss->NewVideoFromHobokenSlot (slot);

                // Then try to see if it's a viddle name
                if (vt == NULL) {
                    char viddle_name[32];
                    if (sscanf (ast_uid.utf8 (), "vi-%s", viddle_name))
                        vt = doss->NewVideoFromViddleName (Str (viddle_name));
                }

                if (vt) {
                    vt->AppendWhatnot (el_uid_str, Slaw (el_uid));
                    vt->AppendWhatnot (cnt_src_str, Slaw (ast_uid));
                    vt->SetLocHard (loc);
                    vt->SetSizeHard (wid, hei);
                    vt->ReInscribe ();
                    bk->AppendChild (vt);
                }

                if (vt == NULL) {
                    OB_LOG_WARNING (
                        "Windshield::AppendElementFromDir -- unknown video %s",
                        ast_uid.utf8 ());
                    return OB_UNKNOWN_ERR;
                }
            }
    } else {
        OB_LOG_WARNING (
            "Windshield::AppendElementFromDir -- unknown asset ilk. "
            "here, see for yourself: %s",
            ast_uid.utf8 ());
        return OB_UNKNOWN_ERR;
    }
    element_uid_source.ConditionallyUpdateMostRecentlyDribbledUID (el_uid);
    return OB_OK;
}

ObRetort Windshield::PassMzHandiEventsThrough (MzHandiEvent *e,
                                               Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    OEPointingEvent *oe = e->RawOEPointingEvent ();
    FlatThing *ft =
        TopmostIntersectedOccupant (oe->PhysOrigin (), oe->PhysThrough ());
    if (ft) {
        if (TaggedVidQuad *tvq = dynamic_cast<TaggedVidQuad *> (ft))
            e->ProfferTo (tvq, atm);
        else if (VideoThing *videoThing = dynamic_cast<VideoThing *> (ft))
            e->ProfferTo (videoThing, atm);
        else if (WebFluoroscope *webFluoro =
                     dynamic_cast<WebFluoroscope *> (ft))
            return DescentSingleTarget (webFluoro);

        return OB_EE_EXCLUSIVE_TERMINAL_CONNECTION;
    }

    return OB_EE_TERMINAL_CONNECTION;
}

ObRetort Windshield::RemoveExoskeletonIfStale (FlatThing *ft) {
    if (!(hoveree_by_provenance.ValIsPresent (ft) ||
          manipulee_by_provenance.ValIsPresent (ft) ||
          regulee_by_provenance.ValIsPresent (
              ft))) { // no one else is hovering, either. lose it.
        Exoskeleton *old_exo = exoskeleton_by_ft.Find (ft);
        if (old_exo) {
            exoskeleton_by_ft.Remove (ft);
            RemoveChild (old_exo);
        }
    }
    return OB_OK;
}

bool Windshield::RemoveAllExoskeletonedFluoroscopes () {
    bool ret = false;
    typedef ObMap<FlatThing *, Exoskeleton *, WeakRef, WeakRef>::MapCons cons_t;
    ObTrove<FlatThing *> rm;
    ObCrawl<cons_t *> cr = exoskeleton_by_ft.Crawl ();
    while (!cr.isempty ()) {
        cons_t *cons = cr.popfore ();
        if (FlatThing *f = cons->Car ()) {
            manipulee_by_provenance.RemoveByVal (f);
            regulee_by_provenance.RemoveByVal (f);
            hoveree_by_provenance.RemoveByVal (f);
            rm.Append (f);
            ret = true;
        }
    }
    ObCrawl<FlatThing *> cr2 = rm.Crawl ();
    while (!cr2.isempty ()) {
        FlatThing *f = cr2.popfore ();
        RemoveExoskeletonIfStale (f);
        RemoveChild (f);
    }
    return ret;
}

void Windshield::FadeAssets () {
    if (ShowyThing *st = ~asset_bucket) { // OB_LOG_DEBUG ("fade assets");
        st->SetAdjColor (ObColor (1.0, 0.5));
    }
}

void Windshield::BrightenAssets () {
    if (ShowyThing *st = ~asset_bucket)
        st->SetAdjColor (ObColor (1.0, 1.0));
}

void Windshield::SetAssetOpacity (float64 opaq) {
    if (ShowyThing *st = ~asset_bucket)
        st->SetAdjColor (ObColor (1.0, opaq));
}

void Windshield::SetAssetOpacityHard (float64 opaq) {
    if (ShowyThing *st = ~asset_bucket)
        st->SetAdjColorHard (ObColor (1.0, opaq));
}

void Windshield::SetAssetsVisibility (bool show) {
    if (ShowyThing *st = ~asset_bucket) {
        st->SetShouldDraw (show);
        st->SetShouldDrawChildrenEvenIfNotSelf (show);
    }
}

bool Windshield::IsFull () {
    if (NestyThing *bucket = (~asset_bucket)) {
        int64 max = SluiceSettings::SharedSettings ()->WindshieldMaxItems ();
        return (bucket->ChildCount () >= max);
    }
    // If the asset bucket doesn't exist, then we probably shouldn't allow
    // items to be deposited, hence full
    return true;
}

ObRetort Windshield::OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (e->Utterance () == "e" || e->Utterance () == "\b")
        if (RemoveAllExoskeletonedFluoroscopes ())
            return OB_EVENT_ABORT_DISTRIBUTION;

    if (e->Utterance () == "^") {
        auto drome = UrDrome::OutermostDrome ();
        drome->SetContinueRespiring (false);
    }

    return OB_OK;
}

ObRetort Windshield::MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    const Str &prv = e->Provenance ();
    OEPointingEvent *oe = e->RawOEPointingEvent ();
    const Str &tent = e->Intent ();

    if (tent == handi_pass) // passthrough")
        return PassMzHandiEventsThrough (e, atm);

    if (tent != "pointing")
        return OB_EE_NO_ROUTE; // for the moment, only deal with pointing.

    // check if some child object is using the event, and if so,
    // defer just let the child do its thing.
    FlatThing *man_ft = manipulee_by_provenance.Find (prv);
    if (man_ft)
        return OB_OK;

    if (!oe)
        return OB_EE_NO_ROUTE;

    FlatThing *old_hoveree = hoveree_by_provenance.Find (prv);

    // next up: check SystemArea. real case won't be this simple, but
    // we're just getting started. I think the thing to do is call
    // IsInteractable or something like it with some pointing data; we'd
    // like to give the button an opportunity to unhighlight, for example.
    if (SystemArea *sysa = ~sys_area)
        if (sysa->WhackCheck (oe->PhysOrigin (), oe->PhysThrough (), NULL, NULL,
                              NULL)) {
            hoveree_by_provenance.Remove (prv);
            if (old_hoveree)
                RemoveExoskeletonIfStale (old_hoveree);
            return DescentSingleTarget (sysa);
        } else
            sysa->DissociateProvenance (prv);

    FlatThing *hov_ft =
        TopmostIntersectedOccupant (oe->PhysOrigin (), oe->PhysThrough ());
    if (old_hoveree && old_hoveree != hov_ft) { // remove old hoveree and remove
                                                // its exoskeleton, if need be.
        hoveree_by_provenance.Remove (prv);
        RemoveExoskeletonIfStale (old_hoveree);
        // if there's nothing at all, we're done here.
        if (!hov_ft)
            return OB_EE_NO_ROUTE;
    }

    if (hov_ft) { // a brand new hoveree! take care of it and make it an
                  // exoskeleton,
        // but only if it doesn't have one already (say, because someone else is
        // pointing at or manipulating the same object).
        hoveree_by_provenance.Put (prv, hov_ft);
        if (!(manipulee_by_provenance.ValIsPresent (hov_ft) ||
              regulee_by_provenance.ValIsPresent (hov_ft)) &&
            !exoskeleton_by_ft.Find (hov_ft)) {
            Exoskeleton *exo = Exoskeleton::NewCustomFittedFor (hov_ft);
            exo->SetParent (this);
            if (Fluoroscope *f = dynamic_cast<Fluoroscope *> (hov_ft))
                exo->SetLabelString (f->DisplayText ());

            // render before handipoints.
            InsertChildBefore (exo, FindChildByClass ("HandiPoint"));
            exoskeleton_by_ft.Put (hov_ft, exo);
        }
        return OB_EE_TERMINAL_CONNECTION;
    }

    return OB_EE_NO_ROUTE;
}

ObRetort Windshield::MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    OEPointingEvent *oe = e->RawOEPointingEvent ();
    const Str &tent = e->Intent ();
    if (tent == handi_pass) // passthrough")
        return PassMzHandiEventsThrough (e, atm);

    if (tent != "pointing")
        return OB_EE_NO_ROUTE; // for the moment, only deal with pointing.

    if (!oe)
        return OB_EE_NO_ROUTE;

    const Str &prv = e->Provenance ();

    hoveree_by_provenance.Remove (prv);

    // next up: check SystemArea. real case won't be this simple, but
    // we're just getting started. I think the thing to do is call
    // IsInteractable or something like it with some pointing data; we'd
    // like to give the button an opportunity to unhighlight, for example.
    if (SystemArea *sysa = ~sys_area)
        if (!sysa->IsDisabled () &&
            sysa->WhackCheck (oe->PhysOrigin (), oe->PhysThrough (), NULL, NULL,
                              NULL)) {
            return DescentSingleTarget (sysa);
        } else
            sysa->DissociateProvenance (prv);

    if (FlatThing *ft = TopmostIntersectedOccupant (
            oe->PhysOrigin (),
            oe->PhysThrough ())) { // bail if someone else already has priority.
        if (manipulee_by_provenance.ValIsPresent (ft) ||
            regulee_by_provenance.ValIsPresent (ft))
            return OB_EE_EXCLUSIVE_TERMINAL_CONNECTION;

        Regulator *reggie = new Regulator (ft);
        reggie->SetParent (this);
        Ernestine *fh = NULL;
        SwitchboardAgent *sa = (ClosestAtmosphericSwitchboardAgent (atm));
        fh = dynamic_cast<Ernestine *> (sa);
        if (!fh) {
            YaroSneeze *ysne = dynamic_cast<YaroSneeze *> (sa);
            OB_LOG_WARNING ("Using YaroSneeze to find the focus hog in "
                            "Windshield::MzHandiHarden");
            if (ysne)
                fh = ysne->OurFocusHog ();
            reggie->Delete ();
        }
        if (fh) {
            InsertChildBefore (reggie, FindChildByClass ("HandiPoint"));
            manipulee_by_provenance.Put (prv, reggie);
            regulee_by_provenance.Put (prv, ft);
            // reggie does some setup in harden, so we'll manually call that.
            // reggie -> MzHandiHarden (e, atm);
            Exoskeleton *exo = exoskeleton_by_ft.Find (ft);
            // NOTE (kjhollen) it's possible that HandiPoints driven by
            // the mezzanine web application can be over objects without
            // exoskeletons, because they're not generating move events.
            // so we can't assume that an object we're over has an exoskeleton,
            // and we have to check if there isn't one, and make it.
            if (!exo) {
                exo = Exoskeleton::NewCustomFittedFor (ft);
                exo->SetParent (this);
                // render before handipoints.
                InsertChildBefore (exo, FindChildByClass ("HandiPoint"));
                exoskeleton_by_ft.Put (ft, exo);
            }
            exo->ProvenanceHold (e->Provenance ());
            return DescentSingleTarget (reggie);
        }
    }

    return OB_EE_TERMINAL_CONNECTION;
}

ObRetort Windshield::MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    OEPointingEvent *oe = e->RawOEPointingEvent ();
    const Str &tent = e->Intent ();
    if (tent == handi_pass) // passthrough")
        return PassMzHandiEventsThrough (e, atm);

    if (tent != "pointing")
        return OB_EE_NO_ROUTE; // for the moment, only deal with pointing.

    const Str &prv = e->Provenance ();
    FlatThing *ft = regulee_by_provenance.Find (prv);

    Exoskeleton *exo = exoskeleton_by_ft.Find (ft);
    if (exo)
        exo->ProvenanceRelease (prv);
    regulee_by_provenance.Remove (prv);
    manipulee_by_provenance.Remove (prv);

    if (oe) {
        if (ft && (ft != TopmostIntersectedOccupant (oe->PhysOrigin (),
                                                     oe->PhysThrough ())))
            RemoveExoskeletonIfStale (ft);
    }

    if (SystemArea *sysa = ~sys_area)
        return DescentSingleTarget (sysa);

    return OB_EE_BREAK_CONNECTION;
}

ObRetort Windshield::RemoveChild (KneeObject *ko, Atmosphere *atm) {
    if (((~asset_bucket)->RemoveChild (ko, atm)).IsError ())
        return super::RemoveChild (ko, atm);

    return OB_OK;
}

ObRetort Windshield::ScheduleChildRemoval (KneeObject *ch) {
    if (FlatThing *ft = dynamic_cast<FlatThing *> (
            ch)) { // First find the providance of the
                   // manipulated object. Currently
        // only Regulator calls ScheduleChildRemoval
        if (Str provenance = manipulee_by_provenance.KeyFromVal (ft)) {
            regulee_by_provenance.Remove (provenance);
            manipulee_by_provenance.Remove (provenance);

            Exoskeleton *exo = exoskeleton_by_ft.Find (ft);
            if (exo)
                exo->ProvenanceRelease (provenance);
        }
    }
    return NestyThing::ScheduleChildRemoval (ch);
}

ObRetort Windshield::MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    OEPointingEvent *oe = e->RawOEPointingEvent ();
    if (!oe)
        return OB_UNKNOWN_ERR;

    if (e->Intent () == handi_pass)
        return PassMzHandiEventsThrough (e, atm);

    if (e->Intent () != "pointing")
        return OB_EE_NO_ROUTE;

    const Str &prv = e->Provenance ();
    // FlatThing *ft = manipulee_by_provenance . Find (prv);
    manipulee_by_provenance.Remove (prv);
    regulee_by_provenance.Remove (prv);
    FlatThing *hov_ft = hoveree_by_provenance.Find (prv);
    hoveree_by_provenance.Remove (prv);

    RemoveExoskeletonIfStale (hov_ft);

    return OB_EE_BREAK_CONNECTION;
}

ObRetort Windshield::MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                           Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    const Str &prv = e->Provenance ();
    if (prv.IsNull ())
        return OB_UNKNOWN_ERR;

    if (e->Intent () != "pointing") { // remove the exo of any hovered asset
        FlatThing *hoveree = hoveree_by_provenance.Find (prv);
        if (hoveree) {
            hoveree_by_provenance.Remove (prv);
            RemoveExoskeletonIfStale (hoveree);
        }
    } else { // We should show an exo here if needed, but that's rather
             // difficult.
             // Since the MzHandiIntentChanged event comes as the result of an
             // OERatchetSnick, we don't have a raw OEPointingEvent to use for
             // collision detection, nor can we simulate an MzHandiMove event.
             // Since wands continuously generate move events, the only visible
        // symptom of this is that no exo will appear when a web user ratchets
        // into pointing mode while hovering an asset, until they move.

        // A possible solution to this issue would be to introduce a secondary
        // "hover" state (pointee_by_provenance?) that indicates the pointer is
        // in fact pointing at an asset even though its mode is not pointing and
        // as such no exo should be shown. This would allow us to look up
        // the pointee by provenance here and create an exo for it.
    }

    return OB_EE_TERMINAL_CONNECTION;
}

ObRetort Windshield::MzTenderProbe (MzTenderProbeEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (Conductivity *c = dynamic_cast<Conductivity *> (
            e->ChargeOrigin ())) { // Return to sender (probably Ovipositor)
        if (this->IsFull ()) {     // Reject this deposit with an appropriate
                                   // message
            Slaw s = Slaw::Map ("sender", "Windshield", "action", "rejected",
                                "color", ObColor (1.0, 0.0, 0.0, 0.5),
                                "message", "Windshield is full");
            c->ReturnStroke (s, e, atm);
        } else { // Reset the ovipositor state, in case another tender target
            // such as the deck had previously set it in a rejected state
            Slaw s = Slaw::Map ("sender", "Windshield", "action", "deposit");
            c->ReturnStroke (s, e, atm);
        }
    }

    return OB_EE_TERMINAL_CONNECTION;
}

ObRetort Windshield::MzTenderRescind (MzTenderRescindEvent *e,
                                      Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (Acetate *source_entity = e->SourceEntity ())
        source_entity->RemoveChild (e->Item ());

    return OB_EE_TERMINAL_CONNECTION;
}

ObRetort Windshield::MzTenderSolidify (MzTenderSolidifyEvent *e,
                                       Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    ObRetort ort = OB_EE_NO_ROUTE;

    if (KneeObject *ko = e->Item ()) {
        Acetate *source_zone = e->SourceEntity ();
        bool point_is_inside = GeomSlab::PointInsideRectTest (
            e->CurrLocation (), LocOf (V::Center, H::Center) - Lage (),
            Over () * Width () * 0.5, Up () * Height () * 0.5);
        if (!this->IsFull () && point_is_inside) {
            if (LocusThing *lt = dynamic_cast<LocusThing *> (ko)) {
                if (e->EventArcElapsedInterval () < 0.2) {
                    ObRef<FeldGeomPod *> gp = RetrieveGeometry ();
                    if (FeldGeomPod *fgp = ~gp) {
                        lt->SetLocHard (e->EstabLocation ());
                        lt->SetLoc (fgp->cent);
                    }
                } else if (Acetate *tate =
                               dynamic_cast<Acetate *> (Parent ())) {
                    if (OEPointingEvent *oep = e->RawOEPointingEvent ()) {
                        Vect hit;

                        if (tate->SelfPlaneWhackCheck (oep->PhysOrigin (),
                                                       oep->PhysThrough (),
                                                       NULL, &hit, NULL)) {
                            lt->SetLocHard (e->EstabLocation ());
                            lt->SetLoc (hit);
                        }
                    }
                }
                if (Acetate *ate = dynamic_cast<Acetate *> (lt))
                    ate->SetParent (this);
                if (TexQuad *tq = dynamic_cast<TexQuad *> (lt)) {
                    ObRef<FeldGeomPod *> gp = RetrieveGeometry ();
                    if (FeldGeomPod *fgp = ~gp)
                        tq->SetSize (tq->TextureImageWidth () * fgp->width /
                                         fgp->pixw,
                                     tq->TextureImageHeight () * fgp->height /
                                         fgp->pixh);
                    tq->SetBorderColor (ObColor (0.8, 0.8));
                    if (ImageClot *ic = tq->TextureImage ())
                        tq->AppendWhatnot (cnt_src_str,
                                           ic->FindWhatnot (cnt_src_str));
                }
                if (VideoThing *vt = dynamic_cast<VideoThing *> (
                        lt)) { // set to full 1:1 pixel size, if we have a video
                               // source,
                    // that is. otherwise, we'll fall back to a width and height
                    // that's 75% of the feld size, or in the worst case,
                    // 100 x 100 mm (if feld geometry is unknown).
                    float64 backup_width = 100.0, backup_height = 100.0;
                    float64 px_ratio = 1.0;
                    // if there's some feld geometry, our fallback values
                    // can be better guesses, at least.
                    ObRef<FeldGeomPod *> gp = RetrieveGeometry ();
                    if (FeldGeomPod *fgp = ~gp) {
                        px_ratio = fgp->width / fgp->pixw;
                        // and
                        backup_width = fgp->width * 0.75;
                        backup_height = fgp->height * 0.75;
                    } else
                        OB_LOG_WARNING (
                            "no feld geom pod when solidifying video");
                    // now, finally, see if there's some useful aspect ratio
                    // info we can get from the video source.
                    float64 vidthing_w = backup_width,
                            vidthing_h = backup_height;
                    if (VideoSource *vs = vt->GetVideoSource ()) {
                        float64 vs_w = vs->Width ();
                        float64 vs_h = vs->Height ();
                        if (vs_w <= 0.0 || vs_h <= 0.0) { // if we get here,
                                                          // this means the
                                                          // video source
                            // has not been prerolled, so its width and height
                            // are not yet known. we'll try to kick it in
                            // to gear, but this is a blocking call, and might
                            // result in preformance hiccups -- so we stop
                            // trying to preroll after 0.2 seconds.
                            vs->Preroll (0.2);
                            vs_w = vs->Width ();
                            vs_h = vs->Height ();
                        }
                        // only replace the "backup" values if we've got
                        // something good to go on here (note that it's
                        // possible that preroll finishes above without
                        // having successfully prerolled the video):
                        if (vs_w >= 0.0)
                            vidthing_w = vs_w;
                        if (vs_h >= 0.0)
                            vidthing_h = vs_h;
                    }
                    // remember: if no feld geometry, px_ratio is just 1.0.
                    vidthing_w = vidthing_w * px_ratio;
                    vidthing_h = vidthing_h * px_ratio;
                    vt->SetSize (vidthing_w, vidthing_h);
                    vt->SetBorderColor (ObColor (0.8, 0.8));
                    vt->draw_outline = true;
                    vt->SetShouldDrawChildrenFirst (false);
                }
                if (ShowyThing *bk = OurAssetBucket ()) {
                    bk->AppendChild (lt);
                    lt->AppendWhatnot (el_uid_str, Slaw (NextElementUID ()));
                }
            }
        } else { // TODO: create asset ? send message to corkboards?
        }
        source_zone->RemoveChild (ko);
        ort = OB_EE_TERMINAL_CONNECTION;
    }

    return ort;
}

/*ObRetort Windshield::MzMigrateBegin (MzMigrateBeginEvent *e, Atmosphere *atm)
{ if (! e)
    return OB_ARGUMENT_WAS_NULL;

  FlatThing *ft = dynamic_cast <FlatThing *> (e -> Migrant ());
  if (ft)
    { ft -> SetLocHard (e -> CurrentLoc ());
      return OB_EE_PROMOTE_TO_CONDUCTION;
    }

  return OB_EE_NO_ROUTE;
}

ObRetort Windshield::MzMigrateContinue (MzMigrateContinueEvent *e,
                                        Atmosphere *atm)
{ if (! e)
    return OB_ARGUMENT_WAS_NULL;

  if (FlatThing *ft = dynamic_cast <FlatThing *> (e -> Migrant ()))
    { ft -> SetLocHard (e -> CurrentLoc ());
      OB_LOG_DEBUG ("migrate: " + e -> CurrentLoc () . ToS ());
      return OB_OK;
    }

  return OB_EE_NO_ROUTE;
}

ObRetort Windshield::MzMigrateEnd (MzMigrateEndEvent *e, Atmosphere *atm)
{ if (! e)
    return OB_ARGUMENT_WAS_NULL;

  if (FlatThing *ft = dynamic_cast <FlatThing *> (e -> Migrant ()))
    { ft -> SetLocHard (e -> CurrentLoc ());
      return OB_EE_BREAK_CONNECTION;
    }

  return OB_EE_NO_ROUTE;
}

ObRetort Windshield::MzMigrateAbort (MzMigrateAbortEvent *e, Atmosphere *atm)
{ if (! e)
    return OB_ARGUMENT_WAS_NULL;

  if (FlatThing *ft = dynamic_cast <FlatThing *> (e -> Migrant ()))
    { // cancelled, softly animate back to orig loc.
      ft -> SetLoc (e -> OriginalLoc ());
      return OB_EE_BREAK_CONNECTION;
    }

  return OB_EE_NO_ROUTE;
}
*/
ObRetort Windshield::HandleValveOpen (
    Atmosphere *a) { // ObCrawl <HandiPoint *> cr = condensed_hands . Crawl ();
    ObCrawl<HandiPoint *> cr = HandiPoint::AllHandiPointCrawl ();
    while (!cr.isempty ()) {
        HandiPoint *hp = cr.popfore ();
        if (hp) // && ! IsHandiPointFromWeb (hp))
            hp->Condense ();
    }
    condensed_hands.Empty ();
    return OB_OK;
}

ObRetort Windshield::HandleValveShut (Atmosphere *a) {
    ObCrawl<HandiPoint *> cr = HandiPoint::AllHandiPointCrawl ();
    while (!cr.isempty ()) {
        HandiPoint *hp = cr.popfore ();
        if (hp && hp->AdjColor () != ObColor (1.0, 0.0)) {
            hp->Evaporate ();
            condensed_hands.Append (hp);
        }
    }
    return OB_OK;
}
