
/* (c)  oblong industries */

#ifndef SLUICE_RETORTS_WITH_ZEST
#define SLUICE_RETORTS_WITH_ZEST

#include "libLoam/c/ob-retorts.h"
#include "libLoam/c/ob-api.h"

#define OB_RETORTS_SLUICE_FIRST OB_RETORTS_APP_FIRST
#define OB_RETORTS_SLUICE_LAST OB_RETORTS_APP_LAST

// We have 100000 retorts to play with, from OB_RETORTS_MEZZANINE_FIRST
// to OB_RETORTS_MEZZANINE_LAST.

// ---------- failure codes (negative) ----------

#define OB_VAL_EXISTS -(OB_RETORTS_SLUICE_FIRST + 45000)

#define OB_NO_SHOVING -(OB_RETORTS_SLUICE_FIRST + 48448)
#define OB_NO_APPROPRIATE_TWEEZERS -(OB_RETORTS_SLUICE_FIRST + 48449)

#define OB_SLUICE_UNKNOWN_CLIENT -(OB_RETORTS_SLUICE_FIRST + 49100)
#define OB_SLUICE_MALFORMED_MESSAGE -(OB_RETORTS_SLUICE_FIRST + 49160)

#define OB_SLUICE_DESTINATION_FULL -(OB_RETORTS_SLUICE_FIRST + 50000)

#define OB_SLUICE_DISK_ACCESS_BADTH -(OB_RETORTS_SLUICE_FIRST + 51000)
#define OB_SLUICE_EMPTY_DOSSIER_DIR -(OB_RETORTS_SLUICE_FIRST + 51010)
#define OB_SLUICE_UNKNOWN_ASSET -(OB_RETORTS_SLUICE_FIRST + 51020)
#define OB_SLUICE_UNKNOWN_DOSSIER -(OB_RETORTS_SLUICE_FIRST + 51030)

#define OB_SLUICE_NO_DATA -(OB_RETORTS_SLUICE_FIRST + 52000)

#define OB_SLUICE_INVALID_JAVASCRIPT -(OB_RETORTS_SLUICE_FIRST + 53000)

// ---------- success codes (positive) ----------

#define OB_ALREADY_REGISTERED_TWEEZERS OB_RETORTS_SLUICE_FIRST + 48450

// This is a symbol which can be referenced to force SluiceRetorts.C
// to be pulled into static libraries.  Without pulling it in, the
// string conversion function will not be registered.

extern int ob_private_hack_pull_in_sluice_retorts;

#endif /* SLUICE_RETORTS_WITH_ZEST */
