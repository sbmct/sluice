#ifndef __SIMPLE_TEXRECT_H__
#define __SIMPLE_TEXRECT_H__

#include <libBasement/ImageClot.h>
#include <libBasement/SoftColor.h>
#include <libNoodoo/FlatThing.h>
#include <libNoodoo/GLMiscellany.h>
#include <libNoodoo/ob-platspec-cg.h>

#include <libImpetus/OEPointing.h>

#include <libNoodoo/GLTex.h>

#include "Acetate.h"

#include "gspeak_30_shim.h"

namespace oblong {
namespace sluice {

using namespace oblong::impetus;
using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::loam;

/**
 * SimpleTexRect is a flat, rectangular area that draws a texture.
 *
 * It exposes several controls to the user to manage how the texture is mapped
 * to its geometry.
 *
 * Coordinates & Convensions:
 *  - UV & ST origin @ BL
 *  - expand [0,1] along Over,Up
 **/
class SimpleTexRect : public Acetate, public OEPointing::Evts {
    PATELLA_SUBCLASS (SimpleTexRect, Acetate);

public: /// public enums, structs, flags, & member vars
        /**
         * Clamp   : uses tex coords of nearest edge (clamps S&T to [0,1])
         * Discard : only renders ST within [0,1], turns rest to BackGroundColor
         * Repeat  : modulates ST to range [0,1]
         * Mirror  : repeats texture, but alternating 0->1 & 1->0 for ST
         **/
    enum Wrap_Behavior { Clamp = 0, Discard, Repeat, Mirror, Num_Wrap_Options };

    /**
     * None     : leave tex coords entirely to the user
     * Inscribe : fit texture inside geometry (maintaining tex's aspect ratio)
     * Crop     : fill geometry w/ texture (maintaining tex's aspect ratio)
     * Stretch  : fill geometry w/ entire texture (disregard aspect ratio)
     **/
    enum Stretch_Behavior {
        Geen = 0,
        Inscribe,
        Crop,
        Stretch,
        Num_Stretch_Options
    };

protected: /// member data
    Str touchy_prov;

    ObRef<GLTex *> texGL;

    v2float64 tex_coor_bl, tex_coor_tr;
    Wrap_Behavior wrap_mode_s, wrap_mode_t;
    Stretch_Behavior stretch_mode;

    // Background_Option
    ObColor background_clr[4]; /// color of background if looking at front
    float64 border_wid;
    ObColor border_iro;

public:
    SimpleTexRect ();
    virtual void Init ();

    // virtual ObRetort Inhale (Atmosphere *atm);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

    virtual ObRetort AcknowledgeSizeChange ();

    /**
     * Texture options
     **/
    FLAG_MACHINERY_FOR (ShouldDrawTexture);
    GLTex *Texture () const { return ~texGL; }
    virtual bool HasTexture () const;

    virtual void SetImage (ImageClot *ic,
                           GLTex::MipmapPolicy mipPolicy = GLTex::MipOnLoad,
                           GLTex::SizePolicy sizePolicy = GLTex::NPOT_OK);
    virtual void SetTexture (GLTex *texture);

    /**
     * Draw simple colored border around geometry
     **/
    FLAG_MACHINERY_FOR (ShouldDrawBorder);

    ObColor BorderColor () const { return border_iro; }
    void SetBorderColor (const ObColor &c) { border_iro = c; }

    float BorderWidth () const { return border_wid; }
    void SetBorderWidth (float width) { border_wid = width; }

    /**
     * Background drawing modes
     **/
    FLAG_MACHINERY_FOR (ShouldDrawBackground);

    ObColor BackgroundColor (const Vect &v);
    ObColor BackgroundColor (int idx);
    void SetBackgroundColor (const ObColor &c);
    void SetBackgroundColors (const ObColor &ctl,
                              const ObColor &ctr,
                              const ObColor &cbr,
                              const ObColor &cbl);

    /**
     * Set the user-defined texture coordinates used to map the texture
     * on this TexRect's geometry.  The mapping follows g-speak
     * conventions meaning that if you're looking at the front of the
     * TexRect, s1 is to the left, s2 is to the right, t1 is at the
     * bottom and t2 is at the top.  Of course, these mappings can be
     * swapped by calling ShouldInvert[XY].  For compactness, the
     * abbreviations BL and TR are used for "bottom-left" and
     * "top-right".
     */
    //@{
    v2float64 TexCoorBL () const { return tex_coor_bl; }
    v2float64 TexCoorTR () const { return tex_coor_tr; }

    FLAG_MACHINERY_FOR (ShouldInvertS); /// upside down (top-to-bottom)
    FLAG_MACHINERY_FOR (ShouldInvertT); /// backwards (left-to-right)

    virtual ObRetort AcknowledgeTexCoorsChange ();

    virtual void SetTexCoors (float64 s1, float64 t1, float64 s2, float64 t2);
    virtual void SetTexCoors (v2float64 bl, v2float64 tr);

    virtual void SetTexCoorBL (float64 s, float64 t);
    virtual void SetTexCoorBL (v2float64 bl) {
        return SetTexCoorBL (bl.x, bl.y);
    }
    virtual void SetTexCoorTR (float64 s, float64 t);
    virtual void SetTexCoorTR (v2float64 tr) {
        return SetTexCoorTR (tr.x, tr.y);
    }

    virtual void SetTextureCenter (float64 s, float64 t);
    virtual void SetTextureCenter (v2float64 st) {
        return SetTextureCenter (st.x, st.y);
    }
    //@}

    /// TODO : document this one's purpose.
    virtual void SquareUpTexCoors ();

    virtual void PanTexture (float64 ds, float64 dt);
    virtual void PanTexture (v2float64 dsdt) {
        return PanTexture (dsdt.x, dsdt.y);
    }

    /// ZoomTexture in texture coordinates
    virtual void ZoomTexture (float64 zs, float64 zt, v2float64 cntr);
    virtual void ZoomTexture (float64 zm, v2float64 cntr) {
        ZoomTexture (zm, zm, cntr);
    }
    virtual void ZoomTexture (float64 zs, float64 zt);
    virtual void ZoomTexture (float64 zm) { return ZoomTexture (zm, zm); }

    virtual int HorizontalWrapBehavior () { return wrap_mode_s; }
    virtual int VerticalWrapBehavior () { return wrap_mode_t; }
    virtual void SetWrapBehavior (Wrap_Behavior wbs, Wrap_Behavior wbt);
    virtual void SetHorizontalWrapBehavior (Wrap_Behavior wbs);
    virtual void SetVerticalWrapBehavior (Wrap_Behavior wbt);

    virtual int StretchBehavior () { return stretch_mode; }
    virtual void SetStretchBehavior (Stretch_Behavior sb);

    virtual ObRetort OEPointingAppear (OEPointingAppearEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort OEPointingVanish (OEPointingVanishEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort OEPointingHarden (OEPointingHardenEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort OEPointingSoften (OEPointingSoftenEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort OEPointingMove (OEPointingMoveEvent *e, Atmosphere *atm);

protected: /// utility fuctions
    /// the range of the texture coordinates from BL -> TR of geometry
    v2float64 TexCoorExtent ();

    /// ST <---> TxTy
    v2float64 TexelsToTexCoors (float64 x, float64 y);
    v2float64 TexelsToTexCoors (v2float64 xy) {
        return TexelsToTexCoors (xy.x, xy.y);
    }
    v2float64 TexCoorsToTexels (float64 s, float64 t);
    v2float64 TexCoorsToTexels (v2float64 st) {
        return TexCoorsToTexels (st.x, st.y);
    }

    /// UV <---> ST
    virtual v2float64 UVToTexCoors (float64 u, float64 v);
    virtual v2float64 UVToTexCoors (v2float64 uv) {
        return UVToTexCoors (uv.x, uv.y);
    }
    virtual v2float64 TexCoorsToUV (float64 s, float64 t);
    virtual v2float64 TexCoorsToUV (v2float64 st) {
        return TexCoorsToUV (st.x, st.y);
    }

    /// Wrld <---> ST
    Vect TexCoorsToWorldPos (float64 s, float64 t);
    Vect TexCoorsToWorldPos (v2float64 st) {
        return TexCoorsToWorldPos (st.x, st.y);
    }
    v2float64 WorldPosToTexCoor (Vect v); /// note: first projects onto plane

    /// Wrld <---> UV
    v2float64 VectToUV (Vect v);
};
}
} // namespaces staging, oblong

#endif //__SIMPLE_TEXRECT_H__
