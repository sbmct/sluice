
/* (c)  oblong industries */

#ifndef HANDIPOINT__A_NEW_HANDIPOINT_FOR_A_NEW_ERA
#define HANDIPOINT__A_NEW_HANDIPOINT_FOR_A_NEW_ERA

#include <libNoodoo/VisiFeld.h>
#include <libNoodoo/SoftVertexForm.h>
#include <libNoodoo/VertEllipse.h>
#include <libNoodoo/TexQuad.h>
#include <libNoodoo/GlyphoString.h>

#include <libImpetus/OEPointing.h>
#include <libImpetus/OERatchet.h>
#include <libImpetus/OEBlurt.h>

#include <libBasement/SoftColor.h>
#include <libBasement/TWrangler.h>
#include <libBasement/SWrangler.h>
#include <libBasement/RWrangler.h>
#include <libBasement/CoordTransformWrangler.h>
#include <libBasement/ThrobWrangler.h>
#include <libBasement/AsympVect.h>
#include <libBasement/InterpColor.h>
#include <libBasement/QuadraticFloat.h>

#include <libLoam/c++/ObMap.h>
#include <libLoam/c++/ObUniqueTrove.h>
#include <libLoam/c++/Str.h>
#include <libLoam/c++/Vect.h>

#include <libBasement/AsympFloat.h>

#include "Tweezers.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::loam;

class HandiPoint : public Tweezers,
                   public OEPointing::Evts,
                   public OERatchet::Evts,
                   public OEBlurt::Evts {
    PATELLA_SUBCLASS (HandiPoint, Tweezers);

public:
    // this is just a workaround to let us pass around arrays of exactly
    // 12 Vects. Thanks, C++!
    struct HandiTine {
        Vect tines[12];
    };

    /// A set of
    class HandiPointStyle : public AnkleObject {
    public:
        /// name used to look up this HandiPointStyle in HandiPoint's
        /// SetStyleByName.
        Str name;
        /// Sets of three quadrilaterals per array used in different
        /// HandiPoint states: hardened and softened. We separate two
        /// sets of points that get treated slightly differently by
        /// the HandiPoint: an alpha set, that can have their own
        /// color and bobble frequency, and a beta set, that march to
        /// the beat of a different drummer (err, that is, they can
        /// have a different color and bobble frequency from the alpha
        /// set. The tines describe points relative to the extents of
        /// the HandiPoint's size in our conventional yovo way
        /// (between -0.5 and 0.5). -0.5, -0.5 is the bottom left hand
        /// corner of the HandiPoint's "size" while 0.5, 0.5 is the top
        /// right. the z coordinate should generally be 0.0, unless you
        /// want a pointer mode where the shapes are out of the plane
        /// of the HandiPoint's flatness.
        HandiTine softened_alpha_tines, softened_beta_tines;
        HandiTine hardened_alpha_tines, hardened_beta_tines;
        HandiTine off_feld_tines;
        /// The HandiPoint applies the alpha_wrangler to the alpha tines,
        /// and the beta wrangler to the beta tines. These can be the
        /// same or different wranglers, or wranglers that don't really
        /// bobble at all, but they must not be NULL.
        ObRef<ThrobWrangler *> alpha_wrangler, beta_wrangler;

    public:
        HandiPointStyle (const Str &p_name,
                         Vect soft_a_tines[12],
                         Vect soft_b_tines[12],
                         Vect hard_a_tines[12],
                         Vect hard_b_tines[12],
                         Vect off_tines[12],
                         ThrobWrangler *a_throb,
                         ThrobWrangler *b_throb);

        /// note that everything here that returns a reference
        /// returns const. we don't want people mucking with the internals
        /// of a HandiPointStyle after it's been created, since it's
        /// shared across multiple (many) HandiPoints.
        const Str &Name () const { return name; }
        const HandiTine SoftenedAlphaTines () const {
            return softened_alpha_tines;
        }
        const HandiTine SoftenedBetaTines () const {
            return softened_beta_tines;
        }
        const HandiTine HardenedAlphaTines () const {
            return hardened_alpha_tines;
        }
        const HandiTine HardenedBetaTines () const {
            return hardened_beta_tines;
        }
        const ThrobWrangler *AlphaWrangler () const { return ~alpha_wrangler; }
        const ThrobWrangler *BetaWrangler () const { return ~beta_wrangler; }
        const HandiTine OffFeldTines () const { return off_feld_tines; }
    };

protected:
    /// internal boolean indicating whether or not
    /// the handipoint appears on a feld
    bool onscreen;

    // this applies to the tines
    // When offscreen, this fades to 0.
    ObRef<SWrangler *> fade_sizer;
    // this represents how far offscreen to draw
    // is a percentage that is applied to visifeld (width + height)
    float64 fade_threshold;

    /// we set the HandiPoint's location by translating it to the
    /// appropriate place.
    ObRef<TWrangler *> child_loc_wrangler;
    ObRef<CoordTransformWrangler *> child_orientation_wrangler;
    /// we can also rotate the tines: typically reserved for offscreen
    /// graphics.
    ObRef<RWrangler *> rot_wrangler;

    /// last VisiFeld this HandiPoint appeared on.
    ObWeakRef<VisiFeld *> last_vf;
    /// alpha set in 0-2, beta set in 3-5
    ObTrove<SoftVertexForm *> tines;
    /// extra ratchet texture / gets rotated
    ObRef<RWrangler *> ratchet_rotter;

    /// texture that visualizes how much the user must rotate to trigger
    /// trigger a ratchet.
    ObRef<TexQuad *> ratchet_goal_marker;
    /// texture that visualizes how much a user has progressed toward a new
    /// ratchet state, once they've passed the "edge up" mark.
    ObRef<TexQuad *> ratchet_progress_marker;
    /// parent showy for all ratchet preview textures.
    ObRef<ShowyThing *> ratchet_showy;
    /// wrangler responsible for rotating the ratchet_progress_marker about
    /// its normal.
    ObRef<RWrangler *> ratchet_progress_wrangler;
    /// we keep an extra special reference to the ratchet_showy's adj color
    /// as an InterpColor, so that we can adjust its interpolation time in
    /// special circumstances (say, when ratchet is canceled vs. achieved)
    ObRef<InterpColor *> ratchet_adj;
    /// the current angle that the ratchet_progress_marker texture has been
    /// rotated about its normal.
    float64 ratchet_progress_angle;

    /// we keep a reference to the width and height of the HandiPoint
    /// as a QuadraticFloat so that we may adjust its InterpTime and
    /// easing when it grows and shrinks after ratchet.
    ObRef<QuadraticFloat *> q_width;
    ObRef<QuadraticFloat *> q_height;

    /// text label for new ratchet mode in helper / preview graphic.
    ObRef<GlyphoString *> new_ratchet_label;
    /// encapsulating showy for textures related to ratchet hint / helper
    /// graphic. appears if user ponders ratcheting graphic for a little bit.
    ObRef<ShowyThing *> helper_showy;
    /// texture showing a preview of what the HandiPoint looks like in the
    /// pending ratchet state, shown with helper.
    ObRef<TexQuad *> preview_tex;

    /// independently track helper and label adj color so we can adjust
    /// their fade times by having an InterpColor reference.
    ObRef<InterpColor *> helper_adj;
    ObRef<InterpColor *> label_iro;

    /// angle of rotation for ratchet_progress_marker at start and end
    /// of ratchet detent.
    float64 ratchet_start_angle;
    float64 ratchet_end_angle;
    /// name of intent for pending ratchet, if there is one.
    Str pending_ratchet;

    /// little circle that shows the handipoint user's identity (by color),
    /// if the flag ShouldShowProvenance is set to true.
    ObRef<VertEllipse *> provenance_dot;
    /// the two sets of handipoint tines can throb at different rates.
    ObRef<ThrobWrangler *> alpha_tine_throbber, beta_tine_throbber;

    /// the following timers are used to prevent ratcheting in certain
    /// instances:

    /// following ceiling reset, don't allow ratcheting for a little while...
    FatherTime ratchet_stabilizer_timer;

    /// amount of time since last soften event.
    FatherTime soften_timer;
    /// amount of time elapsed since user last ratcheted "forward"
    FatherTime ratchet_forward_timer;
    /// amount of time elapsed since user last ratcheted "backward"
    FatherTime ratchet_backward_timer;

    /// amount of time elapsed since user crossed some large velocity threshold,
    /// NOT CURRENTLY IN USE.
    // FatherTime feld_velocity_timer;

    /// and some other, somewhat different timers:

    /// amount of time elapsed since this HandiPoint appeared off feld. if the
    /// HandiPoint is off_feld for a certain amount of time, ratcheting will
    /// be disabled so the user does not return their HandiPoint to the screen
    /// in a new mode, confused.
    FatherTime off_feld_timer;
    /// amount of time the ratchet glyph has appeared on screen. used to
    /// determine
    /// when helper glyph should appear. only valid when
    /// ratchet_previs_on == true.
    FatherTime ratchet_glyph_timer;

    /// how far the user must rotate past "edge up" to trigger a ratchet
    /// to a new state.
    float64 ratchet_hysteresis_angle;
    /// quantized wand face which is "up"
    int32 ratchet_face;
    float64 ratchet_reset_threshold;

    /// keeps track of whether or not the ratchet pending graphics
    /// should be drawn on screen.
    bool ratchet_previs_on;

    /// scaler that stretches the underlying shapes out to the extents
    /// of the HandiPoint (we expect HandiPointStyles to generally
    /// have points with values between -0.5 and 0.5, so we scale them
    /// up here).
    ObRef<SWrangler *> tine_sizer;
    /// the style of the shapes and wobbliness of this handipoint.
    ObRef<HandiPointStyle *> hp_style;

    /// flag to turn on/off optional provenance marker.
    bool should_show_provenance : 1;
    /// flag to turn on/off whether or not a reminder glyph appears
    /// at the edge of the closest feld when the HandiPoint is not
    /// actually on one of the felds.
    bool should_show_off_feld_marker : 1;
    /// hold on to whether or not this HandiPoint should be in its "softened"
    /// state (as opposed to "hardened")--we need this when the glyph is
    /// off feld, because we don't change to show hardened/softened then and
    /// need to know which state we're in when we're back on).
    bool am_softened : 1;
    /// determine whether or not a Move event should trigger relocating
    /// this handipoint.
    bool am_frozen : 1;

    bool should_use_oe_ratchet : 1;

    bool am_exclusive : 1;

    /// In the case of wand ratchet reset, this tells the handi point
    /// to not allow ratcheting until the handi point is once again
    /// on the feld
    bool ceiling_ratchetreset_enable;
    bool disable_ratcheting_until_on_feld;

    Str intent;
    /// ORDERED list of intents this handipoint can currently ratchet through.
    ObTrove<Str> intent_list;

    /// provenance to handipoint map
    static ObMap<Str, HandiPoint *, NoMemMgmt, WeakRef> use_map;
    /// list of validated and registered styles
    static ObMap<Str, HandiPointStyle *> style_map;
    /// list of validated and registered intents
    static ObMap<Str, ObTrove<Str>> intent_to_styles_map;
    /// list of all living HandiPoints
    static ObUniqueTrove<HandiPoint *, WeakRef> all_handipoint_trove;

    /// flag to indicate control the creation and propagation of events
    /// Handi continues to receive and visually respond to events when this
    /// flag is off
    bool is_venting;

public:
    HandiPointStyle *CurrentStyle () const;
    const Str &CurrentStyleName () const;
    ObRetort SetStyle (HandiPointStyle *hps);

    int32 event_gen_mode;
    Vect curr_feldloc, prev_feldloc;

    /// look up HandiPoint for provenance. returns NULL if there
    /// is currently not a HandiPoint for the given provenance (prov).
    static HandiPoint *FindByProvenance (const Str &prov);

    /// returns a crawl of all living handipoints.
    static ObCrawl<HandiPoint *> AllHandiPointCrawl ();

    /// load basic styles.
    static bool InitializeClassRidiculously ();

    /// validates that the given style can work and holds on to
    /// it in a static list of available modes. If the name of the style
    /// to add conflicts with a style already in the list,
    static ObRetort RegisterStyle (HandiPointStyle *hps);
    static ObRetort RegisterIntent (const Str &intent_name,
                                    const ObTrove<Str> &lst);
    /// looks up the HandiPointStyle by name. returns NULL if there isn't
    /// one (or it hasn't been registered).
    static const HandiPointStyle *StyleByName (const Str &name);
    /// the default style is "pointing"
    static Str DefaultStyleName ();
    /// the default intent is "pointing"
    static Str DefaultIntent ();

    /// creates a handipoint with the default style.
    HandiPoint ();
    virtual ~HandiPoint ();

    ObRetort SetStyleByName (const Str &name);

    /// apply colors for the given pointing source (as a provenance
    /// string prv).
    void StyleForProvenance (const Str &prv);

    void SetShouldShowOffFeldMarker (bool truth) {
        if (should_show_off_feld_marker == truth)
            return;
        should_show_off_feld_marker = truth;
    }
    bool QueryShouldShowOffFeldMarker () const {
        return should_show_off_feld_marker;
    }

    void SetShouldShowProvenance (bool truth) {
        if (~provenance_dot)
            (~provenance_dot)->SetShouldDraw (truth);
        should_show_provenance = truth;
    }
    bool QueryShouldShowProvenance () const { return should_show_provenance; }

    void SetIsExclusive (bool truth) { am_exclusive = truth; }
    bool QueryIsExclusive () const { return am_exclusive; }

    void SetCeilingRatchetResetEnable (bool enable) {
        ceiling_ratchetreset_enable = enable;
    }
    bool QueryCeilingRatchetResetEnable () const {
        return ceiling_ratchetreset_enable;
    }

    void SetRotation (float64 a);

    // sets the list of allowable intents/modes for this HandiPoint.
    // the list is ordered: ratcheting forward will cycle through modes in
    // the order specified in lst.
    void SetAvailableIntents (const ObTrove<Str> &lst);
    // returns the list of intents that are currently available for
    // this HandiPoint.
    const ObTrove<Str> &AvailableIntents () const;
    // returns the nth item in the list of available intents.
    const Str &Intent ();
    // returns the index of the current intent in the list of available intents.
    int64 IntentIndex (const Str &tent) const;
    bool IsRatchetingAllowed ();
    // sets the intent to the next one in the list of available intents.
    // if the current intent is the last one in the list, the mode is wrapped
    // around the list to the first available intent.
    void ProceedToNextIntent (Atmosphere *atm, OEPointingEvent *e);
    // sets the intent to the next one in the list of available intents.
    // if the current intent is the last one in the list, the mode is wrapped
    // around the list to the first available intent.
    void RecedeToPreviousIntent (Atmosphere *atm, OEPointingEvent *e);
    // sets the current intent to the nth available one.
    ObRetort
    AssertNamedIntent (const Str &tent, Atmosphere *atm, OEPointingEvent *e);

    // puts graphics on screen to show that a ratchet is pending, and whether
    // that ratchet is "forward" or "backward" (true or false, respectively)
    void ResetAndShowPendingRatchet (const bool &forward);
    // updates ratchet graphics based on how close the user is to completing
    // a pending ratchet; expects values 0.0 to 1.0 (cancel -> complete)
    void UpdatePendingRatchet (const float64 &detent_pct);
    // cancels a ratchet in progress, hides visual feedback.
    void CancelPendingRatchet ();

    // calculates the ratchet angle relative to gravity; returns value in
    // radians.
    float64 CalculateRatchetAngle (const Vect &aim, const Vect &horiz) const;

    virtual ObRetort OERatchetSnick (OERatchetSnickEvent *e, Atmosphere *atm);
    virtual ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *atm);
    // virtual ObRetort OEPointingHerald (OEPointingHeraldEvent *e,
    //                                   Atmosphere *atm);
    virtual ObRetort OEPointingAppear (OEPointingAppearEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort OEPointingVanish (OEPointingVanishEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort OEPointingMove (OEPointingMoveEvent *e, Atmosphere *atm);
    virtual ObRetort OEPointingSoften (OEPointingSoftenEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort OEPointingHarden (OEPointingHardenEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort OEPointingLinger (OEPointingLingerEvent *e,
                                       Atmosphere *atm);

    // accepts an intent and style
    // if intent matches, set style.
    virtual ObRetort
    ReturnStroke (const Slaw &info, ElectricalEvent *ee, Atmosphere *atm);

    ObRetort AdoptProvenance (const Str &prv);
    ObRetort DisownProvenance ();
    const Str CurrentProvenance ();

    static const Str EnhancedProvenance (const Str &prv);
    static const Str OriginalProvenance (const Str &prv);

    ObRetort ValidateEventProvenance (const Str &prv);

    /// if the handipoint's event source does not intersect with any feld,
    /// this method can be used to find a point (loc) on the edge of the closest
    /// feld and a direction (direction) that approximates where the handipoint
    /// is off the feld
    /// returns 'false' if onscreen, 'true' if offscreen
    virtual bool FindFeldEdge (const Vect &origin,
                               const Vect &aimer,
                               Vect &loc,
                               Vect &direction,
                               float64 &angle);

    /// update internal state to scale glyphs to the appropriate size.
    // virtual ObRetort AcknowledgeSizeChange ();
    /// update internal state to orient glyphs appropriately
    virtual ObRetort AcknowledgeOrientationChange ();

    /// soft fade-in; called upon OEPointingAppear.
    virtual void Condense ();
    /// soft fade-out; called upon OEPointingVanish.
    virtual void Evaporate ();

    /// stop tracking the location of the provenance
    virtual void Freeze ();
    bool Frozen ();
    /// begin tracking again
    virtual void Thaw ();

    /// update visual effects to show that the pointing gesture
    /// has hardened.
    virtual void Harden ();
    /// update visual effects to show that the pointing gesture
    /// has softened.
    virtual void Soften ();

    virtual bool IsHardened () { return !am_softened; }
    virtual bool IsSoftened () { return am_softened; }

    /// update visual effects to show that pointing gesture is offscreen
    void ConvertToOffscreen ();
    /// update visual effects to show that pointing gesture returns onscreen
    void ConvertToOnscreen ();

    virtual void SetLoc (const Vect &p) { FlatThing::SetLoc (p); }
    virtual void SetLocHard (const Vect &p) { FlatThing::SetLocHard (p); }

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm) {
        super::DrawSelf (vf, atm);
    }

    virtual ObRetort Travail (Atmosphere *atm);
    virtual ObRetort Exhale (Atmosphere *atm);

    virtual void SmotherVenting () { is_venting = false; }
    virtual void PermitVenting () { is_venting = true; }
    virtual bool IsVenting () const { return is_venting; }
};
}
} // a bittersweet farewell to namespaces mezzanine and oblong

#endif // HANDIPOINT__A_NEW_HANDIPOINT_FOR_A_NEW_ERA
