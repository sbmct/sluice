
/* (c)  oblong industries */

#ifndef MEZZANINE_HANDIPOINT_EVENT
#define MEZZANINE_HANDIPOINT_EVENT

#include <libImpetus/OEPointing.h>

#include <libGanglia/ElectricalEvent.h>

namespace oblong {
namespace sluice {
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class MzHandiEvent : public ElectricalEvent {
    PATELLA_SUBCLASS (MzHandiEvent, ElectricalEvent);
    OE_MISCY_MAKER (MzHandi, Electrical, "handi");

public:
    Vect handi_point_loc, prev_handi_point_loc;
    Str intent;

public:
    MzHandiEvent (KneeObject *orig_ko = NULL) : ElectricalEvent (orig_ko) {}

    const Vect &Location () const;
    const Vect &PreviousLocation () const;

    void SetCurrentLocation (const Vect &loc);
    void SetPreviousLocation (const Vect &loc);

    const Str &Intent () const;
    void SetIntent (const Str &ent);

    OEPointingEvent *RawOEPointingEvent () const;
    void SetRawOEPointingEvent (OEPointingEvent *pe);

    virtual void SynthesizeInternalProtein ();
    virtual ObRetort AnalyzeInternalProtein ();
};

class MzHandiCondenseEvent : public MzHandiEvent {
    PATELLA_SUBCLASS (MzHandiCondenseEvent, MzHandiEvent);
    OE_MISCY_MAKER (MzHandiCondense, MzHandi, "condense");
    MzHandiCondenseEvent (KneeObject *orig_ko = NULL)
        : MzHandiEvent (orig_ko) {}
};

class MzHandiEvaporateEvent : public MzHandiEvent {
    PATELLA_SUBCLASS (MzHandiEvaporateEvent, MzHandiEvent);
    OE_MISCY_MAKER (MzHandiEvaporate, MzHandi, "evaporate");
    MzHandiEvaporateEvent (KneeObject *orig_ko = NULL)
        : MzHandiEvent (orig_ko) {}
};

class MzHandiMoveEvent : public MzHandiEvent {
    PATELLA_SUBCLASS (MzHandiMoveEvent, MzHandiEvent);
    OE_MISCY_MAKER (MzHandiMove, MzHandi, "move");
    MzHandiMoveEvent (KneeObject *orig_ko = NULL) : MzHandiEvent (orig_ko) {}
};

class MzHandiHardenEvent : public MzHandiEvent {
    PATELLA_SUBCLASS (MzHandiHardenEvent, MzHandiEvent);
    OE_MISCY_MAKER (MzHandiHarden, MzHandi, "harden");
    MzHandiHardenEvent (KneeObject *orig_ko = NULL) : MzHandiEvent (orig_ko) {}
};

class MzHandiSoftenEvent : public MzHandiEvent {
    PATELLA_SUBCLASS (MzHandiSoftenEvent, MzHandiEvent);
    OE_MISCY_MAKER (MzHandiSoften, MzHandi, "soften");
    MzHandiSoftenEvent (KneeObject *orig_ko = NULL) : MzHandiEvent (orig_ko) {}
};

class MzHandiEnterEvent : public MzHandiEvent {
    PATELLA_SUBCLASS (MzHandiEnterEvent, MzHandiEvent);
    OE_MISCY_MAKER (MzHandiEnter, MzHandi, "enter");
    MzHandiEnterEvent (KneeObject *orig_ko = NULL) : MzHandiEvent (orig_ko) {}

    ObWeakRef<KneeObject *> prev_obj;
    ObWeakRef<KneeObject *> curr_obj;

    KneeObject *PrevObj () { return ~prev_obj; }
    void SetPrevObj (KneeObject *ko) {
        if (ko != NULL)
            prev_obj = ko;
    }

    KneeObject *CurrObj () { return ~curr_obj; }
    void SetCurrObj (KneeObject *ko) {
        if (ko != NULL)
            curr_obj = ko;
    }
};

class MzHandiExitEvent : public MzHandiEvent {
    PATELLA_SUBCLASS (MzHandiExitEvent, MzHandiEvent);
    OE_MISCY_MAKER (MzHandiExit, MzHandi, "exit");
    MzHandiExitEvent (KneeObject *orig_ko = NULL) : MzHandiEvent (orig_ko) {}

    ObWeakRef<KneeObject *> prev_obj;
    ObWeakRef<KneeObject *> curr_obj;

    KneeObject *PrevObj () { return ~prev_obj; }
    void SetPrevObj (KneeObject *ko) {
        if (ko != NULL)
            prev_obj = ko;
    }

    KneeObject *CurrObj () { return ~curr_obj; }
    void SetCurrObj (KneeObject *ko) {
        if (ko != NULL)
            curr_obj = ko;
    }
};

class MzHandiIntentChangedEvent : public MzHandiEvent {
    PATELLA_SUBCLASS (MzHandiIntentChangedEvent, MzHandiEvent);
    OE_MISCY_MAKER (MzHandiIntentChanged, MzHandi, "intent-changed");
    MzHandiIntentChangedEvent (KneeObject *orig_ko = NULL)
        : MzHandiEvent (orig_ko) {}
};

class MzHandiEventAcceptorGroup
    : public MzHandiCondenseEvent::MzHandiCondenseAcceptor,
      public MzHandiEvaporateEvent::MzHandiEvaporateAcceptor,
      public MzHandiMoveEvent::MzHandiMoveAcceptor,
      public MzHandiHardenEvent::MzHandiHardenAcceptor,
      public MzHandiSoftenEvent::MzHandiSoftenAcceptor,
      public MzHandiEnterEvent::MzHandiEnterAcceptor,
      public MzHandiExitEvent::MzHandiExitAcceptor,
      public MzHandiIntentChangedEvent::MzHandiIntentChangedAcceptor {};

class MzHandi {
public:
    typedef MzHandiEventAcceptorGroup Evts;
};
}
} // a bittersweet end for namespaces mezzanine and oblong

DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED (oblong::sluice::MzHandi::Evts);

#endif
