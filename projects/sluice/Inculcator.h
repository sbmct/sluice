
#pragma once

#include <libBasement/KneeObject.h>
#include <libLoam/c++/ObTrove.h>
#include <libLoam/c++/ObMap.h>
#include <set>
#include "Subscription.h"

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;

class Node;
class Javascript;

/** Determine whether or not a node should be displayed by a pigment */
class Inculcator : public KneeObject {
    PATELLA_SUBCLASS (Inculcator, KneeObject);

protected:
    Str js_test, pigment;
    ObMap<Str, Str> properties_match;
    ObTrove<Str> kinds;

    bool MatchesProperties (Node *n, const float64 t) const;
    bool MatchesJS (Node *n, const float64 t, Javascript &js) const;

    bool MatchesProperties (const SubscriptionNode &) const;
    bool MatchesJS (const SubscriptionNode &, Javascript &js) const;

    FLAG_MACHINERY (UsesJSTest);
    FLAG_MACHINERY (UsesPropertiesMatch);

public:
    Inculcator (const Str name, Str pigment, ObTrove<Str> &ks);

    ObTrove<Str> &Kinds () { return kinds; }

    void SetJSTest (const Str js) {
        js_test = js;
        SetUsesJSTest (true);
    }

    void AddPropertyMatch (const Str key, const Str val) {
        properties_match.Put (key, val);
        SetUsesPropertiesMatch (true);
    }

    const size_t HashCode () const;
    const bool Equals (const Inculcator &other) const;

    const Str Pigment () const { return pigment; }
    bool Matches (Node *n, const float64 t, Javascript &js) const;
    bool Matches (const SubscriptionNode &, Javascript &) const;

    static Inculcator *FromSlaw (const Slaw inc);
    Slaw ToSlaw () const;

    const int Compare (const Inculcator *other) const;
};
}
}
