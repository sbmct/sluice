
#pragma once

#include <libLoam/c++/ObTrove.h>
#include <libPlasma/c++/Slaw.h>
#include "geo_utils.h"

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::plasma;

class Node;
class NodeStore;

class NodeUpdate {
protected:
    ObTrove<LatLon> path;
    LatLon geoloc;
    Str ident, kind;
    float64 timestamp;
    bool good, has_geoloc, has_path;

public:
    NodeUpdate (Slaw s, NodeStore *store);
    const bool IsGood () const { return good; }
    const Str Ident () const { return ident; }
    const Str Kind () const { return kind; }
    const bool Matches (Node *n) const;
    const float64 Timestamp () const { return timestamp; }
    void Apply (Node *n) const;
};
}
}
