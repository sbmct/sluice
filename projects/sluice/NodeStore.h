/* Copyright (C) 2011 Oblong Industries */

#ifndef OBLONG_SLUICE_NODESTORE_H
#define OBLONG_SLUICE_NODESTORE_H

#include <map>
#include <set>

#include <libBasement/ob-hook-troves.h>
#include <libBasement/KneeObject.h>
#include <libLoam/c++/FatherTime.h>

#include <libPlasma/c++/Hose.h>

#include <algorithm>

#include "geo_utils.h"
#include "Inculcator.h"

namespace oblong {
namespace sluice {

class Node;
class Hasselhoff;
class GeoSubset;

using namespace oblong::loam;
using namespace oblong::basement;

/**
 * Store them nodes, central-like
 *
 * Note: The implementation has been hidden in an Impl class
 *       to aid compile times.
 */
class NodeStore : public KneeObject {
    PATELLA_SUBCLASS (NodeStore, KneeObject);

protected:
    // Call when a node is updated
    DOUBLE_ARG_HOOK_MACHINERY (NodeUpdated, NodeStore *, Node *);
    // Call when a node is removed
    DOUBLE_ARG_HOOK_MACHINERY (NodeRemoved, NodeStore *, Node *);
    // Call when the time range has been updated
    SINGLE_ARG_HOOK_MACHINERY (TimeRangeUpdated, NodeStore *);
    // Call when the current sluice time has been updated by a
    // means other than simple forward movement
    SINGLE_ARG_HOOK_MACHINERY (TimeUpdated, NodeStore *);
    // Call when the underlying node store has gone away
    // Note: if you store node pointers, you need to listen for
    // this and clear away *everything* in your fluoroscope when
    // this hook is called.
    SINGLE_ARG_HOOK_MACHINERY (UniverseUpdated, NodeStore *);

    FLAG_MACHINERY (InculcatorsAreDirty);

public:
    virtual ~NodeStore () {}

    enum TimeState { LiveTime, ReplayTime };

    virtual void SetConfigurationSlaw (const Slaw s) = 0;
    virtual ObTrove<Inculcator *> Inculcators () = 0;

    // Add node should replace existing nodes, updating will come separately
    virtual void AddNode (Node *n, const float64 t) = 0;
    virtual void RemoveNode (Node *n, const float64 t) = 0;

    virtual void AddInculcators (const Slaw inculs) = 0;
    virtual void RemoveInculcators (const Slaw inculs) = 0;

    virtual const int64 KindsCount () const = 0;
    virtual void KindsPSA () const = 0;

    virtual Node *FindByIdent (const Str x) const = 0;

    virtual const void FindByLatLonRange (const LatLon bl,
                                          const LatLon tr,
                                          const float64 visible_degrees,
                                          std::set<Node *> &out) = 0;

    virtual const void SubsetByLatLonRange (const LatLon bl,
                                            const LatLon tr,
                                            const float64 visible_degrees,
                                            GeoSubset &subset) = 0;

    virtual void InitializePoolRegistration (Hasselhoff *hoff) = 0;

    virtual ObRetort SetSluiceTime (const float64 x) = 0;
    virtual ObRetort SetSluiceTime (const Str time_str) = 0;
    virtual const float64 SluiceTime () = 0;
    virtual const bool QueryOnLiveTime () const = 0;

    virtual ObRetort SetSluiceSecondsPerSecond (const float64 x) = 0;
    virtual const float64 SluiceSecondsPerSecond () const = 0;

    virtual const float64 MaxTime () = 0;
    virtual const float64 MinTime () = 0;
    virtual bool IsTimePaused () const = 0;
    virtual ObRetort PauseTime () = 0;
    virtual ObRetort UnPauseTime () = 0;
    virtual ObRetort UseLiveTime () = 0;

    virtual Slaw TimeChangeNotificationSlaw () = 0;
    virtual void SendTimeChangeNotificationProtein () = 0;

    virtual ObRetort ParseDate (const Str &s, float64 &when) = 0;

    /// Determine whether we have crossed in to a new "zoom level"
    virtual const bool
    CrossesZoomLevelBoundary (const float64 visdeg_a,
                              const float64 visdeg_b) const = 0;
    virtual bool GranularityForVisibleDegrees (const float64 visdeg,
                                               float64 &output) const {
        return false;
    }

    virtual const bool IsActiveForVisibleDegrees (Node *n,
                                                  const float64 zoom) = 0;

    static const float64 NowGMT ();

    virtual void ForceZoomLevel (const Str &) = 0;
    virtual Str ForcedZoomLevel () const = 0;
    virtual ObRetort AdjustPausedTime (const float64 prev_max_time) = 0;
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_NODESTORE_H
