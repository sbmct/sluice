
#include "StaticImageFluoroscope.h"
#include "GeoTexQuad.h"
#include "SluiceConsts.h"
#include "YaroSneeze.h"

#include "ConfigSlawStuff.h"

#include <libMedia/PngImageClot.h>
#include <libMedia/JpegImageClot.h>

using namespace oblong::sluice;
using namespace oblong::media;

StaticImageFluoroscope::StaticImageFluoroscope ()
    : Fluoroscope (), texture_wait_threshold (5.0), rebase_wait_minimum (0.1),
      gtex (new GeoTex (new MercProj, NULL)) {
    SetShouldInhale (true);
    InstallNotificationColor (new SoftColor (ObColor (1.0, 0.5)));
    GeoQuad *gq = new GeoTexQuad;
    SetGQuad (gq);
    SetKnowsVisibleBounds (false);
    SetShouldRefreshPeriodically (false);
    SetRebaseRequested (false);
    SetWaitingForTexture (false);
    SetDisableNotifications (false);
}

void StaticImageFluoroscope::SetFadeInTime (const float64 t) {
    if (GeoTexQuad *gq = dynamic_cast<GeoTexQuad *> (GQuad ())) {
        gq->InstallAdjColor (new AsympColor (ObColor (1.0, 1.0), t));
        gq->AdjColorSoft ()->SetHard (ObColor (1.0, 0.0));
        gq->AdjColorSoft ()->SetGoal (ObColor (1.0, 1.0));
    }
}

ObRetort StaticImageFluoroscope::SetConfigurationSlaw (const Slaw config) {
    ObRetort tort = super::SetConfigurationSlaw (config);
    if (tort.IsSplend ()) {
        if (config.MapFind ("update-frequency").Into (refresh_frequency))
            SetShouldRefreshPeriodically (true);
        else
            SetShouldRefreshPeriodically (false);

        bool tmp = false;
        SetVerboseRequests (config.Find ("verbose-request").Into (tmp) ? tmp
                                                                       : false);
        SetIgnoreMovement (config.Find ("ignore-movement").Into (tmp) ? tmp
                                                                      : false);
        SetTreatImageAsStatic (
            config.Find ("image-is-static").Into (tmp) ? tmp : false);
        SetDisableNotifications (
            config.Find ("disable-notifications").Into (tmp) ? tmp : false);
        Str new_format = "disable";
        new_format = ReadAttribute (config, "enable-datastore", new_format)
                         ? new_format
                         : "disable";

        float64 texture_wait = 5.0;
        if (ReadAttribute (config, "texture-wait", texture_wait))
            SetTextureWaitThreshold (texture_wait);
    }
    return tort;
}

const float64 StaticImageFluoroscope::TextureWaitThreshold () const {
    return texture_wait_threshold;
}
void StaticImageFluoroscope::SetTextureWaitThreshold (const float64 x) {
    texture_wait_threshold = x;
}

void StaticImageFluoroscope::SetImageClot (ImageClot *clot,
                                           const LatLon &bl,
                                           const LatLon &tr) {
    GeoTexQuad *gq = dynamic_cast<GeoTexQuad *> (GQuad ());
    if (gq) { // In the case our image is RGB, then use a special update with
              // proper GL format
        if (clot->NumComponents () == 3)
            gtex->Update (clot, GL_RGBA, GL_RGB);
        else
            gtex->Update (clot);

        gtex->SetGeoCorners (bl, tr);

        GeoTexQuad::Tex_Proj_Policy proj_policy = QueryTreatImageAsStatic ()
                                                      ? GeoTexQuad::Static
                                                      : GeoTexQuad::GeoProject;

        gq->SetGeoTexture (gtex, proj_policy);

        full_bl = bl;
        full_tr = tr;

        SetKnowsVisibleBounds (VisibleBounds (visible_bounds).IsSplend ());
    }
}

StaticImageFluoroscope::~StaticImageFluoroscope () {}

static const int64 default_img_height = 1080; // in pixels
static const int64 max_img_width = 4096;      // in pixels

ObRetort StaticImageFluoroscope::RequestProtein (Protein &out_prot) {
    Slaw conf = ConfigurationSlaw ();
    Str fluoro_type;
    if (!conf.MapFind ("daemon").Into (fluoro_type))
        return OB_INVALID_ARGUMENT;

    Slaw des = SluiceConsts::TextureRequestDescrips (ObQID_ToSlaw (QID ()),
                                                     fluoro_type);

    GeoRect bounds;
    ObRetort tort = VisibleBounds (bounds);
    if (!tort.IsSplend ())
        return tort;

    // Calculate appropriate pixel texture sizes from the visible bounds
    float64 tr_x, tr_y, bl_x, bl_y;
    GQuad ()->Projection ()->LatLonToCarto (bounds.tr.lat, bounds.tr.lon, tr_x,
                                            tr_y);
    GQuad ()->Projection ()->LatLonToCarto (bounds.bl.lat, bounds.bl.lon, bl_x,
                                            bl_y);
    v2float64 aspect_ratio_size = {
        default_img_height * (tr_x - bl_x) / (tr_y - bl_y), default_img_height};
    // Clamp the width to 4096 pixels because of possible texture size
    // contraints.
    // Adjust the height accordingly.
    if (aspect_ratio_size.x > max_img_width) {
        float64 scale = aspect_ratio_size.x / 4096.0;
        aspect_ratio_size.x = max_img_width;
        aspect_ratio_size.y /= scale;
    }

    GeoRect b = ClampToSemiReasonable (bounds.ScaleBy (2.0));

    Str style;
    ObTrove<Str> stylez;
    if (ReadAttribute (conf, "style", stylez))
        style = Join (stylez, ",");

    Slaw ing = Slaw::Map (
        "tr", Slaw::List (b.tr.lat, b.tr.lon), "bl",
        Slaw::List (b.bl.lat, b.bl.lon), "style", style, "zoom", GetMapZoom (),
        "tilezoom", GetTileZoom (), "px",
        Slaw::List ((int64)aspect_ratio_size.x, (int64)aspect_ratio_size.y),
        "name", Str ().Sprintf ("image-%p", this));

    if (NodeStore *store = ~node_store) {
        ing = ing.MapPut ("time", store->SluiceTime ())
                  .MapPut ("live", store->QueryOnLiveTime ());
    }

    if (QueryVerboseRequests ()) {
        ing = ing.MapPut ("config", ConfigurationSlaw ());
    }

    out_prot = Protein (des, ing);
    return OB_OK;
}

void StaticImageFluoroscope::InitializePoolRegistration (Hasselhoff *hoff) {
    super::InitializePoolRegistration (hoff);

    hoff->PoolParticipate (SluiceConsts::FluoroResponseLogicalHoseName (),
                           this);

    AppendMetabolizer (SluiceConsts::TextureResponseDescrips (),
                       &StaticImageFluoroscope::MetabolizeTextureUpdate,
                       Str ().Sprintf ("texture-updates-%p", this));
    // AppendMetabolizer (SluiceConsts::FluoroscopesRequestDescrips (),
    //                    &Fluoroscope::MetabolizeFluoroRequest,
    //                    "fluoro-request");
}

static Str tex_typ ("type");
static Str tex_dat ("bytes");
static Str tex_png ("png");
static Str tex_jpg ("jpg");
static Str tex_sta ("status");
static Str tex_tor ("tort");
static Str tex_msg ("msg");

// TODO: can this be made smaller?
ObRetort StaticImageFluoroscope::MetabolizeTextureUpdate (const Protein &prt,
                                                          Atmosphere *atm) {
    if (!prt.Descrips ().ListContains (ObQID_ToSlaw (QID ())))
        return OB_OK;
    // TODO: Is this really necessary? If it has our QID, it's *probably*
    // ours....
    // if (! prt . Descrips () . ListContains (TypeString ()))
    //   return OB_OK;

    Slaw ing = prt.Ingests ();
    int64 tort;
    if (ing.Find (tex_sta).Find (tex_tor).Into (tort)) {
        if (tort < 0) {
            Str err;
            if (ing.Find (tex_sta).Find (tex_msg).Into (err))
                SetError (err);

            if (GeoQuad *gq = GQuad ())
                gq->SetShouldDraw (false);
            SetWaitingForTexture (false);
            return OB_OK;
        } else {
            DismissError ();
            if (GeoQuad *gq = GQuad ())
                gq->SetShouldDraw (true);
        }
    }

    Str img_typ;
    if (!(ing.Find (tex_typ).Into (img_typ))) {
        OB_LOG_DEBUG ("MetabolizeTextureUpdate::"
                      "Missing type identifier (%s)",
                      tex_typ.utf8 ());
        return OB_OK;
    }

    Slaw img_s = ing.Find (tex_dat);
    if (img_s.IsNull ()) {
        OB_LOG_DEBUG ("MetabolizeTextureUpdate::"
                      "Missing pixel data (%s)",
                      tex_dat.utf8 ());
        return OB_OK;
    }

    ImageClot *im = NULL;
    img_typ = img_typ.Downcase ();
    if (img_typ == tex_png)
        im = new PngImageClot (img_s.SlawValue ());
    else if (img_typ == tex_jpg)
        im = new JpegImageClot (img_s.SlawValue ());
    else {
        OB_LOG_DEBUG ("MetabolizeTextureUpdate::"
                      "unsupported image type %s",
                      img_typ.utf8 ());
        return OB_OK;
    }

    SetWaitingForTexture (false);
    InstallNotificationColor (new SoftColor (ObColor (1.0, 0.0)));
    (~msg_banner)->Hide ();

    float64 bllat, bllon, trlat, trlon;

    if (ing.MapFind ("tr").Nth (0).Into (trlat) &&
        ing.MapFind ("tr").Nth (1).Into (trlon) &&
        ing.MapFind ("bl").Nth (0).Into (bllat) &&
        ing.MapFind ("bl").Nth (1).Into (bllon)) {
        LatLon tr (trlat, trlon), bl (bllat, bllon);
        SetImageClot (im, bl, tr);
        OB_LOG_DEBUG ("setting image to lats[%f, %f] lons[%f, %f]\n", trlat,
                      bllat, trlon, bllon);
    } else {
        Str keys = "", key, val;
        for (int i = 0; i < ing.Count (); i += 2) {
            if (ing.Nth (i).Into (key)) {
                if ("bytes" == key)
                    keys += key + "; ";
                else if (ing.Nth (1 + i).Into (val))
                    keys += key + ": " + val + "; ";
            }
        }
        OB_LOG_DEBUG ("Malformed texture protein: %s", keys.utf8 ());
    }

    DismissError ();
    GQuad ()->SetShouldDraw (true);

    return OB_OK;
}

const bool StaticImageFluoroscope::VisibleSizeHasDoubledOrHalved () {
    bool ret = false;
    if (QueryKnowsVisibleBounds ()) {
        GeoRect current;
        if (VisibleBounds (current).IsSplend ()) {
            float64 old = visible_bounds.Width () * visible_bounds.Height ();
            float64 now = current.Width () * current.Height ();
            ret = (2.0 * now < old) || (2.0 * old < now);
        }
    }
    return ret;
}

const bool StaticImageFluoroscope::ViewportIsNearTheEdgeOfOurTexture () {
    LatLon bl = WrldPosToLatLon (LocOf (V::Bottom, H::Left));
    LatLon tr = WrldPosToLatLon (LocOf (V::Top, H::Right));
    return (NULL == dynamic_cast<GeoTexQuad *> (GQuad ()))
               ? false
               : bl.lat < full_bl.lat || bl.lon < full_bl.lon ||
                     tr.lat > full_tr.lat || tr.lon > full_tr.lon;
}

ObRetort StaticImageFluoroscope::AcknowledgeGeoQuadSizeChanged (GeoQuad *gq) {
    if (gq == GQuad ())
        if (ShouldRebaseDueToMovement ())
            Rebase ();
    return super::AcknowledgeGeoQuadSizeChanged (gq);
}

ObRetort
StaticImageFluoroscope::AcknowledgeGeoQuadLocationChanged (GeoQuad *gq) {
    if (gq == GQuad ())
        if (ShouldRebaseDueToMovement ())
            Rebase ();
    return super::AcknowledgeGeoQuadLocationChanged (gq);
}

const bool StaticImageFluoroscope::ShouldRebaseDueToMovement () {
    return (!QueryIgnoreMovement ()) && (ViewportIsNearTheEdgeOfOurTexture () ||
                                         VisibleSizeHasDoubledOrHalved ());
}

ObRetort StaticImageFluoroscope::AcknowledgeCartoCoordsChanged (GeoQuad *gq) {
    if (gq == GQuad ())
        if (ShouldRebaseDueToMovement ())
            Rebase ();
    return super::AcknowledgeCartoCoordsChanged (gq);
    return OB_OK;
}

void StaticImageFluoroscope::SendToPool (Protein &prot, Str hose) {
    if (YaroSneeze *sneeze = ClosestParent<YaroSneeze> ()) {
        if (Hasselhoff *hoff = sneeze->OurHasselhoff ()) {
            if (Chiklis *shield = sneeze->OurChiklis ()) {
                hoff->PoolDeposit (hose, shield->DecorateProtein (prot));
            }
        }
    }
}

void StaticImageFluoroscope::SendTextureRequestToPool (Protein &prot) {
    Str hose = SluiceConsts::FluoroRequestLogicalHoseName ();
    SendToPool (prot, hose);
}

void StaticImageFluoroscope::RequestNewTexture () {
    Protein p;
    ObRetort tort = RequestProtein (p);
    if (tort.IsSplend () && !p.IsNull ()) {
        SendTextureRequestToPool (p);

        SetWaitingForTexture (true);
        // TODO: Is using AdjColor the best way to go about things?
        if (!QueryDisableNotifications ()) {
            InstallNotificationColor (
                new SineColor (ObColor (0.0, 1.0, 0.0, 0.75),
                               ObColor (0.0, 0.0, 0.0, 0.10), 1.0, 1.0));
            (~msg_banner)->SetMessage ("L O A D I N G");
            (~msg_banner)->SetBackgroundLiveliness (true);
            (~msg_banner)->SetShouldRepeatText (true);
            (~msg_banner)->Show ();
        }
        update_timer.ZeroTime ();
        if (NodeStore *store = ~node_store) {
            last_refresh_time = store->SluiceTime ();
        } else {
            last_refresh_time = 0.0;
        }
    }
    SetRebaseRequested (false);
}

void StaticImageFluoroscope::Rebase () {
    super::Rebase ();
    SetRebaseRequested (true);
    rebase_timer.ZeroTime ();
}

const bool StaticImageFluoroscope::ShouldRequestNewTexture () {
    float64 s_time = 0.0;

    if (NodeStore *store = ~node_store) {
        s_time = store->SluiceTime ();
    }

    bool timeToRebase = (QueryRebaseRequested () &&
                         (rebase_timer.CurTime () > rebase_wait_minimum));

    bool timeToRefresh =
        (QueryShouldRefreshPeriodically () &&
         (refresh_frequency < fabs (s_time - last_refresh_time)));

    return timeToRebase || timeToRefresh;
}

ObRetort StaticImageFluoroscope::Inhale (Atmosphere *atmo) {
    if (QueryWaitingForTexture ()) {
        if (!QueryDisableNotifications ()) {
            if (texture_wait_threshold < update_timer.CurTime ()) {
                InstallNotificationColor (
                    new SineColor (ObColor (1.0, 0.0, 0.0, 0.75),
                                   ObColor (0.0, 0.0, 0.0, 0.10), 1.0, 1.0));
                SetWaitingForTexture (false);
                (~msg_banner)->SetMessage ("U N A V A I L A B L E");
                (~msg_banner)->SetBackgroundLiveliness (false);
            }
        }
    }

    if (ShouldRequestNewTexture ()) {
        RequestNewTexture ();
    }

    return super::Inhale (atmo);
}

void StaticImageFluoroscope::DrawSelf (VisiFeld *vf, Atmosphere *atmo) {
    super::DrawSelf (vf, atmo);
    // OB_glColor (NotificationColor ());
    // glBegin (GL_QUADS);
    // { OB_glVertex (LocOf (V::Top, H::Left));
    //   OB_glVertex (LocOf (V::Top, H::Right));
    //   OB_glVertex (LocOf (V::Bottom, H::Right));
    //   OB_glVertex (LocOf (V::Bottom, H::Left));
    // }
    // glEnd ();
}
