
#pragma once

#include "Shader.h"
#include "NodeBuffer.h"
#include "RenderingInstructions.h"
#include "TopoPigment.h"
#include "GLQuad.h"
#include "Shader.h"
#include <libNoodoo/GLTex.h>

#include <set>
#include <vector>

namespace oblong {
namespace sluice {

class Node;

struct PigSackPod {
    PigSackPod (Str _name, topo::Pigment _pig) : name (_name), pigment (_pig) {}

    Str name, tex_path;
    topo::Pigment pigment;

    const bool operator<(const PigSackPod &other) const;
    const bool operator>(const PigSackPod &other) const;
    const bool operator==(const PigSackPod &other) const;
    const bool operator!=(const PigSackPod &other) const;
};

/*
The PigmentSack has the pigment and the collection of nodes that
belong to the pigment
*/
class PigmentSack : public KneeObject {
    PATELLA_SUBCLASS (PigmentSack, KneeObject);

protected:
    bool vbo_is_fresh;

    virtual ObRetort Recalc (GeoProj *proj, const float64 t) = 0;

public:
    PigmentSack ();

    topo::Pigment pigment;

    FLAG_MACHINERY (PigmentIsDirty);
    conduce::sluice::PigmentBuffer pigment_vbo_buffer;

    static PigmentSack *FromPod (const PigSackPod pod);

    virtual bool AddNode (Node *n) { return false; };
    virtual bool RemoveNode (Node *n) { return false; };
    virtual bool HasNode (Node *) const = 0;
    /// Bulk add/remove nodes

    virtual void HardReset () = 0;
    virtual bool IsEmpty () const = 0;
    virtual bool IsReady () const = 0;

    /** WARNING! This does NOT generate a Slaw that can be correctly
     *  read back in as a pigment!!!  At least, not yet.
     *  It is intended to be used for debugging.
     */
    virtual Slaw DescribeAsSlaw () const;
};

class PointPigmentSack : public PigmentSack {
    PATELLA_SUBCLASS (PointPigmentSack, PigmentSack);

protected:
    virtual ObRetort Recalc (GeoProj *proj, const float64 t);

    ObTrove<NodeBuffer *> buffers;

public:
    PointPigmentSack ();

    std::map<Node *, NodeBuffer *> buffer_map;

    virtual bool AddNode (Node *n);
    virtual bool RemoveNode (Node *n);
    virtual bool HasNode (Node *) const;

    virtual void HardReset ();
    virtual bool IsEmpty () const;
    virtual bool IsReady () const;

    /** WARNING! This does NOT generate a Slaw that can be correctly
     *  read back in as a pigment!!!
     *  It is intended to be used for debugging.
     */
    virtual Slaw DescribeAsSlaw () const;
};

class PathPigmentSack : public PigmentSack {
    PATELLA_SUBCLASS (PathPigmentSack, PigmentSack);

protected:
    ObTrove<PathBuffer *> buffers;

    virtual ObRetort Recalc (GeoProj *proj, const float64 t);

public:
    PathPigmentSack ();

    std::map<Node *, PathBuffer *> buffer_map;
    virtual bool AddNode (Node *n);
    virtual bool RemoveNode (Node *n);
    virtual bool HasNode (Node *) const;

    virtual void HardReset ();
    virtual bool IsEmpty () const;
    virtual bool IsReady () const;

    /** WARNING! This does NOT generate a Slaw that can be correctly
     *  read back in as a pigment!!!
     *  It is intended to be used for debugging.
     */
    virtual Slaw DescribeAsSlaw () const;
};
}
}
