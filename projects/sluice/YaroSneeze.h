
/* (c)  oblong industries */

#ifndef YARO_SNEEZE_EXPULSIONS
#define YARO_SNEEZE_EXPULSIONS

#include <libImpetus/OEComprehensive.h>
#include <projects/quartermaster/TriptychConcierge.h>

#include "Acetate.h"

#include "Dossier.h"
#include "HandiPoint.h"
#include "Hasselhoff.h"
#include "QuadriPlunderer.h"
#include "SluiceConsts.h"
#include "Windshield.h"
#include "Chiklis.h"

#include "AppBucket.h"

#include "UIDSpigot.h"

#include "PassforwardGlimpser.h"

#include <libGanglia/Ernestine.h>

#include <libImpetus/OEBlurt.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class YaroSneeze : public Acetate, public SwitchboardAgent {
    PATELLA_SUBCLASS (YaroSneeze, Acetate);

public:
    enum FeldStreamState {
        No_Felds,
        Center_Feld,
        Left_Feld,
        Right_Feld,
        All_Felds
    };

private:
    FeldStreamState feld_stream_state;
    ObRef<TriptychConcierge *> tychco;

protected:
    ObRef<Windshield *> windy;
    ObRef<Dossier *> dossy;

    ObRef<Ernestine *> focus_hog;

    ObRef<Hasselhoff *> pool_boy;

    ObRef<Chiklis *> shield;

    ObRef<AppBucket *> buck;

    ObRef<PassforwardGlimpser *> pfw_glim;
    ObRef<QuadriPlunderer *> plunderer;

    Vect feld_n, feld_o, feld_u;
    Vect feld_cent;
    float64 feld_w, feld_h;
    float64 view_dist, near_dist, far_dist;
    float64 mullion_w;
    int32 pix_wid, pix_hei;

    /// (provenance) owner of all current shove interactions
    Str shove_owner;
    Vect shove_direction;
    float64 shove_deadzone;
    float64 shove_detent;
    Vect shove_displacement_appear_loc;

    FatherTime native_time;

    bool scheduled_dossier_closure : 1;

    struct ProvAndTid {
        Str prov;
        int64 tid;
    };

    struct ClientIlk {
        ObTrove<Str> client_types;
        int32 client_count_limit;
        int32 client_count;
        ClientIlk (ObTrove<Str> c_types, const int32 &limit) {
            client_types = c_types;
            client_count_limit = limit;
            client_count = 0;
        }
    };

    ObTrove<ClientIlk *> client_groups;

    ObMap<Str, float64> client_timestamp_by_prov;

public:
    YaroSneeze ();
    virtual ~YaroSneeze ();

    void EstablishGeometry (const Vect &n,
                            const Vect &o,
                            const Vect &u,
                            const Vect &c,
                            float64 w,
                            float64 h,
                            float64 view_d,
                            float64 near_d,
                            float64 far_d,
                            float64 mul_wid,
                            int32 pix_w = 20,
                            int32 pix_h = 15);

    Dossier *OurDossier () const { return ~dossy; }
    void InstallDossier (Dossier *dos, Atmosphere *atm = NULL);

    Windshield *OurWindshield () const { return ~windy; }
    void InstallWindshield (Windshield *wnd, Atmosphere *atm = NULL);

    Ernestine *OurFocusHog () const { return ~focus_hog; }
    void InstallFocusHog (Ernestine *fh, Atmosphere *atm = NULL);

    Hasselhoff *OurHasselhoff () const { return ~pool_boy; }
    void InstallHasselhoff (Hasselhoff *hoff, Atmosphere *atm = NULL);

    Chiklis *OurChiklis () const { return ~shield; }
    void InstallChiklis (Chiklis *chik, Atmosphere *atm = NULL);

    AppBucket *OurAppBucket () const { return ~buck; }
    void InstallAppBucket (AppBucket *bck, Atmosphere *atm = NULL);

    PassforwardGlimpser *OurPassforwardGlimpser () { return ~pfw_glim; }
    void InstallPassforwardGlimpser (PassforwardGlimpser *pg,
                                     Atmosphere *atm = NULL);

    QuadriPlunderer *OurQuadriPlunderer () { return ~plunderer; }
    void InstallQuadriPlunderer (QuadriPlunderer *qp);

    TriptychConcierge *OurTriptychConcierge () const { return ~tychco; }

    float64 NativeTime () { return native_time.CurTime (); }

    const Str &CurDossierUID () const {
        if (Dossier *ier = OurDossier ())
            return ier->UID ();
        static Str doss_null;
        return doss_null;
    }

    // Populates the supplied arguments with the point representing the lower
    // left corner of the triptych, as well as vectors over and up which
    // indicate
    // both its orientation and size. Returns true on success.
    void TriptychRect (Vect &corner, Vect &over, Vect &up) const;
    virtual ObRef<FeldGeomPod *> RetrieveGeometry () const;

    virtual void ArrangeDenizens ();

    ObRetort WireUpDossier ();

    virtual ObRetort ConsignToParamus (KneeObject *ko);
    virtual bool CanConsignToParamus ();

    ObRetort ReceiveVidYankingProtein (const Protein &vmp, Atmosphere *atm);
    ObRetort ReceiveVidHurlingProtein (const Protein &vmp, Atmosphere *atm);

    ObRetort OpenDossier (const Str &doss_to_load, Atmosphere *atm);
    ObRetort CloseDossier (Atmosphere *atm);
    ObRetort ScheduleDossierClosure (Atmosphere *atm);

    ObRetort ConsiderClientMummifyRequest (const Protein &cddkr,
                                           Atmosphere *atm);
    ObRetort ConsiderClientVivifyRequest (const Protein &cddkr,
                                          Atmosphere *atm);

    // Protein GenerateP5150 (const Str &fname, const Str &asst);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm) {
        super::DrawSelf (vf, atm);
    }

    virtual ObRetort Exhale (Atmosphere *atm);

    //
    // the following is submitted in partial fulfillment of the
    // SwitchboardAgent PhD.
    //
    virtual ObRetort RouteEvent (ObEvent *e, Atmosphere *atm);

    virtual KneeObject *
    NthTargetForEvent (ObEvent *e, int64 n, Atmosphere *atm);

    virtual RelayPath *RPathForProvenance (const Str &prov, Atmosphere *atm);

    virtual ObRetort
    SetRPathForProvenance (const Str &prov, RelayPath *rp, Atmosphere *atm);
    virtual ObRetort ClearRPathForProvenance (const Str &prov, Atmosphere *atm);

    static bool EntityInDeletableLatitudes (const OEPointingEvent *e,
                                            const Vect &upp);

    /// Client participation/requests handling

    Slaw MinimalDeckStateMap ();
    Slaw DossierStateMap ();
    ObRetort BroadcastDeckState (Atmosphere *atm);

    int32 ClientCount () { return client_timestamp_by_prov.Count (); }
    /// number of active connections from clients of a particular type
    /// e.g. browser or iPod
    int32 ClientCount (const Str &client_type);

    int32 ClientCountLimit (const Str &client_type);
    void SetClientCountLimit (const Str &client_type, int32 ccl);
    ClientIlk *FindClientIlk (const Str &client_type);
    Str ExtractClientTypeFromProvenance (const Str &prv);

    static bool ExtractClientProvAndTid (const Protein &pr, ProvAndTid &pat);
    bool ValidateClientParticipation (const Str &prov) {
        return client_timestamp_by_prov.KeyIsPresent (prov);
    }

    HandiPoint *AllocateHandiPointForClient (const Str &prv);

    ObRetort ReceiveSynchroSystemProtein (const Protein &ssp, Atmosphere *atm);

    void RetireClient (const Str &client_prov);

    Slaw FeldsSlawMap ();

    void ProclaimSnapshotPost (const Str &ast_path);

    ObRetort DispatchClientRequest (const Protein &crp,
                                    const ProvAndTid &pat,
                                    Atmosphere *atm);

    ObRetort ConsiderClientJoinRequest (const Protein &cjr,
                                        const ProvAndTid &pat,
                                        Atmosphere *atm);
    ObRetort ConsiderClientHeartbeat (const Protein &chp,
                                      const ProvAndTid &at,
                                      Atmosphere *atm);
    ObRetort ConsiderClientStateRequest (const Protein &csr,
                                         const ProvAndTid &pat,
                                         Atmosphere *atm);
    ObRetort ConsiderClientPushbackRequest (const Protein &cpr,
                                            const ProvAndTid &pat,
                                            Atmosphere *atm);
    ObRetort ConsiderClientRatchetRequest (const Protein &crr,
                                           const ProvAndTid &pat,
                                           Atmosphere *atm);
    ObRetort ConsiderClientClearWindshieldRequest (const Protein &ccwr,
                                                   const ProvAndTid &pat,
                                                   Atmosphere *atm);
    ObRetort ConsiderClientClearFluoroscopesRequest (const Protein &ccfl,
                                                     const ProvAndTid &pat,
                                                     Atmosphere *atm);
    ObRetort ConsiderClientFeldStreamStateRequest (const Protein &cfssr,
                                                   const ProvAndTid &pat,
                                                   Atmosphere *atm);

    void SetFeldStreamState (const Str &new_state);
    void SetFeldStreamState (const FeldStreamState &new_state);
    FeldStreamState CurFeldStreamState (Str *state) const;
    void TurnOffFeldStream ();
    void AdjustFeldStream ();
    void StopStreamingProcesses ();
    void StartStreamingProcesses ();
    void DepositStartProcessProtein (const Str &feld_pool,
                                     const Str &feld_protein);
};
}
} // we hardly knew ye, namespaces sluice and oblong!

#endif
