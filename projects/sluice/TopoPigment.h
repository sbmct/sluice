
// (c) Conduce Inc.

#pragma once

#include <libLoam/c++/Str.h>
#include <libPlasma/c++/Slaw.h>
#include <vector>
#include <map>

#include "Subscription.h"

namespace conduce {
namespace sluice {

// Write data from each subscription node in to one of these structures
class PigmentBuffer {
public:
    using vtype = std::vector<float32>;

    PigmentBuffer () = default;
    PigmentBuffer (const PigmentBuffer &) = default;
    PigmentBuffer &operator=(const PigmentBuffer &) = default;
    PigmentBuffer (PigmentBuffer &&) = default;
    PigmentBuffer &operator=(PigmentBuffer &&) = default;

    void clear () {
        verts.clear ();
        texs.clear ();
        attrs.clear ();
    }

    void addVert (const float32 x) { verts.push_back (x); }
    void addTex (const float32 x) { texs.push_back (x); }
    void addAttr (const oblong::loam::Str &name, const float32 x) {
        attrs[name].push_back (x);
    }

    std::vector<oblong::loam::Str> getAttrs () const {
        std::vector<oblong::loam::Str> out;
        out.reserve (attrs.size ());
        for (auto &&pair : attrs) {
            out.push_back (pair.first);
        }
        return out;
    }

    size_t n_verts () const { return verts.size (); }
    size_t n_texs () const { return texs.size (); }
    size_t n_attrs (oblong::loam::Str &name) const {
        auto found = attrs.find (name);
        if (found != attrs.end ()) {
            return found->second.size ();
        }
        return 0;
    }

    size_t size () const {
        size_t out = n_verts () + n_texs ();
        for (auto &&pair : attrs) {
            out += pair.second.size ();
        }
        return out;
    }

    void writeVerts (vtype &target) const {
        for (auto &&x : verts) {
            target.push_back (x);
        }
    }

    void writeTexs (vtype &target) const {
        for (auto &&x : texs) {
            target.push_back (x);
        }
    }

    void writeAttr (const oblong::loam::Str &name, vtype &target) const {
        auto found = attrs.find (name);
        if (found != attrs.end ()) {
            for (auto &&x : found->second) {
                target.push_back (x);
            }
        }
    }

private:
    vtype verts{}, texs{};
    std::map<oblong::loam::Str, vtype> attrs{};
};

// For each pigment, calculate the absolute offsets for each
// rendering section once.
class PigmentOffsets {
private:
    size_t vert_offset{0}, tex_offset{0};
    std::map<oblong::loam::Str, size_t> attr_offset{};

public:
    PigmentOffsets () = default;
    PigmentOffsets (const PigmentOffsets &) = default;
    PigmentOffsets &operator=(const PigmentOffsets &) = default;
    PigmentOffsets (PigmentOffsets &&) = default;
    PigmentOffsets &operator=(PigmentOffsets &&) = default;

    void setVertOffset (const size_t x) { vert_offset = x; }
    void setTexOffset (const size_t x) { tex_offset = x; }
    void setAttrOffset (const oblong::loam::Str &name, const size_t x) {
        attr_offset[name] = x;
    }

    size_t getVertOffset () const { return vert_offset; }
    size_t getTexOffset () const { return tex_offset; }
    size_t getAttrOffset (const oblong::loam::Str &name) const {
        auto found = attr_offset.find (name);
        if (found != attr_offset.end ()) {
            return found->second;
        }
        return 0;
    }

    void spew (std::ostream &) const;
};

inline std::ostream &operator<<(std::ostream &out, const PigmentOffsets &off) {
    off.spew (out);
    return out;
}

// Write all of the PigmentBuffers in to this structure
class BufferDescription {
private:
    std::vector<float32> buffer;
    size_t vert_offset, tex_offset;
    std::map<oblong::loam::Str, size_t> attr_offset;

public:
    const size_t getVertOffset () const { return vert_offset; }
    const size_t getTexOffset () const { return tex_offset; }
    const size_t getAttrOffset (const oblong::loam::Str &name) const {
        auto found = attr_offset.find (name);
        if (found != attr_offset.end ()) {
            return found->second;
        }
        return 0;
    }

    // Given a set of pigment buffers, populate a VBO and then
    // write out rendering descriptions for each pigment
    std::vector<PigmentOffsets>
    refresh (const std::vector<PigmentBuffer> &pigs);

    size_t size () const { return buffer.size (); }
    const std::vector<float32> &getBuffer () const { return buffer; }

    void writeToBoundArrayBuffer ();
};
}
}
