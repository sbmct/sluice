
/* (c)  oblong industries */

#ifndef VIDFLUX_IMP_SQUATTING_ON_YOUR_CHEST
#define VIDFLUX_IMP_SQUATTING_ON_YOUR_CHEST

#include <libNoodoo/TexQuad.h>

#include <projects/quartermaster/ViddleSet.h>

namespace oblong {
namespace sluice {
using namespace oblong::quartermaster;
using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class VidfluxImp : public TexQuad {
    PATELLA_SUBCLASS (VidfluxImp, TexQuad);

public:
    Viddle *rep_vdl;

public:
    VidfluxImp ();
    VidfluxImp (float64 wid, float64 hei);
    VidfluxImp (ImageClot *ic);
    VidfluxImp (ImageClot *ic,
                float64 wid,
                float64 hei,
                Geom_Fitting fit = Inscribe_Geom);

    ~VidfluxImp ();

    Viddle *AssociatedViddle () const { return rep_vdl; }
    ObRetort SetAssociatedViddle (Viddle *v);
};
}
} // namespaces sluice and oblong, fallen from grace...

#endif
