
/* (c)  oblong industries */

#ifndef ASSET_HUTCH_OF_STARSKYLESSNESS
#define ASSET_HUTCH_OF_STARSKYLESSNESS

#include "BuffedAcetate.h"

#include <libBasement/AsympVect.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class AssetGrip : public KneeObject {
    PATELLA_SUBCLASS (AssetGrip, KneeObject);

public:
    ObRef<KneeObject *> asset;
    int32 col, row;
    float64 wid, hei;
    ObRef<SoftVect *> there;

    AssetGrip () : KneeObject (), col (-2), row (-2), wid (10.0), hei (7.5) {
        AsympVect *av = new AsympVect;
        av->SetInterpTime (0.5);
        there = av;
    }
    AssetGrip (KneeObject *ast)
        : KneeObject (), col (-2), row (-2), wid (10.0), hei (7.5) {
        AsympVect *av = new AsympVect;
        av->SetInterpTime (0.5);
        there = av;
        SetAsset (ast);
    }
    virtual KneeObject *Asset () const { return ~asset; }
    virtual void SetAsset (KneeObject *ast) { asset = ast; }
};

class AssetHutch : public BuffedAcetate {
    PATELLA_SUBCLASS (AssetHutch, BuffedAcetate);

public:
    ObTrove<AssetGrip *> asset_grips;
    unt32 dom_capacity;
    float64 gap_fract, inset_fract;
    int32 ordinal;

public:
    AssetHutch ();
    virtual ~AssetHutch ();

    unt32 DominantDimensionCapacity () const { return dom_capacity; }
    void SetDominantDimensionCapacity (unt32 ddc) {
        dom_capacity = ddc;
        ArrangeDenizens ();
    }

    float64 GapFraction () const { return gap_fract; }
    void SetGapFraction (float64 gf) {
        gap_fract = gf;
        ArrangeDenizens ();
    }

    float64 InsetFraction () const { return inset_fract; }
    void SetInsetFraction (float64 ifr) {
        inset_fract = ifr;
        ArrangeDenizens ();
    }

    int32 Ordinal () const { return ordinal; }
    void SetOrdinal (int32 o) { ordinal = o; }

    void PutAssetGripThere (AssetGrip *ag,
                            AssetHutch *from_hutch = NULL,
                            bool set_hardly = false);

    virtual int64 AssetGripCount () const;
    virtual AssetGrip *NthAssetGrip (int64 ind) const;
    int64 FindAssetGrip (AssetGrip *ag) const;
    virtual AssetGrip *FindAssetGripByAsset (KneeObject *ko) const;
    virtual AssetGrip *FindAssetGripByRowAndCol (int64 r, int64 c) const;

    virtual void AppendAssetGrip (AssetGrip *assg,
                                  bool manhandle_row_and_col = true);
    virtual ObRetort RemoveAssetGrip (AssetGrip *ag,
                                      bool also_remove_asset = true);

    virtual ObRetort ScheduleChildRemoval (KneeObject *ko);

    virtual int32 OutermostLevel () const;
    virtual bool OutermostLevelIsFull () const;

    virtual AssetGrip *WhackedAssetGrip (const Vect &frm, const Vect &thr);

    void Empty ();

    virtual ObRetort Inhale (Atmosphere *atm);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);
};
}
} // we all knew the day'd come for namespaces sluice & oblong

#endif
