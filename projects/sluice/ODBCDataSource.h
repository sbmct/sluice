
/* (c)  oblong industries */

#ifndef ODBCDATASOURCE_H
#define ODBCDATASOURCE_H

#include <sql.h>
#include <sqlext.h>

#include <libLoam/c++/Str.h>
#include <libLoam/c++/ObMap.h>
#include <libPlasma/c++/Slaw.h>

using namespace oblong::loam;
using namespace oblong::plasma;

class ODBCDataSource {
public:
    typedef struct {
        Str name;
        int datatype;
    } SqlColumn;

    ODBCDataSource (Str connection_string);
    ~ODBCDataSource ();

    int Open ();
    void Close ();
    int Execute (Str statement,
                 ObTrove<Str> &colnames,
                 ObTrove<ObTrove<Str>> &coldata);
    int ExecuteProc (Str statement);
    Str LastError ();

private:
    void ExtractError (SQLHANDLE handle, SQLSMALLINT type);

    SQLHENV env;
    SQLHDBC dbc;
    Str last_error;
    Str connection_string;
};

#endif // ODBCDATASOURCE_H
