/* (c)  Oblong Industries */

#ifndef CTHULHUS_OBBY_LIKE_APP
#define CTHULHUS_OBBY_LIKE_APP

#include <libBasement/ob-logging-macros.h>
#include <libBasement/TWrangler.h>
#include <libBasement/LinearFloat.h>

#include <libNoodoo/TexQuad.h>
#include <libNoodoo/GlyphoString.h>

#include <libCthulhu/CthWhisperer.h>

#include "MzShove.h"
#include "Acetate.h"

namespace oblong {
namespace sluice {
using namespace oblong::cthulhu;
using namespace oblong::ganglia;
using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::loam;

class AppBucket : public Acetate, public MzShove::Evts {
    PATELLA_SUBCLASS (AppBucket, Acetate);

private:
    struct destination {
        Str d_id;
        ObRef<TexQuad *> tq;
        ObRef<GlyphoString *> gs;
    };
    ObTrove<destination *> bucket;

    ObRef<TexQuad *> wait_boog;
    ObRef<LinearFloat *> rot_ang;

    int64 cent_idx;

    Str pull_prov;

    ObRef<TWrangler *> pull_wrangler;
    ObRef<TWrangler *> shimmy_wrangler;

    float64 pullback_lock_dist;
    float64 shimmy_offset;

    Vect feld_n, feld_o, feld_u;
    Vect feld_cent;
    float64 feld_w, feld_h;
    float64 view_dist;
    float64 mullion_w;

public:
    FLAG_MACHINERY_FOR (CurrentlySelecting);
    FLAG_MACHINERY_FOR (Exiting);
    FLAG_MACHINERY_FOR (PullbackLocked);

    AppBucket (CthWhisperer *cth);

    void EstablishGeometry (const Vect &n,
                            const Vect &o,
                            const Vect &u,
                            const Vect &c,
                            float64 w,
                            float64 h,
                            float64 view_d,
                            float64 mul_wid);

    ObRetort Inhale (Atmosphere *atm);

    virtual ObRetort HandleAppListResponse (const Protein &prt, Atmosphere *a);
    virtual ObRetort HandleValveOpen (Atmosphere *a);

    virtual ObRetort MzShoveBegin (MzShoveBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzShoveContinue (MzShoveContinueEvent *e, Atmosphere *atm);
    virtual ObRetort MzShoveEnd (MzShoveEndEvent *e, Atmosphere *atm);

    void SetPullbackLockDistance (float64 dist) { pullback_lock_dist = dist; }
    float64 PullbackLockDistance () { return pullback_lock_dist; }

    Vect PullbackDisplacement () const;
    Vect PullbackDisplacementGoal () const;
    void SetPullbackDisplacement (const Vect &pd);
    void SetPullbackDisplacementHard (const Vect &pd);

    Vect ShimmyDisplacement () const;
    Vect ShimmyDisplacementGoal () const;
    void SetShimmyDisplacement (const Vect &pd);
    void SetShimmyDisplacementHard (const Vect &pd);

    void SettleToPullbackLock ();
    void ReleaseFromPullback ();
    void ShimmyToClosestAppCenter ();

protected:
    void RequestApps ();
    void ClearApps ();
    void HideApps ();
    void ShowApps ();
    void ShimmyLeft ();
    void ShimmyRight ();
};
}
} // bye bye namespaces mezzanine, oblong

#endif
