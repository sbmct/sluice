
/* (c)  oblong industries */

#ifndef LOCK_GLYPH_REELS_THEM_IN
#define LOCK_GLYPH_REELS_THEM_IN

#include <libNoodoo/TexQuad.h>
#include <libNoodoo/FlatThing.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class LockGlyph : public FlatThing {
    PATELLA_SUBCLASS (LockGlyph, FlatThing);

private:
    ObRef<TexQuad *> top_left_tex;
    ObRef<TexQuad *> top_right_tex;
    ObRef<TexQuad *> bottom_left_tex;
    ObRef<TexQuad *> bottom_right_tex;
    ObRef<TexQuad *> center_tex;

public:
    LockGlyph ();

    void SetSubglyphSize (const float64 &s);

    virtual ObRetort Travail (Atmosphere *atm);
};
}
} // a bittersweet goodbye to namespaces mezzanine, oblong

#endif
