
#ifndef GEO_TEX_QUAD_SEES_THE_WORLD_THROUGH_FF007F_TINTED_GLASSES
#define GEO_TEX_QUAD_SEES_THE_WORLD_THROUGH_FF007F_TINTED_GLASSES

#include "GeoTex.h"
#include "GeoQuad.h"
#include "SimpleTexRect.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;

/**
 * GeoTexQuad -->
 *
 * GeoTexQuad is a simple subclass of GeoQuad which knows how to map a
 * texture to its geometry. It allows for the basic texture options like
 * stretch, keepwidth, keepheight, etc., but it also accepts GeoTex
 * textures and will keep them geo-aligned as the GeoQuad projection
 * changes.
 *
 **/

/// Textures:
///
/// The idea is to have GeoTex instances mostly so that the texture can stay
/// fixed geographically as the GeoQuad's geometry moves (i.e. the texture
/// coordinates compensate for the projection differences). However, we don't
/// rule out the possibility of just static overlay images that always fill the
/// bounds of the GeoQuad. For starters, we can just use the class type to
/// differentiate between GeoTex projected instances and regular static TexGL
/// textures.
///
/// There's more to think about here, esp related to sizing, stretching, etc
/// of the non-geotex textures. Think TexQuad interfaces here...
///

class GeoTexQuad : public GeoQuad {
    PATELLA_SUBCLASS (GeoTexQuad, GeoQuad);

public:
    /// Texture Projection Policy
    /// Defines how this quad should treat its texture.
    ///  - GeoProject = if tex is a GeoTex, use the GeoProj's to get tex coors.
    ///  - GeoProjectWrap = same as above, but assumes full longitude range and
    ///                     sets up the texture to wrap its texture coordinates
    ///                     This would be the case for a full map.
    ///  - Static = straight up mapping texture to the full bounds
    enum Tex_Proj_Policy { GeoProject = 0, GeoProjectWrap, Static };

protected:
    ObRef<SimpleTexRect *> tex_rect;
    ObRef<GeoTex *> geo_tex;
    Tex_Proj_Policy tex_proj_policy;

public:
    GeoTexQuad ();

    void SetGeoTexture (GeoTex *texture);
    void SetGeoTexture (GeoTex *texture, Tex_Proj_Policy tpp);

    int TexProjPolicy () { return tex_proj_policy; }
    void SetTexProjPolicy (Tex_Proj_Policy tpp);

    virtual ObRetort AcknowledgeUVXformChange ();
    virtual ObRetort AcknowledgeLocationChange ();
    virtual ObRetort AcknowledgeSizeChange ();

    void Update (ImageClot *ic, int internalFormat, int format);

    /// Accessors
    SimpleTexRect *TexRect () { return ~tex_rect; }
    GeoTex *GeoTexture () { return ~geo_tex; }

    /// We need our tex_rect to shadow our Locus / FlatThing components.
    virtual void InstallLoc (SoftVect *sv);
    virtual void InstallNorm (SoftVect *sv);
    virtual void InstallOver (SoftVect *sv);

    virtual void InstallWidth (SoftFloat *sf);
    virtual void InstallHeight (SoftFloat *sf);
    virtual void InstallVerticalAlignment (SoftFloat *sf);
    virtual void InstallHorizontalAlignment (SoftFloat *sf);

protected:
    /// for internal use - should be called whenever the projection is updated.
    /// or, if we want to support GeoProjection while maintaining the image
    /// aspect
    /// ratio, it will need to be called when the size changes as well.
    virtual void RecalculateTexCoors ();
};
}
} /// end namespaces oblong, staging

#endif /// GEO_TEX_QUAD_SEES_THE_WORLD_THROUGH_FF007F_TINTED_GLASSES
