
#include "GeoMatrix.h"

#include <stdio.h>
#include <libLoam/c/ob-log.h>
#include <libLoam/c++/Vect.h>
#include <libLoam/c++/Vect4.h>

using namespace std;
using namespace oblong::loam;
using namespace oblong::sluice;

GeoMatrix::GeoMatrix () {
    mat.LoadIdent ();
    imat.LoadIdent ();
}

GeoMatrix::GeoMatrix (const Matrix44 &m, const Matrix44 &im) {
    mat = m;
    imat = im;
}

GeoMatrix GeoMatrix::Inverse () const { return GeoMatrix (imat, mat); }

GeoMatrix &GeoMatrix::LoadFromCorners (const float64 &x_bl,
                                       const float64 &y_bl,
                                       const float64 &x_tr,
                                       const float64 &y_tr) {
    Matrix44 sc, tr;

    sc.LoadScale (1.0 / (x_tr - x_bl), 1.0 / (y_tr - y_bl), 1.0);
    tr.LoadTranslation (-1.0 * (x_tr - 0.5 * (x_tr - x_bl)),
                        -1.0 * (y_tr - 0.5 * (y_tr - y_bl)), 0.0);

    mat.LoadIdent ();
    mat = sc * mat;
    mat = tr * mat;

    sc.LoadScale (1.0 * (x_tr - x_bl), 1.0 * (y_tr - y_bl), 1.0);
    tr.LoadTranslation (1.0 * (x_tr - 0.5 * (x_tr - x_bl)),
                        1.0 * (y_tr - 0.5 * (y_tr - y_bl)), 0.0);

    imat.LoadIdent ();
    imat *= sc;
    imat *= tr;
    return *this;
}

GeoMatrix &GeoMatrix::LoadFromCorners (const v2float64 &xy_bl,
                                       const v2float64 &xy_tr) {
    return LoadFromCorners (xy_bl.x, xy_bl.y, xy_tr.x, xy_tr.y);
}

GeoMatrix GeoMatrix::Scale (const float64 &sc) const { return Scale (sc, sc); }

GeoMatrix GeoMatrix::Scale (const float64 &sx, const float64 &sy) const {
    GeoMatrix m (*this);
    m.ScaleInPlace (sx, sy);
    return m;
}

GeoMatrix &GeoMatrix::ScaleInPlace (const float64 &sc) {
    return ScaleInPlace (sc, sc);
}

GeoMatrix &GeoMatrix::ScaleInPlace (const float64 &sx, const float64 &sy) {
    Matrix44 sc_mat;
    sc_mat.LoadScale (1.0f / sx, 1.0f / sy, 1.0);
    mat = sc_mat * mat;
    sc_mat.LoadScale (sx, sy, 1.0);
    imat *= sc_mat;
    return *this;
}

GeoMatrix GeoMatrix::ScaleAbout (const float64 &sc,
                                 const float64 &x_cntr,
                                 const float64 &y_cntr) const {
    GeoMatrix m (*this);
    m.ScaleAboutInPlace (sc, x_cntr, y_cntr);
    return m;
}

GeoMatrix GeoMatrix::ScaleAbout (const float64 &sx,
                                 const float64 &sy,
                                 const float64 &x_cntr,
                                 const float64 &y_cntr) const {
    GeoMatrix m (*this);
    m.ScaleAboutInPlace (sx, sy, x_cntr, y_cntr);
    return m;
}

GeoMatrix &GeoMatrix::ScaleAboutInPlace (const float64 &sc,
                                         const float64 &x_cntr,
                                         const float64 &y_cntr) {
    return ScaleAboutInPlace (sc, sc, x_cntr, y_cntr);
}

GeoMatrix &GeoMatrix::ScaleAboutInPlace (const float64 &sx,
                                         const float64 &sy,
                                         const float64 &x_cntr,
                                         const float64 &y_cntr) {
    TranslateInPlace (x_cntr, y_cntr);
    ScaleInPlace (sx, sy);
    return TranslateInPlace (-x_cntr, -y_cntr);
}

/**
GeoMatrix GeoMatrix::Rotate (const float64 &ang)  const;
GeoMatrix &GeoMatrix::RotateInPlace (const float64 &ang);

GeoMatrix GeoMatrix::RotateAbout (const float64 &ang, const float64 &rx, const
float64 &ry)  const;
GeoMatrix &GeoMatrix::RotateAboutInPlace (const float64 &ang, const float64 &rx,
const float64 &ry);
*/

/**
   * 2D Translation
   */
GeoMatrix GeoMatrix::Translate (const float64 &dx, const float64 &dy) const {
    GeoMatrix m (*this);
    m.TranslateInPlace (dx, dy);
    return m;
}

GeoMatrix &GeoMatrix::TranslateInPlace (const float64 &dx, const float64 &dy) {
    Matrix44 mtrans;
    mtrans.LoadTranslation (dx, dy, 0);
    mat = mtrans * mat;
    mtrans.LoadTranslation (-dx, -dy, 0);
    imat *= mtrans;
    return *this;
}

/**
   * Transforms on v2float64 types, by which we'll mostly represent UV & CxCy
 * points.
   */

v2float64 GeoMatrix::operator*(const v2float64 &v) const {
    return TransformV2 (v);
}

void GeoMatrix::TransformV2 (const float64 &xsrc,
                             const float64 &ysrc,
                             float64 &xdest,
                             float64 &ydest) const {
    Vect vv = Vect (xsrc, ysrc, 0);
    mat.TransformVectInPlace (vv);
    xdest = vv.x;
    ydest = vv.y;
}

v2float64 GeoMatrix::TransformV2 (const v2float64 &v) const {
    v2float64 oot = {v.x, v.y};
    TransformV2InPlace (oot);
    return oot;
}

v2float64 &GeoMatrix::TransformV2InPlace (v2float64 &v) const {
    Vect vv = Vect (v.x, v.y, 0.0);
    mat.TransformVectInPlace (vv);
    v.x = vv.x;
    v.y = vv.y;
    return v;
}

void GeoMatrix::TransformV2InPlace (float64 &vx, float64 &vy) const {
    v2float64 v = {vx, vy};
    TransformV2InPlace (v);
    vx = v.x;
    vy = v.y;
}

void GeoMatrix::SpewToStderr () const {
    fprintf (stderr, "mat -->                            imat -->\n");
    fprintf (stderr, "[  %5.2f,  %5.2f, %5.2f, %5.2f  ]  ", mat.m[0][0],
             mat.m[0][1], mat.m[0][2], mat.m[0][3]);
    fprintf (stderr, "[  %5.2f,  %5.2f, %5.2f, %5.2f  ]  \n", imat.m[0][0],
             imat.m[0][1], imat.m[0][2], imat.m[0][3]);
    fprintf (stderr, "[  %5.2f,  %5.2f, %5.2f, %5.2f  ]  ", mat.m[1][0],
             mat.m[1][1], mat.m[1][2], mat.m[1][3]);
    fprintf (stderr, "[  %5.2f,  %5.2f, %5.2f, %5.2f  ]  \n", imat.m[1][0],
             imat.m[1][1], imat.m[1][2], imat.m[1][3]);
    fprintf (stderr, "[  %5.2f,  %5.2f, %5.2f, %5.2f  ]  ", mat.m[2][0],
             mat.m[2][1], mat.m[2][2], mat.m[2][3]);
    fprintf (stderr, "[  %5.2f,  %5.2f, %5.2f, %5.2f  ]  \n", imat.m[2][0],
             imat.m[2][1], imat.m[2][2], imat.m[2][3]);
    fprintf (stderr, "[  %5.2f,  %5.2f, %5.2f, %5.2f  ]  ", mat.m[3][0],
             mat.m[3][1], mat.m[3][2], mat.m[3][3]);
    fprintf (stderr, "[  %5.2f,  %5.2f, %5.2f, %5.2f  ]  \n", imat.m[3][0],
             imat.m[3][1], imat.m[3][2], imat.m[3][3]);
}
