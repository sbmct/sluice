/* Copyright (C) 2011 Oblong Industries */

#ifndef OBLONG_SLUICE_NODE_H
#define OBLONG_SLUICE_NODE_H

#include <libLoam/c++/ObMap.h>
#include <libLoam/c++/ObTrove.h>
#include <libBasement/KneeObject.h>
#include <libPlasma/c++/Slaw.h>

#include <map>
#include <set>

#include <boost/tuple/tuple.hpp>

#include "geo_utils.h"
#include "Timeseries.h"

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::plasma;

/**
 * Represents an entity with a physical location
 *
 */

class Node : public KneeObject {
    PATELLA_SUBCLASS (Node, KneeObject);

protected:
    const Str ident;
    const Str kind;

    typedef TimeSeries<LatLon, float64> geoloc_history_t;
    typedef ObTrove<LatLon> path_t;
    typedef TimeSeries<path_t, float64> path_history_t;

    geoloc_history_t geoloc_history;
    path_history_t path_history;

    typedef std::map<Str, Str> obsmap_t;
    typedef TimeSeries<obsmap_t, float64> obshistory_t;

    obshistory_t obs_history;

    typedef std::vector<unt32> pigset_t;
    typedef TimeSeries<pigset_t, float64> pigmap_t;
    pigmap_t pigments;

public:
    virtual ~Node ();
    Node (const Str identity, const Str kind);

    const bool HasUpdateBetweenTimes (const float64 a, const float64 b) const;
    const bool HasNoLaterUpdates (const float64 t) const;

    const bool IntersectsRect (const GeoRect &g, const float64 t);

    const Str Ident () const { return ident; }
    const Str Kind () const { return kind; }

    virtual void FlushPigments ();
    virtual bool PigmentsAtTime (const float64 t,
                                 std::vector<unt32> &pigs) const;
    virtual void SetPigments (const std::vector<unt32> &pigs, const float64 t);

    /** Returns true if node was updated. */
    bool AddObservationsFromSlaw (const Slaw &attrs, const float64 timestamp);

    ObRetort
    AddObservation (const Str attr, const Str value, const float64 timestamp);
    ObRetort RemoveObservationAtTime (const Str attr, const float64 timestamp);

    const ObTrove<Str> ObservationKeys (const float64 t);
    /** For a given series, return the observation at or the first observation
     * previous to timestamp */
    ObRetort
    ObservationAtTime (const Str attr, const float64 timestamp, Str &value);

    const bool IsAvailableForTime (const float64 t);

    virtual bool HasPath (const float64 t) const;
    virtual void SetPath (const ObTrove<LatLon> &lls, const float64 t);
    // If the node has a path, populate it. and return true.
    // If it does not, return false;
    virtual bool GetPath (ObTrove<LatLon> &ll, const float64 t) const;

    virtual void SetGeoloc (const LatLon x, const float64 t);
    virtual const ObRetort
    ClosestPointTo (const LatLon here, LatLon &there, const float64 t);
    virtual ObRetort RepresentativePoint (LatLon &there, const float64 t);
    virtual const ObRetort Bounds (LatLon &bl, LatLon &tr, const float64 t);
    virtual const bool HasGeoloc (const float64 t);
    virtual const LatLon Geoloc (const float64 t);
    virtual void GetUpdateTimes (std::set<float64> &times) const;

    virtual Slaw ToSlaw (const float64 t);
#ifdef NOPE
    virtual ObRetort UpdateFromSlaw (const Slaw s, const float64 t);
#endif
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_NODE_H
