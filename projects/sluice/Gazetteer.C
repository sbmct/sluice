
/* (c)  Oblong Industries */

#include "Gazetteer.h"
#include "SluiceConsts.h"
#include "Hasselhoff.h"
#include "Fluoroscope.h"

using namespace oblong::sluice;

Gazetteer::Gazetteer () : KneeObject (), fluoroscope (NULL) {}

void Gazetteer::InitializePoolRegistration (Hasselhoff *hoff) {
    if (!hoff)
        return;
    hoff->PoolParticipate (SluiceConsts::EdgeInLogicalHoseName (), this);

    AppendMetabolizer (SluiceConsts::BookmarksAnnounceRequestDescrips (),
                       &Gazetteer::MetabolizeRequestAnnounceBookmarks,
                       "bookmarks");
    AppendMetabolizer (SluiceConsts::BookmarksNewRequestDescrips (),
                       &Gazetteer::MetabolizeRequestNewBookmark,
                       "new-bookmark");
    AppendMetabolizer (SluiceConsts::BookmarksDeleteRequestDescrips (),
                       &Gazetteer::MetabolizeRequestDeleteBookmark,
                       "delete-bookmark");
}

void Gazetteer::SetFluoroscope (Fluoroscope *f) { fluoroscope = f; }

void Gazetteer::SetPersistentStore (const Str &ps) { persistent_store = ps; }

void Gazetteer::LoadFromPersistentStore () {
    FromSlaw (Slaw::FromFile (persistent_store));
}

void Gazetteer::SaveToPersistentStore () {
    ToSlaw ().ToFile (persistent_store);
}

const static Slaw nam_s ("name");
const static Slaw uid_s ("uid");
const static Slaw sum_s ("summary");
const static Slaw dsc_s ("description");
const static Slaw lat_s ("lat");
const static Slaw lon_s ("lon");
const static Slaw lvl_s ("level");

void Gazetteer::FromSlaw (const Slaw &s) {
    for (int64 i = 0; i < s.Count (); ++i) {
        Slaw gaz_s = s.Nth (i);
        Str name;
        float64 lat = 0, lon = 0, level = 0;
        if (gaz_s.Find (nam_s).Into (name) && gaz_s.Find (lat_s).Into (lat) &&
            gaz_s.Find (lon_s).Into (lon) && gaz_s.Find (lvl_s).Into (level)) {
            Vect loc (lat, lon, level);
            gazettes.Append (new Gazette (name, loc));
        }
    }
}

Slaw Gazetteer::ToSlaw () {
    Slaw s = Slaw::List ();
    ObCrawl<Gazette *> cr = gazettes.Crawl ();
    while (!cr.isempty ()) {
        Gazette *g = cr.popfore ();
        s = s.ListAppend (Slaw::Map (nam_s, g->name, lat_s, g->loc.X (), lon_s,
                                     g->loc.Y (), lvl_s, g->loc.Z ()));
    }
    return s;
}

ObRetort Gazetteer::AnnounceBookmarks () {
    Protein prot (SluiceConsts::BookmarksAnnounceResponseDescrips (),
                  ToSlaw ());
    Deposit (prot);
    return OB_OK;
}

void Gazetteer::Deposit (const Protein &prot) {
    Hose *h = Pool::Participate (SluiceConsts::EdgeOutLogicalHoseName ());
    if (NULL != h) {
        h->Deposit (prot);
        h->Delete ();
    } else
        OB_LOG_DEBUG ("Could not participate in %s",
                      SluiceConsts::EdgeOutLogicalHoseName ().utf8 ());
}

bool Gazetteer::BookmarkExists (const Str &name) {
    ObCrawl<Gazette *> cr = gazettes.Crawl ();
    while (!cr.isempty ()) {
        Gazette *g = cr.popfore ();
        if (g->name == name)
            return true;
    }
    return false;
}

ObRetort Gazetteer::AddBookmark (const Str &name) {
    LatLon ll_center;
    float64 umin, umax, vmin, vmax, zoom = 0;

    if (fluoroscope) { // Calculate lat, lon, zoom by reversing the calculations
                       // in ZoomTo
        ll_center = fluoroscope->GQuad ()->UVToLatLon (GeoQuad::V::Center,
                                                       GeoQuad::H::Center);
        fluoroscope->GQuad ()->CartoToUV (-0.5, -0.5, umin, vmin);
        fluoroscope->GQuad ()->CartoToUV (0.5, 0.5, umax, vmax);
        zoom = (vmax - vmin);
    }

    return AddBookmark (name, ll_center.lat, ll_center.lon, zoom);
}

ObRetort Gazetteer::AddBookmark (const Str &name,
                                 float64 lat,
                                 float64 lon,
                                 float64 level) {
    if (name.IsEmpty ())
        return OB_INVALID_ARGUMENT;
    if (BookmarkExists (name))
        return OB_ALREADY_PRESENT;

    gazettes.Append (new Gazette (name, Vect (lat, lon, level)));
    SaveToPersistentStore ();
    return OB_OK;
}

ObRetort Gazetteer::DeleteBookmark (const Str &name) {
    ObCrawl<Gazette *> cr = gazettes.Crawl ();
    while (!cr.isempty ()) {
        Gazette *g = cr.popfore ();
        if (g->name == name) {
            gazettes.Remove (g);
            SaveToPersistentStore ();
            return OB_OK;
        }
    }
    return OB_NOTHING_TO_DO;
}

ObRetort Gazetteer::MetabolizeRequestAnnounceBookmarks (const Protein &prt,
                                                        Atmosphere *atmo) {
    return AnnounceBookmarks ();
}

ObRetort Gazetteer::MetabolizeRequestNewBookmark (const Protein &prt,
                                                  Atmosphere *atmo) {
    Str name;
    Str uid;
    ObRetort ret;
    if (prt.Ingests ().Find (nam_s).Into (name) &&
        prt.Ingests ().Find (uid_s).Into (uid)) {
        float64 lat, lon, level;
        if (prt.Ingests ().Find (lat_s).Into (lat) &&
            prt.Ingests ().Find (lon_s).Into (lon) &&
            prt.Ingests ().Find (lvl_s).Into (level)) {
            ret = AddBookmark (name, lat, lon, level);
        } else {
            ret = AddBookmark (name);
        }
    } else {
        ret = OB_INVALID_ARGUMENT;
    }

    if (ret == OB_ALREADY_PRESENT) {
        Protein prot (SluiceConsts::BookmarksNewResponseDescrips (),
                      Slaw::Map (nam_s, name, uid_s, uid, sum_s,
                                 "could not create new bookmark", dsc_s,
                                 "bookmark name " + name + " already exists"));
        Deposit (prot);
    } else if (ret == OB_INVALID_ARGUMENT) {
        Protein prot (SluiceConsts::BookmarksNewResponseDescrips (),
                      Slaw::Map (nam_s, name, uid_s, uid, sum_s,
                                 "could not create new bookmark", dsc_s,
                                 "invalid ingests"));
        Deposit (prot);
    } else if (ret == OB_OK) {
        AnnounceBookmarks ();
    }

    return ret;
}

ObRetort Gazetteer::MetabolizeRequestDeleteBookmark (const Protein &prt,
                                                     Atmosphere *atmo) {
    Str name;
    ObRetort ret;
    if (prt.Ingests ().Find (nam_s).Into (name)) {
        ret = DeleteBookmark (name);
        if (ret == OB_OK) {
            AnnounceBookmarks ();
        }
    }
    return ret;
}
