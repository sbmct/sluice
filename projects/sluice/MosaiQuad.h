
/* (c)  Oblong Industries */

#ifndef MOSAIQ_CLAW
#define MOSAIQ_CLAW

#include <libBasement/ob-logging-macros.h>
#include <libBasement/AsympFloat.h>
#include <libBasement/AsympVect.h>
#include <libBasement/QuadraticVect.h>
#include <libBasement/LinearColor.h>
#include <libBasement/AsympColor.h>
#include <libBasement/LogLogger.h>
#include <libBasement/SineFloat.h>
#include <libBasement/SineColor.h>
#include <libBasement/SineVect.h>
#include <libNoodoo/TexQuad.h>
#include <libPlasma/c++/Plasma.h>

#include <libBasement/LinearFloat.h>
#include <libMedia/JpegImageClot.h>
#include <libMedia/PngImageClot.h>
#include <libNoodoo/GLTex.h>

#include <list>
#include <map>
#include <set>

#include <boost/tuple/tuple.hpp>

#include "TileCache.h"
#include "TileID.h"

namespace oblong {
namespace sluice {

using namespace oblong::noodoo;
using namespace oblong::media;

class MosaiQuad : public TexQuad {
    PATELLA_SUBCLASS (MosaiQuad, TexQuad);

private:
    struct RenderTile {
        TileID tid;
        TexGL *tex;
        ObColor color;
        float64 alpha_start;
        v2float64 texcoors[4];
        Vect verts[4];
    };

    std::vector<RenderTile> tiles_to_render;
    int tile_render_index;

    enum Zooming { NOT_ZOOMING = 0, ZOOMING_IN = 1, ZOOMING_OUT = 2 };

    struct DrawPod {
        DrawPod ()
            : coverx (0), covery (0), morex0 (0), z (0), phasex (0.0),
              phasey (0.0), cursizeW (0.0), cursizeH (0.0), crossfading (false),
              best_bigtile_z (0), tx0 (0), ty0 (0), levelTilesI (0),
              best_bigtile_tgl (NULL), zooming (NOT_ZOOMING), ready (false) {}
        int coverx, covery, morex0, z;
        double phasex, phasey;
        float cursizeW, cursizeH;
        ObColor color;
        bool crossfading;
        int best_bigtile_z;
        int tx0, ty0, levelTilesI;
        TexGL *best_bigtile_tgl;
        Zooming zooming;

        bool ready;
    };

    DrawPod current_pod;

    Str remoteTileReqPool;
    Str remoteTileRespPool;

    Hose *remoteReq;
    Hose *remoteResp2;

    int tile_size, factor;
    double resolution;
    double t;
    int maxtiles;
    double cacheLayers;
    bool xwrap;
    TileCache *cache;
    Str mapID;
    double roi_x, roi_y;

    int hack_cur_layer;
    bool show_boxen;

    std::map<const TileID, double> tileFreshened;
    ObRef<TexGL *> unitile;
    Str unitileMap;
    bool use_alpha;

    std::map<TileID, float64> tile_opacity;
    float64 alpha_per_second;
    void ResetOpacityConditionally (const TileID);
    void SetTileOpacity (const TileID, const float64);
    const float64 TileOpacity (const TileID);
    const bool IsOpaque (const TileID &) const;

    void SetOpacityForLevelToDefault (int level); // yes
    void SetOpacityForLevelToMaximum (int level); // yes
    void SetOpacityForLevel (int level, const float64 new_opacity);

    static Str MakeRandomPoolStr ();

    void RecalculateMaxTiles ();

    void ProcessOneTile (Protein p);

    void DrawRelativeRect (const TileID &tid,
                           float x0,
                           float y0,
                           float x1,
                           float y1,
                           ObColor color,
                           TexGL *tgl,
                           float lef,
                           float top,
                           float rig,
                           float bot,
                           const float64 opacity_start);

    void DrawAbsoluteRect (const TileID &tid,
                           float tx0,
                           float ty0,
                           float tx1,
                           float ty1,
                           float x0,
                           float y0,
                           float x1,
                           float y1,
                           ObColor color,
                           TexGL *tgl,
                           float cursizeW,
                           float cursizeH,
                           const float64 opacity_start);

    TileID FindFirstBigtile (const int tx0,
                             const int ty0,
                             const int z,
                             const int coverx,
                             const int covery,
                             const int morex0,
                             const int levelTilesI);

    void SetupDrawPod ();
    void DrawInner (int x, int y);
    void DrawPass (const int x,
                   const int y,
                   const int z,
                   const int tx,
                   const int ty,
                   const bool drawn_more_than_once,
                   bool &useBG,
                   bool &use1x1,
                   bool &useLower,
                   bool &useHigher);

    bool HasHigherRes (const TileID &t) const;
    bool HasLowerRes (const TileID &t) const;

    void DrawInnerUnitile (const int x,
                           const int y,
                           const int z,
                           const int tx,
                           const int ty,
                           bool &useBG);
    void DrawUseLower (const int x,
                       const int y,
                       const int z,
                       const int tx,
                       const int ty,
                       const int totalSubTileAttempts,
                       bool &useLower,
                       bool &useBG);
    void DrawUseHigher (const int x,
                        const int y,
                        const int z,
                        const int tx,
                        const int ty,
                        bool &useHigher,
                        bool &useBG);
    void DrawHighres (const int x,
                      const int y,
                      const TileID &kfull,
                      const bool drawn_more_than_once,
                      bool &use1x1);

    ObRetort TileArrived (const TileID &);
    ObRetort TileExpired (const TileID &);

    SOFT_MACHINERY (LinearFloat, Scaledenom);
    SOFT_MACHINERY (LinearFloat, CenterX);
    SOFT_MACHINERY (LinearFloat, CenterY);
    SOFT_MACHINERY (AsympFloat, Squetch);

    FLAG_MACHINERY (DrawBoxes);
    FLAG_MACHINERY (TilesUpdated);

public:
    // parts you should care about
    MosaiQuad (int tileSize,
               int factor,
               bool x_wrap,
               Str remoteTileReqPool_,
               Str remoteTileRespPool_,
               double startingX,
               double startingY,
               double startingScale,
               Str startingMapID,
               double cachelayers_ = 3.0);

    virtual void Shutdown ();

    void SetCacheLayers (double cachelayers_);
    // more resolution means more work (and tinier tiles)! default resolution is
    // 1.0
    void SetResolution (double newResolution);
    double Resolution () { return resolution; }
    // Squetch of > 1.0 means wider tiles, aspect of < 1.0 means tall, skinny
    // tiles, default 1.0
    void SetSquetch (double newAspect) { SquetchSoft ()->Set (newAspect); }
    void SetSquetchHard (double newAspect) {
        SquetchSoft ()->SetHard (newAspect);
    }
    // double Squetch () { return Squetchsoft (); }

    // 0,0 is bottom-left, 1,1 is top-right
    void SetROICenter (double x, double y) {
        if (x < 0.0)
            x = 0.0;
        if (x > 1.0)
            x = 1.0;
        roi_x = x;
        if (y < 0.0)
            y = 0.0;
        if (y > 1.0)
            y = 1.0;
        roi_y = 1.0 - y;
    }

    void SetMap (Str newMapID);
    // SetScale: 1.0 is level 0 (one tile), 2.0 is level 1 (four tiles), and so
    // on
    void SetScale (double newScale);
    void SetScaleHard (double newScale);
    // MoveTo: x,y both in range 0.0 to 1.0, with positive y going up, not down
    void MoveTo (double x, double y);
    void MoveToHard (double x, double y);

    void SetShowDebugBoxen (bool showBoxen);

    // implementation details
    virtual void DrawSelf (VisiFeld *v, Atmosphere *a);
    virtual ObRetort Inhale (Atmosphere *atm);

    bool OneWorkCycle ();

    const float64 AlphaPerSecond () const { return alpha_per_second; }
    void SetAlphaPerSecond (const float64 x) { alpha_per_second = x; }

    static const float64 REQUEST_TIMEOUT;
};
}
}
#endif // MOSAIQ_CLAW
