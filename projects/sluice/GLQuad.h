
// (c) Conduce Inc.

#pragma once

#include "RenderingInstructions.h"
#include "Shader.h"
#include "GeoQuad.h"
#include "Subscription.h"
#include "Inculcator.h"
#include "SlawTraits.h"
#include "Hasselhoff.h"

#include "TopoPigment.h"

#include <libLoam/c++/FatherTime.h>
#include <libNoodoo/GLTex.h>

namespace oblong {
namespace sluice {

using conduce::sluice::PigmentBuffer;
using conduce::sluice::PigmentOffsets;
using conduce::sluice::BufferDescription;

enum class PigmentType { Point, Path };

class GLQuad;

namespace topo {

struct Pigment {
    Pigment () = default;
    ~Pigment ();

    GLQuad *parent{nullptr};
    Str name, texture_path;
    PigmentType type;
    ShaderPrograms programs;
    float64 min_zoom;
    ObRef<SoftColor *> color;
    ObRef<SoftFloat *> size, ribbon_texture_per_mm{nullptr},
        ribbon_offset_speed{nullptr};

    ObColor getColor () const {
        if (~color) {
            return color->Val ();
        } else {
            return SoftColor::UnspecifiedValue ();
        }
    }

    float64 getSize () const {
        if (~size) {
            return size->Val ();
        } else {
            return SoftFloat::UnspecifiedValue ();
        }
    }

    float64 getRibbonTexturePerMM () const {
        if (~ribbon_texture_per_mm) {
            return ribbon_texture_per_mm->Val ();
        } else {
            return SoftFloat::UnspecifiedValue ();
        }
    }

    float64 getRibbonOffsetSpeed () const {
        if (~ribbon_offset_speed) {
            return ribbon_offset_speed->Val ();
        } else {
            return SoftFloat::UnspecifiedValue ();
        }
    }

    // And then the more transient things
    // Where do we start each portion of the array?
    // how many indices are we going to render?
    size_t count{0};

    RenderingInstructions rendering;
    ObWeakRef<GLQuad *> tq;
    ObRef<GLTex *> texture{nullptr};
    ObRef<Shader *> shady{nullptr};
    bool should_draw{true};

    const float64 textureWidth () const {
        if (~texture) {
            return texture->Width ();
        } else {
            return 1.0;
        }
    }

    void setParent (GLQuad *);

    // given the global offsets, set start points for our different buffers
    void setOffsets (const PigmentOffsets &);

    // Let's minimize texture switching
    bool operator<(const Pigment &other) const {
        return std::tie (texture_path, programs) <
               std::tie (other.texture_path, other.programs);
    }
};

struct PigMatch {
    PigMatch () = default;
    PigMatch (const size_t p, const size_t n) : pig_index{p}, node_index{n} {}
    size_t pig_index, node_index;
    bool operator<(const PigMatch &other) const {
        return std::tie (pig_index, node_index) <
               std::tie (other.pig_index, other.node_index);
    }
};

struct RenderState {
    Shader *shader{nullptr};
    GLTex *texture{nullptr};
};
}

class GLQuad : public GeoQuad {
    PATELLA_SUBCLASS (GLQuad, GeoQuad);

protected:
    GLuint vbo_id{0};
    ShaderPrograms default_path, default_point;
    BufferDescription raw_buffer;
    ShaderStash shaders;
    float64 node_size, current_zoom;
    // Really, Oblong? You had to make FatherTime::CurTime()
    // non-const??
    mutable FatherTime internal_timer;
    size_t num_pigments_rendered;

    bool RefreshVBO ();
    void SwapShaderIfNecessary (topo::RenderState &, Shader *) const;
    void SwapTextureIfNecessary (topo::RenderState &, GLTex *) const;
    void RenderPigment (topo::Pigment &, topo::RenderState &) const;

    // void depositRenderingInfo ();

    SOFT_MACHINERY (SoftFloat, RibbonLengthScale);
    SOFT_MACHINERY (SoftFloat, RibbonOffsetSpeed);
    FLAG_MACHINERY (VBOIsDirty);
    FLAG_MACHINERY (RenderRefreshed);

    SINGLE_ARG_HOOK_MACHINERY (VBOInfoUpdated, Slaw);

public:
    GLQuad () = default;
    virtual ~GLQuad ();

    void SetNodeSize (float64 x) { node_size = x; }
    float64 ZoomLevel () { return current_zoom; }
    virtual void SetCurrentZoom (float64 x) = 0;

    void PopulateDefaultShadersFromSlaw (Slaw);
    bool SpewOpenGLErrors ();

    void Clear ();

    size_t numPigmentsRendered () const { return num_pigments_rendered; }
};
}
}

DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::topo::Pigment);
CONDUCE_DECLARE_VECTOR_TRAITS (oblong::sluice::topo::Pigment);
CONDUCE_DECLARE_VECTOR_TRAITS (oblong::sluice::RenderingInstructions::Attr);
