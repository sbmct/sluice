
#pragma once

#include <libLoam/c++/ObRef.h>
#include <libBasement/KneeObject.h>

#include <set>
#include <list>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/unordered_map.hpp>

#include "RTree.h"
#include "Node.h"
#include "profile.h"

namespace oblong {
namespace sluice {

using oblong::loam::ObRef;
using oblong::basement::KneeObject;

class GeoSubset;

// Should be std::tuple, but we don't have c++0x turned on
class GeoIndex : public KneeObject {
    PATELLA_SUBCLASS (GeoIndex, KneeObject);

protected:
    struct bounds_t {
        float64 min_bounds[2];
        float64 max_bounds[2];
        size_t id;
        Node *n;
        float64 t;
    };

    typedef ObWeakRef<Node *> NodeWRef;

    typedef boost::unordered_map<Node *, NodeWRef> node_lifemap_t;
    typedef boost::unordered_map<Node *, std::list<size_t>> node_entries_t;
    typedef std::map<float64, bool, std::greater<float64>> validmap_t;
    typedef boost::unordered_map<Node *, validmap_t> contained_t;
    typedef boost::unordered_map<size_t, bounds_t> entries_t;
    typedef std::multimap<float64, size_t> timeline_t;

    size_t counter;             // How many nodes have we looked at, total?
    entries_t entries;          // Our active set of entries, by numeric id
    node_entries_t entries_rev; // Entry id's by node
    contained_t contained;      // times when each node is valid
    node_lifemap_t
        alive; // node -> node weakref, to see if the node is still alive
    RTree<size_t, float64, 2> rtree;
    timeline_t timeline;
    timeline_t::iterator timeline_start;

    std::vector<Node *> to_expunge;
    void QueueExpunge (Node *);
    void ClearExpungable ();

    const bool
    Alive (Node *n); // <- Note: may call Expunge (Does this make sense?)
    bool Bounds (Node *n,
                 const float64 t,
                 float64 min_bounds[2],
                 float64 max_bounds[2]);

    void ExpireEntry (const size_t entry);

public:
    GeoIndex (const GeoRect &bounds);

    virtual void Delete ();
    void Add (Node *n, const float64 t);
    void Remove (Node *n, const float64 t);
    const int64 Count ();
    void Find (const float64 t, const GeoRect &search, std::set<Node *> &out);
    void Find (const float64 t, const GeoRect &search, GeoSubset &subset);
    void Expunge (Node *n);

    bool NodeIsValidAt (const size_t e, const float64 t);
    Node *NodeForEntry (const size_t e);

    const bool Contains (Node *n, const float64 t);

    void Clear ();

    Node *First ();

    void ExpireNodes (const float64 expireTime);
};
}
}
