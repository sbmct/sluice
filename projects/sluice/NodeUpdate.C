
#include "NodeUpdate.h"
#include "Node.h"
#include "NodeStore.h"

using namespace oblong::sluice;

NodeUpdate::NodeUpdate (Slaw s, NodeStore *store)
    : good (false), has_geoloc (false), has_path (false) {
    if (!(s.MapFind ("id").Into (ident) && s.MapFind ("kind").Into (kind)))
        return;

    Str now_str;
    float64 now = NodeStore::NowGMT ();
    if (s.MapFind ("timestamp").Into (now_str)) {
        if (!store->ParseDate (now_str, now).IsSplend ())
            return;
    } else if ((!s.MapFind ("timestamp").IsNull ()) &&
               (!s.MapFind ("timestamp").Into (now)))
        return;

    timestamp = now;

    Vect loc;
    if (s.MapFind ("loc").Into (loc)) {
        geoloc = LatLon (loc.x, loc.y);
        has_geoloc = true;
    } else {
        Slaw p;
        if (s.MapFind ("path").Into (p) && !p.IsNull ()) {
            has_path = true;
            for (int64 i = 0; i < p.Count (); ++i) {
                if (p.Nth (i).Into (loc))
                    path.Append (LatLon (loc.x, loc.y));
            }
        }
    }
    good = true;
}

const bool NodeUpdate::Matches (Node *n) const {
    return ident == n->Ident () && kind == n->Kind ();
}

void NodeUpdate::Apply (Node *n) const {
    if (has_geoloc)
        n->SetGeoloc (geoloc, timestamp);
    else if (has_path)
        n->SetPath (path, timestamp);
}
