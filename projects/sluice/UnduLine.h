
/* (c)  oblong industries */

#ifndef UNDU_LINE_FROM_YOUR_FAVORITE_YEATS
#define UNDU_LINE_FROM_YOUR_FAVORITE_YEATS

#include <libNoodoo/VertexForm.h>

#include <libBasement/SoftVect.h>
#include <libBasement/SineVect.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::loam;

class UnduLine : public VertexForm {
    PATELLA_SUBCLASS (UnduLine, VertexForm);

public:
    ObRef<SoftVect *> p0, p3;
    ObRef<SineVect *> p1, p2;
    ObRef<SoftVect *> plane;
    float64 p1_dist_frac, p2_dist_frac;
    float64 p1_amp, p2_amp;

public:
    UnduLine ();
    virtual ~UnduLine ();

    void SetNumVertices (int32 nv);

    const Vect &P0 () const { return (~p0)->Val (); }
    const Vect &P0GoalVal () const { return (~p0)->GoalVal (); }
    SoftVect *P0Soft () { return ~p0; }
    void SetP0 (const Vect &p) { (~p0)->Set (p); }
    void SetP0Hard (const Vect &p) { (~p0)->SetHard (p); }
    void InstallP0 (SoftVect *sv) {
        if (sv) {
            p0 = sv;
        }
    }

    const Vect &P3 () const { return (~p3)->Val (); }
    const Vect &P3GoalVal () const { return (~p3)->GoalVal (); }
    SoftVect *P3Soft () { return ~p3; }
    void SetP3 (const Vect &p) { (~p3)->Set (p); }
    void SetP3Hard (const Vect &p) { (~p3)->SetHard (p); }
    void InstallP3 (SoftVect *sv) {
        if (sv) {
            p3 = sv;
        }
    }

    const Vect &Plane () const { return (~plane)->Val (); }
    const Vect &PlaneGoalVal () const { return (~plane)->GoalVal (); }
    void SetPlane (const Vect &p) { (~plane)->Set (p); }
    void SetPlaneHard (const Vect &p) { (~plane)->SetHard (p); }
    void InstallPlane (SoftVect *pl) {
        if (pl) {
            plane = pl;
        }
    }

    SineVect *P1Soft () { return ~p1; }
    SineVect *P2Soft () { return ~p2; }

    float64 P1DistFrac () const { return p1_dist_frac; }
    void SetP1DistFrac (float64 df) { p1_dist_frac = df; }

    float64 P2DistFrac () const { return p2_dist_frac; }
    void SetP2DistFrac (float64 df) { p2_dist_frac = df; }

    float64 P1Amp () const { return p1_amp; }
    void SetP1Amp (float64 a) { p1_amp = a; }

    float64 P2Amp () const { return p2_amp; }
    void SetP2Amp (float64 a) { p2_amp = a; }

    float64 P1Freq () const { return (~p1)->FrequencyVal (); }
    void SetP1Freq (float64 f) { (~p1)->SetFrequency (f); }
    SoftFloat *P1FreqSoft () { return (~p1)->FrequencySoft (); }
    void InstallP1Freq (SoftFloat *sf) { (~p1)->InstallFrequency (sf); }

    float64 P2Freq () const { return (~p2)->FrequencyVal (); }
    void SetP2Freq (float64 f) { (~p2)->SetFrequency (f); }
    SoftFloat *P2FreqSoft () { return (~p2)->FrequencySoft (); }
    void InstallP2Freq (SoftFloat *sf) { (~p2)->InstallFrequency (sf); }

    virtual ObRetort Inhale (Atmosphere *atm);
};
}
} // besotted blow-out wake for namespaces sluice and oblong

#endif
