
/* Copyright (C) 2011 Oblong Industries */

#ifndef OBLONG_SLUICE_JAVASCRIPT_H
#define OBLONG_SLUICE_JAVASCRIPT_H

#include <libBasement/KneeObject.h>

#include <v8.h>
#include "Subscription.h"

namespace oblong {
namespace sluice {

class Node;

using namespace oblong::basement;

/**
 * Execute some javascript
 *
 */
class Javascript {
protected:
    v8::Isolate *isolate;
    v8::HandleScope master_scope;
    v8::Handle<v8::ObjectTemplate> internal_global, external_global;
    v8::Handle<v8::ObjectTemplate> node_tmpl, subnode_tmpl;
    v8::Handle<v8::Context> internal_store_context;
    v8::Handle<v8::Context> external_store_context;

    template <typename T>
    ObRetort RunScript (const Str &javascript,
                        Node *node,
                        const float64 sluice_time,
                        T &output);

    static Javascript *instance;

public:
    virtual ~Javascript ();
    Javascript ();
    Javascript (Javascript &&) = default;
    Javascript &operator=(Javascript &&) = default;
    Javascript (const Javascript &) = delete;
    Javascript &operator=(const Javascript &) = delete;

    ObRetort RunScriptWithNodeAndTime (const Str &javascript,
                                       Node *node,
                                       const float64 sluice_time,
                                       Str &result);

    ObRetort RunScriptWithNodeAndTime (const Str &javascript,
                                       Node *node,
                                       const float64 sluice_time,
                                       float64 &result);

    ObRetort RunScriptWithNodeAndTime (const Str &javascript,
                                       Node *node,
                                       const float64 sluice_time,
                                       bool &result);

    ObRetort RunScriptWithNode (const Str &javascript,
                                const SubscriptionNode &node,
                                bool &result);

    static Javascript &Instance ();
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_JAVASCRIPT_H
