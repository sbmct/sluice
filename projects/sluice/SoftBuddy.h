
#pragma once

#include <libBasement/SoftFloat.h>
#include <libBasement/SoftColor.h>
#include <libPlasma/c++/Slaw.h>

namespace oblong {
namespace sluice {

using oblong::basement::SoftFloat;
using oblong::basement::SoftColor;
using oblong::plasma::Slaw;

/**
 * Handy methods to make softs from slawx.
 */
struct SoftBuddy {
    /**
     * Create a SoftFloat from slaw.
     *
     * Possible formats include:
     * - A regular soft float:
     * \code
     * - type: "SoftFloat"
     *   value: 1.0 !f64
     * \endcode
     * - A Sine Float
     * \code
     * - type: "SineFloat"
     *   center: 1.0 !f64
     *   amplitude: 1.0 !f64
     *   frequency: 1.0 !f64
     *   phase: 1.0 !f64
     * \endcode
     *
     * If none of these formats are matched, default is returned
     */
    static SoftFloat *SoftFloatFromSlaw (Slaw sf,
                                         SoftFloat *default_result = NULL);

    static Slaw SoftFloatToSlaw (SoftFloat *serialize_me);

    /**
     * Create a SoftColor from a slaw.
     *
     * Possible formats include:
     * - A regular soft color
     * \code
     * - type: "SoftColor"
     *   value: {1.0, 1.0, 1.0, 1.0} !ObColor
     * \endcode
     * - A Sine Color
     * \code
     * - type: "SineColor"
     *   center: {1.0, 1.0, 1.0, 1.0} !ObColor
     *   amplitude: {1.0, 1.0, 1.0, 1.0} !ObColor
     *   frequency: 1.0 !f64
     *   phase: 1.0 !f64
     * \endcode
     *
     * If none of these formats are matched, SoftColor(ObColor(1.0, 1.0)) is
     *returned
     */
    static SoftColor *SoftColorFromSlaw (Slaw colors,
                                         SoftColor *default_soft = NULL);

    static Slaw SoftColorToSlaw (SoftColor *serialize_me);
};
}
}
