
// (c) Conduce Inc.

#pragma once
#include "Hasselhoff.h"
#include "Fluoroscope.h"
#include "Subscription.h"

#include <vector>

namespace oblong {
namespace sluice {

class TopoScope : public Fluoroscope {
    PATELLA_SUBCLASS (TopoScope, Fluoroscope);

private:
    ObRef<NodeStore *> parent;
    ObRef<Hasselhoff *> hoff;
    Subscription subscription;
    float64 heartbeat_frequency{5.0};
    oblong::loam::FatherTime heartbeat_timer;

    std::vector<SubscriptionNode> store;
    std::set<oblong::loam::Str> enabledKinds;
    std::set<oblong::loam::Str> subscribedKinds;

    // Used to recognize the order in which subscription nodes are submitted.
    size_t node_sequence{0};

    void AddRawNode (const SubscriptionNode &);
    void RemoveRawNode (const Str &);
    ObRetort CartoCoordsChange (GeoQuad *gq);

    void UpdateSubscription ();
    void UnregisterSubscription ();
    void SendHeartbeat ();
    void UpdateEnabledKinds (const Slaw config);
    void UpdateSubscribedKinds ();
    void ResetSubscribedKinds ();

    FLAG_MACHINERY (SubscriptionUpdateRequired);
    FLAG_MACHINERY (StoreIsSorted);
    FLAG_MACHINERY (StoreIsCompact);
    FLAG_MACHINERY (QuadIsDirty);

public:
    TopoScope ();
    virtual ~TopoScope ();

    ObRetort MetabolizeSubscriptionData (const Protein &, Atmosphere *);
    ObRetort MetabolizeSubscriptionDelta (const Protein &, Atmosphere *);

    // KneeObject
    virtual ObRetort Inhale (Atmosphere *) override;

    virtual std::vector<Str> WhitelistedAttributes () const {
        return std::vector<Str>{"opacity", "enabled-kinds"};
    }

    ObRetort TimeUpdated (NodeStore *);

    ObRetort vboInfoUpdated (Slaw);

    size_t storeSize () const { return store.size (); }

    // Fluoroscope
    virtual void InitializePoolRegistration (Hasselhoff *hoff) override;
    virtual void SetNodeStore (NodeStore *ns) override;
    virtual ObRetort SetConfigurationSlaw (const Slaw config) override;
    virtual void Rebase () override;
};
}
}
