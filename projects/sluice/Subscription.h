/* (c) Conduce Inc. */

#ifndef SUBSCRIPTION_H__
#define SUBSCRIPTION_H__

#include <libLoam/c++/Str.h>
#include <libPlasma/c++/Slaw.h>
#include "SlawTraits.h"
#include "geo_utils.h"
#include <vector>
#include <unordered_map>

using ConduceVectType = oblong::loam::Vect;
using ConduceString = oblong::loam::Str;
using StrPair = std::pair<ConduceString, ConduceString>;

namespace oblong {
namespace sluice {

struct SubscriptionCache {
    PLASMA_MAP_ATTR (bounds) GeoRect bounds{};
    bool has_bounds{false};
    PLASMA_MAP_ATTR (modified_time) float64 modified_time{0.0};
    PLASMA_MAP_ATTR (satart_time) float64 start_time{0.0};
    PLASMA_MAP_ATTR (end_time) float64 end_time{0.0};
};

struct BinningRules {
    PLASMA_MAP_ATTR (should_bin) bool should_bin{false};
    PLASMA_MAP_ATTR (granularity) float64 granularity{-1.0};
};

struct Subscription {
    PLASMA_MAP_ATTR (ident) oblong::plasma::Slaw ident;
    PLASMA_MAP_ATTR (bounds) GeoRect bounds;
    PLASMA_MAP_ATTR (bounds) GeoRect visible_bounds;
    PLASMA_MAP_ATTR (current_time) float64 current_time{0.0};
    PLASMA_MAP_ATTR (time_rate) float64 time_rate{0.0};
    PLASMA_MAP_ATTR (kinds) std::vector<oblong::loam::Str> kinds;
    PLASMA_MAP_ATTR (attrs) std::vector<oblong::loam::Str> attributes;
    PLASMA_MAP_ATTR (binning) BinningRules binning;
    PLASMA_MAP_ATTR (max) unsigned int max;
    PLASMA_MAP_ATTR (skip_if_too_many_results) bool skip_if_too_many_results{
        false};
    PLASMA_MAP_ATTR_OPTIONAL (cache) std::vector<SubscriptionCache> cache;
};

struct SubscriptionNode {
    PLASMA_MAP_ATTR (id) Str ident;
    PLASMA_MAP_ATTR (kind) Str kind;
    PLASMA_MAP_ATTR_OPTIONAL (attrs) std::vector<StrPair> attrs;
    PLASMA_MAP_ATTR_OPTIONAL (path) std::vector<ConduceVectType> path;
    PLASMA_MAP_ATTR (timestamp) float64 timestamp;
    PLASMA_MAP_ATTR (endtime) float64 endtime;

    bool observation (const ConduceString &, ConduceString &) const;
    bool hasLocation () const { return 0 < path.size (); }
    ConduceVectType representativePoint () const;

    // An internal variable for TopoScope
    size_t storage_sequence;
};

struct SubscriptionData {
    PLASMA_MAP_ATTR (current_time) float64 current_time;
    PLASMA_MAP_ATTR (modified_time) float64 modified_time;
    PLASMA_MAP_ATTR_OPTIONAL (add) bslaw add_;
    PLASMA_MAP_ATTR_OPTIONAL (remove) bslaw remove_;

    using NodeSlawx = conduce::plasma::SlawContainer<SubscriptionNode>;
    NodeSlawx add () const { return NodeSlawx (add_); }

    using StrSlawx = conduce::plasma::SlawContainer<Str>;
    StrSlawx remove () const { return StrSlawx (remove_); }

    int64 addCount () const { return conduce::plasma::SafeListCount (add_); }
    int64 removeCount () const {
        return conduce::plasma::SafeListCount (remove_);
    }
};
}
}

// Everything below this point *really should* be machine generated.
CONDUCE_DECLARE_PAIR_TRAITS (oblong::loam::Str, oblong::loam::Str);
CONDUCE_DECLARE_VECTOR_TRAITS (StrPair);

CONDUCE_DECLARE_VECTOR_TRAITS (oblong::loam::Str);
CONDUCE_DECLARE_MAP_TRAITS (oblong::loam::Str, oblong::loam::Str);
CONDUCE_DECLARE_VECTOR_TRAITS (oblong::sluice::SubscriptionCache);
CONDUCE_DECLARE_VECTOR_TRAITS (oblong::sluice::SubscriptionNode);
CONDUCE_DECLARE_VECTOR_TRAITS (oblong::loam::Vect);

DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::BinningRules);
DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::SubscriptionCache);
DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::Subscription);
DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::SubscriptionNode);
DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::SubscriptionData);

#endif //! SUBSCRIPTION_H__
