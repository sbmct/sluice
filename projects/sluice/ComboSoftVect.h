
/* (c)  oblong industries */

#ifndef COMBO_SOFT_VECT_COMES_WITH_FRIES
#define COMBO_SOFT_VECT_COMES_WITH_FRIES

#include <libBasement/SoftVect.h>
#include <libBasement/SoftFloat.h>

#include <libLoam/c++/ObMap.h>
#include <libLoam/c++/ObTrove.h>

/**
  ComboSoftVect:
      This soft vect combines multiple softvects, optionally scaled
      by softfloats, to create the ultimate value.  This class has a
      trove of vect/float combinations (at least one combination is
      required) and a trove of operators. Thus,
      the val of the combovect is determined as follows:

        val = combos[0] . Val () * combos [0] . Key ()
        for (all combos starting at 1)
          val =
            val [+-*x](operator [i-1]) combos[i] . Val() * combos[i] . Key()

      You can add as many combos as you would like and the softFloat is
      optional.

      A common usage may be that the location of one locusthing depends on
      the location and size of another, thus being as follows:
          val = other->LocSoft() + other->WidthSoft() * other->OverSoft()
*/

namespace oblong {
namespace basement {
using namespace oblong::plasma;
using namespace oblong::loam;

class ComboSoftVect : public SoftVect {
    PATELLA_SUBCLASS (ComboSoftVect, SoftVect);

public:
    struct Operate {
        enum Thisaways { ADD = 0, SUBTRACT = 1, SCALE = 2, CROSS = 3 };
    };

protected:
    typedef ObCons<SoftVect *, SoftFloat *> pt_scale_cons;
    ObMap<SoftVect *, SoftFloat *> combos;
    ObTrove<Operate::Thisaways> operators;

public:
    ComboSoftVect (SoftVect *vect) : SoftVect () { combos.Put (vect, NULL); }

    ComboSoftVect (SoftVect *vect, SoftFloat *scale) : SoftVect () {
        combos.Put (vect, scale);
    }

    virtual ~ComboSoftVect () {}

    virtual void
    AppendSoftCombo (SoftVect *vect, SoftFloat *scale, Operate::Thisaways op) {
        combos.Put (vect, scale);
        operators.Append (op);
    }

    virtual void
    AppendSoftCombo (SoftVect *vect, float64 scale, Operate::Thisaways op) {
        combos.Put (vect, new SoftFloat (scale));
        operators.Append (op);
    }

    virtual void
    AppendSoftCombo (Vect vect, SoftFloat *scale, Operate::Thisaways op) {
        combos.Put (new SoftVect (vect), scale);
        operators.Append (op);
    }

    virtual void
    AppendSoftCombo (Vect vect, float64 scale, Operate::Thisaways op) {
        combos.Put (new SoftVect (vect), new SoftFloat (scale));
        operators.Append (op);
    }

    virtual void AppendSoftVect (SoftVect *vect, Operate::Thisaways op) {
        combos.Put (vect, NULL);
        operators.Append (op);
    }

    virtual void AppendSoftVect (Vect vect, Operate::Thisaways op) {
        combos.Put (new SoftVect (vect), NULL);
        operators.Append (op);
    }

    virtual void ShouldFreshenUp () { needs_freshening = true; }

    virtual ObRetort Inhale (Atmosphere *atm) {
        if (IsAlreadyCurrent (atm))
            return OB_ALREADY_CURRENT;

        MakeCurrent (atm);
        changed_during_this_quantum = false;

        ObCrawl<pt_scale_cons *> cr = combos.Crawl ();
        Vect new_v;
        int i = -1;
        while (!cr.isempty ()) {
            pt_scale_cons *con = cr.popfore ();
            SoftVect *v = con->Car ();
            if (!v)
                continue;
            SoftFloat *s = con->Cdr ();
            Vect tmp = v->Val ();
            /// if any of our dependent vects/floats changed,
            /// then we have changed
            if (!v->IsStatic ())
                changed_during_this_quantum = true;
            /// multiply the scalar if it exists
            if (s) {
                if (!s->IsStatic ())
                    changed_during_this_quantum = true;
                tmp = tmp * s->Val ();
            }
            if (i >= 0) {
                Operate::Thisaways op = operators.Nth (i);
                switch (op) {
                case Operate::ADD:
                    new_v = new_v + tmp;
                    break;
                case Operate::SUBTRACT:
                    new_v = new_v - tmp;
                    break;
                case Operate::SCALE:
                    new_v = new_v.Scale (tmp);
                    break;
                case Operate::CROSS:
                    new_v = new_v.Cross (tmp);
                    break;
                }
            } else
                new_v = tmp;
            i++;
        }

        if (changed_during_this_quantum) {
            val = new_v;
            needs_freshening = false;
            return OB_EXPENDED_EFFORT;
        }

        return OB_OK;
    }
};
}
} // so sad the passing of namespace basement, of namespace oblong

#endif
