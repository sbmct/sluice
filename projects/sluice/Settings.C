
/* (c)  oblong industries */

#include "Settings.h"

#include <libLoam/c/ob-log.h>
#include <libLoam/c/ob-retorts.h>

#include <libPlasma/c++/Protein.h>
#include <libPlasma/c++/Slaw.h>

using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

static Settings *shared_settings;

Settings *Settings::SharedSettings () { // Create a shared settings object if
                                        // not already available
    if (!shared_settings)
        shared_settings = new Settings;
    return shared_settings;
}

ObRetort Settings::LoadSettingsFile (const Str &file) {
    ObRetort retort;
    Protein p = Protein (Slaw::FromFile (file, &retort));

    if (p.IsNull () || retort.IsError ()) {
        OB_LOG_DEBUG ("Unable to load app settings from protein: %s",
                      retort.Description ().utf8 ());
        return retort;
    }

    return MetabolizeProtein (p, NULL);
}

ObRetort Settings::UpdateSettingsProtein (KneeObject *ko,
                                          const Protein &p,
                                          Atmosphere *atm) {
    return MetabolizeProtein (p, atm);
}

void Settings::SetUpMetabolizer (Slaw descrips) {
    this->AppendMetabolizer (descrips, &Settings::MetabolizeProtein,
                             "app-settings-update");
}

ObRetort Settings::MetabolizeProtein (const Protein &p, Atmosphere *atm) {
    OB_LOG_DEBUG ("loading settings from protein");

    Slaw descrips = p.Descrips ();
    if (!descrips.ListContains ("app-settings"))
        return OB_UNKNOWN_ERR;

    settings = p.Ingests ();
    return OB_OK;
}
