
/* (c)  oblong industries */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <libBasement/KneeObject.h>

#include <libPlasma/c++/Slaw.h>

namespace oblong {
namespace noodoo {
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class Settings : public KneeObject {
    PATELLA_SUBCLASS (Settings, KneeObject);

public:
    Slaw settings;

public:
    static Settings *SharedSettings ();
    Slaw SettingsSlaw () { return settings; }

    ObRetort LoadSettingsFile (const Str &file);
    ObRetort
    UpdateSettingsProtein (KneeObject *ko, const Protein &p, Atmosphere *atm);
    void SetUpMetabolizer (Slaw descrips);
    ObRetort MetabolizeProtein (const Protein &p, Atmosphere *atm);
};
}
}

#endif
