
/* (c)  oblong industries */

#ifndef WIDESCREEN_RULES
#define WIDESCREEN_RULES

#include "Acetate.h"
#include "MzHandi.h"
#include "MzTender.h"
#include "MzMigrate.h"

#include "ClipShack.h"
#include "DisplayStrs.h"

#include "Fluoroscope.h"
#include "Exoskeleton.h"
#include "HeartbeatThing.h"
#include "Navigator.h"
#include "WebFluoroscope.h"
#include "Gazetteer.h"

#include <libNoodoo/GlyphoString.h>
#include <libNoodoo/TexQuad.h>

#include <libBasement/SoftColor.h>

#include <libBasement/ob-hook-troves.h>

namespace oblong {
namespace sluice {

using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class TopoWard;
class NodeStore;

/**
 * A WideScreen belongs in the center of the main area and fills
   all three screens
 */
class WideScreen : public Acetate,
                   public MzHandi::Evts,
                   public MzTender::Evts,
                   public MzMigrate::Evts {
    PATELLA_SUBCLASS (WideScreen, Acetate);

private: // todo: get rid of public everything
    int32 orig_ordinal, ordinal;
    bool nascent;

    FatherTime timer;

    ObRef<ClipShack *> clippy;
    ObRef<Fluoroscope *> mappy;

    ObRef<SoftColor *> bg_iro;
    ObRef<GlyphoString *> bg_text;

    ObRef<GlyphoString *> time_text;
    Slaw time_display_cfg = Slaw::Map (
        "visible", false, "feld", "main", "loc", v2float64{-0.5, 0.5});

    ObRef<HeartbeatThing *> heartbeat;
    NodeStore *node_store;
    TopoWard *topo_ward;
    ObRef<Gazetteer *> gazetteer;
    ObRef<GlyphoString *> progress_text;

    Str uid;

    Str content_source;

    ObRef<DisplayStrs *> display_strs;

    ObMap<Str, Navigator *, NoMemMgmt, WeakRef> navigator_by_provenance;

    ObMap<Str, FlatThing *, NoMemMgmt, WeakRef> hoveree_by_provenance;
    ObMap<Str, FlatThing *, NoMemMgmt, WeakRef> manipulee_by_provenance;
    ObMap<Str, FlatThing *, NoMemMgmt, WeakRef> regulee_by_provenance;
    ObMap<FlatThing *, Exoskeleton *, WeakRef, WeakRef> exoskeleton_by_ft;

    ObMap<Str, TexQuad *> nascent_shadow_by_provenance;

    void AnnounceFluoroscopeList () const;
    void HandiRemoveByVal (Fluoroscope *fluoro);

    FLAG_MACHINERY (FluoroscopeListUpdated);

public:
    WideScreen ();
    virtual ~WideScreen ();

    virtual ObRetort Inhale (Atmosphere *) override;

    NodeStore *GetNodeStore () const { return node_store; }

    virtual void InitiallyUnfurl (Atmosphere *atm);
    virtual void ArrangeDenizens ();
    virtual void InitializePoolRegistration (Hasselhoff *hoff);
    virtual void AddInculcators (Slaw inc_config);
    virtual void RemoveInculcators (Slaw inc_config);

    int32 OrigOrdinal () const { return orig_ordinal; }
    void SetOrigOrdinal (int32 oo) { orig_ordinal = oo; }

    int32 Ordinal () const { return ordinal; }
    void SetOrdinal (int32 o) { ordinal = o; }

    bool IsNascent () const { return nascent; }
    void SetIsNascent (bool nasc) { nascent = nasc; }

    ObRetort HandleNewWebRequest (const Protein &prt, Atmosphere *atmo);

    ObRetort HandleFluoroRequest (const Protein &p, Atmosphere *atm);

    bool FluoroNameFromConfig (Slaw config, Str *fl_name);
    Slaw GetFluoroConfigFromSlaw (Slaw config);
    bool PlaceFluoroByConfig (Fluoroscope *fluoro, Slaw config);
    bool PlaceFluoroAtNode (Fluoroscope *fluoro, Slaw config);
    bool PlaceFluoroByConfigBounds (Fluoroscope *fluoro, Slaw config);
    bool PlaceFluoroByWindshieldLoc (Fluoroscope *fluoro, Slaw config);
    bool PlaceFluoroByDefaultBounds (Fluoroscope *fluoro);
    Fluoroscope *MakeNewFluoroInstance (Slaw config);
    bool ExtractQID (Slaw search, ObQID *qid);
    Fluoroscope *FindScope (Slaw search);
    ObRetort MetabolizeReorderRequest (const Protein &prt, Atmosphere *atm);

    ObRetort MetabolizeWebAppFluoroHover (const Protein &pro, Atmosphere *);
    ObRetort MetabolizeWebAppNoHover (const Protein &pro, Atmosphere *);

    Slaw GetFluoroConfigFromTemplate (Str fl_s);
    ObRetort MetabolizeNewFluoroInstance (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeRemoveFluoroInstance (const Protein &prt,
                                             Atmosphere *atm);
    ObRetort MetabolizeListFluoroscopeTemplates (const Protein &, Atmosphere *);
    ObRetort MetabolizeRemoveFluoroTemplate (const Protein &prt,
                                             Atmosphere *atm);
    ObRetort MetabolizeRemoveAllFluoros (const Protein &, Atmosphere *);

    WebFluoroscope *EnsureWebby ();
    WebFluoroscope *EnsureWebby (Str nam);
    // If there's a webby, grab it.
    WebFluoroscope *ExistingWebby (Str nam);

    const ObColor &BackgroundColor () const { return (~bg_iro)->Val (); }
    const ObColor &BackgroundColorGoalVal () const {
        return (~bg_iro)->GoalVal ();
    }
    void SetBackgroundColor (const ObColor &iro) { (~bg_iro)->Set (iro); }
    void SetBackgroundColorHard (const ObColor &iro) {
        (~bg_iro)->SetHard (iro);
    }
    void InstallBackgroundColor (SoftColor *sc) {
        if (sc) {
            sc->Set (BackgroundColorGoalVal ());
            RelinquishSoft (~bg_iro);
            bg_iro = sc;
            SuperviseSoft (sc);
        }
    }

    GlyphoString *BackgroundText () const { return ~bg_text; }
    void InstallBackgroundText (GlyphoString *bgt) {
        if (GlyphoString *old_bgt = ~bg_text) {
            RemoveChild (old_bgt);
        }
        AppendChild (bgt);
        bg_text = bgt;
    }
    void SetBackgroundText (const Str &txt) {
        if (GlyphoString *bgt = ~bg_text) {
            bgt->SetString (txt);
        }
    }
    void UpdateBackgroundTextWithOrdinals () {
        if (GlyphoString *bgt = ~bg_text) {
            bgt->SetString (
                Str ().Sprintf ("{ %d : %d }", ordinal, orig_ordinal));
        }
    }

    void ConfigureTimeDisplay (const Slaw ing);

    GlyphoString *TimeText () const { return ~time_text; }
    void InstallTimeText (GlyphoString *tv) {
        if (GlyphoString *old_time = ~time_text) {
            RemoveChild (old_time);
        }
        AppendChild (tv);
        time_text = tv;
    }
    void SetTimeText (const Str &tt) {
        if (GlyphoString *tv = ~time_text) {
            tv->SetString (tt);
        }
    }

    const Str &UID () const { return uid; }
    void SetUID (const Str &u) { uid = u; }

    const Str &ContentSource () const { return content_source; }
    void SetContentSource (const Str &cs) { content_source = cs; }

    void InstallClippers (ClipShack *cs);
    ClipShack *OurClippy () { return ~clippy; }

    void InstallMap (Fluoroscope *gf, bool load_config);
    Fluoroscope *MainMap () const { return ~mappy; }

    void InstallGazetteer (Gazetteer *gz);
    Gazetteer *OurGazetteer () { return ~gazetteer; }

    HeartbeatThing *Heartbeat () { return ~heartbeat; }

    FlatThing *TopmostFlatThing (const Vect &from, const Vect &to);

    ObRetort ClearFluoroscopes ();

    void AppendItemToClip (FlatThing *ft);
    void RemoveItemFromClip (FlatThing *ft);
    ObRetort ProgressUpdated (Str txt);

    virtual ObRetort RemoveExoskeletonIfStale (FlatThing *ft);
    bool RemoveAllExoskeletonedFluoroscopes ();

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

    ObRetort MetabolizeTimeChange (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeTimeAnnounce (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeTimeDisplaySetCfg (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeTimeDisplayRequestCfg (const Protein &prt,
                                              Atmosphere *atmo);
    void TimeDisplayPSA () const;

    void SendTimeChangeNotificationProtein ();
    ObRetort SendTimeChangedUpdate (NodeStore *store);

    Str NodeStoreForcedZoomLevel () const {
        return node_store->ForcedZoomLevel ();
    }
    void SetNodeStoreForcedZoomLevel (const Str &s) {
        node_store->ForceZoomLevel (s);
    }

    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiCondense (MzHandiCondenseEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                           Atmosphere *atm);
    ObRetort DescentHandiTargets ();

    virtual ObRetort MzTenderProbe (MzTenderProbeEvent *e, Atmosphere *atm);
    virtual ObRetort MzTenderSolidify (MzTenderSolidifyEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzTenderRescind (MzTenderRescindEvent *e, Atmosphere *atm);

    virtual ObRetort MzMigrateBegin (MzMigrateBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzMigrateContinue (MzMigrateContinueEvent *e,
                                        Atmosphere *atm);
    virtual ObRetort MzMigrateEnd (MzMigrateEndEvent *e, Atmosphere *atm);
    virtual ObRetort MzMigrateAbort (MzMigrateAbortEvent *e, Atmosphere *atm);

    /// Fluoroscope handling?
    virtual int64 Empty ();

    //------------------------------------------------------------
    // Refactoring for Mapper
    //------------------------------------------------------------
    const float64 VisibleRegionHeight ();
    const bool IsThisKOTheMainMap (KneeObject *ko);
    const GeoRect MainMapVisibleBounds ();

    DisplayStrs *GetDisplayStrs ();
};
}
} // bye bye namespaces sluice, oblong

#endif
