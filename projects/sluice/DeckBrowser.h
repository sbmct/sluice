
/* (c)  oblong industries */

#ifndef DECK_BROWSER_HINTINGS
#define DECK_BROWSER_HINTINGS

#include "Acetate.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * The DeckBrowser shows a clipped collection of Decks in a view that
 * can be scrolled vertically. Or, more correctly, it will someday.
 */
class DeckBrowser : public Acetate {
    PATELLA_SUBCLASS (DeckBrowser, Acetate);

public:
    virtual void InitiallyUnfurl (Atmosphere *atm);
};
}
} // bye bye namespaces sluice, oblong

#endif
