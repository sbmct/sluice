#!/bin/sh

d="`dirname $0`/../debs/sluice-gems"
gems="${d}/opt/oblong/sluice-64-2/lib"

SLUICE_GEMS="rmagick"

test -d "${d}/opt" && rm -fr "${d}/opt"

mkdir -p "$gems"

gem install --no-ri --no-rdoc \
    --install-dir "$gems" $SLUICE_GEMS || {
    echo "Failed to build Sluice gems: $SLUICE_GEMS" 1>&2
    exit 1
}

