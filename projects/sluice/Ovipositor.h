
/* (c)  oblong industries */

#ifndef OVIPOSITOR_REALLY_OUGHT_TO_DO_BACON_TOO
#define OVIPOSITOR_REALLY_OUGHT_TO_DO_BACON_TOO

#include "Tweezers.h"

#include "MzHandi.h"

#include "UnduLine.h"

#include <libBasement/TWrangler.h>
#include <libNoodoo/GlyphoString.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * TODO (mschuresko) : There are four classes that deal directly
 * with UnduLine in Sluice.  Please document which one is used for
 * which UnduLine.
 *
 * As far as I can tell, places where UnduLine is used include
 * 1) When dragging a new Fluoroscope onto the map
 * 2) When dragging an existing Fluoroscope around the map
 * 3) When dragging the map itself
 *
 * Classes which use Unduline are
 * 1) BumbleBells (Not used directly by any other .C or .h file
 *    in the sluice project ?!?)
 * 2) Ovipositor (deposits eggs -- so I'm guessing handles the
 *    dragging of new FluoroScopes onto the map)
 * 3) Pelligator (I think this handles map move and zoom?)
 * 4) Regulator (By process of elimination I'm guessing this
 *    handles moving a Fluoroscope around the map.  Seems to contain
 *    code for deleting Fluroscope instances as well)
 */
class Ovipositor : public Tweezers, public MzHandi::Evts {
    PATELLA_SUBCLASS (Ovipositor, Tweezers);

public:
    Str motive_prov;
    ObWeakRef<Acetate *> source_entity;
    ObWeakRef<KneeObject *> ovum;
    ObRef<UnduLine *> umbilical;
    ObRef<FlatThing *> distal_end;
    ObRef<FlatThing *> estab_end;
    ObRef<GlyphoString *> info_string;
    //  ObRef <TWrangler *> distal_loc;
    Vect estab_loc;
    FatherTime elapsed_uhr;
    static const Str prv_extra;

public:
    Ovipositor ();
    virtual ~Ovipositor ();

    const Str &MotiveProvenance () const { return motive_prov; }
    void SetMotiveProvenance (const Str &mp) { motive_prov = mp; }

    Acetate *SourceEntity () const { return ~source_entity; }
    void SetSourceEntity (Acetate *ate) {
        if (ate)
            source_entity = ate;
        else
            source_entity.Nullify ();
    }

    KneeObject *Ovum () const { return ~ovum; }
    void SetOvum (KneeObject *ko) {
        if (ko)
            ovum = ko;
        else
            ovum.Nullify ();
    }

    const Vect &DistalLoc () const;
    void SetDistalLoc (const Vect &loc) {
        if (FlatThing *de = ~distal_end)
            de->SetLoc (loc);
        if (UnduLine *ul = ~umbilical)
            ul->SetP3 (loc);
    }
    void SetDistalLocHard (const Vect &loc) {
        if (FlatThing *de = ~distal_end)
            de->SetLocHard (loc);
        if (UnduLine *ul = ~umbilical)
            ul->SetP3Hard (loc);
    }

    const Vect &EstabLoc () const { return estab_loc; }
    void SetEstabLoc (const Vect &loc) {
        estab_loc = loc;
        if (FlatThing *ee = ~estab_end)
            ee->SetLoc (loc);
        if (UnduLine *ul = ~umbilical)
            ul->SetP0 (loc);
    }
    void SetEstabLocHard (const Vect &loc) {
        estab_loc = loc;
        if (FlatThing *ee = ~estab_end)
            ee->SetLocHard (loc);
        if (UnduLine *ul = ~umbilical)
            ul->SetP0Hard (loc);
    }

    virtual void InitiallyUnfurl (Atmosphere *atm);

    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);

    virtual ObRetort Dismiss (bool break_connection = true);

    static Str EnhancedProvenance (const Str &prv);
    static Str OriginalProvenance (const Str &prv);

    // To handle responses from MzTender events
    // Variables that can be stored in the info Slaw is as follows:
    //   action: "deposit" means the item can be deposited into the target
    //           "rejected" means the target rejects the item being deposited
    //   color: adjcolor of the ovipositor
    //   message: Short string to show user what is about to happen or why
    //            a deposit may be rejected
    //
    ObRetort
    ReturnStroke (const Slaw &info, ElectricalEvent *ee, Atmosphere *atm);
};
}
} // what a send-off! there go namespaces sluice and oblong...

#endif
