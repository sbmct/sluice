
/* (c)  Oblong Industries */

#ifndef WEBFLUOROSCOPE_H
#define WEBFLUOROSCOPE_H

#include <libLoam/c++/FatherTime.h>
#include <libTwillig/WebThing.h>
#include "Fluoroscope.h"

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::twillig;

/**
 * Show a web browser
 *
 */
class WebFluoroscope : public Fluoroscope {
    PATELLA_SUBCLASS (WebFluoroscope, Fluoroscope);

public:
    WebFluoroscope ();
    virtual ~WebFluoroscope () {}

    void NavigateTo (Str url);
    void NavigateTo (const Str &url, const std::vector<unt8> &post);
    ObRetort RunJS (const Str &js);
    Str Url () const;
    void SetBrowserSize (int64 width, int64 height);
    void SetBrowserSize (const v2int64 &);
    v2int64 BrowserSize () const;
    int64 BrowserWidth () const;
    int64 BrowserHeight () const;
    float64 BrowserAspect () const;

    void ForceBackground (ObColor);
    void RemoveBackground ();

    virtual Str DisplayText ();
    virtual ObRetort SetConfigurationSlaw (const Slaw config);
    virtual ObRetort AcknowledgeSizeChange ();
    virtual ObRetort ScaleBrowserToFluoroBounds ();
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                           Atmosphere *atm);

    virtual bool AcceptsPassthrough () const override { return true; }

    virtual ObRetort Inhale (Atmosphere *) override;

private:
    WebFluoroscope (const WebFluoroscope &);
    WebFluoroscope &operator=(const WebFluoroscope &);

    void FireInitJs ();
    void FireMoveJs ();
    void TryFireInitJs ();
    void TryFireMoveJs ();

    Str init_js, move_js;

    FLAG_MACHINERY (FireInitJs);
    FLAG_MACHINERY (FireMoveJs);
};

class GeoWebQuad : public GeoQuad {
    PATELLA_SUBCLASS (GeoWebQuad, GeoQuad)
private:
    ObRef<WebThing *> web_thing;

public:
    GeoWebQuad ();
    virtual ~GeoWebQuad () {}

    WebThing *WThing () { return ~web_thing; }

    void ForceBackground (ObColor);
    void RemoveBackground ();

    void InstallLoc (SoftVect *sv) {
        super::InstallLoc (sv);
        (~web_thing)->InstallLoc (sv);
    }

    void InstallNorm (SoftVect *sv) {
        super::InstallNorm (sv);
        (~web_thing)->InstallNorm (sv);
    }

    void InstallOver (SoftVect *sv) {
        super::InstallOver (sv);
        (~web_thing)->InstallOver (sv);
    }

    void InstallWidth (SoftFloat *sf) {
        super::InstallWidth (sf);
        (~web_thing)->InstallWidth (sf);
    }

    void InstallHeight (SoftFloat *sf) {
        super::InstallHeight (sf);
        (~web_thing)->InstallHeight (sf);
    }

    void InstallVerticalAlignment (SoftFloat *sf) {
        super::InstallVerticalAlignment (sf);
        (~web_thing)->InstallVerticalAlignment (sf);
    }

    void InstallHorizontalAlignment (SoftFloat *sf) {
        super::InstallHorizontalAlignment (sf);
        (~web_thing)->InstallHorizontalAlignment (sf);
    }
};
}
} // namespace oblong::sluice

#endif
