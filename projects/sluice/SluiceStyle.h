
/* (c)  oblong industries */

#ifndef SLUICE_STYLE
#define SLUICE_STYLE

#include <libLoam/c++/AnkleObject.h>
#include <libLoam/c++/ObColor.h>

#include <libBasement/SoftColor.h>

#include "Acetate.h"

// some day I'd like this to include some FeldGeomPod based-calculations.

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

static Str handi_point = "pointing";
static Str handi_navi = "navigating";
static Str handi_pass = "passthrough";

class SluiceStyle : public KneeObject {
    PATELLA_SUBCLASS (SluiceStyle, KneeObject);

private:
    static ObColor feld_background_color;

    static ObColor buffed_acetate_background_color;
    static ObColor buffed_acetate_border_color;

    static ObColor exoskeleton_background_color_center;
    static ObColor exoskeleton_border_color_center;

    static ObColor exoskeleton_background_color_amplitude;
    static ObColor exoskeleton_border_color_amplitude;

    static ObColor exoskeleton_text_color;

    static ObColor regulator_fill_color;
    static ObColor regulator_stroke_color;

    static ObColor regulator_origin_marker_fill_color;
    static ObColor regulator_origin_marker_stroke_color;

    static float64 boog_reporter_alpha;

    static ObColor handi_point_fill_color;
    static ObColor handi_point_stroke_color;

    static float64 small_label_text_size;
    static float64 large_label_text_size;
    static float64 exoskeleton_label_text_size;

    static float64 exoskeleton_inset;
    static float64 exoskeleton_label_padding;
    static float64 exoskeleton_hull_size;

    static float64 asset_hutch_gutter_fraction;
    static float64 asset_hutch_inset_fraction;

    static float64 vidflux_browser_gutter_fraction;
    static float64 vidflux_browser_inset_fraction;

    static float64 regulator_mover_weight;
    static float64 regulator_mover_radius;
    static float64 regulator_scaler_weight;
    static float64 regulator_detent_weight;
    static float64 regulator_plus_weight;

    static float64 regulator_plus_initial_size;
    static float64 regulator_detent_radius;

    static float64 regulator_scale_detent_dist;

    static float64 handipoint_size;

    static float64 exoskeleton_animation_duration;
    static float64 exoskeleton_pulse_frequency;

    static ObColor widescreen_background_color;

    static Str shark_texture;
    static Str default_node_texture;
    static Str default_highlighted_node_texture;
    static Str default_highlighted_node_texture_name;
    static float64 default_node_scale;
    static ObColor default_path_color;
    static float64 default_path_width;
    static float64 default_highlighted_path_width;

public:
    struct ProvId : public AnkleObject {
        ObColor color;
        v2float64 quadrant;
        ProvId (const ObColor &col, const v2float64 &v) {
            color = col;
            quadrant = v;
        }
    };

private:
    static ObMap<Str, ProvId *> prov_style_map;

public:
    // static SluiceStyle *Guide ();
    static ObRetort
    ApplyUpdateProtein (KneeObject *ko, const Protein &p, Atmosphere *atm);

    // colors ------------------------------------------------------------------
    static ObColor FeldBackgroundColor ();

    static ObColor BuffedAcetateBackgroundColor ();
    static ObColor BuffedAcetateBorderColor ();

    static ObColor ExoskeletonBackgroundColorCenter ();
    static ObColor ExoskeletonBorderColorCenter ();

    static ObColor ExoskeletonBackgroundColorAmplitude ();
    static ObColor ExoskeletonBorderColorAmplitude ();

    static ObColor ExoskeletonTextColor ();

    static ObColor RegulatorFillColor ();
    static ObColor RegulatorStrokeColor ();

    static ObColor RegulatorOriginMarkerFillColor ();
    static ObColor RegulatorOriginMarkerStrokeColor ();

    static ObColor RegulatorScaleOriginFillColor ();
    static ObColor RegulatorScaleOriginStrokeColor ();

    static float64 BoogReporterAlpha ();

    static ObColor NamedProvenanceColor (const Str &col_name);
    static void SetProvenanceIdentity (const Str &prv, ProvId *pid);
    static ProvId *ProvenanceIdentity (const Str &prv);
    static ObColor ProvenanceColor (const Str &provenance);

    static ObColor HandiPointFillColor ();
    static ObColor HandiPointStrokeColor ();

    // fonts -------------------------------------------------------------------
    static float64 SmallLabelTextSize ();
    static float64 LargeLabelTextSize ();

    static float64 ExoskeletonLabelTextSize ();

    // spacing & sizing --------------------------------------------------------
    static float64 ExoskeletonInset ();
    static float64 ExoskeletonLabelPadding ();
    static float64 ExoskeletonHullSize ();

    static float64 AssetHutchGutterFraction ();
    static float64 AssetHutchInsetFraction ();

    static float64 VidfluxBrowserGutterFraction ();
    static float64 VidfluxBrowserInsetFraction ();

    static float64 RegulatorMoverWeight ();
    static float64 RegulatorMoverRadius ();
    static float64 RegulatorScalerWeight ();
    static float64 RegulatorDetentWeight ();
    static float64 RegulatorPlusWeight ();

    static float64 RegulatorPlusInitialSize ();
    static float64 RegulatorDetentRadius ();

    static float64 RegulatorScaleDetentDist ();

    static float64 HandiPointSize ();

    // timing ------------------------------------------------------------------
    static float64 ExoskeletonAnimationDuration ();
    static float64 ExoskeletonPulseFrequency ();

    static ObColor WideScreenBackgroundColor ();

    //  static float64 RegulatorFadeDuration ();
    // static float64 BoogReporterFadeDuration ();

    // regulator line weights
    // regulator colors

    // inset and gutter fractions for paramus, hoboken

    // node art ----------------------------------------------------------------
    static Str SharkTexture ();
    static Str DefaultNodeTexture ();
    static Str DefaultHighlightedNodeTexture ();
    static Str DefaultHighlightedNodeTextureName ();
    static float64 DefaultNodeScale ();
    static ObColor DefaultPathColor ();
    static float64 DefaultPathWidth ();
    static float64 DefaultHighlightedPathWidth ();
};
}
} // what a send-off! there go namespaces sluice and oblong...

#endif
