
#pragma once

#include <libLoam/c++/ObMap.h>
#include <libPlasma/c++/Hose.h>
#include <libBasement/KneeObject.h>
#include <libBasement/ImageClot.h>
#include <libBasement/ob-hook-troves.h>

#include "TileID.h"
#include "TexGLCache.h"

#include <boost/thread.hpp>
#include <set>

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::plasma;
using namespace oblong::basement;

class TileCache : public KneeObject {
    PATELLA_SUBCLASS (TileCache, KneeObject);

private:
    boost::mutex mutex;
    boost::thread *clot_fetcher;
    bool quit;
    int tile_size;

    ObRef<Hose *> input_hose, output_hose;

    // This is the only structure that shall be locked
    ObMap<TileID, ImageClot *> incoming;

    // The request queue
    TileID center;
    std::vector<TileID> requested;
    std::set<TileID> requested_set;
    std::map<TileID, float64> waiting;
    int requests_per_breath;
    float64 min_retry;

    TexGLCache texgl_cache;

    void DepositRequest (const TileID &tile);
    void Store (const TileID &tile, ImageClot *clot);

    ObRetort HandleTileExpiration (const TileID &);

    SINGLE_ARG_HOOK_MACHINERY (TileArrived, const TileID &);
    SINGLE_ARG_HOOK_MACHINERY (TileExpired, const TileID &);

public:
    TileCache (const int _tile_size, Hose *, Hose *);
    void Shutdown ();

    void Start ();

    void SetRequestsPerBreath (const int x);
    void SetMaximumTexGLCacheSize (const unt64 x);

    // request queue
    void SetCenter (const TileID c);
    void SetCenter (const int64 x, const int64 y, const int64 z);
    void SetMap (const Str mapid);
    void AppendTileRequest (const TileID tileid);
    const TileID PopNextClosest ();
    // eueuq tseuqer

    TileID SetBigTileStack (const TileID tid);

    const bool Has (const TileID tid) const;
    const bool Requested (const TileID &tik) const;
    const size_t RequestQueueSize () const;
    TexGL *TileFor (const TileID tid);

    virtual ObRetort Inhale (Atmosphere *atmo);
    virtual ObRetort Travail (Atmosphere *atmo);
};
}
}
