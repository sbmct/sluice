
#ifndef THERE_IS_A_PRETTY_NEAT_GROUPER_AT_BAHOOKA
#define THERE_IS_A_PRETTY_NEAT_GROUPER_AT_BAHOOKA

#include <libBasement/ob-hook-troves.h>

#include "Timeseries.h"
#include <set>
#include <vector>
#include <list>

#include "CompositeNode.h"

namespace oblong {
namespace sluice {

class Node;

class Grouper : public KneeObject {
    PATELLA_SUBCLASS (Grouper, KneeObject);

protected:
    bool should_group;
    float64 granularity, visible_degrees;

    typedef std::pair<float64, CompositeNode *> timecomp_t;
    typedef std::list<timecomp_t> comps_t;
    typedef TimeSeries<CompositeNode *, float64> comp_history_t;

    struct nodeinfo_t {
        comps_t allcomps;
        comp_history_t comp_history;
    };

    typedef std::map<Node *, nodeinfo_t> node_loc_history_t;
    node_loc_history_t node_locs;

    typedef std::map<int, ObRef<CompositeNode *>> comp_inner_map_t;
    typedef std::map<int, comp_inner_map_t> comp_map_t;
    comp_map_t composites;

    ObRef<CompositeNode *> &
    RefForLoc (const int lat, const int lon, bool &created);

    CompositeNode *NodeLoc (Node *n, const float64 t);
    void SetNodeLoc (Node *n, const float64 t, CompositeNode *comp);

    DOUBLE_ARG_HOOK_MACHINERY (CompositeUpdated, Node *, const float64);

public:
    Grouper (const float64 visible,
             const bool _should_group,
             const float64 _granularity);
    virtual ~Grouper ();

    const float64 VisibleDegrees () const;

    std::pair<Node *, bool> Group (Node *n, const float64 t);
    void ExpireNode (Node *n, const float64 t);
    void RemoveNode (Node *n);

    void Clear ();

    float64 Granularity () const { return granularity; }
    bool ShouldGroup () const { return should_group; }

    static Grouper *FromSlaw (const Slaw);
};
}
}

#endif
