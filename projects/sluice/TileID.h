
#pragma once

#include <libLoam/c++/Str.h>
#include <iostream>
#include <boost/unordered_map.hpp>
#include <boost/tuple/tuple.hpp>

namespace oblong {
namespace sluice {

using oblong::loam::Str;

class TileID {
private:
    int x, y, z;
    Str map;
    bool big;

public:
    TileID () : x (-1), y (-1), z (-1) {}
    TileID (const Str _map,
            const int _z,
            const int _y,
            const int _x,
            const bool _big = false)
        : x (_x), y (_y), z (_z), map (_map), big (_big) {}
    TileID (const TileID &other)
        : x (other.x), y (other.y), z (other.z), map (other.map),
          big (other.big) {}
    TileID &operator=(const TileID &other) {
        x = other.x;
        y = other.y;
        z = other.z;
        map = other.map;
        big = other.big;
        return *this;
    }

    /// Fetch our parent tile
    const TileID Up () const {
        return TileID (map, z - 1, y / 2, x / 2, false);
    }

    /// Fetch our four child tiles
    void Down (TileID &tl, TileID &tr, TileID &bl, TileID &br) const {
        const int zz = z + 1;
        const int xx = x + x;
        const int yy = y + x;
        tl = TileID (map, zz, xx, yy);
        tr = TileID (map, zz, xx + 1, yy);
        bl = TileID (map, zz, xx, yy + 1);
        br = TileID (map, zz, xx + 1, yy + 1);
    }

    bool operator<(const TileID &other) const {
        if (z < other.z)
            return true;
        if (z > other.z)
            return false;
        if (y < other.y)
            return true;
        if (y > other.y)
            return false;
        if (x < other.x)
            return true;
        if (x > other.x)
            return false;
        return map < other.map;
    }

    bool operator>(const TileID &other) const { return other < *this; }

    bool operator==(const TileID &other) const {
        return z == other.z && y == other.y && x == other.x && map == other.map;
    }

    bool operator!=(const TileID &other) const {
        return z != other.z || y != other.y || x != other.x || map != other.map;
    }

    const Str ToStr () const {
        return Str::Format ("%s_%d_%d_%d", map.utf8 (), z, y, x);
    }

    const int Z () const { return z; }
    const int X () const { return x; }
    const int Y () const { return y; }
    const Str Map () const { return map; }
    const bool IsBig () const { return big; }

    const bool IsCromulent () const {
        return z > -1 && x > -1 && y > -1 && (!map.IsNull ());
    }

    size_t HashValue () const {
        size_t seed = 0;
        boost::hash_combine (seed, z);
        boost::hash_combine (seed, y);
        boost::hash_combine (seed, x);
        boost::hash_combine (seed, map.utf8 ());
        return seed;
    }
};

inline std::size_t hash_value (TileID const &tid) { return tid.HashValue (); }

inline ::std::ostream &operator<<(std::ostream &os, const TileID &t) {
    os << "Tile(" << t.Map () << "," << t.Z () << "," << t.Y () << "," << t.X ()
       << ")";
    return os;
}
}
}
