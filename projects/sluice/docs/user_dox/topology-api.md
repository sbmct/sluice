Topology Protocol           {#topology-api}
=============

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; Topology API

## Overview
The Topology API provides a protocol for injecting and modifying the network model in Sluice.  The protocol also provides a way to receive a topology request event from Sluice to begin injecting the topology.

@image html topology.svg

sluice-to-topo proteins
- [Request Topology] (request-topology.html)

topo-to-sluice proteins
- [Inject Topology](inject-topology.html)
- [Update Topology] (update-topology.html)
- [Remove Topology] (remove-topology.html)
- [Clear Topology] (clear-topology.html)