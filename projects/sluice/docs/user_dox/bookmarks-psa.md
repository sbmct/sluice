Bookmarks List Announcement (PSA) Protocol       {#bookmarks-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Bookmark PSA

### What does it do?

This protein is sent from sluice to inform external processes of the known bookmarks.  This protein is sent whenever a bookmark is added, deleted or a request for the list is made.  A bookmark is a simple combination of a lat/lon location and a zoom level that can be used to quickly zoom the map view to a specific location. See the [Bookmarks Request Protocol](bookmarks-request.html) for the more details on the forming a request for this information.

@image html bookmark_psa.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, bookmarks ]
i:  {
        [
            {
                name: <code>Str</code> <em>bookmark_name</em>,
                lat: <code>float64</code> <em>latitude</em>,
                lon: <code>float64</code> <em>longitude</em>,
                level: <code>float64</code> <em>zoom_level</em>,
            },
            ...
        ]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - bookmarks

**Ingests:**

- name: [string]

  The name identifier of the bookmark.

- lat: [float64]

  The map view's center latitudinal value.

- lon: [float64]

  The map view's center longitude value.

- level: [float64]

  The map view's zoom level.

### Sample

@include proteins/bookmarks-psa.prot
