Observations Protocol           {#observations-api}
=============

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; Observations API

### What does it do?

Observations are frequent or real-time updates to the attributes associated with entities.  These are separated out from the topology due to the frequency with which these may be sent.  These observations only affect the attributes of an entity and can not be used to change the location of an entity.

@image html observations.svg

### Pool

obs-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, observations ]
i:  {
        observations:
            [
                {
                    id: <code>Str</code> <em>entity_id</em>,
                    timestamp: <code>Str</code> <em>timestamp</em>,
                    attrs:
                        {
                            <code>Str</code> <em>attr_1_id</em>: <code>Str</code> <em>attr_1_val</em>,
                            <code>Str</code> <em>attr_2_id</em>: <code>Str</code> <em>attr_2_val</em>,
                            ...
                        },
                },
                ... 
            ]
        remove-observations:
            [
                {
                    id: <code>Str</code> <em>entity_id</em>,
                    attr-id: <code>Str</code> <em>attribute_id</em>,
                    timestamp: <code>Str</code> <em>timestamp</em>
                },
                ...
            ]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - observations

**Ingests:**

  - observations: [slaw list]

    This key sets up the list of entities that have attributes to be updated and their updates.

  - remove-observations: [slaw list]

    This key sets up the list of entities that have attributes that need to be removed and the id of those attributes.

  - id: [string]

    This string provides a unique identifier for the entity being added. For instance, if your network is licensed drivers, this would be the license number for a driver.

  - timestamp: One of "YYYY-MM-DD HH:MM:SS POSIX_TIME_ZONE" or a float64 timestamp

    Each attribute change is associated with a timestamp so that a user can see the values change over time within sluice. The timestamp maps to machine time. If a float is provided, the number represents seconds elapsed since the Jan 1, 1970 epoch. If a string is provided, sluice will try to parse it into a date/time based on the time formats defined in the [sluice-settings] (sluice-settings.html) protein read in at run-time.  If no value is given, sluice will assign the current "live" machine time to the change.

  - attrs: [slaw map of strings to strings]

    The attrs mapping can provide both new attributes and updated values to existing attributes.  If an attribute id already exists for the entity, the value will be updated.  If an attribute id does not exist, then it is added to the set of attributes. All values in this map MUST BE strings.  If a float or integer is sent, sluice will not be able to convert it and the update will be ignored.

  - attr-id: [string]

    This field is used in the removal of attributes and corresponds to the key value of an attribute previously provided in the attrs map.  The attribute will be removed according to the rules of the timestamp provided above.

### Sample

@include proteins/observations.prot



