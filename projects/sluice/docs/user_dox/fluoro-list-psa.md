Fluoroscope Instances List Announcement (PSA) Protocol       {#fluoro-list-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Fluoroscope List PSA

### What does it do?

This protein is sent from sluice to inform external processes of all the fluoroscopes that are currently instantiated on the map plane or map windshield. The list also contains the entire configuration for each fluoroscope so that external processes can understand what the user is viewing as well as request updates to the configurations of the instances. See the [Fluoroscopes Request Protocol](fluoro-request.html) for the more details on the forming a request for this list. In addition to these ingests, the list also includes each fluoroscope's current position on the map - represented as the relative bounding box's bottom-left (bl) and top-right (tr) lat/lon coordinate pairs.

@image html fluoro_request.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [sluice, prot-spec v1.0, psa, fluoroscopes]
i:  {
        fluoroscopes:
            [
                {
                    SlawedQID: <code>unt8_array</code> <em>unique_id</em>,
                    <em>*See the <a href="fluoro-configuration-spec.html">Fluoroscope Configuration Protein Spec</a> for documentation on the rest of this map</em>
                    bounds:
                      bl: <code>[lat, lon]</code>
                      tr: <code>[lat, lon]</code>
                },
                ...
            ]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - fluoroscopes

**Ingests:**

- fluoroscope: [slaw::list of slaw::maps]

  The key identifier for the list of fluoroscopes to come

- SlawedQID: [unt8 byte array]

  The unique id of the fluoroscope instance. This is needed for sending back any changes to the instance.

- *Rest of map

  Combined with the "SlawedQID" is the entire configuration map of the fluoroscope.  Take a look at the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for details on what all may be in this map.

- bounds: [slaw::map of slaw::lists]

  Two lat/lon coordinate pairs giving the bottom-left (bl) and top-right (tr) locations of the fluoroscope's bounding box at the time of the PSA.

### Sample

@include proteins/fluoro-list-psa.prot
