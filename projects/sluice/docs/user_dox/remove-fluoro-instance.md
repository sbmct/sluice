Remove a Fluoroscope Instance Protocol       {#remove-fluoro-instance}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Fluoroscope Instance Removal

### What does it do?

This protein is used to request to remove an existing fluoroscope from the Atlas.  It requires knowing the QID of the fluoroscope being deleted.  See [fluoroscope list request](fluoro-request.html) and [fluoroscope list psa](fluoro-list-psa.html) for the protocol on acquiring a list of all available fluoroscope instances.  Also look at the [new fluoroscope instance PSA](new-fluoro-instance-psa.html) and [response protein](new-fluoro-instance-response.html) to a [new fluoroscope instance request](new-fluoro-instance.html) for acquiring the QID whenever a new fluoroscope instance is created either programmatically or through the user interface.

@image html remove_fluoro_instance.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, remove-fluoro-instance ]
i:  {
        SlawedQID: <code>unt8_array</code> <em>unique_id</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - remove-fluoro-instance

**Ingests:**

  - SlawedQID: [unt8 byte array]

    The qid value (or unique id) of the fluoroscope to remove from the atlas. See the protocols outlined in the "What does it do?" section for retrieving this qid.

### Sample

@include proteins/remove-fluoro-instance.prot
