New Fluoroscope Instance Response Protocol       {#new-fluoro-instance-response}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; New Fluoroscope Instance Response

### What does it do?

This protein is used to respond to a programmatic [request for a new fluoroscope instance](new-fluoro-instance.html). It returns a success or failure message and on success includes the unique id of the resulting fluoroscope. You may also want to look at how to programmatically [remove a fluoroscope instance](remove-fluoro-instance.html).

@image html new_fluoro_instance.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, response, new-fluoro-instance ]
i:  {
        tort: <code>int32</code> <em>return_val</em>,
        msg: <code>Str</code> <em>return message</em>,
        SlawedQID: <code>unt8_array</code> <em>unique id</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - response
  - new-fluoro-instance

**Ingests:**

  - tort: (int32)

    This value follows the rules of an [ObRetort][1]. A negative value indicates failure while 0 or greater indicates success.  If a failure value is given, sluice will provide an appropriate error message in the "msg" field.

  - msg: (string)

    This string will provide a message describing the error if tort is a negative value or "Success!" if the tort is 0 or greater.

  - SlawedQID: [unt8 byte array]

    The qid value (or unique id) of the fluoroscope created by the request.

### Sample

@include proteins/new-fluoro-instance-response.prot

[1]: https://platform.oblong.com/dox/g-speak-api-reference/3.0/classoblong_1_1loam_1_1_ob_retort.html "ObRetort"