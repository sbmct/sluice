Running Sluice With Custom Configurations   {#running-sluice}
=========================================

[Home](index.html) &rarr; Running with Custom Configs

### Sluice Settings

Sluice reads in a single configuration protein which sets up all it's default fluoroscope templates, various paths, and time formats.

<b>[Click here for full documentation on the sluice settings protein](sluice-settings.html)</b>

By default, sluice is installed with a sluice-settings protein which can be found in the path:<br>
`/opt/oblong/sluice-64-2/etc`

To change the default settings, we recommend copying this file into your `~/.oblong/etc/sluice` folder and editing it there.  Editing the file in it's install location will cause problems on upgrade, as it will be overwritten.  Also, placing it in this location allows you to run without adding any command line arguments.

However, if you would like to store sluice-settings in another location or name it something else, you can run sluice with a command line argument to set this:

  - If running on a demo machine:

    Edit `/opt/oblong/sluice-64-2/bin/ctrl.sh` and change line 14 from `DAEMON_OPTS=""` to `DAEMON_OPTS="-s /path/to/new/sluice-settings.protein"`

  - If running on a developer workstation:

    Edit `/opt/oblong/sluice-64-2/bin/desktop-sluice.sh` and change lines ?

### Environment Settings

See the [File Paths page](file-paths.html) on how you can set environment variables to have sluice look in folders other than the default ones for configuration and resource files.