New Fluoroscope Template Response Protocol       {#fluoro-template-response}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Fluoroscope Template Response

### What does it do?

This protein is sent in response to a request for creating a new fluoroscope template. If there was a failure there is an extra descrip "advisory". The ingests will contain the full description of the success or failure.  See the [New Fluoroscope Template Request Protocol](new-fluoro-template.html) for details on the request. The main reason for failure is having the bin full and incapable of accepting any new fluoroscopes. The user will need to delete a template from the bin before a new one is accepted.  Currently, the maximum number of fluoroscope templates allowed is 12.

@image html new_fluoro_template.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
a) on success
d: [ sluice, prot-spec v1.0, response, new-fluoro-template ]
i:  {
        summary: Success
        description: There are <em>n</em> fluoroscopes in the fluoroscope palette.
    }

b) on failure
d: [ sluice, prot-spec v1.0, response, new-fluoro-template, advisory ]
i:  {
        summary: Sorry, the fluoroscope palette is full
        description: There are <em>n</em> fluoroscopes in the fluoroscope palette.
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - response
  - new-fluoro-template
  - advisory

    "advisory" is only included in the descrips if the response is a failure.

**Ingests:**

  - summary: [string]

    A quick summary of the success or failure.

  - description: [string]

    A longer description about the success or failure, giving the total number of fluoroscopes currently in the fluoroscope palette.

### Sample

@include proteins/fluoro-template-response.prot
