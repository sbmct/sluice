Remove a Web Page Instance Protocol       {#remove-web-instance}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Remove Web Page

### What does it do?

This protein is used to request the removal of an existing web page.  It can be used to remove either the web page that lies on the atlas view and/or any pages existing on the windshield.  Unlike the api for [removing fluoroscope instances](remove-fluoro-instance.html), the web api's refer to pages by "name" not "SlawedQID". See [this page](web-request.html) for how to programmatically create web pages on the windshield or on the atlas.

@image html remove_web_instance.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
a) display on map plane (like a fluoroscope)
d: [ sluice, prot-spec v1.0, request, remove-web-instance ]
i:  {
        remove-atlas-instance: <code>bool</code> <em>should_remove</em>,
        windshield:
            [
                {
                    name: <code>Str</code> <em>name_of_page</em>
                },
                ...
            ]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - remove-web-instance

**Ingests:**

  - remove-atlas-instance: (bool)

    This is an optional field use to clear the atlas view of its web page instance.  By default this value is false. Set it to true to remove the instance.

  - name: (string)

    The unique name of the web page to be removed from the windshield.  If no web page with this name is found, it is considered a no op.

### Sample

@include proteins/remove-web-instance.prot
