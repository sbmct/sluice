Upgrading Plasma API from 1.16 to Emu 1.18   {#emu-plasma-api-upgrade}
=================

[Home](index.html) &rarr; [Release Notes Home](release-notes.html) &rarr; [Emu Release Notes](emu-release-notes.html) &rarr; Upgrading the Plasma API

### References

 - [Sluice Protein API Home Page](sluice-plasma-api.html)

### Changed Pools

**tex-to-sluice:** This pool has changed to "fluoro-to-sluice." It is used for the same purpose of communication, but is now more generically named to accommodate all fluoroscope communication rather than simply texture fluoroscope communication.

**sluice-to-tex:** This pool has changed to "sluice-to-fluoro" for the same reasons as mentioned above.

### New Pools

**sluice-to-heart**: This is a new pool strictly purposed to house heartbeats coming from sluice.

**tiles-req**: This pool is used for the new tiling feature and is where sluice deposits requests for new tiles.

**tiles-resp-<em>rand</em>:** This pool is used for sending map tiles back to sluice.  The name of this pool changes but is sent along with the request so that the responding tile server can deposit its reply in the proper pool.

### Heartbeat Communication Proteins

The entirety of the heartbeat communication API is new and can be looked into [here](heartbeat-api.html).

### Topology Communication Proteins

##### Changed Proteins

[Clear Topology](clear-topology.html):  This protein didn't actually change, but the documentation previously was incorrect.  Use this link to see the proper protein format.

##### New Proteins

[Update Topology](update-topology.html): Sluice now properly implements the ability to over time change the topology and update the lat/lons of entities.

[Remove Entitie(s) From Topology](remove-topology.html): This protein was previously documented but not implemented. It is now a full member of the API so remove items at your leisure.

### Observations Communication Proteins

No proteins in the Topology Communication API changed formats or have been added. You can view the observations API [here](observations-api.html).

### Fluoroscope Communication Proteins

##### Changed Proteins

No proteins for texture fluoroscopes changed, but note above that the pool through which the communication goes through has changed to using the sluice-to-fluoro and fluoro-to-sluice pools.

##### New Proteins

All API surrounding the communication with video fluoroscopes has been added.  Check out the [Video Request](vid-request.html) and [Video Response](vid-response.html) pages for details.

### Edge Device Communication Proteins

##### Changed Proteins

User Action Communication:
  - [Entity Click Announcement](click-psa.html):
    - This protein now gets sent any time a user clicks while over the map.  If the user is not hovering any particular entity in the network, then the protein simply includes the location of the click. Otherwise, the protein remains the same with the one exception of "time" mentioned below.
    - **timestamp**: This field is now "time" in order to be consistent in our usage of "time" vs. "timestamp".  "timestamp" is used for indicating time that may or may not be the current time of sluice, while "time" is used when referring to the current time of sluice... So an entity change may have a "timestamp", while a user click is mapped to current "time".

Fluoroscope Manipulation/Creation Communication:
  - [New Fluoroscope Template Request](new-fluoro-template.html):

    **new-fluoro**: The last descrip has changed to now be "new-fluoro-template" in order to differentiate between whether this is a request for a new template or a new instant instance on the map.
  - [New Fluoroscope Template Response](fluoro-template-response.html):

    **new-fluoro**: The last descrip has changed to now be "new-fluoro-template" to match the request protein.

Layout Communication:
  - [Save Layout](mummy-request.html):

    **file-name**: Changed to "filename".  Note: The older documentation had this field already as "filename" but was at the time incorrect.
  - [Load Layout](vivify-request.html):

    **file-name**: Changed to "filename".  Note: The older documentation had this field already as "filename" but was at the time incorrect.

##### New Proteins

Communication for Determining current map view:
  - [Request Atlas View](atlas-request.html)
  - [Atlas View Announcement](atlas-view-psa.html)

Bookmarking and its communication:
  - [Request Bookmarks](bookmarks-request.html)
  - [Bookmark List Announcement](bookmarks-psa.html)
  - [Create Bookmark](new-bookmark-request.html)
  - [Delete Bookmark](del-bookmark-request.html)
  - [Bookmark Creation Error](bookmark-error.html)

User Action Communication:
  - [Skewer Announcements](skewer-psa.html): This protein API has been added to support a new feature which sets up the ability to build external applications to listen for these "skewer" announcements to know if a fluoroscope has been grabbed in a way to allow it to be dragged around the room and land on other screens.

Fluoroscope Manipulation/Creation Communication:
  - [New Fluoroscope Instance Request](new-fluoro-instance.html)

Time Control Communication: This protocol is not actually new, but it had not been documented previously
  - [Request Time](time-request.html)
  - [Time Announcement](time-psa.html)
  - [Set Time](set-time-request.html)

Security Communication: This protocol has been added to support minimal security integration with proper login procedures.
  - [Login User](login-request.html)
  - [Logout User](logout-request.html)
  - [Request Current User Credentials](credential-request.html)
  - [Current User Credentials Response](credential-response.html)

##### Removed Proteins

Faulted Entities Communication: these are temporarily "out of order"
  - [Fault List Request](fault-request.html)
  - [Fault List Announcement](fault-psa.html)

### Tile Server Communication Proteins

The entirety of the tile server communication API is new and can be looked into [here](tile-server-api.html).