File Paths of Sluice      {#file-paths}
====================

[Home](index.html) &rarr; File Paths

### Overview

Sluice uses the built in g-speak file path system for file lookups, which is based on the [Filesystem Hierarchy Standard](http://www.pathname.com/fhs/pub/fhs-2.3.html).  This system allows users to place files and configurations in user-specific locations that are first checked for files before moving to OS specific locations.  These locations differ for media resource files, configuration files, and variable data files and each are further described below.  In addition, these default paths can be overwritten using environment variables which sluice and all g-speak applications adhere to.

If you are looking for the paths of where the various components of sluice are installed, please refer to the [installation page](installation-guides.html).

**Note:** You can find the paths defined below on your own machine by running the g-speak command line utility, <code>ob-version</code>.  This utility informs you of the version of g-speak installed, along with all the default paths it uses.

<a name="share"></a>
### Resource Files

Resource files include all image and video resources that are used by sluice or any g-speak application for that matter. These must be purely static data. Any run-time modifiable files should be placed in the [var path](#var). Resource files include things like the icons used by network fluoroscopes and templates as well as resource files sluice uses internally for it's own rendering. When sluice is installed, these inherent resources are placed an the OS specific global location.  All user specific files should be placed in the user's folders or a new preferred user folder added to the <code>OB_SHARE_PATH</code> environment.

#### Internal/Installed Sluice Resources

  * **Ubuntu**

    /usr/share/oblong

 * **Mac OSX**

    /opt/oblong/g-speak-32-2/share/oblong

#### User/Client-specific Resources

We recommend you place any new resources in the following locations. The sluice installation does not place any items in these folders and upgrades will not overwrite these locations:

  * **Ubuntu**

    /home/<em>username</em>/.oblong/share

  * **Mac OSX**

    /Users/<em>username</em>/Library/Application Support/oblong/share

**Note**: Sluice is not supported on Mac OSX; these paths are give for helpful reference.

G-speak applications will first look in the user specific location before looking in the global resource location.  You can also add paths to these default locations by setting the <code>OB_SHARE_PATH</code> environment variable:

    OB_SHARE_PATH=/some/new/path:$OB_SHARE_PATH

**IMPORTANT:** Make sure you add the $OB_SHARE_PATH to the end of your environment setting to ensure that sluice still looks in the default locations to find internal resources needed.

<a name="etc"></a>
### Configuration Files

A "configuration file" is a local file used to control the operation of a program; it must be static and cannot be an executable binary. Configuration files include all files read in on startup or during run-time. They include things like the screen and feld proteins as well as the sluice-settings file and fluoroscope configurations. When sluice is installed, the default configurations are placed in an the OS specific global install location.  All user specific files should be placed in the user's folders listed below or a new preferred user folder added to the <code>OB_ETC_PATH</code> environment.

#### Internal/Installed Sluice Configurations

  * **Ubuntu**

    /etc/oblong

  * **Mac OSX**

    /etc/oblong

#### User/Client-specific Configurations

We recommend you place any new configurations and especially all fluoroscope configurations in the following locations. This ensures that the sluice installation/upgrade procedure will not overwrite or remove any of these items:

  * **Ubuntu**

    /home/<em>username</em>/.oblong/etc

  * **Mac OSX**

    /Users/<em>username</em>/Library/Application Support/oblong/etc

**Note**: Sluice is not supported on Mac OSX; these paths are give for helpful reference.

G-speak applications will first look in the user specific location before looking in the global etc location.  You can also add paths to these default locations by setting the <code>OB_ETC_PATH</code> environment variable:

    OB_ETC_PATH=/some/new/path:$OB_ETC_PATH

**IMPORTANT:** Make sure you add the $OB_ETC_PATH to the end of your environment setting to ensure that sluice still looks in the default locations to find internal configurations.

<a name="var"></a>
### Variable Data Files

Variable Data Files includes spool directories and files, administrative and logging data, and transient and temporary files.  For sluice, this is where you would find the saved off layouts and logs.  In general, you will not need to worry about these paths for your own configuration and integration effort, but they are documented here for debugging and completion sake.

#### Sluice Output Files

  * **Ubuntu**

    /var/ob

  * **Mac OSX**

    /var/ob

#### User/Client-specific Variable Data Files

  * **Ubuntu**

    /home/<em>username</em>/.oblong/var

  * **Mac OSX**

    /Users/<em>username</em>/Library/Application Support/oblong/var

**Note**: Sluice is not supported on Mac OSX; these paths are give for helpful reference.

G-speak applications will first look in the user specific location before looking in the global var location.  You can also add paths to these default locations by setting the <code>OB_VAR_PATH</code> environment variable:

    OB_VAR_PATH=/some/new/path:$OB_VAR_PATH

**IMPORTANT:** Make sure you add the $OB_VAR_PATH to the end of your environment setting to ensure that sluice still looks in the default locations to find output files.
