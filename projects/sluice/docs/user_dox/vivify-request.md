Request To Load a Layout Protocol       {#vivify-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Open Layout

### What does it do?

Use this protein to recall and load a previously saved layout.  Doing so will recall all the fluoroscope templates, fluoroscope instances, the map view, and any windshield objects.  See the [Protocol for Saving Layouts](mummy-request.html) for how to save these layouts.

@image html vivify_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, vivify ]
i:  { filename: <code>Str</code> <em>filename</em> }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - vivify

**Ingests:**

  - filename: [string]

    The name of the layout to load.
  
### Sample

@include proteins/vivify-request.prot
