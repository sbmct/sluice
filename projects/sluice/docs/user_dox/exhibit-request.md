Request To Load Archived Data Protocol      {#exhibit-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Archive Data

### What does it do?

Use this protein to recall and load a previously saved data archive.  Doing so will clear the current topology and load the topology data and observations from the archived file.  See the [Request To Archive Data Protocol](curate-request.html) for how to save these archives. In addition, the current sluice time will be set to the archive's begin time and will be in the paused state.

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, exhibit ]
i:  { filename: <code>Str</code> <em>filename</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - exhibit

**Ingests:**

  - filename: [string]

    The name to load archive from.  This field is required.

### Sample

@include proteins/exhibit-request.prot
