Data Configuration {#data-config}
=================

[Home](index.html) &rarr; Data Configuration

### What does it do?

Sluice provides a facility for marking entities with states at a point in time.
A single entity may be marked with any number of states at a given time.  This
marking is done using Inculcators.

### Inculcator Protein Format

<pre>
d: [sluice, prot-spec v1.0, inculcators, <add, remove, change>]
i:  { inculcators:
        [
            {
                name: <code>Str</code> <em>inculcator_name</em>,
                kinds:
                    [ 
                        <code>Str</code> <em>entity type 1</em>,
                        <code>Str</code> <em>entity type 2</em>,
                        ...
                    ],
                pigment: <code>Str</code> <em>pigment</em>,
                properties:
                    {
                        <code>Str</code> <em>key 1</em> : <code>Str</code> <em>value 1</em>,
                        <code>Str</code> <em>key 2</em> : <code>Str</code> <em>value 2</em>,
                        ...
                    },
                jstest: <code>Str</code> <em>javascript_test_code</em>
            }
        ]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - inculcators
  - <add, remove, change>

    The final descrip tells Sluice what to do with the supplied inculcators. `add` will make
Sluice add them to the current set of inculcators.  `remove` will remove them from the current set. `change` will replace the current set with the supplied inculcators.

    <pre>Note that as of Sluice's Emu release, changing the working set of inculcators is computationally expensive. It is best to decide upon your working set of inculcators before you start sluice.</pre>

**Ingests**

  - name: [string]

    Mandatory. Each inculcator should have a unique name.

  - kinds: [list of strings]

    Mandatory.  A list of entity kinds that the inculcator matches against. Each inculcator must include at least one kind.  See the [topology protocol](inject-topology.html) for more information on entity kinds.

  - pigment: [string]

    Mandatory.  If an entity is matched by an inculcator, this pigment (state) will be applied to the entity at the time it is matched. The pigment given here should string match the name of a pigment defined in the Network Configuration. See the [Network Configuration Specification](network-fluoro-api.html#pigs) for details on how pigments are defined.

  - properties: [map of string pairs]

    Optional. String pairs to check against an entity's current set of observations. For each pair &lt;<code>K</code>, <code>V</code>&gt;, the entity matches if either

    1. It has an observation with key <code>K</code> and value <code>V</code>
    2. It does not have an observation with key <code>K</code> and <code>V</code> is null

    An entity must match against *all* elements of the properties match in order to be matched by the inculcator.

  - jstest: [string]

    Optional.  A javascript test may be used to match against the entity. This can perform more complex queries that simple type and property matching do not handle. See [below](#js) for details on using javascript to match an entity state to an inculcator.


### Sample

@include proteins/inculcators.prot

<a name="js" />
### Using javascript to match an inculcator

When rendering, if a javascript filter is applied, sluice will perform the javascript equation against every entity in the topology. So you can think of it as so:

@code
for (entity n in topology)
  if (n is one of the kinds for the inculcator &&
      the jstest equates to true)
    render the given pigment at entity n's location
@endcode

There are two built in functions for accessing entities you will want to use to interact with the underlying topology data:
  - kind(n): This function returns the "kind" of entity <em>n</em>.
  - observation(n, 'attr-id', t): This function will return the value of entity <em>n</em> for attribute <em>attr-id</em> at time <em>t</em>