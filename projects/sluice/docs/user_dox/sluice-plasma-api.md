Sluice Plasma API             {#sluice-plasma-api}
==============

[Home](index.html) &rarr; Plasma API

@htmlonly
<embed src="pool_diagram.svg" width="110%" height="500px" target="_top"></embed>
@endhtmlonly

All input/output from sluice goes through the above pool structure.  For each general type of communication, there exists an input and output pool and there is a protocol for each protein message.

### [Heartbeat Communication](heartbeat-api.html)

Heartbeats are used by sluice to inform other processes of it's running as well as various details about the current state sluice is in including time and world view.

Please refer to the [Heartbeat API](heartbeat-api.html) for more information and diagrams for starting all inter-process communication with sluice.

### [Topology Communication](topology-api.html)

These pools are used for injecting and updating the network monitored by sluice.  The request for the network is sent through the sluice-to-topo pool and all network proteins for adding, updating and removing entities in the network are sent through the topo-to-sluice pool.

Please refer to the [Topology Protein API](topology-api.html) for more information

### [Observations Communication](observations-api.html)

Observations are defined as any change to the attributes to entities in the network. This communication is one-way from the client to send frequent updates of network entity attributes.  Any changes to the actual topology of the network entities (location/name/addition/removal) should be made through the Topology API mentioned above.

Please refer to the [Observations Protein API](observations-api.html) for more information

### [Fluoroscope Communication](fluoroscope-api.html)

These pools are used for communicating with fluoroscope instances within sluice. Fluoroscopes can make requests for updates from external processes and external processes can regularly update a known fluoroscope with up-to-date information based on the type of fluoroscope.

Please refer to the [Fluoroscope Protein API](fluoroscope-api.html) for more information

### [Edge Device/External Process Communication](edge-api.html)

These pools are used for much of the general communication between sluice and external processes or devices that want to stay in synch or send various commands to sluice.

Please refer to the [Edge Device Protein API](edge-api.html) for more information

### [Tile Server Communication](tile-api.html)

These pools are used for communication between sluice and the tile server for requesting and retrieving map tiles filled in as the user navigates the map.

Please refer to the [Tile Server Protein API](tile-api.html) for more information

### Web Client Communication

These pools are used for communication between sluice and the web participants using the main tab in the sluice web interface.  It provides mechanisms for full control of sluice.

The documentation for this communication is coming soon.
