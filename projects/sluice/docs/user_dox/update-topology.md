Update Topology Protocol       {#update-topology}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Topology API](topology-api.html) &rarr; Update Topology

### What does it do?

Provides the ability to update existing entities in sluice's topology store with new lat/lon coordinates.

@image html update_topology.svg

### Pool

topo-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, topology, change ]
i:  {
        topology:
            [
                {   
                    id: <code>Str</code> <em>entity_id</em>,
                    kind: <code>Str</code> <em>entity_type</em>,
                    timestamp: <<code>Str</code> <em>timestamp</em>, <code>float64</code> <em>timestamp</em>>,
                    loc: <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>]
                },
                {   
                    id: <code>Str</code> <em>entity_id</em>,
                    kind: <code>Str</code> <em>entity_type</em>,
                    timestamp: <<code>Str</code> <em>timestamp</em>, <code>float64</code> <em>timestamp</em>>,
                    path:
                        [
                            <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                            <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                            <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                            ...
                        ]
                }, ...
            ]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - topology
  - change

**Ingests:**

  - id: [string]

    This string provides a unique identifier for the entity being added. For instance, if your network is licensed drivers, this would be the license number for a driver.

  - loc: [vect] || path: [list of vects]

    Entities can either be objects with a single point location or a path defined by multiple points. Single point entities have a "loc" field which takes a single vector that provides the lat, lon location of the entity being added.  The z value of the location vector is a placeholder for designating elevation, however, elevation is currently not supported by sluice. Multi-point located entities have a "path" field which takes a list of location vectors.

  - kind: [string]

    This field provides the entity's type or class. Using the license drivers example, this might be "motorcycle," "car," etc.

  - timestamp: One of "YYYY-MM-DD HH:MM:SS POSIX_TIME_ZONE" or a float64 timestamp

    Each attribute change is associated with a timestamp so that a user can see when an entity comes online as they scan time. The timestamp maps to machine time. If a float is provided, the number represents seconds elapsed since Jan 1, 1970. If a string is provided, sluice will try to parse it into a date/time based on the time formats defined in the [sluice-settings] (sluice-settings.html) protein read in at run-time.  If no value is given, sluice will assign the current "live" machine time to the change.

### Sample

@include proteins/topo-update.prot
