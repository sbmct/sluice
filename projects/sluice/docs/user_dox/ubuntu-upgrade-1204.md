Upgrading Ubuntu to 12.04     {#ubuntu-upgrade-1204}
=========================================

[Home](index.html) &rarr; [Installation Guides](installation-guides.html) &rarr; Upgrading Ubuntu 12.04

### Requirements

  - An internet connection or a drive with the 12.04 ISO    

### Instructions

- For safety measures in case of an upgrade failure, backup configurations and resource files to an external drive (Note: If the upgrade is successful, this step will have been for naught)
  - Check the following folders for changed configurations/resources
    - ~
    - ~/.oblong
    - /etc/oblong
    - /var/ob/sluice
    - /usr/share/oblong
    - /usr/share/oblong/sluice
    Note: default configurations will get replaced with the sluice upgrades and are not necessary to backup

- If you have an internet connection:
  1. Open a terminal
  2. Run the update manager<br>
    <code>$ sudo update-manager -d</code>
  3. In the popup manager, click the 'Upgrade' button near the top where it says "New Ubuntu release '12.04LTS' is available"
  4. Follow instructions to continue and finish the upgrade.
    - Notes:
      1. When asked about Configuring libc6, click 'Forward'
      2. When asked to remove obsolete packages, click 'Remove'
      3. When asked to restart the system to complete the upgrade, click 'Restart Now'
  5. Install nvidia drivers
    1. Open a terminal
    2. Check the current version<br>
       <code>$ nvidia-smi</code><br>
       \*If this reports a driver version of 295.40, then skip the rest of the driver installation steps
    3. <code>$ sudo apt-get install nvidia-current</code>
    4. Reboot to complete driver installation
  6. Turn off VSync in the nvidia-settings
    1. Open a terminal
    2. Run <code>$ nvidia-settings</code>
    3. In popup dialog, click on the 'OpenGL Settings' Settings in left column
    4. Deselect 'Sync to VBlank' to ensure VBlank settings are off

- If no internet connection:
  1. Follow the directions for installing from a CD/thumbdrive [here](http://linuxmoz.com/upgrade-ubuntu-12-04-cd-dvd/)
  2. Install nvidia drivers
    1. Open a terminal
    2. Check the current version<br>
       <code>$ nvidia-smi</code><br>
       \*If this reports a driver version of 295.40, then skip the rest of the driver installation steps
    3. Download and copy the driver version [295.40](http://www.nvidia.com/object/linux-display-amd64-295.40-driver) to the machine.
    4. Uninstall the current driver<br>
       <code>$ sudo apt-get purge nvidia-current</code>
    5. Kill the window manager (lightdm)<br>
       <code>$ sudo killall lightdm</code>
    6. Install the downloaded driver
      - use Ctrl+Alt+F1 to swap to a console
      - run the NVidia Script<br>
        <code>$ sudo sh NVIDIA-Linux-x86_64-295.40.run</code>
    7. Restart lightdm<br>
       <code>$ sudo lightdm</code>
  3. Turn off VSync in the nvidia-settings
    1. Open a terminal
    2. Run <code>$ nvidia-settings</code>
    3. In popup dialog, click on the 'OpenGL Settings' Settings in left column
    4. Deselect 'Sync to VBlank' to ensure VBlank settings are off
