Workstation Upgrade Guide (Emu 1.18.3)     {#workstation-upgrade-1183}
=========================================

[Home](index.html) &rarr; [Installation Guides](installation-guides.html) &rarr; Workstation Upgrade Guide 1.18.3

### Requirements

- Ubuntu 10.04 with tinywm
- Sluice 1.18 (Emu) or greater
- sluice-1.18.3.deb file
  - This deb will be provided by Oblong via Accellion. If you have not received it, please use your Basecamp project to make a request.

### Instructions

- Copy the debian file on to the machine

  <code>$ scp sluice-1.18.3.deb ~/.</code>

- Make sure sluice and all daemons are stopped

  <code>$ sudo killall sluice<br>
  $ sudo killall ruby<br>
  $ sudo killall java<br>nn
  $ sudo killall tiled</code>

- Backup configurations

  <code>$ mkdir -p bakup/etc<br>
  $ cp /opt/oblong/sluice-64-2/etc/sluice-settings.protein bakup/etc/.</code>

- Install packages
  
  <code>$ sudo dpkg -i sluice-1.18.3.deb</code>

- Re-instantiate configurations

  <code>$ cp bakup/etc/* /opt/oblong/sluice-64-2/etc/.<br>
  $ rm -rf bakup</code>

- Restart sluice

  double click on sluice shortcut on Desktop

  OR

  <code>$ cd /opt/oblong/sluice-64-2/bin<br>
  $ ./desktop-sluice</code>

### Install Paths

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**logs**

  /var/log/oblong/sluice

### Release Notes

[See Here](release-notes-1183.html)