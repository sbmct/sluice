Demo Upgrade Guide (Emu 1.18.3)     {#demo-upgrade-1183}
=========================================

[Home](index.html) &rarr; [Installation Guides](installation-guides.html) &rarr; Demo Upgrade Guide 1.18.3

### Requirements

- Ubuntu 10.04 with tinywm
- Sluice 1.18 (Emu) or greater
- sluice-1.18.3.deb file
  - This deb will be provided by Oblong via Accellion. If you have not received it, please use your Basecamp project to make a request.

### Instructions

- Log in to the demo machine
  
  <code>$ ssh demo@[machine_name]</code>

- Copy the debian file on to the machine

  <code>$ scp sluice-1.18.3.deb ~/.</code>

- Make sure sluice and all daemons are stopped

  <code>$ sudo service ob/sluice/sluice stop</code>

- Backup configurations

  <code>$ mkdir -p bakup/etc<br>
  $ mkdir bakup/bin<br>
  $ cp /opt/oblong/sluice-64-2/etc/sluice-settings.protein bakup/etc/.<br>
  $ cp /opt/oblong/sluice-64-2/etc/cthulhu.protein bakup/etc/.<br>
  $ cp /opt/oblong/sluice-64-2/etc/swapper_settings.yaml bakup/etc/.<br>
  $ cp /opt/oblong/sluice-64-2/bin/ctrl.sh bakup/bin/.</code>

- Install packages
  
  <code>$ sudo dpkg -i sluice-1.18.3.deb</code>

- Re-instantiate configurations

  <code>$ cp bakup/etc/* /opt/oblong/sluice-64-2/etc/.<br>
  $ cp bakup/bin/ctrl.sh /opt/oblong/sluice-64-2/bin/.<br>
  $ rm -rf bakup</code>

- Reboot machine

  <code>$ sudo shutdown -r now</code>

  Sluice should start up with the machine.

### Install Paths

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**logs**

  /var/log/oblong/sluice

### Release Notes

[See Here](release-notes-1183.html)