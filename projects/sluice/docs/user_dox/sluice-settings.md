Sluice Settings       {#sluice-settings}
===================

[Home](index.html) &rarr; [Running Sluice](running-sluice.html) &rarr; Sluice Settings

### What does it do?

The sluice-settings.protein file configures a variety of sluice defaults.

### How is it used?

The sluice-settings.protein file is loaded by sluice by placing a file called "sluice-settings.protein" in a directory in the OB_ETC_PATH. See [File Paths](file-paths.html).

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ app-settings, sluice ]
i:  {
        <a href="#map">map</a>: <code>Str</code> <em>name_of_map_config_file</em>
        <a href="#startup">startup-proteins</a>: <code>Str</code> <em>name of a startup protein</em>
        <a href="#scopes">scopes</a>:
            [
                <code>Str</code> <em>name_of_fluoroscope_config_file_1</em>,
                <code>Str</code> <em>name_of_fluoroscope_config_file_2</em>,
                ...
            ],
        <a href="#timeformats">time-formats</a>:
            [
                <code>Str</code> <em>time_format_1</em>,
                <code>Str</code> <em>time_format_2</em>,
                ...
            ],
        <a href="#wsmax">windshield-max-items</a>: <code>int64</code> <em>maximum_num_of_windshield_items</em>
        <a href="#feldstream">feldstream-path</a>: <code>Str</code> <em>path_to_feldstream</em>
        <a href="#bookmarks">bookmarks</a>: <code>Str</code> <em>name_of_bookmarks_file</em>
        <a href="#topoloadexpiry">topo-load-expiry</a>: <code>float64</code> <em>topo_load_expiry</em>
        <a href="#tokenenabled">token-enabled</a>: <code>bool</code> <em>token_enabled</em>
        <a href="#tokenexpiry">token-expiry</a>: <code>float32</code> <em>token_expiry</em>
        <a href="#datadef">data-definition</a>:
            {
                <a href="#inculcators">inculcators</a>: <code>Str</code> <em>name_of_inculcators_file</em>,
                <a href="#zoomlevels">zoom-levels</a>:
                    [
                        {
                            <a href="#zoomlevelname">name</a>: <code>Str</code> <em>name_of_zoom_level_1</em>
                            <a href="#zoomlevelvisible">visible</a>: <code>float64</code> <em>visible_degrees_for_zoom_level_1</em>,
                            <a href="#zoomlevelgroup">group</a>: <code>bool</code> <em>group_entities_for_zoom_level_1</em>
                            <a href="#zoomlevelgranularity">granularity</a>: <code>float64</code> <em>granularity_for_zoom_level_1</em>
                        },
                        ...
                    ]
            }
    }
</pre>

**Descrips:**

  - app-settings
  - sluice

**Ingests:**

  - <a name="map"></a>map: [string]

    This is the name of the main map fluoroscope configuration file. See the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for details on what this file contains.

  - <a name="startup"></a>startup: [string]

    This is a name of a protein, either absolute or relative to Sulice's <code>etc</code> path, that describes a set of proteins to be deposited in to specific pools after sluice starts.
    Its format is as follows:

<pre>
    descrips:
    - ignored
    ingests:
      pools:
        pool_name_0:
        - file 0
        - file 1
        - ...
        pool_name_1:
        - file 2
        - file 3
        - ...
        ...
</code>

    For each pool, all of the files listed &mdash; again, either absolute or relative to Sluice's <code>etc</code> path &mdash; will be loaded and deposited in to the named pool.

  - <a name="scopes"></a>scopes: [slaw::list (string)]

    This list contains the configuration files for the default fluoroscope templates that will appear in the fluoroscope palette.  See the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for details on configuring a fluoroscope template.

  - <a name="timeformats"></a>time-formats: [slaw::list (string)]

    This list contains additional time string formats that sluice will parse.  Please refer to [strptime(3)](http://linux.die.net/man/3/strptime) for formatting information.  Sluice will always parse "%Y-%m-%d %H:%M:%S" by default.

  - <a name="wsmax"></a>windshield-max-items: [int64]

    This is the maximum number of items that can appear on the windshield.  This field is optional and the default maximum number of items is 101 if omitted.

  - <a name="feldstream"></a>feldstream-path: [string]

    This is the path to the executable sluice uses to stream felds to video.  This field is optional and the default is /opt/oblong/sluice-64-2/bin/feldstream if omitted.

  - <a name="bookmarks"></a>bookmarks: [string]

    This is the name of the bookmarks configuration file.  This field is optional and the default is bookmarks.protein if omitted.

  - <a name="topoloadexpiry"></a>topo-load-expiry: [float64]

    This is the default timeout in seconds to allow between segments when injecting a network topology with numbered segments.  This field is optional and the default is 60 seconds.

  - <a name="tokenenabled"></a>token-enabled: [bool]

    This defines whether to enable the ability to accept and use user tokens in sluice.  This field is optional and the default is true.

  - <a name="tokenexpiry"></a>token-expiry: [float32]

    This is the default expiration time in seconds until a user is automatically logged out.  This field is optional and the default is 3600 seconds (1 hour).

  - <a name="datadef"></a>data-definition: [slaw::map]

    This map defines default inculcators and zoom levels for the data.

      - <a name="inculcators"></a>inculcators: [string]

        This is the name of the [inculcators protein](data-config.html) file.

      - <a name="zoomlevels"></a>zoom-levels: [slaw::list]

        This is a list that defines zoom levels for the purpose of "zoom level disambiguation", which collapses groupings of entities that overlap, displaying a new graphic instead.

          - <a name="zoomlevelname"></a>name: [string]

            This is the name of the zoom level.  This field is required.

          - <a name="zoomlevelvisible"></a>visible: [float64]

            This defines the visible degrees of latitude at which this zoom-level disambiguation will take effect.  For example, 180.0 of visible degrees of latitude defines the top level map, whereas 5.0 degrees defines the visible degrees at the small country level and 0.5 degrees for the visible degrees at a city level.  This field is required.

          - <a name="zoomlevelgroup"></a>group: [bool]

            This defines whether to group entities at this particular zoom level.  This field is optional if omitted or false, the granularity will effectively be set to 0.0.

          - <a name="zoomlevelgranularity"></a>granularity: [float64]

            This defines the amount the latitude and longitude degrees are sub-divided for grouping nearby entities together.  This field is required if group is set to true.  Finer or smaller granularity has higher computing overhead.

### Sample

@include proteins/sluice-settings.prot
