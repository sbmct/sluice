Clear Topology Protocol       {#clear-topology}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Topology API](topology-api.html) &rarr; Clear Topology

### What does it do?

Remove ALL entities from the network. This protein will remove all points and paths from the network storage in sluice. Timestamps are not currently supported, so clearing the network will remove all entities for all history.

@image html update_topology.svg

### Pool

topo-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
  d: [ sluice, prot-spec v1.0, topology, clear ]
  i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - topology
  - clear

**Ingests:**

none

### Sample

@include proteins/topo-clear.prot