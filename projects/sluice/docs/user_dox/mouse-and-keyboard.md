Mouse and Keyboard operation   {#mouse-and-keyboard}
===========================

[Home](index.html) &rarr; Mouse and Keyboard Operation

### Control keys:

- `w` or UP ARROW : Pushback the sluice view
- `s` or DOWN ARROW : End Pushback
- `e` or BACKSPACE : Delete the Fluoroscope that the mouse is currently hovering over
- `d` or RIGHT ARROW : Switch HandiPoint intent to the next mode of moving the map, moving Fluoroscopes and passthrough
- `a` or LEFT ARROW: Switch HandiPoint intent to the previous mode of moving the map, moving Fluoroscopes and passthrough
- `m` : Enable "displacement mode" for the mouse
- `n` : Disable "displacement mode" for the mouse

### Mouse

- *mouse wheel*: Zoom the Fluoroscope that the mouse is currently hovering over.  (Zoom the main map if the mouse is not hovering over a Fluoroscope.)


 
