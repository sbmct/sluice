Texture Fluoroscope Configuration Specification       {#texture-fluoro-api}
===================

[Home](index.html) &rarr; [Fluoroscope Configuration Specification](fluoro-configuration-spec.html) &rarr; Texture Fluoroscope Configuration Specification

### What does it do?

This configuration provides sluice with all the information it needs to display a texture fluoroscope.  This includes information regarding what daemon it should communicate with, what network information (if any) should be sent along with communication to that daemon, at what opacity it should render as well as styling options/settings it should send along to the daemon. Eventually, the styling will be removed as a reserved attribute and will be left completely to the discretion of the daemon to parse and understand with out sluice acting as a middle man. See the [fluoroscope communication pages](fluoroscope-api.html) for how the communication works and the format of those proteins.

### Slaw format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
{
  <a href="#fluoro-name">name</a>: <code>Str</code> <em>unique_texture_fluoroscope_name</em>,
  <a href="#fluoro-icon">icon</a>: <code>Str</code> <em>path_to_icon_used_in_pushback</em>,
  <a href="#fluoro-disp">display-text</a>: <code>Str</code> <em>text_to_display_in_pushback</em>,
  <a href="#fluoro-type">type</a>: texture,
  <a href="#fluoro-daemon">daemon</a>: <code>Str</code> <em>external_process_identifier</em>,
  <a href="#fluoro-data">include-data</a>: <code>bool</code> <em>should_include_network_data</em>,
  <a href="#fluoro-update">update-frequency</a>: <code>float64</code> <em>frequency_of_auto_requests</em>,
  verbose-request: <code>bool</code> <em>send configuration slaw along with texture request</em>
  ignore-movement: <code>bool</code> <em>do not refresh fluoroscopes when the move</em>
  image-is-static: <code>bool</code> <em>the image does not correspond to any particular geography</em>
  disable-notifications: <code>bool</code> <em>Do not show "LOADING" and "UNAVAILABLE" banners</em>
  <a href="#attributes">attributes</a>:
      [
          {
              <a href="#attributes">name</a>: <code>Str</code> <em>unique_attribute_category</em>,
              <a href="#selection-type">selection-type</a>: <inclusive, exclusive>,
              <a href="#contents">contents</a>:
                  [
                      {
                          <a href="#cont-name">name</a>: <code>Str</code> <em>unique_list_entry</em>,
                          <a href="#cont-disp">display-text</a>: <code>Str</code> <em>text_to_be_used_in_web_interface</em>,
                          <a href="#cont-sel">selected</a>: <code>bool</code> <em>is_selected</em>
                      },
                      ...
                  ]
          },
          ...
      ]
}
</pre>

**Slaw::Map Format:**

  - <a name="fluoro-name"></a>name: (string)

    The unique name identifier of this texture fluoroscope template.

  - <a name="fluoro-icon"></a>icon: (string)

    The path to an image icon that will be used to visually identify this fluoroscope when the user pushes back in sluice.

  - <a name="fluoro-disp"></a>display-text: (string)

    The text that is added to the icon defined above and displayed when a user pushes back in sluice.

  - <a name="fluoro-type"></a>type: texture

    This notifies sluice of the type of fluoroscope that needs to be created and must be "texture".

  - <a name="fluoro-daemon"></a>daemon: (string)

    This string uniquely identifies the external process that will communicate with the instances of this type of fluoroscope.  The external process should match against this string to determine when requests are being made of it and not another process.

  - <a name="fluoro-data"></a>include-data: (bool)

    Some fluoroscope types may render textures based on the underlying network that covers the area of the fluoroscope instance.  For instance, heatmaps based on what the user is viewing would need this knowledge.  For these types of fluoroscope, include the "include-data" field with a value of true.  This field is optional and is set to false by default.  When this field is set, there will be more configuration to do in the attributes section of this configuration to set what network information needs to be sent.  See [the attributes section](#attributes) for more details on these configurations.

  - <a name="fluoro-update"></a>update-frequency: (float64)

    An optional field to set up how often a fluoroscope instance of this type should automatically request a new texture.  By default, a fluoroscope will only make a request when it's coverage area changes (it has been moved, scaled or the user has zoomed in/out) but it may be the case that it should also request every so often to stay live. Weather is an example of needing to do this.  The floating point value given here should be in terms of seconds, so a value of 300 would set up the fluoroscope to make a texture request once every 5 minutes.

  - <a name="fluoro-attr"></a>attributes: (slaw::list)

    [See below](#attributes)

<a name="attributes"></a>
### Attributes

**Overview**

  The list of attributes defines the various parts of a fluoroscope that can be configured or changed on the fly from the "fluoroscopes" tab in the web interface.  Each attribute then has a unique name, selection-type, and contents list to define that attribute. Texture fluoroscopes support seven attributes. The "opacity", "texture-wait" and "style" attributes are available for all texture fluoroscopes, while the other attributes are only available to texture fluoroscopes that have set the include-data field to true. See the [Reserved Attributes](#attr-res) for details on these supported attributes.

  NOTE: These attributes will be reworked in an upcoming release in order to allow for more flexibility for the remote processes, such that attributes can be defined as needed without any changes to sluice to support them.  For now only the attribute below are supported.

  - name: (string)

    The unique name associated with the attribute. Some attributes names are reserved in sluice and map to specific meanings.  See the different types of fluoroscopes for type-specific attributes that have been reserved.

  - <a name="selection-type"></a>selection-type: (string)

    This field denotes what form the attribute should take in menu form.  Currently, there are two options "exclusive" and "inclusive".  Exclusive means that one and only one item in the contents list should be selected at any given time. For instance, "opacity" is exclusive because the fluoroscope can't have both 25% and 100% opacity.  Inclusive means that none or multiple items in the contents list can be selected at any given time.  For instance, the attribute may be a filter on what entity types in a network should be displayed... so you can have everything deselected (and show nothing) or you can have a subset or all entities selected.  This field will likely increase in the future to support more selection types, like floating point ranges (opacity would become a floating point range to allow for all values from 0-100, vs the current form of only selecting a few options).

  - <a name="contents"></a>contents: (slaw::list)

    This list contains all the options for this attribute.  For instance, most default fluoroscopes come with four opacity options (25%, 50%, 75%, 100%).  Each list/option entry should have the following:

    - <a name="cont-name"></a>name: (string)

      This field should be regarded more as the value that is mapped to this entry.  While it needs to be a string, oftentimes it is used as a float or integer.  In the case of opacity, the names are typically '0.25', '0.50', '0.75', and '1.0' but as a reserved attribute type, sluice translates these strings into the appropriate floating point value to properly set the fluoroscope's opacity.

    - <a name="cont-disp"></a>display-text: (string)

      The human-readable display version of the name(value).  This value gets used by the fluoroscope tab in the web interface to display the options so that the user doesn't see '0.25',... but rather "25%",...

    - <a name="cont-sel"></a>selected: (bool)

      Denotes whether the item is selected or not.  If false/0, the item is deselected. If true/1, the item is selected.  In the case of an exclusive list, only one item in the contents list should evaluate to true. While in an inclusive list all to none may evaluate to true.

<a name="attr-res"></a>
### Reserved Attributes

**opacity** - Provides a menu-ing option for the user to change the opacity of a fluoroscope instance.

@code
name: opacity
selection-type: exclusive
contents:
- name: '0.25'
  display-text: 25%
  selected: 0
- name: '0.50'
  display-text: 50%
  selected: 0
- name: '0.75'
  display-text: 75%
  selected: 1
- name: '1.00'
  display-text: 100%
  selected: 0
@endcode  

**style** - Provides a menu-ing option to the user that the daemons receive for determining what style of texture should be returned.

@code
name: style
selection-type: inclusive
contents:
- name: vissatellitegrid
  display-text: Visible satelite
  selected: !i64 0
- name: lowaltradarcontours
  display-text: Radar
  selected: !i64 0
- name: qpe1hrgrid
  display-text: 1-hour QPE(Quantitative Precipitation Estimate)
  selected: !i64 0
- name: qpe24hrgrid
  display-text: 24-hour QPE
  selected: !i64 1
@endcode

**texture-wait** - Provides a menu-ing option to the user for setting how long sluice should wait before determining that a texture request has failed.

@code
name: texture-wait
selection-type: exclusive
contents:
- name: '5.0'
  display-text: '5 seconds'
  selected: 0
- name: '10.0'
  display-text: '10 seconds'
  selected: 1
- name: '20.0'
  display-text: '20 seconds'
  selected: 0
@endcode

**all-entities** - Provides a menu-ing option for the user to set whether or not sluice should include all entities in the fluoroscope coverage area when making texture requests.

@code
name: all-entities
selection-type: exclusive
contents:
- name: false
  display-text: 'off'
  selected: 0
- name: true
  display-text: 'on'
  selected: 1
@endcode

**entity-kinds** - Provides a menu-ing option for the user to set which kinds of entities in the fluoroscope coverage area should be included texture requests.

@code
name: entity-kinds
selection-type: inclusive
contents:
- name: Bus
  display-text: 'Bus'
  selected: 1
- name: Highway
  display-text: 'Highway'
  selected: 1
@endcode

**entity-attributes** - Provides a menu-ing option for the user to set which attribute for the entities that are sent in the texture requests should be sent along with the entities.  Currently, this list is exclusive and only one attribute is ever sent with the entities.  This attribute is sent as 'val' in the request protein.  The next release should supply support for sending multiple or all attributes.

@code
name: entity-attributes
selection-type: exclusive
contents:
- name: gas_mileage
  display-text: 'Gas Mileage'
  selected: 1
- name: num_stops
  display-text: 'Number of Stops'
  selected: 0
@endcode

**dotwidth** - This attribute is specific to the heatmap/Analysis daemon and provides a menu-ing option for the user to set how large the median circle or dot size is for the heatmap daemon to render. In the next release, this attribute will become a daemon specific attribute that is not required in sluice.

@code
name: dotwidth
selection-type: exclusive
contents:
- name: '8'
  display-text: '8'
  selected: 0
- name: '64'
  display-text: '64'
  selected: 1
- name: '256'
  display-text: '256'
  selected: 0
@endcode

### Sample

@include proteins/texture-fluoro-config.prot
