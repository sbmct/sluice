Request Tile From Sluice Protocol       {#tile-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Tile Server Communication API](tile-api.html) &rarr; Tile Request

### What does it do?

Sluice uses this protein to get back from the tile server daemon a map tile or image for a geographical region.  Sluice implements the [Pyramid Coordinate System](http://www.maptiler.org/google-maps-coordinates-tile-bounds-projection/) tiling protocol for defining tile bounds. See the [Tile Response Protocol](tile-response.html) for details on what sluice expects in the response. For further documentation on the Tile Server and things like how to pre-cache tile sets, view the [Tile Server documentation](tile-server.html).

@image html tile_api.svg

### Pool

tiles-req

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ tile-request ]
i:  {
        response-pool: <code>Str</code> <em>tile_response_pool_name</em>,
        map-id: <code>Str</code> <em>map_id</em>,
        x: <code>int64</code> <em>x_index</em>,
        y: <code>int64</code> <em>y_index</em>,
        z: <code>int64</code> <em>z_index</em>
    }
</pre>

**Descrips:**

  - tile-request

**Ingests:**

  - response-pool: [string]

    Provides the address or name of the pool where sluice will listen for the response to this request.

  - map-id: [string]

    Identifies which map tile set should be used to generate the tile. By default, Sluice installs with "mapquest" and "toner"

  - x, y, z: [int64]

    Identifies the index of the tile (location and resolution) that is being requested.  This coordinate system is fully described [here](https://developers.google.com/maps/documentation/javascript/v2/overlays#Google_Maps_Coordinates).

### Sample

@include proteins/tile-request.prot
