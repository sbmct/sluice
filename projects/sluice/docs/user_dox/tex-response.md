Texture Response Protocol       {#tex-response}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Fluoroscope API](fluoroscope-api.html) &rarr; Texture Response

### What does it do?

Sends a full png or jpeg image texture to a specific texture fluoroscope instantiation to fill that fluoroscope's bounds. While this protein is generally sent in response to a request, a daemon can send a new texture at any time to update. IMPORTANT: The texture being sent in this image should be generated using [Mercator projection](http://en.wikipedia.org/wiki/Mercator_projection).

@image html tex_response.svg

### Pool

fluoro-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, response, texture, <em>fluoro_qid_slaw</em>, <em>fluoro_type</em> ]
i:  {
        status:
            {
                tort: <code>int64</code> <em>return val</em>,
                msg: <code>Str</code> <em>return message</em>
            },
        type: <code>Str</code> <jpg, png>,
        bl: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        tr: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        px: [ <code>float64</code> <em>width</em>, <code>float64</code> <em>height</em> ],
        bytes: <code>unt8_array</code> <em>image data</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - response
  - texture
  - fluoro_qid_slaw (slaw::cons ("SlawedQID": byte array))

    The qid value (or unique id) of the fluoroscope which made this request.  This slaw value can be unpacked as a cons with a byte array but is generally intended to simply be passed back in the response to ensure the appropriate fluoroscope receives it.

  - fluoro_type (string)

    This value should be replaced with the string that corresponds with the 'name' ingest fluoroscope's configuration.  Each daemon should have a unique name to match against this value. See the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for more details.

**Ingests:**

  - status (slaw::map)

    This map provides information on the success or failure of the texture response

  - tort (int64)

    This value follows the rules of an [ObRetort][1]. A negative value indicates failure while 0 or greater indicates success.  If a failure value is given, sluice will display the value given for "msg" over the fluoroscope.

  - msg (string)

    This string is displayed over the given fluoroscope if a failure value was given in the "tort" field.

  - type (string)

    Gives the type of image that is being sent.  The options are "jpg" or "png".

  - bl, tr (slaw::list of two float64 values)

    The combination of these two list of values describes the bounding rectangle in geospatial (lat/lon) coordinates of the area being sent.

  - px (slaw::list of two float64 values)

    Gives the image size for the texture in pixel width/height.

  - bytes (unt8 array)

    The actual image in byte array form.  If your daemon is a gspeak application you can use the ToSlaw method provided by the PngImageClot or JpegImageClot class.

### Sample

@include proteins/tex_response.prot

[1]: https://platform.oblong.com/dox/g-speak-api-reference/3.0/classoblong_1_1loam_1_1_ob_retort.html "ObRetort"