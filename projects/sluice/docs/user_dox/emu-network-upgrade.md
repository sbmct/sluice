Upgrading Network Fluoroscope Configurations from 1.16 to Emu 1.18   {#emu-network-upgrade}
=================

[Home](index.html) &rarr; [Release Notes Home](release-notes.html) &rarr; [Emu Release Notes](emu-release-notes.html) &rarr; Network Fluoroscope Upgrading

### References

 - [Network Fluoroscope Configuration Specification](network-fluoro-api.html)
 - [Inculcators Specification](data-config.html)

### Changed/Removed Fields

##### descrips:

**new-fluoro:** This has changed to "new-fluoro-template"

##### ingests:

**type:** "in-process" has changed to "network"

**minimum-zoom:**  this section has been removed and can now be set through each pigment's min-zoom field.

**attributes:**

  - **node-series** & **path-series:**
    
    These sections have been replaced with the introduction of pigments and inculcators. Instead of defining an entity type and all possible rendering behaviors, we now define pigments which setup rendering rules (color, icon, size, etc) and inculcators which map a pigment to an entity kind and state.

    With this change, we have augmented the rendering capabilities of each entity.  Formerly, there was only support for two rendering types per entity and only one ruleset could be rendered at once.  Now, you can have an unlimited number of pigments mapped to unlimited entity states and if multiple states are matched, then multiple pigments will be drawn. For example, if you have a trucking network, you may want to have a pigments for a truck to represent the cargo as well a pigments for rendering if the truck is in-transit/unloading/loading/etc. and have both render simultaneously.

    This change also augments the rendering capabilities of paths. Paths are now rendered using icons, so like points, you can set up an icon that is rendered and can animate along the path along with it's thickness and color multiplier.

    The node-series & path-series section in the old format also allowed for setting up what should be displayed when a user hovers the entity ("observations-blacklist" & "observations-labels"). This functionality is now a part of the "hover" field in the network fluoroscope.  In this field, there is a section called "labels" where you can for each entity type set up a human readable form of the type ("display"), attributes to not show ("skip"), and human readable forms of attributes ("rename").

  - **mark-neareast:** This has been changed to "enable-menu" to be more intuitive to what it does, which is turns on/off whether or not the hovering menu appears when you hover an entity in the network.

### New Required Fields

**kinds:** This section sets up all the point and path types that sluice should recognize and attempt to render

**default_shaders:** This section sets up the shaders sluice should use for rendering points and paths.  Unless you really know what you are doing, we recommend you simply put the following in your configurations:

    default_shaders:
      path:
        fragment: ribbons.frag
        vertex: marching_ribbons.vert
      point:
        fragment: network.frag
        vertex: network.vert

**hover:** This section configures all the rules around the pop-up details menu and what text should be displayed within.