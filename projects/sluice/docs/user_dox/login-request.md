Request User Login Protocol       {#login-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html)&rarr; Request Login

### What does it do?

This protein is used to attach credentials/security token to all sluice requests and actions. The idea is that when security is required, one person should log in and take responsibility for all requests and notifications to back end processes.  This token will then be included in all outgoing requests/notifications in sluice (listed below). It is up to the responding external process to allow/deny the requests based on the token passed.  These request include:

  - [Topology Requests](request-topology.html)
  - [Click Notifications](click-psa.html)
  - [Snapshot Notifications](snapshot-psa.html)
  - [Drag/Drop Notifications](skewer-psa.html)
  - [Texture Fluoroscope Requests](tex-request.html)
  - [Video Fluoroscope Requests](vid-request.html)

Sluice also invokes the token as a cookie when making web requests in order to circumvent the login procedure for internal web pages.  The token by default will expire after one hour, but this timeout can be set through the login protocol. You can also log the user out prematurely using the [Logout Request Protocol](logout-request.html).

@image html login_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, login ]
i:  {
        user-id: <code>Str</code> <em>user_id</em>,
        user-name: <code>Str</code> <em>user_name</em>,
        token: <code>Str</code> <em>token</em>,
        expiry: <code>float64</code> <em>time_to_expiration</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - login

**Ingests:**

  - user-id: [string]

    This is a required field and is used as the user's unique identifier.

  - user-name: [string]

    The label that will be displayed in the system area popup and is optional.

  - token: [string]

    The security token to be sent around for validation and for internal web page loading.

  - expiry: [float64]

    Sets the number of seconds until the user is automatically logged out and is an optional field. The default timeout is 3600 seconds (1 hour).  This default can also be custom defined in the [sluice-settings](sluice-settings.html) configuration file read in at run-time.
  
### Sample

@include proteins/login-request.prot
