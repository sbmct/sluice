Static Layers Protein Configuration   {#static-layers}
===================================

[Home](index.html) &rarr; [Fluoroscope Configuration Specification](fluoro-configuration-spec.html) &rarr; Atlas Configuration Specification

### What does it do?

Sets up the list of static map layers, with their file locations and lat/lon boundaries which the [Atlas Configuration](#atlas-fluoro-api.html) configures in order to allow users to place them directly on top of the base map.  This protein can live anywhere and it's path is setup in the Atlas Configuration itself.

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [sluice, config, layers]
i:  {
       <code>Str</code> <em>layer_name_1</em>:
            {
                image_file: <code>Str</code> <em>path_to_image</em>,
                lat_min: <code>float64</code> <em>minimum_latitude</em>,
                lat_max: <code>float64</code> <em>maximum_latitude</em>,
                lon_min: <code>float64</code> <em>minimum_longitude</em>,
                lon_max: <code>float64</code> <em>maximum_longitude</em>
            },
        ...
    }
</pre>

**descrips**

- sluice
- config
- layers

**ingests**

- <em>layer_name</em>: [string]

  Each key entry in the ingest map should uniquely identify the static layer.  This key can then be used in the [Atlas Configuration](#atlas-fluoro-api.html) to set up the layering options.

- image_file: [string]

  This field should provide the path to the image on disk.

- lat_min: [float64]

  The value given in this field should set where the bottom of the image should align latitudinally.

- lat_max: [float64]

  The value given in this field should set where the top of the image should align latitudinally.
  
- lon_min: [float64]

  The value given in this field should set where the left side of the image should align latitudinally.
  
- lon_max: [float64]

  The value given in this field should set where the right side of the image should align latitudinally.
