Reorder Fluoroscopes on Z-Axis Request Protocol       {#reorder-on-zaxis-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Reorder Fluoroscopes on Z-Axis Request

### What does it do?

Use this protein to tell Sluice when Fluoroscopes should be reordered on the Z-axis. (NOTE: There is no specific response protein for this request. The [Fluoroscopes PSA Protocol](fluoro-list-psa.html) is used instead.)

Fluoroscope z order will be set to the order that you submit their QID's,
with the first Fluoroscopes -- identified by QID -- on the bottom, 
and the last Fluoroscope on top.

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, reorder-z-axis ]
i: { scopes : [ { "SlawedQID" : unt8_array[16] } ] }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - reorder-z-axis

**Ingests:**

{ "scopes" : [ { "SlawedQID": unt8_array[16] } ] }

### Sample

@include proteins/reorder-z.prot
