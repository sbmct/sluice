Time Announcement Protocol       {#time-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Time PSA

### What does it do?

The time announcement protein gets sent by sluice whenever it is requested or time is changed.  It includes the current time, the oldest and newest time, the rate of play and if time is paused. See the [Request Time Protocol](time-request.html) for details on requesting this protein.

@image html request_time.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, time ]
i:  {
        current: <code>float64</code> <em>time</em>,
        paused: <code>bool</code> <em>is_paused</em>,
        rate: <code>float64</code> <em>rate of play</em>,
        live: <code>bool</code> <em>is_live</em>,
        min: <code>float64</code> <em>earliest_known_time</em>,
        max: <code>float64</code> <em>most_recent_time</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - time

**Ingests:**

  - current: [float64]

    The current timestamp that the play head is set to in sluice.  This timestamp maps to machine time and the number represents seconds elapsed since Jan 1, 1970.

  - paused: [bool]

    Quickly indicates whether or not sluice's play head is currently paused. If it is paused, the rate should also be 0.0.

  - rate: [float64]

    Gives the rate of play of sluice in terms of seconds per second.  A value of 2.0 would indicate that sluice is playing at the rate of two seconds for every real world second. Negative values are valid and indicate the rewinding of time.

  - live: [bool]

    Quickly indicates if sluice considers its play head as "live," meaning that the current timestamp is the same as the live machine time.  If live is "true," an external process may chose to ignore the "current" value and use its own definition of live to try and stay closer in synch.

  - min: [float64]

    The earliest time known to sluice. This value will map to the earlier timestamp of either the time sluice began or a time associated with a topology entity (entities can date back to Jan 1, 1970).

  - max: [float64]

    Currently, sluice does not support future timestamps, so this value will generally match the live time of sluice.

### Sample

@include proteins/time-psa.prot