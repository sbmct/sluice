Release Notes   {#release-notes}
=============

[Home](index.html) &rarr; Release Notes

### Sluice Releases

[Finch](finch-release-notes.html): Release Notes

[Emu](emu-release-notes.html):
 - [1.18.0](emu-release-notes.html): Release Notes and Upgrade Help
 - [1.18.2](release-notes-1182.html): Release Notes
 - [1.18.3](release-notes-1183.html): Release Notes

[1.16](release-notes-116.html): Release Notes and Documentation Files

