Fluoroscope Configuration Specification       {#fluoro-configuration-spec}
===================

[Home](index.html) &rarr; Fluoroscope Configuration Specification

## Overview

A fluoroscope is the in-process representation of a single data layer rendered in sluice.  There are four fluoroscope categories: The [Atlas (or Map) Fluoroscope](atlas-fluoro-api.html) defines the background map layer and can be configured to use either a static image or a dynamic tile set, the [Network Fluoroscope type](network-fluoro-api.html) defines a layer which renders sluice's stored topology, the [Texture Fluoroscope type](texture-fluoro-api.html) defines a layer which communicates with an external daemon and renders png textures sent by that daemon, and the [Video Fluoroscope type](video-fluoro-api.html) defines a layer which communicates with an external daemon and renders video either from pool or on disc as provided by the daemon.  Each fluoroscope type can be configured to work with your data and render in various ways.  This page, along with the configuration pages for each fluoroscope type will help you build these configurations to fit your own data and branding.

## Slaw Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
{
    name: <code>Str</code> <em>unique_fluoroscope_name</em>,
    icon: <code>Str</code> <em>path_to_icon_used_in_pushback</em>,
    display-text: <code>Str</code> <em>text_to_display_in_pushback</em>,
    type: <network, texture, video>,
    bounds:
      bl: <code>[lat, lon]</code>
      tr: <code>[lat, lon]</code>
    attributes:
        [
            {
                name: <code>Str</code> <em>unique_attribute_category</em>,
                selection-type: <inclusive, exclusive>,
                contents:
                    [
                        {
                            name: <code>Str</code> <em>unique_list_entry</em>,
                            display-text: <code>Str</code> <em>text_to_be_used_in_web_interface</em>,
                            selected: <bool> <em>is_selected</em>
                        },
                        ...
                    ]
            },
            ...
        ]
    callout: # optional
      loc: v3float64 # required
      width: &lt;soft float&gt; # optional
      color: &lt;soft color&gt; # optional
      background: #optional, must be non-null
        width: &lt;soft float&gt; # optional, defaults to the callout width
        color: &lt;soft color&gt; # optional, defaults to the callout color
}
</pre>

**Slaw::Map Format:**

  - name: (string)

    The unique name identifier of this fluoroscope template.

  - icon: (string)

    The path to an image icon that will be used to visually identify this fluoroscope when the user pushes back in sluice.

  - display-text: (string)

    The text that is added to the icon defined above and displayed when a user pushes back in sluice.

  - type: (string)

    The type of fluoroscope this template is defining.  The options for this are:
  
      - atlas - a fluoroscope that will define how to render the background atlas or map.  While an atlas is implemented as a fluoroscope, sluice does not currently support multiple atlases. See the [Atlas Fluoroscope Page](atlas-fluoro-api.html) for more detailed explanation and the full slaw api for defining this type of fluoroscope.
      - network - a fluoroscope that will define how to render the underlying topology stored in sluice. See the [Network Fluoroscope Page](network-fluoro-api.html) for a more detailed explanation and the full slaw api for defining this type of fluoroscope.
      - texture - a fluoroscope that makes requests for textures to an external process to fill in its bounds. See the [Texture Fluoroscope Page](texture-fluoro-api.html) for a more detailed explanation and the full slaw api for defining this type of fluoroscope.
      - video - a fluoroscope that makes requests for video definitions to an external process to play within its bounds. See the [Video Fluoroscope Page](video-fluoro-api.html) for a more detailed explanation and the full slaw api for defining this type of fluoroscope.

  - bounds: (map, optional)

    The bottom left and top right coordinates of the fluoroscope that you'd like to create

<a name="attrs"></a>
  - attributes: (slaw::list)

    The list of attributes defines the various parts of a fluoroscope that can be configured or changed on the fly from the "fluoroscopes" tab in the web interface.  Each attribute then has a unique name, selection-type, and contents list to define that attribute.  See below for details on these fields.

    - name: (string)

      The unique name associated with the attribute. Each fluoroscope type has its own set of attributes which have been reserved and are inherently understood by sluie.  See the three different types of fluoroscopes for details on their reserved attributes.

    - selection-type: (string)

      This field denotes what form the attribute should take in menu form.  Currently, there are two options "exclusive" and "inclusive".  Exclusive means that one and only one item in the contents list should be selected at any given time. For instance, "opacity" is exclusive because the fluoroscope can't have both 25% and 100% opacity.  Inclusive means that none or multiple items in the contents list can be selected at any given time.  For instance, the attribute may be a filter on what entity types in a network should be displayed... so you can have everything deselected (and show nothing) or you can have a subset or all entities selected.  This field will likely increase in the future to support more selection types, like floating point ranges (opacity would become a floating point range to allow for all values from 0-100, vs the current form of only selecting a few options).

    - contents: (slaw::list)

      This list contains all the options for this attribute.  For instance, most default fluoroscopes come with four opacity options (25%, 50%, 75%, 100%).  Each list/option entry should have the following:

      - name: (string)

        This field should be regarded more as the value that is mapped to this entry.  While it needs to be a string, oftentimes it is used as a float or integer.  In the case of opacity, the names are typically '0.25', '0.50', '0.75', and '1.0' but as a reserved attribute type, sluice translates these strings into the appropriate floating point value to properly set the fluoroscope's opacity.

      - display-text: (string)

        The human-readable display version of the name(value).  This value gets used by the fluoroscope tab in the web interface to display the options so that the user doesn't see '0.25',... but rather "25%",...

      - selected: (bool)

        Denotes whether the item is selected or not.  If false/0, the item is deselected. If true/1, the item is selected.  In the case of an exclusive list, only one item in the contents list should evaluate to true. While in an inclusive list all to none may evaluate to true.

### Sample

Navigate to the three types of fluoroscope pages to see examples of each.

<a name="attr-res"></a>
## Reserved Attributes

There are no reserved attributes that are shared by all fluoroscope types.
