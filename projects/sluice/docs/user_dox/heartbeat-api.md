Heartbeat Protocol       {#heartbeat-api}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; Heartbeat Communication API

### What does it do?

The heartbeat protein is a protein that gets deposited once a second to the "sluice-to-heart" pool by sluice. It allows external processes to know if sluice is running as well as keep them updated to the state of sluice. The state includes information about the current time and what zoom location the map is currently set to.

### On boot protocol

@image html on_boot_diagram.svg

The protocol diagramed above shows how an external process should attempt connection to sluice:

  1. If connection to the sluice-to-heart pool fails, then sluice is not running and the machine may even be turned off. Keep trying to connect no more than once a second.
  2. Once a connection to the sluice-to-heart pool succeeds, begin listening for a heartbeat protein. Sluice will deposit a heartbeat about once per second after it begins running. Once a heartbeat is found, all pool connections should succeed and communication can begin.
  3. Continue listening for heartbeats for the duration of running in order to know when or if Sluice has stopped.

### Pool

sluice-to-heart

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, heartbeat ]
i:  {
        location:
            {
                lat: <code>float64</code> <em>latitude</em>,
                lon: <code>float64</code> <em>longitude</em>,
                zoom: <code>int64</code> <em>zoom level</em>
            },
        time:
            {
                current: <code>float64</code> <em>time</em>,
                paused: <code>bool</code> <em>is_paused</em>,
                rate: <code>float64</code> <em>rate of play</em>,
                live: <code>bool</code> <em>is_live</em>
            }
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - heartbeat

**Ingests:**

  - location: [slaw::map]

    This map contains all the relevant information for what area of the world sluice is currently viewing

  - lat: [float64]

    The latitude value at the center of the current viewing area.

  - lon: [float64]

    The longitude value at the center of the current viewing area.

  - zoom: [int64]

    The current level of zoom of the map. This value is logarithmic, where a value of 1 is fully zoomed out, showing the entire world and a value of 500 would be zoomed in to show an area the size of Los Angeles.

  - time: [slaw::map]

    This map contains all the relevant information for what time sluice is currently set to.

  - current: [float64]

    The current timestamp that the play head is set to in sluice.  This timestamp maps to machine time and the number represents seconds elapsed since Jan 1, 1970.

  - paused: [bool]

    Quickly indicates whether or not sluice's play head is currently paused. If it is paused, the rate should also be 0.0.

  - rate: [float64]

    Gives the rate of play of sluice in terms of seconds per second.  A value of 2.0 would indicate that sluice is playing at the rate of two seconds for every real world second. Negative values are valid and indicate the rewinding of time.

  - live: [bool]

    Quickly indicates if sluice considers its play head as "live," meaning that the current timestamp is the same as the live machine time.  If live is "true," an external process may chose to ignore the "current" value and use its own definition of live to try and stay closer in synch.

### Sample

@include proteins/heartbeat.prot