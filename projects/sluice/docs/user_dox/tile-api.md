Tile Server Protocol           {#tile-api}
=============

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; Tile Server Communication API

@htmlonly
<embed src="tile_api.svg" width="100%" height="300px"></embed>
@endhtmlonly

tiles-req proteins
- [Tile Request] (tile-request.html)

<em>tile_response_pool_name</em> proteins
- [Tile Response](tile-response.html)
