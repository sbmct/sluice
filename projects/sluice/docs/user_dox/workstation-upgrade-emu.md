Workstation Upgrade Guide (Emu 1.18)     {#workstation-upgrade-emu}
=========================================

[Home](index.html) &rarr; [Installation Guides](installation-guides.html) &rarr; Workstation Upgrade Guide 1.18

### Requirements

  - Ubuntu 10.04
  - [The latest NVIDIA Linux x86_64 display driver](http://www.nvidia.com/object/unix.html)
  - Google Chrome
  - tarball of g-speak, sluice, and dependency packages provided via [Basecamp](https://basecamp.com/)

    Folder should include:
      - 166 base debian packages
      - 3 sluice packages
        - sluice-web-1.6.0.deb
        - sluice-kipple-1.18.deb
        - sluice-1.18.0.deb
      - deploySluice.sh script

### Instructions

- Copy the packages provided via Basecamp on to the machine and untar

  <code>$ scp sluice-1.18-pkgs.tgz ~/.<br>
  $ tar -xzvf sluice-1.18-pkgs.tgz</code>

- Install packages
  
  1. <code>$ cd sluice-1.18-pkgs</code>
  2. run script
    1. <code>$ ./deploySluice.sh</code>
    2. follow script instructions

- Run through the demo

  For the latest keyboard/mouse controls, [click here](mouse-and-keyboard.html). Run through your demo as usual to find any missing resources or configurations.  These are backed up in the `sluice-1.8-pkgs/quarantine` folder.  Move any configuration files you need into the `/home/[username]/.oblong/etc` folder (you may have to create this folder) and move any resource files you need into the `/home/[username]/.oblong/share` folder, keeping the relative paths.  So, if you previously had a resource file tmp.png in `/kipple/sluice/symbols/tmp.png`, this should go in `/home/[username]/.oblong/share/sluice/symbols/tmp.png`.

### Path Locations of Install

**g-speak libraries**

  /usr/lib

**g-speak executables**

  /usr/bin

**ob-http-ctl**

  /opt/oblong/jsplasma

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**sluice web**

  /opt/oblong/sluice-web

**resource files**

  /usr/share/oblong/sluice

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**screen and feld proteins**

  /usr/share/oblong

**logs**

  /home/[username]/.sluice-logs

### Release Notes

[See Here](emu-release-notes.html)