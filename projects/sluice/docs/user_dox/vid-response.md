Video Response Protocol       {#vid-response}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Fluoroscope API](fluoroscope-api.html) &rarr; Video Response

### What does it do?

Sends the location for a video to be played within the specified fluoroscope.  This protein can specify whether the video is attached to the specified lat/lon bounding area and should not move with the fluoroscope or if it is a floating video that should fill and move with the fluoroscope.

@image html vid_response.svg

### Pool

fluoro-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, response, video, <em>fluoro_qid_slaw</em>, <em>fluoro_type</em> ]
i:  {
        status:
            {
                tort: <code>int64</code> <em>return val</em>,
                msg: <code>Str</code> <em>return message</em>
            },
        bl: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        tr: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        stay_put: <code>bool</code> <em>should_pin_to_map</em>,
        vid_loc:
            {
                type: <code>Str</code> pool,
                path: <code>Str</code> <em>filename</em>,
                enable-audio: <code>bool</code> <em>should_enable</em>,
                preroll: <code>float64</code> <-1.0, 0.0>
            }

      OR

      vid_loc:
          {
              type: <code>Str</code> file,
              pool-name: <code>Str</code> <em>pool_name</em>,
              enable-audio: <code>bool</code> <em>should_enable</em>,
              is-live: <code>bool</code> <em>is_live</em>
          }
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - response
  - texture
  - fluoro_qid_slaw (slaw::cons ("SlawedQID": byte array))

    The qid value (or unique id) of the fluoroscope which made this request.  This slaw value can be unpacked as a cons with a byte array but is generally intended to simply be passed back in the response to ensure the appropriate fluoroscope receives it.

  - fluoro_type (string)

    This value should be replaced with the string that corresponds with the 'name' ingest fluoroscope's configuration.  Each daemon should have a unique name to match against this value. See the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for more details.

**Ingests:**

  - status (slaw::map)

    This map provides information on the success or failure of the texture response

  - tort (int64)

    This value follows the rules of an [ObRetort][1]. A negative value indicates failure while 0 or greater indicates success.  If a failure value is given, sluice will display the value given for "msg" over the fluoroscope.

  - msg (string)

    This string is displayed over the given fluoroscope if a failure value was given in the "tort" field.

  - bl, tr (slaw::list of two float64 values)

    The combination of these two list of values describes the bounding rectangle in geospatial (lat/lon) coordinates where the video should fill.

  - stay_put (bool)

    This value indicates whether the video is specifically attached to the lat/lon region given by bl, tr and should not move or scale with the fluoroscope or if that is merely where it should first be placed and then should move and scale with the fluoroscope

  - vid_loc (slaw::map)

    This map defines the type of video to be played and the settings required for that video.

  - type (string)

    Gives the type of video that is being specified.  The options are "pool" or "file".

  - pool-name (string)

    The pool name identifier for where the video can be read from.  This is used only if type is "pool".

  - path (string)

    The full path to where the video file is on disk. This is used only if type is "file".

  - enable-audio (bool)

    This boolean value turns audio on/off.  Currently, sluice does not support audio so this has no end effect.

  - preroll (float64)

    Preroll is a float64 that specifies how long to synchronously wait for the first frame. For enpooled video, sluice forces this value to always be 0.0.

  - is-live (bool)

    is-live is mostly relevant when reading from pools and says whether the pool should be read from in live mode, where the most recent frame is displayed.

### Sample

@include proteins/vid_response.prot

[1]: https://platform.oblong.com/dox/g-speak-api-reference/3.0/classoblong_1_1loam_1_1_ob_retort.html "ObRetort"