Glossary of Terms   {#glossary}
=================

[A](#A) [B](#B) [D](#D) [E](#E) [F](#F) [H](#H) [I](#I) [M](#M) [O](#O) [P](#P) [Q](#Q) [S](#S) [T](#T) [W](#W) [V](#V) [Z](#Z)

<a name="A" />
### A

<a name="atlas" />
**atlas**: A programmatic term that is interchangeable with the [map](#map) and is used in the plasma api.

<a name="attribute" />
**attribute**: an overloaded word that can reference one of two things: 1. meta information associated with an [entity](#entity) 2. meta information that is associated with a [fluoroscope](#fluoroscope) and is user-configurable

<a name="B" />
### B

<a name="bookmark" />
**bookmark**: A saved geographical location on the [map](#map), including the latitude, longitude and zoom level.

<a name="D" />
### D

<a name="daemon" />
**daemon**: "In Unix and other multitasking computer operating systems, a daemon ( /ˈdeɪmən/ or /ˈdiːmən/) is a computer program that runs as a background process, rather than being under the direct control of an interactive user." [ <a href="http://en.wikipedia.org/wiki/Daemon_(computing)"> 1 </a>] In sluice, this process may live on the same or a separate machine and generally listens and responds to sluice's plasma api.

<a name="descrips" />
**descrips**: a list of strings that describe the protein.

<a name="E" />
### E

<a name="edge-device" />
**edge device**: any laptop, ios, android, web app or external application to [sluice](#sluice) that listens to user actions and reacts and/or sends commands to control the sluice.

<a name="entity" />
**entity**: any geolocated data part of the [topology](#topology) stored in sluice, which can be a [path](#path) or a [point](#point)

<a name="F" />
### F

<a name="fluoroscope" />
**fluoroscope**: 1. a layer that sits on top of the map and provides a view of a specific set of data 2. the background map itself is also a fluoroscope providing the geographical context

<a name="fluoro-instance" />
**fluoroscope instance**: a fluoroscope that has been instantiated and placed on the [map plane](#map-plane) or [windshield](#map-windshield)

<a name="fluoro-template" />
**fluoroscope template**: a configuration that defines the look and setup of a data lense from which [fluoroscope instances](#fluoro-instance) are created

<a name="fluoro-bin" />
**fluoroscope template bin**: the area above the [map](#map) where [fluoroscope templates](#fluoro-template) are accessed by the user.

<a name="H" />
### H

<a name="handipoint" />
**handipoint**: When using an input device, an icon appears on the sluice screen that indicates where the user is pointing; this icon is known as a "handipoint"

<a name="I" />
### I

<a name="ingests" />
**ingests**: key/value pairs, where the `key` is a string -- ingests are collections of [slawx](#slaw) that live inside proteins and carry the brunt of the information or payload.

<a name="M" />
### M

<a name="map" />
**map**: The 3x1 viewing area of sluice which encompasses the underlying map or map tiles and on which [fluoroscopes](#fluoroscope) can be placed.

<a name="map-plane" />
**map plane**: The viewing area of sluice which directly above and attached to the [map](#map).  [Fluoroscopes](#fluoroscope) placed in this area will pan and zoom along with the map.

<a name="map-windshield" />
**map windshield**: The viewing area of sluice which is directly above but not attached to the [map](#map). [Fluoroscopes](#fluoroscope) placed in this area will remain in place, covering the same portion of the screen area as the map is panned and zoomed.

<a name="O" />
### O

<a name="observation" />
**observation**: meta data associated with an [entity](#entity) and timestamp that can be updated real-time

<a name="P" />
### P

<a name="path" />
**path**: an entity that has multiple geo locations, such as a road.

<a name="plasma" />
**plasma**: the g-speak library that enables cross-platform/cross-process communication through the use of [pools](#pool) and [proteins](#protein)

<a name="point" />
**point**: an entity that has a single geo location, such as a building.

<a name="pool" />
**pool**: A container which stores [proteins](#protein) and communicates them among multiple processes.

<a name="protein" />
**protein**: An immutable container for [descrips](#descrips) and [ingests](#ingests), represented as [slawx](#slaw). Proteins are used for many purposes, including all messaging in and out of sluice; they are a type of slaw.

<a name="psa" />
**psa**: A Public Service Announcement; This is a protein that is sent to a pool for any listening processes' benefit and is not intended for any particular process.

<a name="pushback" />
**pushback**: the action a user takes when holding the [wand](#wand) vertical, pressing the button and pushing the wand away from their body to reveal more information outside the [map](#map)

<a name="Q" />
### Q

<a name="qid" />
**qid**: A unique id associated with all g-speak elements and commonly used to identify [fluorocope](#fluoroscope) instances.

<a name="S" />
### S

<a name="slaw" />
**slaw**: An Oblong-specific binary format for serializing data structures. A Slaw is the lowest level data type in g-speak; it's a basic object.  Note: the plural of "slaw" is "slawx".

<a name="sluice" />
**sluice**: Sluice is a collaborative tool and visualization platform that aligns data in time and space. Sluice provides users with data aggregation, knowledge discovery, and a comprehensive operating picture.  Sluice brings together next-generation end-to-end visualization, existing software applications, massive data sets, & effective real-time command for multiple simultaneous users across multiple screens & devices.

<a name="T" />
### T

<a name="topology" />
**topology**: the full set of data stored in sluice

<a name="W" />
### W

<a name="wand" />
**wand**: A spatially aware input device that provides the primary means for a user to navigate sluice in three modes: reachthrough, move & scale, and pan & zoom.

<a name="windshield" />
**windshield**: the area on screen described as "always on top." Items place in this area such as videos and web pages remain relatively sized and located even when a user [pushes back](#pushback).

<a name="V" />
### V

<a name="video-bin" />
**video bin**: the area below the [map](#map) visible only on [pushback](#pushback) where the user can see thumbnails of live video assets that can be dragged to the [windshield](#windshield)

<a name="Z" />
### Z

<a name="zoom-level" />
**zoom level**: A value from 1 to infinity which is used to set the view of the [map](#map). This value is logarithmic, where a value of 1 is fully zoomed out, showing the entire world, a value of 500 would be zoomed in to show an area the size of Los Angeles, and a value of 132,000 would be zoomed in to show 1600x300 meters. IMPORTANT: This logarithmic approach is something we will likely change in an upcoming releases to be more intuitive.