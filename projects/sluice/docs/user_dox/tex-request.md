Request Texture Protocol       {#tex-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Fluoroscope API](fluoroscope-api.html) &rarr; Request For Texture

### What does it do?

Makes a request to the appropriate texture daemon for an updated texture.  There are three ways this request gets generated:

1. When a new texture fluoroscope is instantiated.
2. As the user zooms in/out and navigates the map and the texture fluoroscope remains in view, the request is made for new geographical areas to ensure higher/lower resolution images of the area are filled in.
3. Fluoroscopes such as weather, may have a configuration set to ensure that a new request is generated at a certain rate in order to keep the texture updated and in synch with the current time of sluice.

@image html tex_request.svg

### Pool

sluice-to-fluoro

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, texture, <em>fluoro_qid_slaw</em>, <em>fluoro_type</em> ]
i:  {
        bl: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        tr: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        style: <code>Str</code> <em>style</em>,
        px: [ <code>float64</code> <em>width</em>, <code>float64</code> <em>height</em> ],
        name: <code>Str</code> <em>name</em>,
        time: <code>float64</code> <em>current time</em>,
        live: <code>bool</code> <em>is_live</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - texture
  - fluoro_qid_slaw (slaw::cons ("SlawedQID": byte array))

    The qid value (or unique id) of the fluoroscope which made this request.  This slaw value can be unpacked as a cons with a byte array but is generally intended to simply be passed back in the response to ensure the appropriate fluoroscope receives it.

  - fluoro_type (string)

    This value should be replaced with the string that corresponds with the 'daemon' ingest fluoroscope's configuration.  Each daemon should have a unique name to match against this value. See the [Texture Fluoroscope Configuration Protein Spec](texture-fluoro-api.html) for more details.

**Ingests:**

  - bl, tr (slaw::list of two float64 values)

    The combination of these two list of values describes the bounding rectangle in geospatial (lat/lon) coordinates of the area being requested.

  - style (string)

    Describes any daemon specific characteristics should be applied to the texture.  The style strings are defined in the corresponding daemon's configuration.  For instance, the weather daemon has styles which define what type of weather to show: IR Satellite, precipitation, temperature, etc. See the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for more details.

  - px (slaw::list of two float64 values)

    Suggests an image size for the returned texture in pixel width/height.  This suggestion can be ignored but may be found useful.

  - name (string)

    Provides the name of the fluoroscope instance and is in general unused.

  - time (float64)

    The current timestamp that the play head is set to in sluice.  This timestamp maps to machine time and the number represents seconds elapsed since Jan 1, 1970.

  - live (bool)

    Quickly indicates if sluice considers its play head as "live," meaning that the current timestamp is the same as the live machine time.  If live is "true," the daemon may chose to ignore the "current" value and use its own definition of live to try and stay closer in synch.

### Sample

@include proteins/tex_request.prot