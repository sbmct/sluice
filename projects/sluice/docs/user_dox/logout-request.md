Request User Logout Protocol       {#logout-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Logout

### What does it do?

This protein is used to remove the current credentials/security token from sluice and log out the current user. The token will be completely removed and no longer attached to any requests or used as a web cookie. See the [Login Request Protocol](login-request.html) for more information on logging in and the [Credential Request Protocol](credential-request.html) for more information on retrieving the currently logged in user credentials.

@image html logout_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, logout ]
i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - logout

**Ingests:**

none
  
### Sample

@include proteins/logout-request.prot
