Create a Web Page Instance Protocol       {#web-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Load Web Page

### What does it do?

This protein is used to request a new web page be added or an existing page be updated in sluice. Sluice uses an open source project called [Berkelium](http://berkelium.org/) to embed Chromium pages into sluice. You can either have a web page act as a fluoroscope and be attached to the map view or you can specify a location for it to live on the windshield like a video.  This protocol also allows you to add multiple web pages to the windshield at once. See [this page](remove-web-instance.html) for how to programmatically remove web pages.

@image html web_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
<b>a) display on map plane (like a fluoroscope)</b>
d: [ sluice, prot-spec v1.0, request, web ]
i:  {
        id: <code>Str</code> <em>entity_id</em>,
        url: <code>Str</code> <em>url</em>
        size: <code>v2float64</code> [<em>percent_width</em>, <em>percent_height</em>],
        auto-resize: <code>bool</code> <em>is_autoresizable</em>
    }

OR

<b>b) display on windshield (like a video)</b>
d: [ sluice, prot-spec v1.0, request, web ]
i:  {
        windshield:
            [
                {
                    name: <code>Str</code> <em>name_of_page</em>,
                    url <code>Str</code> <em>url</em>,
                    feld: <code>Str</code> <left, right, main>,
                    loc: <code>v2float64</code> [<em>up</em>, <em>over</em>],
                    size: <code>v2float64</code> [<em>percent_width</em>, <em>percent_height</em>],
                    visible: <code>bool</code> <em>is_visible</em>,
                    auto-resize: <code>bool</code> <em>is_autoresizable</em>
                    movejs: <code>"javascript(BLLAT, BLLON, TRLAT, TRLON);"</code>
                    initjs: <code>"javascript(BLLAT, BLLON, TRLAT, TRLON);"</code>
                },
                ...
            ]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - web

**Ingests:**

  - id: (string)

    The unique id of the entity that the web page should appear next to.  It should pop up to the right of the entity.

  - name: (string)

    The unique name of the web page to be displayed on the windshield.  If a web page with this name already exists, it will simply update the page rather than create a new one.

  - url: (string)

    The url address for where the page should navigate to.

  - feld: (string)

    The name of the visifeld the web page should appear on.  The options are left, right, or main.  If this field is omitted, it will default to main.

  - loc: (v2float64)

    The relative location on the feld for where the page should appear on the given feld. The values for x and y range from -0.5 to 0.5 and defines the location of the center of the browser.  Thus, (0, 0) indicates that it should be positioned in the center of the designated screen and (0.5, 0.5) would put the center of the browser at the top, right of the screen. This field is optional and if omitted, it will default to the (0, 0).

  - size: (v2float64)

    The size the browser should appear at relative to the size of the designated feld, where (1.0, 1.0) would make it the exact size of the feld and (0.5, 0.5) would make it exactly half the width/height of the feld. This field is optional and if omitted, it will default to (0.5, 0.5).

  - visible: (bool)

    This allows you to hide/show a browser without removing it.  This field is optional and if omitted, it will default to true.

  - auto-resize: (bool)

    This controls whether or not the internal browser size should be resized along with the size of the container.  This field is optional and if omitted, it will default to true.  If it is set to false, then the browser size will be set to the size defined in the size ingest field and the texture will be scaled rather than resized.

  - initjs: (string)

    If this is set, WebThing will run the function you provide in the browser as soon as it opens.  Before it does so it will perform the following string substitutions:

    * BLLAT -> the latitude of the bottom left corner of the browser
    * BLLON -> the longitude of the bottom left corner of the browser
    * TRLAT -> the latitude of the top right corner of the browser
    * TRLON -> the longitude of the top right corner of the browser

  - movejs: (string)
  
    If this is set, WebThing will run the function you provide in the browser every time its cartographic coordinates chage.  (If you move or resize the fluoroscope, or for windshield fluoroscopes if the map behind it is moved.)  Before it does so it will perform the following string substitutions:

    * BLLAT -> the latitude of the bottom left corner of the browser
    * BLLON -> the longitude of the bottom left corner of the browser
    * TRLAT -> the latitude of the top right corner of the browser
    * TRLON -> the longitude of the top right corner of the browser


    

### Sample

@include proteins/web-request.prot
