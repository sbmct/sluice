New Bookmark Error Protocol       {#bookmark-error}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Bookmark Error Response

### What does it do?

This protein gets returned when a request for adding a new bookmark to sluice fails.  This can occur for two reasons: the request protein was malformed, or the bookmark already exists. See the [New Bookmark Request Protocol](new-bookmark-request.html) for more detail.

@image html new_bookmark_error.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, response, new-bookmark ]
i:  {
        name: <code>Str</code> <em>bookmark_name</em>,
        uid: <code>Str</code> <em>unique_id_of_request</em>,
        summary: <code>Str</code> <em>error_summary</em>,
        description: <code>Str</code> <em>further_error_description</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - response
  - new-bookmark

**Ingests:**

  - name: [string]

    The name the new bookmark should be saved as

  - uid: [string]

    The unique identifier for the original request so that it can be mapped to the request.

  - summary: [string]

    A short summary about why the error occurred.

  - description: [string]

    A longer description of why the error occurred.

### Sample

@include proteins/new-bookmark-error.prot
