Demo Upgrade Guide (Emu 1.20)     {#demo-upgrade-finch}
=========================================

[Home](index.html) &rarr; [Installation Guides](installation-guides.html) &rarr; Demo Upgrade Guide 1.20

### Requirements

- Ubuntu 12.04 with tinywm
- tarball of g-speak, sluice, and dependency packages provided via [Basecamp](https://basecamp.com/)

  Folder should include:
    - 248 base debian packages
    - 5 ruby gems
    - 3 sluice packages
      - sluice-web-1.8.0.deb
      - sluice-kipple-1.20.0.deb
      - sluice-1.20.0.deb
    - deploySluice.sh script

### Instructions

- Log in to the demo machine
  
  <code>$ ssh demo@[machine_name]</code>

- Upgrade the demo machine to Ubuntu 12.04

  See [this page](ubuntu-upgrade-1204.html) for OS upgrade procedures

- Copy the packages provided via Basecamp on to the machine and untar

  <code>$ scp sluice-1.20-pkgs.tgz ~/.<br>
  $ tar -xzvf sluice-1.20-pkgs.tgz</code>

- Install packages
  
  1. <code>$ cd sluice-1.20-pkgs</code>
  2. run script
    1. <code>$ ./deploySluice.sh</code>
    2. follow script instructions

- Setup wands
	1. Update sluice configuration to use the wands pool on the machine where the pipeline is running
  	- edit `/opt/oblong/sluice-64-2/etc/sluice-settings.protein` and insert the following line before the "map" field (line 8) with appropriate spacing:

      <code>wands-pool: tcp://[perc-appliance-ip/hostname]/wands</code>

- Setup matrix switch swapping
  1. Copy your backed up swapper_settings.protein file into /opt/oblong/sluice-64-2/etc/

- reboot machine

  <code>$ sudo shutdown -r now</code>

  Sluice should start up with the machine. Run through your demo as usual to find any missing resources or configurations.  Replace any missing/overwritten configurations with your backups.

### Install Paths

**g-speak libraries**

  /opt/oblong/g-speak3.4

**g-speak executables**

  /opt/oblong/g-speak3.4/bin

**ob-http-ctl**

  /opt/oblong/jsplasma

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**sluice web**

  /opt/oblong/sluice-web

**resource files**

  /usr/share/oblong/sluice

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**screen and feld proteins**

  /usr/share/oblong

**logs**

  /var/log/oblong/sluice

### Release Notes

[See Here](finch-release-notes.html)