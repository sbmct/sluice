Network Fluoroscope Configuration Specification       {#network-fluoro-api}
===================

[Home](index.html) &rarr; [Fluoroscope Configuration Specification](fluoro-configuration-spec.html) &rarr; Network Fluoroscope Configuration Specification

### What does it do?

This configuration provides sluice with all the information it needs to display a network topology.  This configuration sets up what entity kinds should be rendered, the rendering rulesets (ie. pigments) it will be using, how the pop-up hover menu should display the meta data of the various entity kinds as well as at what opacity the layer should be rendered. This configuration is paired up with a data configuration (ie. inculcators) protein which separately defines the mapping between the pigments provided in this configuration and specific entity states to inform sluice on when to render each pigment. See the [Data Configuration Page](data-config.html) for more information on the inculcators protein. Also, view the [Topology Communication Page](topology-api.html) and [Observations Communication Page](observations-api.html) for information on inputting and updating sluice's internally stored network topology.

### Slaw format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
{
  <a href="#fluoro-name">name</a>: <code>Str</code> <em>unique_network_fluoroscope_name</em>,
  <a href="#fluoro-icon">icon</a>: <code>Str</code> <em>path_to_icon_used_in_pushback</em>,
  <a href="#fluoro-disp">display-text</a>: <code>Str</code> <em>text_to_display_in_pushback</em>,
  <a href="#fluoro-type">type</a>: network,
  <a href="#attributes">attributes</a>:
      [
          {
              <a href="#attributes">name</a>: <code>Str</code> <em>unique_attribute_category</em>,
              <a href="#selection-type">selection-type</a>: <inclusive, exclusive>,
              <a href="#contents">contents</a>:
                  [
                      {
                          <a href="#cont-name">name</a>: <code>Str</code> <em>unique_list_entry</em>,
                          <a href="#cont-disp">display-text</a>: <code>Str</code> <em>text_to_be_used_in_web_interface</em>,
                          <a href="#cont-sel">selected</a>: <code>bool</code> <em>is_selected</em>
                      },
                      ...
                  ]
          },
          ...
      ],
  <a href="#kinds">kinds</a>:
      [
          <code>Str</code> <em>entity_type_1</em>,
          <code>Str</code> <em>entity_type_2</em>,
          ...
      ],
  <a href="#shaders">default_shaders</a>:
      {
          <a href="#sh-p">point</a>:
              {
                  <a href="#sh-frag">fragment</a>: <code>Str</code> <em>name_of_fragment_shader_file</em>,
                  <a href="#sh-vert">vertex</a>: <code>Str</code> <em>name_of_vertex_shader_file</em>
              },
          <a href="#sh-p">path</a>:
              {
                  <a href="#sh-frag">fragment</a>: <code>Str</code> <em>name_of_fragment_shader_file</em>,
                  <a href="#sh-vert">vertex</a>: <code>Str</code> <em>name_of_vertex_shader_file</em>
              }
      }
  <a href="#pigs">pigments</a>:
      [
          {
              <a href="#pig-name">name</a>: <code>Str</code> <em>pigment_name</em>,
              <a href="#pig-type">type</a>: <point, path>,
              <a href="#pig-icon">icon</a>: <code>Str</code> <em>path_to_icon_to_draw</em>,
              <a href="#pig-size">size</a>:
                  { <a href="#size-def">*See the section on sizes for the api of this map</a> },
              <a href="#pig-col">color</a>:
                  { <a href="#color-def">*See the section on colors for the api of this map</a> },
              <a href="#pig-zoom">min-zoom</a>: <code>float64</code> <em>zoom_level</em>
          }
      ],
  <a href="#hover">hover</a>:
      {
          <a href="#hov-max">max-found</a>: <code>int64</code> <em>max_number_entities_for_hover</em>,
          <a href="#hov-zoom">min-zoom</a>: <code>float64</code> <em>zoom_leve_where_hover_kicks_in</em>,
          <a href="#hov-glyph">glyph</a>: <code>Str</code> <em>path_to_icon_used_for_hover</em>,
          <a href="#hov-labels">labels</a>:
              [
                  { 
                      <a href="#lab-kind">kind</a>: <code>Str</code> <em>entity_kind</em>,
                      <a href="#lab-disp">display</a>: <code>Str</code> <em>display_name_for_type</em>,
                      <a href="#lab-skip">skip</a>:
                          [
                              <code>Str</code> <em>attribute_id_1</em>,
                              <code>Str</code> <em>attribute_id_2</em>,
                              ...
                          ],
                      <a href="#lab-rename">rename</a>:
                          {
                              <code>Str</code> <em>attribute_id_1</em>: <code>Str</code> <em>display_text_for_attribute</em>,
                              <code>Str</code> <em>attribute_id_2</em>: <code>Str</code> <em>display_text_for_attribute</em>,
                              ...
                          }
                  },
                  ...
              ]
      }
}
</pre>

**Slaw::Map Format:**

  - <a name="fluoro-name"></a>name: (string)

    The unique name identifier of this network fluoroscope template.

  - <a name="fluoro-icon"></a>icon: (string)

    The path to an image icon that will be used to visually identify this fluoroscope when the user pushes back in sluice.

  - <a name="fluoro-disp"></a>display-text: (string)

    The text that is added to the icon defined above and displayed when a user pushes back in sluice.

  - <a name="fluoro-type"></a>type: network

    This notifies sluice of the type of fluoroscope that needs to be created and must be "network".

  - <a name="fluoro-attr"></a>attributes: (slaw::list)

    The list of attributes defines the various parts of a fluoroscope that can be configured or changed on the fly from the "fluoroscopes" tab in the web interface.  This includes things like "opacity", which is the one attribute that all fluoroscopes should have built in.  Each attribute then has a unique name, selection-type, and contents list to define that attribute.  Network fluoroscopes have the following additional attributes:
  
    - "enable-menu": This attribute allows the user to turn on/off the text menu that pops up when a user hovers over an entity.

    - "enabled-kinds": This attribute allows the user to filter (hide/show) on the fly the types of entities that are displayed.

    [See below](#attributes) for details on formatting these fields.

  - <a name="kinds"></a>kinds: (slaw::list)

    This field lists out all the entity types this network may be rendering.  The string values in this list should match the "kind" values given when [injecting the topology](inject-topology.html).

  - <a name="shaders"></a>default_shaders: (slaw::map)

    All network entities are currently drawn using glsl shaders. This map defines which shaders should be used by default for points and paths in the network. For each network type (path/point) we must provide both a fragment and vertex shader to use. You can read more about glsl [here](http://www.lighthouse3d.com/opengl/glsl/).

    - <a name="sh-p"></a><point, path>: (slaw::map)

      Define the default shaders for network paths.

      - <a name="sh-frag"></a>fragment: (string)

        Gives the filename of the fragment shader.  Shaders should be placed in the folder /etc/oblong/glsl.

      - <a name="sh-vert"></a> vertex: (string)

        Gives the filename of the vertex shader.  Shaders should be placed in the folder /etc/oblong/glsl.

  - <a name="pigs"></a>pigments: (slaw::list)

    In sluice, we refer to pigments as a named rule-set for how to draw a network entity. Each pigment sets up a single complete definition for what icon to use, at what size, with what color applied and for which type of network entity it should be used for (in terms of if it's for paths or points).  Separately, you will define the rules around *when* to use these pigments in the [inculcators protein](data-config.html).

      - <a name="pig-name"></a>name: (string)

        A unique identifying name for the pigment, such as default_bus_pigment or under_construction_hwy.

      - <a name="pig-type"></a>type: <point, path>

        This sets up whether this pigment is defining the drawing of a path or a point and thus, which default shader defined above to use if a shader is not specified in this rule set.

      - <a name="pig-icon"></a>icon: (string)

        Gives the file path to the icon to be used with this pigment.

      - <a name="pig-size"></a>size: (slaw::map)

        Sets the size at which the entity should be rendered.  This value ...  If this field is not provided, the size will default to 1.  The size can be set as a constant or a softly changing value. [See below for the full details on defining a size](#size-def).

      - <a name="pig-col"></a>color: (slaw::map)

        Sets the adjustment color of the entity.  This field is optional and if not given defaults to (1, 1, 1, 1).  The adjustment color is then multipled against the pixels of pigment.  So if you're pigment is white and you give a color or red, the end result will be red (1, 1, 1, 1) x (1, 0, 0, 1).  The color can be set as a constant or a softly changing value. [See below for the full details on defining a color](#color-def).

      - <a name="pig-zoom"></a>min-zoom: (float64)

        An optional field that sets a zoom level at which the pigment is allowed to draw. If this value were 100, then the pigment would not draw until the user was zoomed in at least to the level of about the size of Hungary.  By default, however, a pigment will be allowed to render at all zoom levels.

  - <a name="hover"></a>hover: (slaw::map)

    This map sets up all the options and configurations surrounding how the hovering of entities works and what information gets displayed in the pop up text.

    - <a name="hov-max"></a>max-found: (int64)

      This sets up how many entities hovering considers to be "too many to hover over" within 1/16th of the viewable area.  This field is optional and defaults to 256; meaning that if more than 256 items are within the area you are pointing, then you will need to zoom in further before you see any details of the items in that area.

    - <a name="hov-zoom"></a>min-zoom: (float64)

      This sets how far zoomed in the user needs to be before the hovering feature becomes active.  This field is optional and defaults to 100.0 (which corresponds to about to a point where the Hungary would fill the screen).

    - <a name="hov-glyph"></a>glyph: (string)

      The path/filename of the icon to be used to highlight the entity being determined as "hovered."  Sluice comes with one icon we suggest using called "sharkmouth.png"

    - <a name="hov-labels"></a>labels: (slaw::list)

      This section sets up what text should be displayed in the hover and is network specific. So if your database is be sending attributes about your entity that are named things like "bus.name.xyz" you can make them more human readable and simply display "name" or set them to not display at all. Each entry in this list sets up the text display for an entity type (as expected to be found in the "kind"s fields of the topology).

      - <a name="lab-kind"></a>kind: (string)

        The kind of network entity this label configuration should be applied to. The string values in this field should match the "kind" values given when [injecting the topology](inject-topology.html).

      - <a name="lab-disp"></a>display: (string)

        Overrides what is displayed in the popup for the "kind" as described above. So if your entity's kind is a database specific look like bus.big, you may want to use this field so that it would display as "Big Bus."

      - <a name="lab-skip"></a>skip: (slaw::list)

        Use this list to identify all attributes that are sent with the topology or observations that shouldn't be displayed in the popup.

      - <a name="lab-rename"></a>rename: (slaw::map (string: string))

        Use this map to map human readable text to not-so-human readable fields.  So for instance if your bus.big entity type has an attribute of capacity.luggage you may want to use this to display "Luggage Capacity" instead.

<a name="size-def"></a>
### Defining Sizes

**About**

Sizes can be defined as either a hard and constant value or a soft value that changes over time according to a sinusoidal wave.  The way in which you define both is defined below.

<pre>
a) Assign a constant size
{
    type: SoftFloat,
    value: <code>float64</code> <em>size</em>
}

OR
b) Assign a soft/sinusoidal size
{
    type: SineFloat,
    center: <code>float64</code> <em>center</em>,
    amplitude: <code>float64</code> <em>amplitude</em>,
    frequency: <code>float64</code> <em>frequency</em>,
    phase: <code>float64</code> <em>phase</em>
}
</pre>

**Slaw::Map Format**

  - type: <SoftFloat, SineFloat>

    Sets the type of float to use for setting the size.  SoftFloat indicates the size should be set constant (the name corresponds to a base type in g-speak). SineFloat indicates the size should be set according to a sinusoidal wave.

factor of size of feld and the log (1/visiblelat) * size you gave us

  - value: (float64)

    This field is only used if the type is "SoftFloat" and gives the constant size to use.

  - center: (float64)

    The center value of the sine wave. This would be the size value that the entity appears to oscillate around.

  - amplitude: (float64)

    The amplitude value of the sine wave. This is the amount the size will offset from the center.  So if you define the center to be 1.0 and an amplitude of 1.0, then the size will range from 0.0 to 2.0.

  - frequency: (float64)

    The frequency of the sine wave in terms of seconds. This means, how long should it take the size to go a full wave.  The larger the number, the faster the entity will oscillate its size.

  - phase: (float64)

    The phase or offset of the sine wave. This changes where along the wave, the value will start and should be given in terms of radians.

**Example**

@include  proteins/size.slaw

<a name="color-def"></a>
### Defining Colors

**About**

Colors can also be defined as either a hard and constant value or a soft value that changes over time according to a sinusoidal wave.  The way in which you define both is defined below.

<pre>
a) Assign a constant color
{
    type: SoftColor,
    value: <<code>v4float64</code> <em>color</em>, <code>v2float64</code> <em>color</em>>
}

OR
b) Assign a soft/sinusoidal color
{
    type: SineColor,
    center: <<code>v4float64</code> <em>center</em>, <code>v2float64</code> <em>center</em>>,
    amplitude: <<code>v4float64</code> <em>amplitude</em>, <code>v2float64</code> <em>amplitude</em>>,
    frequency: <code>float64</code> <em>frequency</em>
    phase: <code>float64</code> <em>phase</em>
}
</pre>

**Slaw::Map Format**

  - type: <SoftColor, SineColor>

    Sets the type of color to use for setting the adjacent color of an entity.  SoftColor indicates the size should be set constant (the name corresponds to a base type in g-speak). SineColor indicates the color should be set according to a sinusoidal wave.

  - value: (v4float64 or v2float64)

    This field is only used if the type is "SoftColor" and gives the constant color to use in terms of RGBA or Grayscale and all values should be given in the range of 0.0 - 1.0.  If a v4float64 is provide, the color will correspond to [R, G, B, A].  If a v2float64 is provide, it will be parsed as a grayscale value where the first value is the grayscale value and the second is the alpha value.  Thus [1, 1, 1, 1] and [1, 1] are equivalent and represent an opaque white.

  - center: (v4float64 or v2float64)

    The center value of the sine wave. This would be the color value that the entity appears to oscillate around. Same as with above, the color can be given as either an RGBA (v4float64) or Grayscale (v2float64) value.

  - amplitude: (v4float64 or v2float64)

    The amplitude value of the sine wave. This is the amount the color will offset from the center.  So if you define the center to be (0.0, 0.0, 0.5, 0.5) and an amplitude of (0.0, 0.0, 0.5, 0.5), then the color will range from invisible black to opaque blue.

  - frequency: (float64)

    The frequency of the sine wave in terms of seconds. This means, how long should it take the size to go a full wave.  The larger the number, the faster the entity will oscillate its color.

  - phase: (float64)

    The phase or offset of the sine wave. This changes where along the wave, the value will start and should be given in terms of radians.

**Example**

@include  proteins/color.slaw

<a name="attributes"></a>
### Attributes  ###

The list of attributes defines the various parts of a fluoroscope that can be configured or changed on the fly from the "fluoroscopes" tab in the web interface.  Each attribute then has a unique name, selection-type, and contents list to define that attribute. Network fluoroscopes support four attributes: "opacity", "enable-menu", "enable-kinds", and "dynamic-filters-whitelist". See the [Reserved Attributes](#attr-res) for details on these supported attributes.

  - name: (string)

    The unique name associated with the attribute. Some attributes names are reserved in sluice and map to specific meanings.  See the three different types of fluoroscopes for type-specific attributes that have been reserved.

  - <a name="selection-type"></a>selection-type: (string)

    This field denotes what form the attribute should take in menu form.  Currently, there are two options "exclusive" and "inclusive".  Exclusive means that one and only one item in the contents list should be selected at any given time. For instance, "opacity" is exclusive because the fluoroscope can't have both 25% and 100% opacity.  Inclusive means that none or multiple items in the contents list can be selected at any given time.  For instance, the attribute may be a filter on what entity types in a network should be displayed... so you can have everything deselected (and show nothing) or you can have a subset or all entities selected.  This field will likely increase in the future to support more selection types, like floating point ranges (opacity would become a floating point range to allow for all values from 0-100, vs the current form of only selecting a few options).

  - <a name="contents"></a>contents: (slaw::list)

    This list contains all the options for this attribute.  For instance, most default fluoroscopes come with four opacity options (25%, 50%, 75%, 100%).  Each list/option entry should have the following:

    - <a name="cont-name"></a>name: (string)

      This field should be regarded more as the value that is mapped to this entry.  While it needs to be a string, oftentimes it is used as a float or integer.  In the case of opacity, the names are typically '0.25', '0.50', '0.75', and '1.0' but as a reserved attribute type, sluice translates these strings into the appropriate floating point value to properly set the fluoroscope's opacity.

    - <a name="cont-disp"></a>display-text: (string)

      The human-readable display version of the name(value).  This value gets used by the fluoroscope tab in the web interface to display the options so that the user doesn't see '0.25',... but rather "25%",...

    - <a name="cont-sel"></a>selected: (bool)

      Denotes whether the item is selected or not.  If false/0, the item is deselected. If true/1, the item is selected.  In the case of an exclusive list, only one item in the contents list should evaluate to true. While in an inclusive list all to none may evaluate to true.

<a name="attr-res" />
### Reserved Attributes

**opacity** - Provides a menu-ing option for the user to change the opacity of a fluoroscope instance.

@code
name: opacity
selection-type: exclusive
contents:
- name: '0.25'
  display-text: 25%
  selected: 0
- name: '0.50'
  display-text: 50%
  selected: 0
- name: '0.75'
  display-text: 75%
  selected: 1
- name: '1.00'
  display-text: 100%
  selected: 0
@endcode  

**enable-menu** - Provides a menu-ing option to the user in the web interface to turn on/off the pop-up text feature that happens when hovering an entity.

@code
name: enable-menu
selection-type: exclusive
contents:
- name: true
  display-text: Enable
  selected: true
- name: false
  display-text: Enable
  selected: true
@endcode

**enabled-kinds** - Provides a menu-ing option to the user to turn on/off the display of single entity kinds.

@code
name: enabled-kinds
contents:
- name: BusStop
  selected: true
  display-text: BusStop
selection-type: inclusive
@endcode

**dynamic-filters-whitelist** - Provides a menu-ing option for allowing the user to toggle filters that are defined by more complicated javascript. [See below](#js) for more details on setting up a javascript function to work with the topology. For this attribute, the "name" field should be filled in with the actual javascript command.

@code
name: dynamic-filters-whitelist
contents:
- name: 'true'
  display-text: All
  selected: false
- name: '("Bus" == kind(n)) ? ("school" == observation(n, "type", t)) : true'
  display-text: School Buses
  selected: false
- name: '("Bus" == kind(n)) ? ("metro" == observation(n, "type", t)) : true'
  display-text: Metro Buses
  selected: false
- name: '("Bus" == kind(n)) ? ("private" == observation(n, "type", t)) : true'
  display-text: Private Buses
  selected: false
selection-type: inclusive
@endcode  

### Sample

@include proteins/network-fluoro-config.prot

<a name="js" />
### Using javascript to instantiate a filter

When rendering, if a javascript filter is applied, sluice will perform the javascript equation against every entity in the topology. So you can think of it as so:

@code
for (entity n in topology)
  if (javascript filter equates to true)
    render entity n
  else
    do not render entity n
@endcode

There are two built in functions for accessing entities you will want to use to interact with the underlying topology data:
  - kind(n): This function returns the "kind" of entity <em>n</em>.
  - observation(n, 'attr-id', t): This function will return the value of entity <em>n</em> for attribute <em>attr-id</em> at time <em>t</em>

IMPORTANT: Turning on filters can reduce the frame rate of sluice.  We suggest only turning on filters when zoomed in to smaller portions of your topology to reduce the number of entities the filters are applied across.  We will continue to work on improving the performance of this feature.