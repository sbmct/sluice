New Fluoroscope Instance Request Protocol       {#new-fluoro-instance}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request New Fluoroscope Instance

### What does it do?

This protein is used to request a new fluoroscope be added to the map view instantaneously (do not pass the fluoroscope palette and do not collect $200).  You can either send an entire fluoroscope configuration to define a new type or you can give the name of a template in the palette to use the configuration for it. Also, you can optionally define the lat/lon bounds for where the fluoroscope should appear on the map.  If no bounds are given, it will chose the bounds currently in view on the center screen.  Upon success, a [New Fluoroscope Instance PSA](new-fluoro-instance-psa.html) and [Response Protein](new-fluoro-instance-response.html) are sent out by sluice.  Upon failure, only the response protein is sent with an error message.

@image html new_fluoro_instance.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, new-fluoro-instance ]
i:  {
        fluoro-config: <code>slaw::map</code> <em><a href="fluoro-configuration-spec.html">Fluoroscope Configuration </a></em>,
        bl: <code>vect</code> [ <em>latitude</em>, <em>longitude</em>, <em>elevation</em> ],
        tr: <code>vect</code> [ <em>latitude</em>, <em>longitude</em>, <em>elevation</em> ],
        windshield: <code>bool</code> <em>should lie on windhshield</em>
    }

OR

d: [ sluice, prot-spec v1.0, request, new-fluoro-instance ]
i:  {
        name: <code>string</code> <em>name_of_template</em>,
        bl: <code>vect</code> [ <em>latitude</em>, <em>longitude</em>, <em>elevation</em> ],
        tr: <code>vect</code> [ <em>latitude</em>, <em>longitude</em>, <em>elevation</em> ],
        windshield: <code>bool</code> <em>should lie on windhshield</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - new-fluoro-instance

**Ingests:**

  - fluoro-config: (slaw::map)

    This key maps directly to the fluoroscope configuration protocol as we are defining a completely new fluoroscope type.  Take a look at the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for details on what all may be in this map.

  - name: (string)

    The unique name identifier of one of the templates in the fluoroscope palette.

  - bl: (vect)

    The lat/lon intersection point for the bottom left corner where the new fluoroscope should appear.  The z value maps to elevation which is currently not supported in sluice.

  - tr: (vect)

    The lat/lon intersection point for the top right corner where the new fluoroscope should appear.  The z value maps to elevation which is currently not supported in sluice.

  - windshield: (bool)

    This is an optional field to allow creating the fluoroscope on the windshield.  By default this value is false.

### Sample

@include proteins/new-fluoro-instance.prot
