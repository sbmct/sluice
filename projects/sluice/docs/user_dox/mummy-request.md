Request To Save a Layout Protocol       {#mummy-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Save Layout

### What does it do?

Use this protein to save a new layout based on the current state of Sluice.  Things that are saved include:

  - The current view of the map [center and zoom level]
  - All fluoroscope instances and their current settings
  - All fluoroscope templates and their configurations
  - All windshield objects and their positions

Things that are NOT saved include:

  - The current time of sluice
  - Security tokens
  - If the user is pushed back

See the [Open Layout Protocol](vivify-request.html) for recalling your saved layouts.

@image html mummify_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, mummify ]
i:  { filename: <code>Str</code> <em>filename</em> }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - mummify

**Ingests:**

  - filename: [string]

    The name to save this layout to. It should be unique and perhaps descriptive.
  
### Sample

@include proteins/mummify-request.prot
