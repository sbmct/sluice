Workstation Install Guide (Emu 1.18)     {#workstation-install-internet-emu}
=========================================

[Home](index.html) &rarr; [Installation Guides](installation-guides.html) &rarr; Workstation Install Guide 1.18

### Requirements

  - Ubuntu 10.04
  - Working Internet Connection
  - [The latest NVIDIA Linux x86_64 display driver](http://www.nvidia.com/object/unix.html)
  - Google Chrome
  - tarball of g-speak, sluice, and dependency packages provided via [Basecamp](https://basecamp.com/)

    Folder should include:
      - 156 base debian packages
      - 3 sluice packages
        - sluice-web-1.6.0.deb
        - sluice-kipple-1.18.deb
        - sluice-1.18.0.deb
      - deploySluice.sh script

### Instructions

#### Install g-speak

  - **Obtain authentication certificate**

    - Our apt-get repository requires certificate-based authorization for external connections. If you have not received your certificates, please request them by emailing dan@oblong.com.

    - Certificates, once obtained, should be placed in the following locations:

      <code>cert: /etc/apt/ssl/oblong-repo.crt<br>
      key: /etc/apt/ssl/oblong-repo.key</code>

  - **Obtain and Install g-speak packages**

    - Download the Oblong APT repo debian package: [oblong-apt-repo.deb](https://platform.oblong.com/system/resources/BAhbBlsHOgZmSSIwMjAxMi8wNi8xMi8xMS80NC8zNS82ODYvb2Jsb25nX2FwdF9yZXBvLmRlYgY6BkVU/oblong-apt-repo.deb).
    - Install repo with the following command:

      <code>sudo dpkg -i /path/to/oblong-apt-repo.deb </code>

    - Download Oblong's public PGP key: [oblong-repo.gpg](https://platform.oblong.com/system/downloads/oblong-repo.gpg).
    - Install it with the following command:

      <code>sudo apt-key add /path/to/oblong-repo.gpg </code>

    - Update the repository:

      <code>sudo apt-get update </code>

    - Install g-speak

      <code>sudo apt-get install g-speak </code>

    - Install g-speak gstreamer plugins

      <code>sudo apt-get install g-speak-gst-plugins</code>

    - Install g-speak staging libraries

      <code>sudo apt-get install g-speak-staging</code>

    - Install ob-http-ctl

      <code>sudo apt-get install ob-http-ctl</code>

#### Obtain and install sluice packages

  - Install berkelium dependency library

    <code>sudo apt-get install berkelium-dev</code>

  - untar sluice_pkgs.tgz obtained on Basecamp

    <code>tar zxvf sluice_pkgs.tgz</code>

  - install packages

    <code>cd sluice_pkgs</code>
    <code>dpkg -i *.deb</code>

### Path Locations of Install

**g-speak libraries**

  /usr/lib

**g-speak executables**

  /usr/bin

**ob-http-ctl**

  /opt/oblong/jsplasma

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**sluice web**

  /opt/oblong/sluice-web

**resource files**

  /usr/share/oblong/sluice

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**screen and feld proteins**

  /usr/share/oblong

**logs**

  /home/[username]/.sluice-logs