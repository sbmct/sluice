Tile Server Configuration Protocol       {#tile-config}
===================

[Home](index.html) &rarr; [Tile Server](tile-server.html) &rarr; Tile Server Config

### What does it do?

This protein is used to configure how the Tile Server should handle [tile requests](tile-request.html). With this configuration, you can change how many threads should be used to handle the requests as well as what tile services it should use, if a request should be forwarded on to another daemon that can handle the request, and if handling the request internally, how the fetched tiles should be cached.

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ tiled, config ]
i:  {
      fetch-threads: <code>int64</code> <em>num threads</em>,
      cache-threads: <code>int64</code> <em>num threads</em>,
      cache-base: <code>Str</code> <em>cache storage location</em>,
      request-pool: <code>Str</code> <em>tile request pool name</em>,
      tile-policies:
          [
              {
                  <em>Tile Policy Map: <a href="#policy-map"> See below</a> for the various formats of this map</em>
              }
          ]
    }
</pre>

**Descrips:**

  - tiled
  - config

**Ingests:**

  - fetch-threads: [int64]

    num for fetching from network

  - cache-threads: [int64]

    num for fetching from local cache

  - cache-base: [Str]

    directory where cache should live

  - request-pool: [Str]

    where requests should come from

  - tile-policies: [Slaw::Map]

    This map configures each tile source. A tile source can be of two types: forward and standard.  A forwarding source means simply that it is expected that another external process can handle the request and so the request is simply forwarded on to another pool.  A standard source means that there is a format by which the server can fetch the requested tile from a url.  The url format is also set up in this map.  [See Below](#policy-map) for details on this map.

<a name="policy-map" />
### Tile Policy Map Formats  

a) if the map source is standard (pulls from the network/url)
<pre>
{
  mapid: <code>Str</code> <em>unique name for source</em>,
  type: standard,
  url: <code>Str</code> <em>url format for fetching tiles</em>,
  cache: <code>Str</code> <em>file for caching tiles</em>,
  max-z: <code>int64</code> <em>maximum zoom level for tile source</em>,
  offline: <code>bool</code> <em>use cache only</em>
}
</pre>

b) if the map source is forward (forwards the request on to another pool)
<pre>
{
  mapid: <code>Str</code> <em>unique name for source</em>,
  type: forward,
  pool: <code>Str</code> <em>pool name for forwarding</em>
}
</pre>

- mapid: [string]

  A unique identifying string for a map source.  The [tile request](tile-request.html) sends a map-id that should match one of the mapids configured in the tile-policies.

- type: <standard, forward>

  Defines whether the tile server should try to handle the request or forward it on.

- url: [string]

  The url format the tile server should use to make an http request for the requested tile.  This format should defined as follows:

  `http://[my_tile_source]/[tile index]`

  where `my_tile_source` may be something like `mapquest/tiles` and the tile index may be something like `$Z/$X/$Y.jpg`.  The tile server parses the string variables `$Z`, `$X`, and `$Y` so that when a request comes in for x:0, y:2, z:1, it will make an http request to `http://otile1.mqcdn.com/tiles/1.0.0/osm/1/0/2.jpg`.

- cache: [string]

  Defines the file format for how each tile should be saved to the "cache-base" directory. In addition to the `$Z`, `$X`, & `$Y` variables, sluice will also parse `$ROOT` which will be parsed to the path given in the "cache-base" field and `$MAPID` which is parsed to the value given in "mapid". Using the same example in the url, this may look something like:

  `$ROOT/$MAPID/$Z/$X/$Z-$X-$Y.jpg`

  where sluice would save the 1/0/2 tile to something like `/home/[user]/tmp/cache/mapquest/1/0/1-0-2.jpg`

- max-z: [int64]

  This value sets up the maximum zoom level the tile source supports. This value is optional and if not given, the tile server will always attempt to retrieve tiles even for zoom levels beyond what the source supports.

- offline: [bool]

  An optional field that can be set to true so that tiled does not even attempt to fetch tiles from the url and instead only look to cache.

- pool: [string]

  This field is used when forwarding requests to determine what pool the request should be sent along through.

### Sample

@include proteins/tiled-config.protein
