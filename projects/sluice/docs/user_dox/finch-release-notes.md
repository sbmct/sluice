Release Notes for Finch 1.20.0    {#finch-release-notes}
============================

[Home](index.html) &rarr; [Release Notes Home](release-notes.html) &rarr; Finch Release Notes

##### Contents:
 - [New Features](#new-features)
 - [Bug Fixes](#bug-fixes)
 - [New Dependencies](#new-depends)
 - [Known Issues](#known-issues)
 - [Credits](#credits)

<a name="new-features"></a>
### New Features:

1.  Upgraded sluice to build on top of Ubuntu 12.04
2.  Upgraded embedded web pages to replace the use of the open source project, Berkelium with a more stable and supported library called [Awesomium](http://awesomium.com/)
3.  Upgraded sluice to build on top of the latest g-speak (3.4)
4.  Discontinued use of mz-link in favor of using the same MzReach application Mezzanine uses and now provides support for remote screen sharing on both Mac and Windows. The executables for these have been sent along with the sluice packages.
5.  Updated look of fluoroscope loading; replaced the flashing red with a loading banner
6.  Added new functionality to the Admin tab on sluice's web page to allow the user to shutdown or reboot the machine.
7.  Added new functionality to the Admin tab on sluice's web page to allow the user to restart sluice or ob-http-ctl as well as check the installed versions of g-speak, sluice and ob-http-ctl.
8.  Added a new allowable field to the [Atlas Fluoroscope Configuration](atlas-fluoro-api.html) that allows users to set the machine address for the tile server. This both allows you to put your local machine name so that response pools are given the appropriate location or let sluice request and listen to responses through pools on a separate machine.
9.  Added 'size' as an optional field in [web fluoroscope](web-request.html) requests.  If no size is given, the fluoroscope will be given the traditional fixed size.

<a name="bug-fixes"></a>
### Bug Fixes

1.	Fixed video issue, where videos initialized to a single pixel wide and required resizing in order to fill out.

<a name="new-depends"></a>
### New Dependencies:

1.  Ubuntu 12.04
2.	g-speak 3.4
3.	Awesomium 1.6.5

<a name="known-issues"></a>
### Known Issues

1. Faults do not work. We have temporarily removed the tab from the web interface for showing entities that are "faulted."  We are working on updating this concept to be more widely usable and configurable.

<a name="credits"></a>
### Credits

Map Tiles are provided by:

  - [Mapquest OSM](http://developer.mapquest.com/web/products/open/map). Data by [OpenStreetMap](http://openstreetmap.org), under [CC BY SA](http://creativecommons.org/licenses/by-sa/3.0).
  - [Staman Design](http://stamen.com), under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0). Data by [OpenStreetMap](http://openstreetmap.org), under [CC BY SA](http://creativecommons.org/licenses/by-sa/3.0).

Web libraries are provided by:

  [Awesomium](http://awesomium.com/)