Snapshot Announcement (PSA) Protocol       {#snapshot-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Snapshot PSA

### What does it do?

This protein is sent from sluice to inform external processes that a user has taken a snapshot using the web client.  It includes a url path to where the snapshot can be downloaded from. NOTE: Snapshots are saved to disk but are overwritten with each run of sluice.  Currently, snapshots must be taken using the web interface; there is no native interface for taking a snapshot.

@image html snapshot_psa.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, snapshot ]
i:  { image-uri: <code>Str</code> <em>path_to_snapshot_image</em> }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - snapshot

**Ingests:**

  - image-uri: [Str]

    Provides the url path for where the snapshot image can be retrieved. External processes should use http GET and prepend "http://" and the machine name of sluice to the path given by image-uri. In the sample below, the full url would be:

<pre>              http://<em>sluice_machine_name</em>/r/as-000000000065.png</pre>

### Sample

@include proteins/snapshot_psa.prot
