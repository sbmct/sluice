Delete Bookmark Protocol       {#del-bookmark-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Delete Bookmark

### What does it do?

Use this protein to remove an existing bookmark from sluice. Upon successful completion, sluice returns the [Bookmarks List PSA](bookmarks-psa.html). 

@image html del_bookmark.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, delete-bookmark ]
i: { name: <code>Str</code> <em>bookmark_name</em> }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - delete-bookmark

**Ingests:**

  - name: [string]

    The name of the bookmark to be deleted.

### Sample

@include proteins/del-bookmark-request.prot
