Request Atlas View Protocol       {#atlas-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Atlas View

-----------

### What does it do?

Use this protein to get back from sluice the current view of the map; the lat/lon location that is at the center of the view along with the current zoom level. Look at the [Atlas View Announcement Protocol](atlas-view-psa.html) for details on what Sluice sends in return.

@image html atlas_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, atlas-view ]
i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - atlas-view

**Ingests:**

none

### Sample

@include proteins/atlas-request.prot
