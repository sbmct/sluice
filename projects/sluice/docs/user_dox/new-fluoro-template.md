Create a New Fluoroscope Template Protocol       {#new-fluoro-template}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request New Fluoroscope Template

### What does it do?

This protein is used to request a new fluoroscope be added to the fluoroscope bin the user sees when they push back. In response, sluice will return a protein notifying of success or failure.  See the [New Fluoroscope Template Response Protocol](fluoro-template-response.html) for details. The main reason for failure is having the bin full and incapable of accepting any new fluoroscopes. The user will need to delete a template from the bin before a new one is accepted.  Currently, the maximum number of fluoroscope templates allowed is 12.

@image html new_fluoro_template.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, new-fluoro-template ]
i:  { <em>*See the <a href="fluoro-configuration-spec.html">Fluoroscope Configuration Protein Spec</a> for documentation on this map</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - new-fluoro-template

**Ingests:**

  - *fluoroscope configuration (slaw::map)

    The ingests of this protein map directly to the fluoroscope configuration protocol as we are defining a completely new fluoroscope type.  Take a look at the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for details on what all may be in this map.

### Sample

@include proteins/new-fluoro-template.prot
