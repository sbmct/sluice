Video Fluoroscope Configuration Specification       {#video-fluoro-api}
===================

[Home](index.html) &rarr; [Fluoroscope Configuration Specification](fluoro-configuration-spec.html) &rarr; Video Fluoroscope Configuration Specification

### What does it do?

This configuration provides sluice with all the information it needs to display a video fluoroscope.  This includes information regarding what daemon it should communicate with and at what opacity it should render. See the [fluoroscope communication pages](fluoroscope-api.html) for how the communication works and the format of those proteins.

### Slaw Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
{
  <a href="#fluoro-name">name</a>: <code>Str</code> <em>unique_video_fluoroscope_name</em>,
  <a href="#fluoro-icon">icon</a>: <code>Str</code> <em>path_to_icon_used_in_pushback</em>,
  <a href="#fluoro-disp">display-text</a>: <code>Str</code> <em>text_to_display_in_pushback</em>,
  <a href="#fluoro-type">type</a>: video,
  <a href="#fluoro-daemon">daemon</a>: <code>Str</code> <em>external_process_identifier</em>,
}
</pre>

**Slaw::Map Format:**

  - <a name="fluoro-name"></a>name: (string)

    The unique name identifier of this video fluoroscope template.

  - <a name="fluoro-icon"></a>icon: (string)

    The path to an image icon that will be used to visually identify this fluoroscope when the user pushes back in sluice.

  - <a name="fluoro-disp"></a>display-text: (string)

    The text that is added to the icon defined above and displayed when a user pushes back in sluice.

  - <a name="fluoro-type"></a>type: video

    This notifies sluice of the type of fluoroscope that needs to be created and must be "video".

  - <a name="fluoro-daemon"></a>daemon: (string)

    This string uniquely identifies the external process that will communicate with the instances of this type of fluoroscope.  The external process should match against this string to determine when requests are being made of it and not another process.

  - <a name="fluoro-attr"></a>attributes: (slaw::list)

    [See below](#attributes)

<a name="attributes"></a>
### Attributes

**Overview**

  The list of attributes defines the various parts of a fluoroscope that can be configured or changed on the fly from the "fluoroscopes" tab in the web interface.  This includes things like "opacity", which is the one attribute that all fluoroscopes should have built in.  Each attribute then has a unique name, selection-type, and contents list to define that attribute. Video fluoroscopes do not currently support any reserved attributes. For details on how attributes would be otherwise set up, you can refer to the [Main Fluoroscope Spec](fluoro-configuration-spec.html#attrs).

### Sample

@include proteins/video-fluoro-config.prot
