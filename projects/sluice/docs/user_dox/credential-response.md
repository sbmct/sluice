Response to Credential Request Protocol       {#credential-response}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Credentials Response

### What does it do?

This protein is sent from sluice in response to the [Credential Request](credential-request.html) protein. It returns the user-id, username, and token of the user currently logged in to sluice.  For more information on logging in and logging out, see the [Request Login](login-request.html) and [Request Logout](logout-request.html) protocols.

@image html credentials_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, response, logout ]
i:  {
        user-id: <code>Str</code> <em>user_id</em>,
        user-name: <code>Str</code> <em>user_name</em>,
        token: <code>Str</code> <em>token</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - response
  - creds

**Ingests:**

  - user-id: [string]

    This field provides the user's unique identifier.

  - user-name: [string]

    The human-readable label that is displayed in the system area popup.

  - token: [string]

    The security token to be sent around for validation and for internal web page loading.
  
### Sample

@include proteins/credentials-response.prot
