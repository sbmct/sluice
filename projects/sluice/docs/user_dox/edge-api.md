Edge Device Protocol           {#edge-api}
=============

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; Edge Device API

##Atlas View Control and Communication

@image html atlas_control.svg

sluice-to-edge proteins
- [Atlas View Announcement](atlas-view-psa.html)
- [List of Bookmarks Announcement](bookmarks-psa.html)
- [Bookmark Creation Error Response](bookmark-error.html)

edge-to-sluice proteins
- [Request Atlas View](atlas-request.html)
- [Request Zoom](zoom-request.html)
- [Request Bookmark List](bookmarks-request.html)
- [Create a New Bookmark](new-bookmark-request.html)
- [Delete a Bookmark](del-bookmark-request.html)

##User Action Announcements

@image html user_psas.svg

sluice-to-edge proteins
- [User Click Announcement](click-psa.html)
- [New Fluoroscope Instance Announcement](new-fluoro-instance-psa.html)
- [Snapshot Announcement](snapshot-psa.html)
- [Fluoroscope Skewered Announcement](skewer-psa.html)

##Fluoroscope Creation and Communication

@image html fluoro_control.svg

sluice-to-edge proteins
- [List of Fluoroscope Instances Announcement](fluoro-list-psa.html)
- [Fluoroscope Configuration Update Announcement](fluoro-update-psa.html)
- [New Fluoroscope Template Response](fluoro-template-response.html)
- [New Fluoroscope Instance Response](new-fluoro-instance-response.html)

edge-to-sluice proteins
- [Request Fluoroscope List](fluoro-request.html)
- [Configure Fluoroscope Instance](config-fluoro.html)
- [Create a New Fluoroscope Template](new-fluoro-template.html)
- [Create a New Fluoroscope Instance](new-fluoro-instance.html)
- [Remove a Fluoroscope Instance](remove-fluoro-instance.html)
- [Remove all extant Fluoroscopes](remove-all-fluoros.html)
- [Create a Web Page Instance](web-request.html)
- [Remove a Web Page Instance](remove-web-instance.html)
- [Reorder Fluoroscopes on Z-Axis Request](reorder-on-zaxis-request.html)
- [Move a Fluoroscope](move-fluoro.html)

##Time Control

@image html time_control.svg

sluice-to-edge proteins
- [Current Time Announcement](time-psa.html)

edge-to-sluice proteins
- [Request Current Time](time-request.html)
- [Request to Set Current Time](set-time-request.html)

##Layout Control

@image html layout_control.svg

edge-to-sluice proteins
- [Save Layout](mummy-request.html)
- [Open Layout](vivify-request.html)

##Security Protocol

@image html security_control.svg

sluice-to-edge proteins
- [Current User Credentials Response](credential-response.html)

edge-to-sluice proteins
- [User Login Request](login-request.html)
- [User Logout Request](logout-request.html)
- [Request Credentials of Current User](credential-request.html)

##Faulty Entity Communication

@image html faults_control.svg

sluice-to-edge proteins
- [List of Faults Announcement](fault-psa.html)

edge-to-sluice proteins
- [Fault Request](fault-request.html)
