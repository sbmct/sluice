New Fluoroscope Instance PSA Protocol       {#new-fluoro-instance-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; New Fluoroscope Instance PSA

### What does it do?

This protein is used to notify the world each time a new fluoroscope has been added to the atlas.  It includes the full fluoroscope's configuration slaw merged with it's qid. You may want to take a look at the protocols for programmatically [creating](new-fluoro-instance.html) and [removing](remove-fluoro-instance.html) fluoroscopes from the atlas.

@image html new_fluoro_instance_psa.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, new-fluoro-instance ]
i:  {
        SlawedQID: <code>unt8_array</code> <em>unique_id</em>,
        <em>*See the <a href="fluoro-configuration-spec.html">Fluoroscope Configuration Protein Spec</a> for documentation on the rest of this map</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - new-fluoro-instance

**Ingests:**

  - SlawedQID: [unt8 byte array]

    The qid value (or unique id) of the newly created fluoroscope.

  - *fluoroscope configuration (slaw::map)

    The ingests of this protein map directly to the fluoroscope configuration protocol.  Take a look at the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for details on what all may be in this map.  It is important that you send the entire configuration back rather than just the changed attributes to ensure that no configurations are lost.

### Sample

@include proteins/new-fluoro-instance-psa.prot
