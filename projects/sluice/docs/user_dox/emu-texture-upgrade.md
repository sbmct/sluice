Upgrading Texture Fluoroscope Configurations from 1.16 to Emu 1.18   {#emu-texture-upgrade}
=================

[Home](index.html) &rarr; [Release Notes Home](release-notes.html) &rarr; [Emu Release Notes](emu-release-notes.html) &rarr; Texture Fluoroscope Upgrading

### References

 - [Texture Fluoroscope Configuration Specification](texture-fluoro-api.html)

### Changed/Removed Fields

##### descrips:

**new-fluoro:** This has changed to "new-fluoro-template"

##### ingests:

**type:** "out-of-process" has changed to "texture"

**process:** This field has been removed

### New Required Fields

**daemon:** This section sets the name of the external process the fluoroscope should communicate with.  Previously the "name" field was used for this, but now "name" is simply to identify the fluoroscope and not dual-purposed.