Workstation Install Guide (Finch 1.20)     {#workstation-install-finch}
=========================================

[Home](index.html) &rarr; [Installation Guides](installation-guides.html) &rarr; Workstation Install Guide 1.20

### Requirements

  - Ubuntu 12.04
  - The current NVidia Driver ([295.40](http://www.nvidia.com/object/linux-display-amd64-295.40-driver))
  - Google Chrome
  - tarball of g-speak, sluice, and dependency packages provided via [Basecamp](https://basecamp.com/)

    Folder should include:
      - 248 base debian packages
      - 5 ruby gems
      - 3 sluice packages
        - sluice-web-1.8.0.deb
        - sluice-kipple-1.20.0.deb
        - sluice-1.20.0.deb
      - deploySluice.sh script

### Instructions

- Upgrade the machine to Ubuntu 12.04

  See [this page](ubuntu-upgrade-1204.html) for OS upgrade procedures

- Copy the packages provided via Basecamp on to the machine and untar

  <code>$ scp sluice-1.20-pkgs.tgz ~/.<br>
  $ tar -xzvf sluice-1.20-pkgs.tgz</code>

- Install packages
  
  1. <code>$ cd sluice-1.20-pkgs</code>
  2. run script
    1. <code>$ ./deploySluice.sh</code>
    2. follow script instructions

- Run through the demo

  For the latest keyboard/mouse controls, [click here](mouse-and-keyboard.html). Run through your demo as usual to find any missing resources or configurations.  If applicable, copy in missing configurations or replace the default configurations with existing ones.

### Path Locations of Install

**g-speak libraries**

  /opt/oblong/g-speak3.4

**g-speak executables**

  /opt/oblong/g-speak3.4/bin

**ob-http-ctl**

  /opt/oblong/jsplasma

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**sluice web**

  /opt/oblong/sluice-web

**resource files**

  /usr/share/oblong/sluice

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**screen and feld proteins**

  /usr/share/oblong

**logs**

  /home/[username]/.sluice-logs

### Release Notes

[See Here](finch-release-notes.html)