Remove all Fluoroscope Instances Protocol       {#remove-all-fluoros}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Fluoroscope Instance Removal

### What does it do?

This protein is used to remove all extant fluoroscopes.

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, remove-all-fluoro ]
i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - remove-all-fluoro

**Ingests:**

Ignored

### Sample

@include proteins/remove-all-fluoros.prot
