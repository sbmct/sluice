Pre Caching Tile Sets for offline use   {#tile-caching}
=========================================

[Home](index.html) &rarr; [Tile Server](tile-server.html) &rarr; Tile Caching

### Overview

When a tile is requested of the server, the Tile Server will first look to the location of its cache before then attempting to fetch a new tile from the network.  Therefore, we can use a sluice with a network connection to pre cache all the necessary tiles and then manually move the cache to the same relative path on a separate sluice machine for use without a network connection.

The default file/directory format for the cached tiles is:

<code>home/[user]/tmp/cache/${DATASET}/${Z}/${X}/${Z}_${X}_${Y}.$EXT)</code>  

where $DATASET is the name of the tile set being fetched ("mapquest", "cyclemap", etc), and $EXT is the file extension (png or jpg). $Z, $X, and $Y describe the tile index or coordinates as defined by the [Google Maps coordinate system](https://developers.google.com/maps/documentation/javascript/v2/overlays#Google_Maps_Coordinates).  The '_' used in the filename may also be '-'s. Whether the format uses '_' or '-' is defined in the Tile Server's [configuration protein](tiled-config.md).

### Requirements

At least one machine running sluice that has network connection for caching the tiles.

### Instructions

There are two ways to pre-populate a cache:

First is to use sluice interface and take advantage of the existing request protocol to have the Tile Server fetch and cache all the tiles needed.
  - Run sluice and the tile server
  - Using the wand or web interface, slowly zoom in and out of the areas of interest to make the tile requests for those areas.
  - When finished, copy the /home/[user]/tmp/cache directory and all of its contents to the same path on the machine without a network connection. The tile server running there should now have access to and serve up all those tiles accordingly.

Second is to write a script to manually fetch all the tiles in your needed area -- an exercise left for the reader -- and store those tiles in the format described in the above Overview section.  Again, once you have those tiles, simply copy the `/home/[user]/tmp/cache` directory to the machine without a network connection and its tile server should now have access and serve up all the saved off tiles.

**Important:** There always needs to be a 0/0/0 tile, even if there is no intention of ever showing the full map. Sluice will not show any data if this base tile is not found.
