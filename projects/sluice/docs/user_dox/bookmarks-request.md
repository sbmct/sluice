Request Bookmarks List Protocol       {#bookmarks-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Bookmarks List

### What does it do?

Use this protein to get back from sluice the a list of all the bookmarks that have been saved off.  A bookmark is a simple combination of a lat/lon location and a zoom level that can be used to quickly zoom the map view to a specific location. See the [Bookmarks PSA Protocol](bookmarks-psa.html) for the more details on the response.

@image html bookmark_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, bookmarks ]
i: { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - bookmarks

**Ingests:**

none

### Sample

@include proteins/bookmarks-request.prot
