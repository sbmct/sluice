Texture Passthru Request (Experimental)       {#store-forward}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Fluoroscope API](fluoroscope-api.html) &rarr; Texture Response


### What does it do?

Sends a request to a texture fluoroscope including the data needed to generate the texture.  The message can come in two different formats, V1 and V2.  V1 is the original format that can only support a single attribute value.  V2 is a new format that can contain multiple attributes.  The V1 format matches the requests directly from sluice - see [Texture Request] (tex-request.html).


### Pool

sluice-to-fluoro


### Protein Format

<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ conduce, prot-2.0, request, texture, <em>fluoro_qid_slaw</em> ]
i:  {
        data: [
            {
                id: <code>Str</code> <em>entity id</em>,
                timestamp: <code>float64</code> <em>entity start time</em>,
                attrs: <code>slaw::map</code> <em>requested attrs</em>,
                loc: <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>]
            },
            {
                id: <code>Str</code> <em>entity id</em>,
                timestamp: <code>float64</code> <em>entity start time</em>,
                attrs: <code>slaw::map</code> <em>requested attrs</em>,
                path:
                    [
                        <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                        <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                        <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                        ...
                    ]
            },
            ...
        ],
        ...
    }
</pre>


**Descrips:**

  - conduce
  - prot-2.0
  - request
  - texture
  - fluoro_qid_slaw (slaw::cons ("SlawedQID": byte array))

**Ingests:**

  - Most of the ingests will be forwarded.  See [Texture Request] (tex-request.html) for the ingests that are used in texture requests.

  - data (slaw::list)

    A list of observations that met the criteria provided in the [Texture Data Passthru Request](store-request.html).  These will contain a set of attrs that also requested.  This data points can contain either locs or paths depending on how what was originally passed in to create the entities.

