User Clicked Announcement (PSA) Protocol       {#click-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Click PSA

### What does it do?

This protein is sent from sluice to inform external processes that a user has clicked within the map view and includes the lat/lon intersection of the click. If the user was hovering over a network entity at the time of clicking, the entity's information is also included in the notification.

@image html click_psa.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, poked-it ]
i:  { loc: <code>vect</code> [ <em>latitude</em>, <em>longitude</em>, <em>elevation</em> ] }

OR

d: [ sluice, prot-spec v1.0, psa, poked-it ]
i:  {
        loc: <code>vect</code> [ <em>latitude</em>, <em>longitude</em>, <em>elevation</em> ],
        id: <code>Str</code> <em>entity-id</em>,
        time: <code>float64</code> <em>timestamp</em>,
        attrs:
            {
                <code>Str</code> <em>attr_1_key</em>: <code>Str</code> <em>attr_1_val</em>,
                ...
            },
        kind: <code>Str</code> <em>entity_type</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - poked-it

**Ingests:**

  - loc: [vect]

    Provides the lat, lon location of the entity being added.  The z value of the location vector is a placeholder for designating elevation, however, elevation is currently not supported by sluice. Multi-point located entities have a "path" field which takes a list of location vectors.

  - id: [string]

    The unique identifier of the network entity the user clicked on. This field is omitted if the user did not have the cursor over any entity.

  - time: [float64]

    The current timestamp that the play head is set to in sluice.  This timestamp maps to machine time and the number represents seconds elapsed since Jan 1, 1970.

  - attrs: [slaw map of strings to strings]

    The attrs map provides all the attributes sluice knows about for the entity the user selected at the current time of sluice.

  - kind: [string]

    The selected entities type or class.    

### Sample

@include proteins/click_psa.prot
