Request Topology Protocol       {#request-topology}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Topology API](topology-api.html) &rarr; Request For Topology

### What does it do?

This protein makes a global request for client-side applications to inject the network.  It gets sent when a user clicks the "default" button on the web interface, which lives under the "data requests" section on the admin tab.  In response to this request, client-side applications should send [Inject Topology](inject-topology.html) proteins.

@image html request_topology.svg

### Pool

sluice-to-topo

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, topology, request ]    
i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - topology
  - request

**Ingests:**

none

### Sample

@include proteins/topo-request.prot
