Zoom the Atlas View Protocol       {#zoom-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Zoom

### What does it do?

Use this protein to change the current view of the map to a specified lat/lon location or a given network entity.  Zoom level can also be set to make the map zoom in/out.

@image html zoom_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, zoom ]
i:  {
        lat: <code>float64</code> <em>latitude</em>,
        lon: <code>float64</code> <em>longitude</em>,
        level: <code>float64</code> <em>zoom_level</em>
    }

OR

d: [ sluice, prot-spec v1.0, request, new-bookmark ]
i:  {
        entity-id: <code>Str</code> <em>entity_id</em>,
        level: <code>float64</code> <em>zoom_level</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - zoom

**Ingests:**

  - lat: [float64]

    The latitudinal value of where this request should center the view to.

  - lon: [float64]

    The longitudinal value of where this request should center the view to.

  - level: [float64]

    The level of zoom the map should navigate to when this request is selected. This field is optional when giving a network entity to zoom to and if missing, the zoom level will be set to 600, which is approximately the level of an average neighborhood.

  - entity-id: [string]

    The unique identifier of a network entity that sluice knows about. Sluice will change the view so that the given entity is centered.

### Sample

@include proteins/zoom-request.prot
