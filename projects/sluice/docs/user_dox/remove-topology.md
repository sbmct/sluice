Remove Topology Protocol       {#remove-topology}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Topology API](topology-api.html) &rarr; Remove Topology

### What does it do?

Remove entitities from the network. This protein will remove an entity in its entirety from the network storage in sluice.  Time is currently not supported, so once an entity is removed, it will not exist in the history.

@image html update_topology.svg

### Pool

topo-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, topology, remove ]
i:  {
        topology:
            [
                { id: <code>Str</code> <em>entity_id</em> }.
                { id: <code>Str</code> <em>entity_id</em> },
                ...
            ]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - topology
  - remove

**Ingests:**

  - id: [string]

    This string provides a unique identifier for the entity being added. For instance, if your network is licensed drivers, this would be the license number for a driver.

### Sample

@include proteins/topo-remove.prot
