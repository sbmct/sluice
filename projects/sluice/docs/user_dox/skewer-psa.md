User Skewered Fluoroscope Announcement (PSA) Protocol       {#skewer-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Drag/Drop Fluoroscope Protocol

### What does it do?

Skewering is a feature that allows users to drag fluoroscope copies around the room and drop on to other screens.  External g-speak applications feeding to these other screens can connect to the same wands pool as sluice to determine pointing intersections with the screens. When a user grabs a fluoroscope from the bottom right corner, a snapshot of the fluoroscope gets made and sent with a "skewer" notification, notifying the external g-speak application(s) that a fluoroscope has been grabbed for dragging.  This same notification is then sent again when the user releases the fluoroscope.  Using the wands pool and these notifications, the external application(s) can determine if a fluoroscope was dropped on to their screen.  If the fluoroscope is a web page, the url of that web page is also sent with the initial grab notification.

@image html skewer_psa.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
a)
d: [ sluice, prot-spec v1.0, psa, skewer ]
i:  {
        skewered: <code>bool</code> true,
        provenance: <code>Str</code> <em>provenance</em>,
        image: <code>unt8_array</code> <em>image_data</em>,
        url: <code>Str</code> <em>url_of_web_fluoroscope</em>,
        lat_min: <code>float64</code> <em>minimum_latitude</em>,
        lon_min: <code>float64</code> <em>minimum_longitude</em>,
        lat_max: <code>float64</code> <em>maximum_latitude</em>,
        lon_max: <code>float64</code> <em>maximum_longitude</em>,
        SlawedQID: <code>unt8_array</code> <em>QID</em>
    }

b)
d: [ sluice, prot-spec v1.0, psa, skewer ]
i:  {
        skewered: <code>bool</code> false,
        provenance: <code>Str</code> <em>provenance</em>,
        SlawedQID: <code>unt8_array</code> <em>QID</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - skewer

**Ingests:**

  - skewered: [bool]

    A boolean value to indicate whether the announcement is to mark the initial grab of a fluoroscope (in which case the value is true) or the final release of the fluoroscope (the value will be false).

  - image: [unt8_array]

    The full snapshot image of the fluoroscope in PNG byte array form.  The remote gspeak application can use the ReadPngFromSlaw method provided by the PngImageClot class to re-create the image.

  - provenance: [string]

    This field identifies the provenance of the device that dragged the fluoroscope.

  - url: [string]

    This field is only included if the fluoroscope being dragged is a web page, in which case the value of this field contains web address the fluoroscope is viewing.

  - lat_min: [float64]

    This field specifies the minimum latitude value of the fluoroscope bounds.

  - lon_min: [float64]

    This field specifies the minimum longitude value of the fluoroscope bounds.

  - lat_max: [float64]

    This field specifies the maximum latitude value of the fluoroscope bounds.

  - lon_max: [float64]

    This field specifies the maximum longitude value of the fluoroscope bounds.

  - SlawedQID: [unt8_array]

    This field identifies the fluoroscope instance that made the announcement.

### Sample

@include proteins/skewer_psa.prot
