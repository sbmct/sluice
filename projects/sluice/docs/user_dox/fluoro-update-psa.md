Update Fluoroscope Instance Protocol       {#fluoro-update-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Fluoroscope Update PSA

### What does it do?

This protein is sent any time the configuration of a fluoroscope instance is changed to notify any external processes that may be keeping track. Most often these changes are in response to [Configure Fluoroscope Requests](config-fluoro.html).  To see these protein, along with the [fluoroscope list request](fluoro-request.html) and [fluoroscope list psa](fluoro-list-psa.html) in action, watch the edge-to-sluice and sluice-to-edge pools while you navigate to the fluoroscope tab on the web browser and change the settings of a fluoroscope.

@image html config_fluoro.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, update-fluoro, <em>fluoro_qid_slaw</em> ]
i:  {
        SlawedQID: <code>unt8_array</code> <em>unique_id</em>,
        <em>*See the <a href="fluoro-configuration-spec.html">Fluoroscope Configuration Protein Spec</a> for documentation on the rest of this map</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - update-fluoro
  - fluoro_qid_slaw (slaw::cons ("SlawedQID": byte array))

    The qid value (or unique id) of the fluoroscope which this update pertains to.

**Ingests:**

- SlawedQID: [unt8 byte array]

  The unique id of the fluoroscope instance. While it is a duplicate of the final descrip above, it may be easier to parse.

- *fluoroscope configuration (slaw::map)

  This map includes the entire configuration of the given fluoroscope and maps directly to the fluoroscope configuration protocol.  Take a look at the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for details on what all may be in this map.

### Sample

@include proteins/fluoro-update-psa.prot
