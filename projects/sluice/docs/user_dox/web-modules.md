Incorporating Custom Tabs in the Web Interface   {#web-modules}
=========================================

[Home](index.html) &rarr; Incorporating Custom Web Tabs

## Coming Soon

This page will provide full documentation and examples for building and incorporating your own custom pages into the sluice web interface. We hope to have this complete shortly after the EMU release.