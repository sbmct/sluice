Faulty Entities Announcement Protocol       {#fault-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Fault PSA

##Release Notes
**This feature is disabled in the 1.18 release and will be fixed in the next release**

### What does it do?

This protein is sent in response to a [Faults Request](fault-request.html) protein.  It identifies all entities that are currently in a user-defined "faulty" state.  The protocol for defining these states is currently disabled and will be properly re-instated in the next release

@image html faults_control.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, faults ]
i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - faults

**Ingests:**

none
  
### Sample

@include proteins/faults-psa.prot
