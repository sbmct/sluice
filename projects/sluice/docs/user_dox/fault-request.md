Request Faulty Entities Protocol       {#fault-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Fault Request

##Release Notes
**This feature is disabled in the 1.18 release and will be fixed in the next release**

### What does it do?

This protein is used to retrieve the list of all entities that are currently labeled as "faulty". The protocol for defining an entity as faulty is currently disabled and will be fixed in the next release. See the [Faulty Entities Announcement Protocol](fault-psa.html) for details on the protein returned by sluice.

@image html faults_control.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, faults ]
i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - faults

**Ingests:**

none
  
### Sample

@include proteins/faults-request.prot
