Atlas Fluoroscope Configuration Specification       {#atlas-fluoro-api}
===================

[Home](index.html) &rarr; [Fluoroscope Configuration Specification](fluoro-configuration-spec.html) &rarr; Atlas Configuration Specification

### What does it do?

This configuration provides sluice with all the background map image and tile set options for which a user can select and toggle between. Map images should be stored on the sluice machine and tile set names should match a tileset provided by the server.  By default, sluice provides one tile set which uses Mapquest's Open Street Maps.

### Slaw Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
{
  <a href="#fluoro-name">name</a>: Map,
  <a href="#fluoro-type">type</a>: atlas,
  <a href="#layers-path">layers-path</a>: <code>Str</code> <em>path_to_static_layers_protein</em>,
  <a href="#tile-server">tile-server</a>: <code>Str</code> <em>machine_name_hosting_tile_pools</em>,
  <a href="#attributes">attributes</a>:
      [
          {
              <a href="#attributes">name</a>: <code>Str</code> <em>unique_attribute_category</em>,
              <a href="#selection-type">selection-type</a>: <inclusive, exclusive>,
              <a href="#contents">contents</a>:
                  [
                      {
                          <a href="#cont-name">name</a>: <code>Str</code> <em>unique_list_entry</em>,
                          <a href="#cont-disp">display-text</a>: <code>Str</code> <em>text_to_be_used_in_web_interface</em>,
                          <a href="#cont-sel">selected</a>: <code>bool</code> <em>is_selected</em>
                      },
                      ...
                  ]
          },
          ...
      ]
}
</pre>

**Slaw::Map Format:**

  - <a name="fluoro-name"></a>name: Map

    The unique name identifier of this map fluoroscope. Currently, we only support the background map (ie. Atlas) so you can just set this as "Map."

  - <a name="fluoro-type"></a>type: atlas

    This notifies sluice of the type of fluoroscope that needs to be created and must be "atlas".

  - <a name="layers-path" />layers-path: [string]

    This field provides the path to where the static layers protein lives, which sets up the file paths and geographic bounds for static image layers that can be placed directly on top of the map.  See the [Static Layers Configuration page](static-layers.html) for information on its format.

  - <a name="tile-server" />tile-server: [string]

    This field allows users to set the machine address for the tile server. This defaults to "localhost".

  - <a name="fluoro-attr"></a>attributes: (slaw::list)

    [See below](#attributes)

<a name="attributes"></a>
### Attributes

**Overview**

  The list of attributes defines the various parts of a fluoroscope that can be configured or changed on the fly from the "fluoroscopes" tab in the web interface.  Each attribute then has a unique name, selection-type, and contents list to define that attribute. The map fluoroscope inherently supports three attributes. See the [Reserved Attributes](#attr-res) for details on these supported attributes.

  - name: (string)

    The unique name associated with the attribute. Some attributes names are reserved in sluice and map to specific meanings.  See the three different types of fluoroscopes for type-specific attributes that have been reserved.

  - <a name="selection-type"></a>selection-type: (string)

    This field denotes what form the attribute should take in menu form.  Currently, there are two options "exclusive" and "inclusive".  Exclusive means that one and only one item in the contents list should be selected at any given time. For instance, "opacity" is exclusive because the fluoroscope can't have both 25% and 100% opacity.  Inclusive means that none or multiple items in the contents list can be selected at any given time.  For instance, the attribute may be a filter on what entity types in a network should be displayed... so you can have everything deselected (and show nothing) or you can have a subset or all entities selected.  This field will likely increase in the future to support more selection types, like floating point ranges (opacity would become a floating point range to allow for all values from 0-100, vs the current form of only selecting a few options).

  - <a name="contents"></a>contents: (slaw::list)

    This list contains all the options for this attribute.  For instance, most default fluoroscopes come with four opacity options (25%, 50%, 75%, 100%).  Each list/option entry should have the following:

    - <a name="cont-name"></a>name: (string)

      This field should be regarded more as the value that is mapped to this entry.  While it needs to be a string, oftentimes it is used as a float or integer.  In the case of opacity, the names are typically '0.25', '0.50', '0.75', and '1.0' but as a reserved attribute type, sluice translates these strings into the appropriate floating point value to properly set the fluoroscope's opacity.

    - <a name="cont-disp"></a>display-text: (string)

      The human-readable display version of the name(value).  This value gets used by the fluoroscope tab in the web interface to display the options so that the user doesn't see '0.25',... but rather "25%",...

    - <a name="cont-sel"></a>selected: (bool)

      Denotes whether the item is selected or not.  If false/0, the item is deselected. If true/1, the item is selected.  In the case of an exclusive list, only one item in the contents list should evaluate to true. While in an inclusive list all to none may evaluate to true.

<a name="attr-res"></a>
### Reserved Attributes

**alpha-per-second** - Provides a menu-ing option to the user to allow them to set how long a tile should take to fade in when loading.

@code
name: alpha-per-second
selection-type: exclusive
contents:
- name: 1.0
  display-text: "One second fade in"
  selected: true
- name: 2.0
  display-text: "Half second fade in"
  selected: false
- name: 0.5
  display-text: "Two second fade in"
  selected: false
- name: 4.0
  display-text: "Quarter second fade in"
  selected: false
@endcode

**layers** - Provides a menu-ing option to allow the user to turn on/off static background layers, such as terrain. The "name" field for the contents' entries should match an entry in the ingests of the [static-layers](static-layers.html) protein.  Sluice uses this protein to reference where the static layer is saved on disk and exactly what lat/lon bounds the image's corners map to on the Atlas.

@code
name: layers
selection-type: inclusive
contents:
- name: us_terrain
  display-text: US Terrain
  selected: !i64 0
@endcode

**background** - Provides a menu-ing option to allow the user to change which map should be used as the background.  The map can be a static image in which case, the "name" field should contain the file path of the image or it can reference a tile set to be used, in which case the "name" field should use a unique name that the Tile Server knows how to serve up.  Currently, there is only one and it is "mapquest".

@code
name: background
selection-type: exclusive
contents:
- name: mapquest
  display-text: Mapquest
  selected: 1
- name: toner
  display-text: Toner
  selected: 0
- name: sluice/mercator/world-mercator-4096-04.png
  display-text: Matte dark
  selected: 0
- name: sluice/mercator/world-mercator-4096-03.png
  display-text: Shiny dark
  selected: 0
- name: sluice/mercator/blue-marble.png
  display-text: Blue Marble
  selected: 0
@endcode

### Sample

@include proteins/video-fluoro-config.prot
