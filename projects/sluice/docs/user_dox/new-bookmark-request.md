Add New Bookmark Protocol       {#new-bookmark-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request New Bookmark

### What does it do?

Use this protein to create a new bookmark for sluice to keep track of.  This protein can specifiy a lat/lon/zoom level for the bookmark, or if no location is provided, the bookmark will be created using the current view state. If the request succeeds, the [Bookmarks List PSA](bookmarks-psa.html) is sent. Upon error, an [error message](bookmark-error.html) is returned. A bookmark is a simple combination of a lat/lon location and a zoom level that can be used to quickly zoom the map view to a specific location.

@image html new_bookmark.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, new-bookmark ]
i:  {
        name: <code>Str</code> <em>bookmark_name</em>,
        uid: <code>Str</code> <em>unique_id_of_request</em>
    }

OR

d: [ sluice, prot-spec v1.0, request, new-bookmark ]
i:  {
        name: <code>Str</code> <em>bookmark_name</em>,
        uid: <code>Str</code> <em>unique_id_of_request</em>,
        lat: <code>float64</code> <em>latitude</em>,
        lon: <code>float64</code> <em>longitude</em>,
        level: <code>float64</code> <em>zoom_level</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - new-bookmark

**Ingests:**

  - name: [string]

    The name the new bookmark should be saved as

  - uid: [string]

    A unique identifier for this request so that in case of an error, it can be determined if the error is for this request.

  - lat: [float64]

    The latitudinal value of where this bookmark should center the view to. This field is optional.

  - lon: [float64]

    The longitudinal value of where this bookmark should center the view to. This field is optional.

  - level: [float64]

    The level of zoom the map should navigate to when this bookmark is selected. This field is optional.

### Sample

@include proteins/new-bookmark-request.prot
