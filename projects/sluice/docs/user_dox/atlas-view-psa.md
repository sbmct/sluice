Atlas View Announcement (PSA) Protocol       {#atlas-view-psa}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Atlas View PSA

### What does it do?

This protein is sent from sluice to inform external processes of the current map view, including the lat/lon intersection at the center of the view, the bounds of the entire lat/lon bounding area, and the current zoom level.  This protein is sent with every change of the map view as well as in response to a request for this view. See the [Atlas View Request Protocol](atlas-request.html) for details on forming a request.

@image html atlas_view_psa.svg

### Pool

sluice-to-edge

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, psa, atlas-view ]
i:  {
        lat_min: <code>float64</code> <em>minimum_latitude</em>,
        lon_min: <code>float64</code> <em>minimum_longitide</em>,
        lat_max: <code>float64</code> <em>maximum_latitude</em>,
        lon_max: <code>float64</code> <em>maximum_longitide</em>,
        lat_center: <code>float64</code> <em>center_latitude</em>,
        lon_center: <code>float64</code> <em>center_longitide</em>,
        level: <code>float64</code> <em>zoom_level</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - psa
  - atlas-view

**Ingests:**

- lat_min: [float64]

  The current bottom-most latitudinal line in the viewable map area.

- lon_min: [float64]

  The current left-most longitudinal line in the viewable map area.

- lat_max: [float64]

  The current top-most latitudinal line in the viewable map area.

- lon_max: [float64]

  The current right-most longitudinal line in the viewable map area.

- lat_center: [float64]

  The currently centered latitudinal line in the viewable map area.

- lon_center: [float64]

  The currently centered longitudinal line in the viewable map area.

- level: [float64]

  The current zoom level of the map view.

### Sample

@include proteins/atlas-view-psa.prot
