Request To Archive Data Protocol      {#curate-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Archive Data

### What does it do?

Use this protein to save an archive of topology data between a given time range.  Things that are saved include:

See the [Request To Load Archived Data Protocol](exhibit-request.html) for loading saved archives.

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, curate ]
i:  { filename: <code>Str</code> <em>filename</em>,
      display-name: <code>Str</code> <em>event_name</em>,
      begin: <code>float64</code> <em>beginning_timestamp</em>,
      end: <code>float64</code> <em>ending_timestamp</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - curate

**Ingests:**

  - filename: [string]

    The name to save this archive to. It should be unique and perhaps descriptive. This field is required.

  - display-name: [string]

    A more descriptive name to display for this archive.  This field is optional and if omitted will default to the filename.

  - begin: [float64]

    The timestamp that the archived data will begin at.  This timestamp maps to machine time and the number represents seconds elapsed since Jan 1, 1970.  This field is optional and if omitted will default to the earliest time known to sluice.

  - end: [float64]

    The timestamp that the archived data will end at.  This timestamp maps to machine time and the number represents seconds elapsed since Jan 1, 1970.  This field is optional and if omitted will default to the live time of sluice.

### Sample

@include proteins/curate-request.prot
