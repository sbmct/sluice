Move Fluoroscope Instance Protocol       {#move-fluoro}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Moveure Fluoroscope

### What does it do?

This protein moves a fluoroscope.

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, fluoroscopes, move, request, <em>fluoro_qid_slaw</em> ]
i:  {
        bounds:
          bl: vector! [<em>bottom lat</em>, <em>bottom lon</em>, <em>ignored</em>]
          tr: vector! [<em>top lat</em>, <em>top lon</em>, <em>ignored</em>]
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - fluoroscopes
  - move
  - request
  - <em>fluoro_qid_slaw</em>

**Ingests:**

  - bounds

A map containing two keys -- `bl` and `tr` -- that each point to [lat,lon,ignored] formatted v3float64's.

