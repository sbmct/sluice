Request Time Protocol       {#time-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Time

### What does it do?

Use this protein to get back from sluice the current view time, whether time is live or paused and the current rate of play. Look at the [Time Announcement Protocol](time-psa.html) for details on what Sluice sends in return.

@image html request_time.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, time ]
i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - time

**Ingests:**

none

### Sample

@include proteins/time-request.prot
