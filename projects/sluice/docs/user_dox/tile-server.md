Integrating with the Tile Server   {#tile-server}
=========================================

[Home](index.html) &rarr; Tile Server

### Overview

The Tile Server is a standalone daemon that provides sluice with background map tiles so that the background map appears to increase and decrease in resolution as a user zooms in and out of the map.  This server follows a generic protein API which sluice uses to request tiles on the fly.  You can read more about request/response API [here](tile-api.html).

### Running

@code
$ tiled -c [tile server configuration file]
@endcode

By default, the Tile Server will run with the configuration file:

`/opt/oblong/sluice-64-2/etc/tiled-config.protein`

See the page below on "How to configure the Tile Server" for information on the configuration protein format.

### Helpful pages

Below are links to some pages that are helpful for using the Tile Server to integrate new tile sets or ensure stability even when not connected to a network.

- [How to pre-cache tile sets](tile-caching.html)
- [How to configure the Tile Server](tile-config.html)