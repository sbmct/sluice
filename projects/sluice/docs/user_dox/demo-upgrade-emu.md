Demo Upgrade Guide (Emu 1.18)     {#demo-upgrade-emu}
=========================================

[Home](index.html) &rarr; [Installation Guides](installation-guides.html) &rarr; Demo Upgrade Guide 1.18

### Requirements

- Ubuntu 10.04 with tinywm
- tarball of g-speak, sluice, and dependency packages provided via [Basecamp](https://basecamp.com/)

  Folder should include:
    - 166 base debian packages
    - 3 sluice packages
      - sluice-web-1.6.0.deb
      - sluice-kipple-1.18.deb
      - sluice-1.18.0.deb
    - deploySluice.sh script

### Instructions

- Log in to the demo machine
  
  <code>$ ssh demo@[machine_name]</code>

- Copy the packages provided via Basecamp on to the machine and untar

  <code>$ scp sluice-1.18-pkgs.tgz ~/.<br>
  $ tar -xzvf sluice-1.18-pkgs.tgz</code>

- Install packages
  
  1. <code>$ cd sluice-1.18-pkgs</code>
  2. run script
    1. <code>$ ./deploySluice.sh</code>
    2. follow script instructions

- Update configurations to allow video streaming from sluice and set up your matrix switch properly for swapping between mezzanine and sluice
  1. edit ~/.xinitrc
    1. open ~/.xinitrc in your favorite editor
    2. add the line:<br>
      `xhost +`
  2. edit sluice startup script
    1. open /opt/oblong/sluice-64-2/bin/ctrl.sh in your favorite editor
    2. comment out line 8: `DAEMON_OPTS=""`
    3. uncomment line 10: `DAEMON_OPTS="-r ${TOPDIR}/etc/cthulhu.protein"`
  3. If the machine name of your mezzanine appliance is not 'mezzanine', then do the following:
    1. edit the cthulhu configuration protein
      1. open /opt/oblong/sluice-64-2/etc/cthulhu.protein in your favorite editor
      2. change 'mezzanine' in line 21, to the name or ip of your mezzanine appliance.
    2. edit the matrix switch settings
      1. open /opt/oblong/sluice-64-2/etc/swapper_settings.yaml
      2. change the 'mezzanine' in 'tcp://mezzanine/cephalopool' to the name or ip of your mezzanine appliance

- reboot machine

  <code>$ sudo shutdown -r now</code>

  Sluice should start up with the machine. Run through your demo as usual to find any missing resources or configurations.  These are backed up in the `sluice-1.8-pkgs/quarantine` folder.  Move any configuration files you need into the `/home/[username]/.oblong/etc` folder (you may have to create this folder) and move any resource files you need into the `/home/[username]/.oblong/share` folder, keeping the relative paths.  So, if you previously had a resource file tmp.png in `/kipple/sluice/symbols/tmp.png`, this should go in `/home/[username]/.oblong/share/sluice/symbols/tmp.png`.

### Install Paths

**g-speak libraries**

  /usr/lib

**g-speak executables**

  /usr/bin

**ob-http-ctl**

  /opt/oblong/jsplasma

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**sluice web**

  /opt/oblong/sluice-web

**resource files**

  /usr/share/oblong/sluice

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**screen and feld proteins**

  /usr/share/oblong

**logs**

  /var/log/oblong/sluice

### Release Notes

[See Here](emu-release-notes.html)