Request Fluoroscope Instances List Protocol       {#fluoro-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Fluoroscope Request

### What does it do?

Use this protein to get back from sluice the a list of all the fluoroscopes currently instantiated on the map view. See the [Fluoroscopes PSA Protocol](fluoro-list-psa.html) for more details on the response.

@image html fluoro_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, fluoroscopes ]
i: { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - fluoroscopes

**Ingests:**

none

### Sample

@include proteins/fluoro-request.prot
