Protein Format Documentation Key    {#protein-format}
=========================

### Symbols

d:  : Descrips

i:  : Ingests

{ } : Slaw::Map

[ ] : Slaw::List

( ) : Slaw::Cons

< > : The entries listed in-between these arrows are the set of allowable options for the given field.

... : Indicates multiple entries in a map or list.

### Fonts

DIN: All things in DIN font are to be taken as-is.

<code>Courier: Things in Courier font provide the datatype for the field </code>

<em>DIN Italic: Things in italic are descriptions of the variable to be placed in the given field</em>
