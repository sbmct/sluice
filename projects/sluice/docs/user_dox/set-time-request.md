Set Time Request Protocol       {#set-time-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Set Time

### What does it do?

Use this protein to change the current view time of sluice, set time to live, pause or change the rate of play.

@image html set_time_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
a) Set time to "live"
d: [ sluice, prot-spec v1.0, request, set-time ]
i:  { live: <code>bool</code> true }

OR
b) Pause/Play, Change Time, Change rate
d: [ sluice, prot-spec v1.0, request, set-time ]
i:  {
        time: <<code>Str</code> <em>timestamp</em>, <code>float64</code> <em>timestamp</em>>,
        rate: <code>float64</code> <em>play_rate</em>,
        pause: <code>bool</code> <em>should_pause_time</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - set-time

**Ingests:**

  - live: [bool]

    Use this field as a quick way to set sluice time to live (machine) time and play live. The value you send should always be true; if false, you should use the second protein format.

  - time: One of "YYYY-MM-DD HH:MM:SS POSIX_TIME_ZONE" or a float64 timestamp

    The time to set sluice to. If a float is provided, the number represents seconds elapsed since Jan 1, 1970. If a string is provided, sluice will try to parse it into a date/time based on the time formats defined in the [sluice-settings] (sluice-settings.html) protein read in at run-time.

  - rate: [float64]

    The play rate sluice should play at.  The value is given in terms of seconds per second and negative values have the effect of rewinding time.

  - pause: [bool]

    Use this value to toggle sluice between paused and playing.  A value of true will pause sluice and false will restart the play. If the value is not given, it defaults to true.

### Sample

@include proteins/set-time-request.prot
