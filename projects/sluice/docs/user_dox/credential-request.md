Request User Credentials Protocol       {#credential-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Request Credentials

### What does it do?

This protein is used to retrieve the current credentials/security token from sluice. See the [Credential Response Protocol](credential-response.html) for more information on the protein that will be returned through the sluice-to-edge pool.

@image html credentials_request.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, creds ]
i:  { }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - creds

**Ingests:**

none
  
### Sample

@include proteins/credentials-request.prot
