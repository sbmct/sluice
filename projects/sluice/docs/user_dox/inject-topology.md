Inject Topology Protocol       {#inject-topology}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Topology API](topology-api.html) &rarr; Inject Topology

### What does it do?

This protocol provides the ability to input geographical data sets into sluice to render internally and allow the user to interact with the data using [Network Fluoroscopes](network-fluoro-api.html). Sluice requires only that each data entry (ie. entity) has a lat/lon coordinate(s), a unique id, and a "kind" (specifying the class of entities to which it belongs).  Entities can be have a single lat/lon location (ie. a point) or multiple lat/lon locations (ie. a path). Meta data about each entity can also be sent along with the entity using the "attrs" field.

@image html inject_topology.svg

### Pool

topo-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, topology, add, <em>x/n</em> ]
i: {
        topology:
            [
                {
                    id: <code>Str</code> <em>entity_id</em>,
                    loc: <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                    kind: <code>Str</code> <em>entity_type</em>,
                    timestamp: <<code>Str</code> <em>timestamp</em>, <code>float64</code> <em>timestamp</em>>,
                    attrs:
                      {
                          <code>Str</code> <em>attr_1_id</em>: <code>Str</code> <em>attr_1_val</em>,
                          <code>Str</code> <em>attr_2_id</em>: <code>Str</code> <em>attr_2_val</em>,
                          ...
                      }
                },
                {
                    id: <code>Str</code> <em>entity_id</em>,
                    path:
                        [
                            <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                            <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                            <code>vect</code> [<em>lat</em>, <em>lon</em>, <em>elevation</em>],
                            ...
                        ],
                    kind: <code>Str</code> <em>entity_type</em>,
                    timestamp: <<code>Str</code> <em>timestamp</em>, <code>float64</code> <em>timestamp</em>>,
                    attrs:
                        {
                            <code>Str</code> <em>attr_1_id</em>: <code>Str</code> <em>attr_1_val</em>,
                            <code>Str</code> <em>attr_2_id</em>: <code>Str</code> <em>attr_2_val</em>,
                            ...
                        }
                }, ...
            ]
   }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - topology
  - add
  - *x/n*
  
     The final descrip is optional.  It helps inform sluice of the status of the loading network and display that status for the user. If the descrip is not given, the enclosed topology will load immediately.  Otherwise, sluice will display a loading graphic and wait until the nth/n protein comes in. For example, if you are sending the topology in 6 proteins,  you can set the last descrip for each to "1/6", "2/6", ..., "6/6."

**Ingests:**

  - id: [string]

    This string provides a unique identifier for the entity being added. For instance, if your network is licensed drivers, this would be the license number for a driver.

  - loc: [vect] || path: [list of vects]

    Entities can either be objects with a single point location or a path defined by multiple points. Single point entities have a "loc" field which takes a single vector that provides the lat, lon location of the entity being added.  The z value of the location vector is a placeholder for designating elevation, however, elevation is currently not supported by sluice. Multi-point located entities have a "path" field which takes a list of location vectors.

  - kind: [string]

    This field provides the entity's type or class. Using the license drivers example, this might be "motorcycle," "car," etc.

  - timestamp: One of "YYYY-MM-DD HH:MM:SS POSIX_TIME_ZONE" or a float64 timestamp

    Each attribute addition is associated with a timestamp so that a user can see when an entity comes online as they scan time. The timestamp maps to machine time. If a float is provided, the number represents seconds elapsed since Jan 1, 1970. If a string is provided, sluice will try to parse it into a date/time based on the time formats defined in the [sluice-settings] (sluice-settings.html) protein read in at run-time.  If no value is given, sluice will assign the current "live" machine time to the change.

  - attrs: [slaw map of strings to strings]

    The attrs mapping can provide both new attributes and updated values to existing attributes.  If an attribute id already exists for the entity, the value will be updated.  If an attribute id does not exist, then it is added to the set of attributes. All values in this map MUST BE strings.  If a float or integer is sent, sluice will not be able to convert it and the update will be ignored.

### Sample

@include proteins/topo-inject.prot
