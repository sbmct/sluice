Release Notes for Emu 1.18.0    {#emu-release-notes}
============================

[Home](index.html) &rarr; [Release Notes Home](release-notes.html) &rarr; Emu Release Notes

##### Contents:
 - [Patch Release Notes](#patches)
 - [New Features](#new-features)
 - [Bug Fixes](#bug-fixes)
 - [New Dependencies](#new-depends)
 - [Known Issues](#known-issues)
 - [Guides for Upgrading Existing Infrastructure](#upgrade-guides)
 - [Credits](#credits)

<a name="patches"></a>
### Patch Release Notes:

 - [1.18.2](release-notes-1182.html)
 - [1.18.3](release-notes-1183.html)

<a name="new-features"></a>
### New Features:

1.	Reworked the implementation of Fluoroscopes to better support geo-locationing, layering and pinning.
2.	Cleaned up startup scripts and increased their reliability
3.	Added documentation for ruby texture daemons
4.	Added ability to create new bookmarks on the fly, based on the current view of sluice using the web interface
5.	Increased the capacity for rendering a greater number of network entities while keeping a high frame rate
6.	Modularized file management to support client-specific resource and configuration files.  This will prevent client files from being overwritten on sluice upgrades.
7.	Implemented a new type of fluoroscope to render video from file or pool.
8.	Implemented support for a hardware configuration which uses a video matrix to swap between mezzanine and sluice. This feature includes a button on the admin tab of the web interface for performing the application swap and appropriately sending input channels to the live application.
9.	Increased the configuration possibilities for points and paths.
	- all entities can support an uncapped number of configurations and multiple configurations can be drawn at once per entity
  - paths can be configured to use texture which animate along the path
10.	Added support for use of doxygen for documentation purposes
11.	Moved sluice resource files from a folder titled "mapper" to a folder titled "sluice" which should be located in the ob_share_dir as determined by g-speak 
12.	Implemented map tiling such that sluice retrieves higher/lower resolution maps as the user zooms in/out
  -	This implementation allows clients to plug in their own map tiling server to provide their own maps.
	- By default, sluice will come with a tile set that is yet to be determined
13.	Added functionality to texture fluoroscopes so they retrieve higher/lower resolution textures as the user zooms in/out
14.	Implemented ability for sluice to stream felds to video pools and be shared with other applications or over teleconference in the same way as mezzanine. 
15.	Updated graphics and logic for calculating which network entity is considered "selected" 
16.	Implemented the ability to add static images to the map during run-time and a protein API for performing this functionality
17.	Implemented the ability to remove a single static image or clear all static images from the map during run-time and a protein API for performing this functionality
18.	Added proxy support to the embedded browser
19.	Implemented new interaction behavior for map navigation such that the action is more fluid.
20.	Updated sluice to build against g-speak v3.2
21.	Added "kind" to the ingests of an entity-clicked (or poked-it) protein API
22.	Added a protein API to allow clients to zoom to a location based on an entity id and give an optional zoom level
23.	Added a protein API for requesting the current view of sluice (including the lat/lon viewing bounds and the zoom level)
24.	Expanded the entity-clicked (or poked-it) protein API to send notifications any time a user clicks on the map (not just when a network entity is clicked).  This API includes the lat/lon location of the click.
25.	Implemented a protein API for a heartbeat generated by sluice.  This heartbeat includes the time information (current time, the play rate of time (seconds/second), whether time is paused, and whether time is live) as well as the current view of sluice (lat/lon bounds and zoom level)
26.	Implemented what we call "zoom level disambiguation," which in effect collapses groupings of entities that detail overlap and become indiscernible as the user zooms out, displaying a new graphic in their stead.  As the user zooms in and detail is discernible again, the entities reappear.
27.	Built in basic token handling, storage, and accounting into sluice and the entire protein API to support security requirements.  In this implementation, one user takes responsibility for the entire system and all actions are associated with his/her login credentials/token.  It is assumed that listening applications will respond (or refuse to respond) according to the user's token. This feature is toggleable, so login and tokens are not required to run sluice.
28.	Added a protein API for instantaneously creating new fluoroscopes on the map at specific lat/lon bounds.  This API allows clients to either instantiate a new fluoroscope instance by sending the name of an existing template or sending an entire unique configuration.  If no lat/lon bounds are given, the fluoroscope instance will appear covering the lat/lon bounds of whatever area is currently encompassing the center screen.
29.	Implemented the capability for creating a "windshield" fluoroscope which acts as a fluoroscope that remains in screen space and does not move or scale with the map, but updates accordingly as the user navigates the map beneath the fluoroscope.
30.	Added UX control and a protein API which allows for clients to drag an instance of a fluoroscope around the room.  A png image of the visible area of the fluoroscope along with its lat/lon bounds are provided for client-side applications to react to; i.e. imagine dragging a snapshot of the current weather to a running laptop in the room or to a table display.
31.	Fixed logging procedures for all processes spawned by sluice to follow proper logging and cleanup behaviors
32.	Cleaned up install/upgrade procedure to be seamless and require minimal instruction
33.	Implemented a feature which allows a user to easily scale and position a fluoroscope to fit (or snap) to an fill a screen, much like windshield objects can do in mezzanine.

<a name="bug-fixes"></a>
### Bug Fixes

1.	Fixed a memory leak in the embedded web browser
2.	Fixed exoskeletons of remote videos not properly conforming to the size of the video.
3.	Fixed sluice ignoring sluice topology updates
4.	Fixed sluice ignoring the removal of a single entity from the topology.
5.	Fixed sluice screen inconsistently appearing when logging in to web interface

<a name="new-depends"></a>
### New Dependencies:

1.	g-speak 3.2
2.	libboost-thread-1.40.0

<a name="known-issues"></a>
### Known Issues

1. Faults do not work. We have temporarily removed the tab from the web interface for showing entities that are "faulted."  We are working on updating this concept to be more widely usable and configurable.
2. Contents inside fluoroscopes jitter while moving/scaling the fluoroscope.  This is particularly more noticeable when using the web or ios for control.
3. Remote video shared via MzReach may not initially display at the proper aspect ratio. This wil be fixed with an upcoming g-speak release and for now you can resolve the by simply resizing the video.
4. Fluoroscopes are given sizing limits on creation based upon how zoomed in on the map you are.  So it can happen that you create a fluoroscope while zoomed in and then upon zooming out, you are unable to scale the fluoroscope to be very big.

<a name="upgrade-guides"></a>
### Transition Helpers

[Network Fluoroscope Upgrade Guide](emu-network-upgrade.html)

[Texture Fluoroscope Upgrade Guide](emu-texture-upgrade.html)

[Protein API Upgrade Guide](emu-plasma-api-upgrade.html)

<a name="credits"></a>
### Credits

Map Tiles are provided by:

  - [Mapquest OSM](http://developer.mapquest.com/web/products/open/map). Data by [OpenStreetMap](http://openstreetmap.org), under [CC BY SA](http://creativecommons.org/licenses/by-sa/3.0).
  - [Staman Design](http://stamen.com), under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0). Data by [OpenStreetMap](http://openstreetmap.org), under [CC BY SA](http://creativecommons.org/licenses/by-sa/3.0).