Fluoroscope Protocol           {#fluoroscope-api}
=============

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; Fluoroscope Communication API

@image html fluoro_api.svg

sluice-to-fluoro proteins
- [Texture Request] (tex-request.html)
- [Video Request] (vid-request.html)
- [Texture Request Passthru (Experimental)] (store-forward.html)

fluoro-to-sluice proteins
- [Texture Response](tex-response.html)
- [Video Response] (vid-response.html)

sluice-to-store proteins
- [Texture Data Passthru Request (Experimental)](store-request.html)

