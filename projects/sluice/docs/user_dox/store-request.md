Texture Data Passthru Request (Experimental)       {#store-request}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Fluoroscope API](fluoroscope-api.html) &rarr; Texture Response


### What does it do?

Sends a request to the datastore to forward data from a particular geolocation/time to another pool.  The forwarded data can be used to generate a texture for a texture fluoroscope.  There are 2 different formats to forward data.  One format, V1, is is the same as [Texture Request](tex-request.html) but can only forward data for a single attribue.  The new format, V2, is not backwards compatible, but can send values for multiple attributes.


### Pool

sluice-to-store to send the pass through request

typically forward texture requests to sluice-to-fluoro


### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

#### V1

<pre>
d: [ sluice, prot-spec v1.0, texture-data, pass]
i:  {
        kinds: {
            kinds:
                [ <code>Str</code> <em>kind</em>, ... ],
            attr: <code>Str</code> <em>attribute_name</em>,
        },
        bl: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        tr: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        output: {
            pool: sluice-to-fluoro,
            descrips: [sluice, prot-spec v1.0, request, texture, fluoro_qid_slaw (slaw::cons ("SlawedQID": byte array))],
            ingests: <code>slaw::map</code> <em>ingests to forward</em>
        }
    }
</pre>

#### V2

<pre>
d: [ conduce, prot-2.0, data, pass]
i:  {
        kinds: [
            {
                kind: <code>Str</code> <em>kind</em>
                attrs: [<code>Str</code> <em>attribute_name</em>, ...]
            },
            ...
        ],
        bl: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        tr: [ <code>float64</code> <em>lat</em>, <code>float64</code> <em>lon</em> ],
        output: {
            pool: sluice-to-fluoro,
            descrips: [conduce, prot-2.0, request, texture, fluoro_qid_slaw (slaw::cons ("SlawedQID": byte array))],
            ingests: <code>slaw::map</code> <em>ingests to forward</em>
        }
    }
</pre>


**Descrips:**

#### V1:

  - sluice
  - prot-spec v1.0
  - texture-data
  - pass

#### V2:

  - conduce
  - prot-2.0
  - data
  - pass


**Ingests:**

  - bl, tr (slaw::list of two float64 values)

    The combination of these two list of values describes the bounding rectangle in geospatial (lat/lon) coordinates of the area being sent.

  - pool (string) (under <em>output</em> key)

    The pool where the datastore will forward data.  For texture fluoroscopes, this should be the same pool as [Texture Requests](tex-request.html).

  - descrips (slaw::list) (under <em>output</em> key)

    The descrips the datastore will forward.  For texture fluoroscopes, this should be the same descrips as [Texture Requests](tex-request.html).

  - ingests (slaw::map) (under output key)

    The ingests the datastore will forward.  For texture fluoroscopes, this should be the same descrips as [Texture Requests](tex-request.html).


### Samples

#### V1

@include proteins/store_request_v1.prot

#### V2

@include proteins/store_request_v2.prot

