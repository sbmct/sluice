Tile Response From Server Protocol       {#tile-response}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Tile Server Communication API](tile-api.html) &rarr; Tile Response

### What does it do?

Sluice listens for this type of protein to fill in the background map. Sluice implements the [Pyramid Coordinate System](http://www.maptiler.org/google-maps-coordinates-tile-bounds-projection/) tiling protocol for defining tile bounds. This is the only protein protocol in Sluice that does not go through a consistent pool.  The pool is defined from within sluice and is sent along as part of the request. See the [Tile Request Protocol](tile-request.html) for details on how sluice requests these tiles. For further documentation on the Tile Server and things like how to pre-cache tile sets, view the [Tile Server documentation](tile-server.html).

@image html tile_api.svg

### Pool

tiles-resp-<em>unique_pool_id</em>

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ tile-response ]
i:  {
        map-id: <code>Str</code> <em>map_id</em>,
        x: <code>int64</code> <em>x_index</em>,
        y: <code>int64</code> <em>y_index</em>,
        z: <code>int64</code> <em>z_index</em>,
        bytes: <code>unt8_array</code> <em>image_bytes</em>,
        image-type: <code>Str</code> <png, jpeg>
    }
</pre>

**Descrips:**

  - tile-request

**Ingests:**

  - map-id: [string]

    Identifies which map tile set was used to generate the tile. By default, Sluice installs with "mapquest" and "toner"

  - x, y, z: [int64]

    Identifies the index of the tile (location and resolution) that is being requested.  This coordinate system is fully described [here](https://developers.google.com/maps/documentation/javascript/v2/overlays#Google_Maps_Coordinates).

  - bytes: [unsigned byte array]

    This array contains the full image data of the tile being sent.

  - image-type: <png, jpeg>

    The image format the above array is sent in.  Currently only png and jpegs are supported.

### Sample

@include proteins/tile-response.prot
