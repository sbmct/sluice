Configure Fluoroscope Instance Protocol       {#config-fluoro}
===================

[Home](index.html) &rarr; [Plasma API](sluice-plasma-api.html) &rarr; [Edge Device API](edge-api.html) &rarr; Configure Fluoroscope

### What does it do?

This protein is used to change the configurations of an instantiated fluoroscope on the map. For instance, the fluoroscope tab on the web app uses this protocol to change weather from showing IR Satellite to showing 24 hour precipitation. Upon success, sluice will send a [Fluoroscope Update PSA](fluoro-update-psa.html) with the new complete configuration.  To see this protein, along with the [fluoroscope list request](fluoro-request.html) and [fluoroscope list psa](fluoro-list-psa.html) in action, watch the edge-to-sluice and sluice-to-edge pools while you navigate to the fluoroscope tab on the web browser and change the settings of a fluoroscope.

@image html config_fluoro.svg

### Pool

edge-to-sluice

### Protein Format
<div id="legendlink">[legend](protein-format.html)</div>

<pre>
d: [ sluice, prot-spec v1.0, request, configure-fluoro, <em>fluoro_qid_slaw</em> ]
i:  {
        SlawedQID: <code>unt8_array</code> <em>unique_id</em>,
        <em>*See the <a href="fluoro-configuration-spec.html">Fluoroscope Configuration Protein Spec</a> for documentation on the rest of this map</em>
    }
</pre>

**Descrips:**

  - sluice
  - prot-spec v1.0
  - request
  - configure-fluoro

**Ingests:**

  - SlawedQID: [unt8 byte array]

    The qid value (or unique id) of the fluoroscope which this request is wanting to update.  This id can be retrieved using the [fluoroscope list request](fluoro-request.html).

  - *fluoroscope configuration (slaw::map)

    The ingests of this protein map directly to the fluoroscope configuration protocol.  Take a look at the [Fluoroscope Configuration Protein Spec](fluoro-configuration-spec.html) for details on what all may be in this map.  It is important that you send the entire configuration back rather than just the changed attributes to ensure that no configurations are lost.

### Sample

@include proteins/config-fluoro.prot
