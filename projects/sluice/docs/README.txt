*** running sluice

    Usage: sluice [options] 
      -h, --help             print this help, then exit
      -s, --sluiceconfig     sluice global configuration file
      -r, --register         <path to cthulhu registration protein> 

    * -s
      by default, the application will look for the sluiceconfig, named
      "sluice-settings.protein" in first the defined ob_etc paths, then resort
      to looking in the local directory for an etc/sluice-settings.protein
      *See below for the proper configuration file format
    * -r
      by default sluice does not register with cthulhu, but if it does then a
      sluice configuration protein file is expected in-line. If no file is
      given, then a run-time failure will occur.
      *See bolow for the proper registration file format

*** configuration protein file formats

  sluice configuration protein:
    Example:
      %YAML 1.1
      %TAG ! tag:oblong.com,2009:slaw/
      --- !protein
      descrips:
      - app-settings
      - sluice
      ingests:
        map: "map.protein"
        scopes:
          - "financials.protein"
          - "analysis.protein"
          - "ge_network.protein"
          - "weather.protein"
        time-formats: # See strptime(3)
          - "%Y-%m-%dT%H:%M:%SZ"
          - "%Y-%m-%d %H:%M:%S"
        windshield-max-items: 50
        # Note: the fields below are not currently used but may be in the future
        video-max-instances: 10
        min-free-disk-space: 50
        viddle-event-pools:
          - viddle-name: dvi-capture-12A
            event-pool: "tcp://localhost/dvi-evts"
          - viddle-name: dvi-capture-52A
            event-pool: "tcp://localhost/dvi-evts"
        viddle-display-names:
          - viddle-name: dvi-capture-12A
            display-name: "DVI 1"
          - viddle-name: dvi-capture-52A
            display-name: "DVI 2"
      ...

    Protein Descrips:
      A list of two strings, ["app-settings", "sluice"]
    Protein Ingests:
      A map with the following keys, each of which is described below:
        * "map": Str filename_of_map_template
                 # the filename of the template for the default configuration of
                 # the background map. the file is expected to be found in the
                 # ob_etc path or in a local etc folder
                 # DEFAULTS IF MISSING TO "map.protein"
        * "scopes": Slaw::List (Str fluoroscope_template_1_filename, ...,
                                Str fluoroscope_template_n_filename)
                 # a list of the filenames for the fluoroscope templates that
                 # should be loaded on start of the application. These files are
                 # expected to be found in the ob_etc path or in a local etc
                 # folder
                 # DEFAULTS IF MISSING TO: Slaw::List ("weather.protein",
                 #                                     "financials.protein")
        * "time-formats": Slaw::List (Str strptime_format_1, ...,
                                      Str strptime_format_n)
                 # a list of time formats sluice should expect and parse. These
                 # format strings should adhere to the same strptime expects
                 # DEFAULTS IF MISSING TO: "%Y-%m-%d %H:%M:%S"
        * "windshield-max-items": int64 num
                 # an integer setting to define the maximum number of all items
                 # allowed on the windshield at a single time.
                 # DEFAULTS IF MISSING TO: 101
        * "video-max-instances": int64 num
                 # the maximum number of video instances allowed at a single
                 # time (not including the thumbnails)
                 # NOTE: THIS FIELD IS CURRENTLY NOT IN USE
                 # DEFAULTS IF MISSING TO: 16
        * "min-free-disk-space": int64 num
                 # the minimum amount of space sluice should always ensure is
                 # left on disk before trying to save any new files. This
                 # number should be in terms of GIGABYTES.
                 # NOTE: THIS FIELD IS CURRENTLY NOT IN USE
                 # DEFAULTS IF MISSING TO: 50 GIGABYTES
        * "viddle-event-pools": Slaw::List (
                                  Slaw::Map (
                                    "viddle-name": Str logical_video_name,
                                    "event-pool": Str reachthrough_pool_name))
                  # This field informs sluice of the pools it should use to send
                  # reachthrough events to (event-pool) for a specified video
                  # (viddle-name).
                  # NOTE: THIS FIELD IS CURRENTLY NOT IN USE
                  # DEFAULTS IF MISSING TO: Slaw::Null ()
        * "viddle-display-names": Slaw::List (
                                    Slaw::Map (
                                      "viddle-name": Str logical_video_name,
                                      "display-name": Str onscreen_display_name))
                  # This field informs sluice of a more user-friendly display
                  # name to put on screen (display-name) for specified videos
                  # (viddle-name)
                  #NOTE: THIS FIELD IS CURRENTLY NOT IN USE
                  # DEFAULTS IF MISSING TO: Slaw::Null ()

  cthulhu configuration protein:
    Example:
      %YAML 1.1
      %TAG ! tag:oblong.com,2009:slaw/
      --- !protein
      descrips:
      - sluice
      - config
      - viddles
      ingests:
        cthulhu-machine: mezzanine
        viddles:
          main: dvi-capture-91A
          left: dvi-capture-91B
          right: dvi-capture-74A
      ...

    Protein descrips:
      Slaw::List ("sluice", "config", "viddles")
      #NOTE: This will soon be outdated and the new descrips should be:
        # Slaw::List ("cthulhu", "config")
    Protein ingests:
      Slaw::Map with the following keys/values
        # (description of each key is commented inline)
        * "cthulhu-machine": Str ip_or_machine_name
                  # This field should designate the ip or machine name of the
                  # machine running ob-cthulhu
                  # NO DEFAULT: Program exits if field is not found
                  # NOTE: in a future version of this format, you will designate
                  #       the full poolaname of cthulhu coordination rather than
                  #       just the machine and it will look like so:
                  #       "cthulhu-coord-pool": Str pool_name
        * "viddles": Slaw::Map (Str feldname: Str logical_video_name)
                  # This sets up a mapping to where each screen's output is
                  # being piped to.  In the example above, these are the logical
                  # video names for input pools.  Theoretically, however this
                  # could be anything that listening scripts/application may
                  # need for swapping the pixels over, whether it's via westar
                  # cards or a video switcher
                  # NO DEFAULTS: Currently, if left, main and right are not
                  #              found, the program will exit. However, in the
                  #              future format, this entire field will not be
                  #              required
        # For full disclosure of the the format to come, the following key will
        # also be expected:
          * "application-id": Str id
                    # this fiels will set up the id by which the running sluice
                    # should register to cthulhu as.  Currently, this is hard-
                    # coded as "sluice" but in future will be read in here.
                    # DEFAULT IF MISSING TO: UrDrome::Name ()
