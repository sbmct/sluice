## Mouse and Keyboard operation

### Control keys:

- *f*: Pushback (show Trenton)
- *g*: End Pushback (hide Trenton)
- *d*: Delete the Fluoroscope that the mouse is currently hovering over
- *s*: Swich HandiPoint intent to the next mode of moving the map, moving Fluoroscopes and passthrough
- *a*: Swich HandiPoint intent to the previous mode of moving the map, moving Fluoroscopes and passthrough
- *z*: Enable "displacement mode" for the mouse
- *q*: Disable "displacement mode" for the mouse

### Mouse

- *mouse wheel*: Zoom the Fluoroscope that the mouse is currently hovering over.  (Zoom the main map if the mouse is not hovering over a Fluoroscope.)


 
