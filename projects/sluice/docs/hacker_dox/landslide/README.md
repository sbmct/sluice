To generate slideshow.html, we use landslide, which converts from simple Markdown to an HTML page containing all the slides. The slideshow can be printed to paper or PDF using operating system facilities; or landslide has the capability to generate PDF directly.

1. install landslide (https://github.com/adamzap/landslide)
2. run landslide with the current, simple configurations file

          $ landslide config.cfg

This will generate slideshow.html.  This html file expects to find the 'oblate' folder nearby, for CSS and fonts.

