#:: **cObbler** ::<br/>composable documentation

---

a brndn+**nick** joint

<!-- brandon@oblong.com and nmasso@oblong.com -->
<!-- June 2012 -->

---
# the task: technical docs for the SnOE project

- audience:
    - new Aramco subcontractors
    - new and current Aramco engineers
    - future Obs
- content:
    - codebase
    - protein protocols, key algorithms, background information
    - building, benchmarking, testing, and other meta
    - expressive diagrams as appropriate
    - slide decks for instruction purposes

---
# bugbears

- bloated 'authoring' tools
    - GBs to install, minutes to boot
    - InDesign/InCopy work<strike>flow</strike> stoppage
    - tools don't xfer among machines; skills don't xfer among tools
- divisions between disciplines => reified through tools  :-\
- opaque, incommensurable binary formats
    - how to store? index? import, export, convert, update?
- consistent style?
- **result**: zip file of binary 'dark matter'

---
# deeper question: **use**

<br/>
<br/>

## how do we use this? how will they use this?

.notes: the meaning of a word is its use in the language -- wittgenstein, philosophical investigations

.notes: if we had to name anything which is the life of the sign, we should have to say that it was its use" -- wittgenstein, blue book

.notes: questions to ask about documentation: is it composable? does it translate? does it self-describe? what's the failure mode? is binary worth it?

---
# the dream

1. low-impedance input: edits from every team member, any platform
    - even graphics
2. low-impedance output: paper, 4 screens, web, any platform
    - online or offline
3. a designed thing
    - coherent document package, one point of entry
4. **composable**
    - build with simple, swappable, shareable pieces

.notes: we want documentation units to be easy to recombine, even at the smallest grain size

.notes: we want documentation activities to be part of actual day-to-day development workflow, not a detour through an "authoring" tool


---
# low-impedance input format to rule them all: **text**

- every tool, every editor, every platform, every era
    - failure mode: excellent
- we use git; so let's use it -- *work with the grain*
    - diff + merge
    - no binary clutter
    - docs never divorced from code
- long legacy of text-manipulation == **a long lever**
    - visible to Spotlight, grep, search engines: no 'dark matter'

.notes: PDF (for example) is not greppable out of the box

---
# all forms of writing: Markdown

- yield control over:
    - **flow** (no right-justified images)
    - **layout** (no two-column layouts)
    - ppt-fu
- but:
    - full HTML/CSS available
    - tables
    - *focus*
- failure mode: clear and readable text
- wikis, blogs, email, code comments, slide decks, GitHub, BaseCamp, etc.

.notes: as soon as we make a document in MS Word, contents won't export cleanly to ANY of those

---
# graphics:  SVG

- XML, aka, **text**
    - ✓ grep
    - ✓ spotlight**
    - ✓ web (including **our own wiki**)
- most browsers ready
- output from OmniGraffle, Illustrator, InkScape, etc.

---

<br/>
<br/>
<br/>
<center>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="50px" height="86px" viewBox="0 0 50 86" enable-background="new 0 0 50 86" xml:space="preserve">
<path fill="#000000" d="M25.538,85.926h-1.108C10.998,85.926,0,75.414,0,61.96V23.952C0,10.386,10.998,0,24.429,0h1.108
    C39.115,0,50,10.386,50,23.952v37.886C50,75.414,39.115,85.926,25.538,85.926"/>
<circle cx="216.257" cy="940.384" r="6"/>
<circle cx="266.569" cy="872" r="6"/>
<circle cx="367.181" cy="872" r="6"/>
<circle cx="316.873" cy="1008.771" r="6"/>
<circle cx="467.799" cy="872" r="6"/>
<circle fill="none" stroke="#000000" stroke-miterlimit="10" cx="367.181" cy="940.386" r="6"/>
<circle fill="none" stroke="#000000" stroke-miterlimit="10" cx="516.104" cy="940.384" r="6"/>
<circle cx="266.569" cy="940.384" r="6"/>
</svg>

</center>

---
# ===

---

<pre>
< ?xml version="1.0" encoding="utf-8"?>
< !DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
< svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="50px" height="86px" viewBox="0 0 50 86" enable-background="new 0 0 50 86" xml:space="preserve">
< path fill="#000000" d="M25.538,85.926h-1.108C10.998,85.926,0,75.414,0,61.96V23.952C0,10.386,10.998,0,24.429,0h1.108
    C39.115,0,50,10.386,50,23.952v37.886C50,75.414,39.115,85.926,25.538,85.926"/>
< circle cx="216.257" cy="940.384" r="6"/>
< circle cx="266.569" cy="872" r="6"/>
< circle cx="367.181" cy="872" r="6"/>
<  circle cx="316.873" cy="1008.771" r="6"/>
< circle cx="467.799" cy="872" r="6"/>
< circle fill="none" stroke="#000000" stroke-miterlimit="10" cx="367.181" cy="940.386" r="6"/>
< circle fill="none" stroke="#000000" stroke-miterlimit="10" cx="516.104" cy="940.384" r="6"/>
< circle cx="266.569" cy="940.384" r="6"/>
< /svg>
</pre>

---
# low-impedance output

What are the decent and universal formats for viewing *technical* content?

- PDF
- paper
- browser
- plain text
- ODF
- ?

<p>&nbsp;</p>

Rosetta stone: browser content

.notes: 2 billion devices can view HTML; converts to PDF, paper, or plain text

---
# deliverable

1. a doxygen site

.notes: show LayerSelectionController and gquery

.notes: styled with CSS and structured with some XML files

---
# deliverable

1. a doxygen site
2. articles

.notes: Markdown rendered into HTML pages within the site (shares CSS)

---
# deliverable

1. a doxygen site
2. articles
3. graphics


.notes: show gesture manual

.notes: SVG graphics linked into the site (also shares CSS)

---
# deliverable

1. a doxygen site
2. articles
3. graphics
4. slides (landslide) -- could try hekyll

<pre>
#:: **cObbler** ::<br/>composable documentation
---
a brndn+**nick** joint
---
# the task: technical docs for the SnOE project

- audience:
    - new Aramco subcontractors
    - new and current Aramco engineers
    - future Obs
- content:
</pre>

.notes: show table of contents

---
# it's all in the repo

- C++, Ruby, & extras

- .md documents

- SVG & a few PNGs

- Doxygen setup: settings file, CSS & fonts, XML for structure

- landslide setup: settings file, CSS & fonts

---
# the dream (recap)

1. low-impedance input: **Markdown**

2. low-impedance output: **web**

3. coherent design

4. composable:
    - freely mix & match content from <br/> **{** prose | manuals | code comments | simple tables | wiki content | email ... **}**<br/>and even paste SVG inline

---
# scorecard

- +
    - we're proud of it
    - refactor code + docs **+ graphics** in one shot: hey, this is kind of fun
    - actual workflow

- -
    - CSS other madness of the world wide web
    - doxygen madness

---
# using cObbler in your own project

documentation starter kit:

     git:/ob/git/repo/cobbler.git

- clone repo into your project
- strip out its .git folder
- add rest of the contents to your own repo
- comes with starter site & examples


---
# hail nick
