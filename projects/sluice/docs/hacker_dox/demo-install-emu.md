Demo Install Guide (Emu 1.18)     {#demo-install-emu}
=========================================

[Home](index.html) &rarr; [Installation Guides](int-installation-guides.html) &rarr; Demo Install Guide 1.18

### Requirements

- R5400 or R5500 with Ubuntu 10.04 installed: See this page for machine, space, and network requirements
- working internet connection
- mezzanine or perception appliance for wands
- tarball of new install files
- tarball of g-speak, sluice, and dependency packages

  Folder should include:
    - 166 base debian packages
    - 3 sluice packages
      - sluice-web-1.6.0.deb
      - sluice-kipple-1.18.deb
      - sluice-1.18.0.deb
    - deploySluice.sh script

### Instructions

- Log in to the demo machine
  
  <code>$ ssh demo@[machine_name]</code>

- change hostname to sluice
  1. edit /etc/hostname to contain just "sluice"
  2. edit /etc/hosts to have all non-localhost references to 127.0.0.1 
say "sluice"
  3. reboot

- copy new demo station tarball on to the machine and untar

  <code>$ scp sluice-1.18-demo.tgz ~/.<br>
  $ tar -xzvf sluice-1.18-demo.tgz</code>

- move xorg.conf to /etc/X11/xorg.conf

  <code>$ mv sluice-1.18-demo/xorg.conf /etc/X11/.</code>

- install tinywm

  <code>$ sudo apt-get install tinywm</code>

- move tinywm configuration to /etc/init

  <code>$ cp sluice-1.18-demo/tinywm.conf /etc/init/.</code>

- move install_files directory to $HOME

  <code>$ mv sluice-1.18-demo/install_files ~/.</code>

- move .xinitrc to $HOME

  <code>$ mv sluice-1.18-demo/.xinitrc ~/.</code>

- disable GDM

  <code>$ mv /etc/init/gdm.conf /etc/init/gdm.conf.DISABLED</code>

- enable tinywm
  - edit /etc/X11/default-display-manager to replace "/usr/sbin/gdm" with "/usr/bin/tinywm"

- set X permissions
  - change the "allowed_users" in /etc/X11/Xwrapper from "console" to 
"anybody"

- setup screens and felds

  1. copy screens and felds into share folder

    <code>$ cp sluice-1.18-demo/screen.protein /etc/oblong/.<br>
    $ cp sluice-1.18-demo/feld.protein /etc/oblong/.</code>
  2. edit screens and felds to match installation setup

- Copy the sluice 1.18 pkgs tarball to the machine

  <code>$ scp sluice-1.18-pkgs.tgz ~/.<br>
  $ tar -xzvf sluice-1.18-pkgs.tgz</code>

- Install packages
  
  1. <code>$ cd sluice-1.18-pkgs</code>
  2. run script
    1. <code>$ sudo ./deploySluice.sh</code>
    2. follow script instructions

- If running with mezzanine:
  - Update configurations to allow video streaming from sluice and set up your matrix switch properly for swapping between mezzanine and sluice
    1. edit sluice startup script
      1. open /opt/oblong/sluice-64-2/bin/ctrl.sh in your favorite editor
      2. comment out line 8: `DAEMON_OPTS=""`
      3. uncomment line 10: `DAEMON_OPTS="-r ${TOPDIR}/etc/cthulhu.protein"`
    2. If the machine name of your mezzanine appliance is not 'mezzanine', then do the following:
      1. edit the cthulhu configuration protein
        1. open /opt/oblong/sluice-64-2/etc/cthulhu.protein in your favorite editor
        2. change 'mezzanine' in line 21, to the name or ip of your mezzanine appliance.
      2. edit the matrix switch settings
        1. open /opt/oblong/sluice-64-2/etc/swapper_settings.yaml
        2. change the 'mezzanine' in 'tcp://mezzanine/cephalopool' to the name or ip of your mezzanine appliance
        3. If the switch is not configured such that mezzanine runs its screens (left to right) on inputs 1-4 and sluice runs its screens (left to right) on inputs 5-8:
          - change the slaw::list of commands to use the correct inputs, where each entry in the list is of the format

            <code>"r [output] [input]\r"</code>

        **IMPORTANT**: If your setup is not using a Gefen 16x16 matrix, please contact client solutions to determine the appropriate commands to insert in your swapper_settings.protein
    3. edit the startup scripts on the mezzanine appliance:
      - If mezzanine 1.0 or 1.6:
        1. `$ ssh mezzanine@[mezzanine]`
        2. edit `/etc/init.d/siemcy`
          1. comment out line 30: `DAEMON_OPTS="--dossiers-dir=$DOSSIERS_DIR --with-corkboard"`
          2. add the following lines after line 30:

            <code>#Use the following when running with cthulhu
            DAEMON_OPTS="--dossiers-dir=$DOSSIERS_DIR --with-corkboard -r"</code>
        3. copy the ob-cthulhu startup script to `/etc/init.d/siemcy`

          <code>$ scp demo@sluice:~/sluice-1.18-demo/ob-cthulhu /etc/init.d/.</code>
        4. create pool for cthulhu communication

          <code>$ p-create cephalopool</code>
      - If mezzanine 2.0:
        1. `$ ssh mezzanine@[mezzanine]`
        2. edit `/etc/init.d/siemcy`
          1. comment out line 30: `DAEMON_OPTS="--dossiers-dir=$DOSSIERS_DIR --with-corkboard"`
          2. uncomment out line 32: `#DAEMON_OPTS="--dossiers-dir=$DOSSIERS_DIR --with-corkboard -r $OB_CONF/cthulhu.protein"`
        3. set `etc/init.d/ob-cthulhu` to run on startup

           <code>$ sudo update-rc.d ob-cthulhu start 30 2 3 4 5 . stop 70 0 1 6 .</code>

        4. create pool for cthulhu communication

           <code>$ sudo p-create cephalopool</code>
- If not running with mezzanine:
  - Update sluice configuration to use the wands pool on the machine where the pipeline is running
    - edit `/opt/oblong/sluice-64-2/etc/sluice-settings.protein` and insert the following line before the "map" field (line 8) with appropriate spacing:

      <code>wands-pool: tcp://[perc-appliance-ip/hostname]/wands</code>

- reboot machine

  <code>$ sudo shutdown -r now</code>

  Sluice should start up with the machine.

### Install Paths

**g-speak libraries**

  /usr/lib

**g-speak executables**

  /usr/bin

**ob-http-ctl**

  /opt/oblong/jsplasma

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**sluice web**

  /opt/oblong/sluice-web

**resource files**

  /usr/share/oblong/sluice

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**screen and feld proteins**

  /usr/share/oblong

**logs**

  /var/log/oblong/sluice

### Release Notes

[See Here](emu-release-notes.html)