Sluice Documentation                      {#index}
==============
This site is dedicated to the frontiersmen (users and developers) of sluice.

### Users

[Native Interface User Guide](native-user-guide.html)

[Web Interface User Guide](web-user-guide.html)

[IOS Pointer User Guide](ios-pointer-user-guide.html)

[MzReach User Guide](mzreach-user-guide.html)

[Mouse and Keyboard Control for workstation development](mouse-and-keyboard.html)

### Developers

#### Configuration

[Fluoroscope Configuration Specifications](fluoro-configuration-spec.html) :: This page provides the base format of a fluoroscope configuration protein and links to the specifications of the various types of fluoroscopes you may configure (network, texture, and video).

[Running Sluice with Custom Configurations](running-sluice.html) :: This page provides information on changing sluice's default configurations to use custom-defined configurations and information on running sluice and it's various processes manually for testing.

[File Paths](file-paths.html) :: This page provides an overview of the default path/filesystem for sluice and where users should save resource and configuration files on the machine to be used by sluice.

[Data Configuration](data-config.html) :: This page provides an overview of how to configure Sluice to interpret your data

#### Integration

[Plasma API](sluice-plasma-api.html) :: This page links to sluice's plasma API with descriptions and examples of every protein interface used to interact with sluice and the pools that are used for each.

[Building Custom Fluoroscope Daemons](fluoro-daemons.html) :: This page links to how-to's and code examples for building your own custom processes that integrate with sluice and populate the pixels of fluoroscopes.

[Web Modules](web-modules.html) :: This page provides a how-to for adding your own tabs and web pages to the sluice web interface.

[Expanding on the Tile Server](tile-server.html) :: This page provides helpful information on using and configuring the provided tile server.

#### Installation

[Installation Guides](int-installation-guides.html) :: This page links to install and upgrade procedures for developer stations and demonstration areas.

[Release Notes](release-notes.html) :: This page contains links to the release notes for each release along with helpful guides for upgrading protein formats to integrate with the latest APIs and Configuration Specs.

@htmlonly

<script>
var ua = $.browser;
if ( ua.msie )
  {
    $('.header').append('<div id="browser-warning-big"><marquee behavior="scroll" direction="left">warning: this website is optimized for Google Chrome. Internet Explorer is not supported.</marquee></div>');
  }
</script>
@endhtmlonly