Gesture User Guide                        {#gesture-guide}
=================

<!--
  Author note: ************** readme ***********
  Note that the vertical pipe character in the gripe strings below is not the normal vertical pipe character, which is used, instead, as the table delimiter character.  In the gripe strings below we use a unicode character ǀ, also known as U+01C0 or the "Latin Letter Dental Click".
-->

### General

Gesture Name  |  Hand Pose  |  Gripes  |  description
------------- | ----------- | -------- | --------------
one finger pointing | <img src="gesture-icons-10.svg" width=125 /> | [ .^^ǀ– : –x ] | handipoint on triptych
one finger pointing harden | <img src="gesture-icons-19.svg" width=125 /> | [ .^^ǀǀ : –x ] | select item onscreen
three axis flying | <img src="gesture-icons-13.svg" width=125 /> | [ .^xǀ– : –x ] | displacement gesture for 3d navigation
hitchhiker | <img src="gesture-icons-05.svg" width=125 /> | [ .^^^– : -^ ] | reset the camera view back to the starting view

### Another Menu

Gesture Name  |  Hand Pose  |  Gripes  |  description
------------- | ----------- | -------- | --------------
hitchhiker | <img src="gesture-icons-05.svg" width=125 /> | [ .^^^– : -^ ] | exit menu
one finger pointing harden | <img src="gesture-icons-19.svg" width=125 /> | [ .^^ǀǀ : –x ] | select object for editing
