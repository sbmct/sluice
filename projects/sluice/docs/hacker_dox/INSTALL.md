# Sluice installation via DEB

## Install dependency DEB's

    sudo dpk -i gst-plugins-bad_0.10.22-1_amd64.deb \
                gst-plugins-base_0.10.35-1_amd64.deb \
                gst-plugins-good_0.10.30-1_amd64.deb \
                gstreamer_0.10.35-1_amd64.deb \
                gstreamer_0.10.35-1_amd64.deb \
                gtest_1.5.0-1_amd64.deb \
                orc_0.4.14-1_amd64.deb


## Install g-speak

You'll need to force the installation of dependencies for the g-speak package.

    sudo dpkg -i g-speak_2.4.0-2_amd64.deb
    sudo apt-get install -f
    sudo dpkg -i g-speak_2.4.0-2_amd64.deb

## Install Sluice

You'll also need to force dependency installation for Sluice

    sudo dpkg -i sluice-1.0.0.deb
    sudo apt-get install -f
    sudo dpkg -i sluice-1.0.0.deb

The Sluice installer will print a warning if you do not have the rmagic gem installed.  To install it by hand:

    sudo gem install rmagick

## Pools

Sluice provides a script to ensure that the proper pools are created.  It is called `create-pools.sh`.  You can run it as follows:

    /opt/oblong/sluice-64-2/bin/create-pools.sh

Be sure to run it as the user you intend to run Sluice as.  (Currently this is assumed to be: demo.)


