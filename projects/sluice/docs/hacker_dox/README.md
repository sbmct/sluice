cObbler documentation starter kit
=================================
<pre>
Authors: Brandon Harvey <brandon@oblong.com>
         Nick Masso <nmasso@oblong.com></pre>

To use cObbler in your own project:

  1. move the contents into the project in a subdirectory (e.g. docs/)

  2. strip out the .git folder

  3. add the rest of the content to your own repo

To generate doxygen documentation, see [generating-doxygen-documentation.md](generating-doxygen-documentation.html)

