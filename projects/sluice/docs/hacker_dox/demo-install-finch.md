Demo Install Guide (Finch 1.20)     {#demo-install-finch}
=========================================

[Home](index.html) &rarr; [Installation Guides](int-installation-guides.html) &rarr; Demo Install Guide 1.20

### Requirements

- R5400 or R5500 with Ubuntu 12.04 installed: See this page for machine, space, and network requirements
- working internet connection
- mezzanine or perception appliance for wands
- tarball of new install files
- tarball of g-speak, sluice, and dependency packages

  Folder should include:
    - 248 base debian packages
 	  - 5 ruby gems
    - 3 sluice packages
      - sluice-web-1.8.0.deb
      - sluice-kipple-1.20.0.deb
      - sluice-1.20.0.deb
    - deploySluice.sh script

### Instructions

- Log in to the demo machine
  
  <code>$ ssh demo@[machine_name]</code>

- change hostname to sluice
  1. edit /etc/hostname to contain just "sluice"
  2. edit /etc/hosts to have all non-localhost references to 127.0.0.1 
say "sluice"
  3. reboot

- copy new demo station tarball on to the machine and untar

  <code>$ scp git.oblong.com:/ob/dumper/sluice-packages/1.20/sluice-new-install.tgz ~/.<br>
  $ tar -xzvf sluice-new-install.tgz</code>

- move xorg.conf to /etc/X11/xorg.conf

  <code>$ mv sluice-new-install/xorg.conf /etc/X11/.</code>

- install tinywm

  <code>$ sudo apt-get install tinywm</code>

- move tinywm configuration to /etc/init

  <code>$ cp sluice-new-install/tinywm.conf /etc/init/.</code>

- move install_files directory to $HOME

  <code>$ mv sluice-new-install/install_files ~/.</code>

- move .xinitrc to $HOME

  <code>$ mv sluice-new-install/.xinitrc ~/.</code>

- disable GDM

  <code>$ mv /etc/init/lightdm.conf /etc/init/lightdm.conf.DISABLED</code>

- enable tinywm
  - edit /etc/X11/default-display-manager to replace "/usr/sbin/lightdm" with "/usr/bin/tinywm"

- set X permissions
  - change the "allowed_users" in /etc/X11/Xwrapper.config from "console" to 
"anybody"

- setup screens and felds

  1. copy screens and felds into share folder

    <code>$ cp sluice-new-install/screen.protein /etc/oblong/.<br>
    $ cp sluice-new-install/feld.protein /etc/oblong/.</code>
  2. edit screens and felds to match installation setup

- Copy the sluice 1.20 pkgs tarball to the machine

  <code>$ scp git.oblong.com:/ob/dumper/sluice-packages/1.20/sluice-1.20.pkgs.tgz ~/.<br>
  $ tar -xzvf sluice-1.20.pkgs.tgz</code>

- Install packages
  
  1. <code>$ cd sluice-1.20-pkgs</code>
  2. run script
    1. <code>$ sudo ./deploySluice.sh</code>
    2. follow script instructions

- Setup wands
	1. Update sluice configuration to use the wands pool on the machine where the pipeline is running
  	- edit `/opt/oblong/sluice-64-2/etc/sluice-settings.protein` and insert the following line before the "map" field (line 8) with appropriate spacing:

      <code>wands-pool: tcp://[perc-appliance-ip/hostname]/wands</code>

- Setup matrix switch
  1. edit the matrix switch settings
     1. open /opt/oblong/sluice-64-2/etc/swapper_settings.yaml
     2. If the switch is not configured such that mezzanine runs its screens (left to right) on inputs 1-4 and sluice runs its screens (left to right) on inputs 5-8:
          - change the slaw::list of commands to use the correct inputs, where each entry in the list is of the format

            <code>"r [output] [input]\r"</code>

        **IMPORTANT**: If your setup is not using a Gefen 16x16 matrix, please contact client solutions to determine the appropriate commands to insert in your swapper_settings.protein

- reboot machine

  <code>$ sudo shutdown -r now</code>

  Sluice should start up with the machine.

### Install Paths

**g-speak libraries**

  /opt/oblong/g-speak3.4

**g-speak executables**

  /opt/oblong/g-speak3.4/bin

**ob-http-ctl**

  /opt/oblong/jsplasma

**sluice**

  /opt/oblong/sluice-64-2/bin

**fluoroscope daemons and miscellaneous sluice processes**

  /opt/oblong/sluice-64-2/bin

**sluice web**

  /opt/oblong/sluice-web

**resource files**

  /usr/share/oblong/sluice

**default sluice configurations**

  /opt/oblong/sluice-64-2/etc

**screen and feld proteins**

  /usr/share/oblong

**logs**

  /var/log/oblong/sluice

### Release Notes

[See Here](finch-release-notes.html)