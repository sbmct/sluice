Documentation Guide                  {#documentation-guide}
===================

@tableofcontents

This guide describes how to document code and create separate documentation for inclusion in this Doxygen-generated documentation base. This is not an inclusive guide of Doxygen's many features, but the set that we have found to work well together. Some of this is dependent is also dependent on the Doxyfile included in this project. If you begin editing that parts of this guide can become inaccurate, all bets are off, your warranty expires just like that time you tried to fix your macbook pro's lcd screen.

**Markdown** can be used in the many documentation forms shown below, with some extensions and limitations thanks to Doxygen. For a reference on Markdown in Doxygen, see [The Official Markdown-Doxygen Guide](http://www.stack.nl/~dimitri/doxygen/markdown.html).

### Commenting Code

Comments placed in the source code will be included by Doxygen if they are in the correct style. Double slash comments (//) are **not** included in Doxygen's output. All comments should go in the header file where the entity is declared.

#### Single Line Comment

A single line comment is done in the following style. Place it before an entity's declaration to have it automatically be attached to its documentation.

@code
/// a comment about this class
class myClassyClass {

...more code...
// comments like this won't be included in documentation
...more code...

}
@endcode

#### Multi-line Comment
For more detailed descriptions use the following form:

@code
/** a blurb about this class

	more details about how classy your class is
	and an important detail you'd be negligent not to mention
*/
class myClassyClass {

// ...more code...
}
@endcode

The first line of your multi-line comment will be used as the brief description of this entity. Or if you use a period, that will end the brief description. Whichever comes first will be used in each case. All of the following text of the comment will be parsed for the detailed description.

### Commenting Code with Great Detail

These are special commands in the Doxygen system to make your code documentation more robust and descriptive. It will utilize special formatting in the output to highlight these important details.

#### Detailed Method Documentation

You can provide descriptions for specific method variables with the following form:

@code
@param[{in,out}] {parameter name} {parameter description}
@endcode

You can also provide a comment on what the method returns with:

@code
@returns {description}
@endcode

For example:

@code
  /** checks to see if a new menu entry gesture has been held long enough.
      checks the wordstamp of your event to determine if its new or not, and 
      whether its already being timed
      @param[in] e the current ObEvent, generally an OEPointingMoveEvent
      @returns the answer to your question, boolean style
  */
  bool TimeToChange (ObEvent *e);
@endcode

#### Code Maintenance Commenting

Place this tag in a member comment to mark it as deprecated. Providing an explanatory message is optional, but probably a good idea. Doxygen will also generate a page that lists all members that are marked as deprecated, so you have a handy, centralized list.

@code
\@deprecated {message}
@endcode

And this tage can be used for todo messages. Doxygen collects these into a centralized location for you too.

@code
\@todo {message}
@endcode

For example:

@code
/** toggles a layer's visibility

@deprecated this method is too simple to be useful
@todo remove this, sooner than later
*/
void ToggleLayer(int layerId);
@endcode

#### Placement & Whitespace

Comments should be placed on the line before the entity they describe. Leave one line of whitespace before each comment for readability's sake. You can place a comment after a member's declaration, but is only advisable when doing so will keep the code easier to read. For example:

@code
struct LayerCorners
  { int dim_1;            	///< number of points in one dimension
    int dim_2;            	///< and in the other, always ordered as i,j,k
    SliceType layer_type; 	///< what kind of layer
    int layer_index;      	///< which layer
    int ref_cnt;          	///< not a true ref count, just how many more times has a show been requested than a hide so we know when we can throw this out

    Vect *corners;

    // ...more code...
  };
@endcode

### General Documentation

You can also create documentation that isn't attached to a particular piece of code. The page you are reading right now is one example. To make a new page create a new Markdown file, like page-name.md. The general form is like this:

@code
My Page Title  		{#page-name}
===============

Content you write in **Markdown.** You can also use <strong>html</strong>.

## Another Section

More carefully explained explanations laid down with loving care at your electric typewriter, late into the night.

@endcode

The first line of the document will be used as the title of your page. The string enclosed in {# } will be used as the filename for the generated page. Naming the .md file the same as this string helps keep the namespaces simple.

#### Linking to Other Pages

Instead of using Doxygen linking (\@ref), we simply use hyperlinking via Markdown. It looks like this:

@code [Structured Grid Geometry](geometry-overview.html) : How the data of the structured grid model is represented within SnOE
@endcode

This links to the page generated by geometry-overview.md, since all the html files are thrown into the same directory.