Installation Guides     {#int-installation-guides}
===================

[Home](index.html) &rarr; Installation Guides

### Installations

**Demo Stations**
  - [1.20 Finch](demo-install-finch.html)
  - [1.18 Emu](demo-install-emu.html)

**Developer Workstations**

  - [1.18 Emu](workstation-install-emu.html)

### Upgrades

**Demo Stations**

  - [1.20 Finch](demo-upgrade-finch.html)
  - [1.18.3 Emu](demo-upgrade-1183.html)
  - [1.18 Emu](demo-upgrade-emu.html)

**Developer Workstations**

  - [1.20 Finch](workstation-upgrade-finch.html)
  - [1.18.3 Emu](workstation-upgrade-1183.html)
  - [1.18 Emu](workstation-upgrade-emu.html)

**Operating System Upgrades**

  - [Ubuntu 12.04](ubuntu-upgrade-1204.html)
