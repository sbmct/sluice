Generating the Documentation Site                {#generating-doxygen-documentation}
============

This documentation is generated using [Doxygen](http://www.doxygen.org), an open source tool. Doxygen extracts documentation from C++ source file comments, as well as from other, hand-written documents written in [Markdown format](http://daringfireball.net/projects/markdown/) (such as the one you are reading).

We used **Doxygen version 1.8.1.1**, and this site *will only generate correctly if you use the same (or a newer) version*. Doxygen has occasionally undergone large functional changes with only minor version number updates.

Installing Doxygen
------------------

### Mac OS X

We have had good experiences using [Homebrew](http://mxcl.github.com/homebrew/) to install Doxygen and also [Graphviz](http://www.graphviz.org/), which helps to generate graph images. Having installed Homebrew, installation looks like this:

    $ brew install doxygen
    $ brew install graphviz

Macports is also a viable option. Or see the [Doxygen Install Instructions](http://www.stack.nl/~dimitri/doxygen/install.html) for other options.

### Ubuntu

Depending on Ubuntu version, the correct version of Doxygen may not be available through apt-get.  We recommend consulting the [Doxygen Install Instructions](http://www.stack.nl/~dimitri/doxygen/install.html).


Running Doxygen
---------------
Starting in the **docs/** (or **cobbler/**) directory, simply run doxygen with no arguments.

    $ doxygen

The entire mini-site will be generated in the directory ./output/. The site front page will be found at

     ./output/html/index.html

