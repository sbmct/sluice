
/* (c)  oblong industries */

#ifndef CARDIAC_PULSE_AND_ITS_FASCINATIN_RHYTHMS
#define CARDIAC_PULSE_AND_ITS_FASCINATIN_RHYTHMS

#include <libBasement/TimedTrigger.h>

namespace oblong {
namespace sluice {
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class CardiacPulse : public TimedTrigger {
    PATELLA_SUBCLASS (CardiacPulse, TimedTrigger);

public:
    Str audience;
    //  Str flux_name;

public:
    CardiacPulse (float32 period);
    virtual ~CardiacPulse ();

    const Str &Audience () const { return audience; }
    void SetAudience (const Str &aud) { audience = aud; }

    /*
      const Str &FluxName ()  const
        { return flux_name; }
      void SetFluxName (const Str &fnm)
        { flux_name = fnm; }
    */

    virtual ObRetort Fire (KneeObject *target, Atmosphere *atm);
};
}
} // the ol' ticker finally stops for namespaces sluice and oblong.

#endif
