// -*- mode: c++ -*-

#pragma once

#include <libLoam/c++/ObMap.h>
#include <libNoodoo/VFImageRecorder.h>

#include "Hasselhoff.h"

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::plasma;
using namespace oblong::basement;
using namespace oblong::noodoo;

class Screenshotter : public NestyThing {
    PATELLA_SUBCLASS (Screenshotter, NestyThing);

protected:
    ObRef<Hasselhoff *> hoff;
    ObMap<Str, VFImageRecorder *> recorders;

    VFImageRecorder *RecorderForFeld (Str feld_name);
    void CaptureFeld (Str feld_name, Str req_name);

    ObRetort ProcessImage (ObRef<ImageClot *>, VisiFeld *vf, const Str &);
    ObRetort PreProcessImage (const Str &);

public:
    Screenshotter (Hasselhoff *hoff);
    ObRetort MetabolizeScreenshotRequest (const Protein &, Atmosphere *);
};
}
}
