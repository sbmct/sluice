
/* (c)  oblong industries */

#ifndef VIDFLUX_BROWSER_TRICKS
#define VIDFLUX_BROWSER_TRICKS

#include "BuffedAcetate.h"
#include "VidfluxImp.h"
#include "VideoThing.h"
#include "MzHandi.h"

#include <libNoodoo/TexQuad.h>
#include <libNoodoo/VidQuad.h>
#include <libNoodoo/VertexForm.h>

#include <projects/quartermaster/ViddleSet.h>

namespace oblong {
namespace sluice {
using namespace oblong::quartermaster;
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * VidfluxBrowser shows thumbnails of available DVI feeds for devices connected
 * to the Mezzanine appliance. The thumbnails are updated regularly but not
 * played as regular videos. Pilots can add video to Slides in the working Deck
 * by dragging the video thumbnails from the VidfluxBrowser onto the Slides in
 * Trenton, the work area.
 *
 * In version 1.0, there are 4 video feeds.
 */
class VidfluxBrowser : public BuffedAcetate, public MzHandi::Evts {
    PATELLA_SUBCLASS (VidfluxBrowser, BuffedAcetate);

public:
    ObTrove<VidfluxImp *> vimps;
    ObTrove<Str> vimps_image_path;
    ViddleSet *vimp_dowser;
    int32 dom_capacity;
    float64 gap_fract, inset_fract;

    // Perhaps these should be managed elsewhere ... but then
    // we'd need another ViddleSet and another object to listen
    // for viddle appearing and vanishing. Would that work?
    ObTrove<VideoThing *, WeakRef> instantiated_videos;
    ObMap<Str, Str> viddle_event_pool_map;

    // Mapping of viddle name to display name
    ObMap<Str, Str> viddle_display_names;

    ObRef<VideoPassthroughManager *> passthrough_manager;

public:
    VidfluxBrowser ();
    ~VidfluxBrowser ();

    virtual void InitiallyUnfurl (Atmosphere *atm);

    virtual void ArrangeDenizens ();

    virtual void InitializePoolRegistration (Hasselhoff *hoff);

    ImageClot *VidfluxImpBackgroundImage (int64 index);

    VideoPassthroughManager *PassthroughManager () {
        return ~passthrough_manager;
    }

    bool IgnoreViddle (Viddle *v);
    VidfluxImp *FindImpByViddle (Viddle *v) const;
    VidfluxImp *FindImpAtPosition (Vect origin, Vect through) const;
    VidfluxImp *LeftmostFreeImp () const { return FindImpByViddle (NULL); }
    VidfluxImp *NthImp (const int64 &n);

    VideoThing *NewVideoThingForImp (VidfluxImp *vimp);
    VideoThing *NewVideoThingForViddle (Viddle *viddle);
    VideoThing *NewVideoThingForViddleName (const Str &name);

    Viddle *FindViddleByName (const Str &name);

    ObTrove<VideoThing *> *FindVideoThingsByTag (int64 tag) const;
    ObTrove<VideoThing *> *FindVideoThingsByViddle (Viddle *viddle) const;
    ObTrove<VideoThing *> *FindVideoThingsByViddleName (Str viddleName) const;
    VideoSource *FindInstantiatedVideoSource (Viddle *viddle) const;

    ObRetort AppendViddleEventAssoc (const Str &vdl_name, const Str &ev_pool);

    Slaw ViddleEventAssocsDigest () const;

    ObRetort HandleViddleAssociation (const Protein &p, Atmosphere *atm);
    ObRetort AssociateViddleWithEventPool (Str viddleName, Str eventPool);
    ObRetort DisassociateViddleWithEventPool (Str viddleName);

    ObRetort SetViddleDisplayName (Str viddleName, Str displayName);
    Str GetViddleDisplayName (Str viddleName);
    ObRetort HandleViddleDisplayName (const Protein &p, Atmosphere *atm);

    ObRetort
    ReceiveVidfluxThumbnail (ViddleSet *vs, Viddle *v, Atmosphere *atm);
    ObRetort HandleViddleLive (ViddleSet *vs, Viddle *v, Atmosphere *atm);
    ObRetort HandleViddleDied (ViddleSet *vs, Viddle *v, Atmosphere *atm);

    ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
};
}
} // Bye bye namespaces sluice, oblong

#endif
