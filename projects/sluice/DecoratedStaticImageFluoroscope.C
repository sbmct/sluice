
#include "SluiceConsts.h"
#include "DecoratedStaticImageFluoroscope.h"
#include "ConfigSlawStuff.h"
#include "Node.h"
#include "SluiceSettings.h"

#include <boost/foreach.hpp>

using namespace oblong::sluice;

Slaw DecoratedStaticImageFluoroscope::ReadAttributes (const Slaw s) {
    Slaw attrs, attr;
    Str name, typ;
    Slaw target = Slaw::Map ();
    attrs = s.MapFind ("attributes");
    for (int64 ai = 0; ai < attrs.Count (); ++ai) {
        attr = attrs.Nth (ai);
        if (attr.MapFind ("selection-type").Into (typ) &&
            attr.MapFind ("name").Into (name)) {
            ObTrove<Slaw> tmp_trove;
            ReadAttributeValues (attr, tmp_trove);
            Slaw tmp = Slaw::List ();
            for (int64 ci = 0; ci < tmp_trove.Count (); ++ci) {
                Slaw citem = tmp_trove.Nth (ci);
                tmp = tmp.ListAppend (citem);
            }
            target = target.MapPut (name, tmp);
        }
    }
    return target;
}

ObRetort
DecoratedStaticImageFluoroscope::SetConfigurationSlaw (const Slaw config) {
    ObRetort tort = super::SetConfigurationSlaw (config);
    if (tort.IsSplend ()) {
        Str new_format = "disable";
        new_format = ReadAttribute (config, "enable-datastore", new_format)
                         ? new_format
                         : "disable";
        SetFlexibleRequestProteins (new_format == "v2");
    }
    return tort;
}

void DecoratedStaticImageFluoroscope::SendTextureRequestToPool (Protein &prot) {
    Str hose;
    if (SluiceSettings::SharedSettings ()->QueryUsesExternalDataStore ()) {
        hose = SluiceConsts::StoreRequestLogicalHoseName ();
    } else {
        hose = SluiceConsts::FluoroRequestLogicalHoseName ();
    }
    SendToPool (prot, hose);
}

ObRetort
DecoratedStaticImageFluoroscope::RequestProteinDeprecated (Protein &prot) {
    ObRetort tort = super::RequestProtein (prot);
    if (tort.IsSplend ()) {
        if (!~node_store)
            return OB_INVALID_ARGUMENT;

        GeoRect bounds;
        VisibleBounds (bounds);

        NodeStore *store = ~node_store;
        Slaw conf = ConfigurationSlaw ();
        ObTrove<Str> series;
        ObTrove<Str> node_types;
        if (ReadAttribute (conf, "entity-attributes", series) &&
            ReadAttribute (conf, "entity-kinds", node_types)) {
            Slaw data = Slaw::List ();

            bool reads_all = false;
            ReadAttribute (conf, "all-entities", reads_all);

            int64 dotwidth = 0;
            ReadAttribute (conf, "dotwidth", dotwidth);

            Str style;
            if (!ReadAttribute (conf, "style", style))
                style = "red";

            // ok, cheesy: for now just get the most recent timestamp
            // TODO: make this suck less
            const float64 when = store->SluiceTime ();

            std::set<Node *> geo;
            float64 vis_deg;
            if (GlobalVisibleDegrees (vis_deg).IsSplend ()) {
                store->FindByLatLonRange (bounds.bl, bounds.tr, vis_deg, geo);
            }

            Str val, sname;
            BOOST_FOREACH (Node *n, geo) {
                if (!n->IsAvailableForTime (when)) {
                    continue;
                }
                if (!reads_all) {
                    if (-1 == node_types.Find (n->Kind ())) {
                        continue;
                    }
                }

                ObCrawl<Str> scr = series.Crawl ();
                while (!scr.isempty ()) {
                    sname = scr.popfore ();
                    ObRetort tort = n->ObservationAtTime (sname, when, val);
                    if (!tort.IsSplend ()) {
                        continue;
                    }
                    Slaw datum = Slaw::Map ("id", n->Ident (), "val", val);
                    if (n->HasGeoloc (when)) {
                        LatLon ll = n->Geoloc (when);
                        datum = datum.MapPut ("loc",
                                              Slaw::List (ll.lat, ll.lon, 0.0));
                    }

                    ObTrove<LatLon> pth;
                    if (n->GetPath (pth, when)) {
                        Slaw path;
                        for (const LatLon &ll : pth) {
                            Slaw pat_s = Slaw::List (ll.lat, ll.lon, 0.0);
                            path = path.ListAppend (pat_s);
                        }
                        datum = datum.MapPut ("path", path);
                    }

                    data = data.ListAppend (datum);
                }
            }

            Slaw config_values = ReadAttributes (conf);

            Slaw ingests = prot.Ingests ();
            ingests = ingests.MapPut ("style", style)
                          .MapPut ("dotwidth", dotwidth)
                          .MapPut ("zoom", GetMapZoom ())
                          .MapPut ("tilezoom", GetTileZoom ())
                          .MapPut ("attributes", config_values)
                          .MapPut ("data", data);

            prot = Protein (prot.Descrips (), ingests);
        } else
            return OB_NOTHING_TO_DO;
    }
    return tort;
}

ObRetort
DecoratedStaticImageFluoroscope::RequestProteinDatastore (Protein &prot) {
    ObRetort tort = super::RequestProtein (prot);
    if (tort.IsSplend ()) {
        if (!~node_store)
            return OB_INVALID_ARGUMENT;

        GeoRect bounds;
        VisibleBounds (bounds);

        Slaw conf = ConfigurationSlaw ();
        ObTrove<Str> series;
        ObTrove<Str> node_types;
        if (ReadAttribute (conf, "entity-attributes", series) &&
            ReadAttribute (conf, "entity-kinds", node_types)) {
            Slaw descrips;
            Slaw set_descrips = prot.Descrips ();
            if (QueryFlexibleRequestProteins ()) {
                descrips = Slaw::List ("conduce", "prot-2.0", "data", "pass");
                // Still need to keep other descrips like SlawedQID and Daemon
                // name
                set_descrips = set_descrips.ListRemoveFirst (Slaw ("sluice"));
                set_descrips =
                    set_descrips.ListRemoveFirst (Slaw ("prot-spec v1.0"));
                set_descrips = set_descrips.ListPrepend ("prot-2.0");
                set_descrips = set_descrips.ListPrepend ("conduce");
            } else {
                descrips = Slaw::List ("sluice", "prot-spec v1.0",
                                       "texture-data", "pass");
            }

            // TODO: Support format in config for multiple attributes on
            // multiple kinds
            // TODO: Support multiple attributes, for now look for a single
            // attribute
            // and use it for all kinds
            Str sname;
            ObCrawl<Str> scr = series.Crawl ();
            if (scr.isempty ()) {
                return OB_NOTHING_TO_DO;
            }

            Slaw kind_info = Slaw::List ();
            if (QueryFlexibleRequestProteins ()) {
                // Real *all* the attributes
                Slaw attrs_list = Slaw::List ();
                while (!scr.isempty ()) {
                    attrs_list = attrs_list.ListAppend (scr.popfore ());
                }
                kind_info = Slaw::List ();
                ObCrawl<Str> kindscr = node_types.Crawl ();
                while (!kindscr.isempty ()) {
                    Slaw kind_map = Slaw::Map ("kind", kindscr.popfore (),
                                               "attrs", attrs_list);
                    kind_info = kind_info.ListAppend (kind_map);
                }
            } else {
                // Read the attr
                sname = scr.popfore ();
                // Get the list of kinds
                Slaw kinds_list = Slaw::List ();
                ObCrawl<Str> kindscr = node_types.Crawl ();
                while (!kindscr.isempty ()) {
                    kinds_list = kinds_list.ListAppend (kindscr.popfore ());
                }
                kind_info = Slaw::Map ("attr", sname, "kinds", kinds_list);
            }

            Slaw set_ingests = prot.Ingests ();

            // include dotwidth and style for v1 fluoroscopes only
            if (!QueryFlexibleRequestProteins ()) {
                Str style;
                if (!ReadAttribute (conf, "style", style)) {
                    style = "red";
                }
                set_ingests = set_ingests.MapPut ("style", style);

                int64 dotwidth = 0;
                ReadAttribute (conf, "dotwidth", dotwidth);
                set_ingests = set_ingests.MapPut ("dotwidth", dotwidth);
            }

            // for v2 fluoroscopes, include passthrough
            if (QueryFlexibleRequestProteins ()) {
                set_ingests = set_ingests.MapPut ("passthrough",
                                                  conf.MapFind ("passthrough"));
            }

            // for all requests include config
            Slaw config_values = ReadAttributes (conf);
            set_ingests = set_ingests.MapPut ("attributes", config_values);

            // Add zoom
            set_ingests = set_ingests.MapPut ("zoom", GetMapZoom ());
            set_ingests = set_ingests.MapPut ("tilezoom", GetTileZoom ());

            Slaw output_info =
                Slaw::Map ("pool", "sluice-to-fluoro", "descrips", set_descrips,
                           "ingests", set_ingests);

            // TODO: Get timestamp
            Slaw ingests = Slaw::Map (
                "kinds", kind_info, "bl", set_ingests.Find ("bl"), "tr",
                set_ingests.Find ("tr"), "output", output_info);

            prot = Protein (descrips, ingests);
        } else
            return OB_NOTHING_TO_DO;
    }
    return tort;
}

ObRetort DecoratedStaticImageFluoroscope::RequestProtein (Protein &prot) {
    if (SluiceSettings::SharedSettings ()->QueryUsesExternalDataStore ()) {
        return RequestProteinDatastore (prot);
    } else {
        return RequestProteinDeprecated (prot);
    }
}
