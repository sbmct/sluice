
/* (c)  oblong industries */

#ifndef SLUICE_NAVIGATION_EVENT
#define SLUICE_NAVIGATION_EVENT

#include <libImpetus/OEPointing.h>

#include <libGanglia/ElectricalEvent.h>

namespace oblong {
namespace sluice {
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class MzNavigateEvent : public ElectricalEvent {
    PATELLA_SUBCLASS (MzNavigateEvent, ElectricalEvent);
    OE_MISCY_MAKER (MzNavigate, Electrical, "navigate");

public:
    Vect handi_point_loc, prev_handi_point_loc, orig_handi_point_loc;

public:
    MzNavigateEvent (KneeObject *orig_ko = NULL) : ElectricalEvent (orig_ko) {}

    const Vect &Location () const;
    const Vect &PreviousLocation () const;
    const Vect &OriginalLocation () const;

    void SetCurrentLocation (const Vect &loc);
    void SetPreviousLocation (const Vect &loc);
    void SetOriginalLocation (const Vect &loc);

    OEPointingEvent *RawOEPointingEvent () const;
    void SetRawOEPointingEvent (OEPointingEvent *pe);

    virtual void SynthesizeInternalProtein ();
    virtual ObRetort AnalyzeInternalProtein ();
};

class MzNavigateBeginEvent : public MzNavigateEvent {
    PATELLA_SUBCLASS (MzNavigateBeginEvent, MzNavigateEvent);
    OE_MISCY_MAKER (MzNavigateBegin, MzNavigate, "begin");
    MzNavigateBeginEvent (KneeObject *orig_ko = NULL)
        : MzNavigateEvent (orig_ko) {}
};

class MzNavigateEndEvent : public MzNavigateEvent {
    PATELLA_SUBCLASS (MzNavigateEndEvent, MzNavigateEvent);
    OE_MISCY_MAKER (MzNavigateEnd, MzNavigate, "end");
    MzNavigateEndEvent (KneeObject *orig_ko = NULL)
        : MzNavigateEvent (orig_ko) {}
};

class MzNavigateContinueEvent : public MzNavigateEvent {
    PATELLA_SUBCLASS (MzNavigateContinueEvent, MzNavigateEvent);
    OE_MISCY_MAKER (MzNavigateContinue, MzNavigate, "continue");
    MzNavigateContinueEvent (KneeObject *orig_ko = NULL)
        : MzNavigateEvent (orig_ko) {}
};

class MzNavigateAbortEvent : public MzNavigateEvent {
    PATELLA_SUBCLASS (MzNavigateAbortEvent, MzNavigateEvent);
    OE_MISCY_MAKER (MzNavigateAbort, MzNavigate, "abort");
    MzNavigateAbortEvent (KneeObject *orig_ko = NULL)
        : MzNavigateEvent (orig_ko) {}
};

class MzNavigateEventAcceptorGroup
    : public MzNavigateBeginEvent::MzNavigateBeginAcceptor,
      public MzNavigateEndEvent::MzNavigateEndAcceptor,
      public MzNavigateContinueEvent::MzNavigateContinueAcceptor,
      public MzNavigateAbortEvent::MzNavigateAbortAcceptor {};

class MzNavigate {
public:
    typedef MzNavigateEventAcceptorGroup Evts;
};
}
} // a bittersweet end for namespaces sluice and oblong

DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED (
    oblong::sluice::MzNavigate::Evts);

#endif
