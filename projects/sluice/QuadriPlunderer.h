
/* (c)  oblong industries */

#ifndef QUADRI_PLUNDERERERER
#define QUADRI_PLUNDERERERER

#include "QuadriPilfer.h"
#include "MzHandi.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::loam;

class QuadriPlunderer : public Acetate, public MzHandi::Evts {
    PATELLA_SUBCLASS (QuadriPlunderer, Acetate);

public:
    // we declare busy_pilfers as a trove of KneeObjects in order to pass them
    // as event targets via Acetate::DescentTargets
    ObTrove<KneeObject *> busy_pilfers;
    ObTrove<QuadriPilfer *> idle_pilfers;

    // the bounds for all our QuadriPilfers
    Vect bounds_corner, bounds_over, bounds_up;

public:
    QuadriPlunderer ();
    virtual ~QuadriPlunderer ();
    virtual void ArrangeDenizens ();

    void SetBounds (Vect &bounds_corner, Vect &bounds_over, Vect &bounds_up);

    // returns a free QuadriPilfer, creating one if needed
    QuadriPilfer *IdleQuadriPilfer ();
    // returns the QuadriPilfer currently pilfering said provenance
    QuadriPilfer *QuadriPilferForProvenance (const Str &prv);

    void ResetMotiveProvenances ();

    // this event is handled by us and used to assign QuadriPilfers
    virtual ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                           Atmosphere *atm);

    // these events are passed along to our busy QuadriPilfers
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
};
}
} // el final de mezzanine, oblong

#endif
