
/* (c)  oblong industries */

#ifndef PASSFORWARD_GLIMPSER_RAMBLINGS
#define PASSFORWARD_GLIMPSER_RAMBLINGS

#include <libAfferent/Glimpser.h>

#include <libImpetus/OEPointing.h>

#include <libBasement/SpaceFeld.h>

#include <libPlasma/c++/Hose.h>

namespace oblong {
namespace sluice {
using namespace oblong::afferent;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class PassforwardGlimpser : public Glimpser {
    PATELLA_SUBCLASS (PassforwardGlimpser, Glimpser);

protected:
    ObRef<Hose *> input_hose;
    bool is_suspended;

public:
    struct Papoose {
        Str provenance;
        Vect cur_loc, cur_aim;
        Vect prev_loc, prev_aim;
        Str prev_button, cur_button;
        // Str cur_species;
        // int32 refractory;
        float64 last_seen;
        SpaceFeld *cur_feld_cntx, *prev_feld_cntx;
        OEPointingEvent::PressureReport *pressure_report;
        FatherTime uhr;
        Papoose (const Str &prov,
                 const Vect &loc,
                 const Vect &aim,
                 const Str &button_p,
                 SpaceFeld *space_cntx,
                 float64 ts);
    };

    static FatherTime LocalFT;

    /// because some clients may be stale, but still trying to send
    /// passforward events, we have to keep a list of clients that
    /// we're currently allowed to generate events for.
    ObUniqueTrove<Str> registered_provenances;
    ObTrove<Papoose *> pooses;

    PassforwardGlimpser ();
    PassforwardGlimpser (KneeObject *ev_target);
    virtual ~PassforwardGlimpser () { /* TODO (kjhollen): hose cleanup */
    }

    /// allow this passforward glimpser to generate events for the
    /// given provenance.
    void RegisterProvenance (const Str &prov);
    /// disallow this passforward glimpser to generate events for the
    /// given provenance.
    void UnregisterProvenance (const Str &prov);
    /// check if this passforward glimpser can generate events for
    /// the given provenance and return true if it can, false otherwise.
    bool IsProvenanceRegistered (const Str &prov);

    Papoose *PapooseByProvenance (const Str &prov);

    /// a call to Suspend () immediately generates a vanish event for all
    /// registered provenances. all subsequent passforward proteins received
    /// from clients are ignored until Resume is called. note that it is still
    /// possible to register and unregister clients while a PassforwardGlimpser
    /// is suspended.
    void SuspendPassforward (Atmosphere *atm);
    void ResumePassforward (Atmosphere *atm);
    bool IsPassforwardSuspended ();

    Hose *InputHose () { return ~input_hose; }
    void SetInputPool (const Str &input_pool_name);

    ObRetort Inhale (Atmosphere *atm);

    OEPointingAppearEvent *GinUpAppearEvent (Papoose *pap);
    OEPointingVanishEvent *GinUpVanishEvent (Papoose *pap);
    OEPointingMoveEvent *GinUpMoveEvent (Papoose *pap);
    OEPointingHardenEvent *GinUpHardenEvent (Papoose *pap);
    OEPointingSoftenEvent *GinUpSoftenEvent (Papoose *pap);

    ObRetort ProcessPassforwardProtein (const Protein &prt, Atmosphere *atm);
};
}
} // glorious end to comrade namespaces sluice and oblong

#endif
