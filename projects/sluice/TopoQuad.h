
// (c) Conduce Inc.

#pragma once

#include "RenderingInstructions.h"
#include "Shader.h"
#include "GLQuad.h"
#include "Subscription.h"
#include "Inculcator.h"
#include "SlawTraits.h"
#include "Hasselhoff.h"

#include "TopoPigment.h"

#include <libLoam/c++/FatherTime.h>
#include <libNoodoo/GLTex.h>

namespace oblong {
namespace sluice {

using conduce::sluice::PigmentBuffer;
using conduce::sluice::PigmentOffsets;
using conduce::sluice::BufferDescription;

class TopoQuad : public GLQuad {
    PATELLA_SUBCLASS (TopoQuad, GLQuad);

private:
    std::vector<topo::Pigment> pigs;

    size_t
    AddVerts (const SubscriptionNode &, const topo::Pigment &, PigmentBuffer &);
    bool PigmentForName (const Str &name, size_t &index) const;
    std::vector<topo::PigMatch>
    CalculatePigmentMatches (const std::vector<SubscriptionNode> &,
                             const ObTrove<Inculcator *> &);

    void depositRenderingInfo ();

public:
    TopoQuad () = default;

    void PopulatePigmentsFromSlaw (Slaw);

    void Clear ();

    void SetData (const std::vector<SubscriptionNode> &,
                  const ObTrove<Inculcator *> &);
    virtual void SetCurrentZoom (float64 x) override { current_zoom = x; }

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm) override;
};
}
}
