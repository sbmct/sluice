#include <iostream>
#include <iterator>

#include "GeoTree.h"
#include "Grouper.h"
#include "profile.h"
#include "GeoSubset.h"

using namespace oblong::sluice;

bool GeoIndex::Bounds (Node *n,
                       const float64 t,
                       float64 min_bounds[2],
                       float64 max_bounds[2]) {
    ObTrove<LatLon> path;
    if (n->HasGeoloc (t)) {
        LatLon ll = n->Geoloc (t);
        min_bounds[0] = max_bounds[0] = ll.lat;
        min_bounds[1] = max_bounds[1] = ll.lon;
    } else if (n->GetPath (path, t) && 1 < path.Count ()) {
        ObCrawl<LatLon> cr = path.Crawl ();
        LatLon ll = cr.popfore ();
        min_bounds[0] = max_bounds[0] = ll.lat;
        min_bounds[1] = max_bounds[1] = ll.lon;
        while (!cr.isempty ()) {
            ll = cr.popfore ();
            min_bounds[0] = std::min (min_bounds[0], ll.lat);
            min_bounds[1] = std::min (min_bounds[1], ll.lon);
            max_bounds[0] = std::max (max_bounds[0], ll.lat);
            max_bounds[1] = std::max (max_bounds[1], ll.lon);
        }
    } else {
        min_bounds[0] = min_bounds[1] = max_bounds[0] = max_bounds[1] = 0.0;
        return false;
    }
    return true;
}

GeoIndex::GeoIndex (const GeoRect &bounds) : KneeObject (), counter (0) {
    timeline_start = timeline.end ();
}

void GeoIndex::Clear () {
    entries.clear ();
    entries_rev.clear ();
    contained.clear ();
    alive.clear ();
    to_expunge.clear ();
    rtree.RemoveAll ();
    timeline.clear ();
    timeline_start = timeline.end ();
}

void GeoIndex::Delete () { super::Delete (); }

void GeoIndex::ExpireEntry (const size_t entry) {
    auto &bounds = entries[entry];
    auto &valid = contained[bounds.n];
    // Erase everything older than bounds.t, presumably.
    valid.erase (valid.lower_bound (bounds.t), valid.end ());
    if (0 == valid.size ()) {
        contained.erase (bounds.n);
    }
    rtree.Remove (bounds.min_bounds, bounds.max_bounds, bounds.id);
    entries.erase (entry);
    auto &rev = entries_rev[bounds.n];
    rev.remove (bounds.id);
    if (0 == rev.size ()) {
        entries_rev.erase (bounds.n);
        alive.erase (bounds.n);
    }
}

void GeoIndex::ExpireNodes (const float64 expire_time) {
    if (timeline_start == timeline.end () ||
        timeline_start->first != expire_time) {
        timeline_start = timeline.lower_bound (expire_time);
    }
    while ((timeline_start != timeline.begin ()) &&
           (std::prev (timeline_start)->first == expire_time)) {
        timeline_start = std::prev (timeline_start);
    }
    const auto finish = timeline.upper_bound (expire_time);
    for (auto i = timeline_start; i != finish; ++i) {
        ExpireEntry (i->second);
    }
    timeline.erase (timeline_start, finish);
    timeline_start = finish;
}

void GeoIndex::Add (Node *n, const float64 t) {
    bounds_t bounds;
    if (Bounds (n, t, bounds.min_bounds, bounds.max_bounds)) {
        bounds.id = counter++;
        bounds.n = n;
        bounds.t = t;

        entries[bounds.id] = bounds;
        entries_rev[n].push_back (bounds.id);
        contained[n][t] = true;
        alive[n] = n;
        rtree.Insert (bounds.min_bounds, bounds.max_bounds, bounds.id);
        timeline.insert (std::make_pair (t, bounds.id));
    }
}

void GeoIndex::Remove (Node *n, const float64 t) { Expunge (n); }

const bool GeoIndex::Contains (Node *n, const float64 t) {
    validmap_t::const_iterator found = contained[n].lower_bound (t);
    return found != contained[n].end () && found->second;
}

const int64 GeoIndex::Count () { return contained.size (); }
Node *GeoIndex::First () { return NULL; }

bool GeoIndex::NodeIsValidAt (const size_t e, const float64 t) {
    entries_t::const_iterator iter = entries.find (e);
    if (iter == entries.end ())
        return false;
    const bounds_t &bounds = iter->second;
    Node *n = bounds.n;
    const float64 x = bounds.t;
    if (!Alive (n))
        return false;
    if (!(n->HasGeoloc (t) || n->HasPath (t)))
        return false;
    validmap_t::const_iterator found = contained[n].lower_bound (t);
    if (found == contained[n].end ())
        return false;
    if (!found->second && x == found->first)
        return false;

    return true;
}

Node *GeoIndex::NodeForEntry (const size_t e) {
    entries_t::const_iterator iter = entries.find (e);
    if (iter != entries.end ())
        return iter->second.n;
    else
        return NULL;
}

void GeoIndex::Expunge (Node *n) {
    BOOST_FOREACH (const size_t id, entries_rev[n]) {
        entries_t::iterator iter = entries.find (id);
        if (iter != entries.end ()) {
            bounds_t &bounds = iter->second;
            rtree.Remove (bounds.min_bounds, bounds.max_bounds, bounds.id);
            entries.erase (iter);
            while (timeline_start != timeline.end () &&
                   timeline_start->first == bounds.t) {
                ++timeline_start;
            }
            timeline.erase (bounds.t);
        }
    }
    entries_rev.erase (n);
    contained.erase (n);
    alive.erase (n);
}

const bool GeoIndex::Alive (Node *n) {
    node_lifemap_t::const_iterator iter = alive.find (n);
    bool ret = false;
    if (iter != alive.end ()) {
        ret = NULL != ~(iter->second);
        if (!ret)
            QueueExpunge (n);
    }
    return ret;
}

void GeoIndex::QueueExpunge (Node *n) { to_expunge.push_back (n); }

void GeoIndex::ClearExpungable () {
    if (0 < to_expunge.size ()) {
        BOOST_FOREACH (Node *n, to_expunge)
            Expunge (n);
        to_expunge.clear ();
    }
}

template <typename Container> struct Inserter {
    const float64 t;
    GeoIndex *index;
    Container &output;

    Inserter (const float64 _t, GeoIndex *i, Container &c)
        : t (_t), index (i), output (c) {}

    void Insert (size_t x) {
        if (index->NodeIsValidAt (x, t))
            output.insert (index->NodeForEntry (x));
    }
};

template <typename C> static bool inner_insert (size_t x, void *set_p) {
    ((Inserter<C> *)set_p)->Insert (x);
    return true;
}

typedef Inserter<std::set<Node *>> nodeout_t;
typedef Inserter<GeoSubset> subout_t;

void
GeoIndex::Find (const float64 t, const GeoRect &search, std::set<Node *> &out) {
    float64 min_bounds[2] = {search.bl.lat, search.bl.lon};
    float64 max_bounds[2] = {search.tr.lat, search.tr.lon};
    nodeout_t nodeout (t, this, out);
    rtree.Search (min_bounds, max_bounds, inner_insert<std::set<Node *>>,
                  (void *)&nodeout);
    ClearExpungable ();
}

void
GeoIndex::Find (const float64 t, const GeoRect &search, GeoSubset &subset) {
    float64 min_bounds[2] = {search.bl.lat, search.bl.lon};
    float64 max_bounds[2] = {search.tr.lat, search.tr.lon};
    subout_t setout (t, this, subset);
    rtree.Search (min_bounds, max_bounds, inner_insert<GeoSubset>,
                  (void *)&setout);
    ClearExpungable ();
}
