
/* (c) oblong industries */

#ifndef Protein_Sedatable_Drome_GUARD
#define Protein_Sedatable_Drome_GUARD

#include <libNoodoo/VisiDrome.h>

using namespace oblong::noodoo;

/** Adds a single capability to VisiDrome: this drome can
    be sedated or revived by a protein sent to its drome pool.

    Descrips for these simple, standard proteins are just
    "sedate" or "revive".

    What this drome doesn't automatically do is sedate itself
    in response to a gesture.  If you want your app to be
    used within underhand or other related sedate/revive schemes,
    you must add a sayonara gesture like this:

        TwoHandBlurtRod *t = new TwoHandBlurtRod;
        t -> AppendEntryGripe ("*||||:.^");
        t -> AppendEntryBGripe ("*||||:.^");
        t -> SetUtterance ("sedate-self-and-revive-underhand");
        gestator -> AppendGlimpser (new RoddedGlimpser (this, t));

    ...and trigger UrDrome's own SedateSelfAndReviveUnderhand ():

        ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *atm)
          { if (e -> Utterance () == "sedate-self-and-revive-underhand")
              return SedateSelfAndReviveUnderhand (Protein (), atm);
            return OB_OK;
          }
  */

class ProteinSedatableDrome : public VisiDrome {
    PATELLA_SUBCLASS (ProteinSedatableDrome, VisiDrome);

public:
    ProteinSedatableDrome (const Str &drome_name, int ac, char **av)
        : VisiDrome (drome_name, ac, av) {
        AppendMetabolizer (Slaw::List ("sedate"),
                           &ProteinSedatableDrome::MetabolizeSedation);
    }

    ProteinSedatableDrome (const Str &drome_name,
                           const ObTrove<Str> &cmdline_leftovers)
        : VisiDrome (drome_name, cmdline_leftovers) {
        AppendMetabolizer (Slaw::List ("sedate"),
                           &ProteinSedatableDrome::MetabolizeSedation);
    }

    /// When a "sedate" protein comes, calls UrDrome's SedateSelf ().
    ObRetort MetabolizeSedation (const Protein &p, Atmosphere *a) {
        OB_LOG_INFO ("%s metabolized sedation protein; sedating now",
                     program_name.utf8 ());
        UrDrome::OutermostDrome ()->PoolDeposit (
            "drome-pool",
            Protein (Slaw::List ("sedated-on-command"), Slaw::Map ()));
        SedateSelf ();
        return OB_OK;
    }

    /** The UrDrome implementation of this method stops metabolization
        completely, leaving the Drome unable to metabolize a revive protein.
        Instead, we will loop & watch for a revive protein on the drome pool.
        Old-school POSIX signals can also wake the drome up, as normal.

        Exercise to the reader:
        In order to still have some rendering going on, could think
        about manually calling some rendering machinery from here -- not
        sure what, but not Respire (recursive!)
     */
    virtual void SedationQuiscence () {
        OB_LOG_INFO ("Watching for revive proteins on pool %s",
                     DromePoolName ().utf8 ());

        Hose *dromePoolHose = Pool::Participate (DromePoolName ());
        if (!dromePoolHose)
            OB_FATAL_ERROR (
                "Could not acquire drome-pool hose -- and that's bad.");

        while (sedation_status != SEDATION_STATUS_AWAITING_REVIVICATION) {
            Protein p = dromePoolHose->ProbeForward (Slaw ("revive"), 0.5);
            if (!p.IsNull ()) {
                UrDrome::OutermostDrome ()->PoolDeposit (
                    "drome-pool",
                    Protein (Slaw::List (program_name + "-revives"),
                             Slaw::Map ()));
                sedation_status = SEDATION_STATUS_AWAITING_REVIVICATION;
            }
        }
        dromePoolHose->Withdraw ();

        sedation_status = SEDATION_STATUS_VIVACIOUS;
    }
};

#endif
