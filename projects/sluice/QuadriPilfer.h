
/* (c)  oblong industries */

#ifndef QUADRI_PILFER_A_POCKET_OR_TWO_SAYS_FAGIN
#define QUADRI_PILFER_A_POCKET_OR_TWO_SAYS_FAGIN

#include "Tweezers.h"

#include "MzHandi.h"

#include <libNoodoo/VFImageRecorder.h>

#include <libBasement/SineColor.h>
#include <libNoodoo/GlyphoString.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class QuadriPilfer : public Tweezers, public MzHandi::Evts {
    PATELLA_SUBCLASS (QuadriPilfer, Tweezers);

public:
    Str motive_prov;
    bool is_hardened;
    bool in_error_mode;
    Vect orig_loc, curr_loc;
    Vect bounds_corner, bounds_over, bounds_up;
    ObRef<VFImageRecorder *> img_rcrdr;
    Vect *initial_h, *initial_v;
    float64 initial_estab_dist;
    ObColor bord_iro;
    ObColor adj_color;
    ObRef<SineColor *> fill_iro;
    ObRef<GlyphoString *> label;
    Vect label_offset;

    static int32 pilfers_in_progress;
    static time_t last_capture_time;
    static int64 snapshot_counter;

public:
    // Each QuadriPilfer is given a constraint rect. If all bounds vectors
    // are zero they are ignored and the marquee remains unconstrained.
    QuadriPilfer (Vect &bounds_corner, Vect &bounds_over, Vect &bounds_up);
    virtual ~QuadriPilfer ();
    virtual void ArrangeDenizens ();

    // a QP doesn't use its built-in 'loc' or 'lage', so it's going to
    // fail a reasonable visibility test (it seems for all the world to
    // be at the absolute origin, where our felds typically ... aren't).
    // so we just revert to the default BSphere, whose negative radius
    // field instructs the visibility test to let it slide on by.
    virtual BSphere BoundingSphere () const {
        return ShowyThing::BoundingSphere ();
    }

    // Indicates whether or not a bounding rect has been provided
    bool IsBounded ();

    // Returns the nearest point in the rect to the point specified. The rect is
    // defined by its lower left corner, and over and up vectors indicating its
    // orientation and size. Returns a new vect which must later be deleted.
    static Vect *NearestPointInRectToPoint (const Vect &corner,
                                            const Vect &over,
                                            const Vect &up,
                                            const Vect &point);

    // Populates the vector supplied with the nearest point in the rect to the
    // point specified. The rect is defined by its lower left corner, and over
    // and up vectors indicating its orientation and size. Returns true to
    // indicate that the point was constrained, and false if it already lay
    // within the bounds of the rect.
    static bool NearestPointInRectToPoint (const Vect &corner,
                                           const Vect &over,
                                           const Vect &up,
                                           const Vect &point,
                                           Vect *fill_v);

    // Returns the nearest point in the rect to the ray defined by points from
    // and through. The rect is defined by its lower left corner, and over and
    // up
    // vectors indicating its orientation and size. Returns a new vect which
    // must
    // later be deleted. Note that this method is optimized under the assumption
    // that the ray points generally in the direction of the rect, and actually
    // returns the nearest point in the rect to the intersection of the ray with
    // the plane of the rect, thus yielding incorrect answers for rays
    // approaching parallel with that plane.
    static Vect *NearestPointInRectToRay (const Vect &corner,
                                          const Vect &over,
                                          const Vect &up,
                                          const Vect &from,
                                          const Vect &through);

    // Populates the vector supplied with the nearest point in the rect to the
    // ray
    // defined by points from and through. The rect is defined by its lower left
    // corner, and over and up vectors indicating its orientation and size.
    // Returns true when the supplied point was constrained, and false if the
    // ray
    // already intersected the rect. Note that this method is optimized under
    // the
    // assumption that the ray points generally in the direction of the rect,
    // and
    // actually returns the nearest point in the rect to the intersection of the
    // ray with the plane of the rect, thus yielding incorrect answers for rays
    // approaching parallel with that plane.
    static bool NearestPointInRectToRay (const Vect &corner,
                                         const Vect &over,
                                         const Vect &up,
                                         const Vect &from,
                                         const Vect &through,
                                         Vect *fill_v);

    // Constrains a rect within a constraint rect by directly modifying its
    // corner point. Both rects are defined by their lower left corners, and the
    // over and up vectors which indicate both their orientation and their size.
    // Returns true if the rect must be constrained, or false if it already lies
    // completely within the bounds of the constraint rect.
    static bool ConstrainRectToRect (Vect &corner,
                                     const Vect &over,
                                     const Vect &up,
                                     const Vect &constraint_corner,
                                     const Vect &constraint_over,
                                     const Vect &constraint_up);

    float64 InitialEstablishmentDist () const { return initial_estab_dist; }
    void SetInitialEstablishmentDist (float64 ied) { initial_estab_dist = ied; }

    Str WriteAssetFromImageClot (ImageClot *ic, const Str &ast_uid);

    ObRetort NewlyGrabbedImageCallback (ObRef<ImageClot *> iccy,
                                        VisiFeld *,
                                        const Str &name);
    // pre-image grab call back: hide HandiPoints & QuadriPilfers
    ObRetort PreImageGrabCallback (const Str &name);

    void SetMotiveProvenance (const Str &prv);
    void ResetMotiveProvenance ();
    const Str &MotiveProvenance ();

    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);

    void ShowError (Str error_desc);
    void HideError ();

    virtual ObRetort Inhale (Atmosphere *atm);
    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);
};
}
} // le mort d'namespace mezzanine, oblong

#endif
