
#pragma once

#include <map>
#include <set>
#include <vector>
#include <algorithm>

#include <libLoam/c++/Str.h>
#include <libBasement/KneeObject.h>

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;

class Node;

class GeoSubset {
    PATELLA_CLASS (GeoSubset);

protected:
    typedef std::vector<Node *> nodeset_t;
    nodeset_t store;

    std::set<Str> active_kinds;

public:
    GeoSubset (std::set<Str> &kinds) : active_kinds (kinds) {}
    virtual ~GeoSubset () {}

    void insert (Node *n);

    // make sure that the underlying store can hold the number of
    // Nodes we want to insert
    void Reserve (int64 Count);
    // Insert a node without consideration of active kind.
    void BlindAppend (Node *);

    const int64 Count () const { return (int64)store.size (); }
    void Sort ();

    // Following to support foreach-style iteration
    typedef nodeset_t::iterator iterator;
    typedef nodeset_t::const_iterator const_iterator;
    typedef nodeset_t::value_type value_type;

    const_iterator begin () const { return store.begin (); }
    const_iterator end () const { return store.end (); }

    iterator begin () { return store.begin (); }
    iterator end () { return store.end (); }
};
}
}
