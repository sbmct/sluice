/* Copyright (C) 2012 Oblong Industries */

#ifndef OBLONG_SLUICE_DECORATEDSTATICIMAGEFLUOROSCOPE_H
#define OBLONG_SLUICE_DECORATEDSTATICIMAGEFLUOROSCOPE_H

#include "StaticImageFluoroscope.h"

namespace oblong {
namespace sluice {

/**
 * Static image fluoroscope with some node data along for good measure
 *
 */
class DecoratedStaticImageFluoroscope : public StaticImageFluoroscope {
    PATELLA_SUBCLASS (DecoratedStaticImageFluoroscope, StaticImageFluoroscope);

protected:
    virtual ObRetort RequestProtein (Protein &prot);
    virtual ObRetort SetConfigurationSlaw (const Slaw config);
    virtual void SendTextureRequestToPool (Protein &prot);

private:
    FLAG_MACHINERY (FlexibleRequestProteins);

    ObRetort RequestProteinDatastore (Protein &prot);
    ObRetort RequestProteinDeprecated (Protein &prot);

    Slaw ReadAttributes (const Slaw s);
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_DECORATEDSTATICIMAGEFLUOROSCOPE_H
