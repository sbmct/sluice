
#pragma once

#include <libLoam/c++/ObMap.h>
#include <libNoodoo/GlyphoString.h>

#include "Acetate.h"

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::noodoo;

/// Show strings on the WideScreen
class DisplayStrs : public Acetate {
    PATELLA_SUBCLASS (DisplayStrs, FlatThing);

protected:
    ObMap<Str, GlyphoString *> strs_by_provenance;

    void Reshuffle ();
    GlyphoString *MakeGlyphoString (const Str provenance);

public:
    void Add (const Str provenance, const Str data);
    void Remove (const Str provenance);
};
}
}
