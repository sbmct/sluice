/* (c) Conduce Inc. */

#ifndef SINGLE_FLUORO_NODE_STORE
#define SINGLE_FLUORO_NODE_STORE

#include <libLoam/c++/ObTrove.h>
#include <libLoam/c++/FatherTime.h>
#include <vector>
#include <algorithm>
#include <map>

#include "NodeStore.h"
#include "Node.h"
#include "Hasselhoff.h"
#include "Subscription.h"

namespace oblong {
namespace sluice {

class SingleFluoroNodeStore : public NodeStore {
    PATELLA_SUBCLASS (SingleFluoroNodeStore, NodeStore);

private:
    // There shall be but one timkeeping node store in all of sluice.
    ObRef<NodeStore *> timekeeper;
    ObRef<NodeStore *> inculcator_keeper;

    typedef ObWeakRef<Node *> WeakNode;
    typedef std::map<Str, WeakNode> NodeIdentMap;
    typedef ObTrove<Node *> NodeTrove;

    mutable NodeIdentMap node_id_lookup;
    NodeTrove current, to_remove;
    NodeTrove consider_add, consider_remove;

    ObRef<Hasselhoff *> hoff;

    float64 heartbeat_frequency{5.0};
    oblong::loam::FatherTime heartbeat_timer;

    Subscription subscription;

protected:
    void UpdateSubscription ();

    void StoreCacheSettings (const SubscriptionData &);
    void FillNode (Node *n, const SubscriptionNode &);
    void AddRawNode (const SubscriptionNode &);
    void RemoveRawNode (const Str &);
    void RemoveIdentMapping (Node *);

    void ConsiderUpdates ();

    void SendHeartbeat ();

    FLAG_MACHINERY (SubscriptionUpdateRequired);

public:
    SingleFluoroNodeStore (NodeStore *);
    virtual ~SingleFluoroNodeStore ();

    // Make this explicit rather than via a destructor such that
    // we can subclass without sending out a protein twice. (Also,
    // deposits can fail. Keep destructors simple.)
    void Unregister ();

    void Configure (const std::set<Str> _enabled_kinds);
    void SetBounds (const GeoRect bounds,
                    const float64 visible_degrees,
                    const LatLon &bl,
                    const LatLon &tr);
    void SetHeartbeatFrequency (const float64);

    ObRetort MetabolizeSubscriptionData (const Protein &, Atmosphere *);
    ObRetort MetabolizeSubscriptionDelta (const Protein &, Atmosphere *);

    ObRetort TimeUpdated (NodeStore *);

    virtual ObRetort Inhale (Atmosphere *) override;
    virtual ObRetort Exhale (Atmosphere *) override;

    // Add node should replace existing nodes, updating will come separately
    virtual void AddNode (Node *n, const float64 t) override;
    virtual void RemoveNode (Node *n, const float64 t) override;

    virtual Node *FindByIdent (const Str x) const override;

    virtual const void FindByLatLonRange (const LatLon bl,
                                          const LatLon tr,
                                          const float64 visible_degrees,
                                          std::set<Node *> &out) override;

    virtual const void SubsetByLatLonRange (const LatLon bl,
                                            const LatLon tr,
                                            const float64 visible_degrees,
                                            GeoSubset &subset) override;

    virtual void InitializePoolRegistration (Hasselhoff *hoff) override;

    virtual void AddInculcators (const Slaw inculs) override;

    virtual const int64 KindsCount () const override;
    virtual void KindsPSA () const override;

    // Everything below is unused.
    virtual ObTrove<Inculcator *> Inculcators () override;
    virtual void SetConfigurationSlaw (const Slaw s) override;

    virtual void RemoveInculcators (const Slaw inculs) override;

    virtual ObRetort SetSluiceTime (const float64 x) override;
    virtual ObRetort SetSluiceTime (const Str time_str) override;
    virtual const float64 SluiceTime () override;
    virtual const bool QueryOnLiveTime () const override;

    virtual ObRetort SetSluiceSecondsPerSecond (const float64 x) override;
    virtual const float64 SluiceSecondsPerSecond () const override;

    virtual const float64 MaxTime () override;
    virtual const float64 MinTime () override;
    virtual bool IsTimePaused () const override;
    virtual ObRetort PauseTime () override;
    virtual ObRetort UnPauseTime () override;
    virtual ObRetort UseLiveTime () override;

    virtual Slaw TimeChangeNotificationSlaw () override;
    virtual void SendTimeChangeNotificationProtein () override;

    virtual ObRetort ParseDate (const Str &s, float64 &when) override;

    /// Determine whether we have crossed in to a new "zoom level"
    virtual const bool
    CrossesZoomLevelBoundary (const float64 visdeg_a,
                              const float64 visdeg_b) const override;

    virtual const bool IsActiveForVisibleDegrees (Node *n,
                                                  const float64 zoom) override;

    virtual void ForceZoomLevel (const Str &) override;
    virtual Str ForcedZoomLevel () const override;
    virtual ObRetort AdjustPausedTime (const float64 prev_max_time) override;
};
}
}

#endif //! SINGLE_FLUORO_NODE_STORE
