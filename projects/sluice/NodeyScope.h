/* Copyright (C) 2012 Oblong Industries */

#ifndef OBLONG_SLUICE_NODEYSCOPE_H
#define OBLONG_SLUICE_NODEYSCOPE_H

#include "AtlasQuad.h"
#include "Fluoroscope.h"
#include "Hasselhoff.h"

#include <set>
#include <map>

#include <boost/unordered_set.hpp>

namespace oblong {
namespace sluice {

class NetworkQuad;
class IntStoreQuad;
class SingleFluoroNodeStore;

/**
 * A Fluoroscope for rendering nodes (both points and paths) directly to the
 * screen
 *
 * @TODO: Make this documentation (and NodeStore's) reflect reality. (7/2, cp)
 *
 * NodeyScope's main responsibility is to forward nodes correctly on to
 * a NetworkQuad instance.  A series of Inculcator instances will determine
 * what "pigment" (NodePigment) or pigments should draw each node.  This
 * decision is made based on the node's kind and its attributes.  Pigments
 * are defined by the following slaw:
\code
inculcators:
- name: "pigment name"
  color: <soft color definition>
  size: <soft float definition>
  icon: "path to image resource"
- ...
\endcode
 *
 * See SoftBuddy for soft color/float defintions.
 *
 * Inculcators, which decide which pigments each Node goes to, are defined
 * as follows:
\code
pigments:
- name: "inculcator name"
  pigment: "pigment name"
  kinds: # optional
  - "slaw kind 1"
  - "slaw kind 2"
  - "slaw kind 3"
  jstest: "javascript snippet that returns a bool" # optional
- ...
\endcode
 *
 * If "kinds" is specified, a node must belong to one of the kinds listed
 * to be matched.  If "jstest" is specified, a node must evaluate true by
 * that test at the sluice time it is run in order to be included.
 */
class NodeyScope : public Fluoroscope {
    PATELLA_SUBCLASS (NodeyScope, Fluoroscope);

public:
    // typedef boost::unordered_set<Node*> nodeset_t;
    typedef std::set<Node *> nodeset_t;

protected:
    nodeset_t nodes;
    std::map<Str, Slaw> pigments;
    ObRef<Hasselhoff *> hoff;
    void FindNodesetDeltas ();
    void UpdateNodesForBoundsChange (GeoRect nxt, float64 vis_deg);

    float64 previous_visible_degrees;
    GeoRect previous;
    ObMap<Str, Str> default_vert_shader_fn, default_frag_shader_fn;
    // client-selected kinds to display
    std::set<Str> enabled_kinds, filter_scripts_whitelist,
        filter_scripts_blacklist;

    inline bool MatchesFilters (Node *n);
    inline bool MatchesEnabledKinds (Node *n) const;
    bool MatchesWhitelist (Node *n) const;
    bool MatchesBlacklist (Node *n) const;

    void SetupIntStorePigments (const Slaw pigments);
    void SetupNetworkPigments (const Slaw pigments);

    void InformQuadOfNodeSize ();

    ObRetort QuadSizeChange (GeoQuad *gq);

    ObRetort TimeUpdated (NodeStore *);
    ObRetort UniverseUpdated (NodeStore *);

    /// Update a node after we've determined that it is in our area
    bool broadcast_vbos;
    bool CastableQuad (GeoQuad *gq) const;
    void RemoveNodeQuad (Node *node, GeoQuad *gq);
    ObRetort UpdateProperlyLocatedNode (Node *n, GeoQuad *nq, const float64 t);
    ObRetort UpdateNode (Node *node,
                         GeoQuad *nq,
                         const GeoRect &bounds,
                         const float64 t);

    ObRetort ShowNodeInfo (const Str provenance, const Str text);
    ObRetort HideNodeInfo (const Str provenance);

    void IngestGeoSubset (GeoSubset &subset, GeoRect &bounds, GeoQuad *nq);
    bool WhacksAppropriately (MzHandiEvent *e, Vect *hit);

    void RemoveHovercraft ();

    FatherTime expunge_counter;
    float64 expunge_threshold;
    void PeriodicExpunge ();

    void SetCentralNodeStore (NodeStore *);
    void SetSingleNodeStore (NodeStore *);
    void ConfigureSingleStore ();

    SingleFluoroNodeStore *SingleStore ();

    FLAG_MACHINERY (UpdateRequired);
    FLAG_MACHINERY (LocsChanged);
    FLAG_MACHINERY (MenuEnabled)
    FLAG_MACHINERY (NeedsRebase);
    FLAG_MACHINERY (JustRebased);
    FLAG_MACHINERY (WaitingOnAutopilot);
    FLAG_MACHINERY (ListeningOnAutopilot);

    SOFT_MACHINERY (SoftColor, SearchArea);

private:
    GeoRect last_rebase_search;
    bool single_node_store{false};

public:
    virtual ~NodeyScope ();
    NodeyScope ();
    NodeyScope (const NodeyScope &) = delete;
    NodeyScope &operator=(const NodeyScope &) = delete;

    bool NodeIsShowing (Node *) const;
    const nodeset_t &AllNodes () const { return nodes; }

    ObRetort CartoCoordsChange (GeoQuad *gq);
    ObRetort AutopiloitCartoCoordsChangeStopped (AtlasQuad *__gq);
    virtual void SetGQuad (GeoQuad *gq);

    /* So that NodeyScope can be interrogated for debugging information.
     * We whould be EXTREMELY CAREFUL to make sure that this calls
     * super::InitializePoolRegistration (hoff);
     */
    virtual void InitializePoolRegistration (Hasselhoff *hoff) override;

    /**
     * Respond to node debug requests...
     */
    ObRetort MetabolizeDebugTopo (const Protein &prt, Atmosphere *atmo);
    Slaw DescribeInculcators (Slaw ing) const;

    // Hookups for the hookup
    virtual void SetNodeStore (NodeStore *ns);
    ObRetort UpdateNode (Node *node);
    ObRetort Update (NodeStore *store, Node *updated);
    ObRetort RemoveNode (Node *node);
    ObRetort Remove (NodeStore *store, Node *removed);

    virtual ObRetort SetConfigurationSlaw (const Slaw config) override;
    virtual void Rebase ();

    virtual ObRetort Inhale (Atmosphere *atmo);
    virtual ObRetort Exhale (Atmosphere *atmo);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atmo);

    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort
    ReturnStroke (const Slaw &news, ElectricalEvent *ee, Atmosphere *atmo);
    const bool IsWhackedFirst (const Vect physo, const Vect physt);
    virtual bool AcceptsPassthrough () const override;

    ObRetort vboInfoUpdated (Slaw);
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_NODEYSCOPE_H
