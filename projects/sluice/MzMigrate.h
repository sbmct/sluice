
/* (c)  oblong industries */

#ifndef MEZZANINE_MIGRATE_EVENT
#define MEZZANINE_MIGRATE_EVENT

#include <libGanglia/ElectricalEvent.h>

namespace oblong {
namespace sluice {
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class MzMigrateEvent : public ElectricalEvent {
    PATELLA_SUBCLASS (MzMigrateEvent, ElectricalEvent);
    OE_MISCY_MAKER (MzMigrate, Electrical, "migrate");

    // TODO: establish plane of dragging?
private:
    Vect original_loc, curr_loc, prev_loc;
    KneeObject *migrant;

public:
    MzMigrateEvent (KneeObject *orig_ko = NULL)
        : ElectricalEvent (orig_ko), migrant (NULL) {}

    const Vect &OriginalLoc () const;
    const Vect &CurrentLoc () const;
    const Vect &PreviousLoc () const;

    KneeObject *Migrant () const;
    void SetMigrant (KneeObject *ft);

    void SetOriginalLocation (const Vect &org);
    void SetPreviousLocation (const Vect &prv);
    void SetCurrentLocation (const Vect &cur);

    virtual void SynthesizeInternalProtein ();
    virtual ObRetort AnalyzeInternalProtein ();
};

class MzMigrateBeginEvent : public MzMigrateEvent {
    PATELLA_SUBCLASS (MzMigrateBeginEvent, MzMigrateEvent);
    OE_MISCY_MAKER (MzMigrateBegin, MzMigrate, "begin");
    MzMigrateBeginEvent (KneeObject *orig_ko = NULL)
        : MzMigrateEvent (orig_ko) {}
};

class MzMigrateContinueEvent : public MzMigrateEvent {
    PATELLA_SUBCLASS (MzMigrateContinueEvent, MzMigrateEvent);
    OE_MISCY_MAKER (MzMigrateContinue, MzMigrate, "continue");
    MzMigrateContinueEvent (KneeObject *orig_ko = NULL)
        : MzMigrateEvent (orig_ko) {}
};

class MzMigrateEndEvent : public MzMigrateEvent {
    PATELLA_SUBCLASS (MzMigrateEndEvent, MzMigrateEvent);
    OE_MISCY_MAKER (MzMigrateEnd, MzMigrate, "end");
    MzMigrateEndEvent (KneeObject *orig_ko = NULL) : MzMigrateEvent (orig_ko) {}
};

class MzMigrateAbortEvent : public MzMigrateEvent {
    PATELLA_SUBCLASS (MzMigrateAbortEvent, MzMigrateEvent);
    OE_MISCY_MAKER (MzMigrateAbort, MzMigrate, "abort");
    MzMigrateAbortEvent (KneeObject *orig_ko = NULL)
        : MzMigrateEvent (orig_ko) {}
};

class MzMigrateEventAcceptorGroup
    : public MzMigrateBeginEvent::MzMigrateBeginAcceptor,
      public MzMigrateContinueEvent::MzMigrateContinueAcceptor,
      public MzMigrateEndEvent::MzMigrateEndAcceptor,
      public MzMigrateAbortEvent::MzMigrateAbortAcceptor {};

class MzMigrate {
public:
    typedef MzMigrateEventAcceptorGroup Evts;
};
}
} // a bittersweet end for namespaces sluice and oblong

DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED (oblong::sluice::MzMigrate::Evts);

#endif
