
/* (c)  oblong industries */

#include "Ovipositor.h"

#include "HandiPoint.h"
#include "MzTender.h"
#include "SluiceFile.h"
#include "YaroSneeze.h"

#include <libLoam/c/ob-rand.h>

#include <libNoodoo/TexQuad.h>

#include <libBasement/LinearColor.h>
#include <libBasement/ThrobWrangler.h>
#include <libBasement/AsympFloat.h>
#include <libBasement/AsympVect.h>
#include <libBasement/QuadraticVect.h>

using namespace oblong::sluice;
using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

const Str Ovipositor::prv_extra = " | ovipositor";

Ovipositor::Ovipositor () : Tweezers () {
    static float32 DUR = 0.3;
    static ObRef<ImageClot *> ic = ImageClot::FindOrLoadBySourceName (
        SluiceFile::FindResourceFile ("sluice/mezzanine/hex.tif"));

    LinearColor *lc = new LinearColor;
    lc->SetInterpTime (DUR);
    InstallAdjColor (lc);
    SetAdjColorHard (ObColor (1.0, 0.0));

    UnduLine *ul = new UnduLine;
    ul->SetNumVertices (30);
    QuadraticVect *locv = new QuadraticVect;
    locv->SetInterpTime (DUR);
    ul->InstallP3 (locv);
    ul->SetP1Freq (0.45);
    ul->SetP2Freq (0.430872);
    //  ul -> SetP1DistFrac (0.35);
    //  ul -> SetP2DistFrac (0.35);
    ul->P1Soft ()->SetPhase (1.5);
    ul->SetStrokeColor (ObColor (1.0, 1.0));
    AppendChild (ul);
    umbilical = ul;

    TexQuad *tq = new TexQuad (~ic);
    AppendChild (tq);

    float64 frq = ob_rand_float64 (0.9, 1.1);
    ThrobWrangler *tw = new ThrobWrangler (
        Vect (0.95, 0.85, 0.85), Vect (1.15, 1.15, 1.15), Vect (frq, frq, frq));
    tq->AppendWrangler (tw);
    AsympFloat *af = new AsympFloat;
    af->SetInterpTime (DUR);
    tq->InstallWidth (af);
    af = new AsympFloat;
    af->SetInterpTime (DUR);
    tq->InstallHeight (af);
    locv = new QuadraticVect;
    locv->SetInterpTime (DUR);
    tq->InstallLoc (locv);
    tw->InstallCenter (locv);
    distal_end = tq;

    tq = new TexQuad (~ic);
    AppendChild (tq);

    frq = ob_rand_float64 (0.9, 1.1);
    tw = new ThrobWrangler (Vect (0.95, 0.85, 0.85), Vect (1.15, 1.15, 1.15),
                            Vect (frq, frq, frq));
    tq->AppendWrangler (tw);
    af = new AsympFloat;
    af->SetInterpTime (DUR);
    tq->InstallWidth (af);
    af = new AsympFloat;
    af->SetInterpTime (DUR);
    tq->InstallHeight (af);
    locv = new QuadraticVect;
    locv->SetInterpTime (DUR);
    tq->InstallLoc (locv);
    tw->InstallCenter (locv);
    estab_end = tq;

    info_string = new GlyphoString ();
    (~info_string)->SetAlignment (FlatThing::V::Center, FlatThing::H::Left);
    (~info_string)->SetFont (ObStyle::MediumFontPath ());
    (~info_string)->SetTextColor (ObColor (1.0, 1.0));
    (~info_string)
        ->SetTextBlockAlignment (GlyphoString::V::Center,
                                 GlyphoString::H::Left);
    (~info_string)->SetBackingColor (ObColor (1.0, 0.3));
    (~info_string)->SetShouldDraw (false);
    AppendChild (~info_string);

    SetShouldInhale (true);

    // we'll use this to show how much time has elapsed since the
    // event occurred that caused this Ovipositor to come in to existence.
    elapsed_uhr.ZeroTime ();
}

Ovipositor::~Ovipositor () {}

const Vect &Ovipositor::DistalLoc () const {
    static Vect zero_v;
    FlatThing *de = ~distal_end;
    return (de ? de->Loc () : zero_v);
}

void Ovipositor::InitiallyUnfurl (Atmosphere *atm) {
    SetAdjColor (ObColor (1.0, 1.0));

    ObRef<FeldGeomPod *> gp = RetrieveGeometry ();
    FlatThing *de = ~distal_end;
    FlatThing *ee = ~estab_end;
    GlyphoString *g_string = ~info_string;
    if (de && ee && g_string)
        if (FeldGeomPod *g = ~gp) {
            float64 size = 0.1 * g->height;
            de->SetSize (0.1 * g->height, 0.1 * g->height);
            ee->SetSize (0.07 * g->height, 0.07 * g->height);
            g_string->SetFontSize (size * 0.42);
            g_string->SetPadding (size / 8.0, size / 16.0);
        } else {
            de->SetSize (50.0, 50.0);
            ee->SetSize (40.0, 40.0);
            g_string->SetFontSize (24.0);
            g_string->SetPadding (8.0, 4.0);
        }
}

ObRetort Ovipositor::MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    const Str &prv = e->Provenance ();
    if (prv != MotiveProvenance ())
        return OB_EVENT_UNMATCHED_PROVENANCE;

    if (FlatThing *de = ~distal_end) {
        if (OEPointingEvent *oep = e->RawOEPointingEvent ()) {
            Vect hit;
            if (de->SelfPlaneWhackCheck (oep->PhysOrigin (),
                                         oep->PhysThrough (), NULL, &hit, NULL))
                SetDistalLocHard (hit);
            if (UnduLine *ul = ~umbilical)
                ul->SetP3Hard (hit);
            if (GlyphoString *g_string = ~info_string) {
                Vect position = Vect (hit.X () + g_string->FontSize () * 1.33,
                                      hit.Y (), hit.Z ());
                g_string->SetLocHard (position);
            }
        }
    }

    {
        MzTenderEvent *te = new MzTenderProbeEvent;
        ObRef<MzTenderEvent *> selbstmord (te);
        te->SetEventArcElapsedInterval (elapsed_uhr.CurTime ());
        te->SetProvenance (EnhancedProvenance (prv));
        te->SetEstabLocation (EstabLoc ());
        te->SetCurrLocation (e->Location ());
        te->SetPrevLocation (e->PreviousLocation ());
        te->SetSourceEntity (SourceEntity ());
        te->SetChargeOrigin (this);
        te->SetItem (Ovum ());
        te->SetRawEvent (e);
        KneeObject *trg = dynamic_cast<KneeObject *> (
            ClosestAtmosphericSwitchboardAgent (atm));
        if (!trg)
            trg =
                dynamic_cast<KneeObject *> (ClosestParentalSwitchboardAgent ());
        te->ProfferTo (trg, atm);
        // te -> Delete ();
    }

    return OB_OK;
}

ObRetort Ovipositor::MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    const Str &prv = e->Provenance ();
    if (prv != MotiveProvenance ())
        return OB_EVENT_UNMATCHED_PROVENANCE;

    SetMotiveProvenance (prv);

    const Str &raw_prv = HandiPoint::OriginalProvenance (prv);
    if (HandiPoint *hp = HandiPoint::FindByProvenance (raw_prv))
        hp->Evaporate ();

    // we do this to intercept all events, cutting out anyone one else further
    // up the conduction chain, who should instead respond to tender events.
    return ObRetort (OB_EE_CONDUCT_VIA_OTHER_PATH,
                     new EERelayPathPod (new RelayPath (this)));
}

ObRetort Ovipositor::MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;
    const Str &prv = e->Provenance ();
    if (prv != MotiveProvenance ())
        return OB_EVENT_UNMATCHED_PROVENANCE;

    // disappear gracefully once we're done being used
    Dismiss (false);

    OEPointingEvent *oep = e->RawOEPointingEvent ();
    if (oep) {
        if (HandiPoint *hp = HandiPoint::FindByProvenance (oep->Provenance ()))
            hp->Condense ();
    }

    {
        MzTenderEvent *te = new MzTenderSolidifyEvent;
        ObRef<MzTenderEvent *> selbstmord (te);
        te->SetProvenance (EnhancedProvenance (prv));
        te->SetEventArcElapsedInterval (elapsed_uhr.CurTime ());
        te->SetEstabLocation (EstabLoc ());
        te->SetCurrLocation (e->Location ());
        te->SetPrevLocation (e->PreviousLocation ());
        te->SetSourceEntity (SourceEntity ());
        te->SetChargeOrigin (this);
        te->SetItem (Ovum ());
        te->SetRawEvent (e);
        KneeObject *trg = dynamic_cast<KneeObject *> (
            ClosestAtmosphericSwitchboardAgent (atm));
        if (!trg)
            trg =
                dynamic_cast<KneeObject *> (ClosestParentalSwitchboardAgent ());
        te->ProfferTo (trg, atm);
        // te -> Delete ();
    }

    return OB_EE_BREAK_CONNECTION;
}

ObRetort Ovipositor::MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;
    const Str &prv = e->Provenance ();
    if (prv != MotiveProvenance ())
        return OB_EVENT_UNMATCHED_PROVENANCE;

    // hide ourselves gracefully if handipoint vanishes
    Dismiss (false);

    // ensure the handipoint isn't shown, even though we restore its
    // visibility in Dismiss by default
    const Str &raw_prv = HandiPoint::OriginalProvenance (prv);
    if (HandiPoint *hp = HandiPoint::FindByProvenance (raw_prv))
        hp->Evaporate ();

    MzTenderEvent *te = new MzTenderRescindEvent;
    ObRef<MzTenderEvent *> selbstmord (te);
    te->SetProvenance (EnhancedProvenance (prv));
    te->SetEventArcElapsedInterval (elapsed_uhr.CurTime ());
    te->SetEstabLocation (EstabLoc ());
    te->SetCurrLocation (e->Location ());
    te->SetPrevLocation (e->PreviousLocation ());
    te->SetSourceEntity (SourceEntity ());
    te->SetChargeOrigin (this);
    te->SetItem (Ovum ());
    te->SetRawEvent (e);
    KneeObject *trg =
        dynamic_cast<KneeObject *> (ClosestAtmosphericSwitchboardAgent (atm));
    if (!trg)
        trg = dynamic_cast<KneeObject *> (ClosestParentalSwitchboardAgent ());
    te->ProfferTo (trg, atm);

    return OB_EE_BREAK_CONNECTION;
}

ObRetort Ovipositor::Dismiss (bool break_connection) {
    const Str &prv = MotiveProvenance ();

    // restore handipoint visibility
    const Str &raw_prv = HandiPoint::OriginalProvenance (prv);
    if (HandiPoint *hp = HandiPoint::FindByProvenance (raw_prv))
        hp->Condense ();

    // break event conduction if necessary
    if (break_connection)
        if (SwitchboardAgent *switcher = ClosestParentalSwitchboardAgent ())
            if (switcher->BreakConnectionForProvenance (prv, NULL) == OB_OK)
                OB_LOG_DEBUG ("Ovipositor conduction broken by its dismissal.");

    // animate distal loc to origin
    SetDistalLoc (estab_loc);
    // fade
    SetAdjColor (ObColor (1.0, 0.0));
    // and defer to super class for timing.

    SetMotiveProvenance (Str::Null);
    return super::Dismiss (break_connection);
}

Str Ovipositor::EnhancedProvenance (const Str &prv) { return prv + prv_extra; }

Str Ovipositor::OriginalProvenance (const Str &prv) {
    Str ret (prv);
    return ret.Chomp (prv_extra);
}

ObRetort Ovipositor::ReturnStroke (
    const Slaw &info,
    ElectricalEvent *ee,
    Atmosphere *atm) { // Using the ReturnStroke mechanism as a means of
    // providing user feedback as to the state of the Ovipositor
    // and its action

    Str action;
    if (info.MapFind ("action").Into (action)) { // Perhaps the following block
                                                 // can be replaced by various
                                                 // styles like
        // HandiPoint styles, but for now this should suffice
        if (action == "deposit") {
            ObColor target_color = ObColor (1.0, 1.0);
            if (AdjColorGoalVal () != target_color)
                SetAdjColor (ObColor (1.0, 1.0));
        } else if (action == "rejected" || action == "pending-deletion") {
            ObColor color;
            if (info.MapFind ("color").Into (color)) {
                if (AdjColorGoalVal () != color)
                    SetAdjColor (color);
            }
        }
    }

    if (GlyphoString *g_string = ~info_string) {
        Str message;
        if (info.MapFind ("message")
                .Into (message)) { // If a message is set, show the string
            g_string->SetString (message);
            g_string->SetShouldDraw (true);
        } else { // If there isn't a message automatically hide the string
            g_string->SetString (NULL);
            g_string->SetShouldDraw (false);
        }
    }

    return OB_OK;
}
