
#include "ClipShack.h"
#include <boost/foreach.hpp>

using namespace oblong::sluice;
using namespace oblong::noodoo;
using namespace oblong::loam;
using namespace oblong::basement;

ClipState::ClipState (GLenum _p) : plane (_p) {}

void ClipState::Save () {
    if (enabled = glIsEnabled (plane))
        glGetClipPlane (plane, equation);
}

void ClipState::Restore () {
    if (enabled) {
        glEnable (plane);
        glClipPlane (plane, equation);
    } else
        glDisable (plane);
}

ClipShack::ClipShack ()
    : LocusThing (), isClippingDepth (false), scale_wrang (new SWrangler ()),
      loc_wrang (new TWrangler ()) {
    InstallDimensionWidth (new SoftFloat);
    InstallDimensionHeight (new SoftFloat);
    InstallDimensionDepth (new SoftFloat);
    InstallVerticalAlignment (new SoftFloat);
    InstallHorizontalAlignment (new SoftFloat);
    InstallDepthAlignment (new SoftFloat);

    am_clipping = false;
    SetShouldClip (true);

    clip_states.push_back (ClipState (GL_CLIP_PLANE0));
    clip_states.push_back (ClipState (GL_CLIP_PLANE1));
    clip_states.push_back (ClipState (GL_CLIP_PLANE2));
    clip_states.push_back (ClipState (GL_CLIP_PLANE3));
    clip_states.push_back (ClipState (GL_CLIP_PLANE4));
    clip_states.push_back (ClipState (GL_CLIP_PLANE5));
}

void ClipShack::ApplyClippers () {
    Vect o (Over ());
    Vect u (Up ());
    Vect d (Vect (DimensionWidth (), DimensionHeight (), DimensionDepth ()));
    Vect l (LocOf (0.0, 0.0, 0.0));

    BOOST_FOREACH (ClipState &cs, clip_states)
        cs.Save ();

    static GLdouble clop[4];

    clop[0] = o.x;
    clop[1] = o.y;
    clop[2] = o.z;
    clop[3] = (l - d / 2).Dot (-o);
    glClipPlane (GL_CLIP_PLANE0, clop);

    clop[0] = -o.x;
    clop[1] = -o.y;
    clop[2] = -o.z;
    clop[3] = (l + d / 2).Dot (o);
    glClipPlane (GL_CLIP_PLANE1, clop);

    clop[0] = u.x;
    clop[1] = u.y;
    clop[2] = u.z;
    clop[3] = (l - d / 2).Dot (-u);
    glClipPlane (GL_CLIP_PLANE2, clop);

    clop[0] = -u.x;
    clop[1] = -u.y;
    clop[2] = -u.z;
    clop[3] = (l + d / 2).Dot (u);
    glClipPlane (GL_CLIP_PLANE3, clop);

    glEnable (GL_CLIP_PLANE0);
    glEnable (GL_CLIP_PLANE1);
    glEnable (GL_CLIP_PLANE2);
    glEnable (GL_CLIP_PLANE3);

    if (isClippingDepth) {
        Vect n (Norm ());
        clop[0] = n.x;
        clop[1] = n.y;
        clop[2] = n.z;
        clop[3] = (l - d / 2).Dot (-n);
        glClipPlane (GL_CLIP_PLANE4, clop);

        clop[0] = -n.x;
        clop[1] = -n.y;
        clop[2] = -n.z;
        clop[3] = (l + d / 2).Dot (n);
        glClipPlane (GL_CLIP_PLANE5, clop);

        glEnable (GL_CLIP_PLANE4);
        glEnable (GL_CLIP_PLANE5);
    }

    am_clipping = true;
}

void ClipShack::UnapplyClippers () {
    BOOST_FOREACH (ClipState &cs, clip_states)
        cs.Restore ();

    am_clipping = false;
}

ObRetort ClipShack::PreDraw (VisiFeld *feld, Atmosphere *atm) {
    if (QueryShouldClip ())
        ApplyClippers ();
    return OB_OK;
}

ObRetort ClipShack::PostDraw (VisiFeld *feld, Atmosphere *atm) {
    if (QueryShouldClip () || am_clipping)
        UnapplyClippers ();
    return OB_OK;
}

void ClipShack::SetDimensions (const Vect &d) {
    DimensionWidthSoft ()->Set (d.x);
    DimensionHeightSoft ()->Set (d.y);
    DimensionDepthSoft ()->Set (d.z);
}

void ClipShack::SetDimensions (float x, float y, float z) {
    DimensionWidthSoft ()->Set (x);
    DimensionHeightSoft ()->Set (y);
    DimensionDepthSoft ()->Set (z);
}

void ClipShack::SetDimensionsHard (const Vect &d) {
    DimensionWidthSoft ()->SetHard (d.x);
    DimensionHeightSoft ()->SetHard (d.y);
    DimensionDepthSoft ()->SetHard (d.z);
}

void ClipShack::SetDimensionsHard (float x, float y, float z) {
    DimensionWidthSoft ()->SetHard (x);
    DimensionHeightSoft ()->SetHard (y);
    DimensionDepthSoft ()->SetHard (z);
}

void ClipShack::AppendTarget (ShowyThing *t, int64 ind) {
    if (ind == -1)
        AppendChild (t);
    else
        InsertChildAt (t, ind);

    t->AppendWrangler (~scale_wrang);
    t->AppendWrangler (~loc_wrang);
}

void ClipShack::RemoveTarget (ShowyThing *t) { RemoveChild (t); }

void ClipShack::SetScaleCenter (const Vect &loc) {
    (~scale_wrang)->SetCenter (loc);
}

const Vect &ClipShack::ScaleCenter () const {
    return (~scale_wrang)->Center ();
}

void ClipShack::SetIsClippingDepth (bool t) { isClippingDepth = t; }

bool ClipShack::QueryIsClippingDepth () { return isClippingDepth; }

void ClipShack::InstallScale (SoftVect *sof_s) {
    (~scale_wrang)->InstallScale (sof_s);
}

void ClipShack::SetScale (const Vect &sv) { (~scale_wrang)->SetScale (sv); }

void ClipShack::SetScale (float s) { (~scale_wrang)->SetScale (s); }

void ClipShack::AddToScale (const Vect &sv) {
    (~scale_wrang)->SetScale (sv + Scale ());
}

void ClipShack::AddToScale (float s) {
    (~scale_wrang)->SetScale (Vect (s, s, s) + Scale ());
}

const Vect &ClipShack::Scale () const { return (~scale_wrang)->Scale (); }

void ClipShack::InstallScroll (SoftVect *sof_s) {
    (~loc_wrang)->InstallTranslation (sof_s);
}

void ClipShack::SetScroll (const Vect &loc) {
    (~loc_wrang)->SetTranslation (loc);
}

void ClipShack::SetScrollHard (const Vect &loc) {
    (~loc_wrang)->SetTranslationHard (loc);
}

void ClipShack::AddToScroll (const Vect &off) {
    (~loc_wrang)->AddToTranslation (off);
}

const Vect &ClipShack::Scroll () const { return (~loc_wrang)->Translation (); }

const Vect &ClipShack::ScrollGoal () const {
    return (~loc_wrang)->TranslationGoal ();
}

void ClipShack::Reset () {
    (~scale_wrang)->UnityScale ();
    (~loc_wrang)->ZeroTranslation ();
}

void ClipShack::ResetHard () {
    (~scale_wrang)->UnityScale ();
    (~loc_wrang)->ZeroTranslationHard ();
}

void ClipShack::SetLoc (const Vect &p) { super::SetLoc (p); }
// note that AcknowledgeLocationChange() gets called in the super::
// call immediately foregoing; otherwise, we'd sure's shooting be
// calling it explicitly.

void
ClipShack::SetLoc (const Vect &p, V::Align vrt, H::Align hrz, D::Align dep) {
    SetLoc (p, (vrt - 1) / 2.0, (hrz - 1) / 2.0, (dep - 1) / 2.0);
}

void ClipShack::SetLoc (const Vect &p, float64 vrt, float64 hrz, float64 dep) {
    float64 dva = VerticalAlignmentGoalVal () - vrt;
    float64 dha = HorizontalAlignmentGoalVal () - hrz;
    float64 dda = DepthAlignmentGoalVal () - dep;

    SetLoc (p + (dva * Up () * DimensionHeightGoalVal ()) +
            (dha * Over () * DimensionWidthGoalVal ()) -
            (dda * Norm () * DimensionDepthGoalVal ()));
}

void ClipShack::SetLocHard (const Vect &p) { super::SetLocHard (p); }

void ClipShack::SetLocHard (const Vect &p,
                            V::Align vrt,
                            H::Align hrz,
                            D::Align dep) {
    SetLocHard (p, (vrt - 1) / 2.0, (hrz - 1) / 2.0, (dep - 1) / 2.0);
}

void
ClipShack::SetLocHard (const Vect &p, float64 vrt, float64 hrz, float64 dep) {
    float64 dva = VerticalAlignmentGoalVal () - vrt;
    float64 dha = HorizontalAlignmentGoalVal () - hrz;
    float64 dda = DepthAlignmentGoalVal () - dep;

    SetLocHard (p + (dva * Up () * DimensionHeightGoalVal ()) +
                (dha * Over () * DimensionWidthGoalVal ()) -
                (dda * Norm () * DimensionDepthGoalVal ()));
}

Vect ClipShack::LocOf (V::Align vrt, H::Align hrz, D::Align dep) const {
    return LocOf ((vrt - 1) / 2.0, (hrz - 1) / 2.0, (dep - 1) / 2.0);
}

Vect ClipShack::LocOf (float64 vrt, float64 hrz, float64 dep) const {
    float64 dva = vrt - VerticalAlignment ();
    float64 dha = hrz - HorizontalAlignment ();
    float64 dda = dep - DepthAlignment ();

    return Loc () + (dva * Up () * DimensionHeight ()) +
           (dha * Over () * DimensionWidth ()) -
           (dda * Norm () * DimensionDepth ());
}

Vect ClipShack::LocGoalValOf (V::Align vrt, H::Align hrz, D::Align dep) const {
    return LocGoalValOf ((vrt - 1) / 2.0, (hrz - 1) / 2.0, (dep - 1) / 2.0);
}

Vect ClipShack::LocGoalValOf (float64 vrt, float64 hrz, float64 dep) const {
    float64 dva = vrt - VerticalAlignment ();
    float64 dha = hrz - HorizontalAlignment ();
    float64 dda = dep - DepthAlignment ();

    return LocGoalVal () + (dva * Up () * DimensionHeightGoalVal ()) +
           (dha * Over () * DimensionWidthGoalVal ()) -
           (dda * Norm () * DimensionDepthGoalVal ());
}

void ClipShack::SetAlignment (V::Align vrt, H::Align hrz, D::Align dep) {
    SetVerticalAlignment (vrt);
    SetHorizontalAlignment (hrz);
    SetDepthAlignment (dep);
}

void ClipShack::SetAlignment (float64 vrt, float64 hrz, float64 dep) {
    SetVerticalAlignment (vrt);
    SetHorizontalAlignment (hrz);
    SetDepthAlignment (dep);
}

void ClipShack::SetVerticalAlignment (V::Align vrt) {
    SetVerticalAlignment ((vrt - 1) / 2.0);
}

void ClipShack::SetVerticalAlignment (float64 vrt) {
    VerticalAlignmentSoft ()->Set (vrt);
}

void ClipShack::SetHorizontalAlignment (H::Align hrz) {
    SetHorizontalAlignment ((hrz - 1) / 2.0);
}

void ClipShack::SetHorizontalAlignment (float64 hrz) {
    HorizontalAlignmentSoft ()->Set (hrz);
}

void ClipShack::SetDepthAlignment (D::Align dep) {
    SetDepthAlignment ((dep - 1) / 2.0);
}

void ClipShack::SetDepthAlignment (float64 dep) {
    DepthAlignmentSoft ()->Set (dep);
}

void ClipShack::SetAlignmentHard (V::Align vrt, H::Align hrz, D::Align dep) {
    SetVerticalAlignmentHard (vrt);
    SetHorizontalAlignmentHard (hrz);
    SetDepthAlignmentHard (dep);
}

void ClipShack::SetAlignmentHard (float64 vrt, float64 hrz, float64 dep) {
    SetVerticalAlignmentHard (vrt);
    SetHorizontalAlignmentHard (hrz);
    SetDepthAlignmentHard (dep);
}

void ClipShack::SetVerticalAlignmentHard (V::Align vrt) {
    SetVerticalAlignmentHard ((vrt - 1) / 2.0);
}

void ClipShack::SetVerticalAlignmentHard (float64 vrt) {
    VerticalAlignmentSoft ()->SetHard (vrt);
}

void ClipShack::SetHorizontalAlignmentHard (H::Align hrz) {
    SetHorizontalAlignment ((hrz - 1) / 2.0);
}

void ClipShack::SetHorizontalAlignmentHard (float64 hrz) {
    HorizontalAlignmentSoft ()->SetHard (hrz);
}

void ClipShack::SetDepthAlignmentHard (D::Align dep) {
    SetDepthAlignment ((dep - 1) / 2.0);
}

void ClipShack::SetDepthAlignmentHard (float64 dep) {
    DepthAlignmentSoft ()->SetHard (dep);
}

int ClipShack::VerticalAlignmentEnum () {
    float64 vrt = VerticalAlignmentGoalVal ();
    if (vrt == 0.0)
        return V::Center;
    else if (vrt == 0.5)
        return V::Top;
    else if (vrt == -0.5)
        return V::Bottom;
    return -1;
}

int ClipShack::HorizontalAlignmentEnum () {
    float64 hrz = HorizontalAlignmentGoalVal ();
    if (hrz == 0.0)
        return H::Center;
    else if (hrz == 0.5)
        return H::Left;
    else if (hrz == -0.5)
        return H::Right;
    return -1;
}

int ClipShack::DepthAlignmentEnum () {
    float64 dep = DepthAlignmentGoalVal ();
    if (dep == 0.0)
        return D::Center;
    else if (dep == 0.5)
        return D::Front;
    else if (dep == -0.5)
        return D::Back;
    return -1;
}
