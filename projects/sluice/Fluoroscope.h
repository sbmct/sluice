
#ifndef NINE_OUT_OF_TEN_DENTISTS_RECOMMEND_FLUOROSCOPE
#define NINE_OUT_OF_TEN_DENTISTS_RECOMMEND_FLUOROSCOPE

#include "Acetate.h"
#include "GeoProj.h"
#include "GeoQuad.h"
#include "MzWaddle.h"
#include "MzFlate.h"
#include "MzHandi.h"
#include "MzTender.h"

#include "Hasselhoff.h"
#include "NodeStore.h"
#include "Skewer.h"
#include "MessageBanner.h"
#include "ClipShack.h"
#include "Shader.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;

/**
 * A Fluoroscope :
 *
 * - responsible for handling events, texture / data configuration
 * - handles all metabolizing
 * - moves the GeoQuad instance via normal set loc / size, then the
 *   GeoQuad's projection is updated via a hook to the Map (AtlasQuad).
 *
 *   GeoQuad is responsible for handling the texture mapping and rendering
 *   of geographically-aligned showy elements.
 *
 **/
class Fluoroscope : public Acetate,
                    public MzHandi::Evts,
                    public MzFlate::Evts,
                    public MzWaddle::Evts,
                    public MzTender::Evts {
    PATELLA_SUBCLASS (Fluoroscope, Acetate);

public:
    /**
     * Pinning tells the parent scope to respect our geometry and set our
     * corresponding projection instead.
     */
    struct Pin {
        float64 u_pin, v_pin;
        float64 cx_pin, cy_pin;

        Pin () : u_pin (0.0), v_pin (0.0), cx_pin (0.0), cy_pin (0.0) {}

        Pin (float64 u, float64 v, float64 cx, float64 cy)
            : u_pin (u), v_pin (v), cx_pin (cx), cy_pin (cy) {}

        Pin (const v2float64 &uv, const v2float64 &cxcy)
            : u_pin (uv.x), v_pin (uv.y), cx_pin (cxcy.x), cy_pin (cxcy.y) {}

        v2float64 UV ();
        v2float64 CXCY ();

        Str ToS () const {
            return Str ().Sprintf ("PIN {%.2f, %.2f, %.2f, %.2f}", u_pin, v_pin,
                                   cx_pin, cy_pin);
        }
    };

protected:
    ObRef<ClipShack *> clippy;
    ObRef<GeoQuad *> my_quad;
    ObRef<Shader *> callout_shader;

    ObTrove<Fluoroscope *> scopes;
    Fluoroscope *parent_scope;

    ObTrove<Pin> pins;

    Slaw config_slaw;

    Vect waddle_offset, waddle_last;

    ObRef<Skewer *> skewer;

    ObRef<NodeStore *> node_store;
    Str movement_prov;
    ObRetort ValidateMovementProvenance (const Str prov);
    bool MovementProvenanceMatches (const Str prov);
    void DisownMovementProvenance ();

    void MoveBy (const Vect v);
    void MoveByOnePin (const Vect &v);
    void MoveByTwoPins (const Vect &v);
    void MoveByNoPins (const Vect &v);

    void ScaleBy (const float64 scale);
    void ScaleByTwoPins (const float64 scale);
    void ScaleByPinless (const float64 scale);

    void SetCorners (const LatLon bl, const LatLon tr);
    void SetCenterAndSize (Vect cent, Vect size, bool windshield);
    void SetCornersTwoPins (const Vect bl, const Vect tr);

    ObRetort LocalVisibleDegrees (float64 &vis_deg) const;
    virtual ObRetort LocalBounds (LatLon &bl, LatLon &tr) const;

    float64 last_scale;
    bool needs_last_scale;
    Str scale_intent;

    virtual ObRetort TimeUpdated (NodeStore *store);

    bool ShouldRebase (Slaw) const;

    void ReadCalloutConfiguration (Slaw);
    void EnsureCalloutShader ();
    void DrawCallout ();

    LatLon callout_loc;
    Str callout_node;
    SOFT_MACHINERY (SoftColor, CalloutColor);
    SOFT_MACHINERY (SoftFloat, CalloutInteriorSize);
    SOFT_MACHINERY (SoftColor, CalloutBackgroundColor);
    SOFT_MACHINERY (SoftFloat, CalloutBackgroundSize);

    Str provenance;

public:
    Fluoroscope ();
    Fluoroscope (GeoProj *proj);

    virtual ~Fluoroscope ();

    static const Str navi_intent;
    static const Str point_intent;

    ObRef<MessageBanner *> msg_banner;

    /**
     * By default, false.  If true, this fluroscope can't be moved by
     * direct user interaction.  Also, exoskeleton should not show up
     * if this is set to false?
     */
    FLAG_MACHINERY (PermanentAndStatic);
    FLAG_MACHINERY (Disabled);
    FLAG_MACHINERY (ShouldClip);
    FLAG_MACHINERY (HasCallout);
    FLAG_MACHINERY (HasCalloutBackground);

    ObRetort AppendScope (Fluoroscope *scope);
    ObRetort PrependScope (Fluoroscope *scope);
    ObRetort InsertScope (Fluoroscope *scope, const int64 scope_index);

    ObRetort RemoveScope (Fluoroscope *scope);
    int64 NumScopes () const;
    Fluoroscope *NthScope (int64 ind);
    int64 FindScope (Fluoroscope *scope);
    Fluoroscope *FindScopeByQID (const Slaw &qid);
    Fluoroscope *FindScopeByName (const Str name);
    void EmptyScopes ();
    ObCrawl<Fluoroscope *> ScopesCrawl ();

    /// Parental Chain
    ObRetort SetParentScope (Fluoroscope *f);
    Fluoroscope *ParentScope ();

    void InstallSkewer () {
        if (~skewer)
            RemoveChild (~skewer);
        skewer = new Skewer (this);
        AppendChild (~skewer);
    }

    Skewer *GetSkewer () { return ~skewer; }

    /** Pinning --> */
    int64 NumPins ();
    ObTrove<Pin> Pins ();
    Pin NthPin (int64 n);
    ObRetort UnpinIt ();

    ObRetort PinIt (Pin p);
    ObRetort PinIt (Pin p1, Pin p2);

    virtual ObRetort PinSinglePoint (const V::Align &vrt, const H::Align &hrz);
    virtual ObRetort PinCurrentArea ();

    virtual void PinToRegion (const Slaw &config);
    virtual void PinToRegion (float64 lat_min,
                              float64 lat_max,
                              float64 lon_min,
                              float64 lon_max);
    /**
     * Update Child Scopes -->
     *   Typically, external users of FLuoroscopes will want to call
     *   UpdateFromParentScope, which will take care of first updating the self-
     *   fluoro then recurse through its children.
     */
    void UpdateChildScope (Fluoroscope *fl);
    void UpdateFromParentScope ();
    void RecursivelyUpdateChildScopes ();

    /// GeoQuad accessors...
    virtual GeoQuad *GQuad () const;
    virtual void SetGQuad (GeoQuad *gq);

    void DisableHighlight ();
    void EnableHighlight (ObColor color);

    /// We need our geoquad to shadow our Locus / FlatThing components.
    virtual void InstallLoc (SoftVect *sv);
    virtual void InstallNorm (SoftVect *sv);
    virtual void InstallOver (SoftVect *sv);

    virtual void InstallWidth (SoftFloat *sf);
    virtual void InstallHeight (SoftFloat *sf);
    virtual void InstallVerticalAlignment (SoftFloat *sf);
    virtual void InstallHorizontalAlignment (SoftFloat *sf);

    virtual ObRetort AcknowledgeGeoQuadSizeChanged (GeoQuad *);
    virtual ObRetort AcknowledgeGeoQuadLocationChanged (GeoQuad *);
    virtual ObRetort AcknowledgeCartoCoordsChanged (GeoQuad *);

    virtual ObRetort DescentScopes ();

    // we're moving
    virtual ObRetort MzWaddleBegin (MzWaddleBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzWaddleContinue (MzWaddleContinueEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzWaddleEnd (MzWaddleEndEvent *e, Atmosphere *atm);
    virtual ObRetort MzWaddleAbort (MzWaddleAbortEvent *e, Atmosphere *atm);

    // we're scaling
    virtual ObRetort MzFlateBegin (MzFlateBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzFlateContinue (MzFlateContinueEvent *e, Atmosphere *atm);
    virtual ObRetort MzFlateEnd (MzFlateEndEvent *e, Atmosphere *atm);
    virtual ObRetort MzFlateAbort (MzFlateAbortEvent *e, Atmosphere *atm);

    // raw handipoint evt
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiCondense (MzHandiCondenseEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
    // virtual ObRetort MzHandiRatchet (MzHandiRatchetEvent *e, Atmosphere
    // *atm);

    // gimmie a fluoroscope!
    virtual ObRetort MzTenderSolidify (MzTenderSolidifyEvent *e,
                                       Atmosphere *atm);

    //------------------------------------------------------------
    // Methods needed form the old Fluoroscope class
    //------------------------------------------------------------
    virtual void InitializePoolRegistration (Hasselhoff *hoff);
    virtual void SetNodeStore (NodeStore *ns);
    NodeStore *GetNodeStore ();
    Slaw AddFluoroBounds (Slaw f_config) const;
    virtual ObRetort SetConfigurationSlaw (const Slaw config);
    virtual Slaw ConfigurationSlaw () const;
    // used for getting descriptive name for exoskeleton
    virtual Str TypeString ();
    virtual Str DisplayText ();
    Fluoroscope *TopmostIntersectedOccupant (Vect orig, Vect through);
    virtual ObRetort RecursivelyClearFluoroscopes ();
    ObRetort MetabolizeConfChange (const Protein &prt, Atmosphere *atm);
    ObRetort MoveByFeldCoords (Slaw config, bool windshield);
    ObRetort MoveByLatLonBounds (GeoRect new_loc, bool windshield);
    ObRetort MoveToNode (Str ident, v2float64 size, bool windshield);
    virtual ObRetort MetabolizeMoveRequest (const Protein &, Atmosphere *);

    Fluoroscope *MainMap () const;
    const float64 ZoomLevel () const;

    ObRetort Bounds (GeoRect &bounds) const;
    ObRetort VisibleBounds (GeoRect &bounds) const;

    Vect LatLonToWrldPos (const LatLon &ll);
    LatLon WrldPosToLatLon (const Vect &v);
    ObRetort WrldPosToCarto (const Vect &v, float64 &x, float64 &y);

    virtual void Rebase ();

    // Display an error string from the fluoroscope rendering process
    virtual void SetError (const Str error);
    // If there is an error string set, dismiss it
    virtual void DismissError ();

    Vect LocFromPinCarto (const Pin &p);
    void AnnouncePointing (Fluoroscope *fluoro,
                           const Str provenance,
                           Atmosphere *atm);

    float64 GetMapZoom () const;
    float64 GetTileZoom () const;
    ObRetort GlobalVisibleDegrees (float64 &vdeg) const;
    ObRetort GlobalBounds (LatLon &bl, LatLon &tr) const;

    virtual bool AcceptsPassthrough () const { return false; }
    void AnnounceFluoroHoverPSA (const Slaw psa);

    /// A list of attributes that may be changed without forcing a rebase.
    /// remember to always include the results of super:: if you override.
    virtual std::vector<Str> WhitelistedAttributes () const {
        return std::vector<Str>{"opacity"};
    }

    virtual bool CalloutNodeLoc ();

    // ShowyThing
    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm) override;
    virtual ObRetort PreDraw (VisiFeld *, Atmosphere *) override;
    virtual ObRetort PostDraw (VisiFeld *, Atmosphere *) override;

private:
    Slaw WindshieldCoords () const;
    void AnnounceFluoroscopeLocation (GeoQuad *);

    void Init ();
};
}
} // namespaces sluice & oblong

#endif // NINE_OUT_OF_TEN_DENTISTS_RECOMMEND_FLUOROSCOPE
