
/* (c)  oblong industries */

#include "Trenton.h"
#include "YaroSneeze.h"

#include "SluiceRetorts.h"

#include <libBasement/ob-logging-macros.h>

#include <libBasement/AsympVect.h>

#include <libNoodoo/TexQuad.h>

#include <libMedia/PngImageClot.h>

using namespace oblong::sluice;
using namespace oblong::media;
using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

Trenton::Trenton () : Acetate () { SetAlwaysDoChildrenExhale (true); }

Trenton::~Trenton () {}

void Trenton::SetWideScreen (WideScreen *ws) {
    if (WideScreen *descreen = ~screen)
        RemoveChild (descreen);

    screen = ws;
    if (ws) {
        ws->SetHeight (HeightGoalVal ());
        ws->SetParent (this);
        AppendChild (ws);
    }
}

void Trenton::InitiallyUnfurl (Atmosphere *atm) {
    WideScreen *ws = new WideScreen ();
    SetWideScreen (ws);

    ws->InitiallyUnfurl (atm);
    InitializeWideScreen ();
}

void Trenton::ArrangeDenizens () {
    ObRef<FeldGeomPod *> gp = RetrieveGeometry ();
    float64 hei = HeightGoalVal ();
    float64 wid = 3.0 * WidthGoalVal ();

    if (FeldGeomPod *g = ~gp) // have geom?
    {
        hei = g->height;
        wid = 3.0 * g->width + 2.0 * g->mullion_width;
    }

    // a/b mode
    WideScreen *ws = ~screen;
    if (ws) {
        Vect v = Vect ();
        SetScreenGeometry (ws, v, wid, hei);
        ws->ArrangeDenizens ();
    }
}

void Trenton::InitializePoolRegistration (Hasselhoff *hoff) {
    if (WideScreen *ws = ~screen)
        ws->InitializePoolRegistration (hoff);
}

void Trenton::SetScreenGeometry (WideScreen *ws,
                                 const Vect &loc,
                                 float64 wid,
                                 float64 hei) {
    if (!ws)
        return;
    ws->SetLage (loc);
    ws->SetSize (wid, hei);
}

WideScreen *Trenton::InitializeWideScreen () {
    WideScreen *ws = MainScreen ();
    if (!ws)
        return NULL;
    ws->SetBackgroundColor (ObColor (0.4, 1.0));
    ws->SetBorderColor (ObColor (0.0, 1.0, 1.0));
    // ws -> SetOrdinal (1  +  d -> FindChildIndex (s));
    ws->UpdateBackgroundTextWithOrdinals ();

    return ws;
}

ObRetort Trenton::MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (e->IsConducting () || !e->IsIonizing ())
        return OB_OK;

    if (WideScreen *ws = MainScreen ())
        return DescentSingleTarget (ws);

    return OB_EE_NO_ROUTE;
}

ObRetort Trenton::MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (WideScreen *ws = MainScreen ())
        return DescentSingleTarget (ws);

    return OB_EE_NO_ROUTE;
}

ObRetort Trenton::MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (WideScreen *ws = MainScreen ())
        return DescentSingleTarget (ws);
    /*
      { //ObRetort baseRetort = (e -> Intent ()  ==  "passthrough")
        //  ?  OB_EE_DESCEND_FURTHER  :  OB_EE_BREAK_CONNECTION;
        ObRetort baseRetort = OB_EE_BREAK_CONNECTION;
        ObRetort ort (baseRetort);
        ort . SetRetortPod (new EEDescentPod (ws));
        return ort;
      }
    */

    return OB_EE_BREAK_CONNECTION;
}

ObRetort Trenton::MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                        Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (WideScreen *ws = MainScreen ())
        return DescentSingleTarget (ws);

    return OB_EE_NO_ROUTE;
}

ObRetort Trenton::MzTenderProbe (MzTenderProbeEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (WideScreen *ws = MainScreen ())
        return DescentSingleTarget (ws);
    return OB_EE_NO_ROUTE;
}

ObRetort Trenton::MzTenderSolidify (MzTenderSolidifyEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (WideScreen *ws = MainScreen ())
        return DescentSingleTarget (ws);
    return OB_EE_NO_ROUTE;
}

ObRetort Trenton::MzTenderRescind (MzTenderRescindEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (WideScreen *ws = MainScreen ())
        return DescentSingleTarget (ws);
    return OB_EE_NO_ROUTE;
}

ObRetort Trenton::Travail (Atmosphere *atm) { return super::Travail (atm); }
