
/* (c)  oblong industries */

#ifndef PARAMUS_GETS_THE_WILLIAM_CARLOS_WILLIAMS_TREATMENT
#define PARAMUS_GETS_THE_WILLIAM_CARLOS_WILLIAMS_TREATMENT

#include "Acetate.h"
#include "AssetHutch.h"
#include "MzHandi.h"
#include "MzTender.h"
#include "UIDSpigot.h"
#include "WideScreen.h"

#include <libBasement/ImageClot.h>
#include <libBasement/AsympFloat.h>
#include <libNoodoo/GlyphoString.h>
#include <libNoodoo/TexQuad.h>
#include <libNoodoo/ObStyle.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * Paramus contains a collection of available Assets for the current Dossier.
 * Copies of elements in Paramus can be dragged to the work area (Trenton) to
 * be added to Slides in the working Deck. Paramus contains smart layout rules
 * for grouping assets and assuring that no Assets are obscured by gaps between
 * VisiFelds.
 */

class FluoroGlyph : public TexQuad {
    PATELLA_SUBCLASS (FluoroGlyph, TexQuad)

protected:
    ObRef<GlyphoString *> label;

public:
    FluoroGlyph (ImageClot *ic, Str text) : TexQuad (ic) {
        SetLabel (text);
        SetVisibility (false);
    }

    void SetVisibility (bool new_visiblity) {
        SetShouldDraw (new_visiblity);
        if (!new_visiblity) {
            (~label)->SetTextColor (ObColor (0.0, 0.0));
        } else {
            (~label)->SetTextColor (ObColor (1.0, 1.0));
        }
    }

    bool QueryVisibiilty () const { return QueryShouldDraw (); }

    void SetLabel (Str text) {
        if (~label)
            RemoveChild (~label);
        label = new GlyphoString ();
        (~label)->SetFont (
            GlyphoFont::LoadOrRecallGlyphoFont (ObStyle::MediumFontPath ()));
        (~label)->SetString (text);
        AsympFloat *af = new AsympFloat ();
        af->SetInterpTime (0.5);
        (~label)->InstallFontSize (af);
        (~label)->SetAlignment (-0.5, 0.0);
        (~label)->SetLocHard (LocOf (-0.49, 0.0));
        AppendChild (~label);
    }
    GlyphoString *Label () { return ~label; }
    Str LabelString () {
        if (~label)
            return (~label)->String ();
        return Str ();
    }

    virtual ObRetort AcknowledgeLocationChange () {
        OB_LOG_DEBUG ("goal loc = %s\n", LocGoalVal ().ToS ().utf8 ());
        (~label)->SetLocHard (LocOf (-0.49, 0.0));
        (~label)->SetLoc (LocGoalValOf (-0.49, 0.0));
        return super::AcknowledgeLocationChange ();
    }

    virtual ObRetort AcknowledgeSizeChange () {
        OB_LOG_DEBUG ("goal height = %f\n", HeightGoalVal ());
        (~label)->FontSizeSoft ()->SetHard (Height () * 0.16);
        (~label)->SetFontSize (HeightGoalVal () * 0.16);
        return super::AcknowledgeSizeChange ();
    }
};

class Paramus : public Acetate, public MzHandi::Evts, public MzTender::Evts {
    PATELLA_SUBCLASS (Paramus, Acetate);

protected:
    // asset_count_limit now represents the # of *visible* assets
    int64 asset_count_limit;
    ObMap<Str, KneeObject *, NoMemMgmt, WeakRef> endangered_by_provenance;
    ObMap<Str, GlyphoString *, NoMemMgmt, WeakRef> del_string_by_provenance;
    ObMap<Str, KneeObject *, NoMemMgmt, WeakRef> held_asset_by_provenance;
    WideScreen *OurWideScreen ();

private:
    void Endanger (const MzTenderEvent *e);
    void UnEndanger (const MzTenderEvent *e, bool for_good);

public:
    Paramus ();
    virtual ~Paramus ();

    virtual void InitiallyUnfurl (Atmosphere *atm);
    virtual void ArrangeDenizens ();
    virtual void InitializePoolRegistration (Hasselhoff *hoff);
    ObRetort SendFluoroscopeTemplatesPSA () const;

    void InstallDefaultFluoroscopes ();
    ObRetort MetabolizeNewFluoroTemplate (const Protein &prt, Atmosphere *atm);
    ObRetort AddFluoroscopeFromSlaw (const Slaw &scope,
                                     const Slaw remove = Slaw::Nil ());

    int64 HutchCount () const { return ChildCountForClass ("AssetHutch"); }
    AssetHutch *NthHutch (int64 ind) const {
        static Str cl_name ("AssetHutch");
        return dynamic_cast<AssetHutch *> (NthChildOfClass (ind, cl_name));
    }
    AssetHutch *FindHutch (const Str &nm) const {
        return dynamic_cast<AssetHutch *> (FindChild (nm));
    }

    Str AssetUID (KneeObject *asset) const;
    int64 HutchIndex (AssetHutch *ah);
    AssetHutch *FindHutchContaining (KneeObject *element);
    KneeObject *FindAssetByUID (const Str &asst_uid) const;
    KneeObject *FindAssetByName (const Str &name) const;

    ObRetort AppendAsset (KneeObject *ko);
    int64 AssetCount () const;
    void SetAssetCountLimit (const int64 &l);
    int64 AssetCountLimit () const { return asset_count_limit; }

    ObRetort AppendElementFromDir (const Str &doss_dir,
                                   const Str &ast_uid,
                                   bool treat_as_flux = false);

    ObRetort
    InsertElementAt (KneeObject *element, AssetHutch *ah, int32 col, int32 row);
    ObRetort InsertElementNextTo (KneeObject *element,
                                  KneeObject *neighbor,
                                  const Str &befr_aftr = "bef");

    ObRetort RemoveAsset (KneeObject *ko);
    ObRetort RemoveAssetResponsibly (const Str &asst_uid, Atmosphere *atm);
    ObRetort RemoveAssetGrip (AssetGrip *ag, bool also_remove_asset = true);

    ObRetort AppendImageAsset (ImageClot *ic, Str label);

    void Empty ();

    AssetGrip *IntersectedAssetGrip (const Vect &frm,
                                     const Vect &thr,
                                     AssetHutch **from_hutch = NULL);

    int64 AssetHoldersCount (KneeObject *asset);

    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);

    virtual ObRetort MzTenderProbe (MzTenderProbeEvent *e, Atmosphere *atm);
    virtual ObRetort MzTenderSolidify (MzTenderSolidifyEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzTenderRescind (MzTenderRescindEvent *e, Atmosphere *atm);

    ObRetort LoadFluoroscopesFromMummy (const Slaw &m);
    Slaw SpewFluoroscopesForMummy ();
};
}
} // bye bye namespaces sluice, oblong

#endif
