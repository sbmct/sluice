#!/bin/bash

WHERE="./"
if [ "$#" -gt "0" ]; then
    WHERE="$1"
fi

if [ ! -d "$WHERE" ]; then
    echo "Sorry, $WHERE is not a directory..."
    exit -1
fi



###
### add the include line if it's missing
###
logInclude="#include <libLoam/c/ob-log.h>"
function addLogInclude()
{
    if [ ! -f "$1" ]; then
        return 0
    fi

    if [ `grep -c "$logInclude" $1` -eq "0" ]; then
        sed "4 i\\
\"$logInclude\"
" $1 > tmp.sed
        mv tmp.sed $1
    fi
}



###
### Replace DBG & WARN macros
###
function replace_DBG_WARN()
{
#    echo "replacing DBG/WARN in $1"
    sed 's/DBG/OB_LOG_DEBUG/;s/[^_]WARN[^a-z]/ OB_LOG_WARNING /' $1 > tmp.sed
    mv tmp.sed $1
}



#for file in $WHERE/*.[hC]
#do

    ###
    ### WARN / DBG
    ###

    # if [ `egrep -c "WARN|DBG" $file` -eq "0" ]; then
    #     continue
    # fi

    # echo "processing file : $file"
    # addLogInclude $file
    # replace_DBG_WARN $file


    ###
    ### PRINTF's
    ###

    # if [ `egrep -c "printf" $file` -eq "0" ]; then
    #     continue
    # fi

    # echo "processing file : $file"

    # sed 's/fprintf (stderr, /OB_LOG_DEBUG (/' $file > tmp.sed

    # sed 's/ printf / OB_LOG_DEBUG /' $file > tmp.sed
    # mv tmp.sed $file

#done



for file in $WHERE/*
do
    if [ `egrep -c "OB_LOG_DIR" $file` -eq "0" ]; then
        continue
    fi

    sed 's/OB_LOG_DIR=\/var\/log\/oblong/OB_LOG_DIR=\/var\/log\/oblong\/sluice/' $file > tmp.sed
    mv tmp.sed $file

done

