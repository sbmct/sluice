#!/bin/sh



LAT=40.07
LON=-82.755
ZOOM=4800

exec p-deposit \
  -d sluice \
  -d 'prot-spec v1.0' \
  -d request \
  -d zoom \
  -i lat:$LAT \
  -i lon:$LON \
  -i level:$ZOOM edge-to-sluice
