#!/bin/sh

platform=`uname`
if [ $platform = "Linux" ]; then
    if initctl list | grep -q "pool_server" && status ob/sluice/pool_server | grep -q "running"; then
        echo "pool-tcp-server running; stopping it"
        stop ob/sluice/pool_server || true
        sleep 4
    fi
fi

#overkill perhaps
killall -9 pool_tcp_server

pool_create() {
  pool="$1"
  size="$2"
  if test "x" = "x$size" 
  then
    size=1048576
  fi

  p-create -z -s "$size" "$pool" || true
}

BIG=104857600

pool_create sluice-to-heart

pool_create edge-to-sluice
pool_create sluice-to-edge "$BIG"

pool_create topo-to-sluice "$BIG"
pool_create sluice-to-topo "$BIG"

pool_create obs-to-sluice "$BIG"
pool_create alerts-to-sluice

pool_create sluice-to-fluoro
pool_create fluoro-to-sluice "$BIG"

pool_create passforward
pool_create mz-from-native
pool_create mz-into-native

pool_create screenswap
pool_create tiles-req

pool_create remote
pool_create remote-data

p-create -z -s 104857600 -i 1048576 sluice-tardis || true

