#!/bin/sh

# Send a URL to sluice to be displayed in a WebThing

test "x" = "x$1" || exec p-deposit \
  -d sluice \
  -d 'prot-spec v1.0' \
  -d request \
  -d web \
  -i url:"$1" \
  edge-to-sluice
