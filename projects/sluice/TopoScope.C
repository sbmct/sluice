
#include "NodeStoreImpl.h"
#include "TopoScope.h"
#include "TopoQuad.h"
#include "SluiceConsts.h"
#include "ConfigSlawStuff.h"
#include "profile.h"

#include <libPlasma/c++/SlawIterator.h>

#include <algorithm>
#include <iostream>
#include <vector>

#ifndef DEBUG
#define BOOST_DISABLE_ASSERT
#endif

#include <boost/assert.hpp>

using namespace oblong::sluice;
using namespace conduce::plasma;

//----------------------------------------------------------------------
// TopoScope
//----------------------------------------------------------------------

TopoScope::TopoScope () {
    auto tq = new TopoQuad;
    tq->AppendVBOInfoUpdatedHook (&TopoScope::vboInfoUpdated, this,
                                  "vbo-update");
    SetGQuad (tq);
    SetSubscriptionUpdateRequired (false);
    subscription.ident = ObQID_ToSlaw (QID ());

    tq->AppendCartoCoordsChangeHook (&TopoScope::CartoCoordsChange, this,
                                     "carto");
    tq->AppendSizeChangeHook (&TopoScope::CartoCoordsChange, this, "size");
}

TopoScope::~TopoScope () { UnregisterSubscription (); }

static const float64 BOUNDS_SCALE_FOR_REQ = 2.0;
static const float64 BOUNDS_SCALE_FOR_CHECK = 1.25;

void TopoScope::ResetSubscribedKinds () {
    subscribedKinds =
        std::set<Str> (enabledKinds.begin (), enabledKinds.end ());
}

ObRetort TopoScope::CartoCoordsChange (GeoQuad *gq) {
    float64 visdeg{0.0};
    GeoRect bounds;
    if (VisibleBounds (bounds).IsSplend () &&
        GlobalVisibleDegrees (visdeg).IsSplend ()) {
        bounds = bounds.ScaleBy (BOUNDS_SCALE_FOR_CHECK);
        subscription.bounds = bounds.ScaleBy (BOUNDS_SCALE_FOR_REQ);
        auto sub_visdeg = bounds.tr.lat - bounds.bl.lat;
        subscription.binning.should_bin = parent->GranularityForVisibleDegrees (
            sub_visdeg, subscription.binning.granularity);
        subscription.visible_bounds = bounds;
        SetSubscriptionUpdateRequired (true);
    }
    return OB_OK;
}

void TopoScope::UpdateSubscribedKinds () {
    subscription.kinds =
        std::vector<Str> (subscribedKinds.begin (), subscribedKinds.end ());
}

void TopoScope::UpdateSubscription () {
    BOOST_ASSERT (~hoff);
    SetSubscriptionUpdateRequired (false);
    UpdateSubscribedKinds ();
    subscription.current_time = parent->SluiceTime ();
    if (parent->IsTimePaused ()) {
        subscription.time_rate = 0.0;
    } else {
        subscription.time_rate = parent->SluiceSecondsPerSecond ();
    }
    hoff->PoolDeposit (
        SluiceConsts::StoreOutLogicalHoseName (),
        Protein (SluiceConsts::SubscriptionCreate (), Slaw (subscription)));
    heartbeat_timer.ZeroTime ();
}

void TopoScope::UnregisterSubscription () {
    if (~hoff) {
        hoff->PoolDeposit (
            SluiceConsts::StoreOutLogicalHoseName (),
            Protein (SluiceConsts::SubscriptionRemove (),
                     Slaw::Map ("ident", ObQID_ToSlaw (QID ()))));
    }
}

void TopoScope::SendHeartbeat () {
    hoff->PoolDeposit (SluiceConsts::StoreOutLogicalHoseName (),
                       Protein (SluiceConsts::SubscriptionHeartbeat (),
                                Slaw::Map ("ident", ObQID_ToSlaw (QID ()))));
    heartbeat_timer.ZeroTime ();
}

struct SubnodeCmp {
    bool operator()(const SubscriptionNode &a,
                    const SubscriptionNode &b) const {
        auto cmp = a.ident.Compare (b.ident);
        if (0 == cmp) {
            // higher sequence numbers first.
            return a.storage_sequence > b.storage_sequence;
        } else {
            return 0 > cmp;
        }
    }
};

const ObTrove<Inculcator *>
GetActiveInculcators (const ObTrove<Inculcator *> &inculcators,
                      const std::set<Str> &kinds) {
    ObTrove<Inculcator *> activeInculcators;
    for (auto inc : inculcators) {
        for (auto kind : inc->Kinds ()) {
            if (kinds.end () !=
                (std::find (kinds.begin (), kinds.end (), kind))) {
                activeInculcators.Append (inc);
            }
        }
    }

    return activeInculcators;
}

ObRetort TopoScope::Inhale (Atmosphere *atmo) {
    if (QuerySubscriptionUpdateRequired ()) {
        UpdateSubscription ();
    } else if (heartbeat_frequency < heartbeat_timer.CurTime ()) {
        SendHeartbeat ();
    }

    if (TopoQuad *tq = dynamic_cast<TopoQuad *> (GQuad ())) {
        GeoRect bounds;
        if (Fluoroscope *mm = MainMap ()) {
            if (mm->Bounds (bounds).IsSplend ()) {
                float64 ratio = mm->Width () / bounds.Width ();
                tq->SetNodeSize (5.0 * logl (ratio));
                tq->SetCurrentZoom (ZoomLevel ());
            }
        }

        if (QueryQuadIsDirty ()) {
            SetQuadIsDirty (false);
            if (!QueryStoreIsSorted ()) {
                SetStoreIsSorted (true);
                std::sort (store.begin (), store.end (), SubnodeCmp{});
            }
            if (!QueryStoreIsCompact ()) {
                SetStoreIsCompact (true);
                Str previous_ident;
                auto f = [&previous_ident](const SubscriptionNode &sn) -> bool {
                    auto out = previous_ident == sn.ident;
                    if (!out) {
                        previous_ident = sn.ident;
                    }
                    return out;
                };
                auto removed = std::remove_if (store.begin (), store.end (), f);
                if (removed != store.end ()) {
                    store.erase (removed, store.end ());
                }
            }
            tq->SetData (store, GetActiveInculcators (parent->Inculcators (),
                                                      enabledKinds));
        }
    }

    return super::Inhale (atmo);
}

// Fluoroscope
void TopoScope::InitializePoolRegistration (Hasselhoff *hoff_) {
    BOOST_ASSERT (hoff_);
    hoff = hoff_;
    hoff->PoolParticipate (SluiceConsts::StoreInLogicalHoseName (), this);

    Slaw qid = ObQID_ToSlaw (QID ());

    auto data_descrips = SluiceConsts::SubscriptionReceiveData (qid);
    std::cerr << "Looking for " << data_descrips.ToPrintableString () << "\n";
    AppendMetabolizer (data_descrips, &TopoScope::MetabolizeSubscriptionData,
                       "subscibe-data");
    AppendMetabolizer (SluiceConsts::SubscriptionReceiveDelta (qid),
                       &TopoScope::MetabolizeSubscriptionDelta,
                       "subscibe-delta");

    super::InitializePoolRegistration (hoff_);
}

void TopoScope::SetNodeStore (NodeStore *ns) {
    parent = ns;
    parent->AppendTimeUpdatedHook (&TopoScope::TimeUpdated, this, "time-up");

    // TopoScope's still get the list of inculcators from the node store, so
    // make sure to add them.
    ns->AddInculcators (config_slaw.MapFind ("inculcators"));
}

ObRetort TopoScope::TimeUpdated (NodeStore *other) {
    if (other == ~parent) {
        SetSubscriptionUpdateRequired (true);
    }
    return OB_OK;
}

void TopoScope::UpdateEnabledKinds (const Slaw config) {
    ObTrove<Str> attrs;
    if (ReadAttribute (config, "enabled-kinds", attrs)) {
        std::set<Str> newEnabledKinds;
        for (auto &&attr : attrs) {
            newEnabledKinds.insert (attr);
        }

        std::vector<Str> adds;
        std::set_difference (newEnabledKinds.begin (), newEnabledKinds.end (),
                             subscribedKinds.begin (), subscribedKinds.end (),
                             std::back_inserter (adds));
        if (!adds.empty ()) {
            subscribedKinds.insert (adds.begin (), adds.end ());
            enabledKinds = std::set<Str> (newEnabledKinds.begin (),
                                          newEnabledKinds.end ());
            SetSubscriptionUpdateRequired (true);
            SetQuadIsDirty (true);
            return;
        }

        std::vector<Str> differences;
        std::set_symmetric_difference (
            newEnabledKinds.begin (), newEnabledKinds.end (),
            enabledKinds.begin (), enabledKinds.end (),
            std::back_inserter (differences));
        if (!differences.empty ()) {
            enabledKinds = std::set<Str> (newEnabledKinds.begin (),
                                          newEnabledKinds.end ());
            SetQuadIsDirty (true);
        }
    }
}

ObRetort TopoScope::SetConfigurationSlaw (const Slaw config) {
    UpdateEnabledKinds (config);
    ObRetort shouldRebaseRetort = super::SetConfigurationSlaw (config);

    if (shouldRebaseRetort.IsSplend ()) {
        if (TopoQuad *tq = dynamic_cast<TopoQuad *> (GQuad ())) {
            tq->PopulateDefaultShadersFromSlaw (
                config.MapFind ("default_shaders"));
            tq->PopulatePigmentsFromSlaw (config.MapFind ("pigments"));
        }

        SetQuadIsDirty (true);
        SetSubscriptionUpdateRequired (true);
    }

    return shouldRebaseRetort;
}
void TopoScope::Rebase () { super::Rebase (); }

void TopoScope::AddRawNode (const SubscriptionNode &sn) {
    if (sn.hasLocation ()) {
        store.push_back (sn);
        store.back ().storage_sequence = node_sequence++;
        SetStoreIsSorted (false);
        SetStoreIsCompact (false);
        SetQuadIsDirty (true);
    }
}

void TopoScope::RemoveRawNode (const Str &ident) {
    if (!QueryStoreIsSorted ()) {
        std::sort (store.begin (), store.end (), SubnodeCmp{});
        SetStoreIsSorted (true);
    }
    SubscriptionNode mort{ident};
    auto found =
        std::lower_bound (store.begin (), store.end (), mort, SubnodeCmp{});
    if (found != store.end ()) {
        store.erase (found);
        SetQuadIsDirty (true);
    }
}

ObRetort TopoScope::MetabolizeSubscriptionDelta (const Protein &pro,
                                                 Atmosphere *) {
    ScopedTimer _ ("Metabolize Delta");
    SubscriptionData update;
    auto ing = pro.Ingests ();
    if (SlawCoerce (ing, update)) {
        for (const auto &rm : update.remove ()) {
            RemoveRawNode (rm);
        }
        if (QueryQuadIsDirty ()) {
            auto new_end =
                std::remove_if (store.begin (), store.end (),
                                [](const SubscriptionNode &sn)
                                    -> bool { return sn.ident.IsEmpty (); });
            store.erase (new_end, store.end ());
        }
        store.reserve (store.size () + update.addCount ());
        for (const auto &add : update.add ()) {
            AddRawNode (add);
        }
    }
    return OB_OK;
}

ObRetort TopoScope::MetabolizeSubscriptionData (const Protein &pro,
                                                Atmosphere *) {
    ScopedTimer _ ("Metabolize Data");
    SubscriptionData update;
    auto ing = pro.Ingests ();
    if (SlawCoerce (ing, update)) {
        store.clear ();
        if (TopoQuad *tq = dynamic_cast<TopoQuad *> (GQuad ())) {
            tq->Clear ();
        }

        store.reserve (store.size () + update.addCount ());
        for (const auto &add : update.add ()) {
            AddRawNode (add);
        }
    }
    return OB_OK;
}

ObRetort TopoScope::vboInfoUpdated (Slaw payload) {
    auto qid = ObQID_ToSlaw (QID ());
    hoff->PoolDeposit (SluiceConsts::VBOOutLogicalHoseName (),
                       Protein{SluiceConsts::VBOInfoPayload (qid), payload});
    hoff->PoolDeposit (SluiceConsts::EdgeOutLogicalHoseName (),
                       Protein{SluiceConsts::VBOInfoUpdated (qid),
                               Slaw::Map ("timestamp", parent->SluiceTime ())});
    return OB_OK;
}
