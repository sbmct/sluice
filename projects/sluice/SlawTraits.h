
/* (c) Conduce Inc. */

#ifndef SLAW_TRAIS_H__
#define SLAW_TRAIS_H__

#include <libPlasma/c++/slaw-traits.h>
#include <libPlasma/c++/Slaw.h>
#include <vector>
#include <map>

#ifndef PLASMA_MAP_ATTR
// Placeholder for a code generation tool for plasma->slaw->plasma
// transformations
#define PLASMA_MAP_ATTR(x)
#define PLASMA_MAP_ATTR_OPTIONAL(x)
#endif

#define DECLARE_SLAW_TRAITS_SPECIALIZATION(NAM)                                \
    namespace oblong {                                                         \
    namespace plasma {                                                         \
    template <> struct slaw_traits<NAM> {                                      \
        typedef NAM type;                                                      \
        static type Null ();                                                   \
        static bool TypeP (bslaw s);                                           \
        static bool Coerce (bslaw s, type &value);                             \
        static slaw Make (const type &value);                                  \
    };                                                                         \
    }                                                                          \
    }

namespace oblong {
namespace plasma {

template <> struct slaw_traits<bslaw> {
    using type = bslaw;
    static type Null () { return nullptr; }
    static bool TypeP (bslaw) { return true; }
    static bool Coerce (bslaw s, type &value) {
        value = s;
        return true;
    }
    static slaw Make (const type &value) { return slaw_dup (value); }
};
}
}

namespace conduce {
namespace plasma {

template <typename T> using ST = oblong::plasma::slaw_traits<T>;

template <typename T> inline slaw SlawMake (const T &t) {
    return ST<T>::Make (t);
}

template <typename T> inline bool SlawIs (bslaw s) { return ST<T>::TypeP (s); }

template <typename T> inline bool SlawIsOrNull (bslaw s) {
    return nullptr == s || SlawIs<T> (s);
}

template <typename T> inline bool SlawCoerce (bslaw s, T &t) {
    return ST<T>::Coerce (s, t);
}

template <typename T>
bool SlawCoerce (const oblong::plasma::Slaw &s, T &value) {
    return ST<T>::Coerce (s.SlawValue (), value);
}

inline int64 SafeListCount (bslaw s) {
    const int64 ret = slaw_list_count (s);
    if (-1 == ret) {
        return 0;
    } else {
        return ret;
    }
}

#define SlawMapHasKey(s___, name___, T___)                                     \
    conduce::plasma::SlawIs<T___> (slaw_map_find_c (s___, name___))

#define SlawMapInto(s___, name___, val___)                                     \
    conduce::plasma::SlawCoerce (slaw_map_find_c (s___, name___), val___)

template <typename T> struct SlawIter {
private:
    bslaw s;
    int64 counter, N;
    T tmp;

    bool checkCurrent () {
        tmp = T{};
        return SlawCoerce (slaw_list_emit_nth (s, counter), tmp);
    }

    void findNext () {
        do {
            ++counter;
        } while (counter < N && !checkCurrent ());
    }

public:
    SlawIter (bslaw s_, int64 c_) : s{s_}, counter{c_}, N{SafeListCount (s_)} {
        if (counter < N) {
            if (!checkCurrent ()) {
                findNext ();
            }
        }
    }

    bool operator!=(const SlawIter &other) const {
        return s != other.s || counter != other.counter;
    }

    T operator*() const { return tmp; }

    SlawIter &operator++() {
        findNext ();
        return *this;
    }

    SlawIter operator++(int) {
        SlawIter out{s, counter};
        findNext ();
        return out;
    }
};

template <> struct SlawIter<bslaw> {
private:
    bslaw s;
    int64 counter;

public:
    SlawIter (bslaw s_, int64 counter_ = 0) : s{s_}, counter{counter_} {}

    bool operator!=(const SlawIter &other) const {
        return s != other.s || counter != other.counter;
    }

    bslaw operator*() const { return slaw_list_emit_nth (s, counter); }

    SlawIter &operator++() {
        ++counter;
        return *this;
    }

    SlawIter operator++(int) {
        SlawIter out{s, counter};
        ++counter;
        return out;
    }
};

/// Iterate through a slaw list in a typed way. Off-type list elements
// -- that is, those that can't be SlawCoerce<T>'d -- are skipped.
// Provides the usual forward iterator interface.
template <typename T> struct SlawContainer {
    typedef SlawIter<T> IterType;
    SlawContainer (bslaw s_ = nullptr) : s{s_} {}
    SlawContainer (oblong::plasma::Slaw s_) : s{s_.SlawValue ()} {}
    bslaw s;
    IterType begin () const { return IterType{s, 0}; }
    IterType end () const {
        if (nullptr == s || slaw_is_nil (s)) {
            return IterType{s, 0};
        } else {
            return IterType{s, SafeListCount (s)};
        }
    }
};
}
}

#define CONDUCE_DECLARE_VECTOR_TRAITS(TYP)                                     \
    namespace oblong {                                                         \
    namespace plasma {                                                         \
    template <> struct slaw_traits<std::vector<TYP>> {                         \
        typedef std::vector<TYP> type;                                         \
        static type Null () { return type{}; }                                 \
        static bool TypeP (bslaw s) {                                          \
            if (!slaw_is_list_or_map (s)) {                                    \
                return false;                                                  \
            }                                                                  \
            for (bslaw x : conduce::plasma::SlawContainer<bslaw> (s)) {        \
                if (!slaw_is_string (x)) {                                     \
                    return false;                                              \
                }                                                              \
            }                                                                  \
            return true;                                                       \
        }                                                                      \
        static bool Coerce (bslaw s, type &value) {                            \
            if (!slaw_is_list_or_map (s)) {                                    \
                return false;                                                  \
            }                                                                  \
            value.reserve (conduce::plasma::SafeListCount (s));                \
            for (bslaw x : conduce::plasma::SlawContainer<bslaw> (s)) {        \
                TYP tmp;                                                       \
                if (!conduce::plasma::SlawCoerce (x, tmp)) {                   \
                    return false;                                              \
                }                                                              \
                value.push_back (tmp);                                         \
            }                                                                  \
            return true;                                                       \
        }                                                                      \
        static slaw Make (const type &value) {                                 \
            auto bu = slabu_new ();                                            \
            for (auto x : value) {                                             \
                slabu_list_add_f (bu, conduce::plasma::SlawMake (x));          \
            }                                                                  \
            return slaw_list_f (bu);                                           \
        }                                                                      \
    };                                                                         \
    }                                                                          \
    }

#define CONDUCE_DECLARE_PAIR_TRAITS(KEY, VAL)                                  \
    namespace oblong {                                                         \
    namespace plasma {                                                         \
    template <> struct slaw_traits<std::pair<KEY, VAL>> {                      \
        typedef std::pair<KEY, VAL> type;                                      \
        static type Null () { return type{}; }                                 \
        static bool TypeP (bslaw s) {                                          \
            if (!slaw_is_cons (s)) {                                           \
                return false;                                                  \
            }                                                                  \
            if (!conduce::plasma::SlawIs<KEY> (slaw_cons_emit_car (s))) {      \
                return false;                                                  \
            }                                                                  \
            if (!conduce::plasma::SlawIs<VAL> (slaw_cons_emit_cdr (s))) {      \
                return false;                                                  \
            }                                                                  \
            return true;                                                       \
        }                                                                      \
        static bool Coerce (bslaw s, type &value) {                            \
            if (!slaw_is_cons (s)) {                                           \
                return false;                                                  \
            }                                                                  \
            return conduce::plasma::SlawCoerce (slaw_cons_emit_car (s),        \
                                                value.first) &&                \
                   conduce::plasma::SlawCoerce (slaw_cons_emit_cdr (s),        \
                                                value.second);                 \
        }                                                                      \
        static slaw Make (const type &value) {                                 \
            return slaw_cons_ff (conduce::plasma::SlawMake (value.first),      \
                                 conduce::plasma::SlawMake (value.second));    \
        }                                                                      \
    };                                                                         \
    }                                                                          \
    }

#define CONDUCE_DECLARE_MAP_TRAITS(KEY, VAL)                                   \
    namespace oblong {                                                         \
    namespace plasma {                                                         \
    template <> struct slaw_traits<std::map<KEY, VAL>> {                       \
        typedef std::map<KEY, VAL> type;                                       \
        static type Null () { return type{}; }                                 \
        static bool TypeP (bslaw s) {                                          \
            if (!slaw_is_map (s)) {                                            \
                return false;                                                  \
            }                                                                  \
            for (bslaw x : conduce::plasma::SlawContainer<bslaw> (s)) {        \
                if (!slaw_is_cons (x)) {                                       \
                    return false;                                              \
                }                                                              \
                if (!conduce::plasma::SlawIs<KEY> (slaw_cons_emit_car (x))) {  \
                    return false;                                              \
                }                                                              \
                if (!conduce::plasma::SlawIs<VAL> (slaw_cons_emit_cdr (x))) {  \
                    return false;                                              \
                }                                                              \
            }                                                                  \
            return true;                                                       \
        }                                                                      \
        static bool Coerce (bslaw s, type &value) {                            \
            if (!slaw_is_map (s)) {                                            \
                return false;                                                  \
            }                                                                  \
            KEY key;                                                           \
            VAL val;                                                           \
            for (bslaw x : conduce::plasma::SlawContainer<bslaw> (s)) {        \
                if (!slaw_is_cons (x)) {                                       \
                    return false;                                              \
                }                                                              \
                if (!(conduce::plasma::SlawCoerce (slaw_cons_emit_car (x),     \
                                                   key) &&                     \
                      conduce::plasma::SlawCoerce (slaw_cons_emit_cdr (x),     \
                                                   val))) {                    \
                    return false;                                              \
                }                                                              \
                value[key] = val;                                              \
            }                                                                  \
            return true;                                                       \
        }                                                                      \
        static slaw Make (const type &value) {                                 \
            auto bu = slabu_new ();                                            \
            for (auto x : value) {                                             \
                slabu_map_put_ff (bu, conduce::plasma::SlawMake (x.first),     \
                                  conduce::plasma::SlawMake (x.second));       \
            }                                                                  \
            return slaw_map_f (bu);                                            \
        };                                                                     \
    };                                                                         \
    }                                                                          \
    }

#endif
