
/* (c)  oblong industries */

#ifndef TRENTON_MAKES_THE_WORLD_TAKES
#define TRENTON_MAKES_THE_WORLD_TAKES

#include "Acetate.h"
#include "WideScreen.h"

#include "MzHandi.h"
#include "MzShove.h"
#include "MzScroll.h"
#include "MzTender.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * Trenton contains the current working Deck(s). In general, it only contains
 * one deck, unless a pilot kicks Mezzanine into a/b mode, then it contains
 * and manages both the a and b decks.
 */
class Trenton : public Acetate,
                public MzShove::Evts,
                public MzHandi::Evts,
                public MzTender::Evts,
                public MzScroll::Evts {
    PATELLA_SUBCLASS (Trenton, Acetate);

private:
    ObRef<WideScreen *> screen;
    Str active_receipt_screen_id;

public:
    Trenton ();
    ~Trenton ();

    void SetWideScreen (WideScreen *ws);

    WideScreen *MainScreen () const { return ~screen; }

    virtual void InitiallyUnfurl (Atmosphere *atm);
    virtual void ArrangeDenizens ();
    virtual void InitializePoolRegistration (Hasselhoff *hoff);
    void SetScreenGeometry (WideScreen *ws,
                            const Vect &loc,
                            float64 wid,
                            float64 hei);

    WideScreen *InitializeWideScreen ();

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm) {
        super::DrawSelf (vf, atm);
    }

    virtual bool PassUnclaimedEventsUpAcceptorAncestry () { return true; }

    //  virtual ObRetort MzHandi (MzHandiEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                           Atmosphere *atm);

    virtual ObRetort MzTenderProbe (MzTenderProbeEvent *e, Atmosphere *atm);
    virtual ObRetort MzTenderSolidify (MzTenderSolidifyEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzTenderRescind (MzTenderRescindEvent *e, Atmosphere *atm);

    // reconciles number of slides with current scroll state
    virtual ObRetort Travail (Atmosphere *atm);
};
}
} // bye bye namespaces sluice, oblong

#endif
