
/* (c)  oblong industries */

#include "Hoboken.h"

#include <libBasement/ob-logging-macros.h>

using namespace oblong::sluice;
using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

void Hoboken::InitiallyUnfurl (Atmosphere *atm) {
    vdf_brws = new VidfluxBrowser ();
    (~vdf_brws)->SetBorderColor (ObColor (0.2, 0.2, 0.2));
    (~vdf_brws)->InitiallyUnfurl (atm);
    (~vdf_brws)->SetParent (this);

    dck_brws = new DeckBrowser ();
    (~dck_brws)->InitiallyUnfurl (atm);
    (~dck_brws)->SetParent (this);

    // AppendChild (~dck_brws);
    // AppendChild (~vdf_brws);
}

void Hoboken::InitializePoolRegistration (Hasselhoff *hoff) {
    if (VidfluxBrowser *vb = OurVidfluxBrowser ())
        vb->InitializePoolRegistration (hoff);
    if (DeckBrowser *db = OurDeckBrowser ())
        db->InitializePoolRegistration (hoff);
}

VideoThing *Hoboken::NewVideoFromSlot (const int64 &slot) {
    if (VidfluxBrowser *vb = OurVidfluxBrowser ())
        return vb->NewVideoThingForImp (vb->NthImp (slot));
    return NULL;
}

VideoThing *Hoboken::NewVideoFromViddleName (const Str &name) {
    if (VidfluxBrowser *vb = OurVidfluxBrowser ())
        return vb->NewVideoThingForViddleName (name);
    return NULL;
}

void Hoboken::ArrangeDenizens () {
    ObRef<FeldGeomPod *> gp_ref = RetrieveGeometry ();
    Vect o = Over ();
    Vect u = Up ();

    if (FeldGeomPod *gp = ~gp_ref) {
        if (VidfluxBrowser *vf = ~vdf_brws) {
            vf->SetSize (2.0 * gp->width + 2.0 * gp->mullion_width,
                         gp->height * .75);
            vf->SetLage (u * (gp->height * .125 - gp->mullion_width));
            vf->ArrangeDenizens ();
        }

        if (DeckBrowser *db = ~dck_brws) {
            db->SetLage (o * ((3.0 * gp->width) + (3.0 * gp->mullion_width)));
            db->SetSize ((3.0 * gp->width) + (2.0 * gp->mullion_width),
                         gp->height);
            db->ArrangeDenizens ();
        }
    }
}

ObRetort Hoboken::MzHandiCondense (MzHandiCondenseEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (e->Intent () != "pointing")
        return OB_EE_NO_ROUTE;

    if (e->IsConducting () || !e->IsIonizing ())
        return OB_OK;

    ObTrove<KneeObject *> targets;
    if (VidfluxBrowser *vfb = OurVidfluxBrowser ())
        targets.Append (vfb);

    return DescentTargets (targets);
}

ObRetort Hoboken::MzHandiEvaporate (MzHandiEvaporateEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (e->Intent () != "pointing")
        return OB_EE_NO_ROUTE;

    if (e->IsConducting () || !e->IsIonizing ())
        return OB_OK;

    ObTrove<KneeObject *> targets;
    if (VidfluxBrowser *vfb = OurVidfluxBrowser ())
        targets.Append (vfb);
    return DescentTargets (targets);
}

ObRetort Hoboken::MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (e->Intent () != "pointing")
        return OB_EE_NO_ROUTE;

    if (e->IsConducting () || !e->IsIonizing ())
        return OB_OK;

    ObTrove<KneeObject *> targets;
    if (VidfluxBrowser *vfb = OurVidfluxBrowser ())
        targets.Append (vfb);
    return DescentTargets (targets);
}

ObRetort Hoboken::MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    if (e->Intent () != "pointing")
        return OB_EE_NO_ROUTE;

    ObTrove<KneeObject *> targets;
    if (VidfluxBrowser *vfb = OurVidfluxBrowser ())
        targets.Append (vfb);
    return DescentTargets (targets);
}

ObRetort Hoboken::MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm) {
    if (!e)
        return OB_ARGUMENT_WAS_NULL;

    ObTrove<KneeObject *> targets;
    if (VidfluxBrowser *vfb = OurVidfluxBrowser ())
        targets.Append (vfb);
    return DescentTargets (targets);
}
