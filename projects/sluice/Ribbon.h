
#pragma once

#include <libLoam/c++/Vect.h>
#include <vector>
#include <libNoodoo/GLMiscellany.h> // for GLfloat?
#include "TopoPigment.h"

namespace oblong {
namespace sluice {

using conduce::sluice::PigmentBuffer;
using oblong::loam::Vect;

//    ^
//    |  normals?
// a --- c
// |     |   ----> direction or ribbon?
// b --- d
class Segment {
private:
    Vect norm_ab, norm_cd;
    Vect ab, cd;

public:
    Segment () {}
    Segment (const Segment &otha) = default;
    Segment &operator=(const Segment &otha) = default;

    void SetNormAB (const Vect v) { norm_ab = v; }
    void SetNormCD (const Vect v) { norm_cd = v; }
    void SetAB (const Vect v) { ab = v; }
    void SetCD (const Vect v) { cd = v; }

    float64 abx () const { return ab.x; }
    float64 aby () const { return ab.y; }
    float64 cdx () const { return cd.x; }
    float64 cdy () const { return cd.y; }

    float64 nabx () const { return norm_ab.x; }
    float64 naby () const { return norm_ab.y; }
    float64 ncdx () const { return norm_cd.x; }
    float64 ncdy () const { return norm_cd.y; }

    const Vect Norm () const {
        const Vect s = cd - ab;
        const Vect n (-s.y, s.x, 0.0);
        return n.Norm ();
    }

    const Vect Direction () const { return (cd - ab).Norm (); }

    const float64 Mag () const { return (cd - ab).Mag (); }

    static void ExpandToSegments (std::vector<Vect> &skeleton,
                                  std::vector<Segment> &segments);

    static GLfloat *WriteToVBO (std::vector<Segment> &segments,
                                GLfloat *gl_mem,
                                float64 texunit);
    // returns the number of vertices added
    static size_t
    WriteToVBO (std::vector<Segment> &, PigmentBuffer &, float64 texunit);
};
}
}
