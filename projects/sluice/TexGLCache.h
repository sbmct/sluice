
#pragma once

#include <libLoam/c++/patella-macros.h>
#include <libLoam/c++/ObTrove.h>
#include <libBasement/ob-hook-troves.h>
#include "gspeak_30_shim.h"

#include TEXGL_INCLUDE

#include "TileID.h"

#include <map>
#include <list>

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::staging;

class TexGLCache {
    PATELLA_CLASS (TexGLCache);

private:
    typedef ObRef<TexGL *> tgl_t;
    typedef std::pair<TileID, tgl_t> store_t;
    typedef std::list<store_t> lru_t;
    typedef std::map<TileID, lru_t::iterator> fwd_t;

    lru_t lru;
    fwd_t fwd;
    unt64 maximum_size;

    void Append (const TileID &tid, tgl_t tgl);

    SINGLE_ARG_HOOK_MACHINERY (TileExpired, const TileID &);

public:
    TexGLCache (const unt64 _mx);
    virtual ~TexGLCache () {}

    void Resize (const unt64 sz);
    void Insert (const TileID &tid, ImageClot *clot);
    const bool Contains (const TileID &tid) const;
    TexGL *Fetch (const TileID &tid);
};
}
}
