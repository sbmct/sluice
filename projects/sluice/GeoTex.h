
#ifndef GEO_TEX_MEX_MMMMM_BURRITOS
#define GEO_TEX_MEX_MMMMM_BURRITOS

#include "geo_utils.h"
#include "GeoProj.h"

#include "gspeak_30_shim.h"

#include <libNoodoo/GLTex.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::sluice;

/**
 * GeoTex is a cylindrically projected geographic texture.
 *
 * Typically, a texture will represent some sub-area of the full latlon domain.
 * However, GeoTex does not require such. It allows for projections to span the
 * edges of valid longitudes (potentially multiple times) and extend beyond the
 * boundary of valid latitudes.
 *
 * This lack of restriction could be useful, for example, in the case where you
 * are generating a texture overlay on a map which is zoomed out such that the
 * continents repeat.
 *
 **/
class GeoTex : public GLTex {
    PATELLA_SUBCLASS (GeoTex, GLTex);

protected:
    LatLon ll_bl, ll_tr;
    ObRef<GeoProj *> projection;

public:
    GeoTex (GeoProj *proj,
            ImageClot *image_clot,
            MipmapPolicy mip_policy = MipOnLoad,
            SizePolicy size_policy = NPOT_OK);
    GeoTex (GeoProj *proj,
            ImageClot *image_clot,
            int internalFormat,
            int format,
            MipmapPolicy mip_policy = MipOnLoad,
            SizePolicy size_policy = NPOT_OK);

    GeoProj *Projection ();
    void SetProjection (GeoProj *proj);

    LatLon LatLonBL () { return ll_bl; }
    LatLon LatLonTR () { return ll_tr; }

    void SetGeoCorners (const LatLon &bl, const LatLon &tr);
    void GeoCorners (LatLon &bl, LatLon &tr) const;
};
}
} /// end namespaces oblong, staging

#endif /// GEO_TEX_MEX_MMMMM_BURRITOS
