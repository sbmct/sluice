
#ifndef CHEESE_IT_ITS_THE_FUZZ_FRAME
#define CHEESE_IT_ITS_THE_FUZZ_FRAME

#include <libBasement/SineVect.h>

#include <libNoodoo/SoftVertexForm.h>

namespace oblong {
namespace sluice {
using namespace oblong::basement;
using namespace oblong::noodoo;

/**
 * FuzzyFrame -->
 *
 *  A FuzzyFrame is an animated border or frame to adorn other FlatThings.
 *
 **/
class FuzzyFrame : public FlatThing {
    PATELLA_SUBCLASS (FuzzyFrame, FlatThing);

protected:
    struct Fuzz : public SoftVertexForm {
        float64 freq_mul;
        SineVect *fuzzVects[10];

        Fuzz () : SoftVertexForm (), freq_mul (1.0) {}
    };

    ObWeakRef<FlatThing *> subject;

    ObTrove<Fuzz *> daFuzz;

public:
    FuzzyFrame (int64 num_fuzzies = 5);

    void FrameSubject (FlatThing *subj);

    /** Behavior setters */

    /// Total thickness of the frame given in mm
    SOFT_MACHINERY (SoftFloat, Thickness);
    void SetThickness (const float64 &thk);

    // /// Fraction of the border thickness with fuzziness
    // SOFT_MACHINERY (SoftFloat, FuzzyFrac);
    // void SetFuzzyFrac (const float64 &ff);

    /// Can be thought of as "breathy-ness"
    SOFT_MACHINERY (SoftFloat, FuzzyFrequency);
    void SetFuzzyFrequency (const float64 &ff);

    /** Stretch determines the inside / outside bias of the fuzz.
        0 --> fully inside
        1 --> fully outside
    */
    SOFT_MACHINERY (SoftFloat, StretchFactor);
    void SetStretchFactor (const float64 &sf);

    SOFT_MACHINERY (SoftColor, FuzzyColor);
    void SetFuzzyColor (const ObColor &clr);

    virtual ObRetort AcknowledgeSizeChange ();
    virtual ObRetort AcknowledgeLocationChange ();
    virtual ObRetort AcknowledgeOrientationChange ();

    virtual ObRetort Inhale (Atmosphere *atm);

private:
    void GinUpFuzzies (const int64 &num_fuzzies);

    void RefreshFuzziesGeometry ();
};
}
} // end namespaces mapper, oblong

#endif /// CHEESE_IT_ITS_THE_FUZZ_FRAME
