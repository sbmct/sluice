
/* (c)  oblong industries */

#ifndef SYSTEM_AREA_51
#define SYSTEM_AREA_51

#include "Acetate.h"
#include "ButtonThing.h"
#include "MzHandi.h"
#include "MzShove.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * The SystemArea contains useful system menus and information, like a button
 * for logging out and visuals about the wands' battery life, health, signal
 * strength, etc.
 */
class SystemArea : public Acetate, public MzShove::Evts, public MzHandi::Evts {
    PATELLA_SUBCLASS (SystemArea, Acetate);

protected:
    /*
     * button that allows the user to close a dossier.
     */
    ObRef<ButtonThing *> logout_button;
    /*
     * shows the title of the currently open dossier
     */
    ObRef<GlyphoString *> user_name;
    ObRef<GlyphoString *> dossier_label;
    ObRef<TWrangler *> anim_wrangler;
    /*
     * sits just below the system area, used for intersection calculations
     * to determine if user is pointing where the elements are "hiding"
     * offscreen
     */
    ObRef<FlatThing *> riser;
    /*
     * keep track of how long it's been since this area was tickled by
     * a OEPointing source.
     */
    FatherTime last_hover_timer;
    /* keep track of how much time has gone by since we last heard from
     * a pointing source.
     */
    FatherTime last_move_timer;
    /* keep track of how much time has gone by since last shove was
     * completed, so as to know when to hide self.
     */
    FatherTime last_shove_timer;
    FatherTime last_riser_timer;

    bool forcibly_disabled;

    FatherTime time_since_idle;

    float64 feld_padding;

    ObUniqueTrove<Str> hoverers;

public:
    SystemArea ();
    virtual ~SystemArea ();

    virtual void InitiallyUnfurl (Atmosphere *atm);

    virtual void ArrangeDenizens ();

    /*
     * Use this to force the SystemArea NEVER to appear on screen. Useful
     * when Mezzanine context changes -- say, to switch to the dossier portal --
     * and the SystemArea shouldn't be available. To
     */
    void ForciblyDisable (bool hard = false);
    void AutonomouslyEnableAtWill ();
    bool IsDisabled ();

    /*
     * Animate and fade up
     */
    void FadeUp ();
    /*
     * Animate and fade down
     */
    void FadeDown ();
    /*
     * Animate and fade down RIGHT NOW
     */
    void FadeDownHard ();

    /*
     * Internally sets the time since last activity to 0
     */
    void ResetIdleTimer ();

    void AssociateProvenance (const Str &prv);
    void DissociateProvenance (const Str &prv);

    void SetUserName (const Str &label);
    Str UserName () const;

    /*
     * Callback for close dossier button
     */
    ObRetort Logout (ButtonThing *bt, Atmosphere *atm);

    ShowyThing *WhackCheck (const Vect &from,
                            const Vect &to,
                            Vect *loc_hit,
                            Vect *wrl_hit,
                            SpaceWhiff **sp_wf);

    virtual ObRetort MzHandiCondense (MzHandiCondenseEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);

    virtual ObRetort MzShoveBegin (MzShoveBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzShoveContinue (MzShoveContinueEvent *e, Atmosphere *atm);
    virtual ObRetort MzShoveEnd (MzShoveEndEvent *e, Atmosphere *atm);

    virtual ObRetort Exhale (Atmosphere *atm);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);
};
}
} // bye bye namespaces sluice, oblong

#endif
