
/* (c)  oblong industries */

#ifndef TWEEZERS_IN_ACTION
#define TWEEZERS_IN_ACTION

#include <libNoodoo/FlatThing.h>

#include "Acetate.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class Tweezers : public Acetate {
    PATELLA_SUBCLASS (Tweezers, Acetate);

protected:
    ObWeakRef<KneeObject *> explicit_event_target;
    FatherTime mort_uhr;
    float64 dismissal_duration;

public:
    Tweezers ();
    virtual ~Tweezers ();

    KneeObject *ExplicitEventTarget () const { return ~explicit_event_target; }
    virtual ObRetort SetExplicitEventTarget (KneeObject *trg) {
        explicit_event_target = trg;
        return OB_OK;
    }

    /// an opportunity for the glyph to fade away, stop bobbling, or
    /// otherwise suspend its visual activity.
    virtual ObRetort Dismiss (bool break_connection = true);
    virtual bool HasBeenDismissed ();

    virtual ObRetort Inhale (Atmosphere *atm);
};
}
} // end: namespaces sluice, oblong

#endif
