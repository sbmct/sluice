
#ifndef PROFILE_H
#define PROFILE_H

#include "gspeak_30_shim.h"

#ifdef NO_PROFILE

#define TRACK_INIT
#define TRACK_BEGIN(x)
#define TRACK_END(x)

class ScopedTimer {
public:
    ScopedTimer (Str x) {}
};

#else

#include FTT_INCLUDE

#define TRACK_INIT FatherTimeTracker::Instance ()->Init (1.0)
#define TRACK_BEGIN(x) FatherTimeTracker::Instance ()->StartTimingRun (x)
#define TRACK_END(x) FatherTimeTracker::Instance ()->EndRun (x)

class ScopedTimer {
protected:
    Str s;

public:
    ScopedTimer (Str _s) : s (_s) { TRACK_BEGIN (s); }

    ~ScopedTimer () { TRACK_END (s); }
};

#endif

#endif // ! PROFILE_H
