
#pragma once

#include "Shader.h"
#include "GeoQuad.h"
#include "NodeStore.h"

#include <map>
#include <set>

namespace oblong {
namespace sluice {

class Node;
class Pigment;

/** A GeoQuad that renders nodes, OpenGL-style */
class NetworkQuad : public GeoQuad {
    PATELLA_SUBCLASS (NetworkQuad, GeoQuad);

protected:
    typedef std::map<unt32, Pigment *> pigmap_t;
    ObRef<ShaderStash *> stash;
    pigmap_t pigments;
    float64 nominal_width, current_zoom;

    Pigment *FindPig (const unt32 id) const;

    ObRef<NodeStore *> node_store;

    std::map<Node *, std::vector<unt32>> existing_mappings;

public:
    NetworkQuad ();

    bool NodeIsShowing (Node *) const;

    void SetNodeStore (NodeStore *);

    void HardReset ();
    void RemoveNode (Node *n);

    void SetPigmentsForNode (Node *n, std::vector<unt32> &ps);
    ObRetort SetupUniforms (Shader *s);
    void SetShaders (const Str pigment, const Str v_src, const Str f_src);

    /// Optional Settings for path pigments
    void SetRibbonLengthScale (const Str pigment, SoftFloat *scale);
    void SetRibbonOffsetSpeed (const Str pigment, SoftFloat *offset);

    void AddPigment (const Str pigment_typ,
                     const Str name,
                     const Str texture_path,
                     SoftColor *color,
                     SoftFloat *size,
                     const float64 minimum_zoom);
    void RemovePigment (const Str name);

    void SetNodeSize (const float64 x) { nominal_width = x; }
    const float64 NominalNodeSize () const { return nominal_width; }

    void SetCurrentZoom (const float64 x);
    const float64 CurrentZoom () const { return current_zoom; }

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

    Slaw DescribePigment (const Str &pigname) const;
};
}
}
