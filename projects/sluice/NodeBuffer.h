
#pragma once

#include <libLoam/c++/AnkleObject.h>
#include <libNoodoo/GLMiscellany.h>
#include "Node.h"

#include <set>

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;

class GeoProj;

using NodeRef = ObRef<Node *>;
struct NodeRefCmp {
    bool operator()(const NodeRef &a, const NodeRef &b) const {
        return ~a < ~b;
    }
};
using NodeSet = std::set<NodeRef, NodeRefCmp>;

class NodeBuffer : public AnkleObject {
    PATELLA_SUBCLASS (NodeBuffer, AnkleObject);

private:
    GLfloat *mem;
    size_t mem_size;
    bool fresh;

    NodeSet nodes;

    void AssureSize ();

public:
    NodeBuffer ();
    virtual ~NodeBuffer ();

    void Add (Node *n);
    void Remove (Node *n);
    void Recalc (GeoProj *, const float64 t);

    const bool IsFresh () const;
    const bool IsFull () const;
    const size_t SizeInBytes () const;
    void Write (GLfloat *dest) const;
    const size_t Count () const;

    static size_t MAXIMUM_NUMBER_OF_NODES;
    static const size_t FLOATS_PER_VERTEX ();
};

class PathBuffer : public AnkleObject {
    PATELLA_SUBCLASS (PathBuffer, AnkleObject);

private:
    GLfloat *mem;
    size_t mem_size, current_size;
    bool fresh;

    NodeSet nodes;

    void AssureSize (const float64 t);

public:
    PathBuffer ();
    virtual ~PathBuffer ();

    void Add (Node *n);
    void Remove (Node *n);
    void Recalc (GeoProj *proj, const float64 tex_wid, const float64 t);

    const bool IsFresh () const;
    const bool IsFull ();
    const size_t SizeInBytes (const float64 t);
    void Write (GLfloat *dest);
    const size_t Count (const float64 t);

    static size_t MAXIMUM_NUMBER_OF_SEGMENTS;
    static const size_t FLOATS_PER_VERTEX ();
};
}
}
