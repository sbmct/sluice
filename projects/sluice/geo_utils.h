
#ifndef GEO_UTILS_MAKE_THE_WORLD_GO_ROUND
#define GEO_UTILS_MAKE_THE_WORLD_GO_ROUND

#include <math.h>
#include <libLoam/c/ob-types.h>
#include <libLoam/c++/Str.h>
#include <algorithm>
#include <iostream>

#include <boost/tuple/tuple.hpp>

#include "SlawTraits.h"

namespace oblong {
namespace sluice {
using namespace oblong::loam;

struct LatLon {
    float64 lat, lon;
    LatLon () : lat (0), lon (0) {}
    LatLon (float64 latitude, float64 longitude)
        : lat (latitude), lon (longitude) {}
    LatLon (const LatLon &otha) : lat (otha.lat), lon (otha.lon) {}
    LatLon &operator=(const LatLon &otha) {
        lat = otha.lat;
        lon = otha.lon;
        return *this;
    }

    const bool operator<(const LatLon &other) const;
    const bool operator<=(const LatLon &other) const;
    const bool operator==(const LatLon &other) const;
    const bool operator!=(const LatLon &other) const;

    LatLon operator+(const LatLon &other) const;
    LatLon operator-(const LatLon &other) const;

    LatLon &operator+=(const LatLon &other);
    LatLon &operator-=(const LatLon &other);
};

inline std::ostream &operator<<(std::ostream &out, const LatLon &ll) {
    out << "<" << ll.lat << ", " << ll.lon << ">";
    return out;
}

struct GeoRect {
    LatLon bl, tr;
    GeoRect () {}
    GeoRect (LatLon b, LatLon t);
    GeoRect (float64 lat_min,
             float64 lon_min,
             float64 lat_max,
             float64 lon_max);
    GeoRect (const GeoRect &otha) = default;
    GeoRect &operator=(const GeoRect &otha) = default;
    GeoRect (GeoRect &&) = default;
    GeoRect &operator=(GeoRect &&) = default;

    // Make sure that bottom and left are actually to the bottom and
    // to the left of top and right.
    GeoRect Untangle () const;

    const bool operator<(const GeoRect &other) const;
    const bool operator==(const GeoRect &other) const;
    const bool operator!=(const GeoRect &other) const;
    const float64 Width () const;
    const float64 Height () const;
    const bool IsBiggerThan (const GeoRect &other) const;
    const bool IsFullyContainedBy (const GeoRect &other) const;
    const GeoRect ScaleBy (const float64 x) const;
    const GeoRect MoveByPct (const float64 dx, const float64 dy) const;

    const GeoRect Intersection (const GeoRect &other) const;
    const bool IsZero () const;
    const bool Contains (const LatLon &ll) const;

    void Quarter (GeoRect &bl, GeoRect &br, GeoRect &tl, GeoRect &tr);
    void Quarter (GeoRect &bl,
                  GeoRect &br,
                  GeoRect &tl,
                  GeoRect &tr,
                  const LatLon midpoint);
    const LatLon Midpoint () const;

    void
    Diff (const GeoRect &old, ObTrove<GeoRect> &pos, ObTrove<GeoRect> &neg);

    static const GeoRect FULL;

    const bool CrossesInternationalDateLine () const;
    boost::tuple<GeoRect, GeoRect> SplitOnDateLine () const;

    Str ToS () const;
};

inline std::ostream &operator<<(std::ostream &out, const GeoRect &geo) {
    out << "[" << geo.bl << "; " << geo.tr << "]";
    return out;
}

#define EPS 0.000000001

inline bool CrossesLine (const LatLon &a,
                         const LatLon &b,
                         const LatLon &c,
                         const LatLon &d) {
    const float64 x1 = a.lon, y1 = a.lat, x2 = b.lon, y2 = b.lat, x3 = c.lon,
                  y3 = c.lat, x4 = d.lon, y4 = d.lat;

    const float64 denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
    const float64 numera = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3);
    const float64 numerb = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);

    /* Are the line coincident? */
    if (fabsl (numera) < EPS && fabsl (numerb) < EPS && fabsl (denom) < EPS)
        return true;

    /* Are the line parallel */
    if (fabsl (denom) < EPS)
        return false;

    /* Is the intersection along the the segments */
    const float64 mua = numera / denom;
    const float64 mub = numerb / denom;

    if (mua < 0.0 || mua > 1.0 || mub < 0.0 || mub > 1.0)
        return false;

    return true;
}

inline bool CrossesRect (const LatLon &a,
                         const LatLon &b,
                         const LatLon &bl,
                         const LatLon &tr) {
    return CrossesLine (a, b, bl, LatLon (tr.lat, bl.lon)) ||
           CrossesLine (a, b, bl, LatLon (bl.lat, tr.lon)) ||
           CrossesLine (a, b, tr, LatLon (tr.lat, bl.lon)) ||
           CrossesLine (a, b, tr, LatLon (bl.lat, tr.lon));
}

#ifndef M_PI
#define M_PI 3.14159265
#endif
#ifndef TWO_M_PI
#define TWO_M_PI (M_PI * 2.0)
#endif
#ifndef HALF_M_PI
#define HALF_M_PI (M_PI / 2.0)
#endif

inline float64 _deg_to_rad (float64 deg) { return deg * M_PI / 180.0; }

inline float64 _rad_to_deg (float64 rad) { return rad * 180.0 / M_PI; }

inline float64 _fmod_no_neg (float64 num, float64 dnom) {
    float64 rem = fmod (num, dnom);
    if (rem < 0)
        rem += dnom;
    return rem;
}

inline v2float64 v2_add (const v2float64 a, const v2float64 b) {
    v2float64 out = {a.x + b.x, a.y + b.y};
    return out;
}
inline v2float64 v2_sub (const v2float64 a, const v2float64 b) {
    v2float64 out = {a.x - b.x, a.y - b.y};
    return out;
}
inline v2float64 v2_mul (const v2float64 a, const v2float64 b) {
    v2float64 out = {a.x * b.x, a.y * b.y};
    return out;
}
inline v2float64 v2_div (const v2float64 a, const v2float64 b) {
    v2float64 out = {a.x / b.x, a.y / b.y};
    return out;
}

inline void printv2 (const v2float64 &v) {
    OB_LOG_DEBUG ("[ %5.2f, %5.2f ]", v.x, v.y);
}

inline void printv3 (const v3float64 &v) {
    OB_LOG_DEBUG ("[ %5.2f, %5.2f, %5.2f ]", v.x, v.y, v.z);
}

inline void printv4 (const v4float64 &v) {
    OB_LOG_DEBUG ("[ %5.2f, %5.2f, %5.2f, %5.2f ]", v.x, v.y, v.z, v.w);
}

/// given normalized [0,1] value
/// returns reverse-projected value in valid mercator latitude range
inline float64 _merc_norm_to_lat (float64 y) {
    float64 yy = ((2.0 * y) - 1.0) * M_PI;
    float64 lat = atan (sinh (yy));
    return lat;
}

/// given latitude in radians
///  - note: must be in valid range --> +/-reverse_project(PI)
/// returns normalized projected value [0,1] over whole valid range
inline float64 _merc_lat_to_norm (float64 rad_lat) {
    float64 yy = log ((sin (rad_lat) + 1.0) / cos (rad_lat));
    yy = (1.0 + yy / M_PI) / 2.0;
    return yy;
}

#define V2_CONST(A)                                                            \
    { A, A };
#define V2_ADD(A, B)                                                           \
    { A.x + B.x, A.y + B.y };
#define V2_SUB(A, B)                                                           \
    { A.x - B.x, A.y - B.y };
#define V2_MUL(A, B)                                                           \
    { A.x *B.x, A.y *B.y };
#define V2_DIV(A, B)                                                           \
    { A.x / B.x, A.y / B.y };
#define V2_POW(A, B)                                                           \
    { A.x ^ B.x, A.y T B.y };

/// note: we're only clamping latitude to avoid bad juju w/ mercator
/// we still require daemons to be smart about wrapping logitudes
// TODO: Make this work with GeoProj and friends.
inline GeoRect ClampToSemiReasonable (GeoRect rect) {
    const float64 merc_lim_n = _rad_to_deg (_merc_norm_to_lat (1.0));
    const float64 merc_lim_s = _rad_to_deg (_merc_norm_to_lat (0.0));
    LatLon bl (std::max (merc_lim_s, rect.bl.lat),
               std::max (-180.0, rect.bl.lon));
    LatLon tr (std::min (merc_lim_n, rect.tr.lat),
               std::min (180.0, rect.tr.lon));
    return GeoRect (bl, tr);
}
}
} /// namespaces sluice, oblong

DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::LatLon);
DECLARE_SLAW_TRAITS_SPECIALIZATION (oblong::sluice::GeoRect);

#endif // GEO_UTILS_MAKE_THE_WORLD_GO_ROUND
