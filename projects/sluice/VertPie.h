
/* (c)  oblong industries */

#ifndef VERT_PIE_IS_SLICING_GOODNESS
#define VERT_PIE_IS_SLICING_GOODNESS

#include <libNoodoo/VertEllipse.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 * a derivative of VertexEllipse that adds the center of the circle as
 * a vertex so that you can draw pie shapes.  You definitely don't want to
 * use this if you want full ellipse shapes
 */
class VertPie : public VertEllipse {
    PATELLA_SUBCLASS (VertPie, VertEllipse);

public:
    VertPie ();
    virtual ~VertPie ();

    /**
     * in this subclass, we can specify the number of vertices
     * to control how smooth or harsh the shape is.
     */
    virtual ObRetort SetNumVertices (const int32 &n);

    /**
     * called automatically whenever the recomp_needed flag is
     * set (set by setter methods above); you'd only
     * need to call this one explicitly if you go 'behind
     * the scenes' to change stuff (which: pray don't). The
     * activity of this method is not to be confused with the
     * per-frame update of the PlentiEllipse's (= PlentiSegment's)
     * constituent SoftVect instances, which is handled automatically
     * by Travail (Atmosphere*).
     */
    virtual void RecomputeVertices ();
};
}
} //   mort, tod, shi; all for namespaces noodoo and oblong

#endif // VERT_PIE_IS_SLICING_GOODNESS
