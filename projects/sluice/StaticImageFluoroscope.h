/* Copyright (C) 2012 Oblong Industries */

#ifndef OBLONG_SLUICE_STATICIMAGEFLUOROSCOPE_H
#define OBLONG_SLUICE_STATICIMAGEFLUOROSCOPE_H

#include "Fluoroscope.h"

#include <libLoam/c++/FatherTime.h>
#include <libBasement/ImageClot.h>
#include <libBasement/SoftColor.h>

namespace oblong {
namespace sluice {

using namespace oblong::basement;

class GeoTex; // prefer forward declarations

/**
 * Show a geolocated image
 *
 * Logic about refreshing the image and what-have-you goes here?
 */
class StaticImageFluoroscope : public Fluoroscope {
    PATELLA_SUBCLASS (StaticImageFluoroscope, Fluoroscope);

private:
    float64 texture_wait_threshold;
    float64 last_refresh_time;
    FatherTime update_timer;
    float64 refresh_frequency;
    GeoRect visible_bounds;

    FLAG_MACHINERY (KnowsVisibleBounds);
    FLAG_MACHINERY (ShouldRefreshPeriodically);
    FLAG_MACHINERY (IgnoreMovement);
    FLAG_MACHINERY (VerboseRequests);
    FLAG_MACHINERY (TreatImageAsStatic);
    FLAG_MACHINERY (DisableNotifications);

    FLAG_MACHINERY (RebaseRequested);
    FatherTime rebase_timer;
    float64 rebase_wait_minimum;

    LatLon full_bl, full_tr;

    GeoTex *gtex;

    SOFT_MACHINERY (SoftColor, NotificationColor);
    FLAG_MACHINERY (WaitingForTexture);

    const bool VisibleSizeHasDoubledOrHalved ();
    const bool ViewportIsNearTheEdgeOfOurTexture ();
    const bool ShouldRebaseDueToMovement ();

public:
    virtual ~StaticImageFluoroscope ();
    StaticImageFluoroscope ();

    virtual ObRetort SetConfigurationSlaw (const Slaw config) override;

    virtual ObRetort AcknowledgeGeoQuadSizeChanged (GeoQuad *) override;
    virtual ObRetort AcknowledgeGeoQuadLocationChanged (GeoQuad *) override;
    virtual ObRetort AcknowledgeCartoCoordsChanged (GeoQuad *) override;

    virtual void InitializePoolRegistration (Hasselhoff *hoff);
    ObRetort MetabolizeTextureUpdate (const Protein &prt, Atmosphere *atm);

    const float64 TextureWaitThreshold () const;
    void SetTextureWaitThreshold (const float64 x);

    void SetFadeInTime (const float64);

    virtual void Rebase () override;

    virtual void
    SetImageClot (ImageClot *clot, const LatLon &bl, const LatLon &tr);

    virtual ObRetort Inhale (Atmosphere *atmo) override;

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atmo) override;

protected:
    virtual void SendTextureRequestToPool (Protein &prot);
    virtual void SendToPool (Protein &prot, Str hose);
    virtual ObRetort RequestProtein (Protein &prot);
    virtual void RequestNewTexture ();
    virtual const bool ShouldRequestNewTexture ();
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_STATICIMAGEFLUOROSCOPE_H
