
#ifndef ATLAS_QUAD_GETS_YOU_WHERE_YOU_WANT_TO_GO
#define ATLAS_QUAD_GETS_YOU_WHERE_YOU_WANT_TO_GO

#include "Fluoroscope.h"

#include <libBasement/SoftVect.h>

namespace oblong {
namespace sluice {

using namespace oblong::basement;

/**
 * AtlasQuad -->
 *
 * AtlasQuad is a representation of a 2D geographic map.
 *
 * - needs to know how to zoom & scale the map based on events.
 *
 **/
class AtlasQuad : public Fluoroscope {
    PATELLA_SUBCLASS (AtlasQuad, Fluoroscope);

private:
    float64 heartbeat_update;
    FatherTime heartbeat_timer;
    Str previous_background;
    bool curtain_is_drawn = false;

    Protein static_layers_config;
    ObUniqueTrove<Fluoroscope *> static_image_trove;

    ObRetort SetupBackground (const Slaw &config);
    void SetupStaticLayers (const Slaw &config);
    ObRetort SetupStaticImage (const Slaw &config);
    ObRetort SetupCurtain (const Slaw &config);
    ObRetort SetupCurtainFromMap (const Slaw &config);
    ObRetort SetupCurtainFromLayers (const Slaw &config,
                                     const Slaw &layer_slaw);
    virtual ObRetort SetupCurtainOnGeoTileQuad (const Slaw &config,
                                                bool draw_the_curtain,
                                                const ObColor &color,
                                                const float64 &fade_time);

    virtual ObRetort SetConfigurationSlawFromCurtain (const Slaw &config,
                                                      bool drawing_curtain,
                                                      const ObColor &color,
                                                      const float64 &fade_time);

    ObRetort AddStaticLayerDefToConfig (Str layer, Slaw layer_def);

    /**
     * Returns a copy of old_config with the state of "layer" toggled.
     * functional-style, so therefore const.
     * Had been stateful-OOP-style, until we realized that SetConfigurationSlaw
     * did a lot more work than we necessarily want to do each time we toggle
     * a static layer's state.
     */
    Slaw ToggleStaticLayerInConfig (Slaw old_config,
                                    const Str &layer,
                                    bool enabled) const;

    /* override of Fluoroscope::RecursivelyClearFluoroscopes
    in order to keep static images when fluoroscopes are cleared */
    virtual ObRetort RecursivelyClearFluoroscopes ();

    /**
     * Used for controling the rate of zoom in Flate.
     */
    float64 scale_constant;

    v2float64 uv_grab;

    virtual void ScaleBy (const float64 scale);
    virtual void PanBy (const float64 &du, const float64 &dv);
    virtual void ZoomTo (const float64 &lat,
                         const float64 &lon,
                         const float64 &scale,
                         const float64 interp_time = 0.5);
    void AdjustForBadPosition ();
    void CorrectForWaddleOvershoot ();
    void CorrectForFlateOvershoot ();
    virtual ObRetort LocalBounds (LatLon &bl, LatLon &tr) const override;

    /// This function will deposit a psa of the current view area
    ObRetort SpewViewExtent ();

    ObRetort PassHandiPassthroughEventToScopes (MzHandiEvent *e,
                                                Atmosphere *atmo);

    SOFT_MACHINERY (SoftVect, CartoMin);
    SOFT_MACHINERY (SoftVect, CartoMax);
    FLAG_MACHINERY (Autopilot);
    FLAG_MACHINERY (LocationHeartbeat);

public:
    AtlasQuad ();
    virtual ~AtlasQuad ();

    virtual void InitializePoolRegistration (Hasselhoff *hoff);
    virtual ObRetort SetConfigurationSlaw (const Slaw config);

    virtual void SetGQuad (GeoQuad *gq);

    /// Static Layer Accessors
    void SetStaticLayersConfig (Protein new_config) {
        static_layers_config = new_config;
    }
    Protein StaticLayersConfig ();
    ObCrawl<Fluoroscope *> StaticLayersCrawl () {
        return static_image_trove.Crawl ();
    }
    virtual float64 GetZoom ();
    SINGLE_ARG_HOOK_MACHINERY (AutopilotStopped, AtlasQuad *);
    ObRetort TargetVisibleBounds (const GeoRect &fluoro_bounds,
                                  GeoRect &out,
                                  float64 &vis_deg) const;

    void InstallFluoroscope (Fluoroscope *f);
    void DoKlugyThingsForMummies ();
    void DecorateFluoroscope (Fluoroscope *f);

    /** SquareUp -->
     *  AtlasQuad enforces that pixels don't get all squishy.
     *  Note that this is just
     *  a wrapper for the SquareUp method in GeoQuad.
     *  Going forward, we should actually
     *  just use that one directly.
     */
    enum Square_Up_Behavior { Keep_Lat = 0, Keep_Lon };
    void SquareUpTexture (Square_Up_Behavior sub = Keep_Lat);

    virtual ObRetort AcknowledgeSizeChange ();

    /// This is used to trigger the RecursivelyUpdateChildScopes whenever
    /// our projection changes. It is passed to our GeoQuad instance
    ObRetort ProjectionChangedCallback (GeoQuad *gq);

    virtual ObRetort Inhale (Atmosphere *atm);

    /**
     * Flags and floats to control scaling behavior.
     *
     * Perhaps ScaleConstant should be a SoftFloat?  I can't imagine wanting
     * to interpolate it...
     */
    float64 QueryScaleConstant () { return scale_constant; }
    void SetScaleConstant (float64 k) { scale_constant = k; }
    FLAG_MACHINERY (ExponentialScale);
    FLAG_MACHINERY (VelocityBasedScaling);

    /** Metabolizers */
    ObRetort MetabolizeAtlasViewRequest (const Protein &zr, Atmosphere *atm);
    ObRetort MetabolizeZoomRequest (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeZoomParameters (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeStaticLayersRequest (const Protein &prt,
                                            Atmosphere *atm);
    ObRetort MetabolizeAddStaticImage (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeRemoveStaticImage (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeRemoveAllStaticImages (const Protein &prt,
                                              Atmosphere *atm);
    ObRetort MetabolizePOIRequest (const Protein &prt, Atmosphere *atm);

    // Let's make sure that we don't do this one.
    virtual ObRetort MetabolizeMoveRequest (const Protein &, Atmosphere *) {
        return OB_OK;
    };

    /// There are some special cases to handle with the map.
    /// we're scaling
    virtual ObRetort MzFlateContinue (MzFlateContinueEvent *e, Atmosphere *atm);
    virtual ObRetort MzFlateEnd (MzFlateEndEvent *e, Atmosphere *atm);
    virtual ObRetort MzFlateAbort (MzFlateAbortEvent *e, Atmosphere *atm);

    /// we're moving
    virtual ObRetort MzWaddleBegin (MzWaddleBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzWaddleContinue (MzWaddleContinueEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzWaddleEnd (MzWaddleEndEvent *e, Atmosphere *atm);
    virtual ObRetort MzWaddleAbort (MzWaddleAbortEvent *e, Atmosphere *atm);

    // gimmie a fluoroscope!
    virtual ObRetort MzTenderSolidify (MzTenderSolidifyEvent *e,
                                       Atmosphere *atm);

    // raw handipoint evt
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiCondense (MzHandiCondenseEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
};
}
} /// end namespaces oblong, staging

#endif /// ATLAS_QUAD_GETS_YOU_WHERE_YOU_WANT_TO_GOS
