
/* (c)  oblong industries */

#ifndef SLUICE_SETTINGS_H
#define SLUICE_SETTINGS_H

#include <libBasement/KneeObject.h>
#include <libPlasma/c/pool_options.h>

/**
* SluiceSettings:
*   This class is used for determining the various configurations of sluice.
*   All configurations are set via a protein loaded at runtime (but hooks are
*   provided to reset these through the drome pool). This class assumes the
*   configuration is stored by the Settings class and uses the defined keys
*   below to determine the given or default value for each configurable setting.
*/

#define TIME_FORMATS_KEY "time-formats"
#define TIME_FORMATS_DEFAULT "%Y-%m-%d %H:%M:%S"

#define MAP_CONFIGURATION_KEY "map"
#define MAP_CONFIGURATION_DEFAULT "map.protein"

#define DEFAULT_FLUOROSCOPES_KEY "scopes"
#define DEFAULT_FLUOROSCOPES_DEFAULT "weather.protein\tfinancials.protein"

#define WANDS_POOL_KEY "wands-pool"

// carry over from mezzanine

#define WINDSHIELD_MAX_ITEMS_KEY "windshield-max-items"
#define WINDSHIELD_MAX_ITEMS_DEFAULT 101

#define VIDDLE_DISPLAY_NAMES_KEY "viddle-display-names"
#define VIDDLE_DISPLAY_NAME_DEFAULT Slaw::Null ()

#define VIDDLE_EVENT_POOLS_KEY "viddle-event-pools"
#define VIDDLE_EVENT_POOLS_DEFAULT Slaw::Null ()

#define MIN_FREE_DISK_SPACE_KEY "min-free-disk-space"
#define MIN_FREE_DISK_SPACE_DEFAULT 50.0 * GIGABYTE

#define FELDSTREAM_PATH_KEY "feldstream-path"
#define FELDSTREAM_PATH_DEFAULT "/opt/oblong/sluice-64-2/bin/feldstream"

#define GST_PLUGIN_PATH_KEY "gst-plugin-path"
#ifndef __APPLE__
#define GST_PLUGIN_PATH_DEFAULT "/opt/oblong/deps/lib/gstreamer-0.10"
#else
#define GST_PLUGIN_PATH_DEFAULT "/opt/oblong/sluice-32-2/lib/gstreamer-0.10"
#endif

#define TOKEN_ENABLED_KEY "token-enabled"
#define TOKEN_ENABLED_DEFAULT true

#define TOKEN_EXPIRY_KEY "token-expiry"
#define TOKEN_EXPIRY_DEFAULT 3600.0

#define BOOKMARKS_FILE_KEY "bookmarks"
#define BOOKMARKS_FILE_DEFAULT "bookmarks.protein"

#define TOPO_LOAD_TIMEOUT_KEY "topo-load-expiry"
#define TOPO_LOAD_TIMEOUT_DEFAULT 60.0

#define NODE_INFO_FELD_KEY "node-info-feld"
#define NODE_INFO_FELD_DEFAULT "right"

#define MAX_NODE_HISTORY_KEY "max-node-history"
#define MAX_NODE_HISTORY_DEFAULT 0

namespace oblong {
namespace sluice {
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class SluiceSettings {
private:
    bool use_external_data_store;
    bool force_vbo_broadcast;

public:
    /** Static accessor for the one sluiceSettings instance */
    static SluiceSettings *SharedSettings ();

    void SetUsesExternalDataStore (bool);
    bool QueryUsesExternalDataStore () const;

    void SetForceVBOBroadcast (bool);
    bool QueryForceVBOBroadcast () const;

    // Static methods for retrieving configuration and resource files via
    // Sluice's standard method, which is to first look in the user defined
    // ob_etc_path or ob_share_path and then look locally for an etc or share
    // folder if no file was found in the user defined folders

    /**
     * Find a configuration file
     *
     * Search order:
     * - $OB_ETC_PATH if it is defined
     * - $GSPEAK_DIR/etc if it is defined
     * - /etc/oblong
     * - ./etc
     */
    static bool FindEtcFile (Str filename, Str &full_path);

    /**
     * Find a resource file
     *
     * Search order:
     * - $OB_SHARE_PATH if it is defined
     * - $GSPEAK_DIR/share if it is defined
     * - /usr/share/oblong
     * - ./share
     */
    static bool FindResourceFile (Str filename, Str &full_path);

    /** Allowable time formats */
    ObTrove<Str> TimeFormats ();

    Str MainMapTemplate ();

    ObTrove<Str> DefaultFluoroscopeTemplates ();

    const bool HasWandsPool () const;
    const Str WandsPool () const;

    const Str GSTPluginPath () const;

    int64 WindshieldMaxItems ();
    Slaw ViddleDisplayNames ();
    Slaw ViddleEventPools ();
    float64 MinimumFreeDiskSpace ();
    Str FeldstreamPath ();
    bool TokenEnabled ();
    float32 TokenExpiry ();
    Str BookmarksFile ();
    float64 TopoLoadExpiry ();

    Str NodeInfoFeld ();

    int MaxNodeHistory ();
};
}
}

#define SSETTINGS SluiceSettings::SharedSettings ()

#endif
