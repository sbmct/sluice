/* (c)  Oblong Industries */

#ifndef SECOND_ORDER_ODE_CHASE_VECT_H
#define SECOND_ORDER_ODE_CHASE_VECT_H

#include <math.h> // for fmin, fmax, sin, cos, exp

#include <libBasement/SoftVect.h>
#include <libBasement/AsympVect.h>

namespace oblong {
namespace basement {
using namespace oblong::plasma;
using namespace oblong::loam;

class SecondOrderChaseVect : public AsympVect {
    PATELLA_SUBCLASS (SecondOrderChaseVect, AsympVect);

    // TODO (mschuresko) : Consider making this "lock when catches up"
    // functionality
    // part of another wrapper ChaseVect (one that wraps the behavior of a
    // simpler, non-locking, ChaseVect).
    /**
     * has_caught_up, locks_when_caught_up and catch_up_thresh are used
     * to control whether this chase vecto "locks" to the target when
     * it "reaches" (gets within catch_up_thresh of) it.
     * By default they are false and this chase vect just chases.
     * There is a sluice use-case for being able to dynamically set
     * locks_when_caught_up t otrue
     */
    bool has_caught_up;
    bool locks_when_caught_up;

    float64 catch_up_thresh;

protected:
    /**
     * Should start out at zero.
     * Should zero itself at any hard set event as well.
     *
     * Cool trick : Set this to some value asynchronously to give the
     * ChaseVect some "impulse" (i.e. to give it a bump of momentum
     * in some direction).
     */
    Vect vel;

    float64 tau, omega;

    /**
     * Total hack to reduce the overshoot in the "bounce=0" case.
     * Set to false if you want this to work like a textbook control system.
     */
    bool half_vel_hack;

    /**
     * For the way in which we're advancing the dynamics, we need to know
     * what the goal position was at the last Inhale.  If we don't know,
     * we just assume it was the same as this inhale.
     * Peeking at the comments for our Inhale function should reveal what
     * effect this has.
     */
    Vect last_goal;
    bool last_goal_valid;

    bool critical; // if the system is criticall damped

    void SetParamsFromTauAndBounce (float64 tau_in, float64 bounce) {
        tau = tau_in;
        if (bounce > 0.001 * tau) {
            omega = bounce;
            critical = false;
        } else {
            critical = true;
        }
        half_vel_hack = true;
        locks_when_caught_up = false; // by default just chase, don't lock
        has_caught_up = false;
        catch_up_thresh = 1.e-2; // default value
    }

public:
    SecondOrderChaseVect () : AsympVect () {
        SetParamsFromTauAndBounce (1.0, 0.0);
        last_goal_valid = false;
    }

    SecondOrderChaseVect (const Vect &goal_v) : AsympVect (goal_v) {
        SetParamsFromTauAndBounce (1.0, 0.0);
        last_goal_valid = false;
    }

    SecondOrderChaseVect (const Vect &goal_v, float32 interp_t)
        : AsympVect (goal_v) {
        SetParamsFromTauAndBounce (interp_t, 0.0);
        last_goal_valid = false;
    }

    SecondOrderChaseVect (const Vect &start_v,
                          const Vect &goal_v,
                          float32 interp_t)
        : AsympVect (start_v, goal_v, interp_t) {
        SetParamsFromTauAndBounce (interp_t, 0.0);
        last_goal_valid = false;
    }

    /**
     * This sets the behavior of the soft without setting the initial position
     * or the goal.  Should only be used if a "SetHard" can be guaranteed prior
     * to any use of this soft for animation.
     */
    SecondOrderChaseVect (float64 interp_t, float64 bounce) : AsympVect () {
        SetParamsFromTauAndBounce (interp_t, bounce);
        last_goal_valid = false;
    }

    /**
     * This is the constructor you're *supposed* to use if you actually want
     * control over interesting second-order behavior from your chase vect.
     */
    SecondOrderChaseVect (const Vect &goal_v, float64 interp_t, float64 bounce)
        : AsympVect (goal_v, interp_t) {
        SetParamsFromTauAndBounce (interp_t, bounce);
        last_goal_valid = false;
    }

    /**
     * This is the other constructor you can use if you actually want
     * control over interesting second-order behavior from your chase vect.
     */
    SecondOrderChaseVect (const Vect &start_v,
                          const Vect &goal_v,
                          float64 interp_t,
                          float64 bounce)
        : AsympVect (start_v, goal_v, interp_t) {
        SetParamsFromTauAndBounce (interp_t, bounce);
        last_goal_valid = false;
    }

    SecondOrderChaseVect (SoftVect *start_v, SoftVect *goal_v, float32 interp_t)
        : AsympVect (start_v, goal_v, interp_t) {
        SetParamsFromTauAndBounce (interp_t, 0.0);
        last_goal_valid = false;
    }

    SecondOrderChaseVect (const SecondOrderChaseVect &otra) : AsympVect (otra) {
        tau = otra.tau;
        omega = otra.omega;
        vel = otra.vel;
        last_goal = otra.last_goal;
        last_goal_valid = otra.last_goal_valid;
    }

    virtual ~SecondOrderChaseVect () {}

    virtual SecondOrderChaseVect *Dup () const {
        return new SecondOrderChaseVect (*this);
    }

    SecondOrderChaseVect &CopyFrom (const SecondOrderChaseVect &otra) {
        super::CopyFrom (otra);
        vel = otra.vel;
        tau = otra.tau;
        omega = otra.omega;
        last_goal = otra.last_goal;
        last_goal_valid = otra.last_goal_valid;
        locks_when_caught_up = otra.locks_when_caught_up;
        has_caught_up = otra.has_caught_up;
        catch_up_thresh = otra.catch_up_thresh;
        return *this;
    }

    void SetLockingBehavior (bool locks_after_catching_up_to_target) {
        locks_when_caught_up = locks_after_catching_up_to_target;
        if (!locks_when_caught_up)
            has_caught_up = false;
    }

    /**
     * catch_up_threshold should probably be like "1" for screen coordinates,
     * and like 0.001 for normalized [-0.5, 0.5] sorts of coordinates.
     */
    void SetLockingBehavior (bool locks_after_catching_up_to_target,
                             float64 catch_up_threshold) {
        catch_up_thresh = catch_up_threshold;
        SetLockingBehavior (locks_after_catching_up_to_target);
    }

    // Why are we still keeping around start_val?
    // It makes marginal sense for the first-order chase vect, and
    // absolutely no sense for this chase vect.
    virtual const Vect &Set (const Vect &v) {
        ShouldFreshenUp ();
        (~goal_val)->Set (v);
        return (~start_val)->SetHard (val);
    }

    virtual const Vect &SetHard (const Vect &v) {
        ShouldFreshenUp ();
        (~goal_val)->SetHard (v);
        (~start_val)->SetHard (v);
        val = v;
        vel = Vect (0.0, 0.0, 0.0);
        return (~start_val)->SetHard (val);
    }

    Vect AddToVel (const Vect &add_to_vel) { return vel += add_to_vel; }

    /**
     * Okay, this is where 90% of the dynamics magic happens.
     * What we do here is a bit funny.
     * We assume that the target moved with constant velocity between the last
     * Inhale and this one (or stayed at the current location if we have no
     *valid
     * value for the goal at the last inhale).
     * Then we simply solve for the differential equation defined by (our
     * motion parameters)
     * between the last Inhale and now.
     * This almost completely defends us from problems with our respiration
     * time step not matching the simulation time step for a more conventional
     * simulation.
     *
     * Note : I have removed the classic "control theory style"
     * constants "kp" and "kd" since the explicit solution of the differential
     * equation comes out quite neatly in terms of "tau" and "bounce".
     * This means that it takes some work to extract the actual differential
     * equation I claim to be approximating, but it also means that the math
     * to step the equation comes out precisely in terms of a set of constants
     * that more naturally describe the behavior of our ChaseVect.
     *
     */
    virtual ObRetort Inhale (Atmosphere *atm) {
        InterpVect::Inhale (atm);
        // Because we are a chasing vector, rather than an animation vector,
        // we are never "done" in the same way a conventional soft would be
        // "done".  Because of this we intentionally ignore any
        // OB_REQUIRES_MORE_WORK return values from our parent class.
        // Failing to do this leads to strange and difficult to debug
        // problems.

        interp_complete = false;

        val = (~start_val)->Val ();
        Vect goal = (~goal_val)->Val ();

        if (!last_goal_valid) {
            last_goal = goal;
            last_goal_valid = true;
        }

        float64 tt = uhr.CurTime ();
        uhr.ZeroTime ();

        // First we approximate the path of the target as a linear ramp.
        double one_over_tt = (1.0 / tt);
        if (tt <= 0.0) {
            OB_LOG_INFO ("elapsed time, tt, should never be zero or negative");
            one_over_tt = 0.0;
        }
        Vect goal_vel = one_over_tt * (goal - last_goal);

        if (half_vel_hack)
            goal_vel *= 0.5;

        // Then we transform into coordinates in which the target sits still
        // at the origin.
        Vect ini_vel = vel - goal_vel; // velocity relative to goal.
        Vect ini_pos = val - goal + tt * goal_vel;

        // Then we do some math.
        // representing everything as A * exp ( -alpha t) * cos (omega t)
        //                          + B * exp ( -tau   t) * sin (omega t)
        // Unless "critical" is true, in which case everything is
        //   A * exp ( -tau   t)
        // + B * exp ( -tau   t) * t

        Vect A, B;

        A = ini_pos;

        if (critical) { // So the derivative of
            //   A * exp ( -tau   t)
            // + B * exp ( -tau   t) * t
            // is
            //   -tau   * A * exp ( -tau   t)
            // + B * exp ( -tau   t) * (1 - tau   * t)
            // So we need B - tau   * A = ini_vel
            // and
            B = tau * A + ini_vel;

            val = (A * exp (-tau * tt)) + (B * exp (-tau * tt) * tt);

            vel = A * (-tau) * exp (-tau * tt) +
                  B * exp (-tau * tt) * (1.0 - tau * tt);
        } else { // So the derivative of
            //   A * exp ( -tau   t) * cos (omega t)
            // + B * exp ( -tau   t) * sin (omega t)
            // is
            //   -tau   * A * exp ( -tau   t) * cos (omega t)
            // + -omega * A * exp ( -tau   t) * sin (omega t)
            // + -tau   * B * exp ( -tau   t) * sin (omega t)
            // +  omega * B * exp ( -tau   t) * cos (omega t)
            // so we need
            // -tau   * A + omega * B = ini_vel
            B = (tau * A + ini_vel) * (1.0 / omega);

            val = A * exp (-tau * tt) * cos (omega * tt) +
                  B * exp (-tau * tt) * sin (omega * tt);

            vel = A * exp (-tau * tt) *
                      (-tau * cos (omega * tt) - omega * sin (omega * tt)) +
                  B * exp (-tau * tt) *
                      ((-tau * sin (omega * tt)) + omega * cos (omega * tt));
        }

        // Then we transform back.
        vel += goal_vel;
        val += goal;

        if (locks_when_caught_up) {
            if (has_caught_up)
                val = (~goal_val)->Val ();
            else if (val.DistFrom ((~goal_val)->Val ()) < catch_up_thresh) {
                has_caught_up = true;
                // Uncomment this OB_LOG_DEBUG if you want to check whether
                // you're
                // setting catch_up_thresh correctly.
                // OB_LOG_DEBUG (stderr, "Lock on!\n");
            }
        }

        // Perhaps we should stop copying things to and from start_val
        // and just use "val" for everything.
        (~start_val)->SetHard (val);

        needs_freshening = false;
        changed_during_this_quantum = true;

        last_goal = goal;
        return OB_EXPENDED_EFFORT;
    }
};
}
} // so passes the musky breath of namespace basement and namespace oblong

#endif // SECOND_ORDER_ODE_CHASE_VECT_H
