
/* (c)  oblong industries */

#ifndef THE_FLIGHT_OF_THE_NAVIGATOR
#define THE_FLIGHT_OF_THE_NAVIGATOR

#include "Tweezers.h"
#include "MzHandi.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class Navigator : public Tweezers, public MzHandi::Evts {
    PATELLA_SUBCLASS (Navigator, Tweezers);

private:
    Vect grab_origin;
    Vect glyph_origin;
    Vect targ_orig_loc;

    VisiFeld *feld_origin;

    bool am_conducting;
    bool am_navigating;

    v2float64 grab_rel_loc;

    ObWeakRef<FlatThing *> target;

    // list of all living Navigators
    static ObUniqueTrove<Navigator *, WeakRef> all_navigator_trove;

public:
    Navigator (FlatThing *ft);
    virtual ~Navigator ();

    ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e, Atmosphere *atm);
    ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                   Atmosphere *atm);

    ObRetort
    ReturnStroke (const Slaw &news, ElectricalEvent *ee, Atmosphere *atm);

    virtual ObRetort Travail (Atmosphere *atm);
};
}
} // bye bye namespaces sluice, oblong

#endif // THE_FLIGHT_OF_THE_NAVIGATOR
