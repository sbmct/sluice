

#ifndef IS_IT_SPLEND_OR_NOT
#define IS_IT_SPLEND_OR_NOT

#include <libLoam/c/ob-log.h>
#include <libLoam/c++/Str.h>
#include <libLoam/c++/ObRetort.h>

#define ASSURE_SPLEND(wat, x)                                                  \
    do {                                                                       \
        ObRetort _t = (x);                                                     \
        if (!_t.IsSplend ()) {                                                 \
            OB_LOG_DEBUG ("%s",                                                \
                          (Str (wat) + ": " + _t.Description ()).utf8 ());     \
            return _t;                                                         \
        }                                                                      \
    } while (false)

#endif
