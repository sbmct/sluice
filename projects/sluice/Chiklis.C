#include "Chiklis.h"
#include "Hasselhoff.h"
#include "SluiceConsts.h"
#include "Windshield.h"
#include "YaroSneeze.h"

using namespace oblong::sluice;

static const double DEFAULT_CHIKLIS_TIMEOUT = 3600.0;

ObRetort TokenExpiryTrigger::Fire (KneeObject *self, Atmosphere *atm) {
    ObRetort ret;
    if (Chiklis *chk = dynamic_cast<Chiklis *> (self)) {
        if (!chk)
            return OB_UNKNOWN_ERR;
        super::Fire (self, atm);
        OB_LOG_INFO ("%s is logging out due to timeout",
                     chk->CurrentUserId ().utf8 ());
        // PSA - logging out due to timeout

        ret = chk->Logout ();
    }
    return ret;
}

Chiklis::Chiklis ()
    : default_timeout (DEFAULT_CHIKLIS_TIMEOUT), token (""), user_id (""),
      user_name (""), tot (new TokenExpiryTrigger) {
    AppendTrigger (tot);
    SetEnabled (true);
}

void Chiklis::InitializePoolRegistration (Hasselhoff *hoff) {
    if (!hoff)
        return;
    hoff->PoolParticipate (SluiceConsts::EdgeInLogicalHoseName (), this);

    AppendMetabolizer (SluiceConsts::AuthLoginRequestDescrips (),
                       &Chiklis::MetabolizeLogin);
    AppendMetabolizer (SluiceConsts::AuthLogoutRequestDescrips (),
                       &Chiklis::MetabolizeLogout);
    AppendMetabolizer (SluiceConsts::AuthRequestCurrentCredsDescrips (),
                       &Chiklis::MetabolizeRequestCurrentCreds);
}

ObRetort Chiklis::MetabolizeLogin (const Protein &prt, Atmosphere *atmo) {
    ObRetort ret;
    Slaw ing = prt.Ingests ();

    Str tok, id, nam;
    if (ing.Find ("token").Into (tok) && ing.Find ("user-id").Into (id)) {
        ing.Find ("user-name").Into (nam);
        float32 expiry = 0;
        if (ing.Find ("expiry").Into (expiry))
            ret = Login (id, nam, tok, expiry);
        else
            ret = Login (id, nam, tok);
    } else
        ret = OB_INVALID_ARGUMENT;

    return ret;
}

ObRetort Chiklis::MetabolizeLogout (const Protein &prt, Atmosphere *atmo) {
    return Logout ();
}

Slaw Chiklis::CurrentCredsSlaw () const {
    return Slaw::Map ("token", token, "user-id", user_id, "user-name",
                      user_name);
}

ObRetort Chiklis::MetabolizeRequestCurrentCreds (const Protein &prt,
                                                 Atmosphere *atmo) {
    if (YaroSneeze *sneeze = ClosestParent<YaroSneeze> ()) {
        if (Hasselhoff *hoff = sneeze->OurHasselhoff ()) {
            Protein p (SluiceConsts::AuthCurrentCredsReponseDescrips (),
                       CurrentCredsSlaw ());
            Str hose = SluiceConsts::EdgeOutLogicalHoseName ();
            hoff->PoolDeposit (hose, p);
        }
    }
    return OB_OK;
}

void Chiklis::SetDefaultTimeout (float32 t) { default_timeout = t; }

ObRetort Chiklis::Logout () {
    if (!QueryEnabled ())
        return OB_NOTHING_TO_DO;

    if (!user_id.IsEmpty ()) {
        OB_LOG_INFO ("%s is logged out", user_id.utf8 ());
        // PSA - logged out

        user_id.SetToEmpty ();
        user_name.SetToEmpty ();
        token.SetToEmpty ();
        tot->Suspend ();

        if (YaroSneeze *yar = ClosestParent<YaroSneeze> ())
            if (Windshield *wnd = yar->OurWindshield ())
                if (SystemArea *sa = wnd->OurSystemArea ()) {
                    sa->SetUserName ("");
                    sa->AutonomouslyEnableAtWill ();
                }
        return OB_OK;
    }

    return OB_NOTHING_TO_DO;
}

ObRetort Chiklis::Login (const Str &uid, const Str &nam, const Str &tok) {
    return Login (uid, nam, tok, default_timeout);
}

ObRetort Chiklis::Login (const Str &uid,
                         const Str &nam,
                         const Str &tok,
                         float32 expiry) {
    if (!QueryEnabled ())
        return OB_NOTHING_TO_DO;

    if (uid.IsEmpty () || tok.IsEmpty () || expiry <= 0)
        return OB_INVALID_ARGUMENT;

    if (!user_id.IsEmpty ())
        Logout ();

    user_id = uid;
    user_name = nam;
    token = tok;
    tot->Set (expiry);

    OB_LOG_INFO ("%s is logged in", user_id.utf8 ());
    // PSA - logged in

    if (YaroSneeze *yar = ClosestParent<YaroSneeze> ())
        if (Windshield *wnd = yar->OurWindshield ())
            if (SystemArea *sa = wnd->OurSystemArea ()) {
                sa->SetUserName (user_name);
                sa->AutonomouslyEnableAtWill ();
            }

    return OB_OK;
}

Protein Chiklis::DecorateProtein (const Protein &p) {
    if (!QueryEnabled ())
        return p;
    Slaw ing = p.Ingests ();
    if (!token.IsEmpty ())
        ing = ing.MapPut ("token", token);
    return Protein (p.Descrips (), ing);
}
