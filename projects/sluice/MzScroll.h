
/* (c)  oblong industries */

#ifndef MEZZANINE_SCROLL_EVENT
#define MEZZANINE_SCROLL_EVENT

#include <libGanglia/ElectricalEvent.h>

namespace oblong {
namespace sluice {
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class MzScrollEvent : public ElectricalEvent {
    PATELLA_SUBCLASS (MzScrollEvent, ElectricalEvent);
    OE_MISCY_MAKER (MzScroll, Electrical, "scroll");

public:
    float64 scroll_unit_delta;

public:
    MzScrollEvent (KneeObject *orig_ko = NULL);

    float64 ScrollDelta () const;
    void SetScrollDelta (const float64 &f);

    virtual void SynthesizeInternalProtein ();
    virtual ObRetort AnalyzeInternalProtein ();
};

class MzScrollProbeEvent : public MzScrollEvent {
    PATELLA_SUBCLASS (MzScrollProbeEvent, MzScrollEvent);
    OE_MISCY_MAKER (MzScrollProbe, MzScroll, "probe");
    MzScrollProbeEvent (KneeObject *orig_ko = NULL) : MzScrollEvent (orig_ko) {}
};

class MzScrollBeginEvent : public MzScrollEvent {
    PATELLA_SUBCLASS (MzScrollBeginEvent, MzScrollEvent);
    OE_MISCY_MAKER (MzScrollBegin, MzScroll, "begin");
    MzScrollBeginEvent (KneeObject *orig_ko = NULL) : MzScrollEvent (orig_ko) {}
};

class MzScrollContinueEvent : public MzScrollEvent {
    PATELLA_SUBCLASS (MzScrollContinueEvent, MzScrollEvent);
    OE_MISCY_MAKER (MzScrollContinue, MzScroll, "continue");
    MzScrollContinueEvent (KneeObject *orig_ko = NULL)
        : MzScrollEvent (orig_ko) {}
};

class MzScrollEndEvent : public MzScrollEvent {
    PATELLA_SUBCLASS (MzScrollEndEvent, MzScrollEvent);
    OE_MISCY_MAKER (MzScrollEnd, MzScroll, "end");
    MzScrollEndEvent (KneeObject *orig_ko = NULL) : MzScrollEvent (orig_ko) {}
};

class MzScrollEventAcceptorGroup
    : public MzScrollProbeEvent::MzScrollProbeAcceptor,
      public MzScrollBeginEvent::MzScrollBeginAcceptor,
      public MzScrollContinueEvent::MzScrollContinueAcceptor,
      public MzScrollEndEvent::MzScrollEndAcceptor {};

class MzScroll {
public:
    typedef MzScrollEventAcceptorGroup Evts;
};
}
} // a bittersweet end for namespaces sluice and oblong

DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED (oblong::sluice::MzScroll::Evts);

#endif
