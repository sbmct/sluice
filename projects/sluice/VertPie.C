
/* (c)  oblong industries */

#include "VertPie.h"

using namespace oblong::sluice;
using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

VertPie::VertPie () : VertEllipse () {}

VertPie::~VertPie () {}

ObRetort VertPie::SetNumVertices (const int32 &n) {
    return super::SetNumVertices (n + 1);
    // int32 nv = n;
    // if (n < 3)
    //   nv = 3;
    // nv+=2;
    // if (nv == num_vertices)
    //   return OB_OK;
    // EnsureRoomFor (nv + 2);
    // num_vertices = nv;
    // recomp_needed = true;
    // return OB_OK;
}

void VertPie::RecomputeVertices () {
    Vect c = Center ();
    Vect min = MinorAxis ();
    Vect maj = MajorAxis ();
    float ang0 = StartAngle ();
    float ang1 = StopAngle ();
    float delta_th;

    recomp_needed = false;

    if (ang0 > ang1) {
        float tmp = ang0;
        ang0 = ang1;
        ang1 = tmp;
    }

    if (ang1 - ang0 >= 2.0 * M_PI) {
        SetIsClosed (true);
        delta_th = 2.0 * M_PI / (float)(num_vertices - 1);
    } else {
        SetIsClosed (false);
        delta_th = (ang1 - ang0) / (float)(num_vertices - 2);
    }

    vertices[0].Set (c);
    for (int q = 1; q < num_vertices; q++) {
        vertices[q].Set (c + cos (ang0) * maj + sin (ang0) * min);
        ang0 += delta_th;
    }
}
