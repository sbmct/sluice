
/* (c)  oblong industries */

#ifndef HASSELHOFF_KEEPS_YOU_FROM_DROWNING
#define HASSELHOFF_KEEPS_YOU_FROM_DROWNING

#include <libBasement/KneeObject.h>

#include <libLoam/c++/ObRetort.h>
#include <libLoam/c++/ObMap.h>
#include <libLoam/c++/ObUniqueTrove.h>

#include <libPlasma/c++/Pool.h>
#include <libPlasma/c++/Hose.h>
#include <libPlasma/c++/HoseGang.h>
#include <libPlasma/c++/Slaw.h>
#include <libPlasma/c++/Protein.h>

namespace oblong {
namespace sluice {

using namespace oblong::plasma;
using namespace oblong::basement;
using namespace oblong::loam;

class Hasselhoff : public KneeObject {
    PATELLA_SUBCLASS (Hasselhoff, KneeObject);

public:
    enum Skim_Ilk {
        Slurp_To_Initial_Newest_Idx,
        Slurp_Fixed_Number,
        Slurp_Single,
        Drain,
        Use_Default
    };

private:
    struct HasselGuts : AnkleObject {
        Str pool_url;
        Skim_Ilk skimmer_type;
        ObRef<Hose *> jose;
        HasselGuts (const Str &url)
            : AnkleObject (), pool_url (url), skimmer_type (Use_Default) {}
    };

    ObMap<Str, HasselGuts *> logical_to_guts;
    ObMap<KneeObject *, ObTrove<HasselGuts *> *, WeakRef, UnspecifiedMemMgmt>
        hose_pairings;

    /// This defaults to Drain
    Skim_Ilk default_skimmer_type;

    /// This defaults to 15
    int max_proteins_to_skim;

    ObTrove<Str> noted_deposit_failures;
    bool report_failures;

public:
    Hasselhoff ();

    virtual ~Hasselhoff ();

    void Init ();

    /// change to return ObRetort and fail if the logical name already exists
    /// add function to ChangeLogicalPoolName
    virtual ObRetort SetLogicalPoolName (Str logical_hose_name, Str pool_name);
    virtual ObRetort PoolParticipate (const Str &logical_hose_name,
                                      KneeObject *for_whom);
    virtual ObRetort_DepositInfo PoolDeposit (const Str &logical_hose_name,
                                              const Protein &prt);
    virtual ObRetort PoolWithdraw (const Str &logical_hose_name,
                                   KneeObject *for_whom);

    void SetDefaultSkimIlk (Skim_Ilk ilk) { default_skimmer_type = ilk; }
    void SetSkimIlkForLogicalPool (Str logical_hose_name, Skim_Ilk type);
    void SetMaximumNumberProteinsToSkim (int num) {
        max_proteins_to_skim = num;
    }

    ObRetort SkimPools (Atmosphere *atm);

    ObRetort SlurpToInitialIdx (Hose *jose, Atmosphere *atm);
    ObRetort SlurpFixedNumber (Hose *jose, Atmosphere *atm);
    ObRetort SlurpSingle (Hose *jose, Atmosphere *atm);
    ObRetort DrainHose (Hose *jose, Atmosphere *atm);

    /**
     * Attempts to close all the hoses this hasselhoff has open.
     * This is not for general use, but is useful for the special case
     * where we're exiting in a way where the hasselhoff won't be deleted,
     * but we want to make sure its hoses get closed.
     */
    virtual void WithdrawFromAllPoolsAndDisposeOfAutoDisposables ();
};
}
} // so long sluicer

#endif
