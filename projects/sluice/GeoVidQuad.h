
#ifndef GEO_VID_QUAD_SEES_THE_WORLD_THROUGH_FF007F_TINTED_GLASSES
#define GEO_VID_QUAD_SEES_THE_WORLD_THROUGH_FF007F_TINTED_GLASSES

#include "GeoVid.h"
#include "GeoQuad.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;

/**
 * GeoVidQuad -->
 *
 * GeoVidQuad is a simple subclass of GeoQuad which knows how to map a
 * texture to its geometry. It allows for the basic texture options like
 * stretch, keepwidth, keepheight, etc., but it also accepts GeoTex
 * textures and will keep them geo-aligned as the GeoQuad projection
 * changes.
 *
 **/

class GeoVidQuad : public GeoQuad {
    PATELLA_SUBCLASS (GeoVidQuad, GeoQuad);

protected:
    ObRef<GeoVid *> geo_vid;

public:
    GeoVidQuad ();

    void SetGeoVid (GeoVid *video);

    GeoVid *GetGeoVid ();

    virtual ObRetort AcknowledgeUVXformChange ();

    virtual void InstallLoc (SoftVect *sv);
    virtual void InstallNorm (SoftVect *sv);
    virtual void InstallOver (SoftVect *sv);

    virtual void InstallWidth (SoftFloat *sf);
    virtual void InstallHeight (SoftFloat *sf);
    virtual void InstallVerticalAlignment (SoftFloat *sf);
    virtual void InstallHorizontalAlignment (SoftFloat *sf);

    virtual ObRetort AcknowledgeSizeChange ();
    virtual ObRetort AcknowledgeLocationChange ();

private:
    /**
     * Installs the Softs for Loc, Norm, Over, etc into child GeoVid.
     */
    void WireUpSofts ();

    /// for internal use - should be called whenever the projection is updated.
    /// or, if we want to support GeoProjection while maintaining the image
    /// aspect
    /// ratio, it will need to be called when the size changes as well.
    void RecalculateTexCoors ();
};
}
} /// end namespaces oblong, staging

#endif /// GEO_VID_QUAD_SEES_THE_WORLD_THROUGH_FF007F_TINTED_GLASSES
