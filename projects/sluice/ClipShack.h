
#ifndef CLIP_SHACK_PANELING
#define CLIP_SHACK_PANELING

#include <libNoodoo/LocusThing.h>
#include <libBasement/SoftFloat.h>
#include <libBasement/SWrangler.h>
#include <libBasement/TWrangler.h>

#include <libLoam/c++/ObRef.h>

#include <vector>

/**
  Clipshack:  Stolen from Lauren McCarthy's work
      This class defines a cubic phsyical region in which its
      children can draw.  Any part of a child outside this region
      is clipped.  It follows similar setup/functionality as a TexQuad.
*/

namespace oblong {
namespace sluice {
using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::noodoo;

class ClipState {
private:
    GLenum plane;
    GLdouble equation[4];
    GLboolean enabled;

public:
    ClipState (GLenum _plane);

    void Save ();
    void Restore ();
};

class ClipShack : public LocusThing {
    PATELLA_SUBCLASS (ClipShack, LocusThing);

protected:
    bool isClippingDepth;

    ObRef<SWrangler *> scale_wrang;
    ObRef<TWrangler *> loc_wrang;
    std::vector<ClipState> clip_states;

public:
    SOFT_MACHINERY (SoftFloat, DimensionWidth);
    SOFT_MACHINERY (SoftFloat, DimensionHeight);
    SOFT_MACHINERY (SoftFloat, DimensionDepth);
    SOFT_MACHINERY (SoftFloat, VerticalAlignment);
    SOFT_MACHINERY (SoftFloat, HorizontalAlignment);
    SOFT_MACHINERY (SoftFloat, DepthAlignment);

    bool am_clipping;
    FLAG_MACHINERY_FOR (ShouldClip);

public:
    struct V {
        enum Align { Bottom = 0, Center = 1, Top = 2 };
    };
    struct H {
        enum Align { Left = 0, Center = 1, Right = 2 };
    };
    struct D {
        enum Align { Front = 0, Center = 1, Back = 2 };
    };

    ClipShack ();

    ObRetort PreDraw (VisiFeld *feld, Atmosphere *atm);
    ObRetort PostDraw (VisiFeld *feld, Atmosphere *atm);

    virtual void ApplyClippers ();
    virtual void UnapplyClippers ();

    void SetDimensions (const Vect &d);
    void SetDimensions (float x, float y, float z);
    void SetDimensionsHard (const Vect &d);
    void SetDimensionsHard (float x, float y, float z);

    void AppendTarget (ShowyThing *t, int64 ind = -1);
    void RemoveTarget (ShowyThing *t);

    void SetScaleCenter (const Vect &loc);
    void SetIsClippingDepth (bool t);
    bool QueryIsClippingDepth ();
    const Vect &ScaleCenter () const;

    void InstallScale (SoftVect *sof_s);
    void SetScale (const Vect &sv);
    void SetScale (float s);
    void AddToScale (const Vect &sv);
    void AddToScale (float s);
    const Vect &Scale () const;

    void InstallScroll (SoftVect *sof_s);
    void SetScroll (const Vect &loc);
    void SetScrollHard (const Vect &loc);
    void AddToScroll (const Vect &off);
    const Vect &Scroll () const;
    const Vect &ScrollGoal () const;

    void Reset ();
    void ResetHard ();

    virtual void SetLoc (const Vect &p);
    virtual void
    SetLoc (const Vect &p, V::Align vrt, H::Align hrz, D::Align dep);
    virtual void SetLoc (const Vect &p, float64 vrt, float64 hrz, float64 dep);
    virtual void SetLocHard (const Vect &p);
    virtual void
    SetLocHard (const Vect &p, V::Align vrt, H::Align hrz, D::Align dep);
    virtual void
    SetLocHard (const Vect &p, float64 vrt, float64 hrz, float64 dep);

    /// family of alignment-aware location queries. Given two alignment
    /// offsets, return a Vect specifying a point inside the FlatThing.
    ///
    virtual Vect LocOf (V::Align vrt, H::Align hrz, D::Align dep) const;
    virtual Vect LocOf (float64 vrt, float64 hrz, float64 dep) const;
    virtual Vect LocGoalValOf (V::Align vrt, H::Align hrz, D::Align dep) const;
    virtual Vect LocGoalValOf (float64 vrt, float64 hrz, float64 dep) const;

    /// family of functions managing the alignment that will be used to
    /// calculate this FlatThing's layout. Horizontal and Vertical
    /// alignment are governed by independent softs, which may be
    /// queried and installed as usual.
    ///
    /// InstallVerticalAlignment ()    InstallHorizontalAlignment ()
    ///
    /// VerticalAlignment ()      VerticalAlignmentGoalVal ()
    /// HorizontalAlignment ()    HorizontalAlignmentGoalVal ()
    ///
    virtual void SetAlignment (V::Align vrt, H::Align hrz, D::Align dep);
    virtual void SetAlignment (float64 vrt, float64 hrz, float64 dep);
    virtual void SetVerticalAlignment (V::Align vert);
    virtual void SetVerticalAlignment (float64 vert);
    virtual void SetHorizontalAlignment (H::Align hrz);
    virtual void SetHorizontalAlignment (float64 hrz);
    virtual void SetDepthAlignment (D::Align dep);
    virtual void SetDepthAlignment (float64 dep);
    virtual void SetAlignmentHard (V::Align vrt, H::Align hrz, D::Align dep);
    virtual void SetAlignmentHard (float64 vrt, float64 hrz, float64 dep);
    virtual void SetVerticalAlignmentHard (V::Align vrt);
    virtual void SetVerticalAlignmentHard (float64 vrt);
    virtual void SetHorizontalAlignmentHard (H::Align hrz);
    virtual void SetHorizontalAlignmentHard (float64 hrz);
    virtual void SetDepthAlignmentHard (D::Align dep);
    virtual void SetDepthAlignmentHard (float64 dep);
    virtual int VerticalAlignmentEnum ();
    virtual int HorizontalAlignmentEnum ();
    virtual int DepthAlignmentEnum ();
};
}
} /// namespaces sluice , oblong

#endif
