/* Copyright (C) 2011 Oblong Industries */

#ifndef OBLONG_SLUICE_DATEPARSER_H
#define OBLONG_SLUICE_DATEPARSER_H

#include <libLoam/c++/Str.h>
#include <libLoam/c++/ObTrove.h>
#include <libPlasma/c++/Slaw.h>
#include <boost/date_time.hpp>

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::plasma;

/**
 * Parse dates and times from strings
 *
 */
class DateParser {
protected:
    ObTrove<Str> parsers;
    boost::local_time::local_time_input_facet *input_facet;
    std::locale locale;

    bool ParseStandard (const std::string &s, float64 &output);

public:
    DateParser ();
    ObRetort AppendDateFormat (const Str &fmt);
    ObRetort Parse (const Str &dstr, float64 &output);
};
}
} // namespace oblong::sluice

#endif // OBLONG_SLUICE_DATEPARSER_H
