
/* (c)  oblong industries */

#ifndef EXOSKELETON_PROTECTS_YOUR_FRAIL_INSECT_BODY
#define EXOSKELETON_PROTECTS_YOUR_FRAIL_INSECT_BODY

#include <libBasement/InterpFloat.h>
#include <libBasement/LinearColor.h>

#include <libNoodoo/ShowyThing.h>
#include <libNoodoo/VertexForm.h>
#include <libNoodoo/VertEllipse.h>
#include <libNoodoo/GlyphoString.h>
#include <libNoodoo/FlatThing.h>
#include <libNoodoo/TexQuad.h>

#include "VideoThing.h"

#include "Tweezers.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class Exoskeleton : public Tweezers, public OEPointing::Evts {
    PATELLA_SUBCLASS (Exoskeleton, ShowyThing);

private:
    /**
     * Upper and lower corners surround the object
     */
    ObRef<VertexForm *> corn_a;
    ObRef<VertexForm *> corn_b;

    /**
     * title of object
     */
    ObRef<GlyphoString *> label;

    /**
     * for internal use: radius of rounded corners, how much to
     * inset the exoskeleton over its contents.
     */
    float64 /*exo_label_width,*/ exo_corner_radius, exo_inset;

    float64 label_text_size, label_padding;

    /**
     * how many points in every bezier corner
     */
    int32 num_bez_pts;

    float64 hull_size;

    /**
     * length of animation when exoskeleton appears
     */
    // float64 exo_bobble_period;
    // FatherTime bobble_timer;
    /**
     * control the width and height of the exeskeleton parts
     */
    ObRef<InterpFloat *> exo_upper_width;
    ObRef<InterpFloat *> exo_lower_width;
    ObRef<InterpFloat *> exo_height;

    ObRef<LinearColor *> fill_amplitude;

    ObWeakRef<FlatThing *> precious_innards;

    ObRef<VertEllipse *> provenance_dot;

    bool needs_recalc;

    SOFT_MACHINERY (SoftColor, BackingColor);
    SOFT_MACHINERY (SoftColor, BorderColor);

public:
    Exoskeleton ();
    virtual ~Exoskeleton ();

    static Exoskeleton *NewCustomFittedFor (FlatThing *ft) {
        Exoskeleton *xo = new Exoskeleton ();
        xo->Attach (ft);
        if (dynamic_cast<TexQuad *> (ft)) {
            xo->SetLabelString ("Image");
        } else if (VideoThing *vt = dynamic_cast<VideoThing *> (ft)) {
            if (Viddle *dle = vt->GetViddle ()) {
                if (dle->name.Index ("dvi-capture") >= 0)
                    xo->SetLabelString ("Local DVI Video");
                else
                    xo->SetLabelString ("Remote Video");
            } else
                xo->SetLabelString ("Uninstantiated Video");
        } else {
            xo->SetLabelString (ft->Name ());
        }

        return xo;
    }

    void ProvenanceHold (const Str &prv);
    void ProvenanceRelease (const Str &prv);

    /**
     * for internal use: update the bezier points of one of the skeletal parts
     */
    void ComputeBezCorner (VertexForm *vform,
                           const Vect &a1,
                           const Vect &c1,
                           const Vect &c2,
                           const Vect &a2,
                           const int32 &start_pt,
                           const int32 &num_pts);

    /**
     * for internal use:
     */
    void ComputeHalfExo (VertexForm *vert,
                         const Vect &corner,
                         const Vect &u_cavity_h,
                         const Vect &o_cavity_w,
                         const Vect &u_hull_h,
                         const Vect &o_hull_w,
                         const Vect &u_radius,
                         const Vect &o_radius);

    void SetLabelString (const Str &s);
    //  Str LabelString () const;

    void Attach (FlatThing *a);

    ObRetort Travail (Atmosphere *atm);
};
}
} // bye bye namespaces sluice, oblong

#endif
