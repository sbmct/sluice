
/* (c)  oblong industries */

#ifndef WINDSHIELD_BUG_COLLECTION
#define WINDSHIELD_BUG_COLLECTION

#include "Acetate.h"
#include "BoogReporter.h"

#include "UIDSpigot.h"

#include "SluiceStyle.h"
#include "MzHandi.h"
#include "MzTender.h"
#include "MzMigrate.h"
#include "Exoskeleton.h"
#include "HandiPoint.h"
#include "SystemArea.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class Windshield : public Acetate,
                   public MzHandi::Evts,
                   public MzTender::Evts,
                   public MzMigrate::Evts,
                   public OEBlurt::Evts {
    PATELLA_SUBCLASS (Windshield, Acetate);

    ObRef<BoogReporter *> booger;

    ObRef<ShowyThing *> asset_bucket;

    ObRef<SystemArea *> sys_area;

    UIDSpigot element_uid_source;

    ObMap<Str, FlatThing *, NoMemMgmt, WeakRef> hoveree_by_provenance;
    ObMap<Str, FlatThing *, NoMemMgmt, WeakRef> manipulee_by_provenance;
    ObMap<Str, FlatThing *, NoMemMgmt, WeakRef> regulee_by_provenance;
    ObMap<FlatThing *, Exoskeleton *, WeakRef, WeakRef> exoskeleton_by_ft;

    ObTrove<HandiPoint *> condensed_hands;

public:
    Windshield ();
    virtual ~Windshield ();

    Slaw WindshieldDigest () const;

    virtual void InitiallyUnfurl (Atmosphere *atm);

    virtual void InstallShoveReporter (BoogReporter *br);
    virtual void InstallSystemArea (SystemArea *sa);

    BoogReporter *OurBoogReporter () const { return ~booger; }
    ShowyThing *OurAssetBucket () const { return ~asset_bucket; }
    SystemArea *OurSystemArea () const { return ~sys_area; }

    const Str &MostRecentlyDribbledElementUID () const {
        return element_uid_source.MostRecentlyDribbledUIDAsStr ();
    }
    const Str &NextElementUIDPeek () const {
        return element_uid_source.NextUIDPeekAsStr ();
    }
    const Str &NextElementUID () { return element_uid_source.NextUIDAsStr (); }

    virtual void ArrangeDenizens ();

    FlatThing *TopmostIntersectedOccupant (const Vect &from_v,
                                           const Vect &to_v);
    int64 RemoveAllOccupants ();

    virtual ObRetort AppendElementFromDir (const Str &ds_dir,
                                           const Str &ast_uid,
                                           const Str &el_uid,
                                           const Vect &loc,
                                           float64 wid,
                                           float64 hei);

    virtual ObRetort PassMzHandiEventsThrough (MzHandiEvent *e, Atmosphere *a);
    virtual ObRetort RemoveChild (KneeObject *ko, Atmosphere *atm = NULL);
    virtual ObRetort ScheduleChildRemoval (KneeObject *ch);

    virtual ObRetort RemoveExoskeletonIfStale (FlatThing *ft);
    bool RemoveAllExoskeletonedFluoroscopes ();

    virtual void FadeAssets ();
    virtual void BrightenAssets ();

    virtual void SetAssetOpacity (float64 opaq);
    virtual void SetAssetOpacityHard (float64 opaq);
    virtual void SetAssetsVisibility (bool show);

    bool IsFull ();

    virtual ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *atm);

    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzHandiIntentChanged (MzHandiIntentChangedEvent *e,
                                           Atmosphere *atm);

    virtual ObRetort MzTenderProbe (MzTenderProbeEvent *e, Atmosphere *atm);
    virtual ObRetort MzTenderSolidify (MzTenderSolidifyEvent *e,
                                       Atmosphere *atm);
    virtual ObRetort MzTenderRescind (MzTenderRescindEvent *e, Atmosphere *atm);

    // virtual ObRetort MzMigrateBegin (MzMigrateBeginEvent *e, Atmosphere
    // *atm);
    // virtual ObRetort MzMigrateContinue (MzMigrateContinueEvent *e,
    //                                     Atmosphere *atm);
    // virtual ObRetort MzMigrateEnd (MzMigrateEndEvent *e, Atmosphere *atm);
    // virtual ObRetort MzMigrateAbort (MzMigrateAbortEvent *e, Atmosphere
    // *atm);

    /// handles for cthulhu to hide/show handipoints
    virtual ObRetort HandleValveOpen (Atmosphere *a);
    virtual ObRetort HandleValveShut (Atmosphere *a);
};
}
} // bye bye namespaces sluice, oblong

#endif
