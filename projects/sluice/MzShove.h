
/* (c)  oblong industries */

#ifndef MEZZANINE_SHOVE_EVENT
#define MEZZANINE_SHOVE_EVENT

#include <libImpetus/OEDisplacement.h>

#include <libGanglia/ElectricalEvent.h>

namespace oblong {
namespace sluice {
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class MzShoveEvent : public ElectricalEvent {
    PATELLA_SUBCLASS (MzShoveEvent, ElectricalEvent);
    OE_MISCY_MAKER (MzShove, Electrical, "shove");

public:
    Vect shove_loc, prev_shove_loc;
    float64 shimmy_delta;
    Vect shove_origin;
    Vect shove_direction, shimmy_axis;
    //  ObRef <OEDisplacementEvent *> raw_event;
    // Str provenance;

public:
    MzShoveEvent (KneeObject *orig_ko = NULL)
        : ElectricalEvent (orig_ko), shimmy_delta (0.0) {}

    Vect ShoveOffset () const;

    const Vect &PrevShoveLoc () const;
    const Vect &ShoveLoc () const;

    const Vect &ShoveOrigin () const;
    //  const Vect &ShoveDelta ();
    float64 ShoveDistance () const;
    const Vect &ShoveDirection () const;

    float64 ShimmyDelta () const;
    const Vect &ShimmyAxis () const;

    OEDisplacementEvent *RawOEDisplacementEvent () const;
    void SetRawOEDisplacementEvent (OEDisplacementEvent *e);

    void SetInitialLocationAndDirection (const Vect &loc, const Vect &axis);
    void SetPreviousShove (const Vect &shv);
    void SetCurrentShove (const Vect &shv);

    void SetShimmyDeltaAndAxis (float64 sd, const Vect &axis);

    virtual void SynthesizeInternalProtein ();
    virtual ObRetort AnalyzeInternalProtein ();
};

class MzShoveBeginEvent : public MzShoveEvent {
    PATELLA_SUBCLASS (MzShoveBeginEvent, MzShoveEvent);
    OE_MISCY_MAKER (MzShoveBegin, MzShove, "begin");
    MzShoveBeginEvent (KneeObject *orig_ko = NULL) : MzShoveEvent (orig_ko) {}
};

class MzShoveContinueEvent : public MzShoveEvent {
    PATELLA_SUBCLASS (MzShoveContinueEvent, MzShoveEvent);
    OE_MISCY_MAKER (MzShoveContinue, MzShove, "continue");
    MzShoveContinueEvent (KneeObject *orig_ko = NULL)
        : MzShoveEvent (orig_ko) {}
};

class MzShoveEndEvent : public MzShoveEvent {
    PATELLA_SUBCLASS (MzShoveEndEvent, MzShoveEvent);
    OE_MISCY_MAKER (MzShoveEnd, MzShove, "end");
    MzShoveEndEvent (KneeObject *orig_ko = NULL) : MzShoveEvent (orig_ko) {}
};

class MzShoveEventAcceptorGroup
    : public MzShoveBeginEvent::MzShoveBeginAcceptor,
      public MzShoveContinueEvent::MzShoveContinueAcceptor,
      public MzShoveEndEvent::MzShoveEndAcceptor {};

class MzShove {
public:
    typedef MzShoveEventAcceptorGroup Evts;
};
}
} // a bittersweet end for namespaces sluice and oblong

DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED (oblong::sluice::MzShove::Evts);

#endif
