
/* (c)  oblong industries */

#ifndef TOPOWARD_H
#define TOPOWARD_H

#include <libLoam/c++/FatherTime.h>
#include <libBasement/KneeObject.h>
#include <libBasement/ob-hook-troves.h>
#include "Hasselhoff.h"
#include "Node.h"

#include <map>

#include <list>

namespace oblong {
namespace sluice {

using namespace oblong::loam;
using namespace oblong::basement;

class NodeStore;

struct RefCmp {
    bool operator()(const ObRef<Node *> &a, const ObRef<Node *> &b) {
        return (~a) < (~b);
    }
};

/**
 * TopoWard ->
 *
 * TopoWard manages the addition of topology both by paged loads and bulk dumps.
 *
 **/
class TopoWard : public KneeObject {
    PATELLA_SUBCLASS (TopoWard, KneeObject);

public:
    TopoWard (NodeStore *ns, float64 timeout_secs = 60.0);
    virtual ~TopoWard () {}

    virtual ObRetort Inhale (Atmosphere *atm);
    virtual void InitializePoolRegistration (Hasselhoff *hoff);

    ObRetort MetabolizeTopologyAdd (const Protein &prt, Atmosphere *atmo);
    SINGLE_ARG_HOOK_MACHINERY (ProgressUpdated, Str);

    ObRetort UniverseUpdated (NodeStore *);

    // Read node will process the ingest into Node
    ObRetort ReadNode (const Slaw &ing, Node *&n, float64 &t);

private:
    // Resets everything back to the start state
    void AbandonShip ();

    // Handles adding to the candidate pile, once everything is received
    // the pile is processed
    ObRetort AppendToPile (const Slaw &ing);

    // Process the pile, mainly calling NodeStore addNode
    void FloodTheDeck ();

    NodeStore *node_store;
    typedef ObRef<Node *> noderef_t;
    typedef std::pair<noderef_t, float64> nodetime_t;
    typedef std::list<nodetime_t> the_pile_t;
    the_pile_t the_pile;
    typedef std::map<Str, Node *> ident_map_t;
    ident_map_t ident_map;
    int32 pile_size;
    int32 expected_chunk;
    float64 timeout;
    FatherTime watch_uhr;

    // Two states, either we are in a start state or loading state
    // To go from start to loading a chunk must start with 1/N...
    // where N is an integer greater than 0
    // Loading state follows the uniform progression of 1/N to N/N
    // Any deviation from this we abandon ship and start again
    enum STATE { START, LOADING } state;
};
}
}

#endif // TOPOWARD_H
