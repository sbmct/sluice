
#ifndef GEO_MATRIX_SAYS_NEO_IS_NOT_THE_ONE
#define GEO_MATRIX_SAYS_NEO_IS_NOT_THE_ONE

#include <libLoam/c++/Matrix44.h>

namespace oblong {
namespace sluice {
using namespace oblong::loam;

/**
 * GeoMatrix
 *
 * Encapsulates the linear transformation between from UV to Cartographic (CxCy)
 * coordinates. Internally, this is simply stored as a Matrix44, but we expose
 * methods to work with v2float64 variables.
 *
 * This representation supports 2D Scale, Translation, and Rotation. We provide
 * convenience methods for setting the transform from a "corners" representation
 * which enforces a pure axis-aligned scale/translation xform.
 *
 **/
class GeoMatrix {
private:
    Matrix44 mat, imat;

public:
    GeoMatrix ();
    GeoMatrix (const Matrix44 &m, const Matrix44 &im);

    /** Accessors for matrices */
    Matrix44 Mat () { return mat; }
    Matrix44 IMat () { return imat; }

    /**
     * Note: since we're constantly accumulating the inverse operations, this
     * is simply a matter of reversing mat & imat in the new GeoMatrix instance.
     * We're not actually computing an inverse!
     **/
    GeoMatrix Inverse () const;

    /**
     * Convenience loaders which assume that the two spaces are axis-aligned and
     * simply a scale and traslate away from one another.
     *
     * Note: the corners refer to the bottom-left and top-right corners of the
     *       unit square of the target space as given in the source space's
     *units.
     *       So, typically bl/tr of Carto space given in UV coordinates.
     */
    GeoMatrix &LoadFromCorners (const float64 &x_bl,
                                const float64 &y_bl,
                                const float64 &x_tr,
                                const float64 &y_tr);
    GeoMatrix &LoadFromCorners (const v2float64 &xy_bl, const v2float64 &xy_tr);

    /**
     * 2D Scale
     */
    GeoMatrix Scale (const float64 &sc) const;
    GeoMatrix Scale (const float64 &sx, const float64 &sy) const;

    GeoMatrix &ScaleInPlace (const float64 &sc);
    GeoMatrix &ScaleInPlace (const float64 &sx, const float64 &sy);

    /**
     * 2D Scale about a given point where the point is defined in the source
     * coordinate space.
     */
    GeoMatrix ScaleAbout (const float64 &sc,
                          const float64 &x_cntr,
                          const float64 &y_cntr) const;
    GeoMatrix ScaleAbout (const float64 &sx,
                          const float64 &sy,
                          const float64 &x_cntr,
                          const float64 &y_cntr) const;

    GeoMatrix &ScaleAboutInPlace (const float64 &sc,
                                  const float64 &x_cntr,
                                  const float64 &y_cntr);
    GeoMatrix &ScaleAboutInPlace (const float64 &sx,
                                  const float64 &sy,
                                  const float64 &x_cntr,
                                  const float64 &y_cntr);

    /**
     * 2D Rotation - To come...
     */
    //   GeoMatrix Rotate (const float64 &ang)  const;
    //   GeoMatrix &RotateInPlace (const float64 &ang);

    //   GeoMatrix RotateAbout (const float64 &ang, const float64 &rx, const
    //   float64 &ry)  const;
    //   GeoMatrix &RotateAboutInPlace (const float64 &ang, const float64 &rx,
    //   const float64 &ry);

    /**
     * 2D Translation
     */
    GeoMatrix Translate (const float64 &dx, const float64 &dy) const;
    GeoMatrix &TranslateInPlace (const float64 &dx, const float64 &dy);

    /**
     * Transforms on v2float64 types, by which we'll mostly represent UV & CxCy
     * points.
     */
    v2float64 operator*(const v2float64 &v) const;

    void TransformV2 (const float64 &xsrc,
                      const float64 &ysrc,
                      float64 &xdest,
                      float64 &ydest) const;
    void TransformV2 (const v2float64 &vsrc, v2float64 &vdest) const;
    v2float64 TransformV2 (const v2float64 &v) const;

    v2float64 &TransformV2InPlace (v2float64 &v) const;
    void TransformV2InPlace (float64 &vx, float64 &vy) const;

    void SpewToStderr () const;
};
}
} /// end namespaces oblong, staging

#endif // GEO_MATRIX_SAYS_NEO_IS_NOT_THE_ONE
