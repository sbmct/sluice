
#include <stdio.h>
#include <AppKit/AppKit.h>
#include <ApplicationServices/ApplicationServices.h>

void SetIcon (const char *filename) {
    NSImage *loadImage = [[NSImage alloc]
        initWithContentsOfFile:[[NSString alloc] initWithUTF8String:filename]];
    [[NSApplication sharedApplication] setApplicationIconImage:loadImage];
    [loadImage release];
}
