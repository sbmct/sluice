
#ifndef GEO_TILE_QUAD_ALWAYS_LOOKS_SHARP
#define GEO_TILE_QUAD_ALWAYS_LOOKS_SHARP

#include "GeoQuad.h"
#include "MosaiQuad.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;

/**
 * GeoTileQuad -->
 *
 *
 **/

class GeoTileQuad : public GeoQuad {
    PATELLA_SUBCLASS (GeoTileQuad, GeoQuad);

public:
protected:
    /**
     * For now, we'll simply wrap MosaiQuad, which will handle the fetching of
     * named tile sets and arranging of the tiles within the FlatThing geometry.
     *
     * Note: We are hijacking the MosaiQuad's FlatThing attributes, so we extend
     *       Installxxx methods and pass the installed soft onto the MosaiQuad
     *       instance as well. This way, the we always have the same bounds.
     **/
    ObRef<MosaiQuad *> mquad;

    Str tile_server;
    Str tile_set;

    /// private method to seize ownership over the target quad's Loc/FlatThing
    /// softs
    void AdoptQuad (FlatThing *ft);

    SOFT_MACHINERY (SoftColor, CurtainColor);
    FLAG_MACHINERY (DrawCurtain);

public:
    GeoTileQuad ();
    GeoTileQuad (const Str &tileServer, const Str &tileSet);
    virtual ~GeoTileQuad ();

    void SetCurtain (ObColor c, float64 fade);
    void SetCurtain ();

    void SetCurtainColor (ObColor c);
    ObColor GetCurtainColor () const;

    /// Location of the tile server
    // ObRetort SetTileServerHost (const Str &tileServer);

    /// Accessors
    MosaiQuad *MQuad () { return ~mquad; }

    /// Name of the tileset to fetch
    Str TileSet ();
    ObRetort SetTileSet (const Str &tileSet);

    float64 Resolution () { return (~mquad)->Resolution (); }
    void SetResolution (const float64 &res) { (~mquad)->SetResolution (res); }

    virtual ObRetort AcknowledgeUVXformChange ();

    /// We need our MosaiQuad to shadow our Locus / FlatThing components.
    virtual void InstallLoc (SoftVect *sv);
    virtual void InstallNorm (SoftVect *sv);
    virtual void InstallOver (SoftVect *sv);

    virtual void InstallWidth (SoftFloat *sf);
    virtual void InstallHeight (SoftFloat *sf);
    virtual void InstallVerticalAlignment (SoftFloat *sf);
    virtual void InstallHorizontalAlignment (SoftFloat *sf);

    void SetAlphaPerSecond (const float64 x);

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

private:
    void Init ();
    void SyncGeography ();
};
}
} /// end namespaces oblong, staging

#endif /// GEO_TILE_QUAD_ALWAYS_LOOKS_SHARP
