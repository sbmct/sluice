
#ifndef GEO_VIDEO_H_FROM_SLUICE
#define GEO_VIDEO_H_FROM_SLUICE

#include "geo_utils.h"
#include "GeoProj.h"

#include <libNoodoo/VidQuad.h>

namespace oblong {
namespace sluice {
using namespace oblong::sluice;
using namespace oblong::noodoo;

/**
 * GeoVid is a geolocated video source cannibalized from GeoTex.
 **/
class GeoVid : public VidQuad {
    PATELLA_SUBCLASS (GeoVid, VidQuad);

protected:
    LatLon ll_bl, ll_tr;
    v2float64 tex_coor_bl, tex_coor_tr;
    ObRef<GeoProj *> projection;

    /**
     * If this is true, the video will remain tied to the fluoroscope frame,
     * if false, the video will remain fixed geographically while the
     * fluoroscope moves.
     */
    FLAG_MACHINERY (StayPut);

public:
    GeoVid (GeoProj *proj);

    GeoProj *Projection ();
    void SetProjection (GeoProj *proj);

    LatLon LatLonBL () { return ll_bl; }
    LatLon LatLonTR () { return ll_tr; }

    void SetGeoCorners (const LatLon &bl, const LatLon &tr);
    void GeoCorners (LatLon &bl, LatLon &tr) const;

    void SetTexCoors (const v2float64 &st_bl, const v2float64 &st_tr);
    void TexCoors (v2float64 &st_bl, v2float64 &st_tr) const;

    virtual bool Open (Slaw resource, Atmosphere *atm = NULL);

    FLAG_MACHINERY (HasValidSource);
};
}
} /// end namespaces oblong

#endif /// GEO_VIDEO_H_FROM_SLUICE
