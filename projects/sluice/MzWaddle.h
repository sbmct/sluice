
/* (c)  oblong industries */

#ifndef MEZZANINE_WADDLE_EVENT
#define MEZZANINE_WADDLE_EVENT

#include <libGanglia/ElectricalEvent.h>
#include <libImpetus/OEPointing.h>

namespace oblong {
namespace sluice {
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class MzWaddleEvent : public ElectricalEvent {
    PATELLA_SUBCLASS (MzWaddleEvent, ElectricalEvent);
    OE_MISCY_MAKER (MzWaddle, Electrical, "waddle");

    // TODO: establish plane of dragging?
private:
    Str intent;
    Vect original_loc;
    KneeObject *migrant;

public:
    MzWaddleEvent (KneeObject *orig_ko = NULL)
        : ElectricalEvent (orig_ko), migrant (NULL) {}

    const Str &Intent () const;
    void SetIntent (const Str &ent);

    const Vect &OriginalLoc () const;

    KneeObject *Migrant () const;
    void SetMigrant (KneeObject *ft);

    OEPointingEvent *RawOEPointingEvent () const;
    void SetRawOEPointingEvent (OEPointingEvent *pe);

    void SetOriginalLocation (const Vect &org);

    virtual void SynthesizeInternalProtein ();
    virtual ObRetort AnalyzeInternalProtein ();
};

class MzWaddleBeginEvent : public MzWaddleEvent {
    PATELLA_SUBCLASS (MzWaddleBeginEvent, MzWaddleEvent);
    OE_MISCY_MAKER (MzWaddleBegin, MzWaddle, "begin");
    MzWaddleBeginEvent (KneeObject *orig_ko = NULL) : MzWaddleEvent (orig_ko) {}
};

class MzWaddleContinueEvent : public MzWaddleEvent {
    PATELLA_SUBCLASS (MzWaddleContinueEvent, MzWaddleEvent);
    OE_MISCY_MAKER (MzWaddleContinue, MzWaddle, "continue");
    MzWaddleContinueEvent (KneeObject *orig_ko = NULL)
        : MzWaddleEvent (orig_ko) {}
};

class MzWaddleEndEvent : public MzWaddleEvent {
    PATELLA_SUBCLASS (MzWaddleEndEvent, MzWaddleEvent);
    OE_MISCY_MAKER (MzWaddleEnd, MzWaddle, "end");
    MzWaddleEndEvent (KneeObject *orig_ko = NULL) : MzWaddleEvent (orig_ko) {}
};

class MzWaddleAbortEvent : public MzWaddleEvent {
    PATELLA_SUBCLASS (MzWaddleAbortEvent, MzWaddleEvent);
    OE_MISCY_MAKER (MzWaddleAbort, MzWaddle, "abort");
    MzWaddleAbortEvent (KneeObject *orig_ko = NULL) : MzWaddleEvent (orig_ko) {}
};

class MzWaddleEventAcceptorGroup
    : public MzWaddleBeginEvent::MzWaddleBeginAcceptor,
      public MzWaddleContinueEvent::MzWaddleContinueAcceptor,
      public MzWaddleEndEvent::MzWaddleEndAcceptor,
      public MzWaddleAbortEvent::MzWaddleAbortAcceptor {};

class MzWaddle {
public:
    typedef MzWaddleEventAcceptorGroup Evts;
};
}
} // a bittersweet end for namespaces sluice and oblong

DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED (oblong::sluice::MzWaddle::Evts);

#endif
