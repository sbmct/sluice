
#pragma once

#include <libLoam/c++/FatherTime.h>
#include <libLoam/c++/ObMap.h>

#include <libPlasma/c++/Protein.h>

#include "Acetate.h"

#include <boost/function.hpp>

namespace oblong {
namespace sluice {

class HeartBeater : public AnkleObject {
    PATELLA_SUBCLASS (HeartBeater, AnkleObject);

public:
    virtual Slaw Beat () = 0;
};

class HeartbeatThing : public Acetate {
    PATELLA_SUBCLASS (HeartbeatThing, Acetate);

private:
    oblong::loam::ObMap<Str, HeartBeater *> heartbeats;
    FatherTime timer;
    float64 update_frequency;

    oblong::plasma::Protein GenerateHeartbeat ();

public:
    HeartbeatThing (const float64 up_freq = 1.0);

    Str time_string;

    const float64 UpdateFrequency () const { return update_frequency; }
    void SetUpdateFrequency (const float64 x) { update_frequency = x; }

    void AppendHeartbeatFunction (const Str name, HeartBeater *hb);
    void RemoveHeartbeatFunction (const Str name);

    virtual ObRetort Inhale (Atmosphere *atmo);
};
}
}
