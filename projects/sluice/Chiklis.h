
/* (c)  oblong industries */

#ifndef OPTION_ONE_IS_WE_TAKE_HIM_ALIVE_BUT_FEEL_FREE_TO_CONSIDER_OPTION_TWO
#define OPTION_ONE_IS_WE_TAKE_HIM_ALIVE_BUT_FEEL_FREE_TO_CONSIDER_OPTION_TWO

#include "Acetate.h"
#include <libBasement/KneeObject.h>
#include <libBasement/TimedTrigger.h>

namespace oblong {
namespace sluice {

using namespace oblong::basement;

class TokenExpiryTrigger : public TimedTrigger {
    PATELLA_SUBCLASS (TokenExpiryTrigger, TimedTrigger);

public:
    TokenExpiryTrigger () : TimedTrigger () { Suspend (); }

    void Set (float32 timeout) {
        SetTriggerPeriod (timeout);
        pacer.ReadyReset ();
        SetNumFirings (1);
        if (IsSuspended ())
            Unsuspend ();
    }

    float32 Expiry () { return pacer.PacePeriod (); }
    virtual ObRetort Fire (KneeObject *self, Atmosphere *atm);
};

/**
 * Chiklis :
 *
 * - responsible for handling tokens and tokenizing proteins
 * - logins, logouts, expirations
 *
 **/
class Hasselhoff;
class Chiklis : public Acetate {
    PATELLA_SUBCLASS (Chiklis, Acetate);

private:
    Chiklis (const Chiklis &);
    Chiklis &operator=(const Chiklis &);

    float32 default_timeout;
    Str token;
    Str user_id;
    Str user_name;
    TokenExpiryTrigger *tot;

public:
    Chiklis ();
    virtual ~Chiklis () {}

    virtual void InitializePoolRegistration (Hasselhoff *hoff);

    void SetDefaultTimeout (float32 t);

    ObRetort Logout ();
    ObRetort Login (const Str &uid, const Str &nam, const Str &tok);
    ObRetort
    Login (const Str &uid, const Str &nam, const Str &tok, float32 expiry);
    Protein DecorateProtein (const Protein &p);

    ObRetort MetabolizeLogout (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeLogin (const Protein &prt, Atmosphere *atmo);
    ObRetort MetabolizeRequestCurrentCreds (const Protein &prt,
                                            Atmosphere *atmo);

    Str CurrentUserId () const { return user_id; }
    Str CurrentUserName () const { return user_name; }
    Str CurrentToken () const { return token; }
    float32 CurrentExpiry () const { return tot->Expiry (); }
    Slaw CurrentCredsSlaw () const;

    FLAG_MACHINERY_FOR (Enabled)
};
}
} // so long sluicer

#endif
