
/* (c)  oblong industries */

#ifndef SCOPE_SKEWER
#define SCOPE_SKEWER

#include <libNoodoo/FlatThing.h>
#include <libNoodoo/SoftVertexForm.h>
#include <libNoodoo/TexQuad.h>

#include "Acetate.h"
#include "MzHandi.h"
#include "VertPie.h"
#include <libNoodoo/VFImageRecorder.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class Fluoroscope;

class Skewer : public Acetate, public MzHandi::Evts {
    PATELLA_SUBCLASS (Skewer, Acetate);

private:
    ObRef<VertPie *> handle;
    ObRef<VFImageRecorder *> ir;
    ObWeakRef<Fluoroscope *> parent_scope;
    Vect grab_offset;
    Slaw have_url;
    bool am_skewered;
    Str cur_prov;
    ObMap<Str, TexQuad *> shadow_map;

    Skewer &operator=(const Skewer &);
    Skewer (const Skewer &);

public:
    explicit Skewer (Fluoroscope *f);

    void InstallHandle ();

    void TrySkewering (Vect from, Vect to);
    void SetAmSkewered (bool skewer);
    bool QueryAmSkewered () { return am_skewered; }

    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);

    ObRetort
    ProcessImage (ObRef<ImageClot *> im, VisiFeld *vf, const Str &name);
    ObRetort PreProcessImage (const Str &name);
};
}
}

#endif
