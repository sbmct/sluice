#ifndef COLOREDQUAD_H
#define COLOREDQUAD_H

#include <libNoodoo/FlatThing.h>
#include <libLoam/c++/ObColor.h>
#include <libBasement/SoftColor.h>

namespace oblong {
namespace sluice {
using namespace oblong::basement;
using namespace oblong::noodoo;

class ColoredQuad : public FlatThing {
    PATELLA_SUBCLASS (ColoredQuad, FlatThing);

private:
    ObColor color;
    SOFT_MACHINERY (SoftColor, Color);

public:
    ColoredQuad ();
    ColoredQuad (ObColor c);
    void SetColor (ObColor c);
    virtual void DrawSelf (VisiFeld *vf, Atmosphere *);
    void Glom (FlatThing *other);
};
}
} // namespace oblong::sluice

#endif