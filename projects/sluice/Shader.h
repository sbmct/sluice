
#pragma once

#include <libBasement/KneeObject.h>
#include <libNoodoo/GLMiscellany.h>
#include <map>

#include <boost/tuple/tuple.hpp>

namespace oblong {
namespace sluice {

using namespace oblong::basement;
using namespace oblong::noodoo;

/**
 * Manager an OpenGL shader (Version 1.0, vertex and fragment)
 */
class Shader : public KneeObject {
    PATELLA_SUBCLASS (Shader, KneeObject);

private:
    bool already_setup;

protected:
    GLint program_id;
    Str vert_shader, frag_shader;

    std::map<Str, GLint> uniform_locs;

    /** Load a shader program, assuming a program id */
    ObRetort LoadShader (const Str &program, GLint shader_type);
    /** Link all currently loaded shader programs */
    ObRetort LinkProgram ();

    FLAG_MACHINERY (HasValidProgram);

public:
    /** Create a shader given source for vertex and fragment shading programs */
    Shader (const Str &vert_shader, const Str &frag_shader);
    virtual ~Shader ();

    const Str &VertShader () const { return vert_shader; }

    /**
     * Do important pre-use setup for the shader programs.
     *
     * Must be called
     * - Before the shader is used
     * - Within a valid OpenGL comment
     * - While this program is in effect.
     * Setup will only actually do setup *once*(unless it fails),
     * so it's safe to run it multiple times.  A useful pattern
     * might be:
     * \code
     * ObRetort tort = shader_pointer -> Setup ();
     * if (tort . IsSplend () && shader_pointer -> QueryHasValidProgram ())
     *   do_the_rest_of_your_drawing ();
     * \endcode
     * Note that if you use the UseShader class, this will all be taken
     * care of for you
     */
    virtual ObRetort Setup ();

    /** The location of a uniform variable
     *  Must only be called when this shader is the active program
     */
    GLint UniformLocation (const Str &name);
    /** The location of a attribute variable
     * Must only be called when this shader is the active program
     */
    GLint AttributeLocation (const Str &attr);

    void SetUniform (const Str &name, const float64 val);
    void SetUniform (const Str &name, const int64 val);
    /// Sets a named uniform vec4 with the value from an ObColor
    void SetUniform (const Str &name, const ObColor val);
    /// Sets a named uniform vec3 with the value from a Vect
    void SetUniform (const Str &name, const Vect val);
    void SetUniform (const Str &name, const int val);
    /// Sets a named uniform vec2 with the value from a v2float64
    void SetUniform (const Str &name, const v2float64 val);
    void SetUniform (const Str &name, const v4int64 val);

    GLint Program ();
};

/**
 * Manage the lifecycle of a Shader
 *
 * Handles shader lifecycle issues.  Recommended use is:
 * \code
 * void MyShowyThing::DrawSelf (VisiFeld *vf, Atmosphere *atmo)
 * { UseShader shady_mort (shader_pointer);
 *   if (shady_mort . IsShading ())
 *   { set_up_uniforms (shader_pointer);
 *     do_other_rendering_things ();
 *   }
 * }
 * \endcode
 *
 * Once the UseShader instance goes out of scope it will restore
 * the program that was active before the shader was invoked.
 * (Defaulting to 0, the default program.)
 */
class UseShader {
    PATELLA_CLASS (UseShader);

private:
    Shader *s;

    GLint current_program;
    bool shading;

public:
    UseShader () = delete;
    explicit UseShader (Shader *__s) : s (__s) {
        shading = false;
        glGetIntegerv (GL_CURRENT_PROGRAM, &current_program);
        if (s->Setup ().IsSplend () && (shading = s->QueryHasValidProgram ()))
            glUseProgram (s->Program ());
    }

    /**
     * Was the shader successfully installed?
     */
    const bool IsShading () const { return shading; }

    virtual ~UseShader () { glUseProgram (current_program); }
};

class ShaderStash {
private:
    typedef std::pair<unt32, unt32> shade_hashes_t;
    typedef ObRef<Shader *> shaderef_t;
    typedef std::map<shade_hashes_t, shaderef_t> shader_map_t;
    shader_map_t shader_map;

public:
    Shader *Get (const Str vert, const Str frag);
    template <typename T> void SetUniforms (T *setter) {
        shader_map_t::iterator iter = shader_map.begin (),
                               end = shader_map.end ();
        while (iter != end)
            if (Shader *s = ~((iter++)->second))
                setter->SetupUniforms (s);
    }
};

struct ShaderPrograms {
    Str vertex_filename, fragment_filename;
    Str vertex_program, fragment_program;

    bool Load ();
    bool IsValid () const;

    bool operator<(const ShaderPrograms &other) const {
        return std::tie (vertex_filename, fragment_filename) <
               std::tie (other.vertex_filename, other.fragment_filename);
    }
};
}
}
