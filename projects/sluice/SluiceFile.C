
#include "SluiceFile.h"

#include "gspeak_30_shim.h"
#include "sys/stat.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace oblong::sluice;
using namespace oblong::loam;

Str SluiceFile::FindEtcFile (Str filename,
                             Str prefix) { /// Prepend the prefix, if it exists
    if (!prefix.IsEmpty ()) {
        if (prefix.At (prefix.Length () - 1) == '/')
            filename = prefix + filename;
        else
            filename = prefix + "/" + filename;
    }

    /// First look in all the default etc paths
    Str full_path = FIND_FILE_QUIETLY (ob_etc_path, filename);
    if (!full_path.IsNull ()) {
        return full_path;
    }

    /// Next look for a local etc
    struct stat filestat;
    full_path = "etc/" + filename;
    if (stat (full_path.utf8 (), &filestat) >= 0) {
        OB_LOG_INFO ("Found %s in ./etc\n", filename.utf8 ());
        return full_path;
    }

    /// Next look for the installed etc
    prefix = Str (SLUICE_INSTALL_PATH) + "etc/";
    full_path = prefix + filename;
    if (stat (full_path.utf8 (), &filestat) >= 0) {
        OB_LOG_INFO ("Found %s in %s\n", filename.utf8 (), prefix.utf8 ());
        return full_path;
    }

    return Str ();
}

Str SluiceFile::FindResourceFile (
    Str filename, Str prefix, bool quiet) { /// Prepend the prefix, if it exists
    if (!prefix.IsEmpty ()) {
        if (prefix.At (prefix.Length () - 1) == '/')
            filename = prefix + filename;
        else
            filename = prefix + "/" + filename;
    }

    /// Look in all the default resource paths
    Str full_path;
    if (quiet) {
        full_path = FIND_FILE_QUIETLY (ob_share_path, filename);
    } else {
        full_path = FIND_FILE (ob_share_path, filename);
    }
    if (!full_path.IsEmpty ())
        return full_path;

    /// Failure, return the Null string
    return Str ();
}

Str SluiceFile::FindResourceFolder (
    Str foldername, Str prefix) { /// Prepend the prefix, if it exists
    if (!prefix.IsEmpty ()) {
        if (prefix.At (prefix.Length () - 1) == '/')
            foldername = prefix + foldername;
        else
            foldername = prefix + "/" + foldername;
    }

    /// Look in all the default resource paths
    Str full_path = FIND_FOLDER (ob_share_path, foldername);
    if (!full_path.IsEmpty ())
        return full_path;

    /// Failure, return the Null string
    return Str ();
}

Str SluiceFile::FindVarFile (Str filename,
                             Str prefix) { /// Prepend the prefix, if it exists
    if (!prefix.IsEmpty ()) {
        if (prefix.At (prefix.Length () - 1) == '/')
            filename = prefix + filename;
        else
            filename = prefix + "/" + filename;
    }

    /// Look in all the default var paths
    Str full_path = FIND_FILE (ob_var_path, filename);
    if (!full_path.IsEmpty ())
        return full_path;

    /// Failure, return the Null string
    return Str ();
}

Str
SluiceFile::FindVarFolder (Str foldername,
                           Str prefix) { /// Prepend the prefix, if it exists
    if (!prefix.IsEmpty ()) {
        if (prefix.At (prefix.Length () - 1) == '/')
            foldername = prefix + foldername;
        else
            foldername = prefix + "/" + foldername;
    }

    /// Look in all the default var paths
    Str full_path = FIND_FOLDER (ob_var_path, foldername);
    if (!full_path.IsEmpty ())
        return full_path;

    /// Failure, return the Null string
    return Str ();
}

// Assumes ASCII? Yes.
ObRetort SluiceFile::ReadFileContents (const Str &full_path, Str &output) {
    ObRetort tort = OB_OK;

    std::ifstream in (full_path.utf8 ());
    if (in.is_open ()) {
        std::stringstream buf;
        buf << in.rdbuf ();
        output = Str (buf.str ().c_str ());
        in.close ();
    } else
        tort = OB_NOT_FOUND;

    return tort;
}
