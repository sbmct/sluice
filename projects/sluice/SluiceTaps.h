/* (c)  Oblong Industries */

#ifndef NOSY_SLUICE_TAPS
#define NOSY_SLUICE_TAPS

#include "AtlasQuad.h"
#include "GeoTexQuad.h"
#include "NetworkQuad.h"
#include "GeoTileQuad.h"
#include "GeoVidQuad.h"

#include "SimpleTexRect.h"
#include "VideoThing.h"

#include <libOuija/NoodooTaps.h>

#include "NodeyScope.h"
#include "StaticImageFluoroscope.h"
#include "DecoratedStaticImageFluoroscope.h"
#include "VideoFluoroscope.h"
#include "WebFluoroscope.h"
#include "Gazetteer.h"

namespace oblong {
namespace sluice {
using namespace oblong::ouija;
using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

/**
 */
class AcetateTap : public FlatThingTap {
    PATELLA_SUBCLASS (AcetateTap, FlatThingTap);

protected:
    FUNC_LEARNER (LageLearner, Acetate, Lage);
    FUNC_LEARNER (BorderColorLearner, Acetate, BorderColor);
    VAL_LEARNER (DrawOutlineLearner, Acetate, draw_outline);
    static Slaw ContentSourceLearner (AnkleObject *inspectee);

    FUNC_TEACHER (LageTeacher, Acetate, Vect, SetLageHard);
    FUNC_TEACHER (BorderColorTeacher, Acetate, ObColor, SetBorderColor);
    VAL_TEACHER (DrawOutlineTeacher, Acetate, bool, draw_outline);
    static ObRetort ContentSourceTeacher (AnkleObject *clone, Slaw val);

public:
    AcetateTap () : FlatThingTap () {
        AppendKeyedFunqs ("lage", &LageLearner, &LageTeacher);
        AppendKeyedFunqs ("border-color", &BorderColorLearner,
                          &BorderColorTeacher);
        AppendKeyedFunqs ("draw-outline", &DrawOutlineLearner,
                          &DrawOutlineTeacher);
        AppendKeyedFunqs ("content-source", &ContentSourceLearner,
                          &ContentSourceTeacher);
    }
    TAP_CONSTRUCTOR (AcetateTap);
    CLONE_CONSTRUCTOR (Acetate);

    virtual ObRetort MakeFinalConnections (AnkleObject *clone,
                                           Slaw anatomy,
                                           ObMap<Slaw, AnkleObject *> qid_map);
};

/**
 */
class VideoThingTap : public AcetateTap {
    PATELLA_SUBCLASS (VideoThingTap, AcetateTap);

protected:
    FUNC_LEARNER (ViddleNameLearner, VideoThing, ViddleName);
    FUNC_LEARNER (EventPoolNameLearner, VideoThing, EventPoolName);

    FUNC_TEACHER (ViddleNameTeacher, VideoThing, Str, SetViddleName);
    FUNC_TEACHER (EventPoolNameTeacher, VideoThing, Str, SetEventPoolName);

    virtual void ClearSlate (AnkleObject *clone) {
        if (ShowyThing *st = dynamic_cast<ShowyThing *> (clone))
            if (WranglerCollection *wc = st->ItsWranglerCollection ())
                wc->ClearWranglers ();
    }

public:
    VideoThingTap () : AcetateTap () {
        AppendKeyedFunqs ("viddle-name", &ViddleNameLearner,
                          &ViddleNameTeacher);
        AppendKeyedFunqs ("event-pool-name", &EventPoolNameLearner,
                          &EventPoolNameTeacher);
    }
    TAP_CONSTRUCTOR (VideoThingTap);
    CLONE_CONSTRUCTOR (VideoThing);
};

/**
 */
class SimpleTexRectTap : public AcetateTap {
    PATELLA_SUBCLASS (SimpleTexRectTap, AcetateTap);

protected:
    static Slaw STRBackgroundColorLearner (AnkleObject *inspectee);
    FUNC_LEARNER (VertWrapBehaviorLearner, SimpleTexRect, VerticalWrapBehavior);
    FUNC_LEARNER (HorzWrapBehaviorLearner,
                  SimpleTexRect,
                  HorizontalWrapBehavior);
    FUNC_LEARNER (StretchBehaviorLearner, SimpleTexRect, StretchBehavior);
    FUNC_LEARNER (STRBorderColorLearner, SimpleTexRect, BorderColor);
    FUNC_LEARNER (STRBorderWidthLearner, SimpleTexRect, BorderWidth);
    FUNC_LEARNER (TexCoorBLLearner, SimpleTexRect, TexCoorBL);
    FUNC_LEARNER (TexCoorTRLearner, SimpleTexRect, TexCoorTR);
    FUNC_LEARNER (ShouldDrawTextureLearner,
                  SimpleTexRect,
                  QueryShouldDrawTexture);
    FUNC_LEARNER (ShouldDrawBorderLearner,
                  SimpleTexRect,
                  QueryShouldDrawBorder);
    FUNC_LEARNER (ShouldDrawBackgroundLearner,
                  SimpleTexRect,
                  QueryShouldDrawBackground);
    FUNC_LEARNER (ShouldInvertSLearner, SimpleTexRect, QueryShouldInvertS);
    FUNC_LEARNER (ShouldInvertTLearner, SimpleTexRect, QueryShouldInvertT);

    static ObRetort STRBackgroundColorTeacher (AnkleObject *clone, Slaw val);
    static ObRetort VertWrapBehaviorTeacher (AnkleObject *clone, Slaw val);
    static ObRetort HorzWrapBehaviorTeacher (AnkleObject *clone, Slaw val);
    static ObRetort StretchBehaviorTeacher (AnkleObject *clone, Slaw val);
    FUNC_TEACHER (STRBorderColorTeacher,
                  SimpleTexRect,
                  ObColor,
                  SetBorderColor);
    FUNC_TEACHER (STRBorderWidthTeacher, SimpleTexRect, float, SetBorderWidth);
    FUNC_TEACHER (TexCoorBLTeacher, SimpleTexRect, v2float64, SetTexCoorBL);
    FUNC_TEACHER (TexCoorTRTeacher, SimpleTexRect, v2float64, SetTexCoorTR);
    FUNC_TEACHER (ShouldDrawTextureTeacher,
                  SimpleTexRect,
                  bool,
                  SetShouldDrawTexture);
    FUNC_TEACHER (ShouldDrawBorderTeacher,
                  SimpleTexRect,
                  bool,
                  SetShouldDrawBorder);
    FUNC_TEACHER (ShouldDrawBackgroundTeacher,
                  SimpleTexRect,
                  bool,
                  SetShouldDrawBackground);
    FUNC_TEACHER (ShouldInvertSTeacher, SimpleTexRect, bool, SetShouldInvertS);
    FUNC_TEACHER (ShouldInvertTTeacher, SimpleTexRect, bool, SetShouldInvertT);

public:
    SimpleTexRectTap () : AcetateTap () {
        AppendKeyedFunqs ("str-background-colors", &STRBackgroundColorLearner,
                          &STRBackgroundColorTeacher);
        AppendKeyedFunqs ("vertical-wrap-behavior", &VertWrapBehaviorLearner,
                          &VertWrapBehaviorTeacher);
        AppendKeyedFunqs ("horizontal-wrap-behavior", &HorzWrapBehaviorLearner,
                          &HorzWrapBehaviorTeacher);
        AppendKeyedFunqs ("stretch-behavior", &StretchBehaviorLearner,
                          &StretchBehaviorTeacher);
        AppendKeyedFunqs ("str-border-color", &STRBorderColorLearner,
                          &STRBorderColorTeacher);
        AppendKeyedFunqs ("str-border-width", &STRBorderWidthLearner,
                          &STRBorderWidthTeacher);
        AppendKeyedFunqs ("tex-coor-bl", &TexCoorBLLearner, &TexCoorBLTeacher);
        AppendKeyedFunqs ("tex-coor-tr", &TexCoorTRLearner, &TexCoorTRTeacher);
        AppendKeyedFunqs ("should-draw-texture", &ShouldDrawTextureLearner,
                          &ShouldDrawTextureTeacher);
        AppendKeyedFunqs ("should-draw-border", &ShouldDrawBorderLearner,
                          &ShouldDrawBorderTeacher);
        AppendKeyedFunqs ("should-draw-background",
                          &ShouldDrawBackgroundLearner,
                          &ShouldDrawBackgroundTeacher);
        AppendKeyedFunqs ("should-invert-S", &ShouldInvertSLearner,
                          &ShouldInvertSTeacher);
        AppendKeyedFunqs ("should-invert-T", &ShouldInvertTLearner,
                          &ShouldInvertTTeacher);
    }
    TAP_CONSTRUCTOR (SimpleTexRectTap);
    CLONE_CONSTRUCTOR (SimpleTexRect);
};

/**
 */
class FluoroscopeTap : public AcetateTap {
    PATELLA_SUBCLASS (FluoroscopeTap, AcetateTap);

private:
    static Slaw PinToSlaw (Fluoroscope::Pin p);
    static Fluoroscope::Pin PinFromSlaw (Slaw s);

protected:
    static Slaw PinsLearner (AnkleObject *inspectee);
    static Slaw ConfigurationLearner (AnkleObject *inspectee);
    FUNC_LEARNER (PermanentAndStaticLearner,
                  Fluoroscope,
                  QueryPermanentAndStatic);
    ANKLE_LEARNER (GeoQuadLearner, Fluoroscope, GeoQuad, GQuad);
    QID_TROVE_LEARNER (FluoroscopesLearner,
                       Fluoroscope,
                       Fluoroscope,
                       ScopesCrawl);

    static ObRetort PinsTeacher (AnkleObject *clone, Slaw val);
    FUNC_TEACHER (PermanentAndStaticTeacher,
                  Fluoroscope,
                  bool,
                  SetPermanentAndStatic);
    static ObRetort GeoQuadTeacher (AnkleObject *clone, Slaw val);

public:
    FluoroscopeTap () : AcetateTap () {
        AppendKeyedLearner ("configuration", &ConfigurationLearner);
        AppendKeyedLearner ("fluoroscopes", &FluoroscopesLearner);
        AppendKeyedFunqs ("pins", &PinsLearner, &PinsTeacher);
        AppendKeyedFunqs ("permanent-and-static", &PermanentAndStaticLearner,
                          &PermanentAndStaticTeacher);
        AppendKeyedFunqs ("geo-quad", &GeoQuadLearner, &GeoQuadTeacher);
    }
    TAP_CONSTRUCTOR (FluoroscopeTap);
    CLONE_CONSTRUCTOR (Fluoroscope);

    virtual ObRetort MakeFinalConnections (AnkleObject *clone,
                                           Slaw anatomy,
                                           ObMap<Slaw, AnkleObject *> qid_map);
};

/**
 */
class AtlasQuadTap : public FluoroscopeTap {
    PATELLA_SUBCLASS (AtlasQuadTap, FluoroscopeTap);

protected:
    FUNC_LEARNER (ScaleConstantLearner, AtlasQuad, QueryScaleConstant);
    FUNC_LEARNER (ExponentialScaleLearner, AtlasQuad, QueryExponentialScale);
    FUNC_LEARNER (VelocityBasedScalingLearner,
                  AtlasQuad,
                  QueryVelocityBasedScaling);
    QID_TROVE_LEARNER (StaticLayersLearner,
                       AtlasQuad,
                       Fluoroscope,
                       StaticLayersCrawl);

    FUNC_TEACHER (ScaleConstantTeacher, AtlasQuad, float64, SetScaleConstant);
    FUNC_TEACHER (ExponentialScaleTeacher,
                  AtlasQuad,
                  bool,
                  SetExponentialScale);
    FUNC_TEACHER (VelocityBasedScalingTeacher,
                  AtlasQuad,
                  bool,
                  SetVelocityBasedScaling);

public:
    AtlasQuadTap () : FluoroscopeTap () {
        AppendKeyedFunqs ("scale-constant", &ScaleConstantLearner,
                          &ScaleConstantTeacher);
        AppendKeyedFunqs ("exponential-scale", &ExponentialScaleLearner,
                          &ExponentialScaleTeacher);
        AppendKeyedFunqs ("velocity-based-scaling",
                          &VelocityBasedScalingLearner,
                          &VelocityBasedScalingTeacher);
        AppendKeyedLearner ("static-layers", &StaticLayersLearner);
    }
    TAP_CONSTRUCTOR (AtlasQuadTap);
    CLONE_CONSTRUCTOR (AtlasQuad);

    virtual ObRetort MakeFinalConnections (AnkleObject *clone,
                                           Slaw anatomy,
                                           ObMap<Slaw, AnkleObject *> qid_map);
};

/**
 */
class WebFluoroscopeTap : public FluoroscopeTap {
    PATELLA_SUBCLASS (WebFluoroscopeTap, FluoroscopeTap);

protected:
    FUNC_LEARNER (URLLearner, WebFluoroscope, Url);
    FUNC_TEACHER (URLTeacher, WebFluoroscope, Str, NavigateTo);
    FUNC_LEARNER (SizeLearner, WebFluoroscope, BrowserSize);
    FUNC_TEACHER (SizeTeacher, WebFluoroscope, v2int64, SetBrowserSize);

public:
    WebFluoroscopeTap () : FluoroscopeTap () {
        AppendKeyedFunqs ("url", &URLLearner, &URLTeacher);
        AppendKeyedFunqs ("browser-size", &SizeLearner, &SizeTeacher);
    }
    TAP_CONSTRUCTOR (WebFluoroscopeTap);
    CLONE_CONSTRUCTOR (WebFluoroscope);
};

/**
 */
class NodeyScopeTap : public FluoroscopeTap {
    PATELLA_SUBCLASS (NodeyScopeTap, FluoroscopeTap);

public:
    NodeyScopeTap () : FluoroscopeTap () {}
    TAP_CONSTRUCTOR (NodeyScopeTap);
    CLONE_CONSTRUCTOR (NodeyScope);
};

/**
 */
class StaticImageFluoroscopeTap : public FluoroscopeTap {
    PATELLA_SUBCLASS (StaticImageFluoroscopeTap, FluoroscopeTap);

public:
    StaticImageFluoroscopeTap () : FluoroscopeTap () {}
    TAP_CONSTRUCTOR (StaticImageFluoroscopeTap);
    CLONE_CONSTRUCTOR (StaticImageFluoroscope);
};

class DecoratedStaticImageFluoroscopeTap : public StaticImageFluoroscopeTap {
    PATELLA_SUBCLASS (DecoratedStaticImageFluoroscopeTap,
                      StaticImageFluoroscopeTap);

public:
    DecoratedStaticImageFluoroscopeTap () : StaticImageFluoroscopeTap () {}
    TAP_CONSTRUCTOR (DecoratedStaticImageFluoroscopeTap);
    CLONE_CONSTRUCTOR (DecoratedStaticImageFluoroscope);
};

/**
 */
class VideoFluoroscopeTap : public FluoroscopeTap {
    PATELLA_SUBCLASS (VideoFluoroscopeTap, FluoroscopeTap);

public:
    VideoFluoroscopeTap () : FluoroscopeTap () {}
    TAP_CONSTRUCTOR (VideoFluoroscopeTap);
    CLONE_CONSTRUCTOR (VideoFluoroscope);
};

/**
 */
class GeoProjTap : public AnkleObjectTap {
    PATELLA_SUBCLASS (GeoProjTap, AnkleObjectTap);

protected:
    FUNC_LEARNER (LonExtrapPolicyLearner, GeoProj, LEP);

    static ObRetort LonExtrapPolicyTeacher (AnkleObject *clone, Slaw val);

public:
    GeoProjTap () : AnkleObjectTap () {
        AppendKeyedFunqs ("lon-extrap-policy", &LonExtrapPolicyLearner,
                          &LonExtrapPolicyTeacher);
    }
    TAP_CONSTRUCTOR (GeoProjTap);
    /** GeoProj has pure virtual members so you can't make an instance of
        it, therefore we have no clone_constructor, but we've made this tap
        for the same purpose GeoProj exists
    */
    // CLONE_CONSTRUCTOR (GeoProj);
};

/**
 */
class EquirectProjTap : public GeoProjTap {
    PATELLA_SUBCLASS (EquirectProjTap, GeoProjTap);

public:
    EquirectProjTap () : GeoProjTap () {}
    TAP_CONSTRUCTOR (EquirectProjTap);
    CLONE_CONSTRUCTOR (EquirectProj);
};

/**
 */
class MercProjTap : public GeoProjTap {
    PATELLA_SUBCLASS (MercProjTap, GeoProjTap);

public:
    MercProjTap () : GeoProjTap () {}
    TAP_CONSTRUCTOR (MercProjTap);
    CLONE_CONSTRUCTOR (MercProj);
};

/**
 */
class GeoQuadTap : public FlatThingTap {
    PATELLA_SUBCLASS (GeoQuadTap, FlatThingTap);

protected:
    ANKLE_LEARNER (ProjectionLearner, GeoQuad, GeoProj, Projection);
    static Slaw UVXformLearner (AnkleObject *inspectee);
    FUNC_LEARNER (DrawLatLonLearner, GeoQuad, QueryDrawLatLonLines);
    static Slaw LatLonLinesResLearner (AnkleObject *inspectee);
    FUNC_LEARNER (DrawFlatThingBoundsLearner,
                  GeoQuad,
                  QueryDrawFlatThingBounds);
    FUNC_LEARNER (BoundsColorLearner, GeoQuad, BoundsColor);
    FUNC_LEARNER (DrawFlatThingBorderLearner,
                  GeoQuad,
                  QueryDrawFlatThingBorder);
    FUNC_LEARNER (BorderColorLearner, GeoQuad, BorderColor);
    FUNC_LEARNER (BorderSizeLearner, GeoQuad, BorderSize);

    ANKLE_TEACHER (ProjectionTeacher, GeoQuad, GeoProj, SetProjection);
    static ObRetort UVXformTeacher (AnkleObject *clone, Slaw val);
    FUNC_TEACHER (DrawLatLonTeacher, GeoQuad, bool, SetDrawLatLonLines);
    static ObRetort LatLonLinesResTeacher (AnkleObject *clone, Slaw val);
    FUNC_TEACHER (DrawFlatThingBoundsTeacher,
                  GeoQuad,
                  bool,
                  SetDrawFlatThingBounds);
    FUNC_TEACHER (BoundsColorTeacher, GeoQuad, ObColor, SetBoundsColor);
    FUNC_TEACHER (DrawFlatThingBorderTeacher,
                  GeoQuad,
                  bool,
                  SetDrawFlatThingBorder);
    FUNC_TEACHER (BorderColorTeacher, GeoQuad, ObColor, SetBorderColor);
    FUNC_TEACHER (BorderSizeTeacher, GeoQuad, float64, SetBorderSize);

public:
    GeoQuadTap () : FlatThingTap () {
        AppendKeyedFunqs ("projection", &ProjectionLearner, &ProjectionTeacher);
        AppendKeyedFunqs ("uv-xform", &UVXformLearner, &UVXformTeacher);
        AppendKeyedFunqs ("draw-lat-lon", &DrawLatLonLearner,
                          &DrawLatLonTeacher);
        AppendKeyedFunqs ("lat-lon-lines-res", &LatLonLinesResLearner,
                          &LatLonLinesResTeacher);
        AppendKeyedFunqs ("draw-bounds", &DrawFlatThingBoundsLearner,
                          &DrawFlatThingBoundsTeacher);
        AppendKeyedFunqs ("bounds-color", &BoundsColorLearner,
                          &BoundsColorTeacher);
        AppendKeyedFunqs ("draw-border", &DrawFlatThingBorderLearner,
                          &DrawFlatThingBorderTeacher);
        AppendKeyedFunqs ("border-color", &BorderColorLearner,
                          &BorderColorTeacher);
        AppendKeyedFunqs ("border-size", &BorderSizeLearner,
                          &BorderSizeTeacher);
    }
    TAP_CONSTRUCTOR (GeoQuadTap);
    CLONE_CONSTRUCTOR (GeoQuad);
};

/**
 */
class GeoTexQuadTap : public GeoQuadTap {
    PATELLA_SUBCLASS (GeoTexQuadTap, GeoQuadTap);

protected:
    FUNC_LEARNER (TexProjPolicyLearner, GeoTexQuad, TexProjPolicy);
    static ObRetort TexProjPolicyTeacher (AnkleObject *clone, Slaw val);

public:
    GeoTexQuadTap () : GeoQuadTap () {
        AppendKeyedFunqs ("tex-proj-policy", &TexProjPolicyLearner,
                          &TexProjPolicyTeacher);
    }
    TAP_CONSTRUCTOR (GeoTexQuadTap);
    CLONE_CONSTRUCTOR (GeoTexQuad);

    virtual ObRetort MakeFinalConnections (AnkleObject *clone,
                                           Slaw anatomy,
                                           ObMap<Slaw, AnkleObject *> qid_map);
};

/**
 */
class GeoWebQuadTap : public GeoQuadTap {
    PATELLA_SUBCLASS (GeoWebQuadTap, GeoQuadTap);

public:
    GeoWebQuadTap () : GeoQuadTap () {}
    TAP_CONSTRUCTOR (GeoWebQuadTap);
    CLONE_CONSTRUCTOR (GeoWebQuad);

    virtual ObRetort MakeFinalConnections (AnkleObject *clone,
                                           Slaw anatomy,
                                           ObMap<Slaw, AnkleObject *> qid_map);
};

/**
 */
class NetworkQuadTap : public GeoQuadTap {
    PATELLA_SUBCLASS (NetworkQuadTap, GeoQuadTap);

public:
    NetworkQuadTap () : GeoQuadTap () {}
    TAP_CONSTRUCTOR (NetworkQuadTap);
    CLONE_CONSTRUCTOR (NetworkQuad);
};

/**
 */
class GeoTileQuadTap : public GeoQuadTap {
    PATELLA_SUBCLASS (GeoTileQuadTap, GeoQuadTap);

protected:
    FUNC_LEARNER (CurtainColorLearner, GeoTileQuad, GetCurtainColor);
    FUNC_TEACHER (CurtainColorTeacher, GeoTileQuad, ObColor, SetCurtainColor);

    FUNC_LEARNER (DrawCurtainLearner, GeoTileQuad, QueryDrawCurtain);
    FUNC_TEACHER (DrawCurtainTeacher, GeoTileQuad, bool, SetDrawCurtain);

public:
    GeoTileQuadTap () : GeoQuadTap () {
        AppendKeyedFunqs ("draw-curtain", &DrawCurtainLearner,
                          &DrawCurtainTeacher);
        AppendKeyedFunqs ("curtain-color", &CurtainColorLearner,
                          &CurtainColorTeacher);
    }
    TAP_CONSTRUCTOR (GeoTileQuadTap);
    CLONE_CONSTRUCTOR (GeoTileQuad);

    virtual ObRetort MakeFinalConnections (AnkleObject *clone,
                                           Slaw anatomy,
                                           ObMap<Slaw, AnkleObject *> qid_map);
};

/**
 */
class GeoVidQuadTap : public GeoQuadTap {
    PATELLA_SUBCLASS (GeoVidQuadTap, GeoQuadTap);

public:
    GeoVidQuadTap () : GeoQuadTap () {}
    TAP_CONSTRUCTOR (GeoVidQuadTap);
    CLONE_CONSTRUCTOR (GeoVidQuad);

    virtual ObRetort MakeFinalConnections (AnkleObject *clone,
                                           Slaw anatomy,
                                           ObMap<Slaw, AnkleObject *> qid_map);
};

/**
 */
class GazetteerTap : public KneeObjectTap {
    PATELLA_SUBCLASS (GazetteerTap, KneeObjectTap);

protected:
    FUNC_LEARNER (PersistentStoreLearner, Gazetteer, PersistentStore);
    FUNC_TEACHER (PersistentStoreTeacher, Gazetteer, Str, SetPersistentStore);
    FUNC_LEARNER (BookmarkSlawLearner, Gazetteer, ToSlaw);
    FUNC_TEACHER (BookmarkSlawTeacher, Gazetteer, Slaw, FromSlaw);

public:
    GazetteerTap () : KneeObjectTap () {
        AppendKeyedFunqs ("bookmark-persistent-store", &PersistentStoreLearner,
                          &PersistentStoreTeacher);
        AppendKeyedFunqs ("bookmark-slaw", &BookmarkSlawLearner,
                          &BookmarkSlawTeacher);
    }
    TAP_CONSTRUCTOR (GazetteerTap);
    CLONE_CONSTRUCTOR (Gazetteer);
};
}
}

#endif
