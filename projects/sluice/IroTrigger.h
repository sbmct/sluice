
/* (c)  oblong industries */

#ifndef IRO_TRIGGER_WILL_GET_TO_THAT_LATER
#define IRO_TRIGGER_WILL_GET_TO_THAT_LATER

#include <libBasement/TimedTrigger.h>
#include <libLoam/c++/ObColor.h>

namespace oblong {
namespace sluice {
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class IroTrigger : public TimedTrigger {
    PATELLA_SUBCLASS (IroTrigger, TimedTrigger);

protected:
    ObColor future_iro;

public:
    IroTrigger (float32 period);
    IroTrigger (float32 period, const ObColor &color);
    virtual ~IroTrigger ();

    void SetColor (const ObColor &color);
    const ObColor &Color () const;

    virtual ObRetort Fire (KneeObject *target, Atmosphere *atm);
};
}
} // the ol' ticker finally stops for namespaces mezzanine and oblong.

#endif
