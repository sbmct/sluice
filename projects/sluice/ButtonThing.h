
/* (c)  oblong industries */

#ifndef BUTTON_THING_BLING
#define BUTTON_THING_BLING

#include "BuffedAcetate.h"
#include "MzHandi.h"

#include <libNoodoo/GlyphoString.h>

#include <libLoam/c++/ObUniqueTrove.h>
#include <libLoam/c++/Str.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::loam;

/**
 * Simple, dumb button class. The button has a text label and a background
 * image that gives it some depth and makes it look clickable. The button can
 * change color to show three different states: docile, whacked, and pressed.
 */
class ButtonThing : public BuffedAcetate, public MzHandi::Evts {
    PATELLA_SUBCLASS (ButtonThing, BuffedAcetate);

protected:
    /// trove of pointing provenances that have whacked the button.
    ObUniqueTrove<Str> hoverers;

    ObRef<GlyphoString *> label;

    /// pointing provenance for pilot that makes a harden gesture while
    /// they also have the button whacked
    Str prv_pressed;

public:
    /// static colors for the various states of buttonness.
    static ObColor ButtonWhackedBackingColor ();
    static ObColor ButtonWhackedTextColor ();
    static ObColor ButtonWhackedBorderColor ();
    static ObColor ButtonBackingColor ();
    static ObColor ButtonTextColor ();
    static ObColor ButtonBorderColor ();
    static ObColor ButtonPressedBackingColor ();
    static ObColor ButtonPressedTextColor ();
    static ObColor ButtonPressedBorderColor ();

    /// default constructor: creates a button with an empty string.
    ButtonThing ();

    virtual void InitiallyUnfurl (Atmosphere *atm);
    virtual void ArrangeDenizens ();

    DOUBLE_ARG_HOOK_MACHINERY (ButtonPressed, ButtonThing *, Atmosphere *);

    bool Whacked () const;
    bool Whacked (const Str &provenance) const;

    void AssociateHoverProvenance (const Str &prv);
    void DissociateHoverProvenance (const Str &prv);

    void AssociatePressedProvenance (const Str &prv);
    void DissociatePressedProvenance (const Str &prv);

    virtual void SetLabelString (const Str &label);
    Str LabelString () const;

    virtual void SetStringPadding (const float64 &hrz, const float64 &vrt);

    virtual void SetFontSize (const float64 &s);

    virtual ObRetort MzHandiCondense (MzHandiCondenseEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiHarden (MzHandiHardenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiSoften (MzHandiSoftenEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiMove (MzHandiMoveEvent *e, Atmosphere *atm);
    virtual ObRetort MzHandiEvaporate (MzHandiEvaporateEvent *,
                                       Atmosphere *atm);
};
}
} // namespaces twillig, oblong

#endif
