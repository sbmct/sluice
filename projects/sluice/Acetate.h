
/* (c)  oblong industries */

#ifndef ACETATE_OVERLAY_IN_A_MEDICAL_ENCYCLOPEDIA
#define ACETATE_OVERLAY_IN_A_MEDICAL_ENCYCLOPEDIA

#include <libLoam/c++/ObPseudopod.h>

#include <libNoodoo/FlatThing.h>

#include <libBasement/TWrangler.h>

#include <libGanglia/RelayPath.h>
#include <libGanglia/SupervisedConductivity.h>
#include <libGanglia/ElectricalEvent.h>
#include <libGanglia/SwitchboardAgent.h>
#include <libGanglia/GangliaRetorts.h>

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class Hasselhoff;

class FeldGeomPod : public ObPseudopod {
    PATELLA_SUBCLASS (FeldGeomPod, ObPseudopod);

public:
    Vect norm, over, up;
    Vect cent;
    float64 width, height, view_dist, near_dist, far_dist, mullion_width;
    int32 pixw, pixh;

    FeldGeomPod () {}

    FeldGeomPod (const Vect &n,
                 const Vect &o,
                 const Vect &u,
                 const Vect &c,
                 float64 wid,
                 float64 hei,
                 float64 view_d,
                 float64 near_d,
                 float64 far_d,
                 float64 mul_w,
                 int32 pw,
                 int32 ph) {
        norm = n;
        over = o;
        up = u;
        cent = c;
        width = wid;
        height = hei;
        view_dist = view_d;
        near_dist = near_d;
        far_dist = far_d;
        mullion_width = mul_w;
        pixw = pw;
        pixh = ph;
    }
};

/**
 * Actetate is ... something.
 * Acetates might someday generically enable
 *    routing events to child objects
 *    saving self-representing proteins to disk
 *    restoring state from self-representing protiens on disk
 *    caching their view as an image
 *    presenting context card options
 *    saving action history to enable undo
 *    showing an exoskeleton
 *    maintaining user ownership / provenance
 *    arranging child elements in 3d space
 *    clipping child objects
 *
 * Acetates inhert from libNoodoo's FlatThing, but may arrange children
 * both within and outside of their bounding rectangles.
 */
class Acetate : public FlatThing, public SupervisedConductivity {
    PATELLA_SUBCLASS (Acetate, FlatThing);

public:
    ObWeakRef<ShowyThing *> parent;
    ObWeakRef<TWrangler *> lage_wrang;
    ObColor bord_iro;
    bool draw_outline;

    ObMap<int32, Slaw> state_digest_by_detail_level;

public:
    Acetate ();
    virtual ~Acetate ();

    ShowyThing *Parent () const;
    void SetParent (ShowyThing *prnt);
    virtual ObRetort AcknowledgeParentChange () { return OB_OK; }

    virtual void InitiallyUnfurl (Atmosphere *atm);

    virtual void ArrangeDenizens () {}

    virtual void InitializePoolRegistration (Hasselhoff *hoff) {}

    void SetBorderColor (const ObColor &iro);
    const ObColor &BorderColor () const;

    virtual ObRef<FeldGeomPod *> RetrieveGeometry () const;

    template <typename TYP> void SetParameter (const Str &pname, TYP val) {
        AppendWhatnot (pname, Slaw (val));
    }
    virtual Slaw RetrieveParameter (const Str &pname);

    virtual void SetLage (const Vect &p);
    virtual void SetLageHard (const Vect &p);
    virtual const Vect &Lage () const;
    virtual const Vect &LageGoalVal () const;

    virtual TWrangler *LageWrangler () const;

    virtual ObRetort AcknowledgeLocationChange ();

    virtual ObRetort ReportNovelty ();
    virtual ObRetort AcknowledgeNovelty (KneeObject *nov_obj);

    virtual const Slaw &StateDigest (int32 detail_level);
    const Slaw &StateDigest () { return StateDigest (0); }

    virtual ObRetort ReassembleStateDigest (int32 detail_level);
    ObRetort ReassembleStateDigest () { return ReassembleStateDigest (0); }

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

    SwitchboardAgent *ClosestParentalSwitchboardAgent () const;
    SwitchboardAgent *FurthestParentalSwitchboardAgent () const;

    SwitchboardAgent *
    ClosestAtmosphericSwitchboardAgent (Atmosphere *atm) const;
    SwitchboardAgent *
    FurthestAtmosphericSwitchboardAgent (Atmosphere *atm) const;

    template <class KLASSY> KLASSY *ClosestParent () const {
        if (ShowyThing *p = Parent ()) {
            if (KLASSY *k = dynamic_cast<KLASSY *> (p)) {
                return k;
            } else if (Acetate *a = dynamic_cast<Acetate *> (p)) {
                return a->ClosestParent<KLASSY> ();
            }
        }
        return NULL;
    }

    // a little conductivity convenience
    ObRetort DescentSingleTarget (KneeObject *trg) const;
    ObRetort DescentDoubleTarget (KneeObject *t1, KneeObject *t2) const;
    ObRetort
    DescentTripleTarget (KneeObject *t1, KneeObject *t2, KneeObject *t3) const;
    ObRetort DescentTargets (const ObTrove<KneeObject *> &targets) const;

    // the kind of utilities we can all get behind
    static void EnsoftifyLocusThingStandardly (LocusThing *lt);
    static void EnsoftifyFlatThingStandardly (FlatThing *ft);

    void PoolDeposit (const Str &logical_hose_name, const Protein &prt);
};
}
} // bye bye namespaces sluice, oblong

#if ACETATE_DEBUG
#define ACETOUT()                                                              \
    fprintf (stderr, "uffond fur-whale from <%p> (a %s).\n", this, ClassName ())
#else
#define ACETOUT()                                                              \
    do {                                                                       \
    } while (false)
#endif

#endif
