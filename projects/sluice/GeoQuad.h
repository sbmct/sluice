
#ifndef GEO_QUAD_SLEPT_THROUGH_MAGELLANS_CLASS
#define GEO_QUAD_SLEPT_THROUGH_MAGELLANS_CLASS

#include <libNoodoo/FlatThing.h>

#include "geo_utils.h"
#include "GeoProj.h"
#include "GeoMatrix.h"

namespace oblong {
namespace sluice {
using namespace oblong::noodoo;

/**
 * GeoQuad -->
 *
 * GeoQuad is a FlatThing whose geometry corresponds to a portion of a
 * cylindrically-projected geographic area. (e.g. Equirectangular or
 * Mercator map projection.)
 *
 * Coordinates & Conventions:
 *
 * There are 3 coordinate frames to consider: Geographic, Cartographic, and UV.
 *
 *  - "Geographic" coordinates are parameterized in terms of LatLon's
 *    (degrees latitude/longitude).
 *
 *  -  From there, a geographic projection (GeoProj) transforms those
 *coordinates
 *     into 2D "Cartographic" space - a normalized rectangular coordinate frame
 *     representing the entire valid range of projected LatLon's.
 *
 *  - UV space is similarly a 2D normalized space relative to the GeoQuad's
 *    geometric bounds. ***NOTE: we follow the FlatThing convention where (0,0)
 *    refers to the center of the quad, with the edges at a distance of 0.5,
 *    making up the normalized UV space.
 *
 * GeoQuad keeps track of a UV-to-Cartographic transformation (translate &
 *scale)
 * so that the contents of its geometry may be mapped back into any portion of
 * geometric space.
 *
 **/
class GeoQuad : public FlatThing {
    PATELLA_SUBCLASS (GeoQuad, FlatThing);

public:
protected:
    /// The projection for the geometric bounds / FlatThing extent
    ObRef<GeoProj *> projection;

    /// UV-to-Carto transformation & inverse.
    /// Note that we only expose the uv_xform portion, although GeoMatrix stores
    /// the inverse itself which is equivalent to carto_xform. That is the
    /// the preferred way to get at it.
    GeoMatrix uv_xform, carto_xform;

    //// DEBUGGING DRAWING STUFF
    /// Resolution of latitude / longitude guide lines as a power of 2
    ///
    int64 lat_lines_res, lon_lines_res;
    ObColor lat_lines_clr, lon_lines_clr;

    ObColor bounds_color;

    ObColor border_color;
    float64 border_size;

    float64 opacity;

public:
    GeoQuad ();
    GeoQuad (GeoProj *proj);
    virtual ~GeoQuad ();

    /// DEBUGGING LATLON LINES
    ///
    FLAG_MACHINERY (DrawLatLonLines);
    void SetLatLonLinesResolution (int64 nlat, int64 nlon);
    int64 LatLinesResolution () { return lat_lines_res; }
    int64 LonLinesResolution () { return lon_lines_res; }
    void SetLatLineColor (ObColor clr) { lat_lines_clr = clr; }
    void SetLonLineColor (ObColor clr) { lon_lines_clr = clr; }
    ObColor LatLinesColor () { return lat_lines_clr; }
    ObColor LonLinesColor () { return lon_lines_clr; }

    FLAG_MACHINERY (DrawFlatThingBounds);
    void SetBoundsColor (const ObColor &clr);
    ObColor BoundsColor () { return bounds_color; }

    FLAG_MACHINERY (DrawFlatThingBorder);
    void SetBorderColor (const ObColor &clr);
    void SetBorderSize (float64 s) { border_size = s; }
    ObColor BorderColor () { return border_color; }
    float64 BorderSize () { return border_size; }

    void SetOpacity (const float64 x) {
        GeoQuad::SetAdjColor (ObColor (1.0, x));
    }

    /**
     * The geographic projection. That which defines LatLon <--> CxCy.
     */
    GeoProj *Projection ();
    void SetProjection (GeoProj *proj);

    /// Use our geometry and geo-projection as a basis for others.
    ///
    /// GeoAlignOthersProjection -->
    ///   Sets other's projection, keeping its loc, size the same.
    ///
    /// GeoAlignOthersGeometry -->
    ///   Sets other's loc, size, & ori, keeping its projection the same.
    ///
    ///   Note : this should first project the other GeoQuad's geometry to be
    ///   co-planar with us.
    ///
    ObRetort GeoAlignOthersProjection (GeoQuad *gq) const;
    ObRetort GeoAlignOthersGeometry (GeoQuad *gq) const;

    //   /// set loc from latlon
    //   using FlatThing::SetLoc;
    //   using FlatThing::SetLocHard;

    //   void SetLoc (const LatLon &ll);
    //   void SetLoc (const LatLon &ll, V::Align vrt, H::Align hrz);
    //   void SetLoc (const LatLon &ll, float64 vrt, float64 hrz);
    //   void SetLocHard (const LatLon &ll);
    //   void SetLocHard (const LatLon &ll, V::Align vrt, H::Align hrz);
    //   void SetLocHard (const LatLon &ll, float64 vrt, float64 hrz);

    /// The following declarations with LatLon arguments hide the FlatThing
    /// versions of those functions. This tells the compiler to bring them
    /// along.
    ///
    using FlatThing::LocOf;
    using FlatThing::LocGoalValOf;

    virtual Vect LocOf (const LatLon &ll) const;
    virtual Vect LocGoalValOf (const LatLon &ll) const;

    /// inscribe a geographic area
    //  void InscribeInside (const LatLon &bl, const LatLon &tr);
    //  void InscribeInside (const v2float64 &cxcy_bl, const LatLon &cxcy_tr);

    /**
     * UVXform
     *
     * The UVXform is central to how geometry and geography relate in GeoQuad.
     *
     * To start, remember the coordinate systems at work. A traditional map is
     * defined in polar (or spherical) coordinates of Latitude vs. Longitude
     * (and maybe altitude, but we'll let that slide for now, eh?). In the world
     * of FlatThings (such as this GeoQuad here), we often describe space self-
     * referntially in terms of units of our width or height and in directions
     *of
     * our Up, Over, & Norm vectors. GeoQuad calls this reference frame "UV"
     *space
     * where the unit square centered at UV [0,0] corresponds to the FlatThing
     * extent. From there our parent classes, LocusThing & FlatThing, make the
     *jump
     * into"world space" trivial.
     *
     * But wait, how on earth do we go between the narcisistic FlatThing
     * coordinates and the geographic space of a map? That's where this UVXform
     * bit comes in.
     *
     * Remember that GeoProj defines "Cartographic" space as the 'normalized,
     * projected coordinate space' whose unit square centered at the origin
     * corresponds to the full valid range of latitude and longitude. That is:
     *   - CxCy [0,0] --> LatLon [0,0]
     *   - CxCy [-0.5, -0.5] --> LatLon [ MinValidLat, MinValidLon ]
     *   - CxCy [0.5, 0.5] --> LatLon [ MaxValidLat, MaxValidLon ]
     *
     * The UVXform, then is simply a linear transformation between the
     *Cartographic
     * and UV coordinate frames.
     *
     **/

    /// UV Xform accessors - returned as new objects on the stack.
    GeoMatrix UVXform () const;

    void SetUVXform (const GeoMatrix &m);

    /// set xform indirectly such that:  UV[-0.5,-0.5] --> ll_bl  &  UV[0.5,0.5]
    /// --> ll_tr
    /// (Convenient for uv's completely within latlon range)
    /// That is, "show me this geographic region, i don't care about your
    /// geometry"
    /// Note: this has to be virtual and at the GeoQuad level because it's
    /// dependent
    /// on having a GeoProj to convert the LatLon's.
    void SetUVXformByCorners (const v2float64 &uv_bl, const v2float64 &uv_tr);
    void SetUVXformByCorners (const float64 &u_bl,
                              const float64 &v_bl,
                              const float64 &u_tr,
                              const float64 &v_tr);

    void SetUVXformByCarto (const v2float64 &cxcy_bl, const v2float64 &cxcy_tr);
    void SetUVXformByCarto (const float64 &cx_bl,
                            const float64 &cy_bl,
                            const float64 &cx_tr,
                            const float64 &cy_tr);

    void SetUVXformByLatLon (const LatLon &ll_bl, const LatLon &ll_tr);

    /**
     * High level transform operations
     *  PanBy -> Move the map by units specified in UV space
     *  ZoomAbout -> zoom the map, keeping the given point fixed
     */
    void PanTo (const float64 &cx, const float64 cy);
    void PanTo (const LatLon &ll);

    void PanBy (const float64 &du, const float64 &dv);
    void PanBy (const v2float64 &dudv);
    // void PanByLatLon (const LatLon &dee_ll, V::Align vrt = V::Center,
    // H::Align hrz = Center);

    void Zoom (const float64 &sc);
    void ZoomAbout (const float64 &sc, const float64 &u, const float64 &v);
    void
    ZoomAbout (const float64 &sc, const V::Align &vrt, const H::Align &hrz);
    void ZoomAboutLatLon (const float64 &sc, const LatLon &ll);

    /**
     * Convenience methods that don't necessarily work in the general cases
     * (i.e. once rotation is introduced)
     */
    enum Geo_Square_Up_Behavior { Geo_Keep_Lat = 0, Geo_Keep_Lon };
    void SquareUp (Geo_Square_Up_Behavior sub = Geo_Keep_Lat);

    void SnapToValidLatRange ();

    /// Intended for subclasses to use to perform any updates needed whenever
    /// the
    /// geographic region encapsulated by this quad changes.
    virtual ObRetort AcknowledgeUVXformChange ();

    virtual ObRetort AcknowledgeLocationChange ();
    virtual ObRetort AcknowledgeSizeChange ();
    virtual ObRetort AcknowledgeOrientationChange ();

    SINGLE_ARG_HOOK_MACHINERY (SizeChange, GeoQuad *);
    SINGLE_ARG_HOOK_MACHINERY (LocationChange, GeoQuad *);
    SINGLE_ARG_HOOK_MACHINERY (OrientationChange, GeoQuad *);
    SINGLE_ARG_HOOK_MACHINERY (CartoCoordsChange, GeoQuad *);

    /// Utility Functions -->
    ///
    /// Some useful functions for dealing with the various coordinate frames
    /// like projecting a 3D vect to the closest LatLon on the plane of this
    /// quad.
    ///
    void VectToClosestUV (const Vect &vec, v2float64 &uv) const;
    void VectToClosestUV (const Vect &vec, float64 &u, float64 &v) const;
    v2float64 VectToClosestUV (const Vect &vec) const;

    void VectToLatLon (const Vect &v, LatLon &ll) const;
    LatLon VectToLatLon (const Vect &v) const;

    /// Carto <--> UV
    ///
    /// These functions do the simple conversion between cartographic (CXCY)
    /// space and the transformed rectangular (UV) space. As with the above
    /// cases,
    /// the result can be obtained either by reference or as a new object on the
    /// stack.
    ///
    void CartoToUV (const float64 &cx,
                    const float64 &cy,
                    float64 &u,
                    float64 &v) const;
    void CartoToUV (const v2float64 &cxy, v2float64 &uv) const;

    v2float64 CartoToUV (const float64 &cx, const float64 &cy) const;
    v2float64 CartoToUV (const v2float64 &cxcy) const;

    void UVToCarto (V::Align vrt, H::Align hrz, float64 &cx, float64 &cy) const;
    void UVToCarto (V::Align vrt, H::Align hrz, v2float64 &cxcy) const;
    void UVToCarto (const float64 &u,
                    const float64 &v,
                    float64 &cx,
                    float64 &cy) const;
    void UVToCarto (const v2float64 &uv, v2float64 &cxcy) const;

    v2float64 UVToCarto (V::Align vrt, H::Align hrz) const;
    v2float64 UVToCarto (const float64 &u, const float64 &v) const;
    v2float64 UVToCarto (const v2float64 &uv) const;

    /// UV <--> LatLon
    ///
    /// There are options to receive the result via reference or as a new
    /// object on the stack.
    ///
    void UVToLatLon (V::Align vrt, H::Align hrz, LatLon &ll) const;
    void UVToLatLon (const float64 &u, const float64 &v, LatLon &ll) const;
    void UVToLatLon (const v2float64 &uv, LatLon &ll) const;

    LatLon UVToLatLon (V::Align vrt, H::Align hrz) const;
    LatLon UVToLatLon (const float64 &u, const float64 &v) const;
    LatLon UVToLatLon (const v2float64 &uv) const;

    void LatLonToUV (const float64 &lat,
                     const float64 &lon,
                     float64 &u,
                     float64 &v) const;
    void LatLonToUV (const LatLon &ll, float64 &u, float64 &v) const;
    void LatLonToUV (const LatLon &ll, v2float64 &uv) const;

    v2float64 LatLonToUV (const float64 &lat, const float64 &lon) const;
    v2float64 LatLonToUV (const LatLon &ll) const;

    v2float64 LatLonToCarto (const float64 &lat, const float64 &lon) const;

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

private:
    void Init ();
};
}
} /// end namespaces oblong, staging

#endif /// GEO_QUAD_SLEPT_THROUGH_MAGELLANS_CLASS
