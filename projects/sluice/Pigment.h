
#pragma once

#include "Shader.h"
#include "NodeBuffer.h"
#include <libNoodoo/GLTex.h>

#include <set>
#include <vector>

namespace oblong {
namespace sluice {

class Node;

struct PigPod {
    enum PigmentType { NONE, NODE, PATH };
    static PigmentType TypeFromStr (const Str);

    PigPod (PigmentType pt,
            Str _name,
            Str _tex,
            SoftColor *_c,
            SoftFloat *_f,
            const float64 mz)
        : typ (pt), name (_name), tex_path (_tex), color (_c), size (_f),
          min_zoom (mz) {}

    PigmentType typ;
    Str name, tex_path;
    ObRef<SoftColor *> color;
    ObRef<SoftFloat *> size;
    float64 min_zoom;

    SoftColor *Color () const {
        return (NULL == ~color) ? NULL : (~color)->Dup ();
    }
    SoftFloat *Size () const {
        return (NULL == ~size) ? NULL : (~size)->Dup ();
    }

    const bool operator<(const PigPod &other) const;
    const bool operator>(const PigPod &other) const;
    const bool operator==(const PigPod &other) const;
    const bool operator!=(const PigPod &other) const;
};

class Pigment : public KneeObject {
    PATELLA_SUBCLASS (Pigment, KneeObject);

protected:
    ObRef<Shader *> shady;
    ObRef<GLTex *> tex;

    const float64 TexWidth () { return (~tex) ? (~tex)->Width () : 1.0; }

    const float64 ImgWidth () const;
    const float64 ImgHeight () const;

    bool vbo_is_fresh;
    int64 current_count;
    Str image;

    float64 minimum_zoom;

    ObRetort AssureTexture ();
    virtual ObRetort Recalc (GeoProj *proj, const float64 t) = 0;
    virtual ObRetort AssureVBO (GeoProj *proj, const float64 t) = 0;
    virtual ObRetort RenderVBO (const ObColor &adj,
                                const float64 node_size,
                                const float64 t) = 0;

    SOFT_MACHINERY (SoftColor, NodeColor);
    SOFT_MACHINERY (SoftFloat, NodeSize);

    SINGLE_ARG_HOOK_MACHINERY (SetupUniforms, Shader *);

public:
    Pigment ();
    virtual ~Pigment ();

    static Pigment *FromPod (const PigPod pod);

    void SetShader (Shader *_s) { shady = _s; }
    Shader *GetShader () const { return ~shady; }

    ObRetort Render (GeoProj *proj,
                     const ObColor &adj,
                     const float64 node_size,
                     const float64 t);

    void SetTextureImage (const Str img) { image = img; }

    void SetMinimumZoom (const float64 x) { minimum_zoom = x; }
    const float64 MinimumZoom () const { return minimum_zoom; }

    virtual void AddNode (Node *n) = 0;
    virtual void RemoveNode (Node *n) = 0;
    virtual bool HasNode (Node *) const = 0;
    /// Bulk add/remove nodes

    virtual void HardReset () = 0;
    virtual bool IsEmpty () const = 0;
    virtual bool IsReady () const = 0;

    /** WARNING! This does NOT generate a Slaw that can be correctly
     *  read back in as a pigment!!!  At least, not yet.
     *  It is intended to be used for debugging.
     */
    virtual Slaw DescribeAsSlaw () const;
};
/**
 * A description of how a node should be rendered
 *
 * Attributes include:
 * - Texture (image)
 * - Color (soft color)
 * - Size (soft float)
 * - Shader (vertex and fragment)
 */
class NodePigment : public Pigment {
    PATELLA_SUBCLASS (NodePigment, Pigment);

protected:
    GLuint vbo_id;
    int FLOATS_PER_VERTEX;
    virtual ObRetort Recalc (GeoProj *proj, const float64 t);
    virtual ObRetort AssureVBO (GeoProj *proj, const float64 t);
    virtual ObRetort
    RenderVBO (const ObColor &adj, const float64 node_size, const float64 t);

    ObTrove<NodeBuffer *> buffers;
    std::map<Node *, NodeBuffer *> buffer_map;

public:
    NodePigment ();
    virtual ~NodePigment ();

    virtual void AddNode (Node *n);
    virtual void RemoveNode (Node *n);
    virtual bool HasNode (Node *) const;

    virtual void HardReset ();
    virtual bool IsEmpty () const;
    virtual bool IsReady () const;

    /** WARNING! This does NOT generate a Slaw that can be correctly
     *  read back in as a pigment!!!
     *  It is intended to be used for debugging.
     */
    virtual Slaw DescribeAsSlaw () const;
    const unt32 Id () const { return Name ().Hash (); }
};

class PathPigment : public Pigment {
    PATELLA_SUBCLASS (PathPigment, Pigment);

protected:
    GLuint vbo_id;
    int FLOATS_PER_VERTEX;
    int64 path_count;

    ObTrove<PathBuffer *> buffers;
    std::map<Node *, PathBuffer *> buffer_map;

    typedef std::vector<Vect> vects;

    /// Shader settings for speed and length of ribbon animation
    SOFT_MACHINERY (SoftFloat, RibbonLengthScale);
    SOFT_MACHINERY (SoftFloat, RibbonOffsetSpeed);

    const int64 PathCount (const float64 t) const;
    void ExpandLine (
        GeoProj *proj, Node *n, vects &verts, vects &sideways, vects &texcoors);

    virtual ObRetort Recalc (GeoProj *proj, const float64 t);
    virtual ObRetort AssureVBO (GeoProj *proj, const float64 t);
    virtual ObRetort
    RenderVBO (const ObColor &adj, const float64 node_size, const float64 t);

public:
    PathPigment ();
    virtual ~PathPigment ();

    virtual void AddNode (Node *n);
    virtual void RemoveNode (Node *n);
    virtual bool HasNode (Node *) const;

    virtual void HardReset ();
    virtual bool IsEmpty () const;
    virtual bool IsReady () const;

    /** WARNING! This does NOT generate a Slaw that can be correctly
     *  read back in as a pigment!!!
     *  It is intended to be used for debugging.
     */
    virtual Slaw DescribeAsSlaw () const;
};
}
}
