
/* (c)  oblong industries */

#ifndef BOOG_REPORTER_YELPINGS
#define BOOG_REPORTER_YELPINGS

#include "libNoodoo/TexQuad.h"

#include "libBasement/TWrangler.h"
#include "libBasement/SWrangler.h"
#include "libBasement/RWrangler.h"

#include "libBasement/LinearColor.h"
#include "libImpetus/OEDisplacement.h"
#include "libImpetus/OEBlurt.h"

#include "Tweezers.h"

namespace oblong {
namespace greenwich {

using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::loam;
using namespace oblong::sluice;

/// UI element designed to report on shoves into a plane.
class PushbackViz : public Tweezers,
                    public OEDisplacement::Evts,
                    public OEBlurt::Evts {
    PATELLA_SUBCLASS (PushbackViz, Tweezers);

OB_PROTECTED:
    ObRef<TexQuad *> outer_boog;
    ObRef<TexQuad *> inner_boog;
    ObRef<TexQuad *> arrow_boog;
    ObRef<TexQuad *> lock_boog;
    ObRef<RWrangler *> rot_wrang;
    ObRef<SWrangler *> sca_wrang;
    ObRef<TWrangler *> tra_wrang;
    ObRef<SWrangler *> lock_sca;
    float32 dead_zone_depth;
    float32 lat_scroll_offset;
    float32 lock_anticipation_offset;
    float32 lock_offset;

    float32 max_shove_offset;

    Str shove_prov;
    Vect maneuver_start;

    Vect shove_cur, shove_prv;
    Vect shove_direc;

    Vect shimmy_cur, shimmy_prv;
    Vect shimmy_direc;

    FatherTime shove_uhr;

    bool crossed_detent;

    KneeObject *listener;

    SOFT_MACHINERY (SoftVect, ShoveOff);

public:
    PushbackViz (const Str &outIm,
                 const Str &inIm,
                 const Str &arrowIm,
                 const Str &lockIm,
                 const Str &pIm);

    void SetListener (KneeObject *target) { listener = target; }

    void SetShoveDirection (const Vect &v) { shove_direc = v; }

    void SetShimmyDirection (const Vect &v) { shimmy_direc = v; }

    float32 DeadZoneDepth () const { return dead_zone_depth; }
    void SetDeadZoneDepth (float32 dzd) { dead_zone_depth = dzd; }

    float32 LateralScrollOffset () const { return lat_scroll_offset; }
    void SetLateralScrollOffset (float32 lso) { lat_scroll_offset = lso; }

    float32 LockAnticipationOffset () const { return lock_anticipation_offset; }
    void SetLockAnticipationOffset (float32 lao) {
        lock_anticipation_offset = lao;
    }

    float32 LockOffset () const { return lock_offset; }
    void SetLockOffset (float32 lo) { lock_offset = lo; }

    float32 MaxShoveOffset () const { return max_shove_offset; }
    void SetMaxShoveDepth (float mso) { max_shove_offset = mso; }

    float64 Transgress () const;
    bool IsLocked () const;
    float64 LockDistance () const;

    virtual ObRetort AcknowledgeSizeChange () {
        TexQuad *boo;
        if (boo = ~outer_boog)
            boo->SetSize (Width (), Height ());
        if (boo = ~inner_boog)
            boo->SetSize (Width (), Height ());
        if (boo = ~arrow_boog)
            boo->SetSize (Width (), Height ());
        if (boo = ~lock_boog)
            boo->SetSize (Width (), Height ());
        return OB_OK;
    }

    virtual ObRetort AcknowledgeLocationChange () {
        if (~tra_wrang)
            (~tra_wrang)->SetTranslation (Loc ());
        return OB_OK;
    }

    void SetInnerScale (float s) {
        if (~sca_wrang)
            (~sca_wrang)->SetScale (s);
    }

    void SetRotter (float ang) {
        if (~rot_wrang)
            (~rot_wrang)->SetAngle (ang);
    }

    void SetDegreesRotter (float degAng) { SetRotter (M_PI / 180.0 * degAng); }

    void AddRotter (float diffAng) {
        if (~rot_wrang)
            (~rot_wrang)->SetAngle ((~rot_wrang)->Angle () + diffAng);
    }

    void AddDegreesRotter (float degDiffAng) {
        AddRotter (M_PI / 180.0 * degDiffAng);
    }

    void SetColor (const ObColor &c) {
        SetAdjColor (c);
        if (~inner_boog)
            (~inner_boog)->SetAdjColor (c);
    }

    void SetOuterColor (const ObColor &c) { SetAdjColor (c); }

    void SetInnerColor (const ObColor &c) {
        if (~inner_boog)
            (~inner_boog)->SetAdjColor (c);
    }

    void SetAlpha (float a) {
        ObColor c = AdjColor ();
        c.SetAlpha (a);
        SetAdjColor (c);
        if (~inner_boog) {
            c = (~inner_boog)->AdjColor ();
            c.SetAlpha (a);
            (~inner_boog)->SetAdjColor (c);
        }
    }

    void SetOuterAlpha (float a) {
        ObColor c = AdjColor ();
        c.SetAlpha (a);
        SetAdjColor (c);
    }

    void SetInnerAlpha (float a) {
        if (~inner_boog) {
            ObColor c = (~inner_boog)->AdjColor ();
            c.SetAlpha (a);
            (~inner_boog)->SetAdjColor (c);
        }
    }

    void FadeTo (double ft) { AdjColorSoft ()->Set (ObColor (1.0, ft)); }

    virtual ObRetort PreDraw (VisiFeld *vf, Atmosphere *atm) {
        glPushAttrib (GL_DEPTH_BUFFER_BIT);
        glDisable (GL_DEPTH_TEST);
        return OB_OK;
    }
    virtual ObRetort PostDraw (VisiFeld *vf, Atmosphere *atm) {
        glPopAttrib (); // GL_DEPTH_BUFFER_BIT
        return OB_OK;
    }

    virtual ObRetort AcknowledgeOrientationChange ();

    virtual void ConformAppearance ();

    virtual ObRetort OEDisplacementAppear (OEDisplacementAppearEvent *e,
                                           Atmosphere *atm);
    virtual ObRetort OEDisplacementVanish (OEDisplacementVanishEvent *e,
                                           Atmosphere *atm);
    virtual ObRetort OEDisplacementMove (OEDisplacementMoveEvent *e,
                                         Atmosphere *atm);

    virtual ObRetort
    ReturnStroke (const Slaw &news, ElectricalEvent *ee, Atmosphere *atm);
};
}
} // end namespaces

#endif
