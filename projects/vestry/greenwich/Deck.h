#ifndef __DECK_H__
#define __DECK_H__

#include <libNoodoo/FlatThing.h>
#include <libStaging/GridLayout.h>
#include "Slide.h"

namespace oblong {
namespace greenwich {

using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;
using namespace oblong::staging;

class Deck : public FlatThing {
    PATELLA_SUBCLASS (Deck, FlatThing);

public:
    enum DeckLayoutOption { FullscreenSlides, FitSlidesWithin };

    Deck ();

    void SetLayoutMode (DeckLayoutOption mode) { layoutMode = mode; }

    ObRetort GotoSlide (int iSlide);
    ObRetort PreviousSlide ();
    ObRetort NextSlide ();

    void OffsetSlidesHard (const Vect &dv);

    Slide *SlideFromIndex (int index);
    Slide *SlideFromID (const Str &id);

    /// \return index of slide closest to the given point; -1 if no slides
    int ClosestSlide (const Vect &v) const;

    /// \return slide index from ID (-1 if not found)
    int IndexFromID (const Str &id) const;

    // \return slide ID from index (-1 if not found)
    Str IDFromIndex (int index) const;

    /// \return number of slides in deck
    int NumSlides () const { return ChildCount (); }

    /// set current slide to the given slide represented by its index
    ObRetort SetCurrentSlideByIndex (int iSlide);

    /// set current to the given slide represented by its ID
    ObRetort SetCurrentSlideByID (const Str &id);

    /// \return index of current (i.e., centered) slide
    int CurrentIndex () const { return currentIndex; }

    /// \return ID of current (i.e., centered) slide
    Str CurrentID () const { return currentID; }

    /// update the deck to match the given set of slides
    void UpdateSlides (const ObTrove<Slide *> &slides);

    /// insert the given slide into the deck
    bool InsertSlide (Slide *slide);

    /// remove the given slide
    ObRetort RemoveSlideByID (const Str &id);

    // swap the position of the given slides
    ObRetort SwapSlidesByID (const Str &idA, const Str &idB);

    // change the position of the given slide
    ObRetort ReorderSlide (const Str &id, int64 ordinal);

    virtual ObRetort AcknowledgeSizeChange ();
    virtual ObRetort AcknowledgeOrientationChange ();
    virtual ObRetort AcknowledgeLocationChange ();

    virtual void DrawSelf (VisiFeld *v, Atmosphere *a);

    void DoLayout ();

protected:
    /// update current slide index based on ID
    bool UpdateCurrentSlideInfo ();
    void MoveSlides (const Vect &dv);

    void DoLayoutFull ();
    void DoLayoutOverview ();

    int currentIndex;
    Str currentID;
    float64 gap;
    DeckLayoutOption layoutMode;
};
}
} // end namespace

#endif
