
/* (c)  oblong industries */

#include <libLoam/c++/ArgParse.h>
#include <libNoodoo/VisiDrome.h>
#include <libAfferent/Gestator.h>
#include <libAfferent/DualCase.h>
#include <libMedia/PngImageClot.h>
#include <libNoodoo/ObStyle.h>

#include "DeckManager.h"

using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;
using namespace oblong::afferent;
using namespace oblong::media;
using namespace oblong::greenwich;

////////////////////////////////////////////////////////////

class TexBG : public TexRect {
    PATELLA_SUBCLASS (TexBG, TexRect);
    virtual void DrawSelf (VisiFeld *v, Atmosphere *a) {
        glDepthMask (false);
        TexRect::DrawSelf (v, a);
        glDepthMask (true);
    }
};

////////////////////////////////////////////////////////////

ObRetort Setup (VisiDrome *vd, VisiFeld *vf, Atmosphere *atm) {
    static DeckManager *deckMgr = NULL;
    static Gestator *g = NULL;

    if (vf == NULL)
        return OB_UNKNOWN_ERR;

    if (g == NULL) {
        g = new Gestator;
        g->SetSenseCase (new DualCase (150));
        Str gpoo (getenv ("GRIPES_POOL"));
        g->SetGripesPoolName (gpoo.IsNull () ? "tcp://localhost/gripes" : gpoo);
        vd->AppendChild (g);
    }

    // install background texture
    ImageClot *img = PngImageClot::FindOrLoadImageClotBySourceName ("bg.png");
    if (img != NULL) {
        TexGL *tex = new TexGL (img, TexGL::NoMipmap, TexGL::POT);
        TexBG *quad = new TexBG;
        quad->SetStretchMode (TexRect::Stretch);
        quad->SetTexture (tex);
        quad->SetAlignment (FlatThing::V::Center, FlatThing::H::Center);
        quad->OrientLikeFeld (vf);
        quad->SetSize (vf->Width (), vf->Height ());
        quad->SetLoc (vf->Loc ());
        vf->AppendChild (quad);
    } else {
        fprintf (stderr, "Warning: failed to load bg.png\n");
    }

    if (deckMgr == NULL) {
        deckMgr = new DeckManager (g);

        // are we (more) landscape or portrait?
        if (fabs (vf->Over ().Dot (Vect (0, 1, 0))) <
            cos (M_PI / 4)) // over vector is mostly horizontal
            deckMgr->SetSize (vf->Width (), vf->Height ());
        else
            deckMgr->SetSize (vf->Height (), vf->Width ());
        deckMgr->SetLoc (vf->Loc ());
        deckMgr->OrientLikeFeld (vf);

        // tell deck manager how to request data from the net
        Str sPool (getenv ("NETFETCH_POOL"));
        if (sPool.IsNull ())
            sPool = "net-fetch";
        deckMgr->SetNetFetchPool (sPool);

        // listen to gloveless data pool
        sPool = getenv ("GLOVELESS_POOL");
        if (sPool.IsNull ())
            sPool = "gloveless";
        printf ("Gloveless Pool: [%s]\n", sPool.utf8 ());
        vd->PoolParticipate (sPool, deckMgr);

        // listen to mezzanine
        sPool = getenv ("MEZZ_FROM_NATIVE_POOL");
        if (sPool.IsNull ()) {
            Str sRemote = getenv ("GREENWICH_REMOTE");
            if (sRemote.IsNull ())
                sPool = "tcp://mezz107/mz-from-native";
            else
                sPool = "tcp://" + sRemote + "/mz-from-native";
        }
        printf ("Mezzanine Pool: [%s]\n", sPool.utf8 ());
        vd->PoolParticipate (sPool, deckMgr);

        // listen to drome's pool
        printf ("Drome Pool: [%s]\n", vd->DromePoolName ().utf8 ());
        vd->PoolParticipate ("drome-pool",
                             deckMgr); // vd->DromePoolName(), deckMgr);
    }

    vf->AppendChild (deckMgr);
    vf->SetBackgroundColor (ObStyle::FeldBackingColor ());

    VisiFeld *cent_vf = VisiFeld::FindByName ("main");
    if (!cent_vf) {
        fprintf (stderr, "can't find 'main' feld... should be here by now.\n");
        exit (-1);
    }

    Vect look_t = cent_vf->PhysLoc ();
    Vect look_f =
        cent_vf->PhysLoc () + cent_vf->PhysDist () * cent_vf->PhysNorm ();

    if (LookyCam *cam = vf->Camera ()) {
        cam->ProclaimPerspective (
            2.0 * atan (0.5 * vf->Width () / vf->ViewDist ()),
            2.0 * atan (0.5 * vf->Height () / vf->ViewDist ()),
            10.0,
            20000.0);
        cam->LookFromTo (look_f, look_t, Vect (0.0, 1.0, 0.0)); // yep: 'up'.
        Vect delta = vf->PhysLoc () - cent_vf->Loc ();
        cam->EmbraceFrustumShear (delta.Dot (vf->Over ()),
                                  delta.Dot (vf->Up ()));
        cam->EnableRubbishPlanes ();
    }

    return OB_OK;
}

void usage (ArgParse &ap) {
    printf ("Usage: greenwich [options]\n");
    printf (" Options:\n");
    printf ("%s\n", ap.UsageMessage ().utf8 ());
}

int main (int ac, char **av) {
    bool bHelp = false;

    // parse command line arguments
    ArgParse ap;
    ap.ArgFlag ("?", "\adisplay usage information", &bHelp);
    ap.Alias ("?", "h");

    if (!ap.Parse (ac, av)) {
        fprintf (stderr, "%s\n", ap.ErrorMessage ().utf8 ());
        return EXIT_FAILURE;
    }

    if (bHelp) {
        usage (ap);
        return EXIT_FAILURE;
    }

    VisiDrome *drome = new VisiDrome ("greenwich", ac, av);
    drome->AppendFeldSetupHook (Setup, "greenwich-setup");
    drome->SetDromePoolConfiguration (Pool::MMAP_LARGE);
    drome->SetRespirePeriod (1.0 / 2.0);
    drome->Respire ();
    drome->Delete ();
    return EXIT_SUCCESS;
}
