#ifndef __VIEW_WARNING__
#define __VIEW_WARNING__

#include <libNoodoo/FlatThing.h>

namespace oblong {
namespace greenwich {

using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::loam;

class ViewWarning : public FlatThing {
    PATELLA_SUBCLASS (ViewWarning, FlatThing);

public:
    enum ViewWarningFlag { Left = 0, Top, Right, Bottom, Near, Far, NumVWF };

    ViewWarning ();

    virtual void DrawSelf (VisiFeld *vf, Atmosphere *atm);

    bool Warning (ViewWarningFlag flag) const;
    float Severity (ViewWarningFlag flag) const;

    void SetSeverity (ViewWarningFlag flag, float v);
    void ClearWarnings ();

    void SetBaseColor (const ObColor &color);
    void SetMaxAlpha (float alpha);

protected:
    float severity[NumVWF];
    float alphaMax;
    ObColor baseColor;
};
}
} // end namespace

#endif
