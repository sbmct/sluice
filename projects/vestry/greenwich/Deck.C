#include "libLoam/c/ob-time.h"
#include "libLoam/c++/ObTrove.h"
#include <libStaging/Plane3.h>
#include "DeckManager.h"

using namespace oblong::staging;
using namespace oblong::greenwich;

Deck::Deck () : currentIndex (-1), gap (30.0), layoutMode (FullscreenSlides) {
    SetAlignment (FlatThing::V::Center, FlatThing::H::Center);
}

ObRetort Deck::AcknowledgeSizeChange () {
    DoLayout ();
    return OB_OK;
}

ObRetort Deck::AcknowledgeOrientationChange () {
    DoLayout ();
    return OB_OK;
}

ObRetort Deck::AcknowledgeLocationChange () {
    DoLayout ();
    return OB_OK;
}

void Deck::DrawSelf (VisiFeld *vf, Atmosphere *atm) {
    // we want depth testing for slides
    glEnable (GL_DEPTH_TEST);
    glDepthMask (GL_TRUE);
    glDepthFunc (GL_LEQUAL);

#ifdef DEBUG_OUTLINE
    glDisable (GL_TEXTURE_2D);
    OB_glColor (ObColor (255, 0, 0));
    glBegin (GL_LINE_LOOP);
    const Vect vtl = LocOf (FlatThing::V::Top, FlatThing::H::Left);
    const Vect over = Over ();
    const Vect up = Up ();
    const double height = Height ();
    const double width = Width ();
    OB_glVertex (vtl);
    OB_glVertex (vtl + over * width);
    OB_glVertex (vtl + over * width - up * height);
    OB_glVertex (vtl - up * height);
    glEnd ();
#endif

    // figure out which children are currently visible
    LookyCam::RPlanes *rp = vf->RubbishPlanes ();
    if (layoutMode == FullscreenSlides && rp != NULL) {
        // LookyCam* cam = vf->Camera();
        //     printf("camera: (%.2f, %.2f, %.2f) <%.2f, %.2f, %.2f>\n",
        //            cam->ViewLoc().x, cam->ViewLoc().y, cam->ViewLoc().z,
        //            cam->ViewAim().x, cam->ViewAim().y, cam->ViewAim().z);
        //     printf("    up: <%.2f, %.2f, %.2f> <%.2f, %.2f, %.2f>\n",
        //            cam->ViewUp().x, cam->ViewUp().y, cam->ViewUp().z,
        //            cam->ViewTrueUp().x, cam->ViewTrueUp().y,
        //            cam->ViewTrueUp().z);

        ObCrawl<KneeObject *> cr = ChildCrawl ();
        for (int index = 0; !cr.isempty (); index++) {
            Slide *slide = dynamic_cast<Slide *>(cr.popfore ());
            if (slide == NULL)
                continue;

            // TODO make this work for both landscape and portrait mode
            // issues with width/height
            // what is camera's orientation?

            Vect p = vf->CumuWranglerPointMat ().TransformVect (slide->Loc ());

            // printf("kid %d [%.2f x %.2f]: ", index, slide->Width(),
            // slide->Height());
            const double eps = gap * 0.3;
            int nin = 0, nout = 0, nint = 0; // in, out, intersect
            for (int i = 0; i < rp->num; i++) {
                Plane3 plane (rp->pnt[i], rp->nrm[i]);
                double rad = slide->RadiusInDirection (plane.Normal ()) - eps;
                double dist = plane.Distance (p);
                // printf(" (%.2f, %.2f) ", rad, dist);

                // full visibility test
                // if (dist > rad) nout++;
                // else if (dist < -rad) nin++;
                // else nint++;

                // at least half visible
                if (dist > rad)
                    nout++;
                else if (dist <= 0.0)
                    nin++;
                else
                    nint++;
            }

            slide->SetVisible (nin == rp->num);
            // printf(" %d, %d, %d\n", nin, nout, nint);
        }
    }
}

int Deck::ClosestSlide (const Vect &v) const {
    // we're going to naively look at each slide and find the closest
    const int nSlides = ChildCount ();
    int iBest = -1;
    double bestDist = INFINITY;
    for (int i = 0; i < nSlides; i++) {
        FlatThing *slide = dynamic_cast<FlatThing *>(NthChild (i));
        if (slide == NULL)
            continue;
        double dist = slide->Loc ().DistFrom (v);
        if (dist < bestDist) { // is this the closest slide so far?
            iBest = i;
            bestDist = dist;
        }
    }
    return iBest;
}

void Deck::UpdateSlides (const ObTrove<Slide *> &slides) {
    // first, extract current slides
    ObCrawl<KneeObject *> kr = ChildCrawl ();
    ObTrove<KneeObject *> kids;
    while (!kr.isempty ())
        kids.Append (kr.popfore ());
    assert (kids.Count () == ChildCount ());
    RemoveAllChildren ();

    // now add new slides but re-use when possible
    ObCrawl<Slide *> cr = slides.Crawl ();
    int nReuse = 0;
    while (!cr.isempty ()) {
        Slide *slide = cr.popfore ();

        // does this slide already exist?
        bool bFound = false;
        kr = kids.Crawl ();
        while (!kr.isempty ()) {
            Slide *kid = dynamic_cast<Slide *>(kr.popfore ());
            if (kid == NULL)
                continue;
            if (kid->ID () == slide->ID () // IDs are not unique across dossiers
                &&
                kid->ThumbURI () == slide->ThumbURI () &&
                kid->FullURI () == slide->FullURI () &&
                kid->ViddleName () == slide->ViddleName ()) {
                AppendChild (kid);
                bFound = true;
                nReuse++;
            }
        }

        // didn't find kid so add the new slide
        if (!bFound)
            AppendChild (slide);
    }

    UpdateCurrentSlideInfo ();
    DoLayout ();
}

bool Deck::InsertSlide (Slide *slide) {
    if (slide == NULL)
        return false;
    if (slide->ID ().IsEmpty ()) {
        fprintf (stderr, "Error: can't add slide with no UID\n");
        return false;
    }

    // make sure index is sane, and silently fix it
    int64 index = slide->Ordinal () - 1;
    if (index < 0)
        index = 0;
    else if (index > ChildCount ())
        index = ChildCount ();

    ObRetort ret = InsertChildAt (slide, index);
    if (ret.IsError ()) {
        fprintf (stderr,
                 "Error: failed to insert slide as child (%s)\n",
                 ret.Description ().utf8 ());
        return false;
    }

    UpdateCurrentSlideInfo ();
    DoLayout ();

    return true;
}

ObRetort Deck::SwapSlidesByID (const Str &idA, const Str &idB) {
    if (chirren == NULL)
        return OB_NOT_FOUND;

    int ixA = IndexFromID (idA);
    int ixB = IndexFromID (idB);

    // printf("deck swap: %d <-> %d\n", ixA, ixB);

    if (ixA < 0 || ixB < 0)
        return OB_NOT_FOUND;
    if (ixA == ixB)
        return OB_ALREADY_PRESENT;

    chirren->SwapElemsAt (ixA, ixB);

    // update internal state and layout
    UpdateCurrentSlideInfo ();
    DoLayout ();

    return OB_OK;
}

ObRetort Deck::ReorderSlide (const Str &id, int64 ordinal) {
    int ix = IndexFromID (id);
    if (ix < 0)
        return OB_NOT_FOUND;

    // TODO update internal ordinals

    MoveNthChildTo (ix, ordinal - 1);

    // update internal state and layout
    UpdateCurrentSlideInfo ();
    DoLayout ();

    return OB_OK;
}

ObRetort Deck::RemoveSlideByID (const Str &id) {
    int ix = IndexFromID (id);
    if (ix < 0)
        return OB_ALREADY_PRESENT;

    ObRetort ret = RemoveChildAt (ix);
    if (ret.IsError ())
        return ret;
    ObliterateCondemnedChildren (); // really get rid of children

    // special handling if we killed the current slide
    if (currentID == id) {
        if (currentIndex >= ChildCount ())
            currentIndex = ChildCount () - 1;
        Slide *slide = dynamic_cast<Slide *>(NthChild (currentIndex));
        currentID = (slide == NULL ? "" : slide->ID ());
    } else
        UpdateCurrentSlideInfo ();

    // update internal state and layout
    DoLayout ();

    return OB_OK;
}

bool Deck::UpdateCurrentSlideInfo () {
    // search for the slide that matches the current ID
    Str firstID;
    int firstIndex = -1;
    ObCrawl<KneeObject *> kr = ChildCrawl ();
    int index = 0;
    while (!kr.isempty ()) {
        Slide *slide = dynamic_cast<Slide *>(kr.popfore ());
        if (slide == NULL)
            continue;
        if (slide->ID () == currentID) {
            currentIndex = index;
            return true;
        }
        if (firstID.IsEmpty ()) {
            firstID = slide->ID ();
            firstIndex = index;
        }
        ++index;
    }

    // we didn't find the current ID so set according to first slide (if it
    // exists)
    currentID = firstID;
    currentIndex = firstIndex;
    return false;
}

ObRetort Deck::SetCurrentSlideByIndex (int iSlide) {
    if (iSlide == currentIndex)
        return OB_NOTHING_TO_DO;
    Slide *slide = dynamic_cast<Slide *>(NthChild (iSlide));
    if (slide == NULL)
        return OB_NOT_FOUND;
    currentIndex = iSlide;
    currentID = slide->ID ();
    return OB_OK;
}

ObRetort Deck::SetCurrentSlideByID (const Str &id) {
    return SetCurrentSlideByIndex (IndexFromID (id));
}

int Deck::IndexFromID (const Str &id) const {
    ObCrawl<KneeObject *> kr = ChildCrawl ();
    for (int i = 0; !kr.isempty (); i++) {
        Slide *slide = dynamic_cast<Slide *>(kr.popfore ());
        if (slide == NULL)
            continue;
        if (slide->ID () == id)
            return i;
    }

    // we didn't find the given ID
    return -1;
}

Str Deck::IDFromIndex (int index) const {
    if (index < 0 || index >= ChildCount ())
        return Str ();
    Slide *slide = dynamic_cast<Slide *>(NthChild (index));
    if (slide == NULL)
        return Str ();
    return slide->ID ();
}

ObRetort Deck::GotoSlide (int iSlide) {
    if (iSlide < 0 || iSlide >= ChildCount ())
        return OB_BAD_INDEX;

    FlatThing *slide = dynamic_cast<FlatThing *>(NthChild (iSlide));
    if (slide == NULL)
        return OB_NOT_FOUND;

    SetCurrentSlideByIndex (iSlide);
    MoveSlides (LocGoalVal () - slide->LocGoalVal ());

    return OB_OK;
}

ObRetort Deck::PreviousSlide () {
    if (currentIndex < 1)
        return OB_NOTHING_TO_DO;
    return GotoSlide (currentIndex - 1);
}

ObRetort Deck::NextSlide () {
    if (currentIndex >= NumSlides () - 1)
        return OB_NOTHING_TO_DO;
    return GotoSlide (currentIndex + 1);
}

void Deck::OffsetSlidesHard (const Vect &dv) {
    ObCrawl<KneeObject *> cr = ChildCrawl ();
    while (!cr.isempty ()) {
        FlatThing *kid = dynamic_cast<FlatThing *>(cr.popfore ());
        if (kid == NULL)
            continue;
        SoftVect *sv = kid->LocSoft ();
        if (sv != NULL)
            sv->Offset (dv);
        else
            kid->SetLocHard (kid->LocGoalVal () + dv);
    }
}

void Deck::MoveSlides (const Vect &dv) {
    ObCrawl<KneeObject *> cr = ChildCrawl ();
    while (!cr.isempty ()) {
        FlatThing *kid = dynamic_cast<FlatThing *>(cr.popfore ());
        if (kid == NULL)
            continue;
        kid->SetLoc (kid->LocGoalVal () + dv);
    }
}

void Deck::DoLayout () {
    if (layoutMode == FullscreenSlides)
        DoLayoutFull ();
    else
        DoLayoutOverview ();
}

void Deck::DoLayoutFull () {
    if (currentIndex < 0)
        return; // no slides

    // respect 1080p aspect ratio
    // double aspectRatio = 1920.0 / 1080.0;
    double kidw = Width ();
    double kidh = Height ();

    // child size should inscribe slide in deck
    // kidh = kidw / aspectRatio;
    //   if (kidh > Height()){ // unless
    //     kidh = Height();
    //     kidw = aspectRatio * kidh;
    //   }

    // position the slides
    Vect vNextSlide = Over () * (kidw + gap);
    Vect center = LocOf (FlatThing::V::Center, FlatThing::H::Center);
    Vect vk = center - vNextSlide * currentIndex; // start at far left

    // printf("layout deck: (%.2f, %.2f, %.2f)\n", center.x, center.y,
    // center.z);

    ObCrawl<KneeObject *> cr = ChildCrawl ();
    while (!cr.isempty ()) {
        FlatThing *kid = dynamic_cast<FlatThing *>(cr.popfore ());
        if (kid == NULL)
            continue;

        // only update location if its new -- should avoid animation problems
        if (vk.DistFrom (kid->LocGoalVal ()) > 0.001) {
            kid->SetAlignment (FlatThing::V::Center, FlatThing::H::Center);
            kid->SetLoc (vk);
        }

        kid->SetSize (kidw, kidh);
        kid->OrientLikeOther (this);
        vk += vNextSlide;
    }
}

void Deck::DoLayoutOverview () {
    if (currentIndex < 0)
        return; // no slides

    // compute header and base heights
    const double headerRatio = 0.2;
    const double headerHeight = headerRatio * Height ();
    const double baseHeight = Height () - headerHeight;

    // figure out how wide kids should be so that they all fit
    const int nKids = ChildCount ();
    const double gapRatio = 0.1;
    const double kidw = Width () / (nKids * (1.0 + gapRatio) - gapRatio);
    const double kidh = baseHeight;
    const double gapw = kidw * gapRatio;

    // position the slides
    Vect vNextSlide = Over () * (kidw + gapw);
    Vect vbl = LocOf (FlatThing::V::Bottom, FlatThing::H::Left);
    Vect vk = vbl + Up () * kidh / 2 + Over () * kidw / 2;
    Vect vVisible = Up () * headerHeight;

    ObCrawl<KneeObject *> cr = ChildCrawl ();
    while (!cr.isempty ()) {
        FlatThing *kid = dynamic_cast<FlatThing *>(cr.popfore ());
        if (kid == NULL)
            continue;
        Slide *slide = dynamic_cast<Slide *>(kid);

        Vect v = vk;
        if (slide != NULL && slide->Visible ())
            v += vVisible;

        // only update location if its new -- should avoid animation problems
        if (v.DistFrom (kid->LocGoalVal ()) > 0.001) {
            kid->SetAlignment (FlatThing::V::Center, FlatThing::H::Center);
            kid->SetLoc (v);
        }

        kid->SetSize (kidw, kidh);
        kid->OrientLikeOther (this);
        vk += vNextSlide;
    }
}

Slide *Deck::SlideFromIndex (int index) {
    if (index < 0 || index >= ChildCount ())
        return NULL;
    return dynamic_cast<Slide *>(NthChild (index));
}

Slide *Deck::SlideFromID (const Str &id) {
    return SlideFromIndex (IndexFromID (id));
}
