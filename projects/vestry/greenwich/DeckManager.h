#ifndef __DECK_MANAGER__
#define __DECK_MANAGER__

#include <queue>
#include <libNoodoo/GlyphoString.h>
#include <libImpetus/OEBlurt.h>
#include <libImpetus/OEDisplacement.h>
#include <libAfferent/Gestator.h>
#include <libBasement/TWrangler.h>
#include <libPlasma/c++/Hose.h>
#include "MzShove.h"
#include "PushbackViz.h"
#include "ViewWarning.h"
#include "Deck.h"

namespace oblong {
namespace greenwich {

using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;
using namespace oblong::afferent;
using namespace oblong::sluice;

class DeckManager : public FlatThing,
                    public MzShove::Evts,
                    public OEBlurt::Evts {
    PATELLA_SUBCLASS (DeckManager, FlatThing);

public:
    DeckManager (Gestator *g);
    ~DeckManager ();

    void ShowMessage (const Str &msg);
    void SetDossierTitle (const Str &msg);
    void ShowDeck ();

    bool SendJoinRequest ();
    bool SendHeartbeat ();
    bool RequestDeckDetails ();
    bool RequestMezzState ();

    static void SetNetFetchPool (const Str &pool_name);
    static Hose *NetFetchHose ();
    static Str URIPrefix (); // TODO should be per instance

    virtual ObRetort Inhale (Atmosphere *atm);

    virtual ObRetort AcknowledgeSizeChange ();
    virtual ObRetort AcknowledgeLocationChange ();
    virtual ObRetort AcknowledgeOrientationChange ();

    virtual ObRetort MzShoveBegin (MzShoveBeginEvent *e, Atmosphere *atm);
    virtual ObRetort MzShoveContinue (MzShoveContinueEvent *e, Atmosphere *atm);
    virtual ObRetort MzShoveEnd (MzShoveEndEvent *e, Atmosphere *atm);

    virtual ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *atm);
    virtual ObRetort OEBlurtRepeat (OEBlurtRepeatEvent *e, Atmosphere *atm);
    virtual ObRetort OEBlurtVanish (OEBlurtVanishEvent *e, Atmosphere *atm);

    virtual ObRetort MetabolizeVisionInfo (const Protein &prt, Atmosphere *atm);

    virtual ObRetort MezzJoinResponse (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzDeckDetails (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzStateInfo (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzNewSlide (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzDeleteSlide (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzReorderSlide (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzOpenDossier (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzCloseDossier (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzOhayo (const Protein &prt, Atmosphere *atm);
    virtual ObRetort MezzHeartbeat (const Protein &prt, Atmosphere *atm);

    virtual ObRetort NetFetchResponse (const Protein &prt, Atmosphere *atm);

protected:
    enum VisMode { MODE_MSG = 0, MODE_DECK };

    Slaw PushbackParametersMap () const;
    Slaw GetSlawForSelfID ();
    ObRetort ProcessNetFetchProtein (const Protein &prt);
    Slide *NewSlide (Str uid,
                     int64 ordinal,
                     Str source,
                     Str sThumbURI,
                     Str sFullURI,
                     Str sViddleName,
                     bool bMainDeck);
    void DoLayout ();

    ObRef<Deck *> deck, overviewDeck;
    ObRef<PushbackViz *> pbviz;
    ObRef<ViewWarning *> viewWarn;
    ObRef<GlyphoString *> glyphMsg;
    ObRef<GlyphoString *> glyphTitle;

    ObRef<TWrangler *> deckWrangler;

    VisMode visMode;
    RoddedGlimpser *hand_glim;
    Str shimmy_prov;
    float64 lockDist, displaceShove;
    float64 timeDossierOpen, timeLastJoinRequest;
    float64 timeLastMyHeartbeat, timeLastMezHeartbeat;
    Hose *hoseMz;
    std::queue<Protein> netFetchProts;
    bool bJoined;
    Slaw slawLastJoinID;
    Str uriPrefix;
    Str dossierName;

    static Hose *hoseNetFetch;
    static DeckManager *instance;
};
}
} // end namespace

#endif
