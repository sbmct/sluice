#ifndef __SLIDE_H__
#define __SLIDE_H__

#include "libNoodoo/GlyphoString.h"
#include "libNoodoo/FlatThing.h"
#include <libStaging/TexRect.h>

namespace oblong {
namespace greenwich {

using namespace oblong::noodoo;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;
using namespace oblong::staging;

class Slide : public FlatThing {
    PATELLA_SUBCLASS (Slide, FlatThing);

public:
    Slide (const Str &uid);
    Slide (const Str &uid,
           int64 ordinal,
           const Str &source,
           const Str &thumbUrl,
           const Str &fullUrl,
           const Str &viddle,
           bool bRequestImage = true);

    virtual ObRetort Inhale (Atmosphere *atm);
    virtual void DrawSelf (VisiFeld *v, Atmosphere *a);

    virtual ObRetort AcknowledgeSizeChange ();
    virtual ObRetort AcknowledgeOrientationChange ();
    virtual ObRetort AcknowledgeLocationChange ();

    Str ID () const { return id; }
    void SetImage (TexGL *texture);
    TexRect *Image () { return ~img; }
    int64 Ordinal () const { return ordinal; }

    Str ThumbURI () const { return thumbURI; }
    Str FullURI () const { return fullURI; }
    Str ViddleName () const { return viddleName; }

    bool Visible () const { return bVisible; }
    void SetVisible (bool b) { bVisible = b; }

    float64 RadiusInDirection (const Vect &dir) const;

protected:
    void Construct (const Str &uid,
                    int64 ordinal,
                    const Str &source,
                    const Str &thumbURI,
                    const Str &fullURI,
                    const Str &viddleName);

    Str id, source, thumbURI, fullURI, viddleName;
    int64 ordinal;
    ObRef<TexRect *> img;
    ObRef<GlyphoString *> glyph;
    bool bRequestedImage;
    bool bVisible;
};
}
} // end namespace

#endif
