
/* (c) 2010 Oblong Industries */

#ifndef BIQUAD_GUARD
#define BIQUAD_GUARD

#include "libLoam/c++/ObRetort.h"
#include "libLoam/c++/Str.h"
#include "libNoodoo/VidQuad.h"
#include "libImpetus/OEBlurt.h"

namespace oblong {
namespace plasma {
class Protein;
}
};

namespace oblong {
namespace mezzanine {
namespace feldstream {

class BiQuad : public oblong::noodoo::VidQuad,
               public oblong::impetus::OEBlurt::Evts {
    PATELLA_SUBCLASS (BiQuad, VidQuad);

protected:
    oblong::loam::Str skip_key;

public:
    BiQuad ();
    BiQuad (const VidQuad &vq);
    virtual ~BiQuad ();

    virtual oblong::loam::ObRetort
    OEBlurtAppear (oblong::impetus::OEBlurtAppearEvent *e,
                   oblong::basement::Atmosphere *atm);

    void SetSkipKey (const oblong::loam::Str &key) { skip_key = key; }

    void TimedSeek (float32 fraction);

    oblong::loam::ObRetort SeekAndPlay (const oblong::plasma::Protein &prot,
                                        oblong::basement::Atmosphere *atm);
    oblong::loam::ObRetort ScrubAndPlay (const oblong::plasma::Protein &prot,
                                         oblong::basement::Atmosphere *atm);

private:
    void WhereAmI ();
};
}
}
} // feldstream, mezzanine, oblong

#endif /* BIQUAD_GUARD */
