
/* (c)  oblong industries */

#include <libBasement/LinearColor.h>
#include <libLoam/c++/ob-math-utils.h>
#include <libNoodoo/VidQuad.h>
#include <libMedia/PngImageClot.h>
//#include "../../quartermaster/ViddleSet.h"
#include <projects/quartermaster/ViddleSet.h>

#include <libNoodoo/ObStyle.h>
#include <libNoodoo/GLMiscellany.h>

#include "TriCorn.h"

using namespace oblong::noodoo;
using namespace oblong::media;
using namespace oblong::basement;
using namespace oblong::loam;
using namespace oblong::plasma;
using namespace oblong::quartermaster;

class CapQuad : public ShowyThing, public OEPointing::Evts {
    PATELLA_SUBCLASS (CapQuad, KneeObject);

public:
    TexQuad *tq;
    ObRef<VidQuad *> vqref;
    ObTrove<TriCorn *> corn_row;

    // Basic creation
    CapQuad (ImageClot *ic, Hose *hname, const Str &xfile);

    ObRetort CreateCorners (VisiFeld *vf);
    ObRetort ResetCorners ();
    ObRetort SetCorners ();
    ObRetort AddViddle (Viddle *v);
    ObRetort DropViddle ();

    ObRetort CapToPng ();

    // What do we do when the mouse clicks down?
    ObRetort OEPointingHarden (OEPointingHardenEvent *e, Atmosphere *atm);

    ObRetort MetabolizeUpdateCoord (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeSetTestOut (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeTriggerCapture (const Protein &prt, Atmosphere *atm);

    bool QueryWhitenImage () { return whiten_image; }
    bool SetWhitenImage (bool ns) { return whiten_image = ns; }

    bool QueryOutputToFile () { return output_to_file; }
    bool SetOutputToFile (bool ns);

    Str QueryOutputFilename () { return output_filename; }
    void SetOutputFilename (Str nfn) { output_filename = nfn; }
    void ClearOutputFilename () { output_filename = ""; }

    ObRetort ResetTransformCoordinates ();
    ObRetort SetTransformCoordinates (const Slaw &coordlist);

    ObRetort SpewTransformCoordinates ();

    Slaw SlawTransformCoordinates ();
    ObRetort SaveTransformCoordinates ();

    ObRetort RealToQuadCoords (VisiFeld *v, Vect &r, Vect &q);

// private:
OB_PRIVATE:

    void CQDepositPrime (int32);
    void CQDepositCapture (Slaw &, int32);

    bool whiten_image;
    bool output_to_file;
    Str output_filename;
    Str transform_filename;
    Hose *output_hose;
    double tcoords[16];
    int32 scarecrow;

    float64 mhei; // Used to modify height calculations in letter box case
    float64 mwid; // Used to modify width calculations in pillar box case
};
