
/* (c)  oblong industries */

#include <libBasement/KneeObject.h>
#include <libBasement/AsympFloat.h>
#include <libBasement/AsympVect.h>
#include <libBasement/AsympColor.h>
#include <libBasement/SineColor.h>

#include <libNoodoo/GLMiscellany.h>
#include <libNoodoo/VisiDrome.h>
#include <libNoodoo/FlatThing.h>
#include <libNoodoo/GlyphoString.h>
#include <libNoodoo/ObStyle.h>

using namespace oblong::noodoo;

class TriCorn : public FlatThing {
    PATELLA_SUBCLASS (TriCorn, FlatThing);

    SOFT_MACHINERY (SoftColor, TriCornColor);

    bool selected;

public:
    ObColor DefaultBasicColor () {
        static ObColor iro = ObColor (0.7);
        return iro;
    }

    static ObColor DefaultSelectedColor () {
        static ObColor iro = ObColor (1.0);
        return iro;
    }

    TriCorn (Vect loc, Vect norm, Vect over, float sz) : FlatThing () {
        selected = false;
        InstallLoc (new AsympVect (loc));
        InstallNorm (new AsympVect (norm));
        InstallOver (new AsympVect (over));
        InstallWidth (new AsympFloat (sz));
        InstallHeight (new AsympFloat (sz));
        //    InstallTriCornColor (new AsympColor (DefaultBasicColor ()));
        InstallTriCornColor (new SineColor (
            DefaultBasicColor (), ObColor (0.2, 0.2), 0.25, 0.0));
    }

    bool QuerySelected () { return selected; }

    void SetSelected (bool b) {
        selected = b;

        if (selected) {
            TriCornColorSoft ()->Set (DefaultSelectedColor ());
        } else {
            TriCornColorSoft ()->Set (DefaultBasicColor ());
        }
    }

    void ToggleSelected () { SetSelected (!QuerySelected ()); }

    virtual void DrawSelf (VisiFeld *v, Atmosphere *a) { // draw the corner

        OB_glColor (TriCornColor ());

        // Vect center = LocOf (V::Center, H::Center);
        Vect center = Loc ();

        glBegin (GL_QUADS);
        Vect corner = LocOf (FlatThing::V::Bottom, FlatThing::H::Left);
        Vect third = (corner - center) / 3.0 + center;
        OB_glVertex (corner);
        OB_glVertex (third);
        corner = LocOf (FlatThing::V::Top, FlatThing::H::Left);
        third = (corner - center) / 3.0 + center;
        OB_glVertex (third);
        OB_glVertex (corner);
        glEnd ();

        glBegin (GL_QUADS);
        OB_glVertex (corner);
        OB_glVertex (third);
        corner = LocOf (FlatThing::V::Top, FlatThing::H::Right);
        third = (corner - center) / 3.0 + center;
        OB_glVertex (third);
        OB_glVertex (corner);
        glEnd ();
    }

private:
    TriCorn ();
};
