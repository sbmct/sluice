if [ `pgrep -f fletcher | head -1` ]
then
	echo stopping fletcher
	kill -2 `pgrep -f fletcher`
fi

if [ `pgrep -f matloc | head -1` ]
then
	echo stopping matloc
	kill -2 `pgrep -f matloc`
fi
