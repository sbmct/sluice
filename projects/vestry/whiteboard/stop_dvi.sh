if [ `pgrep -f qm-provisioner | head -1` ]
then
	echo stopping qm-provisioner
	kill -2 `pgrep -f qm-provisioner`
fi

if [ `pgrep -f qm-viddle | head -1` ]
then
	echo stopping qm-viddle
	kill -2 `pgrep -f qm-viddle`
fi

if [ `pgrep -f qm-middle-manager | head -1` ]
then
	echo stopping qm-middle-manager
	kill -2 `pgrep -f qm-middle-manager`
fi

if [ `pgrep -f pool_tcp_server | head -1` ]
then
	echo stopping pool-server
	kill `pgrep -f pool_tcp_server`
fi

