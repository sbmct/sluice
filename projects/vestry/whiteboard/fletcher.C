
/* (c)  oblong industries */

#include <libPlasma/c++/Plasma.h>

#include <libLoam/c++/ArgParse.h>

#include <libBasement/UrDrome.h>
//#include <libBasement/Atmosphere.h>

#include <iostream>

// Defines time in seconds before we assume connection to Mezz is lost
#define MEZZ_CONNECTION_TIMEOUT 90

// Defines time in seconds between (outgoing) heartbeats
#define MEZZ_HEARTBEAT_INTEVAL 15

using namespace oblong::plasma;
using namespace oblong::loam;
using namespace oblong::basement;

// using std::cerr;
// using std::cout;
using std::endl;

// GripesPool is where the gesural info comes from
static Str WhiteboardPool = "tcp://localhost/whiteboard";
static Str InputPool = "mz-from-native";
static Str OutputPool = "mz-into-native";
static Str AssetPool = "mz-incoming-assets";
static Str MezzAddr = "";
static Str LogFile = "";

// static Str fromstr = "whiteboard";
static Str fromstr = "browser-white107";

UrDrome *drome;

class Handler : public KneeObject {
    PATELLA_SUBCLASS (Handler, KneeObject);

public:
    bool joined;
    Slaw capslaw;
    Str image_id;
    int32 capcount;

    // Twas a day when we would wait until we received the image data from
    // matloc before requesting to upload the image.  There were less dangers
    // of race conditions back then.  Now we add capid, an id that is sent
    // to fletcher from matloc, which is in fact derived from a timestamp
    // when the user clicks the whiteboard.  We send this as the transaction
    // id to mezz, and check it when we get an upload images response.  We
    // also check it when we get the new capture data from matloc.  This way
    // we ensure that we are uploading the same data we originally promised.
    // If we get out of order, we'll abort.  This will only happen when a
    // user clicks again before the original upload is complete.

    int64 capid;
    int32 counter;
    int32 expiry;

    Str dossier;
    Str state;

    FatherTime timer;
    float32 interval;

    Handler (float32 hbeat);

    void Dropped () {
        joined = false;
        expiry = MEZZ_CONNECTION_TIMEOUT;
        dossier = "";
        state = "unknown";
    }
    void ResetTO () { expiry = MEZZ_CONNECTION_TIMEOUT; }
    ObRetort Travail (Atmosphere *atm);

    ObRetort ReachOut ();
    ObRetort AskToUpload ();
    void DoDropIn ();

    ObRetort MetabolizeHearHeartbeat (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeNewImage (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeNewImagePrime (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeJoinResponse (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeStateResponse (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeUploadResponse (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeOpenPSA (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeClosePSA (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeAlivePSA (const Protein &prt, Atmosphere *atm);
};

Handler::Handler (float32 hbeat) : KneeObject (), timer () {
    joined = false;
    capcount = 0;
    counter = 0;
    interval = hbeat;
    expiry = MEZZ_CONNECTION_TIMEOUT;

    state = "unknown";
    dossier = "";

    // Note: AppendMetabolizer uses Search not GapSearch...
    // provide list o slaw accoringly...

    AppendMetabolizer (Slaw::List ("new-capture-prime"),
                       &Handler::MetabolizeNewImagePrime,
                       "new-image-prime");

    AppendMetabolizer (
        Slaw::List ("new-capture"), &Handler::MetabolizeNewImage, "new-image");

    AppendMetabolizer (Slaw::List ("response", "join"),
                       &Handler::MetabolizeJoinResponse,
                       "join-response");

    AppendMetabolizer (Slaw::List ("heartbeat"),
                       &Handler::MetabolizeHearHeartbeat,
                       "hear-heartbeat");

    AppendMetabolizer (Slaw::List ("response", "mez-state"),
                       &Handler::MetabolizeStateResponse,
                       "state-response");

    AppendMetabolizer (Slaw::List ("response", "upload-images"),
                       &Handler::MetabolizeUploadResponse,
                       "upload-response");

    AppendMetabolizer (Slaw::List ("psa", "open-dossier"),
                       &Handler::MetabolizeOpenPSA,
                       "open-psa");

    AppendMetabolizer (Slaw::List ("psa", "close-dossier"),
                       &Handler::MetabolizeClosePSA,
                       "close-psa");

    AppendMetabolizer (Slaw::List ("psa", "ohayo-gozaimasu"),
                       &Handler::MetabolizeAlivePSA,
                       "alive-psa");
}

ObRetort Handler::Travail (Atmosphere *atm) {
    float64 time = timer.CurTime ();

    if (time > interval) {
        if (joined) {
            expiry -= int32 (interval);
            if (expiry < 0) {
                Dropped ();
                printf (
                    "Mezz is awfully quiet.  Assuming connection dropped.\n");
            }
        }
        ReachOut ();
        timer.ZeroTime ();
    }

    return OB_OK;
}

ObRetort Handler::ReachOut () {
    Protein p;

    Str tid;
    tid.Sprintf ("%d", counter++);

    if (joined) { // Send heartbeat
        printf ("Sending heartbeat: P2010\n");
        p = Protein (Slaw::List ("mezzanine",
                                 "prot-spec v1.0",
                                 "request",
                                 "heartbeat",
                                 "from:",
                                 Slaw::List (fromstr, tid)));
    } else { // Send request to join
        printf ("Sending request to join: p2010\n");
        p = Protein (Slaw::List ("mezzanine",
                                 "prot-spec v1.0",
                                 "request",
                                 "join",
                                 "from:",
                                 Slaw::List (fromstr, tid)));
    }

    drome->PoolDeposit (OutputPool, p);

    return OB_OK;
}

ObRetort Handler::AskToUpload () {

    int64 numi = 1;
    Str tid;
    tid.Sprintf ("%d", (int)capid);

    printf ("Sending can I upload: p2160\n");
    Protein p = Protein (Slaw::List ("mezzanine",
                                     "prot-spec v1.0",
                                     "request",
                                     "upload-images",
                                     "from:",
                                     Slaw::List (fromstr, tid)),
                         Slaw::Map ("type", "asset", "num-images", numi));
    // Change asset to "both" above if that is preferred.

    drome->PoolDeposit (OutputPool, p);

    return OB_OK;
}

void Handler::DoDropIn () {
    printf ("DoDropIn: entered\n");
    if (image_id.IsNull () || capslaw.IsNull ())
        return;

    printf ("DoDropIn: Looks like we're ready!\n");
    Str tid;
    tid.Sprintf ("%d", capcount);

    Protein p = Protein (Slaw::List ("mezzanine",
                                     "prot-spec v1.0",
                                     "request",
                                     "image-ready",
                                     "from:",
                                     Slaw::List (fromstr, tid),
                                     "to:",
                                     "asset-manager"),
                         Slaw::Map ("dossier",
                                    dossier,
                                    "uid",
                                    image_id,
                                    "file-name",
                                    "wb_upload.png",
                                    "data",
                                    capslaw));

    drome->PoolDeposit (AssetPool, p);

    printf ("DoDropIn: Resetting to blank slate\n");
    image_id.SetToNull ();
    capslaw = Slaw ();
}

ObRetort Handler::MetabolizeHearHeartbeat (const Protein &prt,
                                           Atmosphere *atm) {
    if (prt.Origin () != InputPool)
        return OB_OK;
    printf ("Received heartbeat: p5000\n");
    //  Slaw ingests = prt . Ingests ();

    ResetTO ();

    return OB_OK;
}

ObRetort Handler::MetabolizeNewImagePrime (const Protein &prt,
                                           Atmosphere *atm) {
    Slaw ingests = prt.Ingests ();

    ingests.MapFind ("capid").Into (capid);

    printf ("A new image is on the way from Matloc\n");

    AskToUpload ();

    return OB_OK;
}

// MetabolizeNewImage fires when a new image is received from matloc.
// It calls DoDropIn() which will drop the new image in the mezz asset
// pool if an upload images response has already been received, otherwise
// it will wait to receive that, which fires MetabolizeUploadResponse
ObRetort Handler::MetabolizeNewImage (const Protein &prt, Atmosphere *atm) {
    Slaw ingests = prt.Ingests ();

    capslaw = ingests.MapFind ("data");

    int64 checkid;

    ingests.MapFind ("capid").Into (checkid);

    if (checkid != capid) {
        fprintf (stderr, "Interesting. Prime id != image id.\n");
        fprintf (stderr, " Maybe the user is clicking too quickly?\n");
        return OB_OK;
    }

    if (capslaw.IsNull ()) {
        fprintf (stderr, "New image protein with invalid data!\n");
        return OB_OK;
    }

    printf ("Received new image from Matloc\n");

    DoDropIn ();
    // This increment must happen after AskToUpload
    capcount = counter++;

    return OB_OK;
}

ObRetort Handler::MetabolizeJoinResponse (const Protein &prt, Atmosphere *atm) {
    Slaw descrips = prt.Descrips ();
    Slaw ingests = prt.Ingests ();
    bool response;

    Slaw addr;
    int64 idx;
    Str target;

    if ((idx = descrips.ListFind ("to:")) > -1)
        addr = descrips.Nth (++idx);
    else {
        fprintf (stderr, "Join slaw without to: field\n");
        return OB_OK;
    }

    if (!(addr.Nth (0).Into (target) && target == fromstr)) {
        printf ("Join response sent to %s\n", target.utf8 ());
        return OB_OK; // Yes a join response, but not ours...
    }

    if (!ingests.MapFind ("permission").Into (response)) {
        fprintf (stderr, "Invalid p6010\n");
        return OB_OK;
    }

    joined = response;

    if (!joined) {
        printf ("Attempt to join mezzanine session refused\n");
        return OB_OK;
    }

    printf ("Successfully joined mezzanine session\n");

    Str tid;
    tid.Sprintf ("%d", ++counter);

    printf ("Requesting mez-state: p2020\n");
    Protein p = Protein (Slaw::List ("mezzanine",
                                     "prot-spec v1.0",
                                     "request",
                                     "mez-state",
                                     "from:",
                                     Slaw::List (fromstr, tid)));

    drome->PoolDeposit (OutputPool, p);

    return OB_OK;
}

ObRetort Handler::MetabolizeStateResponse (const Protein &prt,
                                           Atmosphere *atm) {
    Slaw descrips = prt.Descrips ();
    Slaw ingests = prt.Ingests ();

    Slaw addr;
    int64 idx;
    Str target;

    if ((idx = descrips.ListFind ("to:")) > -1)
        addr = descrips.Nth (++idx);
    else {
        fprintf (stderr, "state response without to: field\n");
        return OB_OK;
    }

    if (!(addr.Nth (0).Into (target) && target == fromstr))
        return OB_OK; // Yes a state response, but not ours...

    Str statestr;

    if (!ingests.MapFind ("state").Into (statestr)) {
        fprintf (stderr, "Invalid p6020\n");
        return OB_OK;
    }

    state = statestr;

    if (state !=
        "dossier") { // Only really care about when we are in the dossier state
        printf ("State response, state = %s\n", state.utf8 ());
        dossier = "";
        return OB_OK;
    }

    Slaw dd = ingests.MapFind ("dossier");
    if (!dd.IsMap ()) {
        fprintf (stderr, "Invalid p6020: No dossier map\n");
        return OB_OK;
    }

    Str uidstr;

    if (!dd.MapFind ("uid").Into (uidstr)) {
        fprintf (stderr, "Invalid p6020: Couldn't retrieve dossier uid\n");
        return OB_OK;
    }

    dossier = uidstr;
    printf ("state: dossier is set to %s\n", dossier.utf8 ());

    return OB_OK;
}

ObRetort Handler::MetabolizeOpenPSA (const Protein &prt, Atmosphere *atm) {
    Slaw descrips = prt.Descrips ();
    Slaw ingests = prt.Ingests ();

    Slaw dd = ingests.MapFind ("dossier");
    if (!dd.IsMap ()) {
        fprintf (stderr, "Invalid p5050: No dossier map\n");
        descrips.SpewToStderr ();
        fprintf (stderr, "------------------\n");
        ingests.SpewToStderr ();
        return OB_OK;
    }

    Str uidstr;

    if (!dd.MapFind ("uid").Into (uidstr)) {
        fprintf (stderr, "Invalid p6020: Couldn't retrieve dossier uid\n");
        return OB_OK;
    }

    state = "dossier";
    dossier = uidstr;
    printf ("open: dossier is set to %s\n", dossier.utf8 ());

    return OB_OK;
}

ObRetort Handler::MetabolizeClosePSA (const Protein &prt, Atmosphere *atm) {
    //  Slaw descrips = prt . Descrips ();
    Slaw ingests = prt.Ingests ();

    Slaw dd = ingests.MapFind ("dossier");
    if (!dd.IsMap ()) {
        fprintf (stderr, "Invalid p5090: No dossier map\n");
        return OB_OK;
    }

    state = "portal";
    dossier = "";
    printf ("Dossier is now closed\n");

    return OB_OK;
}

ObRetort Handler::MetabolizeAlivePSA (const Protein &prt, Atmosphere *atm) {
    joined = false;
    printf ("Mezzanine has just come online\n");

    Dropped ();
    ReachOut ();

    return OB_OK;
}

// MetabolizeUploadResponse fires when mezz sends a p6160, telling
// fletcher to go ahead and upload the image.  Said image may not
// be here yet, however.  DoDropIn() is called which will drop said
// image in the pool, if here, otherwise it will drop it in the pool
// when MetabolizeNewImage is fired.
ObRetort Handler::MetabolizeUploadResponse (const Protein &prt,
                                            Atmosphere *atm) {
    Slaw descrips = prt.Descrips ();
    Slaw ingests = prt.Ingests ();

    Slaw addr;
    int64 idx;
    int64 iseq;
    Str target;

    printf ("Upload response received\n");

    if ((idx = descrips.ListFind ("to:")) > -1)
        addr = descrips.Nth (++idx);
    else {
        fprintf (stderr, "Upload response without to: field\n");
        return OB_OK;
    }

    printf ("Checking address:\n");

    addr.Spew (stderr);

    if (!(addr.Nth (0).Into (target) && target == fromstr))
        return OB_OK; // Yes an upload response, but not ours...

    printf ("Checking sequence:\n");
    if (!(addr.Nth (1).Into (iseq) && iseq == capid)) {
        printf ("Oops: %d != %d\n", (int)iseq, (int)capid);
        return OB_OK; // Yes an upload response, capid doesn't match...
    }

    printf ("Checking for error:\n");

    if (!ingests.MapFind ("error-code").IsNull ()) {
        Str summary;
        ingests.MapFind ("summary").Into (summary);
        fprintf (stderr, "Upload error: %s\n", summary.utf8 ());
        return OB_OK;
    }

    Slaw uids = ingests.MapFind ("uids");

    uids.Nth (0).Into (image_id);

    printf ("Permission to upload %s\n", image_id.utf8 ());

    DoDropIn ();

    return OB_OK;
}

const char *usage =
    "Usage: fletcher [OPTIONS]\n"
    "\n"
    "`fletcher' works alongside `matloc' to provide whiteboard capture\n"
    "to Mezzanine.\n"
    "\n"
    "fletcher is responsible for registering with mezzanine and notifying\n"
    "mezzanine when new image assets are available for upload.\n"
    "\n"
    "fletcher requires the following pools to operate:\n"
    "\n"
    "whiteboard pool: the whiteboard deposits new image captures in this "
    "pool.\n"
    "  this poolname is not effected by the mezz address.\n"
    "\n"
    "mezz input pool: fletcher uses this pool to communicate to the mezzanine\n"
    "\n"
    "mezz output: fletcher uses this pool to receive communications from "
    "mezzanine\n"
    "\n"
    "mezz asset: the fletcher uses this pool to upload to mezzanine\n"
    "\n"
    "mezz address: uri address of mezzanine prepended to the mezz pools\n"
    "  e.g. tcp://mezzishonor/\n"
    "\n"
    "Command-line options:\n";

int main (int ac, char **av) {
    ArgParse::apflag showhelp = false;

    ArgParse ap;
    ap.UsageHeader (usage);
    ap.ArgFlag ("help", "\a print this help, then exit", &showhelp);

    ap.ArgString ("whiteboard-pool",
                  "\a whiteboard pool"
                  "(default " +
                      WhiteboardPool + ")",
                  &WhiteboardPool);
    ap.ArgString ("input-pool",
                  "\a input pool"
                  "(default " +
                      InputPool + ")",
                  &InputPool);
    ap.ArgString ("output-pool",
                  "\a output pool"
                  "(default " +
                      OutputPool + ")",
                  &OutputPool);
    ap.ArgString ("asset-pool",
                  "\a asset pool"
                  "(default " +
                      AssetPool + ")",
                  &AssetPool);
    ap.ArgString ("mezz-addr",
                  "\a mezz address"
                  "(default " +
                      MezzAddr + ")",
                  &MezzAddr);
    ap.ArgString ("logfile",
                  "\a file to log to, if such is desired"
                  "(none by default)",
                  &LogFile);

    ap.Alias ("help", "h");
    ap.Alias ("whiteboard-pool", "w");
    ap.Alias ("input-pool", "i");
    ap.Alias ("output-pool", "o");
    ap.Alias ("asset-pool", "a");
    ap.Alias ("mezz-addr", "m");
    ap.Alias ("logfile", "l");

    if (!ap.Parse (ac - 1, av + 1)) {
        std::cerr << ap.ErrorMessage () << std::endl << ap.UsageMessage ();
        return EXIT_FAILURE;
    }

    if (showhelp) {
        std::cout << ap.UsageMessage ();
        return EXIT_SUCCESS;
    }

    if (!MezzAddr.IsEmpty ()) {
        Str Mpath = MezzAddr;
        InputPool = Mpath.Append (InputPool);
        Mpath = MezzAddr;
        OutputPool = Mpath.Append (OutputPool);
        Mpath = MezzAddr;
        AssetPool = Mpath.Append (AssetPool);
    }

    if (!LogFile.IsEmpty ()) {
        fprintf (stderr, "opening log: %s\n", LogFile.utf8 ());
        FILE *newout = fopen (LogFile, "w");
        if (newout) {
            dup2 (fileno (newout), fileno (stdout));
            dup2 (fileno (newout), fileno (stderr));
            fclose (newout);
        }
    }

    drome = new UrDrome ("fletcher", ac, av);

    Handler *h = new Handler (MEZZ_HEARTBEAT_INTEVAL);
    h->SetName ("whiteboard-handler");

    drome->AppendChild (h);

    std::cerr << "Opening " << WhiteboardPool << " to connect to matloc"
              << endl;

    if (OB_OK == drome->PoolParticipate (WhiteboardPool, h))
        std::cout << "Opened " << WhiteboardPool << " to connect to matloc"
                  << endl;
    else {
        std::cerr << "Failed to open " << WhiteboardPool << endl;
        return 1;
    }

    if (OB_OK == drome->PoolParticipate (InputPool, h))
        std::cout << "Opened " << InputPool << " to receive from Mezz" << endl;
    else {
        std::cerr << "Failed to open " << InputPool << endl;
        return 1;
    }

    if (OB_OK == drome->PoolParticipate (OutputPool, h))
        std::cout << "Opened " << OutputPool << " to send to Mezz" << endl;
    else {
        std::cerr << "Failed to open " << OutputPool << endl;
        return 1;
    }

    if (OB_OK == drome->PoolParticipate (AssetPool, h))
        std::cout << "Opened " << AssetPool << " to upload to Mezz" << endl;
    else {
        std::cerr << "Failed to open " << AssetPool << endl;
        return 1;
    }

    drome->SetRespirePeriod (1.0 / 120.0);
    drome->Respire ();

    drome->Delete ();
    return 0;
}
