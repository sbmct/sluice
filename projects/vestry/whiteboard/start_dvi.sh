cd /home/demo/src/mezzanine/projects/vestry/whiteboard
/opt/oblong/g-speak-64-2/bin/p-stop -w "fletcher.*" "lt-qm-middle-manager.*" "matloc.*" "qm-viddle.*" "wb.*" > /dev/null 2>&1

if [ `pgrep -u demo -f pool_tcp_server | head -1` ]
then
        echo pool server running
else
        echo starting pool server
        /opt/oblong/g-speak-64-2/bin/pool_tcp_server > /var/log/oblong/pool_tcp_server.log 2>&1
fi

echo starting qm-provisioner
GST_PLUGIN_PATH=/opt/oblong/g-speak-64-2/lib/gstreamer-0.10 /opt/oblong/g-speak-64-2/bin/qm-provisioner -name=wb qm ../../mezzanine/demo/dvi.conf > /var/log/oblong/qm-provisioner.log 2>&1 &
