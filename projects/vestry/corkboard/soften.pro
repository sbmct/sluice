# g-speak 2.0.50, libYaml 0.1.3                              -*- mode: text -*-
%YAML 1.1
%TAG ! tag:oblong.com,2009:slaw/
--- !protein
descrips:
- corkboard
- soften
ingests:
  # Required
  provenance: osx-mouse
  loc: !vector [175.0, 1230.0, 342.0]
  aim: !vector [0.0, 0.0, -1.0]

  # Optional
  grab-point: !vector [0.50, 0.50]
  image-bounds: !vector [100.0, 100.0]
  
  # Debugging
  image-file: /tmp/fullteam16.jpg
...
