
/* (c)  oblong industries */

#include "GravityHelper.h"

static Vect antiGravity = Vect (0, 1, 0);

// static
void GravityHelper::OrientToGravity (VisiFeld *vf, LocusThing *lt) {
    if (vf->Norm ().Dot (antiGravity) <
        0.1) { // orientation is more or less "on a wall"
        Vect overRelativeToGravity = antiGravity.Cross (vf->Norm ());
        lt->SetOrientation (vf->Norm (), overRelativeToGravity);
    } else { // potentially table-like
        lt->OrientLikeFeld (vf);
    }
}

void GravityHelper::OrientToGravity (LocusThing *refThing, LocusThing *lt) {
    if (refThing->Norm ().Dot (antiGravity) <
        0.1) { // orientation is more or less "on a wall"
        Vect overRelativeToGravity = antiGravity.Cross (refThing->Norm ());
        lt->SetOrientation (refThing->Norm (), overRelativeToGravity);
    } else { // potentially table-like
        lt->OrientLikeOther (refThing);
    }
}

// static
bool GravityHelper::VisifeldIsMorePortrait (VisiFeld *vf) {
    double angle = acos (vf->Over ().Dot (antiGravity));

    if (angle > M_PI / 4.0 &&
        angle < 3 * M_PI / 4) { // more orthoganal to gravity than parallel
        return false;
    } else {
        return true;
    }
}
