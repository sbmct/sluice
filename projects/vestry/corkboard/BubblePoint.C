
/* (c)  oblong industries */

#include "BubblePoint.h"
#include "GravityHelper.h"

ObRetort BubblePoint::OEPointingAppear (OEPointingAppearEvent *e,
                                        Atmosphere *atm) {
    VisiFeld *previousVF = ~last_vf;
    ObRetort response = super::OEPointingAppear (e, atm);
    if (previousVF != ~last_vf)
        GravityHelper::OrientToGravity (~last_vf, this);
    return response;
}

ObRetort BubblePoint::OEPointingMove (OEPointingMoveEvent *e, Atmosphere *atm) {
    VisiFeld *previousVF = ~last_vf;
    ObRetort response = super::OEPointingMove (e, atm);
    if (previousVF != ~last_vf)
        GravityHelper::OrientToGravity (~last_vf, this);
    return response;
}
