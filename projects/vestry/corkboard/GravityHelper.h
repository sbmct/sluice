
/* (c)  oblong industries */

#ifndef GRAVITYHELPER_H_KEEPS_THINGS_LOOKING_UP
#define GRAVITYHELPER_H_KEEPS_THINGS_LOOKING_UP

#include <libNoodoo/VisiDrome.h>

using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::noodoo;

class GravityHelper : public AnkleObject {
    PATELLA_SUBCLASS (GravityHelper, AnkleObject);

public:
    static void OrientToGravity (VisiFeld *vf, LocusThing *lt);
    static void OrientToGravity (LocusThing *refThing, LocusThing *lt);
    static bool VisifeldIsMorePortrait (VisiFeld *vf);
};

#endif /* end of include guard: GRAVITYHELPER_H_KEEPS_THINGS_LOOKING_UP */
