# g-speak 2.0.50, libYaml 0.1.3                              -*- mode: text -*-
%YAML 1.1
%TAG ! tag:oblong.com,2009:slaw/
--- !protein
descrips:
- corkboard
- harden
ingests:
  # Required
  provenance: osx-mouse
  image-uri: http://oblong.com/images/fullteam16.jpg
  loc: !vector [-225.0, 1500.0, 342.0]
  aim: !vector [0.0, 0.0, -1.0]

  # Optional
  grab-point: !vector [0.50, 0.50, 0.0]
  image-bounds: !vector [100.0, 100.0, 0.0]
  
  # Debugging
  image-file: /tmp/fullteam16.jpg
...
