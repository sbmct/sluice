
/* (c)  Oblong Industries */

#ifndef CALIBULATHING_H_BORING_HEADER_GUARD
#define CALIBULATHING_H_BORING_HEADER_GUARD

#include <libLoam/c++/Str.h>
#include <libLoam/c++/Vect.h>

#include <libNoodoo/ShowyThing.h>
#include <libNoodoo/TexQuad.h>
#include <libNoodoo/VisiFeld.h>

#include <libImpetus/OEPointing.h>
#include <libImpetus/OEBlurt.h>

using namespace oblong::loam;
using namespace oblong::noodoo;

class CorkItem : public AnkleObject {
    PATELLA_SUBCLASS (CorkItem, AnkleObject);

public:
    // slaw based...
    Str provenance;
    Str image_url;
    Vect loc;
    Vect aim;
    // optional
    Str owner;
    Vect grab_point;
    Vect image_bounds;
    // debug
    Str image_file;
    // tranitory
    TexQuad *quad;
    bool is_offscreen;
    double inscribeWidth;
    double inscribeHeight;

    CorkItem () {
        grab_point = Vect (0.5, 0.5, 0.0);
        quad = NULL;
        is_offscreen = true;
    }

    // this sets and remembers the inscribe amount
    void DoInscribe (double width, double height) {
        inscribeWidth = width;
        inscribeHeight = height;
        ReInscribe ();
    }

    void ReInscribe () {
        if (quad != NULL)
            quad->InscribeInside (inscribeWidth, inscribeHeight);
    }
};

class CorkZone : public FlatThing,
                 public OEPointing::Evts,
                 public OEBlurt::Evts {
    PATELLA_SUBCLASS (CorkZone, FlatThing);

public:
    CorkZone ();
    virtual ~CorkZone ();

    double maxImageWidth, maxImageHeight;

    ObMap<Str, CorkItem *> liveItems;
    ObTrove<Str> ownedLiveItems;
    ObTrove<CorkItem *> storedItems;
    ObMap<VisiFeld *, CorkItem *> storedPerFeld;

    Str standardBoardImage;
    Str cacheDir;
    ObTrove<TexQuad *> boardBacks;

    virtual void RegisterVisifeld (VisiFeld *vf);

    // size callback
    ObRetort AcknowledgeSizeChange ();

    // match-handle callbacks
    ObRetort MetabolizeHardenProtein (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeSoftenProtein (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeNetResponseProtein (const Protein &prt, Atmosphere *atm);

    ObRetort MetabolizeRandomHardening (const Protein &prt, Atmosphere *atm);
    ObRetort MetabolizeRandomSoftening (const Protein &prt, Atmosphere *atm);

    // mouse / keyboard events
    virtual ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *a);
    virtual ObRetort OEPointingHarden (OEPointingHardenEvent *e, Atmosphere *a);
    virtual ObRetort OEPointingSoften (OEPointingSoftenEvent *e, Atmosphere *a);
    virtual ObRetort OEPointingMove (OEPointingMoveEvent *e, Atmosphere *a);

    virtual void SetOutputHose (Hose *corkboardHose);
    virtual void SetNetFetchHose (Hose *netFetchHose);
    virtual void SetOwner (Str owner);

    void DebugSynthesizeHardening ();
    void DebugSynthesizeSoftening ();
    void RandomDropping (bool isSoftening);

    // map of provenance -> hex cursor texquads
    ObMap<Str, TexQuad *> fakeOvis;
    // make a new hex cursor object
    TexQuad *GetOrCreateFakeOvi (Str provenance);
    // try to change handipoint showiness
    void SetHandiPointVisibility (Str provenance, bool isVisible);

    // manage removal from list *and* scene graph
    void RemoveAnyExistingConflictingLiveItem (CorkItem *item);
    void RemoveStoredItem (CorkItem *item);
    // manage addition of item to list *and* scene graph
    void GenerateQuadAndAddToView (CorkItem *item, bool isLive);

    // get closest visifeld AND a boolean to indicate if really is outside the
    // bounds
    bool GetIntersectedVisiFeld (Vect origin,
                                 Vect aimer,
                                 Vect *hit_p,
                                 VisiFeld **vfp);
    // just update quad location
    void UpdateLocationOfItem (CorkItem *item,
                               Vect loc,
                               Vect aim,
                               bool shouldFillFeld);

    // loads jpg, png, etc.
    ImageClot *LoadVariousImageClots (Str filename);

    // given a url, give the cached file name
    Str UrlToHashedFileName (Str url);

    // sets up unique cache dir
    void SetupCorkCacheDir (Str suffix);

    // serial and deserial ization
    static Slaw CorkItemToSlaw (CorkItem *item);
    static CorkItem *SlawToCorkItem (Slaw s);
    static bool FileExists (Str filename);

    void PrintMemoryReport (Str header);

    Hose *outHose, *netHose;
    Str owner;
    Str badImage, waitImage;

    Str lastProvenance;
    Vect lastAim;
    Vect lastLoc;
};

#endif // CALIBULATHING_H_BORING_HEADER_GUARD
