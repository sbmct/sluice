
/* (c)  oblong industries */

#ifndef BUBBLE_POINT_ALWAYS_FLOATS_TO_THE_TOP
#define BUBBLE_POINT_ALWAYS_FLOATS_TO_THE_TOP

#include <libTwillig/HandiPoint.h>

using namespace oblong::impetus;
using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::noodoo;
using namespace oblong::twillig;

class BubblePoint : public HandiPoint {
    PATELLA_SUBCLASS (BubblePoint, HandiPoint);

public:
    virtual ObRetort OEPointingAppear (OEPointingAppearEvent *e,
                                       Atmosphere *atm);

    virtual ObRetort OEPointingMove (OEPointingMoveEvent *e, Atmosphere *atm);
};

#endif // BUBBLE_POINT_ALWAYS_FLOATS_TO_THE_TOP
