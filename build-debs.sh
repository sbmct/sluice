#!/bin/sh

build_dir=$(mktemp -d)
cd "$build_dir" || {
    echo "Could not cd to build directory: ${build_dir}" 1>&2
    exit 1
}

die() {
    x="$@"
    echo "$x" 1>&2
    # rm -fr "${build_dir}"
    exit 1
}

if test "x" = "x$GIT_USER"
then
	GIT_PREFIX=""
else
	GIT_PREFIX="${GIT_USER}@"
fi

git clone "${GIT_PREFIX}git-mct:/opt/git/repo/sluice.git" || \
    die "Could not clone sluice"

cd sluice
env YOBUILD=/opt/oblong/deps ./make_all_debs.sh || die "Sluice's build failed"

echo "Sluice debs built in ${PWD}"
